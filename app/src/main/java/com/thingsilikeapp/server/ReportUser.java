package com.thingsilikeapp.server;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.ReportUserItem;
import com.thingsilikeapp.data.model.RewardHistoryItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class ReportUser {

    public static ReportUser getDefault(){
        return new ReportUser();
    }


    public APIRequest getReportUser(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ReportUserItem>>(context) {
            @Override
            public Call<CollectionTransformer<ReportUserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformer(Url.getReportUser(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllOrderResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "user")
                .showSwipeRefreshLayout(true)
                .setSwipeRefreshLayout(swipeRefreshLayout);

        return apiRequest;
    }

    public void getRewardHistory(Context context, String type, int id, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<RewardHistoryItem>>(context) {
            @Override
            public Call<SingleTransformer<RewardHistoryItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getRewardHistory(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RewardHistoryResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, type)
//                .addParameter(Keys.server.key.HISTORY_ID, id)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();

    }

    public APIRequest getUnReportUser(Context context, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ReportUserItem>>(context) {
            @Override
            public Call<SingleTransformer<ReportUserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleUnReport(Url.getUnReportUser(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UnReportUserResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.TYPE, "user")
                .addParameter(Keys.server.key.REFERENCE_ID, id)
                .execute();

        return apiRequest;
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<RewardHistoryItem>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ReportUserItem>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ReportUserItem>> requestSingleUnReport(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class AllOrderResponse extends APIResponse<CollectionTransformer<ReportUserItem>> {
        public AllOrderResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RewardHistoryResponse extends APIResponse<SingleTransformer<RewardHistoryItem>> {
        public RewardHistoryResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UnReportUserResponse extends APIResponse<SingleTransformer<ReportUserItem>> {
        public UnReportUserResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}

