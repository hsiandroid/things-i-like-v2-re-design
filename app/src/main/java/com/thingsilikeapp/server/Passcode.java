package com.thingsilikeapp.server;

import android.content.Context;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.BaseTransformer;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Passcode {

    public static Passcode getDefault(){
        return new Passcode();
    }

    public APIRequest passcode(Context context, String passcode) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getSetPasscode(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PasscodeResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "wallet")
                .addParameter(Keys.server.key.PASSCODE,passcode)
                .showDefaultProgressDialog("Setting passcode...")
                .execute();

        return apiRequest;
    }

    public APIRequest validate_passcode(Context context, String old_passcode) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getValidatePasscode(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PasscodeResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "wallet")
                .addParameter(Keys.server.key.OLD_PASSCODE,old_passcode)
                .showDefaultProgressDialog("Validating passcode...")
                .execute();

        return apiRequest;
    }

    public APIRequest update_passcode(Context context, String old_passcode, String new_passcode) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getUpdatePasscode(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PasscodeResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "wallet")
                .addParameter(Keys.server.key.OLD_PASSCODE,old_passcode)
                .addParameter(Keys.server.key.NEW_PASSCODE,new_passcode)
                .showDefaultProgressDialog("Updating passcode...")
                .execute();

        return apiRequest;
    }

    public APIRequest lock_gem(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestLock(Url.getLockSend(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UnlockResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "wallet")
                .showDefaultProgressDialog("Locking passcode...")
                .execute();

        return apiRequest;
    }

    public APIRequest unlock_gem(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestLock(Url.getUnlockSend(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UnlockResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "wallet")
                .showDefaultProgressDialog("Unlocking passcode...")
                .execute();

        return apiRequest;
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestLock(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


        }

    public class PasscodeResponse extends APIResponse<SingleTransformer<UserItem>> {
        public PasscodeResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UnlockResponse extends APIResponse<SingleTransformer<UserItem>> {
        public UnlockResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }



}

