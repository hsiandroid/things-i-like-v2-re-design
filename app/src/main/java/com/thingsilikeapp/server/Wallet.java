package com.thingsilikeapp.server;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.BankCardItem;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.data.model.BuyGemItem;
import com.thingsilikeapp.data.model.ChatThreadModel;
import com.thingsilikeapp.data.model.ChatUserModel;
import com.thingsilikeapp.data.model.EncashHistoryItem;
import com.thingsilikeapp.data.model.EncashItem;
import com.thingsilikeapp.data.model.GetRateItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.BaseTransformer;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.security.Key;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Wallet {

    public static Wallet getDefault(){
        return new Wallet();
    }

    public APIRequest addBank(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<BankItem>>(context) {
            @Override
            public Call<SingleTransformer<BankItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getAddBank(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AddBankResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Adding...");

        return apiRequest;
    }

    public APIRequest editBank(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<BankItem>>(context) {
            @Override
            public Call<SingleTransformer<BankItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getEditBank(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new EditBankResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Updating...");

        return apiRequest;
    }

    public APIRequest RemoveBank(Context context, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<BankItem>>(context) {
            @Override
            public Call<SingleTransformer<BankItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getRemoveBank(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RemoveBankResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Removing...")
                .addParameter(Keys.server.key.SETTLEMENT_ID, id)
                .execute();

        return apiRequest;
    }

    public APIRequest validate(Context context, String gem) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getValidate(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ValidateResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Validating...")
                .addParameter(Keys.server.key.GEM_QTY, gem)
                .execute();

        return apiRequest;
    }

    public APIRequest cancel(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getCancelWithdrawal(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CancelResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Canceling...")
                .execute();

        return apiRequest;
    }

    public APIRequest request(Context context, int id,  String gem, String currency) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<EncashItem>>(context) {
            @Override
            public Call<SingleTransformer<EncashItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestEncash(Url.getRequestWithdrawal(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RequestResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Requesting...")
                .addParameter(Keys.server.key.GEM_QTY, gem)
                .addParameter(Keys.server.key.SETTLEMENT_ID, id)
                .addParameter(Keys.server.key.CURRENCY, currency)
                .execute();

        return apiRequest;
    }


    public APIRequest allBank(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<BankItem>>(context) {
            @Override
            public Call<CollectionTransformer<BankItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionModel(Url.getAllBank(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllBankResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .setPerPage(10)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public APIRequest rate(Context context) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<GetRateItem>>(context) {
            @Override
            public Call<CollectionTransformer<GetRateItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRate(Url.getRate(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RateResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(10)
                .showSwipeRefreshLayout(true).execute();

        return apiRequest;
    }

    public APIRequest history(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<EncashHistoryItem>>(context) {
            @Override
            public Call<CollectionTransformer<EncashHistoryItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestHistory(Url.getHistory(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new HistoryResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "history")
                .setPerPage(10)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public APIRequest currentRate(Context context, String country) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<GetRateItem>>(context) {
            @Override
            public Call<SingleTransformer<GetRateItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCurrentRate(Url.getCurrentRate(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CurrentRateRequestResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.COUNTRY_ISO, country)
                .execute();

        return apiRequest;
    }

    public APIRequest pending(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<EncashHistoryItem>>(context) {
            @Override
            public Call<SingleTransformer<EncashHistoryItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestPending(Url.getPendingWithdrawal(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PendingResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();

        return apiRequest;
    }

    public APIRequest buyGem(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<BuyGemItem>>(context) {
            @Override
            public Call<SingleTransformer<BuyGemItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBuyGemModel(Url.getWallet(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new BuyGemResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Loading...")
                .execute();

        return apiRequest;
    }

    public APIRequest addCard(Context context, String c_num, int c_expiry_m, String c_expiry_y) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<BankCardItem>>(context) {
            @Override
            public Call<SingleTransformer<BankCardItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCardModel(Url.getAddCard(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AddCardResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CARD_NUMBER, c_num)
                .addParameter(Keys.server.key.CARD_EXPIRY_MONTH, c_expiry_m)
                .addParameter(Keys.server.key.CARD_EXPIRY_YEAR, c_expiry_y)
                .showDefaultProgressDialog("Adding...").execute();

        return apiRequest;
    }

    public APIRequest buyCard(Context context, String amount, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<BankCardItem>>(context) {
            @Override
            public Call<SingleTransformer<BankCardItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCardModel(Url.getBuyCard(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new BuyCardResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.AMOUNT, amount)
                .addParameter(Keys.server.key.CARD_ID, id)
                .showDefaultProgressDialog("Processing transaction...").execute();

        return apiRequest;
    }

    public APIRequest removeCard(Context context, int card_id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<BankCardItem>>(context) {
            @Override
            public Call<SingleTransformer<BankCardItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCardModel(Url.getRemoveCard(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AddCardResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CARD_ID, card_id)
                .showDefaultProgressDialog("Removing...").execute();

        return apiRequest;
    }

    public APIRequest allCard(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<BankCardItem>>(context) {
            @Override
            public Call<CollectionTransformer<BankCardItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCardCollection(Url.getAllCard(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllCardResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(10)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public APIRequest allCard(Context context) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<BankCardItem>>(context) {
            @Override
            public Call<CollectionTransformer<BankCardItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCardCollection(Url.getAllCard(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllCardResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(20);

        return apiRequest;
    }


    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<BankItem>> requestSingleModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<EncashItem>> requestEncash(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


        @Multipart
        @POST("{p}")
        Call<SingleTransformer<GetRateItem>> requestCurrentRate(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<EncashHistoryItem>> requestPending(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<BankCardItem>> requestCardModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<BuyGemItem>> requestBuyGemModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<BankItem>> requestCollectionModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<EncashHistoryItem>> requestHistory(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<GetRateItem>> requestRate(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<BankCardItem>> requestCardCollection(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class AddBankResponse extends APIResponse<SingleTransformer<BankItem>> {
        public AddBankResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class AllBankResponse extends APIResponse<CollectionTransformer<BankItem>> {
        public AllBankResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class HistoryResponse extends APIResponse<CollectionTransformer<EncashHistoryItem>> {
        public HistoryResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RateResponse extends APIResponse<CollectionTransformer<GetRateItem>> {
        public RateResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class EditBankResponse extends APIResponse<SingleTransformer<BankItem>> {
        public EditBankResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class RemoveBankResponse extends APIResponse<SingleTransformer<BankItem>> {
        public RemoveBankResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class ValidateResponse extends APIResponse<BaseTransformer> {
        public ValidateResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class CancelResponse extends APIResponse<BaseTransformer> {
        public CancelResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class RequestResponse extends APIResponse<SingleTransformer<EncashItem>> {
        public RequestResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class PendingResponse extends APIResponse<SingleTransformer<EncashHistoryItem>> {
        public PendingResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class CurrentRateRequestResponse extends APIResponse<SingleTransformer<GetRateItem>> {
        public CurrentRateRequestResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class AddCardResponse extends APIResponse<SingleTransformer<BankCardItem>> {
        public AddCardResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class BuyCardResponse extends APIResponse<SingleTransformer<BankCardItem>> {
        public BuyCardResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class BuyGemResponse extends APIResponse<SingleTransformer<BuyGemItem>> {
        public BuyGemResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class AllCardResponse extends APIResponse<CollectionTransformer<BankCardItem>> {
        public AllCardResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}

