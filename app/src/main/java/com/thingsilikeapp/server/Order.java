package com.thingsilikeapp.server;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.OrderItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Order{

    public static Order getDefault(){
        return new Order();
    }

    public APIRequest create(Context context, int product_id, int address_id, String note) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<OrderItem>>(context) {
            @Override
            public Call<SingleTransformer<OrderItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getCreateOrder(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new OrderResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "detail,history")
                .addParameter(Keys.server.key.PRODUCT_ID, product_id)
                .addParameter(Keys.server.key.ADDRESS_BOOK_ID, address_id)
                .addParameter(Keys.server.key.NOTE, note)
                .showDefaultProgressDialog("Purchasing...")
                .execute();

        return apiRequest;
    }

    public APIRequest edit(Context context, int address_id, String note, String order_id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<OrderItem>>(context) {
            @Override
            public Call<SingleTransformer<OrderItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getEditOrder(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new OrderResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "detail,history")
                .addParameter(Keys.server.key.ADDRESS_BOOK_ID, address_id)
                .addParameter(Keys.server.key.ORDER_ID, order_id)
                .addParameter(Keys.server.key.NOTE, note)
                .showDefaultProgressDialog("Editing purchase...")
                .execute();

        return apiRequest;
    }

    public APIRequest cancel(Context context, int order_id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<OrderItem>>(context) {
            @Override
            public Call<SingleTransformer<OrderItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getCancelOrder(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new OrderResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.ORDER_ID, order_id)
                .showDefaultProgressDialog("Cancelling purchase...")
                .execute();

        return apiRequest;
    }

    public APIRequest myorder(Context context, int order_id, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<OrderItem>>(context) {
            @Override
            public Call<SingleTransformer<OrderItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getShowOrder(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ShowOrderResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.ORDER_ID, order_id)
                .addParameter(Keys.server.key.INCLUDE, "detail,history")
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();

        return apiRequest;
    }


    public APIRequest getAll(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<OrderItem>>(context) {
            @Override
            public Call<CollectionTransformer<OrderItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformer(Url.getMyOrder(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllOrderResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "detail,history")
                .showSwipeRefreshLayout(true)
                .setSwipeRefreshLayout(swipeRefreshLayout);

        return apiRequest;
    }

    public APIRequest getFollowers(Context context, SwipeRefreshLayout swipeRefreshLayout, int id, String keyword) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<UserItem>>(context) {
            @Override
            public Call<CollectionTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestUserCollectionTransformer(Url.getFollowers(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new FollowersResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameter(Keys.server.key.USER_ID, id)
                .addParameter(Keys.server.key.KEYWORD, keyword)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .setPerPage(10);
        
        return apiRequest;
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<OrderItem>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<OrderItem>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<UserItem>> requestUserCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);



    }

    public class AllOrderResponse extends APIResponse<CollectionTransformer<OrderItem>> {
        public AllOrderResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class OrderResponse extends APIResponse<SingleTransformer<OrderItem>> {
        public OrderResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ShowOrderResponse extends APIResponse<SingleTransformer<OrderItem>> {
        public ShowOrderResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class FollowersResponse extends APIResponse<CollectionTransformer<UserItem>> {
        public FollowersResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}

