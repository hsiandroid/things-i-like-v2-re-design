package com.thingsilikeapp.server;

import android.content.Context;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class ForgotPasscode {

    public static ForgotPasscode getDefault(){
        return new ForgotPasscode();
    }

    public APIRequest forgotPasscode(Context context){
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(ForgotPasscode.RequestService.class).requestSingleModel(Url.getForgotPasscode(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ForgotPasscodeResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CONTACT_NUMBER, "contact_number")
                .addParameter(Keys.server.key.COUNTRY_CODE, "country_code")
                .addParameter(Keys.server.key.COUNTRY, "country")
                .showDefaultProgressDialog("Verifying...")
                .execute();

        return apiRequest;
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestSingleModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestLock(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


    }

    public class ForgotPasscodeResponse extends APIResponse<SingleTransformer<UserItem>> {
        public ForgotPasscodeResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}
