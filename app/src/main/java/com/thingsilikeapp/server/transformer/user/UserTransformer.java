package com.thingsilikeapp.server.transformer.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class UserTransformer extends BaseTransformer {

    @SerializedName("data")
    @Expose
    public UserItem userItem;

    @SerializedName("first_login")
    @Expose
    public boolean first_login = false;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {

        @SerializedName("username")
        public List<String> username = null;

        @SerializedName("name")
        public List<String> name = null;

        @SerializedName("email")
        public List<String> email = null;

        @SerializedName("contact_number")
        public List<String> contact_number = null;

        @SerializedName("birthdate")
        public List<String> birthdate = null;

        @SerializedName("password")
        public List<String> password = null;

        @SerializedName("profession")
        public List<String> profession = null;

        @SerializedName("street_address")
        public List<String> street_address = null;

        @SerializedName("city")
        public List<String> city = null;

        @SerializedName("state")
        public List<String> state = null;

        @SerializedName("zip_code")
        public List<String> zip_code = null;

        @SerializedName("country")
        public List<String> country = null;
    }
}
