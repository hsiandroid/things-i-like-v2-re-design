package com.thingsilikeapp.server.transformer.user;

import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class EditProfileTransformer extends BaseTransformer {

    @SerializedName("data")
    @Expose
    public UserItem userItem;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {

        @SerializedName("current_password")
        public List<String> current_password;

        @SerializedName("new_password")
        public List<String> new_password;

        @SerializedName("new_password_confirmation")
        public List<String> new_password_confirmation;
    }
}
