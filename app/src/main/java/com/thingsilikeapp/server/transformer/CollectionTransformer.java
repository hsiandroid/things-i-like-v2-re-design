package com.thingsilikeapp.server.transformer;

import com.thingsilikeapp.vendor.server.base.BaseTransformer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class CollectionTransformer<T> extends BaseTransformer {

    @SerializedName("data")
    public List<T> data;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {

    }
}
