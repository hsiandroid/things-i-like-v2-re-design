package com.thingsilikeapp.server.transformer.wishlist;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.WishListTransactionItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class TransactionTransformer extends BaseTransformer {

    @SerializedName("data")
    public WishListTransactionItem data;
}
