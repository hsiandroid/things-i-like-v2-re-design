package com.thingsilikeapp.server.transformer.social;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.GroupFollowingItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */
public class GroupFollowingCollectionTransformer extends BaseTransformer {

    @SerializedName("data")
    public List<GroupFollowingItem> data;
}
