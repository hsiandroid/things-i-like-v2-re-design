package com.thingsilikeapp.server.transformer.notification;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.NotificationItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class SingleNotificationTransformer extends BaseTransformer {

    @SerializedName("data")
    public NotificationItem data;
}
