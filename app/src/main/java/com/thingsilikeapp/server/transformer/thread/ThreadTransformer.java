package com.thingsilikeapp.server.transformer.thread;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.ChatThreadModel;
import com.thingsilikeapp.vendor.server.transformer.BaseTransformer;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class ThreadTransformer extends BaseTransformer {

    @SerializedName("temp_id")
    public int temp_id;

    @SerializedName("data")
    public ChatThreadModel data;
}

