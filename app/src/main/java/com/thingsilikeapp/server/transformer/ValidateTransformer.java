package com.thingsilikeapp.server.transformer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.ValidateItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class ValidateTransformer extends BaseTransformer {

    @SerializedName("id")
    public  int id;

    @SerializedName("data")
    public ValidateItem data;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {

        @SerializedName("username")
        public List<String> username = null;

        @SerializedName("value")
        public List<String> value = null;

        @SerializedName("email")
        public List<String> email = null;

        @SerializedName("contact_number")
        public List<String> contact_number = null;

        @SerializedName("password")
        public List<String> password = null;

    }

}
