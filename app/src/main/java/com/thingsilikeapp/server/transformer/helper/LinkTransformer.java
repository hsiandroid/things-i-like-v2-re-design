package com.thingsilikeapp.server.transformer.helper;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class LinkTransformer extends BaseTransformer {

    @SerializedName("data")
    public Data data;

    public static class Data{

        @SerializedName("title")
        public String title;

        @SerializedName("description")
        public String description;

        @SerializedName("url")
        public String url;

        @SerializedName("cover")
        public String cover;

        @SerializedName("metas")
        public List<Metas> metas;

        @SerializedName("images")
        public List<String> images;

        public static class Metas {

            @SerializedName("name")
            public String name;

            @SerializedName("property")
            public String property;

            @SerializedName("content")
            public String content;
        }

    }
}
