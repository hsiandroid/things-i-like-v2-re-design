package com.thingsilikeapp.server.transformer.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class WalletTransformer extends BaseTransformer {

    @SerializedName("data")
    @Expose
    public UserItem userItem;

    @SerializedName("amount_sent")
    @Expose
    public String amount_sent;

    @SerializedName("refid")
    @Expose
    public String refid;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {

        @SerializedName("username")
        public List<String> username = null;

        @SerializedName("amount")
        public List<String> amount = null;
    }
}
