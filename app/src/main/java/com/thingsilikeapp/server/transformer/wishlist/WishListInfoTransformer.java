
package com.thingsilikeapp.server.transformer.wishlist;

import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class WishListInfoTransformer extends BaseTransformer {

    @SerializedName("data")
    @Expose
    public WishListItem wishListItem;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {

        @SerializedName("content")
        public List<String> content;

        @SerializedName("house_no")
        public List<String> house_no;

        @SerializedName("contact_number")
        public List<String> contact_number;

        @SerializedName("category")
        public List<String> category;

        @SerializedName("recipient")
        public List<String> recipient;

        @SerializedName("street_address")
        public List<String> street_address;

        @SerializedName("city")
        public List<String> city;

        @SerializedName("state")
        public List<String> state;

        @SerializedName("zip_code")
        public List<String> zip_code;

        @SerializedName("country")
        public List<String> country;

        @SerializedName("file")
        public List<String> file;
    }
}
