package com.thingsilikeapp.server.transformer.comment;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.CommentItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

/**
 * Created by BCTI 3 on 12/9/2016.
 */
public class CommentSingleTransformer extends BaseTransformer {

    @SerializedName("temp_id")
    public int temp_id;

    @SerializedName("data")
    public CommentItem data;
}
