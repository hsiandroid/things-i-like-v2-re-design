package com.thingsilikeapp.server.transformer.reward;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.MyRewardsItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class MyRewardShowTransformer extends BaseTransformer {

    @SerializedName("ratio")
    public String ratio;

    @SerializedName("data")
    public MyRewardsItem data;
}
