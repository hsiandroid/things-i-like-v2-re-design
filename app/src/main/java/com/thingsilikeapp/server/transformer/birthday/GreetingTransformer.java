package com.thingsilikeapp.server.transformer.birthday;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.GreetingItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class GreetingTransformer extends BaseTransformer {

    @SerializedName("num_greetings")
    public int num_greetings;

    @SerializedName("unread")
    public int unread;

    @SerializedName("read")
    public int read;

    @SerializedName("data")
    public GreetingItem data;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {

    }
}
