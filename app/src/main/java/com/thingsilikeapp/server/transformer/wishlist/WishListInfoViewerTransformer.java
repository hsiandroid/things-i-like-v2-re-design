
package com.thingsilikeapp.server.transformer.wishlist;

import com.thingsilikeapp.data.model.WishListViewerItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class WishListInfoViewerTransformer extends BaseTransformer {

    @SerializedName("data")
    @Expose
    public WishListViewerItem wishListViewerItem;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {

        @SerializedName("content")
        public List<String> content;

        @SerializedName("contact_number")
        public List<String> contact_number;

        @SerializedName("category")
        public List<String> category;

        @SerializedName("address")
        public List<String> address;
    }
}
