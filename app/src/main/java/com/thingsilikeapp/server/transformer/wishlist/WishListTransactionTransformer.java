package com.thingsilikeapp.server.transformer.wishlist;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.WishListTransactionItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class WishListTransactionTransformer extends BaseTransformer {

    @SerializedName("data")
    public List<WishListTransactionItem> data;
}
