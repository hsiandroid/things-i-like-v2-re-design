package com.thingsilikeapp.server.transformer.comment;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.CommentItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */
public class CommentCollectionTransformer extends BaseTransformer {

    @SerializedName("data")
    public List<CommentItem> data;
}
