package com.thingsilikeapp.server.transformer.user;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.GroupItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */
public class GroupCollectionTransformer extends BaseTransformer {


    @SerializedName("recent")
    public String recent;

    @SerializedName("data")
    public List<GroupItem> data;
}
