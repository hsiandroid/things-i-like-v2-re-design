package com.thingsilikeapp.server.transformer.wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class WishListRelatedTransformer extends BaseTransformer {

    @SerializedName("data")
    public List<WishListItem> data;

    @SerializedName("event")
    public EventItem event;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {

    }
}
