package com.thingsilikeapp.server.transformer;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class SingleTransformer<T, E> extends BaseTransformer {

    @SerializedName("data")
    public T data;

    @SerializedName("errors")
    public E errors;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(errors);
    }
}
