package com.thingsilikeapp.server.transformer.reward;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.RewardCategoryItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class RewardCatalogueCategoryTransformer extends BaseTransformer {

    @SerializedName("data")
    public List<RewardCategoryItem> data;
}
