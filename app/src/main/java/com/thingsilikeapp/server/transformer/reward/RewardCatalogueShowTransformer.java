package com.thingsilikeapp.server.transformer.reward;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.CatalogueItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class RewardCatalogueShowTransformer extends BaseTransformer {

    @Nullable
    @SerializedName("ratio")
    public String ratio;

    @Nullable
    @SerializedName("user")
    public UserItem user;

    @SerializedName("data")
    public CatalogueItem data;
}
