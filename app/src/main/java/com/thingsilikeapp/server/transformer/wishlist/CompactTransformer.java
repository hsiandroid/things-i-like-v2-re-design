
package com.thingsilikeapp.server.transformer.wishlist;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.WishListTransactionItem;
import com.thingsilikeapp.data.model.WishListViewerItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class CompactTransformer extends BaseTransformer {

    @SerializedName("data")
    public Data data;

    public  static class Data{

        @SerializedName("requests_count")
        public int requests_count;

        @SerializedName("activities_count")
        public int activities_count;

        @SerializedName("wishlist_viewer_hasmore")
        public boolean wishlist_viewer_hasmore;

        @SerializedName("wishlist_transaction_hasmore")
        public boolean wishlist_transaction_hasmore;

        @SerializedName("owned_wishlist_viewer_hasmore")
        public boolean owned_wishlist_viewer_hasmore;

        @SerializedName("wishlist_viewer")
        public List<WishListViewerItem> wishlist_viewer;

        @SerializedName("wishlist_transaction")
        public List<WishListTransactionItem> wishlist_transaction;

        @SerializedName("owned_wishlist_viewer")
        public List<WishListViewerItem> owned_wishlist_viewer;
    }
}
