
package com.thingsilikeapp.server.transformer.wishlist;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.WishListViewerItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class PermissionTransformer extends BaseTransformer {

    @SerializedName("data")
    public List<WishListViewerItem> data;
}
