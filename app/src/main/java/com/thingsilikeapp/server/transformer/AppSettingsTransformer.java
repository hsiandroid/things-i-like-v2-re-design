package com.thingsilikeapp.server.transformer;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.AppSettingsItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class AppSettingsTransformer extends BaseTransformer {

    @SerializedName("data")
    public Data data;

    public static class Data{
        @SerializedName("latest_version")
        public AppSettingsItem latest_version;
    }

}
