package com.thingsilikeapp.server.transformer.report;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.ReportItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

/**
 * Created by BCTI 3 on 12/9/2016.
 */
public class ReportTransformer extends BaseTransformer {

    @SerializedName("data")
    public ReportItem data;
}
