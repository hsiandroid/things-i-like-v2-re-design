package com.thingsilikeapp.server.transformer.reward;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.RewardHistoryItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class RewardHistoryTransformer extends BaseTransformer {

    @SerializedName("ratio")
    public String ratio;

    @SerializedName("user")
    public UserItem user;

    @SerializedName("data")
    public List<RewardHistoryItem> data;
}
