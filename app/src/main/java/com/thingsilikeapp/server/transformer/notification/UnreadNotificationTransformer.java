package com.thingsilikeapp.server.transformer.notification;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class UnreadNotificationTransformer extends BaseTransformer {

    @SerializedName("unread")
    public int unread = -1;
}
