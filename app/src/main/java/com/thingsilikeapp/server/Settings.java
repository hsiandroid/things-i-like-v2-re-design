package com.thingsilikeapp.server;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.BlockUserItem;
import com.thingsilikeapp.data.model.ReportPostItem;
import com.thingsilikeapp.data.model.ReportUserItem;
import com.thingsilikeapp.data.model.RewardHistoryItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Settings {



    public static Settings getDefault(){
        return new Settings();
    }


    public APIRequest getBlockUser(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<BlockUserItem>>(context) {
            @Override
            public Call<CollectionTransformer<BlockUserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformer(Url.getBlockUser(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllOrderResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "user")
                .showSwipeRefreshLayout(true)
                .setSwipeRefreshLayout(swipeRefreshLayout);

        return apiRequest;
    }

    public APIRequest blockUser(Context context, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<BlockUserItem>>(context) {
            @Override
            public Call<SingleTransformer<BlockUserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBlock(Url.blockUser(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new BlockResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.TYPE, "user")
                .addParameter(Keys.server.key.REFERENCE_ID,id)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public void getRewardHistory(Context context, String type, int id, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<RewardHistoryItem>>(context) {
            @Override
            public Call<SingleTransformer<RewardHistoryItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getRewardHistory(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RewardHistoryResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, type)
                .addParameter(Keys.server.key.HISTORY_ID, id)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();

    }

    public APIRequest getUnblockUser(Context context, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<BlockUserItem>>(context) {
            @Override
            public Call<SingleTransformer<BlockUserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleUnblock(Url.getUnBlockUser(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UnblockResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.TYPE, "user")
                .addParameter(Keys.server.key.REFERENCE_ID, id)
                .execute();

        return apiRequest;
    }

    public APIRequest getRepost(Context context, int id, String content) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<WishListItem>>(context) {
            @Override
            public Call<SingleTransformer<WishListItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleRepost(Url.getRepost(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RepostResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "info,image,owner,delivery_info,transaction")
                .addParameter(Keys.server.key.WISHLIST_ID, id)
                .addParameter(Keys.server.key.CONTENT, content)
                .showDefaultProgressDialog("Reposting...")
                .execute();

        return apiRequest;
    }




    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<RewardHistoryItem>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<BlockUserItem>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<BlockUserItem>> requestSingleUnblock(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<WishListItem>> requestSingleRepost(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<BlockUserItem>> requestBlock(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class AllOrderResponse extends APIResponse<CollectionTransformer<BlockUserItem>> {
        public AllOrderResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RewardHistoryResponse extends APIResponse<SingleTransformer<RewardHistoryItem>> {
        public RewardHistoryResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class BlockResponse extends APIResponse<SingleTransformer<BlockUserItem>> {
        public BlockResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UnblockResponse extends APIResponse<SingleTransformer<BlockUserItem>> {
        public UnblockResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RepostResponse extends APIResponse<SingleTransformer<WishListItem>> {
        public RepostResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }


}
