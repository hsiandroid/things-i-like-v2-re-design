package com.thingsilikeapp.server;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.HidePostItem;
import com.thingsilikeapp.data.model.HidePostItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;
import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class HidePost {

    public static HidePost getDefault(){
        return new HidePost();
    }


    public APIRequest getHidePost(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<HidePostItem>>(context) {
            @Override
            public Call<CollectionTransformer<HidePostItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformer(Url.getHidePost(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllOrderResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "wishlist.image")
                .showSwipeRefreshLayout(true)
                .setSwipeRefreshLayout(swipeRefreshLayout);

        return apiRequest;
    }

    public APIRequest getUnhidePost(Context context, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<HidePostItem>>(context) {
            @Override
            public Call<SingleTransformer<HidePostItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleUnhide(Url.getUnhidePost(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UnhideResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.TYPE, "wishlist")
                .addParameter(Keys.server.key.REFERENCE_ID, id)
                .execute();

        return apiRequest;
    }

    public interface RequestService {
//        @Multipart
//        @POST("{p}")
//        Call<SingleTransformer<OrderItem>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<HidePostItem>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


        @Multipart
        @POST("{p}")
        Call<SingleTransformer<HidePostItem>> requestSingleUnhide(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


    }

    public class AllOrderResponse extends APIResponse<CollectionTransformer<HidePostItem>> {
        public AllOrderResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UnhideResponse extends APIResponse<SingleTransformer<HidePostItem>> {
        public UnhideResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}

