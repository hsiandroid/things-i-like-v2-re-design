package com.thingsilikeapp.server.request.user;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.DiscoverItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.BaseTransformer;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class ChangeContactNumberRequest {

    public static ChangeContactNumberRequest getDefault(){
        return new ChangeContactNumberRequest();
    }


    public APIRequest phoneOTP(Context context,String country, String contact, String code) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getPhoneOtp(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PhoneResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.COUNTRY,country)
                .addParameter(Keys.server.key.CONTACT_NUMBER, contact)
                .addParameter(Keys.server.key.COUNTRY_CODE, code)
                .showDefaultProgressDialog("Verifying...")
                .execute();

        return apiRequest;
    }

    public APIRequest changeContact(Context context,String country, String contact, String code, String otp) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.changeContactNumber(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ChangeContactResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.COUNTRY,country)
                .addParameter(Keys.server.key.CONTACT_NUMBER, contact)
                .addParameter(Keys.server.key.COUNTRY_CODE, code)
                .addParameter(Keys.server.key.OTP_VALUE, otp)
                .addParameter(Keys.server.key.INCLUDE, "info,social,statistics,kyc")
                .showDefaultProgressDialog("Changing...")
                .execute();

        return apiRequest;
    }




    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestSingleModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestSingleUserModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


    }


    public class PhoneResponse extends APIResponse<SingleTransformer<UserItem>> {
        public PhoneResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }


    public class ChangeContactResponse extends APIResponse<SingleTransformer<UserItem>> {
        public ChangeContactResponse(APIRequest apiRequest) {
        super(apiRequest);
    }
}

}

