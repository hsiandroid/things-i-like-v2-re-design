package com.thingsilikeapp.server.request.auth;

import android.content.Context;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.server.transformer.user.UserTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class LoginRequest extends BaseRequest<UserTransformer> {

    public LoginRequest(Context context){
        super(context);
    }

    @Override
    public Call<UserTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getParameters());
    }

    @Override
    public void responseData(Response<UserTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.auth.LOGIN)
        Call<UserTransformer> requestParam(@Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<UserTransformer> response) {
            super(response);
        }
    }
}
