package com.thingsilikeapp.server.request.address;

import android.content.Context;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.AddressBookItem;
import com.thingsilikeapp.server.transformer.SingleTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class DefaultAddressRequest extends BaseRequest<SingleTransformer<AddressBookItem, AddressBookItem.Errors>> {

    public DefaultAddressRequest(Context context){
        super(context);
    }

    @Override
    public Call<SingleTransformer<AddressBookItem, AddressBookItem.Errors>> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<SingleTransformer<AddressBookItem, AddressBookItem.Errors>> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.addressbook.DEFAULT)
        Call<SingleTransformer<AddressBookItem, AddressBookItem.Errors>> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<SingleTransformer<AddressBookItem, AddressBookItem.Errors>> response) {
            super(response);
        }
    }
}
