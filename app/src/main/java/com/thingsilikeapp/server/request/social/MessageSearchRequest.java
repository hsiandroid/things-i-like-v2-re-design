package com.thingsilikeapp.server.request.social;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.HidePostItem;
import com.thingsilikeapp.data.model.HidePostItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;
import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class MessageSearchRequest {

    public static MessageSearchRequest getDefault(){
        return new MessageSearchRequest();
    }


    public APIRequest getSearch(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<HidePostItem>>(context) {
            @Override
            public Call<CollectionTransformer<HidePostItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformer(Url.getHidePost(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllOrderResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "wishlist.image")
                .showSwipeRefreshLayout(true)
                .setSwipeRefreshLayout(swipeRefreshLayout);

        return apiRequest;
    }


    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<HidePostItem>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


        @Multipart
        @POST("{p}")
        Call<SingleTransformer<HidePostItem>> requestSingleUnhide(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


    }

    public class AllOrderResponse extends APIResponse<CollectionTransformer<HidePostItem>> {
        public AllOrderResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UnhideResponse extends APIResponse<SingleTransformer<HidePostItem>> {
        public UnhideResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}

