package com.thingsilikeapp.server.request.reward;

import android.content.Context;

import com.thingsilikeapp.server.transformer.reward.MyRewardTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.config.Keys;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class MyRewardRequest extends BaseRequest<MyRewardTransformer> {

    public MyRewardRequest(Context context){
        super(context);
    }

    @Override
    public Call<MyRewardTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<MyRewardTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.rewards.INDEX)
        Call<MyRewardTransformer> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<MyRewardTransformer> response) {
            super(response);
        }
    }
}
