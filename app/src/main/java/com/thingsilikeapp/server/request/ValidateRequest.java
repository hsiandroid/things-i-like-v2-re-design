package com.thingsilikeapp.server.request;

import android.content.Context;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.server.transformer.ValidateTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class ValidateRequest extends BaseRequest<ValidateTransformer> {

    public ValidateRequest(Context context){
        super(context);
    }

    @Override
    public Call<ValidateTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<ValidateTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.auth.VALIDATE)
        Call<ValidateTransformer> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<ValidateTransformer> response) {
            super(response);
        }
    }
}
