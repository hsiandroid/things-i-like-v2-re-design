package com.thingsilikeapp.server.request.wishlist;

import android.content.Context;

import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.config.Keys;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class WishListAddressRequest extends BaseRequest<WishListInfoTransformer> {

    public WishListAddressRequest(Context context){
        super(context);
    }

    @Override
    public Call<WishListInfoTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<WishListInfoTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.wishlist.EDIT_DELIVERY_INFO)
        @FormUrlEncoded
        Call<WishListInfoTransformer> requestParam(@Header("Authorization") String authorization, @FieldMap Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<WishListInfoTransformer> response) {
            super(response);
        }
    }
}
