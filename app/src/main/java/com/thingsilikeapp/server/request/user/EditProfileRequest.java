package com.thingsilikeapp.server.request.user;

import android.content.Context;

import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.config.Keys;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class EditProfileRequest extends BaseRequest<UserTransformer> {

    public EditProfileRequest(Context context){
        super(context);
    }

    @Override
    public Call<UserTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getMultipartBody());
    }

    @Override
    public void responseData(Response<UserTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @Multipart
        @POST(Keys.server.route.profile.EDIT)
        Call<UserTransformer> requestParam(@Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<UserTransformer> response) {
            super(response);
        }
    }
}
