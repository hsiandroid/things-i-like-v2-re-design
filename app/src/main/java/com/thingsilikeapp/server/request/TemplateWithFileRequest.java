package com.thingsilikeapp.server.request;

import android.content.Context;

import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;
import com.thingsilikeapp.config.Keys;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class TemplateWithFileRequest extends BaseRequest<BaseTransformer> {

    public TemplateWithFileRequest(Context context){
        super(context);
    }

    @Override
    public Call<BaseTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getRequestBodyParameters());
    }

    @Override
    public void responseData(Response<BaseTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @Multipart
        @POST(Keys.server.route.moment.CREATE)
        Call<BaseTransformer> requestParam(@PartMap Map<String, RequestBody> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<BaseTransformer> response) {
            super(response);
        }
    }
}
