package com.thingsilikeapp.server.request.gift;

import android.content.Context;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.BackgroundItem;
import com.thingsilikeapp.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class BackgroundRequest extends BaseRequest<CollectionTransformer<BackgroundItem>> {

    public BackgroundRequest(Context context){
        super(context);
    }

    @Override
    public Call<CollectionTransformer<BackgroundItem>> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<CollectionTransformer<BackgroundItem>> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.photo.BACKGROUNDS)
        Call<CollectionTransformer<BackgroundItem>> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<CollectionTransformer<BackgroundItem>> response) {
            super(response);
        }
    }
}
