package com.thingsilikeapp.server.request.social;

import android.content.Context;

import com.thingsilikeapp.server.transformer.social.RecentSearchTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.config.Keys;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class SuggestedSearchRequest extends BaseRequest<RecentSearchTransformer> {

    public SuggestedSearchRequest(Context context){
        super(context);
    }

    @Override
    public Call<RecentSearchTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<RecentSearchTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.social.SUGGESTED_SEARCH)
        Call<RecentSearchTransformer> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<RecentSearchTransformer> response) {
            super(response);
        }
    }
}
