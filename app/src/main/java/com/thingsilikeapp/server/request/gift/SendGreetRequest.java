package com.thingsilikeapp.server.request.gift;

import android.content.Context;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class SendGreetRequest extends BaseRequest<WishListInfoTransformer> {

    public SendGreetRequest(Context context){
        super(context);
    }

    @Override
    public Call<WishListInfoTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getMultipartBody());
    }
    @Override
    public void responseData(Response<WishListInfoTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @Multipart
        @POST(Keys.server.route.birthday.GREET)
        Call<WishListInfoTransformer> requestParam(@Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts) ;
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<WishListInfoTransformer> response) {
            super(response);
        }
    }
}
