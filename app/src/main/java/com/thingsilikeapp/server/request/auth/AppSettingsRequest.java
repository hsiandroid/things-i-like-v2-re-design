package com.thingsilikeapp.server.request.auth;

import android.content.Context;

import com.thingsilikeapp.server.transformer.AppSettingsTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.config.Keys;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class AppSettingsRequest extends BaseRequest<AppSettingsTransformer> {

    public AppSettingsRequest(Context context){
        super(context);
        setCheckToken(false);
    }

    @Override
    public Call<AppSettingsTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam();
    }

    @Override
    public void responseData(Response<AppSettingsTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.APP_SETTINGS)
        Call<AppSettingsTransformer> requestParam();
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<AppSettingsTransformer> response) {
            super(response);
        }
    }
}
