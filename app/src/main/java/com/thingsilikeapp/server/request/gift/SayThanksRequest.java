package com.thingsilikeapp.server.request.gift;

import android.content.Context;

import com.thingsilikeapp.server.transformer.birthday.GreetingTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.config.Keys;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class SayThanksRequest extends BaseRequest<GreetingTransformer> {

    public SayThanksRequest(Context context){
        super(context);
    }

    @Override
    public Call<GreetingTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<GreetingTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.birthday.SAY_THANKS)
        Call<GreetingTransformer> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<GreetingTransformer> response) {
            super(response);
        }
    }
}
