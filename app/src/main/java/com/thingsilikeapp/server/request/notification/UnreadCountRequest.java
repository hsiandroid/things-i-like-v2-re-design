package com.thingsilikeapp.server.request.notification;

import android.content.Context;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.server.transformer.notification.UnreadNotificationTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class UnreadCountRequest extends BaseRequest<UnreadNotificationTransformer> {

    public UnreadCountRequest(Context context){
        super(context);
    }

    @Override
    public Call<UnreadNotificationTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<UnreadNotificationTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.notifications.UNREAD_COUNT)
        Call<UnreadNotificationTransformer> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<UnreadNotificationTransformer> response) {
            super(response);
        }
    }
}
