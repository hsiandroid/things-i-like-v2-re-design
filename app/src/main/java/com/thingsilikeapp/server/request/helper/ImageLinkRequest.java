package com.thingsilikeapp.server.request.helper;

import android.os.AsyncTask;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by BCTI 3 on 5/11/2017.
 */

public class ImageLinkRequest {

    private String url;

    public void setUrl(String url){
        this.url = url;
    }

    public void execute(){
        new AsyncTask<String, Void, Boolean> (){

            @Override
            protected Boolean doInBackground(String... params) {
                URLConnection connection;
                String contentType = "";
                try {
                    connection = new URL(url).openConnection();
                    contentType = connection.getHeaderField("Content-Type");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return contentType.startsWith("image/");
            }

            @Override
            protected void onPostExecute(Boolean s) {
                super.onPostExecute(s);

            }
        }.execute();
    }
    public class ServerResponse {
        boolean isImage;
        public ServerResponse(boolean isImage) {
            this.isImage = isImage;
        }
    }
}
