package com.thingsilikeapp.server.request.social;

import android.content.Context;

import com.thingsilikeapp.data.model.FeedCategoryItem;
import com.thingsilikeapp.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.config.Keys;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class FeedCategoryRequest extends BaseRequest<CollectionTransformer<FeedCategoryItem>> {

    private boolean isUser;

    public FeedCategoryRequest(Context context, boolean isUser){
        super(context);
        this.isUser = isUser;
    }

    @Override
    public Call<CollectionTransformer<FeedCategoryItem>> onCreateCall() {
        if(isUser){
            return getRetrofit().create(RequestService.class).requestUserParam(getAuthorization(), getParameters());
        }
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<CollectionTransformer<FeedCategoryItem>> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.profile.TIMELINE)
        Call<CollectionTransformer<FeedCategoryItem>> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);

        @POST(Keys.server.route.users.TIMELINE)
        Call<CollectionTransformer<FeedCategoryItem>> requestUserParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<CollectionTransformer<FeedCategoryItem>> response) {
            super(response);
        }
    }
}
