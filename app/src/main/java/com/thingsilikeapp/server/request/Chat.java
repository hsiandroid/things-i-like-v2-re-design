package com.thingsilikeapp.server.request;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.ChatModel;
import com.thingsilikeapp.data.model.ChatThreadModel;
import com.thingsilikeapp.data.model.ChatUserModel;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Chat {

    public static Chat getDefault() {
        return new Chat();
    }

    public APIRequest createChat(Context context, String title, String participants) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getCreate(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CreateChatResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.TITLE, title)
                .addParameter(Keys.server.key.PARTICIPANTS, participants)
                .showDefaultProgressDialog("Creating...")
                .execute();

        return apiRequest;
    }


    public APIRequest myChat(Context context, String type, SwipeRefreshLayout swipeRefreshLayout, String keyword) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ChatModel>>(context) {
            @Override
            public Call<CollectionTransformer<ChatModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionModel(Url.getMyChat(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new MyChatResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "info,latest_message.info")
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .setPerPage(10)
                .addParameter(Keys.server.key.TYPE, type)
                .addParameter(Keys.server.key.KEYWORD, keyword)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public APIRequest myMessage(Context context, String type, SwipeRefreshLayout swipeRefreshLayout, String keyword) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ChatModel>>(context) {
            @Override
            public Call<CollectionTransformer<ChatModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionModel(Url.getMyMessages(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new MyMessageResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "info,latest_message.info")
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .setPerPage(10)
                .addParameter(Keys.server.key.TYPE, type)
                .addParameter(Keys.server.key.KEYWORD, keyword)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }



    public APIRequest join(Context context, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatThreadModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatThreadModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSendMessage(Url.getJoinGroup(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ChatJoinResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CHAT_ID, id)
                .addParameter(Keys.server.key.INCLUDE, "info,author.info")
                .showDefaultProgressDialog("Joining...")
                .execute();

        return apiRequest;
    }


    public APIRequest sendMessage(Context context, int id, String content, int temp_id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatThreadModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatThreadModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSendMessage(Url.getSendMessage(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new SendMessageResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CHAT_ID, id)
                .addParameter(Keys.server.key.TEMP_ID, temp_id)
                .addParameter(Keys.server.key.CONTENT, content)
                .addParameter(Keys.server.key.INCLUDE, "info,author.info")
                .execute();

        return apiRequest;
    }

    public APIRequest newSendMessage(Context context, int id, String content, int temp_id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatThreadModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatThreadModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSendMessage(Url.getNewChat(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new SendNewMessageResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.USER_ID, id)
                .addParameter(Keys.server.key.TEMP_ID, temp_id)
                .addParameter(Keys.server.key.CONTENT, content)
                .addParameter(Keys.server.key.INCLUDE, "info,author.info")
                .execute();

        return apiRequest;
    }

    public APIRequest sendFile(Context context, int id, File file, int temp_id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatThreadModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatThreadModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSendMessage(Url.getSendFile(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new SendFileResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CHAT_ID, id)
                .addParameter(Keys.server.key.TEMP_ID, temp_id)
                .addParameter(Keys.server.key.FILE, file)
                .addParameter(Keys.server.key.INCLUDE, "info,author.info")
                .execute();

        return apiRequest;
    }

    public APIRequest NewSendFile(Context context, int id, File file, int temp_id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatThreadModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatThreadModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSendMessage(Url.getNewUpload(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new SendNewFileResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CHAT_ID, id)
                .addParameter(Keys.server.key.TEMP_ID, temp_id)
                .addParameter(Keys.server.key.FILE, file)
                .addParameter(Keys.server.key.INCLUDE, "info,author.info")
                .execute();

        return apiRequest;
    }

    public APIRequest removeMessage(Context context, int chat_id, int msg_id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatThreadModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatThreadModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRemoveMessage(Url.getRemoveMessage(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                 EventBus.getDefault().post(new RemoveMessageResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CHAT_ID, chat_id)
                .addParameter(Keys.server.key.MSG_ID, msg_id)
                .execute();

        return apiRequest;
    }

    public APIRequest chatThread(Context context, int id, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ChatThreadModel>>(context) {
            @Override
            public Call<CollectionTransformer<ChatThreadModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestChatThread(Url.getChatThread(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ChatThreadResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "info,author.info")
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .setPerPage(10)
                .addParameter(Keys.server.key.CHAT_ID, id)
                .showSwipeRefreshLayout(true);
        return apiRequest;
    }

    public APIRequest editChatName(Context context, String title, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getUpdateChatName(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdateChatResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.TITLE, title)
                .addParameter(Keys.server.key.CHAT_ID, id)
                .execute();

        return apiRequest;
    }

    public void icon(Context context, File file, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestChatIcon(Url.getChatIcon(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ChatIconResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.server.key.FILE, file)
                .addParameter(Keys.server.key.CHAT_ID, id)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }

    public APIRequest detail(Context context, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getChatDeatil(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ChatDetailResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CHAT_ID, id)
                .addParameter(Keys.server.key.INCLUDE, "info")
                .execute();

        return apiRequest;
    }

    public APIRequest leave(Context context, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatUserModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatUserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleChatUser(Url.getLeaveGroup(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LeaveResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CHAT_ID, id)
                .addParameter(Keys.server.key.INCLUDE, "info")
                .execute();

        return apiRequest;
    }

    public void allModerator(Context context, int id, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ChatUserModel>>(context) {
            @Override
            public Call<CollectionTransformer<ChatUserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionChatUser(Url.getAllModerators(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CollectionModeratorResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "info,author.info")
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .addParameter(Keys.server.key.CHAT_ID, id)
                .setPerPage(5)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    public void allMember(Context context, int id, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ChatUserModel>>(context) {
            @Override
            public Call<CollectionTransformer<ChatUserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionChatUser(Url.getAllMembers(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CollectionMemberResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "info,author.info")
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .addParameter(Keys.server.key.CHAT_ID, id)
                .setPerPage(5)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    public APIRequest demoteParticipant(Context context, int id, int user_id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatUserModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatUserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleChatUser(Url.getDemoteModerator(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new DemoteUserResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CHAT_ID, id)
                .addParameter(Keys.server.key.USER_ID, user_id)
                .addParameter(Keys.server.key.INCLUDE, "info")
                .execute();

        return apiRequest;
    }

    public APIRequest removeParticipant(Context context, int id, int user_id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatUserModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatUserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleChatUser(Url.getRemoveMember(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RemoveUserResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CHAT_ID, id)
                .addParameter(Keys.server.key.USER_ID, user_id)
                .addParameter(Keys.server.key.INCLUDE, "info")
                .execute();

        return apiRequest;
    }

    public APIRequest addParticipant(Context context, int id, String user_id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatUserModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatUserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleChatUser(Url.getAddParticipant(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new SingleUserResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CHAT_ID, id)
                .addParameter(Keys.server.key.USER_ID, user_id)
                .addParameter(Keys.server.key.INCLUDE, "info")
                .execute();

        return apiRequest;
    }

    public APIRequest promoteParticipant(Context context, int id, int user_id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatUserModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatUserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleChatUser(Url.getPromoteModerator(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PromoteUserResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CHAT_ID, id)
                .addParameter(Keys.server.key.USER_ID, user_id)
                .addParameter(Keys.server.key.INCLUDE, "info")
                .execute();

        return apiRequest;
    }


    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ChatModel>> requestSingleModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ChatModel>> requestChatIcon(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ChatModel>> requestCollectionModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ChatThreadModel>> requestChatThread(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ChatThreadModel>> requestSendMessage(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ChatThreadModel>> requestRemoveMessage(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ChatUserModel>> requestSingleChatUser(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ChatUserModel>> requestCollectionChatUser(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class CreateChatResponse extends APIResponse<SingleTransformer<ChatModel>> {
        public CreateChatResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UpdateChatResponse extends APIResponse<SingleTransformer<ChatModel>> {
        public UpdateChatResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ChatDetailResponse extends APIResponse<SingleTransformer<ChatModel>> {
        public ChatDetailResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class MyChatResponse extends APIResponse<CollectionTransformer<ChatModel>> {
        public MyChatResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class MyMessageResponse extends APIResponse<CollectionTransformer<ChatModel>> {
        public MyMessageResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ChatIconResponse extends APIResponse<SingleTransformer<ChatModel>> {
        public ChatIconResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ChatJoinResponse extends APIResponse<SingleTransformer<ChatThreadModel>> {
        public ChatJoinResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class SendMessageResponse extends APIResponse<SingleTransformer<ChatThreadModel>> {
        public SendMessageResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class SendNewMessageResponse extends APIResponse<SingleTransformer<ChatThreadModel>> {
        public SendNewMessageResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class SendFileResponse extends APIResponse<SingleTransformer<ChatThreadModel>> {
        public SendFileResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class SendNewFileResponse extends APIResponse<SingleTransformer<ChatThreadModel>> {
        public SendNewFileResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RemoveMessageResponse extends APIResponse<SingleTransformer<ChatThreadModel>> {
        public RemoveMessageResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ChatThreadResponse extends APIResponse<CollectionTransformer<ChatThreadModel>> {
        public ChatThreadResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class CollectionModeratorResponse extends APIResponse<CollectionTransformer<ChatUserModel>> {
        public CollectionModeratorResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class CollectionMemberResponse extends APIResponse<CollectionTransformer<ChatUserModel>> {
        public CollectionMemberResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class SingleUserResponse extends APIResponse<SingleTransformer<ChatUserModel>> {
        public SingleUserResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class PromoteUserResponse extends APIResponse<SingleTransformer<ChatUserModel>> {
        public PromoteUserResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class DemoteUserResponse extends APIResponse<SingleTransformer<ChatUserModel>> {
        public DemoteUserResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RemoveUserResponse extends APIResponse<SingleTransformer<ChatUserModel>> {
        public RemoveUserResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class LeaveResponse extends APIResponse<SingleTransformer<ChatUserModel>> {
        public LeaveResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }


}

