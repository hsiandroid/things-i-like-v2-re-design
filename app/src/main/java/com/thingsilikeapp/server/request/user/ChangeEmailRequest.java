package com.thingsilikeapp.server.request.user;

import android.content.Context;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.ChangeEmailItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class ChangeEmailRequest  {


//    public ChangeEmailRequest(Context context) {
//        super(context);
//    }

    public static ChangeEmailRequest getDefault(){
        return new ChangeEmailRequest();
    }


    public APIRequest changeEmail(Context context, String email) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.changeEmail(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ChangeEmailResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.EMAIL,  email)
                .addParameter(Keys.server.key.INCLUDE, "info,social,statistics,kyc")
                .showDefaultProgressDialog("Changing...")
                .execute();

        return apiRequest;
    }



    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


    }

    public class ChangeEmailResponse extends APIResponse<SingleTransformer<UserItem>> {
        public ChangeEmailResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }



}

