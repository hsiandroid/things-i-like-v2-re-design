package com.thingsilikeapp.server.request.wishlist;

import android.content.Context;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransactionTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class WishListRejectGiftPermissionRequest extends BaseRequest<WishListInfoTransactionTransformer> {

    public WishListRejectGiftPermissionRequest(Context context){
        super(context);
    }

    @Override
    public Call<WishListInfoTransactionTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<WishListInfoTransactionTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.wishlist.DECLINE)
        Call<WishListInfoTransactionTransformer> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<WishListInfoTransactionTransformer> response) {
            super(response);
        }
    }
}
