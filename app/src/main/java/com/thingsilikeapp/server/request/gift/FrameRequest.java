package com.thingsilikeapp.server.request.gift;

import android.content.Context;

import com.thingsilikeapp.data.model.FrameItem;
import com.thingsilikeapp.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.config.Keys;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class FrameRequest extends BaseRequest<CollectionTransformer<FrameItem>> {

    public FrameRequest(Context context){
        super(context);
    }

    @Override
    public Call<CollectionTransformer<FrameItem>> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<CollectionTransformer<FrameItem>> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Keys.server.route.photo.FRAMES)
        Call<CollectionTransformer<FrameItem>> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<CollectionTransformer<FrameItem>> response) {
            super(response);
        }
    }
}
