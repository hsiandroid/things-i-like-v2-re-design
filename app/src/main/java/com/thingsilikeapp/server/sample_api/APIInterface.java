package com.thingsilikeapp.server.sample_api;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ANDROID-PC on 2/6/2018.
 */

public interface APIInterface {

    @POST("{p}/{id}")
    Call<String> getFollowModel(@Header("Authorization") String authorization, @Path(value = "p", encoded = true) String p, @Query("_output") String output);


}
