package com.thingsilikeapp.server.sample_api;

import com.thingsilikeapp.data.preference.UserData;

import retrofit2.Call;
import retrofit2.Response;

public class SampleRequest {

    public APIInterface apiInterface;
    public Callback callback;
    public String authorization = "";
    public Call<String> call;

    public String getAuthorization(){
        return UserData.getString(UserData.AUTHORIZATION);
    }

    public void dismiss(){
        call.cancel();
    }

    public void addAuthorization(){
        authorization = UserData.getString(UserData.AUTHORIZATION);
    }

    public static SampleRequest getDefault(){
        return new SampleRequest();
    }


    public void sampleAPI(final Callback callback){
        call = apiInterface.getFollowModel(authorization, "api/auth/login.json",  "json");

        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
//                dd("TAG",response.code() + "");
                if (response.code() == 200){
                    if (callback != null){
                        callback.onSuccess(response.body());
                    }
                }else {
//                    ToastMessage.show(getActivity(), "Something went wrong! Please try again!", ToastMessage.Status.FAILED);
                    if (callback != null){
                        callback.onFailed(call);
                    }
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
//                ToastMessage.show(getActivity(), "Something went wrong! Please try again!", ToastMessage.Status.FAILED);
                call.cancel();
            }
        });
    }

    public interface Callback{
        void onSuccess(String value);
        void onFailed(Call<String> call);
    }
}
