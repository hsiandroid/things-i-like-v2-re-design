package com.thingsilikeapp.server;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.ReportPostItem;
import com.thingsilikeapp.data.model.ReportPostItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class ReportPost {

    public static ReportPost getDefault(){
        return new ReportPost();
    }


    public APIRequest getReportPost(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ReportPostItem>>(context) {
            @Override
            public Call<CollectionTransformer<ReportPostItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformer(Url.getReportPost(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllOrderResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "wishlist.image")
                .showSwipeRefreshLayout(true)
                .setSwipeRefreshLayout(swipeRefreshLayout);

        return apiRequest;
    }

    public APIRequest getUnReportPost(Context context, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ReportPostItem>>(context) {
            @Override
            public Call<SingleTransformer<ReportPostItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleUnReport(Url.getUnReportPost(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UnreportPostResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.TYPE, "wishlist")
                .addParameter(Keys.server.key.REFERENCE_ID, id)
                .execute();

        return apiRequest;
    }

    public interface RequestService {
//        @Multipart
//        @POST("{p}")
//        Call<SingleTransformer<OrderItem>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ReportPostItem>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ReportPostItem>> requestSingleUnReport(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        }

    public class AllOrderResponse extends APIResponse<CollectionTransformer<ReportPostItem>> {
        public AllOrderResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UnreportPostResponse extends APIResponse<SingleTransformer<ReportPostItem>> {
        public UnreportPostResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}

