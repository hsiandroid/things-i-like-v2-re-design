package com.thingsilikeapp.server;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.data.model.DiscoverItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.BaseTransformer;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class KYC {

    public static KYC getDefault(){
        return new KYC();
    }

    public APIRequest kyc(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleUserModel(Url.getPersonalKYC(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new KYCResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.INCLUDE, "kyc")
                .showDefaultProgressDialog("Checking...")
                .execute();

        return apiRequest;
    }


    public APIRequest email(Context context, String email) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleUserModel(Url.getEmailKYC(), getAuthorization(), getMultipartBody());
            }


            @Override
            public void onResponse() {
                EventBus.getDefault().post(new EmailResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.EMAIL, email)
                .showDefaultProgressDialog("Verifying...")
                .execute();

        return apiRequest;
    }

    public void discover(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<DiscoverItem>>(context) {
            @Override
            public Call<SingleTransformer<DiscoverItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleDiscover(Url.getDiscover(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new DiscoverResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();

    }

    public APIRequest phone(Context context, String contact, String code, String iso) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getPhoneKYC(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PhoneResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CONTACT_NUMBER, contact)
                .addParameter(Keys.server.key.COUNTRY_CODE, code)
                .addParameter(Keys.server.key.COUNTRY_ISO, iso)
                .showDefaultProgressDialog("Verifying...")
                .execute();

        return apiRequest;
    }

    public APIRequest phoneOTP(Context context, String contact, String code, String iso, String otp) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getValidatePhoneKYC(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PhoneResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.CONTACT_NUMBER, contact)
                .addParameter(Keys.server.key.COUNTRY_CODE, code)
                .addParameter(Keys.server.key.COUNTRY_ISO, iso)
                .addParameter(Keys.server.key.VERIFICATION_CODE, otp)
                .showDefaultProgressDialog("Verifying...")
                .execute();

        return apiRequest;
    }

    public APIRequest identity(Context context, String type, File file) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getIdKYC(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new IdResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.TYPE,type)
                .addParameter(Keys.server.key.FILE,file)
                .showDefaultProgressDialog("Verifying...")
                .execute();

        return apiRequest;
    }

    public APIRequest selfie(Context context, File file) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getSelfieKYC(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new SelfieResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.FILE,file)
                .showDefaultProgressDialog("Verifying...")
                .execute();

        return apiRequest;
    }

    public APIRequest address(Context context, File file) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getAddressKYC(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AddressResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.server.key.FILE,file)
                .showDefaultProgressDialog("Verifying...")
                .execute();

        return apiRequest;
    }


    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestSingleModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestSingleUserModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<DiscoverItem>> requestSingleDiscover(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<UserItem>> requestCollectionModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class EmailResponse extends APIResponse<SingleTransformer<UserItem>> {
        public EmailResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class SelfieResponse extends APIResponse<SingleTransformer<UserItem>> {
        public SelfieResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class IdResponse extends APIResponse<SingleTransformer<UserItem>> {
        public IdResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class AddressResponse extends APIResponse<SingleTransformer<UserItem>> {
        public AddressResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class PhoneResponse extends APIResponse<SingleTransformer<UserItem>> {
        public PhoneResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class KYCResponse extends APIResponse<SingleTransformer<UserItem>> {
        public KYCResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class DiscoverResponse extends APIResponse<SingleTransformer<DiscoverItem>> {
        public DiscoverResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}

