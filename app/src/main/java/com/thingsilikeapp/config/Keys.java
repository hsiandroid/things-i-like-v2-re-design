package com.thingsilikeapp.config;

/**
 * Created by BCTI 3 on 12/8/2016.
 */

public class Keys {
    public static final String API = "\\api";
    public static final String FORMAT = ".json";

    public static String DEVICE_ID = "device_id";
    public static String DEVICE_NAME = "device_name";
    public static String DEVICE_MODEL = "device_model";
    public static String DEVICE_IMEI = "device_imei";
    public static String OS_VERSION= "os_version";
    public static String DEVICE_REG_ID = "device_reg_id";
    public static String PAGE = "page";
    public static String PER_PAGE = "per_page";

    public static final String APP_SETTINGS = "\\app-settings";
    public static final String AUTH = "\\auth";
    public static final String MOMENT = "\\moment";
    public static final String REPORT = "\\report";
    public static final String NOTIFICATIONS = "\\notifications";
    public static final String WISHLIST = "\\wishlist";
    public static final String SOCIAL = "\\social";
    public static final String COMMENT = "\\comment";
    public static final String ADDRESSBOOK = "\\addressbook";

    // social
    public static final String FEED = "\\feed";
    public static final String FEED_V2 = "\\feed-v2";
    public static final String RELATED = "\\related";
    public static final String INTERESTED_ITEM = "\\interested-item";
    public static final String RECENT_SEARCH = "\\memory-search";
    public static final String SUGGESTED_SEARCH = "\\suggested-search";
    public static final String SUGGESTIONS = "\\suggestions";
    public static final String GROUPED_FOLLOWING = "\\grouped-following";
    public static final String GROUP = "\\group";
    public static final String FOLLOW = "\\follow";
    public static final String UNFOLLOW = "\\unfollow";
    public static final String FOLLOWERS = "\\followers";
    public static final String FOLLOWING = "\\following";

    // profile
    public static final String PROFILE = "\\profile";
    public static final String REWARDS = "\\rewards";
    public static final String EARN = "\\earn";
    public static final String INVITE = "\\invite";
    public static final String WALKTHROUGH = "\\walkthrough";
    public static final String INFO = "\\info";
    public static final String CHANGE_AVATAR = "\\change-avatar";
    public static final String GROUPS = "\\groups";


    // reward
    public static final String HISTORY = "\\history";
    public static final String CATALOGUE = "\\catalogue";
    public static final String CATEGORY = "\\reward-categories";
    public static final String CATEGORIES = "\\category";
    public static final String REDEEM_ITEM = "\\redeem-item";
    public static final String CONVERT = "\\convert";

    // user
    public static final String USERS = "\\users";
    public static final String SEARCH = "\\search";
    public static final String SUGGESTED = "\\suggested";
    public static final String CHANGE_PASSWORD = "\\change-password";
    public static final String BIRTHDAY_CELEBRANTS = "\\birthday-celebrants";
    public static final String BDAY_REMINDER = "\\bday-reminder";
    public static final String TIMELINE = "\\timeline";
    public static final String PER_CATEGORY = "\\per-category";

    // helper
    public static final String HELPER = "\\helper";
    public static final String LINK = "\\link-preview";

    // birthday
    public static final String BIRTHDAY = "\\birthday";
    public static final String GREET = "\\greet";
    public static final String CELEBRATION = "\\celebration";
    public static final String GREETINGS = "\\greetings";
    public static final String RANDOM = "\\random";
    public static final String SAY_THANKS = "\\say-thanks";

    // notification
    public static final String UNREAD_COUNT = "\\unread-count";
    public static final String READ = "\\read";
    public static final String READ_ALL = "\\read-all";
    public static final String DELETE_ALL = "\\delete-all";


    // auth
    public static final String LOGIN = "\\login";
    public static final String VALIDATE_FIELD = "\\validate-field";
    public static final String LOGOUT = "\\logout";
    public static final String REGISTER = "\\register";
    public static final String FORGOT_PASSWORD = "\\forgot-password";
    public static final String RESET_PASSWORD = "\\reset-password";
    public static final String FB_LOGIN = "\\fb-login";
    public static final String REFRESH_TOKEN = "\\refresh-token";

    // wishlist
    public static final String PERMISSION = "\\permission";
    public static final String V2 = "\\v2";
    public static final String GRANT = "\\grant";
    public static final String CANCEL = "\\cancel";
    public static final String REVOKE = "\\deny";
    public static final String DECLINE = "\\decline";
    public static final String REQUEST = "\\request";
    public static final String OWNED = "\\owned";
    public static final String BLOCK = "\\block";
    public static final String EDIT_DELIVERY_INFO = "\\edit-delivery-info";
    public static final String BY_WISHLIST = "\\by-wishlist";

    public static final String CATEGORIESS = "\\categories";

    public static final String TRANSACTION = "\\transaction";
    public static final String SEND = "\\send";
    public static final String VALIDATE_BUY = "\\validate-buy";
    public static final String RECEIVE = "\\receive";
    public static final String RECEIVED = "\\received";
    public static final String SENT = "\\sent";
    public static final String PENDING = "\\pending";
    public static final String TRANSACTION_SHOW = "\\show";

    //explore
    public static final String EXPLORE = "\\explore";
    public static final String TRENDING_USER = "\\trending-user";
    public static final String TRENDING = "\\trending";
    public static final String TRENDING_CATEGORY = "\\trending-category";
    public static final String CATEGORYY = "\\trending";
    public static final String RECENTLY = "\\recently";
    public static final String RECOMMENDED = "\\recommended";
    public static final String HOT = "\\hot";
    public static final String MOSTRATED = "\\most-rated";

    public static final String GENERAL_REQUEST = "\\general-requests";
    public static final String COMPACT = "\\compact";
    public static final String GET_COUNTRY = "\\get-country";
    public static final String LIKE = "\\like";
    public static final String LIKES = "\\likes";
    public static final String REPOST = "\\reposts";

    public static final String WALLET = "\\wallet";
    public static final String LYKA_WALLET = "\\lyka-wallet";
    public static final String MARKETPLACE = "\\marketplace";
    public static final String PRINT = "\\print";

    //basic
    public static final String PHOTO = "\\photo";
    public static final String FRAMES = "\\frames";
    public static final String BACKGROUNDS = "\\backgrounds";

    //basic
    public static final String CREATE = "\\create";
    public static final String EDIT = "\\edit";
    public static final String SHOW = "\\show";
    public static final String INDEX = "\\index";
    public static final String DELETE = "\\delete";
    public static final String DEFAULT = "\\default";
    public static final String ALL = "\\all";

    //chat
    public static final String MESSENGER = "\\messenger";
    public static final String CHAT = "\\chat";
    public static final String MESSAGE = "\\message";
    public static final String NEW = "\\new";
    public static final String NEW_UPLOAD = "\\new_upload";
    public static final String UPLOAD = "\\upload";
    public static final String PRODUCT = "\\product";

    public static class server {

        public static class key {
            public static final String API_TOKEN = "api_token";
            public static final String DEVICE_ID = "device_id";
            public static final String DEVICE_NAME = "device_name";
            public static final String OS_VERSION = "os_version";
            public static final String DEVICE_IMEI = "device_imei";
            public static final String DEVICE_MODEL = "device_model";
            public static final String DEVICE_REG_ID = "device_reg_id";
            public static final String AUTH_ID = "auth_id";
            public static final String PAGE = "page";
            public static final String PER_PAGE = "per_page";
            public static final String USERNAME = "username";
            public static final String USER_ID = "user_id";
            public static final String EVENT_ID = "event_id";
            public static final String GROUP = "group";
            public static final String PASSWORD = "password";
            public static final String CONTENT = "content";
            public static final String TAGGED_USER_ID = "tagged_user_id";
            public static final String HOUSE_NO = "house_no";
            public static final String FILE = "file";
            public static final String FILES = "file[]";
            public static final String EMAIL = "email";
            public static final String GENDER = "gender";
            public static final String NAME = "name";
            public static final String PR0FESSION = "profession";
            public static final String CONTACT_NUMBER = "contact_number";
            public static final String PASSWORD_CONFIRMATION = "password_confirmation";
            public static final String NEW_PASSWORD = "new_password";
            public static final String VALIDATION_TOKEN = "validation_token";
            public static final String INCLUDE = "include";
            public static final String HISTORY_ID = "history_id";
            public static final String CATEGORY = "category";
            public static final String PARENT_ID = "parent_id";
            public static final String WISHLIST_OWNER_ID = "wishlist_owner_id";
            public static final String ADDRESS = "address";
            public static final String RECIPIENT = "recipient";
            public static final String ZIP_CODE = "zip_code";
            public static final String PROFESSION = "profession";
            public static final String MY_PRIVACY = "my_privacy";
            public static final String COUNTRY_NAME = "country_name";
            public static final String BIRTHDATE = "birthdate";
            public static final String TITLE = "title";
            public static final String TYPE = "type";
            public static final String PASSCODE = "passcode";
            public static final String NEW_PASSCODE = "new_passcode";
            public static final String OLD_PASSCODE = "old_passcode";
            public static final String REFERENCE_ID = "reference_id";
            public static final String TEMP_ID = "temp_id";
            public static final String WISHLIST_ID = "wishlist_id";
            public static final String GREETINGS_ID = "greetings_id";
            public static final String VIEWER_ID = "viewer_id";
            public static final String ID = "viewer_id";
            public static final String WISHLIST_TRANSACTION_ID = "wishlist_transaction_id";
            public static final String DEDICATION_MESSAGE = "dedication_message";
            public static final String APPRECIATION_MESSAGE = "appreciation_message";
            public static final String URL = "url";
            public static final String KEYWORD = "keyword";
            public static final String TRANSACTION_ID = "transaction_id";
            public static final String CURRENT_PASSWORD = "current_password";
            public static final String NEW_PASSWORD_CONFIRMATION = "new_password_confirmation";
            public static final String FB_ID = "fb_id";
            public static final String URL_TITLE = "url_title";
            public static final String URL_DESCRIPTION = "url_description";
            public static final String URL_IMAGE = "url_image";
            public static final String ACCESS_TOKEN = "access_token";
            public static final String NOTIFICATION_ID = "notification_id";
            public static final String SELECTED_ID = "selected_id";
            public static final String WISHLIST_COMMENT_ID = "wishlist_comment_id";
            public static final String VIEW_ALL = "view_all";
            public static final String BDAY_REMINDER_ID = "bday_reminder_id";

            public static final String ADDRESS_ID = "address_id";
            public static final String ADDRESS_LABEL = "address_label";
            public static final String RECEIVER_NAME = "receiver_name";
            public static final String STREET_ADDRESS = "street_address";
            public static final String CITY = "city";
            public static final String STATE = "state";
            public static final String COUNTRY = "country";
            public static final String COUNTRY_CODE = "country_code";
            public static final String CONTACT_COUNTRY_CODE = "contact_country_code";
            public static final String CONTACT_COUNTRY_DIAL_CODE = "contact_country_dial_code";
            public static final String PHONE_NUMBER = "phone_number";
            public static final String PHONE_COUNTRY_DIAL_CODE = "phone_country_dial_code";
            public static final String PHONE_COUNTRY_CODE = "phone_country_code";
            public static final String CATALOGUE_ID = "catalogue_id";
            public static final String REWARD_ID = "reward_id";
            public static final String AMOUNT = "amount";

            //chat
            public static final String PARTICIPANTS = "participants";

            public static final String FIELD = "field";
            public static final String VALUE = "value";
            public static final String CHAT_ID = "chat_id";
            public static final String MSG_ID = "msg_id";

            //lyka wallet
            public static final String BANK_NAME = "bank_name";
            public static final String BANK_ADDRESS = "bank_address";
            public static final String BANK_SWIFT_CODE = "bank_swift_code";
            public static final String ACCOUNT_NUMBER = "account_number";
            public static final String ACCOUNT_NAME = "account_name";
            public static final String ACCOUNT_ADDRESS = "account_address";
            public static final String COUNTRY_ISO = "country_iso";
            public static final String IS_PRIMARY = "is_primary";
            public static final String SETTLEMENT_ID = "settlement_id";
            public static final String GEM_QTY = "gem_qty";
            public static final String CURRENCY = "currency";
            public static final String VERIFICATION_CODE = "verification_code";
            public static final String OTP_VALUE = "otp_value";

            //lyka Card
            public static final String CARD_NUMBER = "card_number";
            public static final String CARD_EXPIRY_MONTH = "card_expiry_month";
            public static final String CARD_EXPIRY_YEAR = "card_expiry_year";
            public static final String CARD_TYPE = "card_type";
            public static final String CARD_ID = "card_id";
            public static final String COUNTER = "counter";
            public static final String CATEGORY_ID = "category_id";

            //order
            public static final String PRODUCT_ID = "product_id";
            public static final String ADDRESS_BOOK_ID = "addressbook_id";
            public static final String NOTE = "note";
            public static final String ORDER_ID = "order_id";

        }

        public static class route {

            public static final String PRIVACY_POLICY = "http://privacypolicy.technology/kpp.php";
            public static final String APP_SETTINGS = API + Keys.APP_SETTINGS + FORMAT;

            public static class auth {

                public static final String LOGIN = API + AUTH + Keys.LOGIN + FORMAT;
                public static final String VALIDATE = API + AUTH + Keys.VALIDATE_FIELD + FORMAT;
                public static final String LOGOUT = API + AUTH + Keys.LOGOUT + FORMAT;
                public static final String REGISTER = API + AUTH + Keys.REGISTER + FORMAT;
                public static final String FORGOT_PASSWORD = API + AUTH + Keys.FORGOT_PASSWORD + FORMAT;
                public static final String RESET_PASSWORD = API + AUTH + Keys.RESET_PASSWORD + FORMAT;
                public static final String FB_LOGIN = API + AUTH + Keys.FB_LOGIN + FORMAT;
                public static final String REFRESH_TOKEN = API + AUTH + Keys.REFRESH_TOKEN + FORMAT;
            }

            public static class wishlist {
                public static final String RELATED = API + V2 + WISHLIST + Keys.RELATED + FORMAT;
                public static final String INTERESTED_ITEM = API + V2 + WISHLIST + Keys.INTERESTED_ITEM + FORMAT;
                public static final String CREATE = API + V2 + WISHLIST + Keys.CREATE + FORMAT;
                public static final String EDIT = API + V2 + WISHLIST + Keys.EDIT + FORMAT;
                public static final String SHOW = API + V2 + WISHLIST + Keys.SHOW + FORMAT;
                public static final String OWNED = API + V2 + WISHLIST + Keys.OWNED + FORMAT;
                public static final String BLOCK = API + V2 + WISHLIST + Keys.BLOCK + FORMAT;
                public static final String EDIT_DELIVERY_INFO = API + V2 + WISHLIST + Keys.EDIT_DELIVERY_INFO + FORMAT;
                public static final String GRANT = API + V2 + WISHLIST + PERMISSION + Keys.GRANT + FORMAT;
                public static final String CANCEL = API + V2 + WISHLIST + PERMISSION + Keys.CANCEL + FORMAT;
                public static final String REVOKE = API + V2 + WISHLIST + PERMISSION + Keys.REVOKE + FORMAT;
                public static final String REQUEST = API + V2 + WISHLIST + PERMISSION + Keys.REQUEST + FORMAT;
                public static final String PERMISSION_ALL = API + V2 + WISHLIST + PERMISSION + Keys.ALL + FORMAT;
                public static final String PERMISSION_OWNED = API + V2 + WISHLIST + PERMISSION + Keys.OWNED + FORMAT;
                public static final String PERMISSION_BY_WISHLIST = API + V2 + WISHLIST + PERMISSION + Keys.BY_WISHLIST + FORMAT;
                public static final String SEND = API + V2 + WISHLIST + TRANSACTION + Keys.SEND + FORMAT;
                public static final String RECEIVE = API + V2 + WISHLIST + TRANSACTION + Keys.RECEIVE + FORMAT;
                public static final String RECEIVED = API + V2 + WISHLIST + TRANSACTION + Keys.RECEIVED + FORMAT;
                public static final String DECLINE = API + V2 + WISHLIST + TRANSACTION + Keys.DECLINE + FORMAT;
                public static final String SENT = API + V2 + WISHLIST + TRANSACTION + Keys.SENT + FORMAT;
                public static final String PENDING = API + V2 + WISHLIST + TRANSACTION + Keys.PENDING + FORMAT;
                public static final String TRANSACTION_SHOW = API + V2 + WISHLIST + TRANSACTION + Keys.TRANSACTION_SHOW + FORMAT;
                public static final String CATEGORY_ALL = API + V2 + WISHLIST + CATEGORIESS + Keys.ALL + FORMAT;
                public static final String CATEGORY_OWNED = API + V2 + WISHLIST + CATEGORIESS + Keys.OWNED + FORMAT;
                public static final String DELETE = API + V2 + WISHLIST + Keys.DELETE + FORMAT;
                public static final String LIKE = API + V2 + WISHLIST + Keys.LIKE + FORMAT;
                public static final String LIKES = API + V2 + WISHLIST + Keys.LIKES + FORMAT;
                public static final String REPOST = API + V2 + WISHLIST + Keys.REPOST + FORMAT;
            }

            public static class moment {
                public static final String CREATE = API + MOMENT + Keys.CREATE + FORMAT;
            }

            public static class report {
                public static final String CREATE = API + REPORT + Keys.CREATE + FORMAT;
            }

            public static class comment {
                public static final String ALL = API + V2 + WISHLIST + COMMENT + Keys.ALL + FORMAT;
                public static final String CREATE = API + V2 + WISHLIST + COMMENT + Keys.CREATE + FORMAT;
                public static final String EDIT = API + V2 + WISHLIST + COMMENT + Keys.EDIT + FORMAT;
                public static final String DELETE = API + V2 + WISHLIST + COMMENT + Keys.DELETE + FORMAT;
            }

            public static class addressbook {
                public static final String ALL = API + ADDRESSBOOK + Keys.ALL + FORMAT;
                public static final String CREATE = API + ADDRESSBOOK + Keys.CREATE + FORMAT;
                public static final String EDIT = API + ADDRESSBOOK + Keys.EDIT + FORMAT;
                public static final String DELETE = API + ADDRESSBOOK + Keys.DELETE + FORMAT;
                public static final String DEFAULT = API + ADDRESSBOOK + Keys.DEFAULT + FORMAT;
            }

            public static class general {
                public static final String ALL = API + Keys.GENERAL_REQUEST + Keys.ALL + FORMAT;
                public static final String COMPACT = API + Keys.GENERAL_REQUEST + Keys.COMPACT + FORMAT;
                public static final String GET_COUNTRY = API + Keys.GENERAL_REQUEST + Keys.GET_COUNTRY + FORMAT;
            }

            public static class notifications {
                public static final String ALL = API + NOTIFICATIONS + Keys.ALL + FORMAT;
                public static final String UNREAD_COUNT = API + NOTIFICATIONS + Keys.UNREAD_COUNT + FORMAT;
                public static final String READ = API + NOTIFICATIONS + Keys.READ + FORMAT;
                public static final String READ_ALL = API + NOTIFICATIONS + Keys.READ_ALL + FORMAT;
                public static final String DELETE = API + NOTIFICATIONS + Keys.DELETE + FORMAT;
                public static final String DELETE_ALL = API + NOTIFICATIONS + Keys.DELETE_ALL + FORMAT;
            }

            public static class profile {
                public static final String INFO = API + PROFILE + Keys.INFO + FORMAT;
                public static final String EDIT = API + V2 + PROFILE + Keys.EDIT + FORMAT;
                public static final String GROUPS = API + V2 + PROFILE + Keys.GROUPS + FORMAT;
                public static final String CHANGE_AVATAR = API + PROFILE + Keys.CHANGE_AVATAR + FORMAT;
                public static final String CHANGE_PASSWORD = API + PROFILE + Keys.CHANGE_PASSWORD + FORMAT;
                public static final String BDAY_REMINDER = API + V2 + PROFILE + Keys.BDAY_REMINDER + FORMAT;
                public static final String TIMELINE = API + V2 + PROFILE + Keys.TIMELINE + FORMAT;
                public static final String INVITE = API + Keys.REWARDS + Keys.EARN + Keys.INVITE + FORMAT;
                public static final String WALKTHROUGH = API + Keys.REWARDS + Keys.EARN + Keys.WALKTHROUGH + FORMAT;

            }

            public static class users {
                public static final String SHOW = API + USERS + Keys.SHOW + FORMAT;
                public static final String INFO = API + V2 + USERS + Keys.INFO + FORMAT;
                public static final String SEARCH = API + USERS + Keys.SEARCH + FORMAT;
                public static final String SUGGESTED = API + V2 + USERS + Keys.SUGGESTED + FORMAT;
                public static final String TIMELINE = API + V2 + USERS + Keys.TIMELINE + FORMAT;

                public static class birthday_celebrants {
                    public static final String ALL = API + V2 + USERS + BIRTHDAY_CELEBRANTS + Keys.ALL + FORMAT;
                }
            }

            public static class helper {
                public static final String LINK = API + HELPER + Keys.LINK + FORMAT;
            }

            public static class social {
                public static final String FEED1 = API + SOCIAL + Keys.FEED + FORMAT;
                public static final String FEED_V2 = API + V2 + SOCIAL + Keys.FEED_V2 + FORMAT;
                public static final String RECENT_SEARCH = API + SOCIAL + Keys.RECENT_SEARCH + FORMAT;
                public static final String SUGGESTED_SEARCH = API + SOCIAL + Keys.SUGGESTED_SEARCH + FORMAT;
                public static final String SUGGESTIONS = API + V2 + SOCIAL + Keys.SUGGESTIONS + FORMAT;
                public static final String GROUPED_FOLLOWING = API + V2 + SOCIAL + Keys.GROUPED_FOLLOWING + FORMAT;
                public static final String FOLLOW = API + SOCIAL + Keys.FOLLOW + FORMAT;
                public static final String UNFOLLOW = API + SOCIAL + Keys.UNFOLLOW + FORMAT;
                public static final String FOLLOWERS = API + V2 + SOCIAL + Keys.FOLLOWERS + FORMAT;
                public static final String FOLLOWING = API + V2 + SOCIAL + Keys.FOLLOWING + FORMAT;
                public static final String UPDATE_GROUP = API + V2 + SOCIAL + Keys.FOLLOWING + Keys.GROUP + FORMAT;

            }

            public static class birthday {
                public static final String GREET = API + V2 + BIRTHDAY + Keys.GREET + FORMAT;
                public static final String SHOW = API + V2 + BIRTHDAY + Keys.SHOW + FORMAT;
                public static final String CELEBRATION = API + V2 + BIRTHDAY + Keys.CELEBRATION + FORMAT;
                public static final String GREETINGS = API + V2 + BIRTHDAY + Keys.GREETINGS + FORMAT;
                public static final String RANDOM = API + V2 + BIRTHDAY + Keys.RANDOM + FORMAT;
                public static final String SAY_THANKS = API + V2 + BIRTHDAY + Keys.SAY_THANKS + FORMAT;
            }

            public static class photo {
                public static final String FRAMES = API + V2 + PHOTO + Keys.FRAMES + FORMAT;
                public static final String BACKGROUNDS = API + V2 + PHOTO + Keys.BACKGROUNDS + FORMAT;
            }

            public static class rewards {
                public static final String HISTORY = API + LYKA_WALLET + Keys.HISTORY + FORMAT;
                public static final String CATALOGUE = API + REWARDS + Keys.CATALOGUE + FORMAT;
                public static final String CATEGORY = API + Keys.CATEGORY + Keys.ALL + FORMAT;
                public static final String SHOW = API + REWARDS + Keys.SHOW + FORMAT;
                public static final String MY_SHOW = API + PROFILE + REWARDS + Keys.SHOW + FORMAT;
                public static final String CONVERT = API + PROFILE + REWARDS + Keys.CONVERT + FORMAT;
                public static final String REDEEM_ITEM = API + REWARDS + Keys.REDEEM_ITEM + FORMAT;
                public static final String INDEX = API + PROFILE + REWARDS + Keys.INDEX + FORMAT;

            }

            public static class explore{
                public static final String TRENDING_USER = API + V2 + EXPLORE + Keys.TRENDING_USER + FORMAT;
                public static final String LIKEE = API + V2 + EXPLORE + Keys.CATEGORYY + FORMAT;
                public static final String RECENTLY = API + V2 + EXPLORE + Keys.RECENTLY + FORMAT;
                public static final String RECOMMENDED = API + V2 + EXPLORE + Keys.RECOMMENDED + FORMAT;
                public static final String HOT = API + V2 + EXPLORE + Keys.HOT + FORMAT;
                public static final String MOSTRATED = API + V2 + EXPLORE + Keys.MOSTRATED + FORMAT;
                public static final String TRENDING_CATEGORY = API + V2 + EXPLORE + Keys.TRENDING_CATEGORY + FORMAT;
                public static final String TRENDING = API + V2 + EXPLORE + Keys.TRENDING + FORMAT;
            }

            public static class wallet {
                public static final String SEND = API  + LYKA_WALLET + Keys.SEND + FORMAT;
                public static final String VALIDATE_BUY = API + LYKA_WALLET + Keys.VALIDATE_BUY + FORMAT;
                public static final String PRODUCT = API + LYKA_WALLET + MARKETPLACE + Keys.PRODUCT + FORMAT;
                public static final String CATEGORY = API + LYKA_WALLET + MARKETPLACE + Keys.CATEGORIES + FORMAT;
            }

            public static class chat {
                public static final String NEW_CHAT = API + MESSENGER + Keys.CHAT + Keys.MESSAGE +  Keys.NEW + FORMAT; //no chat id
                public static final String NEW_UPLOAD = API + MESSENGER + Keys.CHAT + Keys.MESSAGE + Keys.NEW_UPLOAD + FORMAT; //no chat id
                public static final String CREATE = API + MESSENGER + Keys.CHAT + Keys.MESSAGE + Keys.CREATE + FORMAT;
                public static final String UPLOAD = API + MESSENGER + Keys.CHAT + Keys.MESSAGE + Keys.UPLOAD + FORMAT;
                public static final String DELETE = API + MESSENGER + Keys.CHAT + Keys.MESSAGE + Keys.DELETE + FORMAT;
                public static final String ALL = API + MESSENGER + Keys.MESSAGE + Keys.ALL + FORMAT;
            }
        }
    }
}
