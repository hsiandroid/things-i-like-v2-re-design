package com.thingsilikeapp.config;

/**
 * Created by BCTI 3 on 9/19/2017.
 */

public class App {

    public static final boolean debug = true;

    public static final boolean prod = false;

    public static final boolean production = true;

//    public static final String SHARE_MESSAGE = "The blue cat is out! Discover what everyone's talking about. Download THINGS I LIKE App now and never receive or send the wrong gift again! https://www.thingsilikeapp.com/invite/android";
//    public static final String SHARE_MESSAGE = "The blue cat is out! Discover what everyone's talking about. Download THINGS I LIKE App now and never receive or send the wrong gift again! https://nx8vj.app.goo.gl/jfVL";
//    public static final String SHARE_MESSAGE = "I just joined this new app called LYKA. It's not Facebook, it's not Instagram. It's a whole new level!" +
//        " You can earn LYKA Gems just by posting, liking, chatting, gaining followers, and more." +
//        " You can use these LYKA Gems to buy anything on the app or to pay for your bills when you eat out. Download it now and give me a follow! \n https://nx8vj.app.goo.gl/jfVL";

    public static final String SHARE_MESSAGE = "I just joined this new app called LYKA. It's not Facebook, it's not Instagram. It's a whole new level!" +
            " You can earn LYKA Gems just by posting, liking, chatting, gaining followers, and more." +
            " You can use these LYKA Gems to buy anything on the app or to pay for your bills when you eat out. Download it now and give me a follow! \n www.mylyka.com/share/u/{username}";


    //    public static final String SHARE_PROFILE = "Follow me on Things I Like - http://thingsilikeapp.com/share/u/{username}";
    public static final String SHARE_PROFILE = "I just joined this new app called LYKA. It's not Facebook, it's not Instagram. It's a whole new level!" +
        " You can earn LYKA Gems just by posting, liking, chatting, gaining followers, and more." +
        " You can use these LYKA Gems to buy anything on the app or to pay for your bills when you eat out. Download it now and give me a follow! \n www.mylyka.com/share/u/{username}";


    public static final String SHARE_QR = "Hi! This is {name}. You can send me LYKA Gems by typing my username: {username} in your LYKA wallet. Thank you.";

}
