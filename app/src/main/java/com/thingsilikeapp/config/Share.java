package com.thingsilikeapp.config;

public class Share {

    public static boolean production = true;

    public static final String demoURL = "http://www.mylyka.com";
//    public static final String demoURL = "http://demo.thingsilikeapp.com";
    public static final String liveURL = "http://www.mylyka.com";
//    public static final String liveURL = "http://demo.thingsilikeapp.com";

    public static final String URL = production ? liveURL : demoURL;
}