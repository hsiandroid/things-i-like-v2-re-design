package com.thingsilikeapp.config;


import com.thingsilikeapp.vendor.android.base.security.SecurityLayer;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Url extends SecurityLayer {
    public static final String PRODUCTION_URL = decrypt("http://www.mylyka.com");
//    public static final String PRODUCTION_URL = decrypt("http://demo.thingsilikeapp.com");
//    public static final String PRODUCTION_URL = decrypt("https://mentormetesting.azurewebsites.net");
//    public static final String DEBUG_URL = decrypt("http://www.mylyka.com");
    public static final String DEBUG_URL = decrypt("http://demo.thingsilikeapp.com");

    public static final String APP = App.production ? PRODUCTION_URL : DEBUG_URL;

    //Chat
    public static final String getNewChat(){return "/api/messenger/chat/message/new.json";} //no id

    public static final String getNewUpload(){return "/api/messenger/chat/message/new-upload.json";} //no id

    public static final String getCreate(){return "/api/messenger/chat/create.json";}

    public static final String getMyChat(){return "/api/messenger/chat/all.json";}

    public static final String getMyMessages(){return "/api/messenger/chat/messages.json";}

    public static final String getName(){return "/api/messenger/chat/name.json";}

    public static final String getSendMessage(){return "/api/messenger/chat/message/create.json";}

    public static final String getSendFile(){return "/api/messenger/chat/message/upload.json";}

    public static final String getChatThread(){return "/api/messenger/chat/message/all.json";}

    public static final String getRemoveMessage(){return "/api/messenger/chat/message/delete.json";}

    public static final String getJoinGroup(){return "/api/messenger/chat/join.json";}

    public static final String getUpdateChatName(){return "/api/messenger/chat/name.json";}

    public static final String getChatIcon(){return "/api/messenger/chat/icon.json";}

    public static final String getChatDeatil(){return "/api/messenger/chat/show.json";}

    public static final String getRemoveMember(){return "/api/messenger/chat/participant/delete.json";}

    public static final String getAddParticipant(){return "/api/messenger/chat/participant/create.json";}

    public static final String getAllModerators(){return "/api/messenger/chat/participant/moderator.json";}

    public static final String getAllMembers(){return "/api/messenger/chat/participant/member.json";}

    public static final String getAllParticipants(){return "/api/messenger/chat/participant/all.json";}

    public static final String getPromoteModerator(){return "/api/messenger/chat/participant/promote.json";}

    public static final String getDemoteModerator(){return "/api/messenger/chat/participant/demote.json";}

    public static final String getLeaveGroup(){return "/api/messenger/chat/leave.json";}


    //lyka wallet
    public static final String getAddBank(){return "/api/lyka-wallet/settlement/create.json";}

    public static final String getAllBank(){return "/api/lyka-wallet/settlement/all.json";}

    public static final String getEditBank(){return "/api/lyka-wallet/settlement/edit.json";}

    public static final String getRemoveBank(){return "/api/lyka-wallet/settlement/delete.json";}

    public static final String getValidate(){return "/api/lyka-wallet/encashment/validate.json";}

    public static final String getRequestWithdrawal(){return "/api/lyka-wallet/encashment/request.json";}

    public static final String getCancelWithdrawal(){return "/api/lyka-wallet/encashment/cancel.json";}

    public static final String getPendingWithdrawal(){return "/api/lyka-wallet/encashment/pending.json";}

    public static final String getRate(){return "/api/general-requests/get-rate.json";}

    public static final String getCurrentRate(){return "/api/general-requests/get-current-rate.json";}

    public static final String getHistory(){return "/api/lyka-wallet/encashment/all.json";}

    public static final String getWallet(){return "/api/settings/wallet.json";}


    //lyka verification
    public static final String getPersonalKYC(){return "/api/profile/info.json";}

    public static final String getEmailKYC(){return "/api/lyka-wallet/kyc/email.json";}

    public static final String getSelfieKYC(){return "/api/lyka-wallet/kyc/selfie.json";}

    public static final String getIdKYC(){return "/api/lyka-wallet/kyc/id.json";}

    public static final String getAddressKYC(){return "/api/lyka-wallet/kyc/address.json";}

//    public static final String getPhoneKYC(){return "/api/lyka-wallet/kyc/phone.json";}
    public static final String getPhoneKYC(){return "/api/v2/profile/change-phone.json";}

    public static final String getValidatePhoneKYC(){return "/api/lyka-wallet/kyc/validate-phone.json";}

    //lyka Card
    public static final String getAddCard(){return "/api/lyka-wallet/card/create.json";}

    public static final String getRemoveCard(){return "/api/lyka-wallet/card/delete.json";}

    public static final String getAllCard(){return "/api/lyka-wallet/card/all.json";}

    public static final String getBuyCard(){return "/api/lyka-wallet/buy-card.json";}

    //passcode
    public static final String getSetPasscode(){return "/api/lyka-wallet/set-passcode.json";}

    public static final String getValidatePasscode(){return "/api/lyka-wallet/validate-passcode.json";}

    public static final String getUpdatePasscode(){return "/api/lyka-wallet/update-passcode.json";}

    public static final String getLockSend(){return "/api/lyka-wallet/lock-send.json";}

    public static final String getUnlockSend(){return "/api/lyka-wallet/unlock-send.json";}

    public static final String getForgotPasscode(){return "/api/lyka-wallet/forgot-passcode.json";}


    //marketplace
    public static final String getCreateOrder(){return "/api/lyka-wallet/marketplace/order/create.json";}

    public static final String getMyOrder(){return "/api/lyka-wallet/marketplace/order/all.json";}

    public static final String getCancelOrder(){return "/api/lyka-wallet/marketplace/order/cancel.json";}

    public static final String getShowOrder(){return "/api/lyka-wallet/marketplace/order/show.json";}

    public static final String getEditOrder(){return "/api/lyka-wallet/marketplace/order/edit.json";}


    //discover
    public static final String getDiscover(){return "/api/settings/discover.json";}


    //settings
    public static final String blockUser(){return "/api/block/create.json";}

    public static final String getBlockUser(){return "/api/settings/blocked-users.json";}

    public static final String getUnBlockUser(){return "/api/block/delete.json";}

    public static final String getHidePost(){return "/api/settings/hidden-items.json";}

    public static final String getUnhidePost(){return "/api/block/delete.json";}

    public static final String getReportPost(){return "/api/settings/reported-items.json";}

    public static final String getUnReportPost(){return "/api/report/delete.json";}

    public static final String getReportUser(){return "/api/settings/reported-users.json";}

    public static final String getUnReportUser(){return "/api/report/delete.json";}

    public static final String changeContactNumber(){ return "/api/v2/profile/change-phone.json"; }

    public static final String getPhoneOtp(){ return "/api/v2/profile/request-otp.json"; }

    public static final String changeEmail(){ return "/api/v2/profile/change-email.json"; }


    //history
    public static final String getRewardHistory(){return "/api/lyka-wallet/history-detail.json";}

    //repost
    public static final String getRepost(){return "/api/v2/wishlist/repost.json";}

    //homeNotification
    public static final String getWalletNotification() {return "/api/settings/home.json";}

    //followers
    public static final String getFollowers() {return "/api/v2/social/followers.json";}


}
