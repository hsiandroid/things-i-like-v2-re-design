package com.thingsilikeapp.vendor.android.java.pusher;

import android.content.ContentResolver;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

import com.thingsilikeapp.data.preference.UserData;

/**
 * Created by BCTI 3 on 7/24/2017.
 */

public class Sound {
    private static Ringtone r;
    public static void playNotification(Context context){
        stopSound();

        if (UserData.getString(UserData.RINGTONE).equals("")){
            return;
        }
        if (!UserData.getBoolean(UserData.NOTIFICATION_ENABLE)){
            return;
        }

        try {
            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            switch (UserData.getString(UserData.RINGTONE)){
                case "default":
                    defaultSoundUri  = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    break;
                case "cat1":
                    defaultSoundUri  = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                            + "://" + context.getPackageName() + "/raw/cat1");
                    break;
                case "cat2":
                    defaultSoundUri  = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                            + "://" + context.getPackageName() + "/raw/cat2");
                    break;
            }

            playSound(defaultSoundUri,context);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void playSound(Uri uri,Context context){
        r = RingtoneManager.getRingtone(context, uri);
        r.play();
    }
    public static void stopSound() {
        if (r != null) {
            if (r.isPlaying()) {
                r.stop();
            }
        }
    }
}
