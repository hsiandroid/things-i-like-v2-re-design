package com.thingsilikeapp.vendor.android.java.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ValuePairAdapter extends BaseAdapter {
	private Context context;
	private List<ValuePair> data = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private int layout;
    private Typeface typeFace;

	public ValuePairAdapter(Context context, int layout) {
		this.context = context;
		this.layout = layout;
        layoutInflater = LayoutInflater.from(context);
	}

    public void addValuePair(String value, String title){
        data.add(new ValuePair(value,title));
    }

    public void addValuePairs(List<ValuePair> valuePairs){
        data = valuePairs;
    }

    public Object getValue(int position){
        return data.get(position).getValue();
    }

    public Object getTitle(int position){
        return data.get(position).getTitle();
    }

    public void setTypeFace(Typeface typeFace){
        this.typeFace = typeFace;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(layout, parent, false);
            holder = new ViewHolder(convertView);
            if(typeFace != null){
                holder.text1.setTypeface(typeFace);
            }
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.text1.setText(data.get(position).getTitle());
		return convertView;
	}


	public class ViewHolder{
        TextView text1;
        View view;
        public ViewHolder(View view){
            this.view =view;
            text1 = view.findViewById(android.R.id.text1);
        }
	}
} 
