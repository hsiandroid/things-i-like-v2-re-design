package com.thingsilikeapp.vendor.android.java;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.hardware.Camera;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.DiskLruCacheFactory;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.bumptech.glide.module.GlideModule;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.co.deanwild.materialshowcaseview.IShowcaseListener;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

/**
 * Created by BCTI 3 on 7/26/2017.
 */

public class StringFormatter {

    public static boolean isEmpty(String string) {
        if(string == null) return true;
        if(string.length() == 0) return true;
        return string.equals("null");
    }

    public static String first(String string) {
        if(string.length()>0){
            return string.substring(0,1).toUpperCase() + string.substring(1).toLowerCase();
        }
        return string;
    }

    public static String firstWord(String string) {
        if(string == null) return string;

        if(string.length()<0) return string;

        String [] words = string.split(" ");

        if(words.length < 0 ) return string;

        return words[0];
    }

    public static String titleCase(String string) {
        if(string == null) return string;

        if(string.length()<0) return string;

        StringBuilder sb = new StringBuilder(string.length());
        for(String word : string.split(" ")){
            sb.append(first(word));
            sb.append(" ");
        }
        return sb.toString().trim();
    }

    public static String replaceNull(String string) {
        return replaceNull(string, "");
    }

    public static String replaceNull(String string, String defaultValue) {
        if(string == null) return defaultValue;
        if(string.equals("null")) return defaultValue;
        if(string.equals("") && !defaultValue.equals("")) return defaultValue;
        return string;
    }

    public static String htmlize(String text) {
        return text.startsWith("<?") || text.startsWith("<html>") ? text
                : "<html><body>"
                + text.replace("&", "&amp;").replace("<", "&lt;")
                .replace(">", "&gt;").replace("\n", "<br>")
                + "</body></html>";
    }

    static Pattern spacePattern;

    public static List<String> explode(String s) {
        if (spacePattern == null) {
            spacePattern = Pattern.compile("\\s+");
        }
        return explode(s, spacePattern);
    }

    public static List<String> explode(String s, String sep) {
        StringTokenizer st = new StringTokenizer(s, sep);
        List<String> v = new ArrayList<String>();
        for (; st.hasMoreTokens();) {
            v.add(st.nextToken());
        }
        return v;
    }

    /*
     * public static final String implode(Object[] strings, String separator) {
     * return implode(Arrays.asList(strings), separator); }
     */
    public static String implode(double[] array, String separator) {
        StringBuffer out = new StringBuffer();
        boolean first = true;
        for (double v : array) {
            if (first)
                first = false;
            else
                out.append(separator);
            out.append(v);
        }
        return out.toString();
    }

    public static String implode(Object[] values) {
        return implode(values, ", ");
    }

    public static String implode(Object[] values, Object separator) {
        return implode(Arrays.asList(values), separator);
    }

    public static final <T> String implode(Iterable<T> elements,
                                           Object separator) {
        String sepStr = separator.toString();
        StringBuilder out = new StringBuilder();
        boolean first = true;
        for (Object s : elements) {
            if (s == null)
                continue;

            if (first)
                first = false;
            else
                out.append(sepStr);
            out.append(s);
        }
        return out.toString();
    }

    public static final String implode(Iterable<?> strings) {
        return implode(strings, ", ");
    }

    /*
     * public static final String implode(Collection<?> strings, String
     * separator) { int size = 0, n = strings.size(); for (Object s : strings)
     * if (s != null) size += s.toString().length();
     *
     * StringBuffer out = new StringBuffer(size + separator.length() * (n == 0 ?
     * 0 : n - 1)); boolean first = true; for (Object s : strings) { if (s ==
     * null) continue;
     *
     * if (first) first = false; else out.append(separator); out.append(s); }
     * return out.toString(); }
     */
    public static final List<String> explode(String string, Pattern separator) {
        int lastIndex = 0, len = string.length();

        Matcher matcher = separator.matcher(string);
        List<String> ret = new LinkedList<String>();

        while (matcher.find()) {
            String s = string.substring(lastIndex, matcher.start());
            if (s.length() > 0)
                ret.add(s);
            lastIndex = matcher.end();
        }
        String s = string.substring(lastIndex, len);
        if (s.length() > 0)
            ret.add(s);

        return ret;
    }

    public static String replace(String pattern, String replace, String s) {
        return concatWithSeparator(explode(s, pattern).toArray(new String[0]),
                replace);
    }

    public static final String concat(String[] a) {
        StringBuffer b = new StringBuffer();
        for (int i = 0; i < a.length; i++)
            b.append(a[i]);
        return b.toString();
    }

    public static final String concatln(String[] a) {
        StringBuffer b = new StringBuffer();
        int lenm = a.length - 1;
        for (int i = 0; i < lenm; i++) {
            b.append(a[i]);
            b.append("\n");
        }
        if (lenm != -1)
            b.append(a[lenm]);
        return b.toString();
    }

    public static final String concatSpace(String[] a) {
        StringBuffer b = new StringBuffer();
        int lenm = a.length - 1;
        for (int i = 0; i < lenm; i++) {
            b.append(a[i]);
            b.append(" ");
        }
        if (lenm != -1)
            b.append(a[lenm]);
        return b.toString();
    }

    public static final String concatWithSeparator(String[] a, String sep) {
        StringBuffer b = new StringBuffer();
        int lenm = a.length - 1;
        for (int i = 0; i < lenm; i++) {
            b.append(a[i]);
            b.append(sep);
        }
        if (lenm != -1)
            b.append(a[lenm]);
        return b.toString();
    }

    public static final String javaEscape(String s) {
        if (s == null)
            return null;
        char c;
        int len = s.length();
        StringBuffer b = new StringBuffer(len);
        for (int i = 0; i < len; i++) {
            c = s.charAt(i);
            switch (c) {
                case '\n':
                    b.append("\\n");
                    break;
                case '\t':
                    b.append("\\t");
                    break;
                case '\r':
                    b.append("\\r");
                    break;
                case '"':
                    b.append("\"");
                    break;
                case '\\':
                    b.append("\\\\");
                    break;
                default:
                    if (c > 127 || Character.isISOControl(c)) {
                        b.append("\\u");
                        String nb = Integer.toString((int) c, 16);
                        int nblen = nb.length();
                        switch (nblen) {
                            case 1:
                                b.append(0);
                            case 2:
                                b.append(0);
                            case 3:
                                b.append(0);
                            case 4:
                                b.append(nb);
                                break;
                            default:
                                throw new IllegalArgumentException(
                                        "Should not happen !");
                        }
                    } else
                        b.append(c);
            }
        }
        return b.toString();
    }

    public static final String javaUnEscape(String s) {
        char c;
        int len = s.length();
        StringBuffer b = new StringBuffer(len);
        for (int i = 0; i < len; i++) {
            c = s.charAt(i);
            if (c == '\\') {
                c = s.charAt(++i);
                switch (c) {
                    case 'n':
                        b.append('\n');
                        break;
                    case 'r':
                        b.append('\r');
                        break;
                    case 't':
                        b.append('\t');
                        break;
                    case '\\':
                        b.append('\\');
                        break;
                    case '"':
                        b.append('"');
                        break;
                    case '\'':
                        b.append('\'');
                        break;
                    case 'u':
                        try {
                            String nb = s.substring(i + 1, i + 5);
                            int n = Integer.parseInt(nb, 16);
                            b.append((char) n);
                            i += 4;
                        } catch (Exception ex) {
                            throw new IllegalArgumentException(
                                    "Illegal unicode escaping in string \"" + s
                                            + "\" at index " + i, ex);
                        }
                        break;
                    default:
                        throw new IllegalArgumentException(
                                "Unknown character: \"\\" + String.valueOf(c)
                                        + "...\"");
                }
            } else
                b.append(c);
        }
        return b.toString();
    }

    public static String capitalize(String string) {
        return string == null ? null : string.length() == 0 ? "" : Character
                .toUpperCase(string.charAt(0)) + string.substring(1);
    }

    public static String capitalize(List<String> strings, String separator) {
        List<String> cap = new ArrayList<String>(strings.size());
        for (String s : strings)
            cap.add(capitalize(s));
        return implode(cap, separator);
    }

    public static String uncapitalize(String string) {
        return string.length() == 0 ? "" : Character.toLowerCase(string
                .charAt(0)) + string.substring(1);
    }

    public static final String LINE_SEPARATOR;
    static {
        LINE_SEPARATOR = System.getProperty("line.separator");
    }

//    public static class MyGlideModule implements GlideModule {
//        @Override public void applyOptions(Context context, GlideBuilder builder) {
//
//            MemorySizeCalculator calculator = new MemorySizeCalculator(context);
//            int defaultMemoryCacheSize = calculator.getMemoryCacheSize();
//            int defaultBitmapPoolSize = calculator.getBitmapPoolSize();
//            // Apply options to the builder here.
//            builder.setDiskCache(
//                    new InternalCacheDiskCacheFactory(context, "TIL", defaultMemoryCacheSize));
//            builder.setDiskCache(
//                    new DiskLruCacheFactory("TIL", "glide", defaultMemoryCacheSize));
//            builder.setMemoryCache(new LruResourceCache(defaultMemoryCacheSize));
//            builder.setBitmapPool(new LruBitmapPool(defaultBitmapPoolSize));
//            builder.setDecodeFormat(DecodeFormat.PREFER_ARGB_8888);
//            Log.e("Glide Module", ">>>" + true);
//        }
//
//        @Override public void registerComponents(Context context, Glide glide) {
//            // register ModelLoaders here.
//        }
//    }

    public static class MyItemAnimator extends SimpleItemAnimator {
        private static final boolean DEBUG = false;

        private static TimeInterpolator sDefaultInterpolator;

        private ArrayList<RecyclerView.ViewHolder> mPendingRemovals = new ArrayList<>();
        private ArrayList<RecyclerView.ViewHolder> mPendingAdditions = new ArrayList<>();
        private ArrayList<MoveInfo> mPendingMoves = new ArrayList<>();
        private ArrayList<ChangeInfo> mPendingChanges = new ArrayList<>();

        ArrayList<ArrayList<RecyclerView.ViewHolder>> mAdditionsList = new ArrayList<>();
        ArrayList<ArrayList<MoveInfo>> mMovesList = new ArrayList<>();
        ArrayList<ArrayList<ChangeInfo>> mChangesList = new ArrayList<>();

        ArrayList<RecyclerView.ViewHolder> mAddAnimations = new ArrayList<>();
        ArrayList<RecyclerView.ViewHolder> mMoveAnimations = new ArrayList<>();
        ArrayList<RecyclerView.ViewHolder> mRemoveAnimations = new ArrayList<>();
        ArrayList<RecyclerView.ViewHolder> mChangeAnimations = new ArrayList<>();

        private static class MoveInfo {
            public RecyclerView.ViewHolder holder;
            public int fromX, fromY, toX, toY;

            MoveInfo(RecyclerView.ViewHolder holder, int fromX, int fromY, int toX, int toY) {
                this.holder = holder;
                this.fromX = fromX;
                this.fromY = fromY;
                this.toX = toX;
                this.toY = toY;
            }
        }

        private static class ChangeInfo {
            public RecyclerView.ViewHolder oldHolder, newHolder;
            public int fromX, fromY, toX, toY;
            private ChangeInfo(RecyclerView.ViewHolder oldHolder, RecyclerView.ViewHolder newHolder) {
                this.oldHolder = oldHolder;
                this.newHolder = newHolder;
            }

            ChangeInfo(RecyclerView.ViewHolder oldHolder, RecyclerView.ViewHolder newHolder,
                       int fromX, int fromY, int toX, int toY) {
                this(oldHolder, newHolder);
                this.fromX = fromX;
                this.fromY = fromY;
                this.toX = toX;
                this.toY = toY;
            }

            @Override
            public String toString() {
                return "ChangeInfo{"
                        + "oldHolder=" + oldHolder
                        + ", newHolder=" + newHolder
                        + ", fromX=" + fromX
                        + ", fromY=" + fromY
                        + ", toX=" + toX
                        + ", toY=" + toY
                        + '}';
            }
        }

        @Override
        public void runPendingAnimations() {
            boolean removalsPending = !mPendingRemovals.isEmpty();
            boolean movesPending = !mPendingMoves.isEmpty();
            boolean changesPending = !mPendingChanges.isEmpty();
            boolean additionsPending = !mPendingAdditions.isEmpty();
            if (!removalsPending && !movesPending && !additionsPending && !changesPending) {
                // nothing to animate
                return;
            }
            // First, remove stuff
            for (RecyclerView.ViewHolder holder : mPendingRemovals) {
                animateRemoveImpl(holder);
            }
            mPendingRemovals.clear();
            // Next, move stuff
            if (movesPending) {
                final ArrayList<MoveInfo> moves = new ArrayList<>();
                moves.addAll(mPendingMoves);
                mMovesList.add(moves);
                mPendingMoves.clear();
                Runnable mover = new Runnable() {
                    @Override
                    public void run() {
                        for (MoveInfo moveInfo : moves) {
                            animateMoveImpl(moveInfo.holder, moveInfo.fromX, moveInfo.fromY,
                                    moveInfo.toX, moveInfo.toY);
                        }
                        moves.clear();
                        mMovesList.remove(moves);
                    }
                };
                if (removalsPending) {
                    View view = moves.get(0).holder.itemView;
                    ViewCompat.postOnAnimationDelayed(view, mover, getRemoveDuration());
                } else {
                    mover.run();
                }
            }
            // Next, change stuff, to run in parallel with move animations
            if (changesPending) {
                final ArrayList<ChangeInfo> changes = new ArrayList<>();
                changes.addAll(mPendingChanges);
                mChangesList.add(changes);
                mPendingChanges.clear();
                Runnable changer = new Runnable() {
                    @Override
                    public void run() {
                        for (ChangeInfo change : changes) {
                            animateChangeImpl(change);
                        }
                        changes.clear();
                        mChangesList.remove(changes);
                    }
                };
                if (removalsPending) {
                    RecyclerView.ViewHolder holder = changes.get(0).oldHolder;
                    ViewCompat.postOnAnimationDelayed(holder.itemView, changer, getRemoveDuration());
                } else {
                    changer.run();
                }
            }
            // Next, add stuff
            if (additionsPending) {
                final ArrayList<RecyclerView.ViewHolder> additions = new ArrayList<>();
                additions.addAll(mPendingAdditions);
                mAdditionsList.add(additions);
                mPendingAdditions.clear();
                Runnable adder = new Runnable() {
                    @Override
                    public void run() {
                        for (RecyclerView.ViewHolder holder : additions) {
                            animateAddImpl(holder);
                        }
                        additions.clear();
                        mAdditionsList.remove(additions);
                    }
                };
                if (removalsPending || movesPending || changesPending) {
                    long removeDuration = removalsPending ? getRemoveDuration() : 0;
                    long moveDuration = movesPending ? getMoveDuration() : 0;
                    long changeDuration = changesPending ? getChangeDuration() : 0;
                    long totalDelay = removeDuration + Math.max(moveDuration, changeDuration);
                    View view = additions.get(0).itemView;
                    ViewCompat.postOnAnimationDelayed(view, adder, totalDelay);
                } else {
                    adder.run();
                }
            }
        }

        @Override
        public boolean animateRemove(final RecyclerView.ViewHolder holder) {
            resetAnimation(holder);
            mPendingRemovals.add(holder);
            return true;
        }

        private void animateRemoveImpl(final RecyclerView.ViewHolder holder) {
            final View view = holder.itemView;
            final ViewPropertyAnimator animation = view.animate();
            mRemoveAnimations.add(holder);
            animation.setDuration(getRemoveDuration()).alpha(0).setListener(
                    new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animator) {
                            dispatchRemoveStarting(holder);
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            animation.setListener(null);
                            view.setAlpha(1);
                            dispatchRemoveFinished(holder);
                            mRemoveAnimations.remove(holder);
                            dispatchFinishedWhenDone();
                        }
                    }).start();
        }

        @Override
        public boolean animateAdd(final RecyclerView.ViewHolder holder) {
            resetAnimation(holder);
    //        holder.itemView.setAlpha(0);
            mPendingAdditions.add(holder);
            return true;
        }

        void animateAddImpl(final RecyclerView.ViewHolder holder) {
            final View view = holder.itemView;
            final ViewPropertyAnimator animation = view.animate();
            mAddAnimations.add(holder);
            animation.setDuration(getAddDuration())
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animator) {
                            dispatchAddStarting(holder);
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {
                            view.setAlpha(1);
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            animation.setListener(null);
                            dispatchAddFinished(holder);
                            mAddAnimations.remove(holder);
                            dispatchFinishedWhenDone();
                        }
                    }).start();
        }

        @Override
        public boolean animateMove(final RecyclerView.ViewHolder holder, int fromX, int fromY,
                                   int toX, int toY) {
            final View view = holder.itemView;
            fromX += (int) holder.itemView.getTranslationX();
            fromY += (int) holder.itemView.getTranslationY();
            resetAnimation(holder);
            int deltaX = toX - fromX;
            int deltaY = toY - fromY;
            if (deltaX == 0 && deltaY == 0) {
                dispatchMoveFinished(holder);
                return false;
            }
            if (deltaX != 0) {
                view.setTranslationX(-deltaX);
            }
            if (deltaY != 0) {
                view.setTranslationY(-deltaY);
            }
            mPendingMoves.add(new MoveInfo(holder, fromX, fromY, toX, toY));
            return true;
        }

        void animateMoveImpl(final RecyclerView.ViewHolder holder, int fromX, int fromY, int toX, int toY) {
            final View view = holder.itemView;
            final int deltaX = toX - fromX;
            final int deltaY = toY - fromY;
            if (deltaX != 0) {
                view.animate().translationX(0);
            }
            if (deltaY != 0) {
                view.animate().translationY(0);
            }
            // TODO: make EndActions end listeners instead, since end actions aren't called when
            // vpas are canceled (and can't end them. why?)
            // need listener functionality in VPACompat for this. Ick.
            final ViewPropertyAnimator animation = view.animate();
            mMoveAnimations.add(holder);
            animation.setDuration(getMoveDuration()).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animator) {
                    dispatchMoveStarting(holder);
                }

                @Override
                public void onAnimationCancel(Animator animator) {
                    if (deltaX != 0) {
                        view.setTranslationX(0);
                    }
                    if (deltaY != 0) {
                        view.setTranslationY(0);
                    }
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    animation.setListener(null);
                    dispatchMoveFinished(holder);
                    mMoveAnimations.remove(holder);
                    dispatchFinishedWhenDone();
                }
            }).start();
        }

        @Override
        public boolean animateChange(RecyclerView.ViewHolder oldHolder, RecyclerView.ViewHolder newHolder,
                                     int fromX, int fromY, int toX, int toY) {
            if (oldHolder == newHolder) {
                // Don't know how to run change animations when the same view holder is re-used.
                // run a move animation to handle position changes.
                return animateMove(oldHolder, fromX, fromY, toX, toY);
            }
            final float prevTranslationX = oldHolder.itemView.getTranslationX();
            final float prevTranslationY = oldHolder.itemView.getTranslationY();
            final float prevAlpha = oldHolder.itemView.getAlpha();
            resetAnimation(oldHolder);
            int deltaX = (int) (toX - fromX - prevTranslationX);
            int deltaY = (int) (toY - fromY - prevTranslationY);
            // recover prev translation state after ending animation
            oldHolder.itemView.setTranslationX(prevTranslationX);
            oldHolder.itemView.setTranslationY(prevTranslationY);
            oldHolder.itemView.setAlpha(prevAlpha);
            if (newHolder != null) {
                // carry over translation values
                resetAnimation(newHolder);
                newHolder.itemView.setTranslationX(-deltaX);
                newHolder.itemView.setTranslationY(-deltaY);
                newHolder.itemView.setAlpha(0);
            }
            mPendingChanges.add(new ChangeInfo(oldHolder, newHolder, fromX, fromY, toX, toY));
            return true;
        }

        void animateChangeImpl(final ChangeInfo changeInfo) {
            final RecyclerView.ViewHolder holder = changeInfo.oldHolder;
            final View view = holder == null ? null : holder.itemView;
            final RecyclerView.ViewHolder newHolder = changeInfo.newHolder;
            final View newView = newHolder != null ? newHolder.itemView : null;
            if (view != null) {
                final ViewPropertyAnimator oldViewAnim = view.animate().setDuration(
                        getChangeDuration());
                mChangeAnimations.add(changeInfo.oldHolder);
    //            oldViewAnim.translationX(changeInfo.toX - changeInfo.fromX);
    //            oldViewAnim.translationY(changeInfo.toY - changeInfo.fromY);
                oldViewAnim.setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                        dispatchChangeStarting(changeInfo.oldHolder, true);
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        oldViewAnim.setListener(null);
                        view.setAlpha(1);
                        view.setTranslationX(0);
                        view.setTranslationY(0);
                        dispatchChangeFinished(changeInfo.oldHolder, true);
                        mChangeAnimations.remove(changeInfo.oldHolder);
                        dispatchFinishedWhenDone();
                    }
                }).start();
            }
            if (newView != null) {
                final ViewPropertyAnimator newViewAnimation = newView.animate();
                mChangeAnimations.add(changeInfo.newHolder);
                newViewAnimation.translationX(0).translationY(0).setDuration(getChangeDuration())
                        .alpha(1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                        dispatchChangeStarting(changeInfo.newHolder, false);
                    }
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        newViewAnimation.setListener(null);
                        newView.setAlpha(1);
                        newView.setTranslationX(0);
                        newView.setTranslationY(0);
                        dispatchChangeFinished(changeInfo.newHolder, false);
                        mChangeAnimations.remove(changeInfo.newHolder);
                        dispatchFinishedWhenDone();
                    }
                }).start();
            }
        }

        private void endChangeAnimation(List<ChangeInfo> infoList, RecyclerView.ViewHolder item) {
            for (int i = infoList.size() - 1; i >= 0; i--) {
                ChangeInfo changeInfo = infoList.get(i);
                if (endChangeAnimationIfNecessary(changeInfo, item)) {
                    if (changeInfo.oldHolder == null && changeInfo.newHolder == null) {
                        infoList.remove(changeInfo);
                    }
                }
            }
        }

        private void endChangeAnimationIfNecessary(ChangeInfo changeInfo) {
            if (changeInfo.oldHolder != null) {
                endChangeAnimationIfNecessary(changeInfo, changeInfo.oldHolder);
            }
            if (changeInfo.newHolder != null) {
                endChangeAnimationIfNecessary(changeInfo, changeInfo.newHolder);
            }
        }
        private boolean endChangeAnimationIfNecessary(ChangeInfo changeInfo, RecyclerView.ViewHolder item) {
            boolean oldItem = false;
            if (changeInfo.newHolder == item) {
                changeInfo.newHolder = null;
            } else if (changeInfo.oldHolder == item) {
                changeInfo.oldHolder = null;
                oldItem = true;
            } else {
                return false;
            }
            item.itemView.setAlpha(1);
            item.itemView.setTranslationX(0);
            item.itemView.setTranslationY(0);
            dispatchChangeFinished(item, oldItem);
            return true;
        }

        @Override
        public void endAnimation(RecyclerView.ViewHolder item) {
            final View view = item.itemView;
            // this will trigger end callback which should set properties to their target values.
            view.animate().cancel();
            // TODO if some other animations are chained to end, how do we cancel them as well?
            for (int i = mPendingMoves.size() - 1; i >= 0; i--) {
                MoveInfo moveInfo = mPendingMoves.get(i);
                if (moveInfo.holder == item) {
                    view.setTranslationY(0);
                    view.setTranslationX(0);
                    dispatchMoveFinished(item);
                    mPendingMoves.remove(i);
                }
            }
            endChangeAnimation(mPendingChanges, item);
            if (mPendingRemovals.remove(item)) {
                view.setAlpha(1);
                dispatchRemoveFinished(item);
            }
            if (mPendingAdditions.remove(item)) {
                view.setAlpha(1);
                dispatchAddFinished(item);
            }

            for (int i = mChangesList.size() - 1; i >= 0; i--) {
                ArrayList<ChangeInfo> changes = mChangesList.get(i);
                endChangeAnimation(changes, item);
                if (changes.isEmpty()) {
                    mChangesList.remove(i);
                }
            }
            for (int i = mMovesList.size() - 1; i >= 0; i--) {
                ArrayList<MoveInfo> moves = mMovesList.get(i);
                for (int j = moves.size() - 1; j >= 0; j--) {
                    MoveInfo moveInfo = moves.get(j);
                    if (moveInfo.holder == item) {
                        view.setTranslationY(0);
                        view.setTranslationX(0);
                        dispatchMoveFinished(item);
                        moves.remove(j);
                        if (moves.isEmpty()) {
                            mMovesList.remove(i);
                        }
                        break;
                    }
                }
            }
            for (int i = mAdditionsList.size() - 1; i >= 0; i--) {
                ArrayList<RecyclerView.ViewHolder> additions = mAdditionsList.get(i);
                if (additions.remove(item)) {
                    view.setAlpha(1);
                    dispatchAddFinished(item);
                    if (additions.isEmpty()) {
                        mAdditionsList.remove(i);
                    }
                }
            }

            // animations should be ended by the cancel above.
            //noinspection PointlessBooleanExpression,ConstantConditions
            if (mRemoveAnimations.remove(item) && DEBUG) {
                throw new IllegalStateException("after animation is cancelled, item should not be in "
                        + "mRemoveAnimations list");
            }

            //noinspection PointlessBooleanExpression,ConstantConditions
            if (mAddAnimations.remove(item) && DEBUG) {
                throw new IllegalStateException("after animation is cancelled, item should not be in "
                        + "mAddAnimations list");
            }

            //noinspection PointlessBooleanExpression,ConstantConditions
            if (mChangeAnimations.remove(item) && DEBUG) {
                throw new IllegalStateException("after animation is cancelled, item should not be in "
                        + "mChangeAnimations list");
            }

            //noinspection PointlessBooleanExpression,ConstantConditions
            if (mMoveAnimations.remove(item) && DEBUG) {
                throw new IllegalStateException("after animation is cancelled, item should not be in "
                        + "mMoveAnimations list");
            }
            dispatchFinishedWhenDone();
        }

        private void resetAnimation(RecyclerView.ViewHolder holder) {
            if (sDefaultInterpolator == null) {
                sDefaultInterpolator = new ValueAnimator().getInterpolator();
            }
            holder.itemView.animate().setInterpolator(sDefaultInterpolator);
            endAnimation(holder);
        }

        @Override
        public boolean isRunning() {
            return (!mPendingAdditions.isEmpty()
                    || !mPendingChanges.isEmpty()
                    || !mPendingMoves.isEmpty()
                    || !mPendingRemovals.isEmpty()
                    || !mMoveAnimations.isEmpty()
                    || !mRemoveAnimations.isEmpty()
                    || !mAddAnimations.isEmpty()
                    || !mChangeAnimations.isEmpty()
                    || !mMovesList.isEmpty()
                    || !mAdditionsList.isEmpty()
                    || !mChangesList.isEmpty());
        }

        /**
         * Check the state of currently pending and running animations. If there are none
         * pending/running, call {@link #dispatchAnimationsFinished()} to notify any
         * listeners.
         */
        void dispatchFinishedWhenDone() {
            if (!isRunning()) {
                dispatchAnimationsFinished();
            }
        }

        @Override
        public void endAnimations() {
            int count = mPendingMoves.size();
            for (int i = count - 1; i >= 0; i--) {
                MoveInfo item = mPendingMoves.get(i);
                View view = item.holder.itemView;
                view.setTranslationY(0);
                view.setTranslationX(0);
                dispatchMoveFinished(item.holder);
                mPendingMoves.remove(i);
            }
            count = mPendingRemovals.size();
            for (int i = count - 1; i >= 0; i--) {
                RecyclerView.ViewHolder item = mPendingRemovals.get(i);
                dispatchRemoveFinished(item);
                mPendingRemovals.remove(i);
            }
            count = mPendingAdditions.size();
            for (int i = count - 1; i >= 0; i--) {
                RecyclerView.ViewHolder item = mPendingAdditions.get(i);
                item.itemView.setAlpha(1);
                dispatchAddFinished(item);
                mPendingAdditions.remove(i);
            }
            count = mPendingChanges.size();
            for (int i = count - 1; i >= 0; i--) {
                endChangeAnimationIfNecessary(mPendingChanges.get(i));
            }
            mPendingChanges.clear();
            if (!isRunning()) {
                return;
            }

            int listCount = mMovesList.size();
            for (int i = listCount - 1; i >= 0; i--) {
                ArrayList<MoveInfo> moves = mMovesList.get(i);
                count = moves.size();
                for (int j = count - 1; j >= 0; j--) {
                    MoveInfo moveInfo = moves.get(j);
                    RecyclerView.ViewHolder item = moveInfo.holder;
                    View view = item.itemView;
                    view.setTranslationY(0);
                    view.setTranslationX(0);
                    dispatchMoveFinished(moveInfo.holder);
                    moves.remove(j);
                    if (moves.isEmpty()) {
                        mMovesList.remove(moves);
                    }
                }
            }
            listCount = mAdditionsList.size();
            for (int i = listCount - 1; i >= 0; i--) {
                ArrayList<RecyclerView.ViewHolder> additions = mAdditionsList.get(i);
                count = additions.size();
                for (int j = count - 1; j >= 0; j--) {
                    RecyclerView.ViewHolder item = additions.get(j);
                    View view = item.itemView;
                    view.setAlpha(1);
                    dispatchAddFinished(item);
                    additions.remove(j);
                    if (additions.isEmpty()) {
                        mAdditionsList.remove(additions);
                    }
                }
            }
            listCount = mChangesList.size();
            for (int i = listCount - 1; i >= 0; i--) {
                ArrayList<ChangeInfo> changes = mChangesList.get(i);
                count = changes.size();
                for (int j = count - 1; j >= 0; j--) {
                    endChangeAnimationIfNecessary(changes.get(j));
                    if (changes.isEmpty()) {
                        mChangesList.remove(changes);
                    }
                }
            }

            cancelAll(mRemoveAnimations);
            cancelAll(mMoveAnimations);
            cancelAll(mAddAnimations);
            cancelAll(mChangeAnimations);

            dispatchAnimationsFinished();
        }

        void cancelAll(List<RecyclerView.ViewHolder> viewHolders) {
            for (int i = viewHolders.size() - 1; i >= 0; i--) {
                viewHolders.get(i).itemView.animate().cancel();
            }
        }

        /**
         * {@inheritDoc}
         * <p>
         * If the payload list is not empty, DefaultItemAnimator returns <code>true</code>.
         * When this is the case:
         * <ul>
         * <li>If you override {@link #animateChange(RecyclerView.ViewHolder, RecyclerView.ViewHolder, int, int, int, int)}, both
         * ViewHolder arguments will be the same instance.
         * </li>
         * <li>
         * If you are not overriding {@link #animateChange(RecyclerView.ViewHolder, RecyclerView.ViewHolder, int, int, int, int)},
         * then DefaultItemAnimator will call {@link #animateMove(RecyclerView.ViewHolder, int, int, int, int)} and
         * run a move animation instead.
         * </li>
         * </ul>
         */
        @Override
        public boolean canReuseUpdatedViewHolder(@NonNull RecyclerView.ViewHolder viewHolder,
                                                 @NonNull List<Object> payloads) {
            return !payloads.isEmpty() || super.canReuseUpdatedViewHolder(viewHolder, payloads);
        }
    }

    /**
     * Created by Highly Succeed Inc on 10/13/2017.
     */

    public static class PhotoHandler implements Camera.PictureCallback {

        private final Context context;

        public PhotoHandler(Context context) {
            this.context = context;
        }

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File pictureFileDir = getDir();

            if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {

                Log.d("TAG", "Can't create directory to save image.");
                Toast.makeText(context, "Can't create directory to save image.",
                        Toast.LENGTH_LONG).show();
                return;

            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
            String date = dateFormat.format(new Date());
            String photoFile = "Picture_" + date + ".jpg";

            String filename = pictureFileDir.getPath() + File.separator + photoFile;

            File pictureFile = new File(filename);

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                Toast.makeText(context, "New Image saved:" + photoFile,
                        Toast.LENGTH_LONG).show();
            } catch (Exception error) {
                Log.d("TAG", "File" + filename + "not saved: "
                        + error.getMessage());
                Toast.makeText(context, "Image could not be saved.",
                        Toast.LENGTH_LONG).show();
            }
        }

        private File getDir() {
            File sdDir = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            return new File(sdDir, "CameraAPIDemo");
        }
    }

    /**
     * Created by ANDROID-PC on 4/11/2018.
     */

    public static class ReverseInterpolator implements Interpolator {
        private final Interpolator delegate;

        public ReverseInterpolator(Interpolator delegate){
            this.delegate = delegate;
        }

        public ReverseInterpolator(){
            this(new LinearInterpolator());
        }

        @Override
        public float getInterpolation(float input) {
            return 1 - delegate.getInterpolation(input);
        }
    }


}
