package com.thingsilikeapp.vendor.android.java;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by BCTI 3 on 9/22/2017.
 */

public class LoadFromAsset {

    public static Drawable getDrawable(Context context, String path){
        try {
            InputStream inputStream = context.getAssets().open(path);
            Drawable drawable = Drawable.createFromStream(inputStream, null);
            inputStream.close();
            return drawable;
        }
        catch(IOException ex) {
            return null;
        }
    }
}
