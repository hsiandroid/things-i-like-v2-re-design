package com.thingsilikeapp.vendor.android.java.andengine.scene;

import android.content.Context;
import android.util.Log;

import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.java.andengine.BaseScene;
import com.thingsilikeapp.vendor.android.java.andengine.ResourcesManager;
import com.thingsilikeapp.vendor.android.java.andengine.SceneManager;
import com.thingsilikeapp.vendor.android.java.andengine.model.Balloon;
import com.thingsilikeapp.vendor.android.java.andengine.model.BalloonPopResponse;
import com.thingsilikeapp.android.activity.BirthdaySurpriseActivity;

import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Jomar Olaybal on 10/11/2017.
 */

public class BalloonScene extends BaseScene
        implements Balloon.OnPoppedListener{

    private int balloonId;
    private List<Balloon> balloons;
    private BirthdaySurpriseActivity birthdaySurpriseActivity;
    private Context context;
    private Timer balloonTimer;

    @Override
    public void createScene() {
        context = activity;
        birthdaySurpriseActivity = (BirthdaySurpriseActivity)activity;
        birthdaySurpriseActivity.playBalloonMusic();

        float CAMERA_WIDTH = BirthdaySurpriseActivity.CAMERA_WIDTH / 2;
        float CAMERA_HEIGHT = BirthdaySurpriseActivity.CAMERA_HEIGHT / 2;
        SpriteBackground bg = new SpriteBackground(new Sprite(CAMERA_WIDTH, CAMERA_HEIGHT, ResourcesManager.getInstance().backgroundITR, vbom));
        setBackground(bg);
        setGreetings();
        setBalloon();
        setBalloonTimer();
    }

    @Override
    public void onBackKeyPressed() {

    }

    @Override
    public SceneManager.SceneType getSceneType() {
        return SceneManager.SceneType.SCENE_BALLOON;
    }

    @Override
    public void disposeScene() {
        for(Balloon balloon : balloons){
            balloon.getBalloonResource().unloadBalloonScreen();
        }
        EventBus.getDefault().unregister(this);
        this.detachSelf();
        this.dispose();
    }

    private void setBalloon() {
        fixBalloonCount();
    }

    private void generateBalloons(int count){
        for(int i= 0 ; i < count ; i++){
            balloonId++;
            Balloon balloon = new Balloon(Balloon.BalloonResource.getDefault((BirthdaySurpriseActivity) activity), vbom);
            balloon.setOnPoppedListener(this);
            balloon.setId(balloonId);
            balloons.add(balloon);
            registerTouchArea(balloon);
            attachChild( balloon);
        }
    }

    public void setBalloonTimer(){
        balloonTimer = new Timer();
        balloonTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(balloons.size() > 0){
                    try {
                        Iterator<Balloon> iterator = balloons.iterator();
                        while (iterator.hasNext()) {
                            Balloon balloon = iterator.next();
                            if(balloon != null){
                                balloon.balloonFloat();
                            }
                        }
                    }catch (ConcurrentModificationException e){

                    }
                }else{
                    balloonTimer.cancel();
//                    birthdaySurpriseActivity.stopBalloonMusic();
//                    activity.finish();
                }
            }
        }, 0, 1);
    }

    @Override
    public void onPopped(int id) {
        birthdaySurpriseActivity.openGreetingCard();
        for(Balloon b : balloons){
            if(b.getId() == id){
                balloons.remove(b);
                break;
            }
        }
        fixBalloonCount();
    }

    private void popAll(){
        balloonTimer.cancel();
        Iterator<Balloon> iterator = balloons.iterator();
        while (iterator.hasNext()) {
            Balloon balloon = iterator.next();
            if(balloon != null){
                balloon.poppedDelay();
                iterator.remove();
            }
        }
    }

    private void setGreetings(){
        balloons = new ArrayList<>();
    }

    private void fixBalloonCount(){
        int maxBalloon = 10;
        int balloonCount = balloons.size();
        int greetingCount = UserData.getInt(UserData.GREETINGS);

        Log.e("Balloon count", ">>>" + balloonCount);
        Log.e("Greetings count", ">>>" + greetingCount);

        if(greetingCount < maxBalloon ){
            maxBalloon = greetingCount;
        }

        if(balloonCount < maxBalloon){
            int difference = maxBalloon - balloonCount;
            generateBalloons(difference);
            Log.e("Generated", ">>>" + difference);
        }
    }

    @Subscribe
    public void onResponse(BalloonPopResponse responseData) {
        fixBalloonCount();
    }
}

