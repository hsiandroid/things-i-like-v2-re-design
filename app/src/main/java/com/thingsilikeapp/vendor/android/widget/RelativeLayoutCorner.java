package com.thingsilikeapp.vendor.android.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.thingsilikeapp.R;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class RelativeLayoutCorner extends RelativeLayout {
    /** Used locally to tag Logs */
    @SuppressWarnings("unused")
    private static final String TAG = RelativeLayoutCorner.class.getSimpleName();
    private final Path mPath = new Path();

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public RelativeLayoutCorner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public RelativeLayoutCorner(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RelativeLayoutCorner(Context context) {
        super(context);
        init();
    }

    @SuppressLint("NewApi")
    private void init() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
// we have to remove the hardware acceleration if we want the clip
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        setBackgroundResource(R.drawable.bg_rounded_white);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mPath.reset();
        float round = getResources().getDimension(R.dimen.dimen_20);
        mPath.addRoundRect(new RectF(getPaddingLeft(), getPaddingTop(), w - getPaddingRight(), h - getPaddingBottom()), round, round, Path.Direction.CW);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        canvas.clipPath(mPath);
        super.dispatchDraw(canvas);
    }
}