package com.thingsilikeapp.vendor.android.java.firebase;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.thingsilikeapp.data.preference.Data;
import com.thingsilikeapp.vendor.android.java.pusher.Sound;

/**
 * Created by BCTI 3 on 9/22/2017.
 */

public class NotificationReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Data.setSharedPreferences(context);
        Sound.playNotification(context);
    }
}
