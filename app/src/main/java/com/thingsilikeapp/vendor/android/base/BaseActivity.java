package com.thingsilikeapp.vendor.android.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.Data;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.config.App;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import icepick.Icepick;
import icepick.State;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity {

    private Unbinder unbinder;
    @State String fragmentName;
    @State int fragmentFrame = R.id.content_frame;
    private Context context;
    private Fragment currentFragment;
    @State boolean hasBackButton = false;
    private AppEventsLogger appEventsLogger;
    private long doneButtonClickTime = 0;


    @Nullable@BindView(R.id.noInternetIMG)      View noInternetConnectionIMG;
    @Nullable@BindView(R.id.testIndicatorTXT)   View testIndicatorTXT;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        setContentView(onLayoutSet());
        AppEventsLogger.activateApp(this);
        appEventsLogger = AppEventsLogger.newLogger(this);
        bindView();
        context = this;
        Data.setSharedPreferences(context);
        initialFragment(getActivityTag(), getFragmentTag());
        displayIndicator();
        onViewReady();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void initialFragment(String activityName, String fragmentName){

    }

    private void displayIndicator(){
        if(testIndicatorTXT != null){
            testIndicatorTXT.setVisibility(getPackageName().contains("testapk") ? View.VISIBLE : View.GONE);
        }
    }

    public void disableMultipleClick(){
        if (SystemClock.elapsedRealtime() - doneButtonClickTime < 1000){
            return;
        }

        //store time of button click
        doneButtonClickTime = SystemClock.elapsedRealtime();
    }


    public String getDeviceRegID(){
        return FirebaseInstanceId.getInstance().getToken();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(hasBackButton){
            switch (item.getItemId()) {
                case android.R.id.home:
                    finish();
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Icepick.saveInstanceState(this, outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    @Override
    public void onBackPressed() {
            int backStackCount = getBackStackCount();

            if(backStackCount > 1){
                int index = backStackCount - 2;
                FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
                String tag = backEntry.getName();
                currentFragment = getSupportFragmentManager().findFragmentByTag(tag);
            }

            if ( backStackCount <= 1 ) {
                if(isTaskRoot()){
                    exitConfirmation();
                }else{
                    finish();
                }
            } else {
                super.onBackPressed();
            }

//        if (UserData.getBoolean(UserData.WALKTHRU_DONE) || UserData.getInt(UserData.WALKTHRU_CURRENT) == 8){
//            int backStackCount = getBackStackCount();
//
//            if(backStackCount > 1){
//                int index = backStackCount - 2;
//                FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
//                String tag = backEntry.getName();
//                currentFragment = getSupportFragmentManager().findFragmentByTag(tag);
//            }
//
//            if ( backStackCount <= 1 ) {
//                if(isTaskRoot()){
//                    exitConfirmation();
//                }else{
//                    finish();
//                }
//            } else {
//                super.onBackPressed();
//            }
//            UserData.insert(UserData.WALKTHRU_CURRENT, 9);
//        }
    }

    public int getBackStackCount(){
        return  getSupportFragmentManager().getBackStackEntryCount();
    }

    public void exitConfirmation(){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("We hate to see you leave!")
                .setNote(getString(R.string.app_name) + " app is about to close.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Exit")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build((((RouteActivity) context)).getSupportFragmentManager());
    }

    @Override
    protected void onDestroy() {
        unbindView();
        super.onDestroy();
    }


    public int onLayoutSet(){
        return R.layout.activity_template;
    }

    public void onViewReady(){

    }

    public void showToolbarBackButton(boolean hasBackButton){
        getSupportActionBar().setDisplayHomeAsUpEnabled(hasBackButton);
        this.hasBackButton = hasBackButton;
    }

    private void bindView(){
        unbinder = ButterKnife.bind(this);
    }

    private void unbindView(){
        unbinder.unbind();
    }

    public Context getContext(){
        return context;
    }

    public void setFragmentFrame(int fragmentFrame){
        this.fragmentFrame = fragmentFrame;
    }

    public void switchFragment(Fragment fragment) {
        switchFragment(fragment, true);
    }

    public void switchFragment(Fragment fragment, boolean addBackStack) {
        switchFragment(fragment, addBackStack, true);
    }

    public void switchFragment(Fragment fragment, boolean addBackStack, boolean poppedStack) {

        String backStateName =  fragment.getClass().getName();
        fragmentName = backStateName;
        String fragmentTag = backStateName;

        if(poppedStack){
            FragmentManager manager = getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);
            if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null){
                changeFragment(fragment, addBackStack, backStateName, fragmentTag);
            }else{
                currentFragment = getSupportFragmentManager().findFragmentByTag(fragmentTag);
            }
        }else {
            changeFragment(fragment, addBackStack, backStateName, fragmentTag);
        }
    }

    public void reCreateFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(fragment);
        fragmentTransaction.add(R.id.content_frame, fragment);
        fragmentTransaction.commit();
    }

    public String getCurrentFragmentName(){
        return fragmentName;
    }

    private void changeFragment(Fragment fragment, boolean addBackStack, String backStateName, String fragmentTag){

        currentFragment = fragment;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.replace(R.id.content_frame, fragment, fragmentTag);
        if(addBackStack){
            fragmentTransaction.addToBackStack(backStateName);
        }
        fragmentTransaction.commit();
    }

    public void switchFragmentPopped(Fragment fragment) {
        String backStateName =  fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null){ //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

//    public void switchFragment(Fragment fragment){
//        switchFragment(fragment, true, FragmentTransaction.TRANSIT_NONE);
//    }
//
//    public void switchFragment(Fragment fragment, int animation){
//        switchFragment(fragment, true, animation);
//    }
//
//    public void switchFragment(Fragment fragment, boolean addBackStack){
//        switchFragment(fragment, addBackStack, FragmentTransaction.TRANSIT_NONE);
//    }
//
//    public void switchFragment(Fragment fragment, boolean addBackStack, int animation) {
//
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(fragmentFrame, fragment);
//
//        if(animation == FragmentTransaction.TRANSIT_NONE){
//            transaction.setTransition(FragmentTransaction.TRANSIT_NONE);
//        }else{
//            transaction.setCustomAnimations(animation,0);
//        }
//        if(addBackStack){
//            transaction.addToBackStack(null);
//        }
//        transaction.commit();
//
//    }

    public Fragment getCurrentFragment(){
        return currentFragment;
    }

    public String getActivityTag(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            return bundle.getString(RouteManager.ACTIVITY_TAG, "");
        }
        return "";
    }

    public String getFragmentTag(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            return bundle.getString(RouteManager.FRAGMENT_TAG, "");
        }
        return "";
    }

    public Bundle getActivityBundle(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            return bundle.getBundle(RouteManager.ACTIVITY_BUNDLE);
        }
        return new Bundle();
    }

    public Bundle getFragmentBundle(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            return bundle.getBundle(RouteManager.FRAGMENT_BUNDLE);
        }
        return new Bundle();
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    public String getSHA(){
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                return Base64.encodeToString(md.digest(), Base64.DEFAULT);
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        return "";
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    public boolean isAllPermissionResultGranted(int [] grantResults){
        boolean granted = true;
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                granted = false;
                break;
            }
        }
        return granted;
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(BaseRequest.OnFailedResponse onFailedResponse){
        if(noInternetConnectionIMG != null){
            if(!noInternetConnectionIMG.isShown()){
                Animation slideRightToLeft = AnimationUtils.loadAnimation(context, R.anim.slide_right_to_left);
                noInternetConnectionIMG.startAnimation(slideRightToLeft);
            }
            noInternetConnectionIMG.setVisibility(View.VISIBLE);
        }
    }

    public void facebookLogger (String event,String message) {
        if(UserData.isLogin()){
            Bundle facebookLoggerBundle = new Bundle();
            facebookLoggerBundle.putInt("user_id", UserData.getUserItem().id);
            facebookLoggerBundle.putString("user_name", UserData.getUserItem().name);
            facebookLoggerBundle.putString("fb_id", UserData.getUserItem().fb_id);
            facebookLoggerBundle.putString("message", message);
            appEventsLogger.logEvent(event, facebookLoggerBundle);
        }else{
            appEventsLogger.logEvent(event);
        }
        Log.e("Logger", event);
    }

    public void shareTheApp(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "LYKA");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, App.SHARE_MESSAGE.replace("{username}", UserData.getUserItem().username));
        startActivity(Intent.createChooser(sharingIntent, "Share LYKA"));
    }

    public int getScreenWidth(){
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }

    public int getScreenHeight(){
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.heightPixels;
    }

    public void openUrl(String url){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if(fragment != null){
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

}
