package com.thingsilikeapp.vendor.android.java;

import com.thingsilikeapp.data.preference.UserData;

import java.text.DecimalFormat;

public class GemHelpers {

    public static String getCurrentGem(){
        return formatGem(UserData.getRewardCrystal());
    }

    public static String formatGem(String number){
        return formatGem(parseDouble(number));
    }

    public static String formatGem(double number){
        return new DecimalFormat("0.000").format(number);
    }

    public static double parseDouble(String number){
        if(StringFormatter.isEmpty(number)){
            return 0;
        }

        if(number.equalsIgnoreCase(".")){
            return 0;
        }

        return Double.parseDouble(replaceChar(number));
    }

    public static String replaceChar(String number){
        return number.replaceAll("[*a-zA-Z]", "").replaceAll(" ", "");
    }
}
