package com.thingsilikeapp.vendor.android.java.image;

/**
 * Created by BCTI 3 on 7/25/2017.
 */

public class AlbumItem {
    public String id;
    public String album;
    public String date;
    public String thumbnail;
}
