package com.thingsilikeapp.vendor.android.widget;

/**
 * Created by BCTI 3 on 3/6/2017.
 */
import android.content.Context;
import android.util.AttributeSet;

public class FillParentImageView extends android.support.v7.widget.AppCompatImageView {

    public FillParentImageView(Context context) {
        super(context);
    }

    public FillParentImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FillParentImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        this.setMeasuredDimension(parentWidth / 2, parentHeight);
    }
}
