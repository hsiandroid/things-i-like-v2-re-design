package com.thingsilikeapp.vendor.android.java.andengine.model;

import com.thingsilikeapp.data.model.GreetingItem;

/**
 * Created by Jomar Olaybal on 10/14/2017.
 */

public class BalloonPopResponse{

    private GreetingItem greetingItem;
    public BalloonPopResponse(GreetingItem greetingItem){
        this.greetingItem = greetingItem;
    }

    public GreetingItem getGreetingItem() {
        return greetingItem;
    }
}