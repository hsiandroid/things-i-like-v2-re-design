package com.thingsilikeapp.vendor.android.widget.image;

/**
 * Created by BCTI 3 on 3/6/2017.
 */
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

public class FixedHeightImageView extends AppCompatImageView {

    private int imageWidth = 0;
    private int imageHeight = 0;
    private boolean autoResize = false;

    public FixedHeightImageView(Context context) {
        super(context);
    }

    public FixedHeightImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FixedHeightImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setImageSize(int imageWidth, int imageHeight){
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
    }

    public void autoResize(boolean autoResize){
        this.autoResize = autoResize;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(autoResize){
            Drawable drawable = getDrawable();
            if (drawable != null) {
                int height = MeasureSpec.getSize(heightMeasureSpec);
                int dih = drawable.getIntrinsicHeight();
                if (dih > 0) {
                    int width = height * drawable.getIntrinsicHeight() / dih;
                    setMeasuredDimension(width, height);
                    return;
                }
            }
        }else if (imageHeight != 0) {

            int height = MeasureSpec.getSize(heightMeasureSpec);
            int dih = imageHeight;
            if (dih > 0) {
                int width = height * imageWidth / dih;
                setMeasuredDimension(width, height);
                return;
            }
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
