package com.thingsilikeapp.vendor.android.java.andengine;

import com.thingsilikeapp.android.activity.BirthdaySurpriseActivity;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by Jomar Olaybal on 10/11/2017.
 */

public class ResourcesManager {
    private static final ResourcesManager INSTANCE = new ResourcesManager();

    public Engine engine;
    public BirthdaySurpriseActivity activity;
    public Camera camera;
    public VertexBufferObjectManager vbom;

    public static void prepareManager(Engine engine, BirthdaySurpriseActivity activity, Camera camera, VertexBufferObjectManager vbom) {
        getInstance().engine = engine;
        getInstance().activity = activity;
        getInstance().camera = camera;
        getInstance().vbom = vbom;
    }

    //---------------------------------------------
    // GETTERS AND SETTERS
    //---------------------------------------------

    public static ResourcesManager getInstance()
    {
        return INSTANCE;
    }

    /**
     * BALLOON
     */
    private BitmapTextureAtlas balloonBTA;
    public ITextureRegion balloonITR;

    /**
     * Background
     */
    private BitmapTextureAtlas backgroundBTA;
    public ITextureRegion backgroundITR;

    public void loadBalloonScreen() {
        loadBalloonGraphics();
    }

    public void unloadBalloonScreen() {
        balloonBTA.unload();
        balloonITR = null;
    }

    private void loadBalloonGraphics() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("graphics/balloon/");
        backgroundBTA = new BitmapTextureAtlas(activity.getTextureManager(), 300, 300, TextureOptions.BILINEAR);
        backgroundITR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(backgroundBTA, activity, "red.png", 0, 0);
        backgroundBTA.load();
    }

    public void loadBackgroundScreen() {
        loadBackgroundGraphics();
    }

    public void unloadBackgroundScreen() {
        backgroundBTA.unload();
        backgroundITR = null;
    }

    private void loadBackgroundGraphics() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("graphics/background/");
        backgroundBTA = new BitmapTextureAtlas(activity.getTextureManager(), 720, 1280, TextureOptions.BILINEAR);
        backgroundITR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(backgroundBTA, activity, "background.jpg", 0, 0);
        backgroundBTA.load();
    }
}
