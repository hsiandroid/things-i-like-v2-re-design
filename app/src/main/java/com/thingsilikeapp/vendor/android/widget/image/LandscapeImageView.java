package com.thingsilikeapp.vendor.android.widget.image;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by BCTI 3 on 9/9/2016.
 */
public class LandscapeImageView extends AppCompatImageView {

    public LandscapeImageView(Context context) {
        super(context);
    }
    public LandscapeImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LandscapeImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();
        setMeasuredDimension(width, width / 2);
    }
}
