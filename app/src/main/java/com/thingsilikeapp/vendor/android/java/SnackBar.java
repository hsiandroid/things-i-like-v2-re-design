package com.thingsilikeapp.vendor.android.java;

import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.thingsilikeapp.R;

public class SnackBar {
    private static Snackbar snackbar;
    public static final int LONG = Snackbar.LENGTH_LONG;
    public static final int SHORT = Snackbar.LENGTH_SHORT;

    public static void  custom(View view, String message, int duration){
        snackbar = Snackbar.make(view, message, duration);
        snackbar.show();
    }

    public static void make(View view, String message, int duration){
        snackbar = Snackbar.make(view, message, duration);
        setBackgroundColor(ActivityCompat.getColor(view.getContext(), R.color.colorPrimary));
//        snackbar.show();
    }

    public static void setActionTextColor(int color){
        snackbar.setActionTextColor(color);
    }

    public static void setAction(String message, View.OnClickListener onClickListener){
        snackbar.setAction(message, onClickListener);
    }

    public static void setBackgroundColor(int backgroundColor){
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(backgroundColor);
    }

    public static void show(){
        snackbar.show();
    }

    public static void addCallback(Snackbar.Callback callback){
        snackbar.addCallback(callback);
    }

    public static void dismiss(){
        snackbar.dismiss();
    }

    public static void setDuration(int duration) {
        snackbar.setDuration(duration);
    }
}
