package com.thingsilikeapp.vendor.android.java;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.TypedValue;

import com.thingsilikeapp.R;

/**
 * Created by BCTI 3 on 7/25/2017.
 */

public class VerifiedUserBadge {

    public static SpannableString getFormattedName(Context context, String name, boolean isVerified){
        return getFormattedName(context, name, isVerified, 12, TypedValue.COMPLEX_UNIT_SP, false);
    }

    public static SpannableString getFormattedName(Context context, String name, boolean isVerified, int size){
        return getFormattedName(context, name, isVerified, size, TypedValue.COMPLEX_UNIT_SP, false);
    }

    public static SpannableString getFormattedName(Context context, String name, boolean isVerified, int size, boolean white){
        return getFormattedName(context, name, isVerified, size, TypedValue.COMPLEX_UNIT_SP, white);
    }

    public static SpannableString getFormattedName(Context context, String name, boolean isVerified, int size, int type, boolean white){
        if(isVerified){
            SpannableString spannableString = new SpannableString(name + "   ");
            Drawable image = ActivityCompat.getDrawable(context, white ? R.drawable.icon_verified_user_white : R.drawable.icon_verified_user);
            int px = (int) TypedValue.applyDimension(type, size, context.getResources().getDisplayMetrics());
            image.setBounds(0, 0, px, px);
            ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BASELINE);
            spannableString.setSpan(imageSpan, name.length() + 2, name.length() + 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return spannableString;
        }
        return new SpannableString(name);
    }
}
