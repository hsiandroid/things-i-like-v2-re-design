package com.thingsilikeapp.vendor.android.java.andengine;

import com.thingsilikeapp.vendor.android.java.andengine.scene.BalloonScene;

import org.andengine.engine.Engine;
import org.andengine.ui.IGameInterface;

/**
 * Created by Jomar Olaybal on 10/11/2017.
 */

public class SceneManager {

    //---------------------------------------------
    // SCENES
    //---------------------------------------------

    private BaseScene balloonScene;

    //---------------------------------------------
    // VARIABLES
    //---------------------------------------------

    private static final SceneManager INSTANCE = new SceneManager();

    private SceneType currentSceneType = SceneType.SCENE_BALLOON;

    private BaseScene currentScene;

    private Engine engine = ResourcesManager.getInstance().engine;

    public enum SceneType {
        SCENE_BALLOON,
    }


    //---------------------------------------------
    // GETTERS AND SETTERS
    //---------------------------------------------

    public static SceneManager getInstance() {
        return INSTANCE;
    }

    public SceneType getCurrentSceneType() {
        return currentSceneType;
    }

    public BaseScene getCurrentScene() {
        return currentScene;
    }

    //---------------------------------------------
    // CLASS LOGIC
    //-------------	--------------------------------

    public void setScene(BaseScene scene) {
        engine.setScene(scene);
        currentScene = scene;
        currentSceneType = scene.getSceneType();
    }

    public void setScene(SceneType sceneType) {
        switch (sceneType) {
            case SCENE_BALLOON:
                setScene(balloonScene);
                break;
            default:
                break;
        }
    }

    public void loadBalloonScene(IGameInterface.OnCreateSceneCallback pOnCreateSceneCallback){
        ResourcesManager.getInstance().loadBackgroundScreen();
        balloonScene = new BalloonScene();
        currentScene = balloonScene;
        setScene(balloonScene);
        pOnCreateSceneCallback.onCreateSceneFinished(currentScene);
    }

    public void disposeBalloonScene() {
        ResourcesManager.getInstance().unloadBackgroundScreen();
        balloonScene.disposeScene();
        balloonScene = null;
    }

}
