package com.thingsilikeapp.vendor.android.widget.image;

/**
 * Created by BCTI 3 on 3/6/2017.
 */
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

public class FixedWidthImageView extends AppCompatImageView {

    private int imageWidth = 0;
    private int imageHeight = 0;
    private boolean autoResize = false;

    public FixedWidthImageView(Context context) {
        super(context);
    }

    public FixedWidthImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FixedWidthImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setImageSize(int imageWidth, int imageHeight){
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
    }

    public void autoResize(boolean autoResize){
        this.autoResize = autoResize;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(autoResize){
            Drawable drawable = getDrawable();
            if (drawable != null) {
                int width = MeasureSpec.getSize(widthMeasureSpec);
                int diw = drawable.getIntrinsicWidth();
                if (diw > 0) {
                    int height = width * drawable.getIntrinsicHeight() / diw;
                    setMeasuredDimension(width, height);
                    return;
                }
            }
        }else if (imageWidth != 0) {

            int width = MeasureSpec.getSize(widthMeasureSpec);
            int diw = imageWidth;
            if (diw > 0) {
                int height = width * imageHeight / diw;
                setMeasuredDimension(width, height);
                return;
            }
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
