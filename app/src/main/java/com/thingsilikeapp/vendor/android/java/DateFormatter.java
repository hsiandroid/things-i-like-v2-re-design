package com.thingsilikeapp.vendor.android.java;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by BCTI 3 on 1/31/2018.
 */

public class DateFormatter {

    private static final String DEFAULT_FORMAT  = "yyyy-MM-dd HH:mm:ss";

    public static String format(Date date){
        return format(date, DEFAULT_FORMAT);
    }

    public static String format(Date date, String format){
        if(date == null){
            return "";
        }
        return getSimpleDateFormat(format).format(date.getTime());
    }

    public static String format(String timestamp, String format){
        return format(timestamp, DEFAULT_FORMAT, format);
    }

    public static String format(String timestamp, String timestampFormat, String format){

        if(StringFormatter.isEmpty(timestamp)){
            return "";
        }

        SimpleDateFormat dayFormat = getSimpleDateFormat(timestampFormat);
        try {
            Date date = dayFormat.parse(timestamp);
            return getSimpleDateFormat(format).format(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String addDay(String timestamp, String timestampFormat, String format, int numDays){

        if(StringFormatter.isEmpty(timestamp)){
            return "";
        }

        SimpleDateFormat dayFormat = getSimpleDateFormat(timestampFormat);
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(dayFormat.parse(timestamp));
            calendar.add(Calendar.DATE, numDays);
            return getSimpleDateFormat(format).format(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String formatTimePassed(String timestamp){
        return formatTimePassed(timestamp, new Date());
    }

    public static String formatTimePassed(String timestamp, Date currentDate){
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(getSimpleDateFormat(DEFAULT_FORMAT).parse(timestamp));

            Calendar currentCalendar = Calendar.getInstance();
            currentCalendar.setTime(currentDate);

            long time = calendar.getTime().getTime()  / 1000;
            long currentTime = currentDate.getTime() / 1000;

            long secondsAgo = currentTime - time;
            double period;

            if (secondsAgo < 60){
                if(secondsAgo < 30){
                    return "Just Now.";
                }else{
                    return secondsAgo == 1 ? "1 sec ago"     : secondsAgo + " secs ago";
                }
            } else if(secondsAgo < 3600){
                period = Math.floor(secondsAgo / 60);
                return  (int) period == 1 ? "1 min ago" : (int) period + " mins ago";
            }else if(secondsAgo < 86400){
                period = Math.floor(secondsAgo / 3600);
                return  (int) period == 1 ? "1 hr ago" : (int) period + " hrs ago";
            }else if(secondsAgo < 432000){
                period = Math.floor(secondsAgo / 86400);
                return  (int) period == 1 ? "1 day ago" : (int) period + " days ago";
            }else{
                if(calendar.get(Calendar.YEAR) == currentCalendar.get(Calendar.YEAR)){
                    return getSimpleDateFormat("MMMM dd").format(calendar.getTime());
                }else{
                    return getSimpleDateFormat("MMMM dd yyyy").format(calendar.getTime());
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return "--";
        }
    }

    public static SimpleDateFormat getSimpleDateFormat(String format){
        return new SimpleDateFormat(format);
    }
}
