package com.thingsilikeapp.vendor.android.java;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

public class CustonParentLayout extends LinearLayout {
    private boolean mDisableChildrenTouchEvents;

    public CustonParentLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mDisableChildrenTouchEvents = false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mDisableChildrenTouchEvents;
    }

    public void setDisableChildrenTouchEvents(boolean flag) {
        mDisableChildrenTouchEvents = flag;
    }
}
