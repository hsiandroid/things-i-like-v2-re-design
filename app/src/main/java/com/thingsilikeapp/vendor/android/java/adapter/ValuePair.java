package com.thingsilikeapp.vendor.android.java.adapter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jomar Olaybal on 6/25/2016.
 */
public class ValuePair implements Parcelable {
    private String value;
    private String title;

    public ValuePair(String value, String title){
        this.value = value;
        this.title = title;
    }

    public String getValue(){
        return value;
    }

    public String getTitle(){
        return title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.value);
        dest.writeString(this.title);
    }

    protected ValuePair(Parcel in) {
        this.value = in.readString();
        this.title = in.readString();
    }

    public static final Parcelable.Creator<ValuePair> CREATOR = new Parcelable.Creator<ValuePair>() {
        @Override
        public ValuePair createFromParcel(Parcel source) {
            return new ValuePair(source);
        }

        @Override
        public ValuePair[] newArray(int size) {
            return new ValuePair[size];
        }
    };
}