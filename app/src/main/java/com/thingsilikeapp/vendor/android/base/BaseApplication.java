package com.thingsilikeapp.vendor.android.base;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.danikula.videocache.HttpProxyCacheServer;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.config.App;
import com.thingsilikeapp.vendor.android.base.RouteManager;
import com.thingsilikeapp.android.activity.LandingActivity;
import com.thingsilikeapp.vendor.android.java.FontsOverride;

import io.fabric.sdk.android.BuildConfig;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by BCTI 3 on 3/16/2017.
 */

public class BaseApplication extends Application {

    public static Context context;
    private HttpProxyCacheServer proxy;


    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);

        FacebookSdk.sdkInitialize(getApplicationContext());

        if(!getPackageName().contains("testapk")){

            Crashlytics crashlyticsKit = new Crashlytics.Builder()
                    .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                    .build();

            final Fabric fabric = new Fabric.Builder(this)
                    .kits(crashlyticsKit)
                    .debuggable(true)
                    .build();

            Fabric.with(fabric);
        }

        context = this;

        if(!App.debug){
            Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
            {
                @Override
                public void uncaughtException (Thread thread, Throwable e)
                {
                    facebookLogger("System Error" , e.getMessage());

                    Log.e("BaseApplication", ">>>" + e.getMessage());
                    System.gc();
                    UserData.insert(UserData.ERROR_COUNT, UserData.getInt(UserData.ERROR_COUNT) + 1);
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.mipmap.ic_launcher)
                                    .setContentTitle(getString(R.string.app_name))
                                    .setContentText("Things i like app restarted due to some error.");
                    NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    mNotifyMgr.notify(001, mBuilder.build());

                    RouteManager.Route.with(context)
                            .addActivityClass(LandingActivity.class)
                            .addActivityTag("registration")
                            .addFragmentTag("splash")
                            .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                    System.exit(1);
                }
            });
        }
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.typeface_paragraph))
                .setFontAttrId(R.attr.fontPath)
                .build());
//        FontsOverride.setDefaultFont(this, "monospace", getString(R.string.typeface_paragraph));
    }

    public void facebookLogger (String event,String message) {
        AppEventsLogger appEventsLogger = AppEventsLogger.newLogger(this);
        Bundle facebookLoggerBundle = new Bundle();
        if(UserData.isLogin()){
            facebookLoggerBundle.putInt("user_id", UserData.getUserItem().id);
            facebookLoggerBundle.putString("user_name", UserData.getUserItem().name);
            facebookLoggerBundle.putString("fb_id", UserData.getUserItem().fb_id);
        }

        facebookLoggerBundle.putString("message", message);
        appEventsLogger.logEvent(event, facebookLoggerBundle);
    }

    public static HttpProxyCacheServer getProxy(Context context) {
        BaseApplication app = (BaseApplication) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer(this);
    }

}
