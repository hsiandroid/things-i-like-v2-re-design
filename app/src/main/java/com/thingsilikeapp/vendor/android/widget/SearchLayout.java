package com.thingsilikeapp.vendor.android.widget;


import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by BCTI 3 on 9/21/2017.
 */

public class SearchLayout extends LinearLayout {

    private final ViewDragHelper mDragHelper;

    private View searchHeaderCON;
    private EditText searchBarET;
    private TextView searchBarTXT;
    private ImageView searchBTN;
    private View discoverBTN;
    private View discoverCategoryCON;
    private View caretBTN;
    private View searchResultCON;
    private View viewDesc;

    private float mInitialMotionX;
    private float mInitialMotionY;

    private int mTop;
    private float mDragOffset;
    private boolean hasKeyboard;
    private String hint;
    boolean isSearch = true;
    private Timer timer;
    private SearchListener searchListener;

    public SearchLayout(Context context) {
        this(context, null);
    }

    public SearchLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @Override
    protected void onFinishInflate() {
//        searchHeaderCON = findViewById(R.id.searchHeaderCON);
//        viewDesc = findViewById(R.id.viewDesc);
//        searchBarET = findViewById(R.id.searchBarET);
//        searchBarTXT = findViewById(R.id.searchBarTXT);
//        searchBTN = findViewById(R.id.searchBTN);
//
//        caretBTN = findViewById(R.id.caretBTN);
//        discoverBTN = findViewById(R.id.discoverBTN);
//        discoverCategoryCON = findViewById(R.id.discoverCategoryCON);
//        searchResultCON = findViewById(R.id.searchResultCON);

        initSearchET();
        super.onFinishInflate();
    }

    public SearchLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mDragHelper = ViewDragHelper.create(this, 1f, new DragHelperCallback());
    }

    @Override
    protected void onDetachedFromWindow() {
        if (timer != null) {
            timer.cancel();
        }
        super.onDetachedFromWindow();
    }

    private void initSearchET(){
//        discoverCategoryCON.setVisibility(searchBarET.isFocused() ? View.GONE : View.VISIBLE);
        searchBarTXT.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        searchBarTXT.setSelected(true);
        searchBarTXT.setSingleLine(true);

        hint = searchBarTXT.getText().toString();
        searchBarET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() > 0){
                    setActionErase();
                    searchBarTXT.setText("");
                }else{
                    setActionSearch();
                    searchBarTXT.setText(hint);
                }
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if(searchListener != null){
                            if(!searchBarET.getText().toString().equals("") || !searchBarET.getText().toString().equals(null)){
                                searchListener.searchTextChange(searchBarET.getText().toString());
                            }
                        }
                    }
                }, 500);
            }
        });

        searchBarET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchNow();
                    return true;
                }
                return false;
            }
        });
    }

    public void searchNow(){
        searchNow(searchBarET.getText().toString());
    }

    public void searchNow(String keyword){
        searchBarET.setText(keyword);
        if(searchListener != null){
            searchListener.searchAction(keyword);
        }
    }


    public interface SearchListener{
        void searchTextChange(String keyword);
        void searchAction(String keyword);
    }

    public void setSearchListener(SearchListener searchListener) {
        this.searchListener = searchListener;
    }

    boolean smoothSlideTo(float slideOffset) {
        final int topBound = getPaddingTop();
        int y = (int) (topBound + slideOffset * getDragRange());

        if (mDragHelper.smoothSlideViewTo(searchHeaderCON, searchHeaderCON.getLeft(), y)) {
            ViewCompat.postInvalidateOnAnimation(this);
            return true;
        }
        return false;
    }

    private class DragHelperCallback extends ViewDragHelper.Callback {

        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            return child == searchHeaderCON || child == viewDesc;
        }

        @Override
        public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {

            if (hasKeyboard && mTop < top && (getDragRange() * 0.30) > top) {
                showKeyboard(false);
            }

            if(!hasKeyboard && top == 0){
                showKeyboard(true);
                showCategory(false);
            }

            if (mTop > top && (getDragRange() * 0.30) > top) {
                showCategory(false);
            }

            mTop = top;
            mDragOffset = (float) top / getDragRange();
            requestLayout();
        }

        @Override
        public void onViewReleased(View releasedChild, float xvel, float yvel) {
            int top = getPaddingTop();
            if (yvel > 0 || (yvel == 0 && mDragOffset > 0.5f)) {
                top += getDragRange();
            }
            mDragHelper.settleCapturedViewAt(releasedChild.getLeft(), top);
        }

        @Override
        public int getViewVerticalDragRange(View child) {
            return getDragRange();
        }

        @Override
        public int clampViewPositionVertical(View child, int top, int dy) {
            final int topBound = getPaddingTop();
            final int bottomBound = getHeight() - searchHeaderCON.getHeight() - searchHeaderCON.getPaddingBottom();

            final int newTop = Math.min(Math.max(top, topBound), bottomBound);
            return newTop;
        }

    }

    @Override
    public void computeScroll() {
        if (mDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final int action = MotionEventCompat.getActionMasked(ev);

        if ((action != MotionEvent.ACTION_DOWN)) {
            mDragHelper.cancel();
            return super.onInterceptTouchEvent(ev);
        }

        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            mDragHelper.cancel();
            return false;
        }

        final float x = ev.getX();
        final float y = ev.getY();
        boolean interceptTap = false;

        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                mInitialMotionX = x;
                mInitialMotionY = y;
                interceptTap = mDragHelper.isViewUnder(searchHeaderCON, (int) x, (int) y);
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                final float adx = Math.abs(x - mInitialMotionX);
                final float ady = Math.abs(y - mInitialMotionY);
                final int slop = mDragHelper.getTouchSlop();
                if (ady > slop && adx > ady) {
                    mDragHelper.cancel();
                    return false;
                }
            }
        }

        return mDragHelper.shouldInterceptTouchEvent(ev) || interceptTap;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        mDragHelper.processTouchEvent(ev);

        final int action = ev.getAction();
        final float x = ev.getX();
        final float y = ev.getY();

        boolean isHeaderView = mDragHelper.isViewUnder(searchHeaderCON, (int) x, (int) y);

        switch (action & MotionEventCompat.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                mInitialMotionX = x;
                mInitialMotionY = y;
                break;
            }

            case MotionEvent.ACTION_UP: {
                final float dx = x - mInitialMotionX;
                final float dy = y - mInitialMotionY;
                final int slop = mDragHelper.getTouchSlop();

                boolean isSearchBarView = isViewHit(searchHeaderCON, (int) x, (int) y);

                if (isViewHit(discoverBTN, (int) x, (int) y)) {
                    discoverClicked();
                    return true;
                }else if (isViewHit(searchBTN, (int) x, (int) y)) {
                    searchClicked();
                    return true;
                }else if (isMaximized() && isSearchBarView) {
                    showKeyboard(true);
                    return true;
                }else if (dx * dx + dy * dy < slop * slop && isSearchBarView) {
                    if(searchBarET.isEnabled()){
                        if (mDragOffset == 0) {
                            minimize();
                        } else {
                            maximize();
                        }
                    }else{
                        discoverClicked();
                    }
                }

                break;
            }
        }


        return isHeaderView;
    }

    public void discoverClicked() {
        int y = getDragRange();
        if (!discoverCategoryCON.isShown()) {
            y = getDragRange() - discoverCategoryCON.getHeight();
        }

        showCategory(!discoverCategoryCON.isShown());
        if (mDragHelper.smoothSlideViewTo(searchHeaderCON, searchHeaderCON.getLeft(), y)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    private void showCategory(boolean show) {
//        if(isMaximized()){
//            caretBTN.setRotation(0);
//        }else{
//            caretBTN.setRotation(show ? 0 : 180);
//        }
        caretBTN.setRotation(show ? 0 : 180);
        discoverCategoryCON.setVisibility(show ? VISIBLE : GONE);
        searchResultCON.setVisibility(show ? GONE : VISIBLE);
    }

    public void searchClicked() {
        if (isSearch && !isMaximized()) {
            maximize();
        } else {
            searchBarET.setText("");
            searchBarET.setEnabled(true);
            setActionSearch();
        }
    }

    public void maximize() {
        showCategory(false);
        smoothSlideTo(0f);
    }

    public void minimize() {
        searchHeaderCON.post(new Runnable() {
            @Override
            public void run() {
                smoothSlideTo(1f);
                showCategory(false);
            }
        });
    }

    public void minimizeOnScroll(){
        if(!isMinimized() && !isMaximized()){
            minimize();
        }
    }


    public boolean isMaximized() {
        return mTop == 0;
    }

    public boolean isMinimized(){
        return mTop == getDragRange();
    }

    public void setActionErase() {
        isSearch = false;
        searchBTN.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_close_btn2_blue));
//        searchBarET.setEnabled(false);
    }

    public void setActionSearch() {
        isSearch = true;
        searchBTN.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.menu_search));
        searchBarET.setEnabled(true);
    }

    private void showKeyboard(boolean show) {
        hasKeyboard = show;
        if (show) {
            Keyboard.showKeyboard(getContext(), searchBarET);
        } else {
            searchBarET.clearFocus();
            Keyboard.hideKeyboard(getContext());
        }
    }

    private boolean isViewHit(View view, int x, int y) {
        int[] viewLocation = new int[2];
        view.getLocationOnScreen(viewLocation);
        int[] parentLocation = new int[2];
        this.getLocationOnScreen(parentLocation);
        int screenX = parentLocation[0] + x;
        int screenY = parentLocation[1] + y;
        return screenX >= viewLocation[0] && screenX < viewLocation[0] + view.getWidth() &&
                screenY >= viewLocation[1] && screenY < viewLocation[1] + view.getHeight();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureChildren(widthMeasureSpec, heightMeasureSpec);

        int maxWidth = MeasureSpec.getSize(widthMeasureSpec);
        int maxHeight = MeasureSpec.getSize(heightMeasureSpec);

        setMeasuredDimension(resolveSizeAndState(maxWidth, widthMeasureSpec, 0),
                resolveSizeAndState(maxHeight, heightMeasureSpec, 0));
    }

    public int getDragRange(){
        return getScreenHeight() - getStatusBarHeight() - getResources().getDimensionPixelSize(R.dimen.search_header_height);
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        searchHeaderCON.layout(
                0,
                mTop,
                r,
                mTop + searchHeaderCON.getMeasuredHeight());

        viewDesc.layout(
                0,
                mTop + searchHeaderCON.getMeasuredHeight(),
                r,
                mTop + b);
    }

    public boolean onBackPressed(){

        if(!isMinimized() && isShown()){
            minimize();
            return true;
        }
        return false;
    }
}