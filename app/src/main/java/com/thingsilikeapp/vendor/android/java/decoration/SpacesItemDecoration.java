package com.thingsilikeapp.vendor.android.java.decoration;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class SpacesItemDecoration  extends RecyclerView.ItemDecoration {
//    private final int mSpace;
    private final int left;
    private final int right;
    private final int bottom;
    private final int top;

    public SpacesItemDecoration(int space) {
//        this.mSpace = space;
        this.left = space;
        this.right = space;
        this.bottom = space;
        this.top = space;
    }

    public SpacesItemDecoration(int space, int bottom) {
//        this.mSpace = space;
        this.left = space;
        this.right = space;
        this.bottom = bottom;
        this.top = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = (int) (left / 1.5);
        outRect.right = (int) (right / 1.5);
        outRect.bottom = (int) (bottom / 1.5);
        outRect.top = (int) (top  / 1.5);

//        int position = parent.getChildAdapterPosition(view);
//        // hide the divider for the last child
//        if (position == parent.getAdapter().getItemCount() - 1) {
//            outRect.setEmpty();
//        } else {
//            super.getItemOffsets(outRect, view, parent, state);
//        }
        // Add top margin only for the first item to avoid double space between items
//        if (parent.getChildAdapterPosition(view) == 0 || parent.getChildAdapterPosition(view) == 1)
//            outRect.top = mSpace;
    }
}