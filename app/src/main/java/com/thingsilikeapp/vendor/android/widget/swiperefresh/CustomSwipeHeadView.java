package com.thingsilikeapp.vendor.android.widget.swiperefresh;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thingsilikeapp.R;

/**
 * Created by BCTI 3 on 4/11/2017.
 */

public class CustomSwipeHeadView extends LinearLayout implements CustomSwipeRefreshLayout.CustomSwipeRefreshHeadLayout {

    private LinearLayout mContainer;

    private TextView messageTXT;
    private ImageView logoIV;
    private Context context;

    public CustomSwipeHeadView(Context context) {
        super(context);
        this.context = context;
        setWillNotDraw(false);
        setupLayout();
    }

    public void setupLayout() {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mContainer = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.item_custom_swipe, null);
        addView(mContainer, lp);
        setGravity(Gravity.BOTTOM);

        messageTXT = findViewById(R.id.messageTXT);
        logoIV = findViewById(R.id.logoIV);
    }

    @Override
    public void onStateChange(CustomSwipeRefreshLayout.State state, CustomSwipeRefreshLayout.State lastState) {
        int stateCode = state.getRefreshState();
        int lastStateCode = lastState.getRefreshState();
        if (stateCode == lastStateCode) {
            return;
        }

//        switch (stateCode) {
//            case CustomSwipeRefreshLayout.State.STATE_NORMAL:
//                messageTXT.setText("Swipe down to refresh.");
//                messageTXT.setVisibility(VISIBLE);
//                logoIV.setImageDrawable(ActivityCompat.getDrawable(context, R.drawable.app_logo_small));
//                break;
//            case CustomSwipeRefreshLayout.State.STATE_READY:
//                if (lastStateCode != CustomSwipeRefreshLayout.State.STATE_READY) {
//                    messageTXT.setText("Release now!");
//                    messageTXT.setVisibility(VISIBLE);
//                    logoIV.setImageDrawable(ActivityCompat.getDrawable(context, R.drawable.app_logo_small));
//                }
//                break;
//            case CustomSwipeRefreshLayout.State.STATE_REFRESHING:
//                messageTXT.setText("Refreshing");
//                messageTXT.setVisibility(INVISIBLE);
//                Glide.with(context).load(R.raw.moving_cat).into(logoIV);
//                break;
//
//            case CustomSwipeRefreshLayout.State.STATE_COMPLETE:
//                messageTXT.setText("Success!");
//                messageTXT.setVisibility(INVISIBLE);
//                break;
//            default:
//        }
    }

}