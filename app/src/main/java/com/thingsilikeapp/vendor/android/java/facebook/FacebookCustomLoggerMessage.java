package com.thingsilikeapp.vendor.android.java.facebook;

/**
 * Created by BCTI 3 on 7/13/2017.
 */

public class FacebookCustomLoggerMessage {
    public static final String FACEBOOK_LOGIN = "Facebook Login";
    public static final String EMAIL_LOGIN = "Email Login";
    public static final String SIGN_UP = "New Sign up";
    public static final String LOGOUT = "User Logout";

    public static class MainActivity{
        public static final String EVENT = "MainActivity";
        public static final String CREATE_POST = "Create Post button was clicked!";
        public static final String PROFILE = "Profile button was clicked!";
        public static final String NOTIFICATION = "Notification button was clicked!";
        public static final String SETTINGS = "Settings button was clicked!";
        public static final String SOCIAL_FEED = "Social Feed button was clicked!";
        public static final String SEARCH = "Search button was clicked!";
        public static final String EDIT = "Search button was clicked!";
        public static final String CHANGE_PASSWORD = "Change Password was clicked!";
        public static final String SHARE = "Share App button was clicked!";
        public static final String ABOUT = "About the App button was clicked!";
        public static final String OPEN_LOGOUT = "Open Log Out button was clicked!";
        public static final String REQUEST = "Request button was clicked!";
        public static final String ACTIVITY = "Activity button was clicked!";
    }

    public static  class ItemActivity{
        public static final String EVENT = "ItemActivity";
        public static final String SEND_GIFT = "Open Send Gift Dialog button was clicked!";
        public static final String REQUEST_ADDRESS = "Request Address button was clicked!";
        public static final String BUY_ITEM = "Buy Item button was clicked!";
        public static final String AVATAR = "User Avatar was clicked!";
        public static final String DELETE_WISHLIST = "Delete wish list button was clicked!";
        public static final String EDIT_WISHLIST = "Edit wish list button was clicked!";
        public static final String EDIT_ADRESSS = "Edit address button was clicked!";
        public static final String GRAB = "Grab button was clicked!";
        public static final String FOLLOW = "Follow unfollow button was clicked!";
        public static final String OWNER = "Owner Avatar was clicked!";
        public static final String SENDER = "Sender Avatar was clicked!";
        public static final String MARK_ALL = "Mark all as read button was clicked!";
        public static final String OTHER_OPTION = "Other option button was clicked!";
        public static final String UNRECEIVED_GIFT = "Not yet received gift button was clicked!";
        public static final String CANCEL_REQUEST = "Cancel request button was clicked!";
        public static final String DENY_REQUEST = "Deny information request button was clicked!";
    }

    public static class ProfileActivity{
        public static final String EVENT = "ProfileActivity";
        public static final String FOLLOWERS = "List of followers button was clicked!";
        public static final String FOLLOWING = "List of following button was clicked!";
        public static final String SOCIAL_FEED = "Social Feed button was clicked!";
        public static final String I_LIKE = "I like button was clicked!";
        public static final String I_GAVE = "I gave button was clicked!";
        public static final String I_RECEIVE = "I receive button was clicked!";
        public static final String OTHER_OPTION = "Other option button was clicked!";
        public static final String VIEW_PHOTO = "View photo button was clicked!";
    }

    public static class SettingsActivity{
        public static final String EVENT = "SettingsActivity";
        public static final String TERMS = "Terms and conditions button was clicked!";
        public static final String POLICY = "Privacy Policy button was clicked!";
        public static final String CHANGE_PASSWORD = "Change password button was clicked!";
        public static final String UPDATE_PROFILE = "Update profile button was clicked!";
        public static final String UPDATE_AVATAR= "Update avatar button was clicked!";
        public static final String EDIT_PROFILE = "Edit profile button was clicked!";
        public static final String ABOUT = "About the app button was clicked!";
    }

    public static class WishlistActivity{
        public static final String EVENT = "WishlistActivity";
        public static final String CREATE_POST = "Create Post button was clicked!";
        public static final String UPDATE_POST = "Update Post button was clicked!";
        public static final String CATEGORY = "Select category button was clicked!";
        public static final String COUNTRY = "Select country button was clicked!";
        public static final String PICK_IMAGE = "Pick image button was clicked!";
        public static final String CLOSE_IMAGE = "Close image button was clicked!";
    }

    public static class SearchActivity{
        public static final String EVENT = "SearchActivity";
        public static final String TOP_USER = "Open Top User button was clicked!";
        public static final String FB_FRIENDS = "Open Top User button was clicked!";
        public static final String SEARCH_RESULT = "Search result button was clicked!";
    }
}
