package com.thingsilikeapp.vendor.android.java;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.thingsilikeapp.vendor.android.base.BaseActivity;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Jomar Olaybal on 10/13/2017.
 */

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder holder;
    private Camera camera;
    private Context context;
    private int cameraID = -1;
    private List<Camera.Size> supportedPreviewSizes;
    private Camera.Size previewSize;

    public CameraPreview(Context context) {
        super(context);
        this.context = context;
        startPreview();
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if(camera != null){
            try {
                Camera.Parameters parameters = camera.getParameters();
                if (parameters != null) {
                    parameters.setPreviewSize(previewSize.width, previewSize.height);
                    parameters.setPictureSize(previewSize.width, previewSize.height);
                    camera.setPreviewDisplay(holder);
                    setCameraDisplayOrientation(cameraID, camera);
                    camera.setParameters(parameters);
                    camera.startPreview();
                }
            } catch (IOException e) {
                Log.d("CameraPreview", "Error setting camera preview: " + e.getMessage());
            }catch (RuntimeException e) {

            }
        }
    }

//    public void surfaceCreated(SurfaceHolder holder) {
//        if(camera != null){
//            try {
//                camera.setPreviewDisplay(holder);
//                setCameraDisplayOrientation(cameraID, camera);
//                camera.startPreview();
//            } catch (IOException e) {
//                Log.d("CameraPreview", "Error setting camera preview: " + e.getMessage());
//            }catch (RuntimeException e) {
//
//            }
//        }
//    }



    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        if (holder.getSurface() == null) {

            return;
        }
        try {
            camera.stopPreview();
        } catch (Exception e) {

        }

        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();

        } catch (Exception e) {
            Log.d("CameraPreview", "Error starting camera preview: " + e.getMessage());
        }
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio=(double)h / w;

        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    private Camera.Size getBestPreviewSize(int width, int height, Camera.Parameters parameters){
        Camera.Size bestSize = null;
        List<Camera.Size> sizeList = parameters.getSupportedPreviewSizes();

        bestSize = sizeList.get(0);

        for(int i = 1; i < sizeList.size(); i++){
            if((sizeList.get(i).width * sizeList.get(i).height) >
                    (bestSize.width * bestSize.height)){
                bestSize = sizeList.get(i);
            }
        }

        return bestSize;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        setMeasuredDimension(width, height);

        if (supportedPreviewSizes != null) {
            previewSize = getOptimalPreviewSize(supportedPreviewSizes, width, height);
        }
    }

    public void setCameraDisplayOrientation(int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = ((BaseActivity) context).getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    private Camera openFrontFacingCameraGingerbread() {
        int cameraCount = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    cam = Camera.open(camIdx);
                    cameraID = camIdx;
                    Log.e("camera id", ">>>" + cameraID);
                } catch (RuntimeException e) {
                    Log.e("CameraPreview", "Camera failed to open: " + e.getLocalizedMessage());
                }
            }
        }

        if (cam == null) {
            cam = getCameraInstance();
        }

        return cam;
    }

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    public void startPreview() {
        if (camera != null) {
            camera.startPreview();
        }else{
            camera = openFrontFacingCameraGingerbread();
            if (camera != null) {
                supportedPreviewSizes = camera.getParameters().getSupportedPreviewSizes();
            }
            holder = getHolder();
            holder.addCallback(this);
            holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
    }

    public void stopPreviewAndFreeCamera() {

        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    public void stopPreview() {
        if (camera != null) {
            camera.stopPreview();
        }
    }

    public void saveBitmap(final ImageView imageView, final FrameLayout cameraFL) {
        final ProgressDialog progressDialog = new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false);
        Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                stopPreview();
                progressDialog.setMessage("Processing image quality");
                if (data != null) {
                    try {
                        int screenWidth = getResources().getDisplayMetrics().widthPixels;
                        int screenHeight = getResources().getDisplayMetrics().heightPixels;
                        Bitmap bm = BitmapFactory.decodeByteArray(data, 0, (data != null) ? data.length : 0);
                        Camera.CameraInfo info = new Camera.CameraInfo();
                        Camera.getCameraInfo(cameraID, info);
                        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                            Matrix mtx = new Matrix();
                            mtx.postRotate(90);
                            mtx.postScale(-1.0f, 1.0f);
                            if (cameraID == Camera.CameraInfo.CAMERA_FACING_FRONT){
                                mtx.postRotate(180);
                            }
                            bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), mtx, true);
                        } else {
                            Bitmap scaled = Bitmap.createScaledBitmap(bm,
                                    screenWidth, screenHeight, true);
                            bm = scaled;
                        }

                        ImageQualityManager.getFileFromBitmap(getContext(), bm, new ImageQualityManager.Callback() {
                            @Override
                            public void success(File file) {
                                progressDialog.cancel();
                                Glide.with(getContext())
                                        .load(file)
//                                        .listener(new RequestListener<File, GlideDrawable>(){
//
//                                            @Override
//                                            public boolean onException(Exception e, File model, Target<GlideDrawable> target, boolean isFirstResource) {
//                                                return false;
//                                            }
//
//                                            @Override
//                                            public boolean onResourceReady(GlideDrawable resource, File model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                                                cameraFL.setVisibility(View.GONE);
//                                                imageView.setVisibility(View.VISIBLE);
//                                                Log.e("No Error", ">>>");
//                                                return false;
//                                            }
//                                        })
                                        .listener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                cameraFL.setVisibility(View.GONE);
                                                imageView.setVisibility(View.VISIBLE);
                                                Log.e("No Error", ">>>");
                                                return false;
                                            }
                                        })
                                        .into(imageView);

                            }
                        });
                    } catch (Exception e) {
                        progressDialog.cancel();
                        Log.e("Error", ">>>Exception", e);
                    } catch (Error e) {
                        Log.e("Error", ">>>Error", e);
                        progressDialog.cancel();
                    }
                }else{
                    Log.e("Error", ">>>data null");
                    progressDialog.cancel();
                }
            }
        };
        camera.takePicture(null, null, pictureCallback);

//        cameraFL.setVisibility(View.GONE);
//        imageView.setVisibility(View.VISIBLE);
//        imageView.setImageBitmap(getBitmap());
    }
}
