package com.thingsilikeapp.vendor.android.java;

import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.java.StringFormatter;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class GemHelper {

    public static String getCurrentGem(){
        return formatGem(UserData.getRewardCrystal());
    }

    public static String formatGem(String number){
        return formatGem(new BigDecimal(number));
    }

    public static String formatGem(BigDecimal number){
        return new DecimalFormat("0.00").format(number);
    }

    public static double parseDouble(String number){
        if(StringFormatter.isEmpty(number)){
            return 0;
        }

        if(number.equalsIgnoreCase(".")){
            return 0;
        }

        return Double.parseDouble(replaceChar(number));
    }

    public static String replaceChar(String number){
        return number.replaceAll("[*a-zA-Z]", "").replaceAll(" ", "");
    }
}
