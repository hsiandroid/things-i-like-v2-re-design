package com.thingsilikeapp.vendor.android.java;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

import uk.co.deanwild.materialshowcaseview.IShowcaseListener;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class WalkThrough {

    public static final String CREATE_POST = "create_post";
    public static final String FRIENDS = "friends";
    public static final String PROFILE = "profile";
    public static final String POST = "post";
    public static final String LIKE = "like";
    public static final String SHARE = "share";
    public static final String FOLLOW = "follow";
    public static final String PROFILE_AVATAR = "profile_avatar";
    public static final String GEMS = "gems";
    public static final String CHIPS = "chips";
    public static final String GEMS_FINAL = "gems_final";
    public static final String CAMERA = "camera";
    public static final String DESCRIPTION = "description";
    public static final String CATEGORY = "category";

    public static void createPost(Context context, View view, IShowcaseListener iShowcaseListener) {
//        if (UserData.getInt(UserData.CREATE_POST_SHOW) == 1 && !UserData.getBoolean(UserData.WALKTHRU_CREATE_DONE)) {
//            new MaterialShowcaseView.Builder(getActivity(context))
//                    .setTarget(view)
//                    .setMaskColour(ActivityCompat.getColor(context, R.color.walkthrough_bg1))
//                    .setTargetTouchable(true)
//                    .setShapePadding(-25)
//                    .setDismissOnTargetTouch(true)
//                    .setListener(iShowcaseListener)
//                    //                    .singleUse(CREATE_POST)
//                    .setContentTextColor(ActivityCompat.getColor(context, R.color.white_dim))
//                    .setContentText("Let's start by creating your first post \n Tap the [ + ] to proceed")
//                    .show();
//        }
    }

    public static void friends(Context context, View view, IShowcaseListener iShowcaseListener) {
//        new MaterialShowcaseView.Builder(getActivity(context))
//                .setTarget(view)
//                .setMaskColour(ActivityCompat.getColor(context, R.color.walkthrough_bg1))
//                .setTargetTouchable(true)
//                .setShapePadding(-25)
//                .setDismissOnTargetTouch(true)
//                .singleUse(FRIENDS)
//                .setContentTextColor(ActivityCompat.getColor(context, R.color.white_dim))
//                .setContentText("Search for your friends on Things I Like.")
//                .show();
    }

    public static void profile(Context context, View view, IShowcaseListener iShowcaseListener) {
//        new MaterialShowcaseView.Builder(getActivity(context))
//                .setTarget(view)
//                .setMaskColour(ActivityCompat.getColor(context, R.color.walkthrough_bg1))
//                .setTargetTouchable(true)
//                .setShapePadding(-25)
//                .setDismissOnTargetTouch(true)
//                .singleUse(PROFILE)
//                .setContentTextColor(ActivityCompat.getColor(context, R.color.white_dim))
//                .setContentText("Now let's go to your profile")
//                .show();
    }

    public static void post(Context context, View view, IShowcaseListener iShowcaseListener) {
//        if (UserData.getInt(UserData.POST_SHOW) == 1) {
//            UserData.insert(UserData.POST_SHOW, 2);
//            new MaterialShowcaseView.Builder((RouteActivity) context)
//                    .setTarget(view)
//                    .setMaskColour(ActivityCompat.getColor(context, R.color.walkthrough_bg))
//                    .setTargetTouchable(true)
//                    .setDismissOnTargetTouch(true)
//                    .setShapePadding(0)
//                    .setListener(iShowcaseListener)
//                    .withRectangleShape()
//                    .singleUse(POST)
//                    .setContentTextColor(ActivityCompat.getColor(context, R.color.white_dim))
//                    .setContentText("You can like, comment or repost items.")
//                    .show();
//        }
    }

    public static void like(Context context, View view, IShowcaseListener iShowcaseListener) {
//        if (UserData.getInt(UserData.LIKE_POST_SHOW) == 1) {
//            new MaterialShowcaseView.Builder((RouteActivity) context)
//                    .setTarget(view)
//                    .setMaskColour(ActivityCompat.getColor(context, R.color.walkthrough_bg))
//                    .setTargetTouchable(true)
//                    .setDismissOnTargetTouch(true)
//                    .setShapePadding(5)
//                    .setListener(iShowcaseListener)
//                    .singleUse(LIKE)
//                    .setContentTextColor(ActivityCompat.getColor(context, R.color.white_dim))
//                    .setContentText("Lyka Gems are earned when you reach 100% on your Lyka meter. \n\n " +
//                            "Try to press Like on this post...")
//                    .show();
//        }
    }

    public static void share(Context context, View view, IShowcaseListener iShowcaseListener) {
//        if (UserData.getInt(UserData.LIKE_POST_SHOW) == 1) {
//            if (UserData.getInt(UserData.SHARE_POST) == 1) {
//                UserData.insert(UserData.SHARE_POST, 2);
//                new MaterialShowcaseView.Builder((RouteActivity) context)
//                        .setTarget(view)
//                        .setMaskColour(ActivityCompat.getColor(context, R.color.walkthrough_bg))
//                        .setTargetTouchable(true)
//                        .setDismissOnTargetTouch(true)
//                        .setShapePadding(0)
//                        .setListener(iShowcaseListener)
//                        .withRectangleShape()
//                        .singleUse(SHARE)
//                        .setContentTextColor(ActivityCompat.getColor(context, R.color.white_dim))
//                        .setContentText("Now let’s try sharing your friend’s post. Open this post.")
//                        .show();
//            }
//        }
    }

    public static void follow(Context context, View view, IShowcaseListener iShowcaseListener) {
//        UserData.insert(UserData.PEOPLE_PROCESS, true);
//        new MaterialShowcaseView.Builder((RouteActivity) context)
//                .setTarget(view)
//                .setMaskColour(ActivityCompat.getColor(context, R.color.walkthrough_bg))
//                .setTargetTouchable(true)
//                .setDismissOnTargetTouch(true)
//                .setListener(iShowcaseListener)
//                .setShapePadding(0)
//                .withRectangleShape()
//                .singleUse(FOLLOW)
//                .setContentTextColor(ActivityCompat.getColor(context, R.color.white_dim))
//                .setContentText("Tap to follow friends.")
//                .show();
    }

    public static void profileAvatar(Context context, View view, IShowcaseListener iShowcaseListener) {
//        if (UserData.getBoolean(UserData.PEOPLE_PROCESS)) {
//            new MaterialShowcaseView.Builder((RouteActivity) context)
//                    .setTarget(view)
//                    .setMaskColour(ActivityCompat.getColor(context, R.color.walkthrough_bg))
//                    .setTargetTouchable(true)
//                    .setDismissOnTargetTouch(true)
//                    .setShapePadding(0)
//                    .setListener(iShowcaseListener)
//                    .singleUse(PROFILE_AVATAR)
//                    .setContentTextColor(ActivityCompat.getColor(context, R.color.white_dim))
//                    .setContentText("Tap your friend’s avatar to view their profile.")
//                    .show();
//        }
    }

    public static void gems(Context context, View view, IShowcaseListener iShowcaseListener) {
//        new MaterialShowcaseView.Builder(getActivity(context))
//                .setTarget(view)
//                .setMaskColour(ActivityCompat.getColor(context, R.color.walkthrough_bg1))
//                .setTargetTouchable(true)
//                .setDismissOnTargetTouch(true)
//                .setListener(iShowcaseListener)
//                .withRectangleShape()
//                .singleUse(GEMS)
//                .setContentTextColor(ActivityCompat.getColor(context, R.color.white_dim))
//                .setContentText("Awesomeness!\n" +
//                        "Earn Lyka Gem every time you use the app.\n")
//                .show();
    }

    public static void chips(Context context, View view, IShowcaseListener iShowcaseListener) {
//        new MaterialShowcaseView.Builder(getActivity(context))
//                .setTarget(view)
//                .setMaskColour(ActivityCompat.getColor(context, R.color.walkthrough_bg1))
//                .setTargetTouchable(true)
//                .setShapePadding(5)
//                .setDismissOnTargetTouch(true)
//                .setListener(iShowcaseListener)
//                .singleUse(CHIPS)
//                .setContentTextColor(ActivityCompat.getColor(context, R.color.white_dim))
//                .setContentText("Look, a percentage has been added to your Lyka Meter, which means you have just taken the first step in collecting Lyka Gems")
//                .show();
    }

    public static void gemsFinal(Context context, View view, IShowcaseListener iShowcaseListener) {
//        if (view != null){
//            new MaterialShowcaseView.Builder(getActivity(context))
//                    .setTarget(view)
//                    .setMaskColour(ActivityCompat.getColor(context, R.color.walkthrough_bg1))
//                    .setTargetTouchable(true)
//                    //                .setDismissOnTouch(true)
//                    .setDismissOnTargetTouch(true)
//                    //                    .setShapePadding(-50)
//                    .withRectangleShape()
//                    .setListener(iShowcaseListener)
//                    .singleUse(GEMS_FINAL)
//                    .setContentTextColor(ActivityCompat.getColor(context, R.color.white_dim))
//                    .setContentText("Tap this section to see what products you can redeem with your Lyka Gems.")
//                    .show();
//        }else {
//            ((RouteActivity) context).startRewardsActivity("shop");
//        }
    }

    public static BaseActivity getActivity(Context context) {
        return (BaseActivity) context;
    }
}