package com.thingsilikeapp.vendor.android.java;

import android.Manifest;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseActivity;

import java.io.File;
import java.util.Date;


public class DownloadFileManager {

    private Context context;
    private DownloadManager downloadManager;
    private BroadcastReceiver onComplete;
    private final int STORAGE_PERMISSION = 111;
    private long downloadReferenceId;
    public String urlFile;
    private DownloadCallback downloadCallback;
    private String fileName;

    public static DownloadFileManager newInstance(Context context, String fileName, DownloadCallback downloadCallback) {
        DownloadFileManager fragment = new DownloadFileManager();
        fragment.context = context;
        fragment.urlFile = "/";
        fragment.downloadCallback = downloadCallback;
        fragment.fileName = fileName;
        fragment.downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        return fragment;
    }

    public  void downloadWithPermissionGranted(String urlFile) {
        if(PermissionChecker.checkPermissions(context, Manifest.permission.WRITE_EXTERNAL_STORAGE, STORAGE_PERMISSION)){
            downloadFile(urlFile);
        }else{
            this.urlFile = urlFile;
        }
    }

    public Context getContext() {
        return context;
    }

    private void downloadFile(String urlFile){
        this.urlFile = urlFile;
        final ProgressDialog progressDialog = new ProgressDialog(getContext()).show(getContext(), "", "Saving file...", true, true);
        onComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if(DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    getContext().startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));

                    if(downloadCallback != null){
                        downloadCallback.onFileDownloadCompleted();
                    }
                    if (progressDialog != null){
                        progressDialog.dismiss();
                    }
                }
            }
        };

        getContext().registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        Uri downloadUri = Uri.parse(urlFile);
        String downloadName = fileName;
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle("Downloading " + downloadName);
        request.setDescription("Downloading " + downloadName);
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,  downloadName);
        request.addRequestHeader("Authorization", "Bearer" + UserData.getString(UserData.AUTHORIZATION));
        request.addRequestHeader("Content-Type", "application/pdf");
//        request.addRequestHeader("Content-Disposition", "attachment; filename=\"fast-and-the-furios.apk\"");
        downloadReferenceId = downloadManager.enqueue(request);
    }

    public String getMimeType(Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(getContext().getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }


    private void defaultNotificationBuilder(Intent intent) {

        PendingIntent pendingIntent = PendingIntent.getActivity
                (getContext(), 1 /* Request code */, intent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getContext())
                .setSmallIcon(R.drawable.logo_blue_new)
                .setColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary))
                .setContentTitle(getContext().getString(R.string.app_name))
                .setContentText("Download completed. File is at Downloads/" + fileName)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1 /* ID of notification */, notificationBuilder.build());
    }

    public static String generateRandomName(String fileType){
        return new Date().getTime() + fileType;
    }

    public interface DownloadCallback{
        void onFileDownloadCompleted();
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == STORAGE_PERMISSION){
            if(((BaseActivity) getContext()) .isAllPermissionResultGranted(grantResults)){
                downloadFile(fileName);
            }
        }
    }
}
