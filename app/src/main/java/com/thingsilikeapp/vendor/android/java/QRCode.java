package com.thingsilikeapp.vendor.android.java;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

/**
 * Created by BCTI 3 on 2/5/2018.
 */

public class QRCode {

    public static Bitmap encodeToQrCode(String text){
        return encodeToQrCode(100, text);
    }

    public static Bitmap encodeToQrCode(int size, String text){
        return encodeToQrCode(size, size, text);
    }

    public static Bitmap encodeToQrCode(int width, int height, String text){
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix matrix = null;
        try {
            matrix = writer.encode(text, BarcodeFormat.QR_CODE, width, height);
        } catch (WriterException ex) {
            ex.printStackTrace();
        }

        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bmp.setHasAlpha(true);
        for (int x = 0; x < width; x++){
            for (int y = 0; y < height; y++){
                bmp.setPixel(x, y, matrix.get(x,y) ? Color.BLACK : Color.WHITE);
            }
        }
        return bmp;
    }
}
