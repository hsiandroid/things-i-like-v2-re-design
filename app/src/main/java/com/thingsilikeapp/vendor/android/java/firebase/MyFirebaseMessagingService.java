/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.thingsilikeapp.vendor.android.java.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.Data;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.java.BadgeUtils;
import com.thingsilikeapp.vendor.android.java.pusher.Sound;
import com.thingsilikeapp.vendor.android.base.RouteManager;
import com.thingsilikeapp.android.activity.CommentActivity;
import com.thingsilikeapp.android.activity.ItemActivity;
import com.thingsilikeapp.android.activity.LandingActivity;
import com.thingsilikeapp.android.activity.MainActivity;
import com.thingsilikeapp.android.activity.ProfileActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FMService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "FCM From" + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "FCM Data" + remoteMessage.getData());

            FCMData fcmData = new FCMData(remoteMessage.getData());
            if (remoteMessage.getNotification() != null) {
                fcmData.title = remoteMessage.getNotification().getTitle();
                fcmData.body = remoteMessage.getNotification().getBody();
            }

            Log.e("counter_badge", ">>>" + fcmData.counter_badge);
            BadgeUtils.setBadge(getBaseContext(), fcmData.counter_badge);

            Data.setSharedPreferences(getBaseContext());
            if (UserData.isLogin()) {
                Bundle bundle;
                Intent intent;
                switch (fcmData.type) {
                    case "USER":
                        bundle = new Bundle();
                        bundle.putInt("user_id", fcmData.referenceID);
                        intent = RouteManager.Route.with(getBaseContext())
                                .addActivityClass(ProfileActivity.class)
                                .addActivityTag("profile")
                                .addFragmentTag("profile")
                                .addFragmentBundle(bundle)
                                .getIntent();
                        showNotification(intent, fcmData);
                        break;
                    case "WISHLIST_TRANSACTION":
                        if (fcmData.event.equalsIgnoreCase("App\\Laravel\\Notifications\\WishlistTransaction\\GiftSentNotification")) {
                            intent = RouteManager.Route.with(getBaseContext())
                                    .addActivityClass(MainActivity.class)
                                    .addActivityTag("main")
                                    .addFragmentTag("notification")
                                    .getIntent(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                            showNotification(intent, fcmData);
                        } else {
                            bundle = new Bundle();
                            bundle.putInt("things_i_like_id", fcmData.referenceID);
                            bundle.putInt("user_id", 0);
                            intent = RouteManager.Route.with(this)
                                    .addActivityClass(ItemActivity.class)
                                    .addActivityTag("item")
                                    .addFragmentTag("i_received")
                                    .addFragmentBundle(bundle)
                                    .getIntent();

                            showNotification(intent, fcmData);
                        }

                        break;
                    case "WISHLIST_LIKE":
                    case "WISHLIST":
                        bundle = new Bundle();
                        bundle.putInt("things_i_like_id", fcmData.referenceID);
                        bundle.putInt("user_id", 0);
                        intent = RouteManager.Route.with(this)
                                .addActivityClass(ItemActivity.class)
                                .addActivityTag("item")
                                .addFragmentTag("i_like")
                                .addFragmentBundle(bundle)
                                .getIntent();

                        showNotification(intent, fcmData);
                        break;
                    case "WISHLIST_VIEWER":
                        intent = RouteManager.Route.with(getBaseContext())
                                .addActivityClass(MainActivity.class)
                                .addActivityTag("main")
                                .addFragmentTag("notification")
                                .getIntent(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                        showNotification(intent, fcmData);
                        break;
                    case "COMMENT":
                        bundle = new Bundle();
                        bundle.putInt("wishListID", fcmData.referenceID);
                        bundle.putString("wishListName", "");

                        intent = RouteManager.Route.with(this)
                                .addActivityClass(CommentActivity.class)
                                .addActivityTag("item")
                                .addFragmentTag("comment")
                                .addFragmentBundle(bundle)
                                .getIntent();

                        showNotification(intent, fcmData);
                        break;
                    default:
                        showNotification(getDefaultIntent(fcmData), fcmData);

                }
            } else {
                showNotification(getDefaultIntent(fcmData), fcmData);
            }

        }
    }

    private Intent getDefaultIntent(FCMData fcmData) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("fcm_data", fcmData);
        return RouteManager.Route.with(getBaseContext())
                .addActivityClass(LandingActivity.class)
                .addActivityTag("splash")
                .addFragmentTag("splash")
                .addFragmentBundle(bundle)
                .getIntent(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
    }

//    @Override
//    public boolean zzH(Intent intent) {
//        if(intent.hasExtra("counter_badge"))
//        {
//            try
//            {
//                BadgeUtils.setBadge(getBaseContext(), Integer.parseInt(intent.getStringExtra("counter_badge")));
//            }
//            catch (Exception e)
//            {
//                Log.e("failedToParse", "Badge!?");
//            }
//        }
//        return super.zzH(intent);
//    }
    private void showNotification(Intent intent, FCMData fcmData){
        if(fcmData.displayType != null){
            switch (fcmData.displayType){
                case "big_image":
                    bigNotificationBuilder(intent, fcmData);
                    break;
                default:
                    defaultNotificationBuilder(intent, fcmData);
            }
        }else{
            defaultNotificationBuilder(intent, fcmData);
        }
    }


    private void defaultNotificationBuilder(Intent intent, FCMData fcmData) {

        PendingIntent pendingIntent = PendingIntent.getActivity
                (this, fcmData.referenceID /* Request code */, intent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getBaseContext())
                .setSmallIcon(R.drawable.ic_stat_app_logo_small_white)
                .setColor(ActivityCompat.getColor(getBaseContext(), R.color.colorPrimary))
                .setContentTitle(fcmData.title == null ? getString(R.string.app_name) : fcmData.title)
                .setContentText(fcmData.body)
                .setAutoCancel(true)
                .setNumber(fcmData.counter_badge)
                .setDefaults(Notification.DEFAULT_ALL)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .setBigContentTitle(fcmData.title == null ? getString(R.string.app_name) : fcmData.title)
                        .bigText(fcmData.body))
                .setContentIntent(pendingIntent);

        Sound.playNotification(this);

        NotificationManager notificationManager =
                (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(fcmData.referenceID /* ID of notification */, notificationBuilder.build());
    }


    private void bigNotificationBuilder(Intent intent, FCMData fcmData) {
        new generatePictureStyleNotification(intent, fcmData).execute();
    }

    public class generatePictureStyleNotification extends AsyncTask<String, Void, Bitmap> {

        private Intent intent;
        private FCMData fcmData;

        public generatePictureStyleNotification(Intent intent, FCMData fcmData) {
            super();
            this.intent = intent;
            this.fcmData = fcmData;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            InputStream in;
            try {
                URL url = new URL(this.fcmData.image);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                return myBitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            PendingIntent pendingIntent = PendingIntent.getActivity
                    (getBaseContext(), fcmData.referenceID /* Request code */, intent,
                            PendingIntent.FLAG_CANCEL_CURRENT);

            Sound.playNotification(MyFirebaseMessagingService.this);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getBaseContext())
                    .setSmallIcon(R.drawable.ic_stat_app_logo_small_white)
                    .setColor(ActivityCompat.getColor(getBaseContext(), R.color.colorPrimary))
                    .setContentTitle(fcmData.title == null ? getString(R.string.app_name) : fcmData.title)
                    .setContentText(fcmData.body)
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(result)
                            .setSummaryText(fcmData.body)
                            .setBigContentTitle(fcmData.title == null ? getString(R.string.app_name) : fcmData.title))
                    .setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                            + "://" + MyFirebaseMessagingService.this.getPackageName() + "/raw/cat1"))
                    .setNumber(fcmData.counter_badge)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(fcmData.referenceID /* ID of notification */, notificationBuilder.build());
        }
    }

    public static class FCMData implements Parcelable {

        public String title;
        public String body;
        public String type;
        public String image;
        public String event = "";
        public int referenceID;
        public int counter_badge;
        public String displayType = "";

        public FCMData(){

        }

        public FCMData(Map<String, String> data) {
            title = data.get("title");
            body = data.get("body");
            type = data.get("type");
            image = data.get("thumbnail");
            event = data.get("event");
            displayType = data.get("display_type");

            String id = data.get("reference_id");
            if (id == null && (id.equals("") || id.equals("null"))) {
                id = "0";
            }
            referenceID = Integer.parseInt(id);

            String counter = data.get("counter_badge");
            if (counter == null) {
                counter = "0";
            }

            if((counter.equals("") || counter.equals("null"))){
                counter = "0";
            }
            counter_badge = Integer.parseInt(counter);
        }

        public FCMData(Bundle data) {
            title = data.getString("title");
            body = data.getString("body");
            type = data.getString("type");
            event = data.getString("event");
            image = data.getString("thumbnail");
            displayType = data.getString("display_type");
            String id = data.getString("reference_id");

            if (id == null && (id.equals("") || id.equals("null"))) {
                id = "0";
            }

            referenceID = Integer.parseInt(id);
        }

        protected FCMData(Parcel in) {
            title = in.readString();
            body = in.readString();
            type = in.readString();
            image = in.readString();
            event = in.readString();
            referenceID = in.readInt();
            counter_badge = in.readInt();
            displayType = in.readString();
        }

        public static final Creator<FCMData> CREATOR = new Creator<FCMData>() {
            @Override
            public FCMData createFromParcel(Parcel in) {
                return new FCMData(in);
            }

            @Override
            public FCMData[] newArray(int size) {
                return new FCMData[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(title);
            parcel.writeString(body);
            parcel.writeString(type);
            parcel.writeString(image);
            parcel.writeString(event);
            parcel.writeInt(referenceID);
            parcel.writeInt(counter_badge);
            parcel.writeString(displayType);
        }
    }
}
