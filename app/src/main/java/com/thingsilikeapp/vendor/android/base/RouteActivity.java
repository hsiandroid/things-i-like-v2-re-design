package com.thingsilikeapp.vendor.android.base;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.gson.Gson;
import com.thingsilikeapp.android.activity.CreatePostActivity;
import com.thingsilikeapp.android.activity.DiscoverActivity;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.activity.ExploreActivity;
import com.thingsilikeapp.android.activity.GemActivity;
import com.thingsilikeapp.android.activity.MessageActivity;
import com.thingsilikeapp.android.activity.NewRegistrationActivity;
import com.thingsilikeapp.android.activity.VerificationActivity;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.vendor.server.util.InvalidToken;
import com.thingsilikeapp.vendor.android.java.firebase.MyFirebaseMessagingService;
import com.thingsilikeapp.android.activity.AccountActivity;
import com.thingsilikeapp.android.activity.AddressBookActivity;
import com.thingsilikeapp.android.activity.BirthdayActivity;
import com.thingsilikeapp.android.activity.BirthdaySurpriseActivity;
import com.thingsilikeapp.android.activity.CommentActivity;
import com.thingsilikeapp.android.activity.ImageEditorActivity;
import com.thingsilikeapp.android.activity.ItemActivity;
import com.thingsilikeapp.android.activity.LandingActivity;
import com.thingsilikeapp.android.activity.MainActivity;
import com.thingsilikeapp.android.activity.ProfileActivity;
import com.thingsilikeapp.android.activity.RegistrationActivity;
import com.thingsilikeapp.android.activity.RequestActivity;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.activity.SearchActivity;
import com.thingsilikeapp.android.activity.SettingsActivity;
import com.thingsilikeapp.android.activity.WishListActivity;

import com.thingsilikeapp.android.activity.iGaveReceivedActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by BCTI 3 on 2/6/2017.
 */

public class RouteActivity extends BaseActivity {

        public static final int GEM_CODE = 1000;
        public static final int FRIEND_CODE = 2000;

    public void startMainActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }

    public void startMainActivity(String fragmentTAG, String content) {
        Bundle bundle = new Bundle();
        bundle.putString("post_content", content);
        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag(fragmentTAG)
                .addActivityBundle(bundle)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    public void startMainActivity(String fragmentTAG, Uri uri) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("image_uri", uri);
        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag(fragmentTAG)
                .addActivityBundle(bundle)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    public void startMainActivity(String fragmentTAG, MyFirebaseMessagingService.FCMData fcmData) {

        Bundle bundle = new Bundle();
        bundle.putParcelable("fcm_data", fcmData);

        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag(fragmentTAG)
                .addActivityBundle(bundle)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                .startActivity();

//        finish();
    }

    public void startLandingActivity() {
        RouteManager.Route.with(this)
                .addActivityClass(LandingActivity.class)
                .addActivityTag("registration")
                .addFragmentTag("splash")
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    public void startItemActivity(int id, int userId, String fragmentTAG) {
        Bundle bundle = new Bundle();
        bundle.putInt("things_i_like_id", id);
        bundle.putInt("user_id", userId);
        RouteManager.Route.with(this)
                .addActivityClass(ItemActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startProfileActivity(int id) {
        if (UserData.getUserItem().id == id) {
            Bundle bundle = new Bundle();
            bundle.putInt("user_id", id);
            RouteManager.Route.with(this)
                    .addActivityClass(ProfileActivity.class)
                    .addActivityTag("profile")
                    .addFragmentTag("profile")
                    .addFragmentBundle(bundle)
                    .startActivity();
        }else{
            Bundle bundle = new Bundle();
            bundle.putInt("user_id", id);
            RouteManager.Route.with(this)
                    .addActivityClass(ProfileActivity.class)
                    .addActivityTag("profile")
                    .addFragmentTag("visit")
                    .addFragmentBundle(bundle)
                    .startActivity();
        }

    }



    public void startSettingsActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(SettingsActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startExchangeActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(ExchangeActivity.class)
                .addActivityTag("exchange")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startVerificationActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(VerificationActivity.class)
                .addActivityTag("verification")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startExploreActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(ExploreActivity.class)
                .addActivityTag("explore")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startDiscoverActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(DiscoverActivity.class)
                .addActivityTag("discover")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startAddressBookActivity(String data, String fragmentTAG) {
        Bundle bundle = new Bundle();
        bundle.putString("address_raw", data);
        RouteManager.Route.with(this)
                .addActivityClass(AddressBookActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startSearchActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(SearchActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startNewRegisterActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(NewRegistrationActivity.class)
                .addActivityTag("new")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startRegistrationActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(RegistrationActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startAccountActivity(String fragmentTAG, int code) {
        RouteManager.Route.with(this)
                .addActivityClass(AccountActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .startActivityResult(code);
    }

    public void startSearchActivity(int userID, String name, String fragmentTAG) {
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", userID);
        bundle.putString("user_name", name);
        RouteManager.Route.with(this)
                .addActivityClass(SearchActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startSearchActivityResult() {
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", UserData.getUserId());
        bundle.putString("user_name", UserData.getUserItem().common_name);
        bundle.putBoolean("is_request", true);
        RouteManager.Route.with(this)
                .addActivityClass(SearchActivity.class)
                .addActivityTag("item")
                .addFragmentTag("followers")
                .addFragmentBundle(bundle)
                .startActivityResult(FRIEND_CODE);
    }

    public void startRequestActivity(int wishlist_id, int count, String title, String fragmentTAG) {
        Bundle bundle = new Bundle();
        bundle.putInt("wishlist_id", wishlist_id);
        bundle.putInt("count", count);
        bundle.putString("title", title);
        RouteManager.Route.with(this)
                .addActivityClass(RequestActivity.class)
                .addActivityTag("request")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startRequestActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(RequestActivity.class)
                .addActivityTag("request")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startWishListActivity(WishListItem wishListItem, String fragmentTAG) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("wishlist_item", wishListItem);
        RouteManager.Route.with(this)
                .addActivityClass(WishListActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startCommentActivity(int wishListID, String wishListName) {
        Bundle bundle = new Bundle();
        bundle.putInt("wishListID", wishListID);
        bundle.putString("wishListName", wishListName);
        RouteManager.Route.with(this)
                .addActivityClass(CommentActivity.class)
                .addActivityTag("item")
                .addFragmentTag("comment")
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startWishListActivity(Uri uri, String fragmentTAG) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("image_uri", uri);
        RouteManager.Route.with(this)
                .addActivityClass(WishListActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .addActivityBundle(bundle)
                .startActivity();
    }

    public void startWishListActivity(String post_content, String fragmentTAG) {
        Bundle bundle = new Bundle();
        bundle.putString("post_content", post_content);
        RouteManager.Route.with(this)
                .addActivityClass(WishListActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startBirthdayActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(BirthdayActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startBirthdayActivity(EventItem eventItem, String fragmentTAG) {
        Bundle bundle = new Bundle();
        bundle.putString("event_item", new Gson().toJson(eventItem));
        RouteManager.Route.with(this)
                .addActivityClass(BirthdayActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startBirthdaySurpriseActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(BirthdaySurpriseActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startRewardsActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(RewardsActivity.class)
                .addActivityTag("rewards")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startMessageActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(MessageActivity.class)
                .addActivityTag("message")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startVisitMessageActivity(String fragmentTAG, int chat_id, int user_id, String title) {
        Bundle bundle = new Bundle();
        bundle.putInt("chat_id", chat_id);
        bundle.putInt("user_id", user_id);
        bundle.putString("title", title);
        RouteManager.Route.with(this)
                .addActivityClass(MessageActivity.class)
                .addActivityTag("message")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startRewardsActivity(String fragmentTAG, int id) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        RouteManager.Route.with(this)
                .addActivityClass(RewardsActivity.class)
                .addActivityTag("rewards")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startRewardsActivity(String fragmentTAG, String type) {
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        RouteManager.Route.with(this)
                .addActivityClass(RewardsActivity.class)
                .addActivityTag("rewards")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startGemActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(GemActivity.class)
                .addActivityTag("gem")
                .addFragmentTag(fragmentTAG)
                .startActivityResult(GEM_CODE);
    }

    public void startCreatePostActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(CreatePostActivity.class)
                .addActivityTag("create_post")
                .addFragmentTag(fragmentTAG)
                .startActivityResult(GEM_CODE);
    }

    public void startiGaveReceivedActivity(String fragmentTAG, int id) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        RouteManager.Route.with(this)
                .addActivityClass(iGaveReceivedActivity.class)
                .addActivityTag("Gave")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }



    public void startImageEditorActivity(EventItem eventItem, UserItem userItem, String fragmentTAG) {
        Bundle bundle = new Bundle();
        bundle.putString("user_item", new Gson().toJson(userItem));
        bundle.putString("event_item", new Gson().toJson(eventItem));

        RouteManager.Route.with(this)
                .addActivityClass(ImageEditorActivity.class)
                .addActivityTag("item")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    @Subscribe
    public void invalidToken(InvalidToken invalidToken) {
        Log.e("Token", "Expired");
        EventBus.getDefault().unregister(this);
        UserData.insert(new UserItem());
        UserData.insert(UserData.TOKEN_EXPIRED, true);
        startLandingActivity();
    }

    public void deepLink(){
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        if (pendingDynamicLinkData != null) {
                            processDeepLink(pendingDynamicLinkData.getLink());
                        }

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }

    private void processDeepLink(Uri uri){
        if(uri == null){
            return;
        }

        URL url = null;
        try {
            url = new URL(uri.getScheme(), uri.getHost(), uri.getPath());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if(url == null){
            return;
        }

        String path = url.getPath();
        String[] strings = path.split("/");

        if(strings.length > 2){
            int id = Integer.parseInt(strings[strings.length - 1]);
            String type = strings[strings.length - 2];
            String target = "";
            switch (type){
                case "u":
                    target = "USER";
                    break;
                case "p":
                    target = "WISHLIST";
                    break;
            }
            redirect(target, id, "");
        }
    }

    public void redirect(String target, int id, String event){
        switch (target){
            case "USER":
                startProfileActivity(id);
                break;
            case "WISHLIST_TRANSACTION":
                if(event.equalsIgnoreCase("App\\Laravel\\Notifications\\WishlistTransaction\\GiftSentNotification")){
                    startMainActivity("notification");
                }else {
                    startItemActivity(id, 0, "i_received");
                }
                break;
            case "WISHLIST":
                startItemActivity(id, 0, "i_like");
                break;
            case "WISHLIST_VIEWER":
                startMainActivity("notification");
                break;
        }
    }
}
