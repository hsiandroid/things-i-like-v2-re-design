package com.thingsilikeapp.vendor.android.java.decoration;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by BCTI05 on 3/14/2017.
 */

public class SpaceItemDecoration extends RecyclerView.ItemDecoration {
    private int topSpace;
    private int bottomSpace;
    private int leftSpace;
    private int rightSpace;

    public SpaceItemDecoration(int topSpace,int bottomSpace,int leftSpace,int rightSpace) {
        this.topSpace = topSpace;
        this.bottomSpace = bottomSpace;
        this.leftSpace = leftSpace;
        this.rightSpace = rightSpace;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = bottomSpace;
        outRect.left = leftSpace;
        outRect.right = rightSpace;

        // Add top margin only for the first item to avoid double space between items
        if (parent.getChildLayoutPosition(view) == 0) {
            outRect.top = topSpace;
        } else {
            outRect.top = 10;
        }
    }
}
