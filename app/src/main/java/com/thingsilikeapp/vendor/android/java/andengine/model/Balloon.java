package com.thingsilikeapp.vendor.android.java.andengine.model;

import com.thingsilikeapp.android.activity.BirthdaySurpriseActivity;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.Random;

/**
 * Created by Jomar Olaybal on 10/11/2017.
 */

public class Balloon extends Sprite {

    private BalloonResource balloonResource;
    private int id = 0;
    private int delayCount;
    private int delay = 0;
    private boolean onDragged = false;
    private OnPoppedListener onPoppedListener;
    private long lastClickTime = 0;

    public Balloon(BalloonResource balloonResource, VertexBufferObjectManager vbom) {
        super(balloonResource.getRandomX(), -balloonResource.getBalloonITR().getHeight(), balloonResource.getBalloonITR(), vbom);
        this.balloonResource = balloonResource;
        int size = randInt(1, 4);
        switch (size){
            case 1:
                break;
            case 2:
                setWidth((float)(getWidth() - (getWidth() * 0.15)));
                setHeight((float)(getHeight() - (getHeight() * 0.15)));
                break;
            case 3:
                setWidth((float)(getWidth() - (getWidth() * 0.25)));
                setHeight((float)(getHeight() - (getHeight() * 0.25)));
                break;
            case 4:
                setWidth((float)(getWidth() + (getWidth() * 0.15)));
                setHeight((float)(getHeight() + (getHeight() * 0.15)));
                break;
        }
        delay = randInt(0, 5);
    }

    public void setOnDragged(boolean onDragged) {
        this.onDragged = onDragged;
    }

    public boolean isDragged() {
        return onDragged;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void balloonFloat() {
        if(onDragged){
            return;
        }

        if(delayCount < delay){
            delayCount ++;
            return;
        }
        delayCount = 0;
        setPosition(getX(), getY() + 1);
        if ( getX() <= (-1 * getWidth()) || getY() >= BirthdaySurpriseActivity.CAMERA_HEIGHT + getHeight()) {
            setPosition(balloonResource.getRandomX(), - balloonResource.getBalloonITR().getHeight());
            delay = randInt(0, 5);
        }
    }

    public void setOnPoppedListener(OnPoppedListener onPoppedListener) {
        this.onPoppedListener = onPoppedListener;
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        switch (pSceneTouchEvent.getAction()){
            case TouchEvent.ACTION_DOWN:
                balloonPopped();
                break;
            case TouchEvent.ACTION_UP:

                break;
        }
        return false;
    }
    private void balloonPopped(){
        balloonPoppedOnly(true);
    }

    private void balloonPoppedOnly(final boolean requestCallback){
        setScale(1.25f);
        setAlpha(0.5f);

        DelayModifier delayModifier = new DelayModifier(0.08f){
            @Override
            protected void onModifierFinished(IEntity pItem) {
                getBalloonResource().activity.getEngine().runOnUpdateThread(new Runnable() {
                    @Override
                    public void run() {
                        if(requestCallback && onPoppedListener != null){
                            onPoppedListener.onPopped(Balloon.this.id);
                        }
                        detachSelf();
                    }
                });
                super.onModifierFinished(pItem);
            }
        };
        registerEntityModifier(delayModifier);
    }

    public void poppedDelay(){
        float delay = randFloat(0.8f, 1f);
        DelayModifier delayModifier = new DelayModifier(delay){
            @Override
            protected void onModifierFinished(IEntity pItem) {
                getBalloonResource().activity.getEngine().runOnUpdateThread(new Runnable() {
                    @Override
                    public void run() {
                        balloonPoppedOnly(false);
                    }
                });
                super.onModifierFinished(pItem);
            }
        };
        registerEntityModifier(delayModifier);
    }

    public interface OnPoppedListener{
        void onPopped(int id);
    }

    @Override
    public boolean detachSelf() {
        balloonResource.unloadBalloonScreen();
        return super.detachSelf();
    }

    public BalloonResource getBalloonResource() {
        return balloonResource;
    }

    public static class BalloonResource{

        private BirthdaySurpriseActivity activity;

        private String [] balloons = new String[]{
                "blue",
                "blue_green",
                "green",
                "red",
                "yellow"
        };

        public static BalloonResource getDefault(BirthdaySurpriseActivity activity){
            BalloonResource balloonResource = new BalloonResource();
            balloonResource.activity = activity;
            balloonResource.loadBalloonScreen();
            return balloonResource;
        }

        private BitmapTextureAtlas balloonBTA;
        public ITextureRegion balloonITR;

        public void loadBalloonScreen() {
            loadBalloonGraphics();
        }

        public void unloadBalloonScreen() {
            balloonBTA.unload();
            balloonITR = null;
        }

        private void loadBalloonGraphics() {
            BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("graphics/balloon/");
            balloonBTA = new BitmapTextureAtlas(activity.getTextureManager(), 300, 300, TextureOptions.BILINEAR);
            balloonITR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(balloonBTA, activity, balloons[randInt(0, balloons.length - 1)] + ".png", 0, 0);
            balloonBTA.load();
        }

        public ITextureRegion getBalloonITR() {
            return balloonITR;
        }

        public int getRandomX() {
            int min = 20;
            int xMax = BirthdaySurpriseActivity.CAMERA_WIDTH;
            return randInt(min, xMax);
        }
    }

    public static int randInt(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }

    public static float randFloat(float min, float max) {
        return new Random().nextFloat() * (min - max) + min;
    }
}
