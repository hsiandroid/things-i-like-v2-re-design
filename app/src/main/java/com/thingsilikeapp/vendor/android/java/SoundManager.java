package com.thingsilikeapp.vendor.android.java;

import android.content.Context;
import android.media.AudioManager;
import android.os.Handler;

/**
 * Created by Jomar Olaybal on 10/14/2017.
 */

public class SoundManager {
    private static int targetVol;
    private static int currentVol;
    private static int STEP = 1;
    private static Handler handler;
    private static Runnable runnable;

    public void fadeOutSoundVolume(Context context){
        final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        targetVol = 0;
        currentVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        handler = new Handler();
        runnable =new Runnable() {
            @Override
            public void run() {
                if(currentVol > targetVol){
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVol - STEP, 0);
                    currentVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                    handler.postDelayed(this, 300);
                }else{
                    clearHandler();
                }
            }
        };

        handler.post(runnable);
    }

    public static void mute(Context context){
        final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
    }

    public static void volume(Context context, int volume){
        final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
    }

    public static void fadInSoundVolume(Context context){
        final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        currentVol = (int) (maxVolume * 0.4);
        targetVol = (int) (maxVolume * 0.8);
        handler = new Handler();
        runnable =new Runnable() {
            @Override
            public void run() {
                if(currentVol < targetVol){
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVol + STEP, 0);
                    currentVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                    handler.postDelayed(this, 400);
                }else{
                    clearHandler();
                }
            }
        };

        handler.post(runnable);
    }

    public static void clearHandler(){
        if(handler != null && runnable != null){
            handler.removeCallbacks(runnable);
        }
    }

}
