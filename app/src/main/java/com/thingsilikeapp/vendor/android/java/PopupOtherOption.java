package com.thingsilikeapp.vendor.android.java;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.thingsilikeapp.R;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by BCTI 3 on 11/13/2017.
 */

public class PopupOtherOption {

    private PopupMenu popupMenu;
    private Context context;
    private View view;
    private MenuItemClickListener menuItemClickListener;
    private boolean onEditText;

    public static PopupOtherOption newInstance(Context context, View view) {
        PopupOtherOption popupOtherOption = new PopupOtherOption(context, view);
        return popupOtherOption;
    }

    public static PopupOtherOption newInstance(Context context, ContextThemeWrapper contextThemeWrapper, View view) {
        PopupOtherOption popupOtherOption = new PopupOtherOption(context, contextThemeWrapper, view);
        return popupOtherOption;
    }

    public PopupOtherOption(Context context, final View view){
        this.context = context;
        this.view = view;
        popupMenu = new PopupMenu(context, view);
        popupMenu.getMenuInflater().inflate(R.menu.other_option_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(view instanceof EditText && onEditText){
                    ((EditText)view).setText(item.getTitle());
                }
                if(menuItemClickListener != null){
                    menuItemClickListener.onMenuItemClick(item);
                }
                return false;
            }
        });
    }

    public PopupOtherOption(Context context, ContextThemeWrapper contextThemeWrapper, final View view){
        this.context = context;
        this.view = view;
        popupMenu = new PopupMenu(contextThemeWrapper, view);
        popupMenu.getMenuInflater().inflate(R.menu.other_option_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(view instanceof EditText && onEditText){
                    ((EditText)view).setText(item.getTitle());
                }
                if(menuItemClickListener != null){
                    menuItemClickListener.onMenuItemClick(item);
                }
                return false;
            }
        });
    }

    public PopupOtherOption setOnEditText(boolean onEditText) {
        this.onEditText = onEditText;
        return this;
    }

    public PopupOtherOption setOnMenuItemClickListener(MenuItemClickListener menuItemClickListener){
        this.menuItemClickListener = menuItemClickListener;
        return this;
    }

    public interface MenuItemClickListener{
        void onMenuItemClick(MenuItem item);
    }

    public PopupOtherOption addItem(int id, String title){
        addItem(id, title, null);
        return this;
    }

    public PopupOtherOption addItem(int id, String title, Drawable drawable){
        if(popupMenu != null){
            popupMenu.getMenu().add(0, id, 0, title);

            if(drawable != null){
                drawable.setBounds(0,0,20,20);
                popupMenu.getMenu().findItem(id).setIcon(drawable);
            }
        }
        return this;
    }

    public PopupOtherOption addIcon(Drawable drawable, int position){
        if(popupMenu != null){
            popupMenu.getMenu().getItem(position).setIcon(drawable);
        }
        return this;
    }

    public void show(){
        if(popupMenu != null){
            popupMenu.show();
        }
    }

    public void setForceShowIcon() {
        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
