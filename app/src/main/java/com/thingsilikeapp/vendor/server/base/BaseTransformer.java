package com.thingsilikeapp.vendor.server.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class BaseTransformer {

    @SerializedName("msg")
    @Expose
    public String msg = "";

    @SerializedName("max_fragment")
    @Expose
    public String max_fragment = "";

    @SerializedName("reward_crystal")
    @Expose
    public double reward_crystal = 0.0;

    @SerializedName("reward_fragment")
    @Expose
    public double reward_fragment = 0.0;

    @SerializedName("status")
    @Expose
    public Boolean status = false;

    @SerializedName("status_code")
    @Expose
    public String statusCode = "";

    @SerializedName("token")
    @Expose
    public String token = "";

    @SerializedName("otp_value")
    @Expose
    public String otp_value = "";

    @SerializedName("new_token")
    @Expose
    public String new_token = "";

    @SerializedName("last_page")
    @Expose
    public String last_page = "";

    @SerializedName("current_page")
    @Expose
    public String current_page = "";

    @SerializedName("keyword")
    @Expose
    public String keyword = "";


    @SerializedName("has_morepages")
    @Expose
    public Boolean has_morepages = false;

    public boolean hasRequirements() {
        return false;
    }

    public boolean checkEmpty(Object obj) {
        return obj != null;
    }

}