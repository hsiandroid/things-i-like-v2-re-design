package com.thingsilikeapp.vendor.server.request;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.google.firebase.iid.FirebaseInstanceId;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Url;
import com.thingsilikeapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Labyalo on 8/3/2017.
 */

public class APIRequest<T extends BaseTransformer> implements Callback<T> {

    private final String DEVICE_NAME = "android";

    private Context context;
    private List<MultipartBody.Part> multipartBody;
    private HashMap<String, String> params;
    private Retrofit retrofit;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeRefreshLayout;

    private int page = 1;
    private int perPage = 5;
    private boolean hasMorePage = false;
    private boolean showNoInternetConnection = true;
    private String authorization = "xxxxxx";
    private DataResponseType dataResponseType = DataResponseType.DEFAULT;
    public boolean checkToken = true;
    public boolean isCanceled = false;


    public enum DataResponseType {
        DEFAULT,
        NEXT,
        PREV,
        FIRST
    }

    public String getDeviceIMEI() {
        String deviceIMEI;
        try{
            TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            deviceIMEI = mTelephonyManager.getDeviceId();
        }catch (Exception ex){
            deviceIMEI = String.valueOf(Build.SERIAL);
        }
        return deviceIMEI;
    }


    public String getDeviceModel(){
        return String.valueOf(Build.MODEL);
    }

    public String getOsVersion(){
        return String.valueOf(Build.VERSION.RELEASE);
    }

    public APIRequest(Context context){
        this.context = context;

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder()
                                .addHeader("Content-Type", "application/json").build();
                        return chain.proceed(request);
                    }
                })
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Url.APP)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public APIRequest addParameter(String key, Object object) {

        if (multipartBody == null) {
            multipartBody = new ArrayList<>();
        }

        if (params == null) {
            params = new HashMap<>();
        }

        if(object instanceof File){
            File file = (File) object;
            if(file != null && file.isFile()){
                multipartBody.add(MultipartBody.Part.createFormData(key, file.getName(), RequestBody.create(MediaType.parse(getMimeType(file)) ,file)));
            }
        }else{
            multipartBody.add(MultipartBody.Part.createFormData(key, String.valueOf(object)));
            params.put(key, String.valueOf(object));
        }

        return this;
    }

    public String getMimeType(File file) {
        Uri uri = Uri.fromFile(file);
        String mimeType;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    public Call<T> onCreateCall() {

        return null;
    }

    public interface Service{

    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public String getAuthorization() {
        return authorization;
    }

    public String getDeviceName() {
        return DEVICE_NAME;
    }

    public String getDeviceID() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public DataResponseType getDataResponseType() {
        return dataResponseType;
    }

    public List<MultipartBody.Part> getMultipartBody(){
        if (multipartBody == null) {
            multipartBody = new ArrayList<>();
        }

        List<MultipartBody.Part> parts = multipartBody;
        parts.add(MultipartBody.Part.createFormData(Keys.PAGE, String.valueOf(getPage())));
        parts.add(MultipartBody.Part.createFormData(Keys.PER_PAGE, String.valueOf(getPerPage())));
        parts.add(MultipartBody.Part.createFormData(Keys.DEVICE_ID, getDeviceID()));
        parts.add(MultipartBody.Part.createFormData(Keys.DEVICE_NAME, getDeviceName()));
        parts.add(MultipartBody.Part.createFormData(Keys.DEVICE_REG_ID, FirebaseInstanceId.getInstance().getToken()));
        parts.add(MultipartBody.Part.createFormData(Keys.DEVICE_MODEL, getDeviceModel()));
        parts.add(MultipartBody.Part.createFormData(Keys.DEVICE_IMEI, getDeviceIMEI()));
        parts.add(MultipartBody.Part.createFormData(Keys.OS_VERSION, getOsVersion()));
        return parts;
    }

    public HashMap<String, String> getParameter(){
        if (params == null) {
            params = new HashMap<>();
        }

        HashMap<String, String> parts = params;
        parts.put(Keys.PAGE, String.valueOf(getPage()));
        parts.put(Keys.PER_PAGE, String.valueOf(getPerPage()));
        parts.put(Keys.DEVICE_ID, getDeviceID());
        parts.put(Keys.DEVICE_NAME, getDeviceName());
        parts.put(Keys.DEVICE_REG_ID, FirebaseInstanceId.getInstance().getToken());
        parts.put(Keys.DEVICE_MODEL, getDeviceModel());
        parts.put(Keys.DEVICE_IMEI, getDeviceIMEI());
        parts.put(Keys.OS_VERSION, getOsVersion());

        return parts;
    }

    public APIRequest addAuthorization(String authorization) {
        this.authorization = "Bearer " + authorization;
        return this;
    }

    public void onResponse(){

    }

    public void broadcast(Object event){
        EventBus.getDefault().post(event);
    }

    /** for pagination */
    public int getPage() {
        return page;
    }

    public int getPerPage() {
        return perPage;
    }

    public boolean hasMorePage() {
        return hasMorePage;
    }

    public void setHasMorePage(boolean hasMorePage) {
        this.hasMorePage = hasMorePage;
    }

    public APIRequest setPage(int page) {
        this.page = page;
        return this;
    }

    public APIRequest setPerPage(int perPage) {
        this.perPage = perPage;
        return this;
    }

    private void setDataResponseType(DataResponseType dataResponseType) {
        this.dataResponseType = dataResponseType;
    }


    public APIRequest setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
        return this;
    }

    public APIRequest showDefaultProgressDialog(String message) {
        this.progressDialog = new ProgressDialog(context).show(context, "", message, false, false);
        return this;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public APIRequest setSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout) {
        this.swipeRefreshLayout = swipeRefreshLayout;
        return this;
    }

    public APIRequest showSwipeRefreshLayout(boolean b) {
        if (getSwipeRefreshLayout() != null) {
            getSwipeRefreshLayout().setRefreshing(b);
        }
        return this;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }


    /** Commands */
    public void execute() {
        setDataResponseType(DataResponseType.DEFAULT);
        run();
    }

    public void nextPage() {
        setDataResponseType(DataResponseType.NEXT);
        if (hasMorePage) {
            setPage(getPage() + 1);
        }
        run();
    }

    public void previousPage() {
        setDataResponseType(DataResponseType.PREV);
        if (getPage() > 1) {
            setPage(getPage() - 1);
        }
        run();
    }

    public void first() {
        setDataResponseType(DataResponseType.FIRST);
        setPage(1);
        run();
    }

    public void run() {
        isCanceled = false;
        if(isNetworkAvailable()){
            onCreateCall().enqueue(this);
        }else{
            dismissLoading();
            EventBus.getDefault().post(new NoInternetResponse(false, this));
        }
    }

    public void cancelRequest() {
        isCanceled = true;
        onCreateCall().cancel();
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        Log.e("APIRequest", String.valueOf(response.code()));
        Log.e("APIRequest","URL: "+response.raw().request().url());
        Log.e("APIRequest","BODY: " + response.raw().toString());

        dismissLoading();
        if(!isCanceled){
            this.response = response;
            onResponse();
            EventBus.getDefault().post(new NoInternetResponse(this));
        }
    }

    private Response<T> response;
    public Response<T> getResponse(){
        return response;
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        dismissLoading();
        if(!isCanceled){
            onResponse();
        }
    }

    private void dismissLoading(){
        if (getProgressDialog() != null) {
            getProgressDialog().dismiss();
        }
        if (getSwipeRefreshLayout() != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public class OnFailedResponse{

    }

    public class NoInternetResponse{
        private APIRequest apiRequest;

        private boolean hasInternet = true;

        public NoInternetResponse(APIRequest apiRequest){
            this.apiRequest = apiRequest;
        }

        public NoInternetResponse(boolean hasInternet, APIRequest apiRequest){
            this.hasInternet = hasInternet;
            this.apiRequest = apiRequest;
        }

        public boolean hasInternet(){
            return hasInternet;
        }

        public void retry(){
            if(apiRequest != null){
                apiRequest.execute();
            }
        }

        public APIRequest getApiRequest() {
            return apiRequest;
        }
    }
}

