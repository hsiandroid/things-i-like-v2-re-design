package com.thingsilikeapp.vendor.server.base;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.vendor.server.util.InfoDialog;
import com.thingsilikeapp.vendor.server.util.InvalidToken;
import com.thingsilikeapp.vendor.server.util.ToStringConverterFactory;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class BaseRequest<T> implements Callback<T> {

    private static Context context;
    private Retrofit retrofit;

    private String BASE_URL = "http://google.com";
    private String TOKEN = "123456789";
    private final String DEVICE_NAME = "android";
    private int page = 1;
    private int perPage = 5;
    private boolean hasMorePage = true;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeRefreshLayout;

    private HashMap<String, String> params;
    private HashMap<String, RequestBody> requestBodyParams;
    private List<MultipartBody.Part> parts;
    private DATA_RESPONSE_TYPE dataResponseType = DATA_RESPONSE_TYPE.DEFAULT;

    private String authorization = "xxxxxx";
    private boolean checkToken = true;
    private boolean showStringResponse = true;

    private boolean showNoInternetConnection = true;
    private boolean showError500 = false;

    public BaseRequest(Context context) {
        BaseRequest.context = context;
        Bundle data = null;
        try {
            data = context.getPackageManager().getApplicationInfo(
                    context.getPackageName(),
                    PackageManager.GET_META_DATA).metaData;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        TOKEN = data.getString("server.TOKEN", "");
        BASE_URL = data.getString("server.BASEURL", "");
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder()
                                .addHeader("Content-Type", "application/json").build();
                        return chain.proceed(request);
                    }
                })
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(new ToStringConverterFactory())
                .client(okHttpClient)
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public void setRetrofit(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public String getToken() {
        return TOKEN;
    }

    public String getDeviceID() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getDeviceName() {
        return DEVICE_NAME;
    }

    public void execute() {
        setDataResponseType(DATA_RESPONSE_TYPE.DEFAULT);
        run();
    }

    public BaseRequest<T> addAuthorization(String authorization) {
        this.authorization = "Bearer " + authorization;
        return this;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void run() {
        onCreateCall().enqueue(this);
    }

    public void executeString() {
        onCreateStringCall().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        Log.e("String Response", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Converter<ResponseBody, BaseTransformer> converter = getRetrofit()
                            .responseBodyConverter(BaseTransformer.class, new Annotation[0]);
                    try {

                        Log.e("String Response", converter.convert(response.errorBody()).toString());
                    } catch (IOException e) {
                        Log.e("String Response", "parse error");
                    }
                }

                if (getProgressDialog() != null) {
                    getProgressDialog().dismiss();
                }
                if (getSwipeRefreshLayout() != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                if (getProgressDialog() != null) {
                    getProgressDialog().dismiss();
                }
                if (getSwipeRefreshLayout() != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    public void appendLog(String text) {
        File logFile = new File("sdcard/log" + getCurrentTimeStamp() + ".json");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String getCurrentTimeStamp() {
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateTime = dateFormat.format(new Date());

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public Call<T> onCreateCall() {
        return null;
    }

    public Call<ResponseBody> onCreateStringCall() {
        return null;
    }

    // for pagination

    public boolean hasMorePage() {
        return hasMorePage;
    }

    public int getPage() {
        return page;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setHasMorePage(boolean hasMorePage) {
        this.hasMorePage = hasMorePage;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public BaseRequest<T> setPerPage(int perPage) {
        this.perPage = perPage;
        return this;
    }

    public BaseRequest<T> nextPage() {
        setDataResponseType(DATA_RESPONSE_TYPE.NEXT);
        if (hasMorePage) {
            setPage(getPage() + 1);
            run();
        }
        return this;
    }

    public BaseRequest<T> previousPage() {
        setDataResponseType(DATA_RESPONSE_TYPE.PREV);
        if (getPage() > 1) {
            setPage(getPage() - 1);
            run();
        }
        return this;
    }

    public BaseRequest<T> first() {
        setDataResponseType(DATA_RESPONSE_TYPE.FIRST);
        setPage(1);
        run();
        return this;
    }


    // for parameter

    public HashMap<String, String> getParameters() {
        if (params == null) {
            params = new HashMap<>();
            defaultParam();
        }

        HashMap<String, String> page = params;
        page.put(Keys.server.key.PAGE, getPage() + "");
        page.put(Keys.server.key.PER_PAGE, getPerPage() + "");
        return page;
    }

    public BaseRequest<T> addParameters(String key, Object object) {

        if (params == null) {
            params = new HashMap<>();
            defaultParam();
        }

        params.put(key, String.valueOf(object));
        return this;
    }

    public BaseRequest<T> clearParameters() {
        if (params != null) {
            params.clear();
            params = new HashMap<>();
            defaultParam();
        }
        return this;
    }

    public void defaultParam() {
        params.put(Keys.server.key.API_TOKEN, getToken());
        params.put(Keys.server.key.DEVICE_ID, getDeviceID());
        params.put(Keys.server.key.DEVICE_NAME, getDeviceName());
        params.put(Keys.server.key.DEVICE_REG_ID, getDeviceRegId());
        params.put(Keys.server.key.DEVICE_MODEL, getDeviceModel());
        params.put(Keys.server.key.DEVICE_IMEI, getDeviceIMEI());
        params.put(Keys.server.key.OS_VERSION, getOsVersion());
        addDefaultParam();
    }

    public void addDefaultParam() {

    }

    public String getDeviceIMEI() {
        String deviceIMEI;
        try {
            TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
//                return TODO;

                deviceIMEI = mTelephonyManager.getDeviceId();
//                return deviceIMEI;
            }else {
                deviceIMEI = "";
            }
        }catch (Exception ex){
            deviceIMEI = String.valueOf(Build.SERIAL);
        }
        return deviceIMEI;
    }


    public String getDeviceModel(){
        return String.valueOf(Build.MODEL);
    }

    public String getOsVersion(){
        return String.valueOf(Build.VERSION.RELEASE);
    }

    // for request body parameter

    public HashMap<String, RequestBody> getRequestBodyParameters() {
        if (requestBodyParams == null) {
            requestBodyParams = new HashMap<>();
            defaultRequestBodyParam();
        }

        HashMap<String, RequestBody> page = requestBodyParams;
        page.put(Keys.server.key.PAGE, getStringRequestBody(String.valueOf(getPage())));
        page.put(Keys.server.key.PER_PAGE, getStringRequestBody(String.valueOf(getPerPage())));
        return page;
    }

    public RequestBody getStringRequestBody(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), String.valueOf(value));
    }

    public RequestBody getStringRequestBody(int value) {
        return RequestBody.create(MediaType.parse("text/plain"), String.valueOf(value));
    }


    public BaseRequest<T> addRequestBodyParameters(String key, Object object) {

        if (requestBodyParams == null) {
            requestBodyParams = new HashMap<>();
            defaultRequestBodyParam();
        }
        requestBodyParams.put(key, RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(object)));
        return this;
    }

    public BaseRequest<T> clearRequestBodyParameters() {
        if (requestBodyParams != null) {
            requestBodyParams.clear();
            requestBodyParams = new HashMap<>();
            defaultRequestBodyParam();
        }
        if (parts != null) {
            parts.clear();
            parts = new ArrayList<>();
        }
        return this;
    }

    public void defaultRequestBodyParam() {
        requestBodyParams.put(Keys.server.key.API_TOKEN, getStringRequestBody(String.valueOf(getToken())));
        requestBodyParams.put(Keys.server.key.DEVICE_ID, getStringRequestBody(String.valueOf(getDeviceID())));
        requestBodyParams.put(Keys.server.key.DEVICE_NAME, getStringRequestBody(String.valueOf(getDeviceName())));
        requestBodyParams.put(Keys.server.key.DEVICE_REG_ID, getStringRequestBody(String.valueOf(getDeviceRegId())));
        addDefaultRequestBodyParam();
    }

    public void addDefaultRequestBodyParam() {

    }

    public BaseRequest<T> addMultipartBody(String key, Object object) {

        if (parts == null) {
            parts = new ArrayList<>();
        }

        if(object instanceof File){
            File file = (File) object;
            parts.add(MultipartBody.Part.createFormData(key, file.getName(), RequestBody.create(MediaType.parse("image/*"), file)));
        }else{
            parts.add(MultipartBody.Part.createFormData(key, String.valueOf(object)));
        }

        return this;
    }

    public BaseRequest<T> clearMultipartBody() {
        if (parts != null) {
            parts.clear();
            parts = new ArrayList<>();
            defaultMultipartBody();
        }
        return this;
    }

    public List<MultipartBody.Part> getMultipartBody() {
        return parts;
    }

    public void defaultMultipartBody() {
        addMultipartBody(Keys.server.key.API_TOKEN, getStringRequestBody(String.valueOf(getToken())));
        addMultipartBody(Keys.server.key.DEVICE_ID, getStringRequestBody(String.valueOf(getDeviceID())));
        addMultipartBody(Keys.server.key.DEVICE_NAME, getStringRequestBody(String.valueOf(getDeviceName())));
        addMultipartBody(Keys.server.key.DEVICE_REG_ID, getStringRequestBody(String.valueOf(getDeviceRegId())));
        addMultipartBody(Keys.server.key.DEVICE_MODEL, getStringRequestBody(String.valueOf(getDeviceModel())));
        addMultipartBody(Keys.server.key.DEVICE_IMEI, getStringRequestBody(String.valueOf(getDeviceIMEI())));
        addMultipartBody(Keys.server.key.OS_VERSION, getStringRequestBody(String.valueOf(getOsVersion())));
        addDefaultMultipartBody();
    }

    public void addDefaultMultipartBody() {

    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        Log.e("TAG", ">>>" + response.code());
        Log.e("TAG",">>> URL: "+response.raw().request().url());
        Log.e("TAG",">>> BODY: " + response.raw().toString());
        if(response.code() == 500){
            if(showError500){
                InfoDialog infoDialog = InfoDialog.Builder();
                infoDialog.setIcon(R.drawable.icon_error_500)
                        .setTitle("")
                        .setDescription("Internal Server Error")
                        .setButtonText("Reload")
                        .setButtonClickListener(new InfoDialog.ClickListener() {
                            @Override
                            public void onClick(DialogFragment dialog) {

                                if (getProgressDialog() != null) {
                                    getProgressDialog().show();
                                }
                                if (getSwipeRefreshLayout() != null) {
                                    swipeRefreshLayout.setRefreshing(true);
                                }

                                run();
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            }
                        })
                        .build(((AppCompatActivity) context).getSupportFragmentManager());
            }
        }else {
            responseData(response);
        }

        if (getProgressDialog() != null) {
            getProgressDialog().dismiss();
        }
        if (getSwipeRefreshLayout() != null) {
            swipeRefreshLayout.setRefreshing(false);
        }

        View noInternet = ((Activity) context).findViewById(R.id.noInternetIMG);
        if(noInternet != null){
            if(noInternet.isShown()){
                Animation slide = AnimationUtils.loadAnimation(context, R.anim.slide_left_to_right);
                noInternet.startAnimation(slide);
            }
            noInternet.setVisibility(View.GONE);
        }
    }

    public void generateTextFile(Context context, String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Logs");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
            Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (getProgressDialog() != null) {
            getProgressDialog().dismiss();
        }

        if (getSwipeRefreshLayout() != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
        Log.e("TAG", ">>>FAILED"+t.getLocalizedMessage());
        Log.e("TAG2", ">>>FAILED2"+t.getCause());
        Log.e("TAG3", ">>>" + t.getMessage());
        if (showNoInternetConnection) {
            Toast.makeText(context, "Check your internet connection.", Toast.LENGTH_LONG).show();
        }

        EventBus.getDefault().post(new OnFailedResponse(call, BaseRequest.this));
    }

    public void responseData(Response<T> response) {

    }

    public class OnFailedResponse{
        private Call<T> call;
        private BaseRequest<T> baseRequest;

        public OnFailedResponse(Call<T> call, BaseRequest<T> baseRequest){
            this.call = call;
            this.baseRequest = baseRequest;
        }

        public Call<T> getCall() {
            return call;
        }

        public BaseRequest<T> getRequest(){
            return baseRequest;
        }
    }

    public BaseRequest<T> showNoInternetConnection(boolean showNoInternetConnection) {
        this.showNoInternetConnection = showNoInternetConnection;
        return this;
    }

    public BaseRequest<T> showError500(boolean showError500) {
        this.showError500 = showError500;
        return this;
    }

    public BaseRequest<T> setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
        return this;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public BaseRequest<T> setSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout) {
        this.swipeRefreshLayout = swipeRefreshLayout;
        return this;
    }

    public BaseRequest<T> showSwipeRefreshLayout(boolean b) {
        if (getSwipeRefreshLayout() != null) {
            getSwipeRefreshLayout().setRefreshing(b);
        }
        return this;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }


    private enum DATA_RESPONSE_TYPE {
        DEFAULT,
        NEXT,
        PREV,
        FIRST
    }

    private void setDataResponseType(DATA_RESPONSE_TYPE dataResponseType) {
        this.dataResponseType = dataResponseType;
    }

    private DATA_RESPONSE_TYPE getDataResponseType() {
        return dataResponseType;
    }

    private String deviceRegId = "";

    public BaseRequest<T> setDeviceRegID(String regID) {
        deviceRegId = regID;
        return this;
    }

    public void setCheckToken(boolean checkToken) {
        this.checkToken = checkToken;
    }

    public String getDeviceRegId() {
        return deviceRegId;
    }

    public class ResponseData {
        private Response<T> response;

        private boolean isNext = false;
        private boolean isPrev = false;
        private boolean isFirst = false;

        public boolean isNext() {
            return isNext;
        }

        public boolean isPrev() {
            return isPrev;
        }

        public boolean isFirst() {
            return isFirst;
        }

        public ResponseData(Response<T> response) {
            switch (getDataResponseType()) {
                case DEFAULT:
                    break;
                case NEXT:
                    isNext = true;
                    Log.e("ResponseData", "next");
                    break;
                case PREV:
                    isPrev = true;
                    Log.e("ResponseData", "prev");
                    break;
                case FIRST:
                    isFirst = true;
                    Log.e("ResponseData", "first");
                    break;
            }

            this.response = response;
        }

        public int getCode() {
            return response.code();
        }

        public String getMessage() {
            return response.message();
        }

        public T getData(Class K) {
            return processResponse(K);
        }

        public T getData() {
            return processResponse(BaseTransformer.class);
        }

        public T processResponse(Class K){
            if ( response.code() >= 400 && response.code() < 599 ) {
                Converter<ResponseBody, T> converter = getRetrofit().responseBodyConverter(K, new Annotation[0]);
                try {
                    T temp = converter.convert(response.errorBody());
                    if (checkToken) {
                        switch (((BaseTransformer) temp).statusCode){
                            case "TOKEN_EXPIRED":
                            case "TOKEN_INVALID":
                            case "USER_NOT_FOUND ":
                                EventBus.getDefault().post(InvalidToken.newInstance());
                                break;
                        }
                    }

                    if(showStringResponse){
                        String json = new Gson().toJson(temp);
                        Log.e("showStringResponse", ">>>" + json);
                    }
                    return temp;
                } catch (IOException e) {
                    Log.e("ResponseData Error", ">>> " + e.getMessage());
                    return (T) new BaseTransformer().getClass();
                }
            } else {
                if(showStringResponse){
                    String json = new Gson().toJson(response.body());
                    Log.e("showStringResponse", ">>>" + json);
                }
                T temp = response.body();
                hasMorePage = ((BaseTransformer) temp).has_morepages;
                return temp;
            }
        }
    }
}
