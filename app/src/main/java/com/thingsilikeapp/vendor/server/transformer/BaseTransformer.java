package com.thingsilikeapp.vendor.server.transformer;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.model.ErrorModel;

import java.util.List;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class BaseTransformer {

    @SerializedName("msg")
    public String msg = "Internal Server Error";

    @SerializedName("status")
    public Boolean status = false;

    @SerializedName("status_code")
    public String status_code = "RETROFIT_FAILED";

    @SerializedName("token")
    public String token = "";

    @SerializedName("new_token")
    public String new_token = "";

    @SerializedName("chat_status")
    public String chat_status = "";

    @SerializedName("chat_id")
    public int chat_id;

    @SerializedName("chat_title")
    public String chat_title = "";

    @SerializedName("mentorship_title")
    public String mentorship_title = "";

    @SerializedName("mentorship_code")
    public String mentorship_code = "";

    @SerializedName("has_morepages")
    public Boolean has_morepages = false;

    @SerializedName("mentorship_status")
    public String mentorship_status = "";

    @SerializedName("rating")
    public String rating;

    @SerializedName("review")
    public String review;

    @SerializedName("service_fee")
    public int service_fee;

    @SerializedName("selected_currency")
    public String selected_currency;

    @SerializedName("temp_id")
    public int temp_id;

    @SerializedName("first_login")
    public boolean first_login = false;

    @SerializedName("is_reviewed")
    public boolean is_reviewed = false;

    @SerializedName("is_participant")
    public boolean is_participant = false;

    @SerializedName("errors")
    public ErrorModel errors;

    @SerializedName("content")
    public List<String> content;

    @SerializedName("house_no")
    public List<String> house_no;

    @SerializedName("contact_number")
    public List<String> contact_number;

    @SerializedName("category")
    public List<String> category;

    @SerializedName("recipient")
    public List<String> recipient;

    @SerializedName("street_address")
    public List<String> street_address;

    @SerializedName("city")
    public List<String> city;

    @SerializedName("state")
    public List<String> state;

    @SerializedName("zip_code")
    public List<String> zip_code;

    @SerializedName("country")
    public List<String> country;

    @SerializedName("file")
    public List<String> file;

    @SerializedName("hasRequirements")
    public boolean hasRequirements(){
        return checkEmpty(errors);
    }

    public boolean checkEmpty(Object obj){
        return obj !=  null;
    }
}
