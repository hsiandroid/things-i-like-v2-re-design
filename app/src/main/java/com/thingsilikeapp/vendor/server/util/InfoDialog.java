package com.thingsilikeapp.vendor.server.util;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;

/**
 * Created by BCTI 3 on 5/11/2017.
 */

public class InfoDialog extends DialogFragment {
    public static final String TAG = InfoDialog.class.getName();

    private int icon;
    private String title;
    private String description;
    private String button;

    private ImageView iconIV;
    private TextView descriptionTXT;
    private TextView positiveBTN;


    private ClickListener clickListener;

    public static InfoDialog Builder() {
        InfoDialog fragment = new InfoDialog();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_server_info, container, false);
        iconIV = view.findViewById(R.id.iconIV);
        descriptionTXT = view.findViewById(R.id.descriptionTXT);
        positiveBTN = view.findViewById(R.id.positiveBTN);
        onViewReady();
        return view;
    }

    public void onViewReady() {
        if(icon != 0){
            iconIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), icon));
        }

        if(description != null){
            descriptionTXT.setText(description);
        }

        if(positiveBTN != null){
            positiveBTN.setText(button);
        }

        if(clickListener != null){
            positiveBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(clickListener != null){
                        clickListener.onClick(InfoDialog.this);
                    }
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }



    public void setDialogLayoutParam(int width, int height){
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }


    public InfoDialog setIcon(int icon){
        this.icon = icon;
        return this;
    }

    public InfoDialog setTitle(String title){
        this.title = title;
        return this;
    }

    public InfoDialog setDescription(String description){
        this.description = description;
        return this;
    }

    public InfoDialog setButtonText(String button){
        this.button = button;
        return this;
    }

    public InfoDialog setButtonClickListener(ClickListener clickListener){
        this.clickListener = clickListener;
        return this;
    }

    public interface ClickListener{
        void onClick(DialogFragment dialog);
    }

    public void build(FragmentManager manager){
        show(manager, TAG);
    }
}
