package com.thingsilikeapp.data.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by BCTI 3 on 1/31/2017.
 */

public class Data {

    public static SharedPreferences sharedPreferences;
    public static String gcm_token;

    public static void setSharedPreferences(Context context){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static SharedPreferences getSharedPreferences(){
        return sharedPreferences;
    }
}
