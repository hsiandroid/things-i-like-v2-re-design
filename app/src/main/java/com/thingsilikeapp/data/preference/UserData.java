package com.thingsilikeapp.data.preference;


import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.thingsilikeapp.data.model.UserItem;

/**
 * Created by BCTI 3 on 1/31/2017.
 */

public class UserData extends Data{

    public static final String USER_ITEM = "user_item";
    public static final String LIKER_COUNT = "liker_count";
    public static final String AUTHORIZATION = "authorization";
    public static final String FIRST_OPEN = "first_open";
    public static final String FIRST_NOTIFICATION = "first_notification";
    public static final String FIRST_LOGIN = "first_login";
    public static final String RINGTONE = "ringtone";
    public static final String RINGTONE_NAME = "ringtone_name";
    public static final String NOTIFICATION_ENABLE = "notification_enable";
    public static final String TOKEN_EXPIRED = "token_expired";
    public static final String WALKTHRU_DONE = "walkthru_done";
    public static final String WALKTHRU_RESPONSE= "walkthru_response";
    public static final String WALKTHRU_ALREADY= "walkthru_already";
    public static final String WALKTHRU_CREATE_DONE = "walkthru_create_done";
    public static final String WALKTHRU_CREATE_POST_SHOW = "walkthru_create_post_show";
    public static final String REQUEST_ADDRESS_SHOW = "request_address_show";
    public static final String REQUEST_LIKE_SHOW = "request_like_show";
    public static final String WALKTHRU_CURRENT = "walkthru_current";
    public static final String WALKTHRU_CURRENT_PROFILE = "walkthru_current_profile";
    public static final String PROFILE_CURRENT = "profile_current";
    public static final String CREATE_POST_SHOW = "create_post_show";
    public static final String BIRTHDAY_SHOW = "birthday_show";
    public static final String POST_SHOW = "post_show";
    public static final String SHARE_POST_SHOW = "share_post_show";
    public static final String SHARE_POST = "share_post";
    public static final String LIKE_POST_SHOW = "like_post_show";
    public static final String BIRTHDAY_COUNT = "birthday_count";
    public static final String VIEW_ONCE = "once";
    public static final String PEOPLE_PROCESS = "people_process";
    public static final String USER_ID = "user_id";
    public static final String FEED_CACHE = "feed_cache";
    public static final String MY_FEED_CACHE = "my_feed_cache";
    public static final String SHOW_SHARE = "show_share";
    public static final String TOTAL_POST = "total_post";
    public static final String ERROR_COUNT = "error_count";
    public static final String POST_MESSAGE = "post_message";
    public static final String POST_IMAGE = "post_image";
    public static final String STREET_ADDRESS = "street_address";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String ZIP = "zip";
    public static final String CONTACT_NUMBER = "contact_number";
    public static final String CATEGORY = "category";
    public static final String COUNTRY = "country";
    public static final String GREETINGS = "greetings_count";
    public static final String REWARD_CRYSTAL = "reward_crystal";
    public static final String LYKAGEM_DISPLAY = "lykagem_display";
    public static final String REWARD_FRAGMENT = "reward_fragment";
    public static final String REWARD_RATIO = "reward_ratio";

    public static final String BIRTHDAY_VIDEO_2017 = "birthday_video_2017";

    public static boolean isLogin(){
        return getUserItem().id != 0;
    }

    public static boolean isMe(int id){
        return getUserItem().id == id;
    }

    public static int getUserId(){
        return getUserItem().id;
    }

    public static void insert(String key, String value){
        SharedPreferences.Editor editor = Data.getSharedPreferences().edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void insert(String key, int value){
        SharedPreferences.Editor editor = Data.getSharedPreferences().edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void insert(String key, boolean value){
        SharedPreferences.Editor editor = Data.getSharedPreferences().edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void insert(UserItem userItem){
        SharedPreferences.Editor editor = Data.getSharedPreferences().edit();
        editor.putString(USER_ITEM, new Gson().toJson(userItem));
        editor.commit();
    }

    public static String getString(String key){
        return getString(key, "");
    }

    public static String getString(String key, String defaultVal){
        return Data.getSharedPreferences().getString(key, defaultVal);
    }

    public static int getInt(String key){
        return Data.getSharedPreferences().getInt(key, 0);
    }

    public static boolean getBoolean(String key){
        return getBoolean(key,false);
    }

    public static boolean getBoolean(String key, boolean b){
        return Data.getSharedPreferences().getBoolean(key, b);
    }

    public static UserItem getUserItem(){
        String raw = Data.getSharedPreferences().getString(USER_ITEM, "");
        if (raw.equals("") && raw == null){
            raw = "{\"id\": 0,\n \"name\" : TIL}";
            UserData.insert(USER_ITEM, raw);
        }
        UserItem userItem = new Gson().fromJson(raw, UserItem.class);

        return userItem;
    }

    public static void updateStatistic(int count){
        UserItem userItem = getUserItem();
        if(userItem.statistics == null){
            return;
        }
        userItem.statistics.data.following += count;
        insert(userItem);
    }

    public static void updateRewardCrystal(String rewardCrystal){
        insert(REWARD_CRYSTAL, rewardCrystal);
    }

    public static void updateDisplayRewardCrystal(String rewardCrystal){
        insert(LYKAGEM_DISPLAY, rewardCrystal);
    }

    public static void updateRewardFragment(String rewardFragment){
        insert(REWARD_FRAGMENT, rewardFragment);
    }

    public static void updateRewardRatio(String ratio){
        insert(REWARD_RATIO, ratio);
    }

    public static String getRewardCrystal(){
        return getString(REWARD_CRYSTAL, "0.0");
    }

    public static String getRewardFragment(){
        return getString(REWARD_FRAGMENT, "0");
    }

    public static String getDisplayCrystalFragment(){ return getString(LYKAGEM_DISPLAY, "0.0"); }

    public static String getRewardRatio(){
        return getString(REWARD_RATIO, "0:0");
    }

    public static void clearData(){
        Data.getSharedPreferences().edit().clear().commit();
    }

}
