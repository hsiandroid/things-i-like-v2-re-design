package com.thingsilikeapp.data.preference;

import android.content.Context;

import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.model.UserItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BCTI 3 on 3/7/2017.
 */

public class SampleData {
    public static List<WishListItem> getThingsILikeItem(int count){
        List<WishListItem> all = new ArrayList<>();

        WishListItem wishListItem = new WishListItem();
        wishListItem.title = "Microsoft Band Fitness";
        wishListItem.info = new WishListItem.Info();
        wishListItem.info.data = new WishListItem.Info.InfoData();
        wishListItem.info.data.content = "For people who want to live healthier and achieve more there is Microsoft Band. Reach your health and fitness goals by tracking your heart rate, exercise, calorie burn, and sleep quality, and be productive with email, text, and calendar alerts on your wrist.";
        wishListItem.category = "I just like it";
        wishListItem.image = new WishListItem.Image();
        wishListItem.image.data = new WishListItem.Image.ImageData();
        wishListItem.image.data.full_path = "http://media.philstar.com/images/the-philippine-star/world/20141031/Microsoft-Band-fitness-gadget.jpg";
        wishListItem.owner = new WishListItem.Owner();
        wishListItem.owner.data = new UserItem();
        wishListItem.owner.data.username = "jomar.olaybal";
        wishListItem.owner.data.name = "Jomar Olaybal";
        wishListItem.owner.data.info = new UserItem.Info();
        wishListItem.owner.data.info.data = new UserItem.Info.InfoData();
        wishListItem.owner.data.info.data.avatar = new UserItem.Info.InfoData.Avatar();
        wishListItem.owner.data.info.data.avatar.full_path = "https://graph.facebook.com/100003492353882/picture?type=large";
        all.add(wishListItem);

        wishListItem = new WishListItem();
        wishListItem.title = "Iphone 7";
        wishListItem.info = new WishListItem.Info();
        wishListItem.info.data = new WishListItem.Info.InfoData();
        wishListItem.info.data.content = "iPhone 7 dramatically improves the most important aspects of the iPhone experience. It introduces advanced new camera systems. The best performance and battery life ever in an iPhone. Immersive stereo speakers. The brightest, most colorful iPhone display. Splash and water resistance.1 And it looks every bit as powerful as it is. This is iPhone 7.";
        wishListItem.category = "I just like it";
        wishListItem.image = new WishListItem.Image();
        wishListItem.image.data = new WishListItem.Image.ImageData();
        wishListItem.image.data.full_path = "http://cdn2.gsmarena.com/vv/pics/apple/apple-iphone-7-1.jpg";

        wishListItem.owner = new WishListItem.Owner();
        wishListItem.owner.data = new UserItem();
        wishListItem.owner.data.username = "jomar.olaybal";
        wishListItem.owner.data.name = "Jomar Olaybal";
        wishListItem.owner.data.info = new UserItem.Info();
        wishListItem.owner.data.info.data = new UserItem.Info.InfoData();
        wishListItem.owner.data.info.data.avatar = new UserItem.Info.InfoData.Avatar();
        wishListItem.owner.data.info.data.avatar.full_path = "https://graph.facebook.com/100003492353882/picture?type=large";
        all.add(wishListItem);

        wishListItem = new WishListItem();
        wishListItem.title = "Fart Gun Gadget";
        wishListItem.info = new WishListItem.Info();
        wishListItem.info.data = new WishListItem.Info.InfoData();
        wishListItem.info.data.content = "The Minions certainly are equipped with some of the goofiest gadgets ever created by Dr. Nefario in Despicable Me 2.";
        wishListItem.category = "Birthday Present";
        wishListItem.image = new WishListItem.Image();
        wishListItem.image.data = new WishListItem.Image.ImageData();
        wishListItem.image.data.full_path = "http://www.designbolts.com/wp-content/uploads/2014/01/Despicable-Me-2-Minion-Fart-Gun-Gadget.jpg";

        wishListItem.owner = new WishListItem.Owner();
        wishListItem.owner.data = new UserItem();
        wishListItem.owner.data.username = "jomar.olaybal";
        wishListItem.owner.data.name = "Jomar Olaybal";
        wishListItem.owner.data.info = new UserItem.Info();
        wishListItem.owner.data.info.data = new UserItem.Info.InfoData();
        wishListItem.owner.data.info.data.avatar = new UserItem.Info.InfoData.Avatar();
        wishListItem.owner.data.info.data.avatar.full_path = "https://graph.facebook.com/100003492353882/picture?type=large";
        all.add(wishListItem);

        wishListItem = new WishListItem();
        wishListItem.title = "Head phone";
        wishListItem.info = new WishListItem.Info();
        wishListItem.info.data = new WishListItem.Info.InfoData();
        wishListItem.info.data.content = "Can someone buy me this.";
        wishListItem.category = "I just like it";
        wishListItem.image = new WishListItem.Image();
        wishListItem.image.data = new WishListItem.Image.ImageData();
        wishListItem.image.data.full_path = "https://s-media-cache-ak0.pinimg.com/736x/93/64/e5/9364e509cf987cdf34475e91ab8c3c1c.jpg";

        wishListItem.owner = new WishListItem.Owner();
        wishListItem.owner.data = new UserItem();
        wishListItem.owner.data.username = "paul.santos";
        wishListItem.owner.data.name = "Paul Santos";
        wishListItem.owner.data.info = new UserItem.Info();
        wishListItem.owner.data.info.data = new UserItem.Info.InfoData();
        wishListItem.owner.data.info.data.avatar = new UserItem.Info.InfoData.Avatar();
        wishListItem.owner.data.info.data.avatar.full_path = "https://graph.facebook.com/100013163700064/picture?type=large";
        all.add(wishListItem);

        wishListItem = new WishListItem();
        wishListItem.title = "Huawei";
        wishListItem.info = new WishListItem.Info();
        wishListItem.info.data = new WishListItem.Info.InfoData();
        wishListItem.info.data.content = "Etiam eu dignissim nunc.";
        wishListItem.category = "I just like it";
        wishListItem.image = new WishListItem.Image();
        wishListItem.image.data = new WishListItem.Image.ImageData();
        wishListItem.image.data.full_path = "http://consumer-res.huaweistatic.com/uk/mobile-phones/p10-plus/assets/img/Beauty2BgMobi.jpg?1488254488206";

        wishListItem.owner = new WishListItem.Owner();
        wishListItem.owner.data = new UserItem();
        wishListItem.owner.data.username = "jomar.olaybal";
        wishListItem.owner.data.name = "Jomar Olaybal";
        wishListItem.owner.data.info = new UserItem.Info();
        wishListItem.owner.data.info.data = new UserItem.Info.InfoData();
        wishListItem.owner.data.info.data.avatar = new UserItem.Info.InfoData.Avatar();
        wishListItem.owner.data.info.data.avatar.full_path = "https://graph.facebook.com/100003492353882/picture?type=large";
        all.add(wishListItem);

        wishListItem = new WishListItem();
        wishListItem.title = "Handbag";
        wishListItem.info = new WishListItem.Info();
        wishListItem.info.data = new WishListItem.Info.InfoData();
        wishListItem.info.data.content = "Sed molestie enim non malesuada facilisis.";
        wishListItem.category = "Birthday Present";
        wishListItem.image = new WishListItem.Image();
        wishListItem.image.data = new WishListItem.Image.ImageData();
        wishListItem.image.data.full_path = "https://ae01.alicdn.com/kf/HTB1DC5pKXXXXXbUXpXXq6xXFXXXY/Euro-US-Style-New-Women-bag-Genuine-Leather-Handbag-Women-Messenger-Bag-Vintage-Leather-Crossbody-Shoulder.jpg_350x350.jpg";

        wishListItem.owner = new WishListItem.Owner();
        wishListItem.owner.data = new UserItem();
        wishListItem.owner.data.username = "kenneth.sunday";
        wishListItem.owner.data.name = "Kenneth Sunday";
        wishListItem.owner.data.info = new UserItem.Info();
        wishListItem.owner.data.info.data = new UserItem.Info.InfoData();
        wishListItem.owner.data.info.data.avatar = new UserItem.Info.InfoData.Avatar();
        wishListItem.owner.data.info.data.avatar.full_path = "https://graph.facebook.com/100007227347061/picture?type=large";
        all.add(wishListItem);

        wishListItem = new WishListItem();
        wishListItem.title = "American Tourister Duffel Bag";
        wishListItem.info = new WishListItem.Info();
        wishListItem.info.data = new WishListItem.Info.InfoData();
        wishListItem.info.data.content = "Nunc in eleifend elit, ut hendrerit nibh. Donec fringilla purus non eros hendrerit, at congue est lacinia. ";
        wishListItem.category = "I just like it";
        wishListItem.image = new WishListItem.Image();
        wishListItem.image.data = new WishListItem.Image.ImageData();
        wishListItem.image.data.full_path = "http://img5a.flixcart.com/image/duffel-bag/n/p/8/40x-0-12-011-american-tourister-duffel-bag-x-bag-travel-1-original-imadcxjnqvgwnjgs.jpeg";

        wishListItem.owner = new WishListItem.Owner();
        wishListItem.owner.data = new UserItem();
        wishListItem.owner.data.username = "paul.santos";
        wishListItem.owner.data.name = "Paul Santos";
        wishListItem.owner.data.info = new UserItem.Info();
        wishListItem.owner.data.info.data = new UserItem.Info.InfoData();
        wishListItem.owner.data.info.data.avatar = new UserItem.Info.InfoData.Avatar();
        wishListItem.owner.data.info.data.avatar.full_path = "https://graph.facebook.com/100013163700064/picture?type=large";
        all.add(wishListItem);

        wishListItem = new WishListItem();
        wishListItem.title = "Bag";
        wishListItem.info = new WishListItem.Info();
        wishListItem.info.data = new WishListItem.Info.InfoData();
        wishListItem.info.data.content = "Ut ut mi at odio tincidunt imperdiet.";
        wishListItem.category = "I just like it";
        wishListItem.image = new WishListItem.Image();
        wishListItem.image.data = new WishListItem.Image.ImageData();
        wishListItem.image.data.full_path = "https://s-media-cache-ak0.pinimg.com/originals/c7/c3/ea/c7c3ead2a778f8a4f4f0523a15f3a427.jpg";

        wishListItem.owner = new WishListItem.Owner();
        wishListItem.owner.data = new UserItem();
        wishListItem.owner.data.username = "kenneth.sunday";
        wishListItem.owner.data.name = "Kenneth Sunday";
        wishListItem.owner.data.info = new UserItem.Info();
        wishListItem.owner.data.info.data = new UserItem.Info.InfoData();
        wishListItem.owner.data.info.data.avatar = new UserItem.Info.InfoData.Avatar();
        wishListItem.owner.data.info.data.avatar.full_path = "https://graph.facebook.com/100007227347061/picture?type=large";
        all.add(wishListItem);

        wishListItem = new WishListItem();
        wishListItem.title = "Lutyens";
        wishListItem.info = new WishListItem.Info();
        wishListItem.info.data = new WishListItem.Info.InfoData();
        wishListItem.info.data.content = "Ut tempor leo elit, at ullamcorper elit cursus vitae.";
        wishListItem.category = "I just like it";
        wishListItem.image = new WishListItem.Image();
        wishListItem.image.data = new WishListItem.Image.ImageData();
        wishListItem.image.data.full_path = "http://ecx.images-amazon.com/images/I/71WuGfbntxL._SL1100_.jpg";

        wishListItem.owner = new WishListItem.Owner();
        wishListItem.owner.data = new UserItem();
        wishListItem.owner.data.username = "kenneth.sunday";
        wishListItem.owner.data.name = "Kenneth Sunday";
        wishListItem.owner.data.info = new UserItem.Info();
        wishListItem.owner.data.info.data = new UserItem.Info.InfoData();
        wishListItem.owner.data.info.data.avatar = new UserItem.Info.InfoData.Avatar();
        wishListItem.owner.data.info.data.avatar.full_path = "https://graph.facebook.com/100007227347061/picture?type=large";
        all.add(wishListItem);

        wishListItem = new WishListItem();
        wishListItem.title = "Eco Bag";
        wishListItem.info = new WishListItem.Info();
        wishListItem.info.data = new WishListItem.Info.InfoData();
        wishListItem.info.data.content = "Aenean eget elit elementum, auctor nisi at, tempor libero.";
        wishListItem.category = "Anniversary";
        wishListItem.image = new WishListItem.Image();
        wishListItem.image.data = new WishListItem.Image.ImageData();
        wishListItem.image.data.full_path = "https://s-media-cache-ak0.pinimg.com/736x/35/ec/98/35ec98856f1c949ea9ef6e7eeb0a9268.jpg";

        wishListItem.owner = new WishListItem.Owner();
        wishListItem.owner.data = new UserItem();
        wishListItem.owner.data.username = "paul.santos";
        wishListItem.owner.data.name = "Paul Santos";
        wishListItem.owner.data.info = new UserItem.Info();
        wishListItem.owner.data.info.data = new UserItem.Info.InfoData();
        wishListItem.owner.data.info.data.avatar = new UserItem.Info.InfoData.Avatar();
        wishListItem.owner.data.info.data.avatar.full_path = "https://graph.facebook.com/100013163700064/picture?type=large";
        all.add(wishListItem);


        List<WishListItem> val = new ArrayList<>();

        int counter = 0;

        do{
            for(WishListItem item : all){
                if(counter < count){
                    val.add(item);
                    counter++;
                }
            }
        }while (counter < count);

        return val;
    }


    public static List<CategoryItem> getCategoryItem(Context context){
        List<CategoryItem> categoryItems = new ArrayList<>();
        CategoryItem categoryItem = new CategoryItem();
        categoryItem.title = "I Just Like It!";
        categoryItem.parent = "";
        categoryItems.add(categoryItem);

        categoryItem = new CategoryItem();
        categoryItem.title = "Birthday Present";
        categoryItem.parent = "";
        categoryItems.add(categoryItem);

        categoryItem = new CategoryItem();
        categoryItem.title = "Travel Vacation";
        categoryItem.parent = "";
        categoryItems.add(categoryItem);

        categoryItem = new CategoryItem();
        categoryItem.title = "Beach Resort Vacation";
        categoryItem.parent = "";
        categoryItems.add(categoryItem);

        categoryItem = new CategoryItem();
        categoryItem.title = "Hotel Booking";
        categoryItem.parent = "";
        categoryItems.add(categoryItem);

        categoryItem = new CategoryItem();
        categoryItem.title = "Christmas Wishlist";
        categoryItem.parent = "";
        categoryItems.add(categoryItem);

        categoryItem = new CategoryItem();
        categoryItem.title = "Engagement Present";
        categoryItem.parent = "";
        categoryItems.add(categoryItem);

        categoryItem = new CategoryItem();
        categoryItem.title = "Monthsary Gift";
        categoryItem.parent = "";
        categoryItems.add(categoryItem);

        categoryItem = new CategoryItem();
        categoryItem.title = "Anniversary Gift";
        categoryItem.parent = "";
        categoryItems.add(categoryItem);

        return categoryItems;
    }
}
