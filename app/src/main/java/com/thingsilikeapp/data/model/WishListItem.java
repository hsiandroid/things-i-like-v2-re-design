package com.thingsilikeapp.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class WishListItem implements Parcelable  {


    @SerializedName("id")
    public int id;

    @SerializedName("parent_id")
    public int parent_id;

    @SerializedName("title")
    public String title;

    @SerializedName("pattern")
    public int pattern;

    @SerializedName("category")
    public String category;

    @SerializedName("status")
    public String status;

    @SerializedName("walkthrough_on_process_post")
    public boolean walkthrough_on_process_post = false;

    @SerializedName("walkthrough_on_process_like")
    public boolean walkthrough_on_process_like = false;

    @SerializedName("time_passed")
    public String time_passed;

    @SerializedName("comment_count")
    public int comment_count;

    @SerializedName("comment_count_for_display")
    public String comment_count_for_display;

    @SerializedName("for_display_comment")
    public String for_display_comment;

    @SerializedName("ongoing_viewer_count")
    public int ongoing_viewer_count;

    @SerializedName("for_display_ongoing_viewer")
    public String for_display_ongoing_viewer;

    @SerializedName("liker_count")
    public int liker_count;

    @SerializedName("diamond_sent")
    public int diamond_sent;

    @SerializedName("for_display_liker")
    public String for_display_liker;

    @SerializedName("repost_count")
    public int repost_count;

    @SerializedName("for_display_repost")
    public String for_display_repost;

    @SerializedName("is_liked")
    public boolean is_liked;

    @SerializedName("is_sent")
    public boolean is_sent;

    @SerializedName("same_title")
    public boolean same_title = false;

    @SerializedName("info")
    public Info info;

    @SerializedName("image")
    public Image image;

    @SerializedName("owner")
    public Owner owner;

    @SerializedName("post_type")
    public String post_type;

    @SerializedName("orientation")
    public String orientation;

    @SerializedName("delivery_info")
    public DeliveryInfo delivery_info;

    @SerializedName("transaction")
    public Transaction transaction;

    @SerializedName("logs")
    public Logs logs;

    @SerializedName("tracker")
    public Tracker tracker;

    @SerializedName("images")
    public Images images;

    public static class Images implements Parcelable  {
        @SerializedName("data")
        public List<ImageModel> data;

        protected Images(Parcel in) {
            data = in.createTypedArrayList(ImageModel.CREATOR);
        }

        public static final Creator<Images> CREATOR = new Creator<Images>() {
            @Override
            public Images createFromParcel(Parcel in) {
                return new Images(in);
            }

            @Override
            public Images[] newArray(int size) {
                return new Images[size];
            }
        };

        public String getImage() {
            List<ImageModel> images = getImages();
            if (images.size() > 0) {
                return images.get(0).getImage();
            }
            return "";
        }

        public List<ImageModel> getImages() {
            if (data == null) {
                data = new ArrayList<>();
            }
            return data;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeTypedList(data);
        }
    }

    public WishListMin wishListMin;

    public WishListItem() {
    }

    protected WishListItem(Parcel in) {
        id = in.readInt();
        parent_id = in.readInt();
        title = in.readString();
        category = in.readString();
        status = in.readString();
        walkthrough_on_process_post = in.readByte() != 0;
        walkthrough_on_process_like = in.readByte() != 0;
        time_passed = in.readString();
        comment_count = in.readInt();
        comment_count_for_display = in.readString();
        for_display_comment = in.readString();
        ongoing_viewer_count = in.readInt();
        for_display_ongoing_viewer = in.readString();
        post_type = in.readString();
        orientation = in.readString();
        liker_count = in.readInt();
        for_display_liker = in.readString();
        repost_count = in.readInt();
        for_display_repost = in.readString();
        is_liked = in.readByte() != 0;
        is_sent = in.readByte() != 0;
        same_title = in.readByte() != 0;
        info = in.readParcelable(Info.class.getClassLoader());
        image = in.readParcelable(Image.class.getClassLoader());
        images = in.readParcelable(Image.class.getClassLoader());
        owner = in.readParcelable(Owner.class.getClassLoader());
        delivery_info = in.readParcelable(DeliveryInfo.class.getClassLoader());
        transaction = in.readParcelable(Transaction.class.getClassLoader());
        logs = in.readParcelable(Logs.class.getClassLoader());
        tracker = in.readParcelable(Tracker.class.getClassLoader());
    }

    public static final Creator<WishListItem> CREATOR = new Creator<WishListItem>() {
        @Override
        public WishListItem createFromParcel(Parcel in) {
            return new WishListItem(in);
        }

        @Override
        public WishListItem[] newArray(int size) {
            return new WishListItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(parent_id);
        dest.writeString(title);
        dest.writeString(category);
        dest.writeString(status);
        dest.writeByte((byte) (walkthrough_on_process_post ? 1 : 0));
        dest.writeByte((byte) (walkthrough_on_process_like ? 1 : 0));
        dest.writeString(time_passed);
        dest.writeInt(comment_count);
        dest.writeString(comment_count_for_display);
        dest.writeString(for_display_comment);
        dest.writeString(post_type);
        dest.writeString(orientation);
        dest.writeInt(ongoing_viewer_count);
        dest.writeString(for_display_ongoing_viewer);
        dest.writeInt(liker_count);
        dest.writeString(for_display_liker);
        dest.writeInt(repost_count);
        dest.writeString(for_display_repost);
        dest.writeByte((byte) (is_liked ? 1 : 0));
        dest.writeByte((byte) (is_sent ? 1 : 0));
        dest.writeByte((byte) (same_title ? 1 : 0));
        dest.writeParcelable(info, flags);
        dest.writeParcelable(image, flags);
        dest.writeParcelable(images, flags);
        dest.writeParcelable(owner, flags);
        dest.writeParcelable(delivery_info, flags);
        dest.writeParcelable(transaction, flags);
        dest.writeParcelable(logs, flags);
        dest.writeParcelable(tracker, flags);
    }


    public static class Transaction implements Parcelable {

        @SerializedName("data")
        public WishListTransactionItem data;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.data, flags);
        }

        public Transaction() {
        }

        protected Transaction(Parcel in) {
            this.data = in.readParcelable(WishListTransactionItem.class.getClassLoader());
        }

        public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
            @Override
            public Transaction createFromParcel(Parcel source) {
                return new Transaction(source);
            }

            @Override
            public Transaction[] newArray(int size) {
                return new Transaction[size];
            }
        };
    }

    public static class Owner implements Parcelable {

        @SerializedName("data")
        public UserItem data;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.data, flags);
        }

        public Owner() {
        }

        protected Owner(Parcel in) {
            this.data = in.readParcelable(UserItem.class.getClassLoader());
        }

        public static final Creator<Owner> CREATOR = new Creator<Owner>() {
            @Override
            public Owner createFromParcel(Parcel source) {
                return new Owner(source);
            }

            @Override
            public Owner[] newArray(int size) {
                return new Owner[size];
            }
        };
    }

    public static class Info implements Parcelable {


        @SerializedName("data")
        public InfoData data;

        public static class InfoData implements Parcelable {

            @SerializedName("content")
            public String content;

            @SerializedName("url")
            public String url = "";

            @SerializedName("url_title")
            public String url_title;

            @SerializedName("url_description")
            public String url_description = "";

            @SerializedName("url_image")
            public String url_image;

            @SerializedName("created_at")
            public CreatedAt created_at;

            public static class CreatedAt implements Parcelable {

                @SerializedName("date_db")
                public String date_db;

                @SerializedName("month_year")
                public String month_year;

                @SerializedName("time_passed")
                public String time_passed;

                @SerializedName("timestamp")
                public Timestamp timestamp;

                public static class Timestamp implements Parcelable {

                    @SerializedName("date")
                    public String date;

                    @SerializedName("timezone_type")
                    public int timezone_type;

                    @SerializedName("timezone")
                    public String timezone;

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {
                        dest.writeString(this.date);
                        dest.writeInt(this.timezone_type);
                        dest.writeString(this.timezone);
                    }

                    public Timestamp() {
                    }

                    protected Timestamp(Parcel in) {
                        this.date = in.readString();
                        this.timezone_type = in.readInt();
                        this.timezone = in.readString();
                    }

                    public static final Parcelable.Creator<Timestamp> CREATOR = new Parcelable.Creator<Timestamp>() {
                        @Override
                        public Timestamp createFromParcel(Parcel source) {
                            return new Timestamp(source);
                        }

                        @Override
                        public Timestamp[] newArray(int size) {
                            return new Timestamp[size];
                        }
                    };
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.date_db);
                    dest.writeString(this.month_year);
                    dest.writeString(this.time_passed);
                    dest.writeParcelable(this.timestamp, flags);
                }

                public CreatedAt() {
                }

                protected CreatedAt(Parcel in) {
                    this.date_db = in.readString();
                    this.month_year = in.readString();
                    this.time_passed = in.readString();
                    this.timestamp = in.readParcelable(Timestamp.class.getClassLoader());
                }

                public static final Parcelable.Creator<CreatedAt> CREATOR = new Parcelable.Creator<CreatedAt>() {
                    @Override
                    public CreatedAt createFromParcel(Parcel source) {
                        return new CreatedAt(source);
                    }

                    @Override
                    public CreatedAt[] newArray(int size) {
                        return new CreatedAt[size];
                    }
                };
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.content);
                dest.writeString(this.url);
                dest.writeString(this.url_title);
                dest.writeString(this.url_description);
                dest.writeString(this.url_image);
                dest.writeParcelable(this.created_at, flags);
            }

            public InfoData() {
            }

            protected InfoData(Parcel in) {
                this.content = in.readString();
                this.url = in.readString();
                this.url_title = in.readString();
                this.url_description = in.readString();
                this.url_image = in.readString();
                this.created_at = in.readParcelable(CreatedAt.class.getClassLoader());
            }

            public static final Creator<InfoData> CREATOR = new Creator<InfoData>() {
                @Override
                public InfoData createFromParcel(Parcel source) {
                    return new InfoData(source);
                }

                @Override
                public InfoData[] newArray(int size) {
                    return new InfoData[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.data, flags);
        }

        public Info() {
        }

        protected Info(Parcel in) {
            this.data = in.readParcelable(InfoData.class.getClassLoader());
        }

        public static final Parcelable.Creator<Info> CREATOR = new Parcelable.Creator<Info>() {
            @Override
            public Info createFromParcel(Parcel source) {
                return new Info(source);
            }

            @Override
            public Info[] newArray(int size) {
                return new Info[size];
            }
        };
    }

    public static class Image implements Parcelable {

        @SerializedName("data")
        public ImageData data;

        public static class ImageData implements Parcelable {

            @SerializedName("path")
            public String path;

            @SerializedName("filename")
            public String filename;

            @SerializedName("directory")
            public String directory;

            @SerializedName("full_path")
            public String full_path;

            @SerializedName("thumb_path")
            public String thumb_path;

            @SerializedName("preview")
            public String preview;

            @SerializedName("temp_preview")
            public String temp_preview = "";

            @SerializedName("width")
            public String width = "";

            @SerializedName("height")
            public String height = "";

            public ImageData() {

            }

            protected ImageData(Parcel in) {
                path = in.readString();
                filename = in.readString();
                directory = in.readString();
                full_path = in.readString();
                thumb_path = in.readString();
                preview = in.readString();
                temp_preview = in.readString();
                width = in.readString();
                height = in.readString();
            }

            public static final Creator<ImageData> CREATOR = new Creator<ImageData>() {
                @Override
                public ImageData createFromParcel(Parcel in) {
                    return new ImageData(in);
                }

                @Override
                public ImageData[] newArray(int size) {
                    return new ImageData[size];
                }
            };

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(path);
                dest.writeString(filename);
                dest.writeString(directory);
                dest.writeString(full_path);
                dest.writeString(thumb_path);
                dest.writeString(preview);
                dest.writeString(temp_preview);
                dest.writeString(width);
                dest.writeString(height);
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.data, flags);
        }

        public Image() {
        }

        protected Image(Parcel in) {
            this.data = in.readParcelable(ImageData.class.getClassLoader());
        }

        public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
            @Override
            public Image createFromParcel(Parcel source) {
                return new Image(source);
            }

            @Override
            public Image[] newArray(int size) {
                return new Image[size];
            }
        };
    }

    public static class DeliveryInfo implements Parcelable {

        @SerializedName("data")
        public DeliveryInfoData data;

        public static class DeliveryInfoData implements Parcelable {

            @SerializedName("recipient")
            public String recipient;

            @SerializedName("address")
            public String address;

            @SerializedName("street_address")
            public String street_address;

            @SerializedName("city")
            public String city;

            @SerializedName("state")
            public String state;

            @SerializedName("zip_code")
            public String zip_code;

            @SerializedName("contact_number")
            public String contact_number;

            @SerializedName("country")
            public String country = "ph";

            @SerializedName("permission_to_view")
            public String permission_to_view;

            public DeliveryInfoData() {
            }

            protected DeliveryInfoData(Parcel in) {
                recipient = in.readString();
                address = in.readString();
                street_address = in.readString();
                city = in.readString();
                state = in.readString();
                zip_code = in.readString();
                contact_number = in.readString();
                country = in.readString();
                permission_to_view = in.readString();
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(recipient);
                dest.writeString(address);
                dest.writeString(street_address);
                dest.writeString(city);
                dest.writeString(state);
                dest.writeString(zip_code);
                dest.writeString(contact_number);
                dest.writeString(country);
                dest.writeString(permission_to_view);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public static final Creator<DeliveryInfoData> CREATOR = new Creator<DeliveryInfoData>() {
                @Override
                public DeliveryInfoData createFromParcel(Parcel in) {
                    return new DeliveryInfoData(in);
                }

                @Override
                public DeliveryInfoData[] newArray(int size) {
                    return new DeliveryInfoData[size];
                }
            };
        }

        public DeliveryInfo() {
        }

        protected DeliveryInfo(Parcel in) {
            data = in.readParcelable(DeliveryInfoData.class.getClassLoader());
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(data, flags);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<DeliveryInfo> CREATOR = new Creator<DeliveryInfo>() {
            @Override
            public DeliveryInfo createFromParcel(Parcel in) {
                return new DeliveryInfo(in);
            }

            @Override
            public DeliveryInfo[] newArray(int size) {
                return new DeliveryInfo[size];
            }
        };
    }

    public static class Logs implements Parcelable {

        @SerializedName("data")
        public List<TimelineItem> data;

        public Logs() {
        }

        protected Logs(Parcel in) {
            data = in.createTypedArrayList(TimelineItem.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(data);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Logs> CREATOR = new Creator<Logs>() {
            @Override
            public Logs createFromParcel(Parcel in) {
                return new Logs(in);
            }

            @Override
            public Logs[] newArray(int size) {
                return new Logs[size];
            }
        };
    }

    public static class Tracker implements Parcelable {

        @SerializedName("data")
        public List<TrackerItem> data;

        public Tracker() {
        }

        protected Tracker(Parcel in) {
            data = in.createTypedArrayList(TrackerItem.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(data);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Tracker> CREATOR = new Creator<Tracker>() {
            @Override
            public Tracker createFromParcel(Parcel in) {
                return new Tracker(in);
            }

            @Override
            public Tracker[] newArray(int size) {
                return new Tracker[size];
            }
        };
    }


    public WishListItem minimal(int userId){
        wishListMin = new WishListMin();
        wishListMin.id = id;
        wishListMin.owned = userId == owner.data.id;
        wishListMin.name = owner.data.name;
        wishListMin.is_verified = owner.data.is_verified;
        wishListMin.category = category;
        wishListMin.time_passed = time_passed;
        wishListMin.title = title;
        wishListMin.comment_count = comment_count;
        wishListMin.liker_count = liker_count;
        wishListMin.avatar = owner.data.getAvatar();
        wishListMin.is_liked = is_liked;
        wishListMin.walkthrough_on_process_post = walkthrough_on_process_post;
        wishListMin.walkthrough_on_process_like = walkthrough_on_process_like;
        wishListMin.like_image = image.data.full_path;
        wishListMin.width = intFormat(image.data.width);
        wishListMin.height = intFormat(image.data.height);
        return this;
    }

    public static int intFormat(String number){
        if(number == null) return 0;
        if(number.length() < 1) return 0;
        if(!number.matches(".*\\d+.*")) return 0;
        return Integer.parseInt(number.replaceAll("[*a-zA-Z]", "").replaceAll(" ", ""));
    }

    public class WishListMin{
        public int id;
        public boolean owned;
        public String name;
        public boolean is_verified;
        public boolean for_update = false;
        public String category;
        public String time_passed;
        public String title;
        public int comment_count;
        public int liker_count;
        public String avatar;
        public boolean is_liked = false;
        public boolean walkthrough_on_process_post = false;
        public boolean walkthrough_on_process_like = false;
        public String like_image;
        public int width;
        public int height;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;

        WishListItem wishListItem = (WishListItem) obj;
        return this.id == wishListItem.id;
    }
}
