package com.thingsilikeapp.data.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI05 on 9/22/2017.
 */

public class AddressBookItem {

    @SerializedName("id")
    public int id;

    @SerializedName("user_id")
    public int user_id;

    @SerializedName("address_label")
    public String address_label;

    @SerializedName("receiver_name")
    public String receiver_name;

    @SerializedName("phone_country_dial_code")
    public String phone_country_dial_code;

    @SerializedName("phone_country_code")
    public String phone_country_code;

    @SerializedName("phone_number")
    public String phone_number;

    @SerializedName("zip_code")
    public String zip_code;

    @SerializedName("state")
    public String state;

    @SerializedName("city")
    public String city;

    @SerializedName("street_address")
    public String street_address;

    @SerializedName("country")
    public String country;

    @SerializedName("country_code")
    public String country_code;

    @SerializedName("address")
    public String address;

    @SerializedName("is_default")
    public String is_default;

    public boolean selected = false;

    public class Errors{
        @SerializedName("address_label")
        public List<String> address_label = null;

        @SerializedName("receiver_name")
        public List<String> receiver_name = null;

        @SerializedName("phone_country_dial_code")
        public List<String> phone_country_dial_code = null;

        @SerializedName("phone_country_code")
        public List<String> phone_country_code = null;

        @SerializedName("phone_number")
        public List<String> phone_number = null;

        @SerializedName("zip_code")
        public List<String> zip_code = null;

        @SerializedName("state")
        public List<String> state = null;

        @SerializedName("city")
        public List<String> city = null;

        @SerializedName("street_address")
        public List<String> street_address = null;

        @SerializedName("country")
        public List<String> country = null;

        @SerializedName("country_code")
        public List<String> country_code = null;

    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static AddressBookItem fromJson(String json){
        return new Gson().fromJson(json, AddressBookItem.class);
    }
}
