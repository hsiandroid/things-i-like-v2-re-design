package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class DaysItem {

    @SerializedName("id")
    public int id;

    @SerializedName("day")
    public int day;

    @SerializedName("year")
    public int year;

    @SerializedName("month")
    public int month;
}
