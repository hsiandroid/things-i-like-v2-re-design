package com.thingsilikeapp.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class TrackerItem implements Parcelable {

    @SerializedName("id")
    public int id;

    @SerializedName("wishlist_id")
    public int wishlist_id;

    @SerializedName("user_id")
    public int user_id;

    @SerializedName("completed")
    public boolean completed;

    @SerializedName("title")
    public String title;

    @SerializedName("time_passed")
    public String time_passed;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.wishlist_id);
        dest.writeInt(this.user_id);
        dest.writeByte(this.completed ? (byte) 1 : (byte) 0);
        dest.writeString(this.title);
        dest.writeString(this.time_passed);
    }

    public TrackerItem() {
    }

    protected TrackerItem(Parcel in) {
        this.id = in.readInt();
        this.wishlist_id = in.readInt();
        this.user_id = in.readInt();
        this.completed = in.readByte() != 0;
        this.title = in.readString();
        this.time_passed = in.readString();
    }

    public static final Parcelable.Creator<TrackerItem> CREATOR = new Parcelable.Creator<TrackerItem>() {
        @Override
        public TrackerItem createFromParcel(Parcel source) {
            return new TrackerItem(source);
        }

        @Override
        public TrackerItem[] newArray(int size) {
            return new TrackerItem[size];
        }
    };
}
