package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class EncashItem {

    @SerializedName("id")
    public int id;

    @SerializedName("user_id")
    public int userId;

    @SerializedName("bank_name")
    public String bankName;

    @SerializedName("bank_address")
    public String bankAddress;

    @SerializedName("email")
    public String email;

    @SerializedName("account_name")
    public String accountName;

    @SerializedName("account_number")
    public String accountNumber;

    @SerializedName("contact_number")
    public String contactNumber;

    @SerializedName("country_iso")
    public String countryIso;

    @SerializedName("country_code")
    public String countryCode;

    @SerializedName("gem_qty")
    public String gemQty;

    @SerializedName("currency")
    public String currency;

    @SerializedName("final_amount")
    public double finalAmount;

    @SerializedName("service_fee")
    public int serviceFee;

    @SerializedName("service_fee_percentage")
    public int serviceFeePercentage;

    @SerializedName("request_date")
    public String requestDate;

    @SerializedName("approval_date")
    public String approvalDate;

    @SerializedName("release_date")
    public String releaseDate;

    @SerializedName("status")
    public String status;
}
