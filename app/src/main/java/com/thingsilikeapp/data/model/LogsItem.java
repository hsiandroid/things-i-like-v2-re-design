package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class LogsItem {


    @SerializedName("id")
    public int id;

    @SerializedName("encashment_id")
    public String encashmentId;

    @SerializedName("refno")
    public String refno;

    @SerializedName("remarks")
    public String remarks;

    @SerializedName("created_at")
    public CreatedAt createdAt;

    public static class CreatedAt {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public Timestamp timestamp;

        public static class Timestamp {
            @SerializedName("date")
            public String date;

            @SerializedName("timezone_type")
            public int timezoneType;

            @SerializedName("timezone")
            public String timezone;
        }
    }
}
