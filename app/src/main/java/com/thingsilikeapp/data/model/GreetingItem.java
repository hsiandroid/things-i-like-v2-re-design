package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class GreetingItem {

    @SerializedName("id")
    public int id;

    @SerializedName("year")
    public String year;

    @SerializedName("is_read")
    public String is_read;

    @SerializedName("type")
    public String type ;

    @SerializedName("image")
    public Image image ;

    public GreetingItem(){
        image = new Image();
        image.full_path = "https://tiltesting.blob.core.windows.net/data/images/greetings/bday/20171014/resized/Wlr0kc0mXX3FVI6XvSTb101420170726.jpeg";
    }

    public static class Image{

        @SerializedName("filename")
        public String filename ;

        @SerializedName("path")
        public String path ;

        @SerializedName("directory")
        public String directory ;

        @SerializedName("full_path")
        public String full_path ;

        @SerializedName("thumb_path")
        public String thumb_path ;
    }

}
