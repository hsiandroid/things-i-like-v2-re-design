package com.thingsilikeapp.data.model;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.io.File;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class ConvertFileItem {

    @SerializedName("id")
    public int id;

    @SerializedName("file")
    public File file;

    @SerializedName("isSelected")
    public boolean is_selected = false;

    @SerializedName("selectedFilter")
    public String selectedFilter = "Normal";
}
