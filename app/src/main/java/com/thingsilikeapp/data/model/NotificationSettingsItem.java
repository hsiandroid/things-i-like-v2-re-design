package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI05 on 9/22/2017.
 */

public class NotificationSettingsItem {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("code")
    public String code;

    @SerializedName("selected")
    public boolean selected;
}
