package com.thingsilikeapp.data.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class UserItem implements Parcelable{

    @SerializedName("id")
    public int id = 0;

    @SerializedName("common_name")
    public String common_name;

    @SerializedName("chat_id")
    public String chat_id;

    @SerializedName("name")
    public String name = " ";

    @SerializedName("username")
    public String username;

    @SerializedName("walkthrough_on_process")
    public boolean walkthrough_on_process = false;

    @SerializedName("walkthrough_on_process_avatar")
    public boolean walkthrough_on_process_avatar = false;

    @SerializedName("email")
    public String email;

    @SerializedName("is_online")
    public boolean is_online;

    @SerializedName("walkthrough")
    public boolean walkthrough = false;

    @SerializedName("birthdate")
    public String birthdate;

    @SerializedName("bday_display")
    public String bday_display;

    @SerializedName("fb_id")
    public String fb_id;

    @SerializedName("image")
    public String image;

    @SerializedName("is_verified")
    public boolean is_verified;

    @SerializedName("is_birthday")
    public boolean is_birthday;

    @SerializedName("is_greeted")
    public boolean is_greeted;

    @SerializedName("is_event_greeted")
    public boolean is_event_greeted;

    @SerializedName("max_fragment")
    public String max_fragment = "";

    @SerializedName("lykagem_display")
    public String lykagem_display = "";

    @SerializedName("reward_crystal")
    public double reward_crystal = 0.0;

    @SerializedName("reward_fragment")
    public double reward_fragment = 0;

    @SerializedName("allow_greeting")
    public boolean allow_greeting;

    @SerializedName("info")
    public Info info;

    @SerializedName("social")
    public Social social;

    @SerializedName("statistics")
    public Statistics statistics;

    @SerializedName("is_loading_follow")
    public boolean is_loading_follow;

    @SerializedName("isChecked")
    public boolean isChecked;

    @SerializedName("kyc")
    public K_Y_C kyc;

    public static class Wallet{
        @SerializedName("data")
        public WalletModel data;
    }
    public static class K_Y_C implements Parcelable{
        @SerializedName("data")
        public KYCItem data;

        protected K_Y_C(Parcel in) {
        }

        public static final Creator<K_Y_C> CREATOR = new Creator<K_Y_C>() {
            @Override
            public K_Y_C createFromParcel(Parcel in) {
                return new K_Y_C(in);
            }

            @Override
            public K_Y_C[] newArray(int size) {
                return new K_Y_C[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
        }
    }

    @SerializedName("wallet")
    public Wallet wallet;



    protected UserItem(Parcel in) {
        id = in.readInt();
        common_name = in.readString();
        name = in.readString();
        username = in.readString();
        walkthrough_on_process = in.readByte() != 0;
        walkthrough_on_process_avatar = in.readByte() != 0;
        email = in.readString();
        birthdate = in.readString();
        bday_display = in.readString();
        fb_id = in.readString();
        image = in.readString();
        is_verified = in.readByte() != 0;
        is_birthday = in.readByte() != 0;
        is_greeted = in.readByte() != 0;
        is_event_greeted = in.readByte() != 0;
        max_fragment = in.readString();
        lykagem_display = in.readString();
        reward_crystal = in.readInt();
        reward_fragment = in.readInt();
        allow_greeting = in.readByte() != 0;
        is_loading_follow = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(common_name);
        dest.writeString(name);
        dest.writeString(username);
        dest.writeByte((byte) (walkthrough_on_process ? 1 : 0));
        dest.writeByte((byte) (walkthrough_on_process_avatar ? 1 : 0));
        dest.writeString(email);
        dest.writeString(birthdate);
        dest.writeString(bday_display);
        dest.writeString(fb_id);
        dest.writeString(image);
        dest.writeByte((byte) (is_verified ? 1 : 0));
        dest.writeByte((byte) (is_birthday ? 1 : 0));
        dest.writeByte((byte) (is_greeted ? 1 : 0));
        dest.writeByte((byte) (is_event_greeted ? 1 : 0));
        dest.writeString(max_fragment);
        dest.writeString(lykagem_display);
        dest.writeDouble(reward_crystal);
        dest.writeDouble(reward_fragment);
        dest.writeByte((byte) (allow_greeting ? 1 : 0));
        dest.writeByte((byte) (is_loading_follow ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserItem> CREATOR = new Creator<UserItem>() {
        @Override
        public UserItem createFromParcel(Parcel in) {
            return new UserItem(in);
        }

        @Override
        public UserItem[] newArray(int size) {
            return new UserItem[size];
        }
    };

    public String pronoun(String male, String female){
        return getGender().equalsIgnoreCase("female") ? female : male;
    }

    public String getGender(){
        if(info == null){
            return "male";
        }
        if(info.data == null){
            return "male";
        }
        if(info.data.gender == null){
            return "male";
        }
        return info.data.gender;
    }

    public UserItem() {
    }

    public String getAvatar(){
        if(image == null) return "/";

        if(image.equals("/resized/") || image.equals("") || image.equals("/")){
            return "https://graph.facebook.com/" + fb_id + "/picture?type=large";
        }else{
            return image;
        }
    }

    public static class Statistics implements Parcelable {

        @SerializedName("data")
        public StatisticsData data;

        public static class StatisticsData implements Parcelable{

            @SerializedName("total_wishlist")
            public int total_wishlist;

            @SerializedName("sent_gifts")
            public int sent_gifts;

            @SerializedName("received_gifts")
            public int received_gifts;

            @SerializedName("followers")
            public int followers;

            @SerializedName("following")
            public int following;

            @SerializedName("ongoing_ireceived")
            public boolean ongoing_ireceived;

            @SerializedName("ongoing_igave")
            public boolean ongoing_igave;


            protected StatisticsData(Parcel in) {
                total_wishlist = in.readInt();
                sent_gifts = in.readInt();
                received_gifts = in.readInt();
                followers = in.readInt();
                following = in.readInt();
                ongoing_ireceived = in.readByte() != 0;
                ongoing_igave = in.readByte() != 0;
            }

            public static final Creator<StatisticsData> CREATOR = new Creator<StatisticsData>() {
                @Override
                public StatisticsData createFromParcel(Parcel in) {
                    return new StatisticsData(in);
                }

                @Override
                public StatisticsData[] newArray(int size) {
                    return new StatisticsData[size];
                }
            };

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(total_wishlist);
                parcel.writeInt(sent_gifts);
                parcel.writeInt(received_gifts);
                parcel.writeInt(followers);
                parcel.writeInt(following);
                parcel.writeByte((byte) (ongoing_ireceived ? 1 : 0));
                parcel.writeByte((byte) (ongoing_igave ? 1 : 0));
            }
        }

        public Statistics() {
        }

        protected Statistics(Parcel in) {
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Statistics> CREATOR = new Creator<Statistics>() {
            @Override
            public Statistics createFromParcel(Parcel in) {
                return new Statistics(in);
            }

            @Override
            public Statistics[] newArray(int size) {
                return new Statistics[size];
            }
        };
    }

    public static class Social implements Parcelable{

        @SerializedName("data")
        public SocialData data;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {

        }

        public static class SocialData implements Parcelable{

            @SerializedName("is_follower")
            public String is_follower;

            @SerializedName("is_following")
            public String is_following;

            @SerializedName("bday_reminder_id")
            public int bday_reminder_id;

            @SerializedName("is_my_account")
            public String is_my_account;

            @SerializedName("group")
            public String group;

            public SocialData() {
            }

            protected SocialData(Parcel in) {
                is_follower = in.readString();
                is_following = in.readString();
                bday_reminder_id = in.readInt();
                is_my_account = in.readString();
                group = in.readString();
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(is_follower);
                dest.writeString(is_following);
                dest.writeInt(bday_reminder_id);
                dest.writeString(is_my_account);
                dest.writeString(group);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public static final Creator<SocialData> CREATOR = new Creator<SocialData>() {
                @Override
                public SocialData createFromParcel(Parcel in) {
                    return new SocialData(in);
                }

                @Override
                public SocialData[] newArray(int size) {
                    return new SocialData[size];
                }
            };
        }

        public Social() {
        }

        protected Social(Parcel in) {
            this.data = in.readParcelable(SocialData.class.getClassLoader());
        }

        public static final Parcelable.Creator<Social> CREATOR = new Parcelable.Creator<Social>() {
            @Override
            public Social createFromParcel(Parcel source) {
                return new Social(source);
            }

            @Override
            public Social[] newArray(int size) {
                return new Social[size];
            }
        };
    }

    public static class Info implements Parcelable {

        @SerializedName("data")
        public InfoData data;

        public static class InfoData implements Parcelable {

            @SerializedName("gender")
            public String gender;

            @SerializedName("birthdate")
            public String birthdate = "";

            @SerializedName("profession")
            public String profession = "";

            @SerializedName("address")
            public String address;

            @SerializedName("street_address")
            public String street_address;

            @SerializedName("city")
            public String city;

            @SerializedName("state")
            public String state;

            @SerializedName("zip_code")
            public String zip_code;

            @SerializedName("my_privacy")
            public String my_privacy = "only_me";

            @SerializedName("contact_number")
            public String contact_number;

            @SerializedName("country")
            public String country = "PH";

            @SerializedName("currency")
            public String currency = "Php";

            @SerializedName("rate")
            public double rate = 1;

            @SerializedName("member_since")
            public MemberSince member_since;

            @SerializedName("last_activity")
            public LastActivity last_activity;

            @SerializedName("last_login")
            public LastLogin last_login;

            @SerializedName("avatar")
            public Avatar avatar;

            public static class MemberSince implements Parcelable {

                @SerializedName("date_db")
                public String date_db;

                @SerializedName("month_year")
                public String month_year;

                @SerializedName("time_passed")
                public String time_passed;

                @SerializedName("timestamp")
                public Timestamp timestamp;

                public static class Timestamp {

                    @SerializedName("date")
                    public String date;

                    @SerializedName("timezone_type")
                    public int timezone_type;

                    @SerializedName("timezone")
                    public String timezone;

                    public Timestamp() {
                    }
                }

                public MemberSince() {
                }

                protected MemberSince(Parcel in) {
                    date_db = in.readString();
                    month_year = in.readString();
                    time_passed = in.readString();
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(date_db);
                    dest.writeString(month_year);
                    dest.writeString(time_passed);
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                public static final Creator<MemberSince> CREATOR = new Creator<MemberSince>() {
                    @Override
                    public MemberSince createFromParcel(Parcel in) {
                        return new MemberSince(in);
                    }

                    @Override
                    public MemberSince[] newArray(int size) {
                        return new MemberSince[size];
                    }
                };
            }

            public static class LastActivity implements Parcelable{
                public String date_db;
                public String month_year;
                public String time_passed;
                public String timestamp;

                public LastActivity() {
                }

                protected LastActivity(Parcel in) {
                    this.date_db = in.readString();
                    this.month_year = in.readString();
                    this.time_passed = in.readString();
                    this.timestamp = in.readString();
                }

                public static final Parcelable.Creator<LastActivity> CREATOR = new Parcelable.Creator<LastActivity>() {
                    @Override
                    public LastActivity createFromParcel(Parcel source) {
                        return new LastActivity(source);
                    }

                    @Override
                    public LastActivity[] newArray(int size) {
                        return new LastActivity[size];
                    }
                };

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel parcel, int i) {

                }
            }

            public static class LastLogin implements Parcelable {
                public String date_db;
                public String month_year;
                public String time_passed;
                public String timestamp;

                protected LastLogin(Parcel in) {
                    date_db = in.readString();
                    month_year = in.readString();
                    time_passed = in.readString();
                    timestamp = in.readString();
                }

                public static final Creator<LastLogin> CREATOR = new Creator<LastLogin>() {
                    @Override
                    public LastLogin createFromParcel(Parcel in) {
                        return new LastLogin(in);
                    }

                    @Override
                    public LastLogin[] newArray(int size) {
                        return new LastLogin[size];
                    }
                };

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.date_db);
                    dest.writeString(this.month_year);
                    dest.writeString(this.time_passed);
                    dest.writeString(this.timestamp);
                }

                public LastLogin() {
                }
            }

            public static class Avatar{
                public String path;
                public String filename;
                public String directory;
                public String full_path;
                public String thumb_path;

                public Avatar() {
                }
            }

            public InfoData() {
            }

            protected InfoData(Parcel in) {
                gender = in.readString();
                birthdate = in.readString();
                profession = in.readString();
                address = in.readString();
                street_address = in.readString();
                city = in.readString();
                state = in.readString();
                zip_code = in.readString();
                my_privacy = in.readString();
                contact_number = in.readString();
                country = in.readString();
                member_since = in.readParcelable(MemberSince.class.getClassLoader());
                last_activity = in.readParcelable(LastActivity.class.getClassLoader());
                last_login = in.readParcelable(LastLogin.class.getClassLoader());
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(gender);
                dest.writeString(birthdate);
                dest.writeString(profession);
                dest.writeString(address);
                dest.writeString(street_address);
                dest.writeString(city);
                dest.writeString(state);
                dest.writeString(zip_code);
                dest.writeString(my_privacy);
                dest.writeString(contact_number);
                dest.writeString(country);
                dest.writeParcelable(member_since, flags);
                dest.writeParcelable(last_activity, flags);
                dest.writeParcelable(last_login, flags);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public static final Creator<InfoData> CREATOR = new Creator<InfoData>() {
                @Override
                public InfoData createFromParcel(Parcel in) {
                    return new InfoData(in);
                }

                @Override
                public InfoData[] newArray(int size) {
                    return new InfoData[size];
                }
            };
        }

        public Info() {
        }

        protected Info(Parcel in) {
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Info> CREATOR = new Creator<Info>() {
            @Override
            public Info createFromParcel(Parcel in) {
                return new Info(in);
            }

            @Override
            public Info[] newArray(int size) {
                return new Info[size];
            }
        };
    }
}
