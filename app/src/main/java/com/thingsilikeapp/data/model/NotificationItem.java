package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class NotificationItem {

    @SerializedName("id")
    public String id;

    @SerializedName("reference_id")
    public int reference_id;

    @SerializedName("type")
    public String type;

    @SerializedName("title")
    public String title;

    @SerializedName("content")
    public String content;

    @SerializedName("thumbnail")
    public String thumbnail;

    @SerializedName("is_read")
    public String is_read;

    @SerializedName("time_passed")
    public String time_passed;

    @SerializedName("is_deleted")
    public boolean is_deleted = false;

    @SerializedName("deleted_display")
    public String deleted_display = "";

    @SerializedName("user")
    public User user;

    @SerializedName("selected")
    public boolean selected;

    public static class User {

        @SerializedName("data")
        public UserItem data;
    }
}
