package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.vendor.server.base.AndroidModel;


/**
 * Created by Labyalo on 8/12/2017.
 */

public class ChatUserModel extends AndroidModel {


    @SerializedName("chat_id")
    public int chatId;

    @SerializedName("user_id")
    public int userId;

    @SerializedName("nickname")
    public String nickname;

    @SerializedName("role")
    public String role;

    @SerializedName("info")
    public Info info;

    @SerializedName("author")
    public Author author;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ChatUserModel convertFromJson(String json) {
        return convertFromJson(json, ChatUserModel.class);
    }


    public static class Info {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("date_created")
            public DateCreated dateCreated;

            public static class DateCreated {
                @SerializedName("date_db")
                public String dateDb;

                @SerializedName("month_year")
                public String monthYear;

                @SerializedName("time_passed")
                public String timePassed;

                @SerializedName("timestamp")
                public Timestamp timestamp;

                public static class Timestamp {
                    @SerializedName("date")
                    public String date;

                    @SerializedName("timezone_type")
                    public int timezoneType;

                    @SerializedName("timezone")
                    public String timezone;
                }
            }
        }
    }

    public static class Author {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("id")
            public int idX;

            @SerializedName("name")
            public String name;

            @SerializedName("username")
            public String username;

            @SerializedName("type")
            public String type;

            @SerializedName("email")
            public String email;

            @SerializedName("is_verified")
            public String isVerified;

            @SerializedName("address1")
            public String address1;

            @SerializedName("address2")
            public String address2;

            @SerializedName("city")
            public String city;

            @SerializedName("state")
            public String state;

            @SerializedName("country_iso")
            public String countryIso;

            @SerializedName("country_code")
            public String countryCode;

            @SerializedName("contact_number")
            public String contactNumber;

            @SerializedName("full_contact_number")
            public String fullContactNumber;

            @SerializedName("info")
            public Info info;

            public static class Info {
                @SerializedName("data")
                public DataX data;

                public static class DataX {
                    @SerializedName("member_since")

                    public MemberSince memberSince;
                    @SerializedName("last_activity")
                    public LastActivity lastActivity;

                    @SerializedName("last_login")
                    public LastLogin lastLogin;

                    @SerializedName("avatar")
                    public Avatar avatar;

                    public static class MemberSince {
                        @SerializedName("date_db")
                        public String dateDb;

                        @SerializedName("month_year")
                        public String monthYear;

                        @SerializedName("time_passed")
                        public String timePassed;

                        @SerializedName("timestamp")
                        public Timestamp timestamp;

                        public static class Timestamp {
                            @SerializedName("date")
                            public String date;

                            @SerializedName("timezone_type")
                            public int timezoneType;

                            @SerializedName("timezone")
                            public String timezone;
                        }
                    }

                    public static class LastActivity {
                        @SerializedName("date_db")
                        public String dateDb;

                        @SerializedName("month_year")
                        public String monthYear;

                        @SerializedName("time_passed")
                        public String timePassed;

                        @SerializedName("timestamp")
                        public String timestamp;
                    }

                    public static class LastLogin {
                        @SerializedName("date_db")
                        public String dateDb;

                        @SerializedName("month_year")
                        public String monthYear;

                        @SerializedName("time_passed")
                        public String timePassed;

                        @SerializedName("timestamp")
                        public String timestamp;
                    }

                    public static class Avatar {
                        @SerializedName("path")
                        public String path;

                        @SerializedName("filename")
                        public String filename;

                        @SerializedName("directory")
                        public String directory;

                        @SerializedName("full_path")
                        public String fullPath;

                        @SerializedName("thumb_path")
                        public String thumbPath;
                    }
                }
            }
        }
    }
}
