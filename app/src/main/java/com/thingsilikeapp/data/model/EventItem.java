package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class EventItem {
    @SerializedName("id")
    public int id = 0;

    @SerializedName("type")
    public String type = "birthday";

    @SerializedName("title")
    public String title = "Happy Birthday";

    @SerializedName("content")
    public String content;

    @SerializedName("image")
    public String image = "https://firebasestorage.googleapis.com/v0/b/sample-44188.appspot.com/o/Things%20I%20Like%2Fgive%20greeting.jpg?alt=media&token=89f28ef6-08b9-4f21-be1e-0395153f1313";

    @SerializedName("is_active")
    public boolean is_active;

    @SerializedName("is_today")
    public boolean is_today;
}
