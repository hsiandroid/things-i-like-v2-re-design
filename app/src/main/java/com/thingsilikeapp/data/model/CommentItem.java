package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI05 on 2/7/2017.
 */

public class CommentItem {
    @SerializedName("id")
    public int id;

    @SerializedName("temp_id")
    public int temp_id;

    @SerializedName("wishlist_id")
    public int wishlist_id;

    @SerializedName("user_id")
    public int user_id;

    @SerializedName("tagged_user_id")
    public int tagged_user_id;

    @SerializedName("content")
    public String content;

    @SerializedName("time_passed")
    public String time_passed;

    @SerializedName("author")
    public Author author;

    @SerializedName("tagged_user")
    public TaggedUser tagged_user;

    public CommentItem(){
        author = new Author();
        tagged_user = new TaggedUser();
    }

    public static class Author{

        @SerializedName("data")
        public UserItem data;
        public Author(){
            data = new UserItem();
        }
    }

    public static class TaggedUser{

        @SerializedName("data")
        public UserItem data;
        public TaggedUser(){
            data = new UserItem();
        }
    }

}
