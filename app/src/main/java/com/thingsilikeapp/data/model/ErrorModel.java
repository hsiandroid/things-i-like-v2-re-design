package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.vendor.server.base.AndroidModel;


import java.util.List;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ErrorModel extends AndroidModel {

    @SerializedName("password")
    public List<String> password = null;

    @SerializedName("old_password")
    public List<String> old_password = null;

    @SerializedName("current_password")
    public List<String> current_password = null;

    @SerializedName("description")
    public List<String> description = null;

    @SerializedName("fname")
    public List<String> fname = null;

    @SerializedName("lname")
    public List<String> lname = null;

    @SerializedName("email")
    public List<String> email = null;

    @SerializedName("title")
    public List<String> title = null;

    @SerializedName("content")
    public List<String> content = null;

    @SerializedName("name")
    public List<String> name = null;

    @SerializedName("username")
    public List<String> username = null;

    @SerializedName("country_iso")
    public List<String> country_iso = null;

    @SerializedName("country_code")
    public List<String> country_code = null;

    @SerializedName("city")
    public List<String> city = null;

    @SerializedName("contact_country")
    public List<String> contact_country = null;

    @SerializedName("contact_number")
    public List<String> contact_number = null;

    @SerializedName("address1")
    public List<String> address1 = null;

    @SerializedName("address2")
    public List<String> address2 = null;

    @SerializedName("state")
    public List<String> state = null;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ErrorModel convertFromJson(String json) {
        return convertFromJson(json, ErrorModel.class);
    }
}
