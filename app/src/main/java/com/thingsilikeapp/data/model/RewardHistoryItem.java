package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class RewardHistoryItem {

    @SerializedName("id")
    public int id;

    @SerializedName("reward_type")
    public String rewardType;

    @SerializedName("_reward_type")
    public String _rewardType;

    @SerializedName("item_type")
    public String itemType;

    @SerializedName("type")
    public String type;

    @SerializedName("value")
    public String value;

    @SerializedName("remarks")
    public String remarks;

    @SerializedName("title")
    public String title;

    @SerializedName("created_at")
    public CreatedAt createdAt;

    @SerializedName("user")
    public User user;

    @SerializedName("item")
    public Item item;

    @SerializedName("transaction")
    public Transaction transaction;

    @SerializedName("order")
    public Order order;

    @SerializedName("encashment")
    public Encashment encashment;

    public static class CreatedAt {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public Timestamp timestamp;

        @SerializedName("date")
        public String date;

        @SerializedName("time")
        public String time;

        public static class Timestamp {
            @SerializedName("date")
            public String date;

            @SerializedName("timezone_type")
            public int timezoneType;

            @SerializedName("timezone")
            public String timezone;
        }
    }

    public static class User {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("id")
            public int id;

            @SerializedName("is_online")
            public boolean isOnline;

            @SerializedName("online_description")
            public String onlineDescription;

            @SerializedName("chat_id")
            public String chatId;

            @SerializedName("common_name")
            public String commonName;

            @SerializedName("name")
            public String name;

            @SerializedName("username")
            public String username;

            @SerializedName("email")
            public String email;

            @SerializedName("fb_id")
            public String fbId;

            @SerializedName("image")
            public String image;

            @SerializedName("bday_reminder_id")
            public int bdayReminderId;

            @SerializedName("is_verified")
            public boolean isVerified;

            @SerializedName("is_birthday")
            public boolean isBirthday;

            @SerializedName("is_greeted")
            public boolean isGreeted;

            @SerializedName("is_event_greeted")
            public boolean isEventGreeted;

            @SerializedName("birthdate")
            public String birthdate;

            @SerializedName("bday_display")
            public String bdayDisplay;

            @SerializedName("allow_greeting")
            public boolean allowGreeting;

            @SerializedName("reward_crystal")
            public String rewardCrystal;

            @SerializedName("lykagem")
            public String lykagem;

            @SerializedName("lykagem_display")
            public String lykagemDisplay;

            @SerializedName("reward_fragment")
            public String rewardFragment;

            @SerializedName("max_fragment")
            public int maxFragment;
        }
    }

    public static class Item {
        @SerializedName("data")
        public WishListItem data;

//        public static class Data {
//            @SerializedName("id")
//            public int id;
//
//            @SerializedName("pattern")
//            public int pattern;
//
//            @SerializedName("parent_id")
//            public int parentId;
//
//            @SerializedName("title")
//            public String title;
//
//            @SerializedName("category")
//            public String category;
//
//            @SerializedName("status")
//            public String status;
//
//            @SerializedName("comment_count")
//            public int commentCount;
//
//            @SerializedName("for_display_comment")
//            public String forDisplayComment;
//
//            @SerializedName("ongoing_viewer_count")
//            public int ongoingViewerCount;
//
//            @SerializedName("for_display_ongoing_viewer")
//            public String forDisplayOngoingViewer;
//
//            @SerializedName("liker_count")
//            public int likerCount;
//
//            @SerializedName("for_display_liker")
//            public String forDisplayLiker;
//
//            @SerializedName("repost_count")
//            public String repostCount;
//
//            @SerializedName("for_display_repost")
//            public String forDisplayRepost;
//
//            @SerializedName("time_passed")
//            public String timePassed;
//
//            @SerializedName("is_sent")
//            public boolean isSent;
//
//            @SerializedName("is_liked")
//            public boolean isLiked;
//
//            @SerializedName("diamond_sent")
//            public int diamondSent;
//
//            @SerializedName("same_title")
//            public boolean sameTitle;
//
//            @SerializedName("post_type")
//            public String postType;
//        }
    }

    public static class Transaction {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("id")
            public int id;

            @SerializedName("user_id")
            public String userId;

            @SerializedName("created_at")
            public CreatedAt createdAt;

            public static class CreatedAt {
                @SerializedName("date_db")
                public String dateDb;

                @SerializedName("month_year")
                public String monthYear;

                @SerializedName("time_passed")
                public String timePassed;

                @SerializedName("timestamp")
                public String timestamp;
            }
        }
    }

    public static class Order {
        @SerializedName("data")
        public OrderItem data;

//        public static class Data {
//            @SerializedName("id")
//            public int id;
//
//            @SerializedName("display_id")
//            public String displayId;
//
//            @SerializedName("user_id")
//            public String userId;
//
//            @SerializedName("payment_status")
//            public String paymentStatus;
//
//            @SerializedName("transaction_status")
//            public String transactionStatus;
//
//            @SerializedName("status_display")
//            public String statusDisplay;
//
//            @SerializedName("total")
//            public String total;
//
//            @SerializedName("addressbook_id")
//            public String addressbookId;
//
//            @SerializedName("delivery_street")
//            public String deliveryStreet;
//
//            @SerializedName("delivery_city")
//            public String deliveryCity;
//
//            @SerializedName("delivery_state")
//            public String deliveryState;
//
//            @SerializedName("delivery_country")
//            public String deliveryCountry;
//
//            @SerializedName("delivery_zip")
//            public String deliveryZip;
//
//            @SerializedName("contact_number")
//            public String contactNumber;
//
//            @SerializedName("note")
//            public String note;
//
//            @SerializedName("purchased_date")
//            public PurchasedDate purchasedDate;
//
////            @SerializedName("created_at")
////            public CreatedAt createdAt;
//
//            public static class PurchasedDate {
//                @SerializedName("date_db")
//                public String dateDb;
//
//                @SerializedName("month_year")
//                public String monthYear;
//
//                @SerializedName("time_passed")
//                public String timePassed;
//
//                @SerializedName("timestamp")
//                public String timestamp;
//            }
////
////            public static class CreatedAt {
////                @SerializedName("date_db")
////                public String dateDb;
////
////                @SerializedName("month_year")
////                public String monthYear;
////
////                @SerializedName("time_passed")
////                public String timePassed;
////
////                @SerializedName("timestamp")
////                public String timestamp;
////            }
//        }
    }

    public static class Encashment {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("id")
            public int id;

            @SerializedName("refno")
            public String refno;

            @SerializedName("user_id")
            public String userId;

            @SerializedName("bank_name")
            public String bankName;

            @SerializedName("bank_address")
            public String bankAddress;

            @SerializedName("bank_swift_code")
            public String bankSwiftCode;

            @SerializedName("email")
            public String email;

            @SerializedName("account_name")
            public String accountName;

            @SerializedName("account_number")
            public String accountNumber;

            @SerializedName("account_address")
            public String accountAddress;

            @SerializedName("contact_number")
            public String contactNumber;

            @SerializedName("country_iso")
            public String countryIso;

            @SerializedName("country_code")
            public String countryCode;

            @SerializedName("gem_qty")
            public String gemQty;

            @SerializedName("rate")
            public String rate;

            @SerializedName("currency")
            public String currency;

            @SerializedName("final_amount")
            public String finalAmount;

            @SerializedName("service_fee")
            public String serviceFee;

            @SerializedName("service_fee_value")
            public int serviceFeeValue;

            @SerializedName("service_fee_percentage")
            public String serviceFeePercentage;

            @SerializedName("request_date")
            public String requestDate;

            @SerializedName("approval_date")
            public String approvalDate;

            @SerializedName("release_date")
            public String releaseDate;

            @SerializedName("status")
            public String status;

            @SerializedName("remarks")
            public String remarks;
        }
    }
}
