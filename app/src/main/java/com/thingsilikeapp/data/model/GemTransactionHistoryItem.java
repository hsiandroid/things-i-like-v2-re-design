package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class GemTransactionHistoryItem {
    @SerializedName("id")
    public int id;

    @SerializedName("amount")
    public String amount = "";

    @SerializedName("title")
    public String title = "";

    @SerializedName("date")
    public String date = "";

    @SerializedName("reference_number")
    public String reference_number = "";

    @SerializedName("currency_from")
    public String currency_from = "";

    @SerializedName("currency_to")
    public String currency_to = "";

}
