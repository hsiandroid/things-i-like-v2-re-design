package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class BuyGemItem {

    @SerializedName("bonus_gem_rate")
    public double bonusGemRate;

    @SerializedName("withdrawal_fee_rate")
    public String withdrawalFeeRate;
}
