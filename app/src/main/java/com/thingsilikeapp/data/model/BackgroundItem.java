package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class BackgroundItem {
    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("sorting")
    public String sorting;

    @SerializedName("is_active")
    public boolean is_active;

    @SerializedName("image")
    private String image = "/";

    public String getImage() {
        return image.equals("") ? "/" : image;
    }
}
