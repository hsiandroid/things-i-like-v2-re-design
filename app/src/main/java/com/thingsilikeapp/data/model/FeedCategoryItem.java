package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class FeedCategoryItem {

    @SerializedName("type")
    public String type;

    @SerializedName("has_morepages")
    public boolean has_morepages;

    @SerializedName("data")
    public List<WishListItem> data ;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;

        FeedCategoryItem feedCategoryItem = (FeedCategoryItem) obj;
        return this.type.equalsIgnoreCase(feedCategoryItem.type);
    }

}
