package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class GetCountryItem {

    @SerializedName("id")
    public int id;

    @SerializedName("ip")
    public String ip;

    @SerializedName("city")
    public String city;

    @SerializedName("state")
    public String state;

    @SerializedName("lat")
    public double lat;

    @SerializedName("long")
    public double longX;

    @SerializedName("country")
    public String country;

    @SerializedName("country_name")
    public String countryName;

    @SerializedName("country_currency")
    public String countryCurrency;

    @SerializedName("timezone")
    public String timezone;
}
