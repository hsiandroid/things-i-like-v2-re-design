package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class CategoryItem {
    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("code")
    public String code;

    @SerializedName("parent")
    public String parent;

    @SerializedName("image")
    public String image;
}
