package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI05 on 9/22/2017.
 */

public class GroupItem {

    @SerializedName("title")
    public String title;

    public String other;

    public boolean selected;

    public GroupItem(){

    }

    public GroupItem(String title){
        this.title = title;
    }
}
