package com.thingsilikeapp.data.model;

import android.content.Context;
import android.net.Uri;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.java.DateFormatter;
import com.thingsilikeapp.vendor.server.base.AndroidModel;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Created by Labyalo on 8/12/2017.
 */

public class ChatThreadModel extends AndroidModel {

    @SerializedName("temp_id")
    public int temp_id;

    @SerializedName("sender_user_id")
    public int senderUserId;

    @SerializedName("content")
    public String content;

    @SerializedName("type")
    public String type;

    @SerializedName("info")
    public Info info;

    @SerializedName("participant")
    public Participant participant;

    @SerializedName("author")
    public Author author;

    private File file;
    public String status = " ";
    public static final String SENDING = "";
    public static final String SENT = "sent";
    public static final String FAILED = "failed";

    public static class Author{

        @SerializedName("data")
        public UserItem data;
    }

    public static class Info {
        @SerializedName("data")
        public DataBean data;

        public static class DataBean {
            @SerializedName("date_created")
            public DateCreated dateCreated;

            @SerializedName("attachment")
            public Attachment attachment;

            public static class DateCreated {
                @SerializedName("date_db")
                public String dateDb;

                @SerializedName("month_year")
                public String monthYear;

                @SerializedName("time_passed")
                public String timePassed;

                @SerializedName("timestamp")
                public Object timestamp;
            }

            public static class Attachment {
                @SerializedName("filename")
                public String filename;

                @SerializedName("path")
                public String path;

                @SerializedName("directory")
                public String directory;

                @SerializedName("size")
                public int size;

                @SerializedName("full_path")
                public String fullPath;

                @SerializedName("thumb_path")
                public String thumbPath;
            }
        }
    }

    public static class Participant {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("id")
            public int idX;

            @SerializedName("chat_id")
            public int chatId;

            @SerializedName("user_id")
            public int userId;

            @SerializedName("nickname")
            public String nickname;

            @SerializedName("role")
            public String role;

            @SerializedName("author")
            public Author author;

            public static class Author {
                @SerializedName("data")
                public DataX data;

                public static class DataX {
                    @SerializedName("id")
                    public int idX;

                    @SerializedName("name")
                    public String name;

                    @SerializedName("username")
                    public String username;

                    @SerializedName("type")
                    public String type;

                    @SerializedName("specialty_id")
                    public int specialtyId;

                    @SerializedName("specialty")
                    public String specialty;

                    @SerializedName("email")
                    public String email;

                    @SerializedName("description")
                    public String description;

                    @SerializedName("is_verified")
                    public String isVerified;

                    @SerializedName("address1")
                    public String address1;

                    @SerializedName("address2")
                    public String address2;

                    @SerializedName("city")
                    public String city;

                    @SerializedName("state")
                    public String state;

                    @SerializedName("country_iso")
                    public String countryIso;

                    @SerializedName("country_code")
                    public String countryCode;

                    @SerializedName("contact_number")
                    public String contactNumber;

                    @SerializedName("full_contact_number")
                    public String fullContactNumber;

                    @SerializedName("info")
                    public Info info;

                    public static class Info {
                        @SerializedName("data")
                        public DataXX data;

                        public static class DataXX {
                            @SerializedName("member_since")
                            public MemberSince memberSince;

                            @SerializedName("last_activity")
                            public LastActivity lastActivity;

                            @SerializedName("last_login")
                            public LastLogin lastLogin;

                            @SerializedName("avatar")
                            public Avatar avatar;

                            public static class MemberSince {
                                @SerializedName("date_db")
                                public String dateDb;

                                @SerializedName("month_year")
                                public String monthYear;

                                @SerializedName("time_passed")
                                public String timePassed;

                                @SerializedName("timestamp")
                                public Timestamp timestamp;

                                public static class Timestamp {
                                    @SerializedName("date")
                                    public String date;

                                    @SerializedName("timezone_type")
                                    public int timezoneType;

                                    @SerializedName("timezone")
                                    public String timezone;
                                }
                            }

                            public static class LastActivity {
                                @SerializedName("date_db")
                                public String dateDb;

                                @SerializedName("month_year")
                                public String monthYear;

                                @SerializedName("time_passed")
                                public String timePassed;

                                @SerializedName("timestamp")
                                public Object timestamp;
                            }

                            public static class LastLogin {
                                @SerializedName("date_db")
                                public String dateDb;

                                @SerializedName("month_year")
                                public String monthYear;

                                @SerializedName("time_passed")
                                public String timePassed;

                                @SerializedName("timestamp")
                                public Object timestamp;
                            }

                            public static class Avatar {
                                @SerializedName("path")
                                public String path;

                                @SerializedName("filename")
                                public String filename;

                                @SerializedName("directory")
                                public String directory;

                                @SerializedName("full_path")
                                public String fullPath;

                                @SerializedName("thumb_path")
                                public String thumbPath;
                            }
                        }
                    }
                }
            }
        }
    }

    public static ChatThreadModel generateMessage(Context context, int temp_id, String message, File file, String type){

        Date date = new Date();
        String strDateFormat = "hh:mm a";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat, Locale.getDefault());
        String formattedDate= dateFormat.format(date);

        ChatThreadModel chatThreadModel = new ChatThreadModel();
        chatThreadModel.id = temp_id;
        chatThreadModel.temp_id = temp_id;
        chatThreadModel.type = type;
        chatThreadModel.content = message;
        chatThreadModel.status = ChatThreadModel.SENDING;
        chatThreadModel.senderUserId = UserData.getUserId();
        chatThreadModel.author = new ChatThreadModel.Author();
        chatThreadModel.author.data = new UserItem();
        chatThreadModel.author.data.image = UserData.getUserItem().image;
        chatThreadModel.info = new ChatThreadModel.Info();
        chatThreadModel.file = file;
        chatThreadModel.info.data = new ChatThreadModel.Info.DataBean();
        chatThreadModel.info.data.dateCreated = new ChatThreadModel.Info.DataBean.DateCreated();
        chatThreadModel.info.data.dateCreated.timePassed = formattedDate;
        chatThreadModel.info.data.attachment = new Info.DataBean.Attachment();

        if(file != null && file.isFile()){
            chatThreadModel.info.data.attachment.filename = file.getName();
            chatThreadModel.info.data.attachment.fullPath = file.getAbsolutePath();
        }

        return chatThreadModel;
    }

    @NotNull
    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ChatThreadModel convertFromJson(String json) {
        return convertFromJson(json, ChatThreadModel.class);
    }

}
