package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class CurrencyItem {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("flag")
    public String flag;

    @SerializedName("currency")
    public String currency;


}
