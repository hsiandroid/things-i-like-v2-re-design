package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class BirthdayItem {

    @SerializedName("header")
    public String header;

    @SerializedName("month")
    public int month;

    @SerializedName("year")
    public int year ;

    @SerializedName("users")
    public User users ;

    public static class User{
        @SerializedName("data")
        public List<UserItem> data;
    }

}
