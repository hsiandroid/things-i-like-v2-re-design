package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class ContactsItem {

    @SerializedName("id")
    public int id;

    @SerializedName("image")
    public String image;

    @SerializedName("name")
    public String name;

    @SerializedName("number")
    public String number;

    public boolean selected;


}
