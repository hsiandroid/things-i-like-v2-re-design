package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class WalletItem {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("amount")
    public String amount;

    @SerializedName("refid")
    public String refid;

    @SerializedName("avatar")
    public String avatar;
}
