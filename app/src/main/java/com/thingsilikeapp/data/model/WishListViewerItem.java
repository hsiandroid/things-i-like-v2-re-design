package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/16/2017.
 */

public class WishListViewerItem {

    @SerializedName("id")
    public int id;

    @SerializedName("other_user_id")
    public int other_user_id;

    @SerializedName("content")
    public String content;

    @SerializedName("type")
    public String type;

    @SerializedName("address_label")
    public String address_label;

    @SerializedName("receiver_name")
    public String receiver_name;

    @SerializedName("phone_country_dial_code")
    public String phone_country_dial_code;

    @SerializedName("phone_country_code")
    public String phone_country_code;

    @SerializedName("phone_number")
    public String phone_number;

    @SerializedName("zip_code")
    public String zip_code;

    @SerializedName("state")
    public String state;

    @SerializedName("city")
    public String city;

    @SerializedName("address")
    public String address;

    @SerializedName("street_address")
    public String street_address;

    @SerializedName("country")
    public String country;

    @SerializedName("country_code")
    public String country_code;


    @SerializedName("status")
    public String status;

    @SerializedName("time_passed")
    public String time_passed;

    @SerializedName("wishlist")
    public Wishlist wishlist;

    @SerializedName("other_user")
    public OtherUser other_user;

    @SerializedName("viewer")
    public Viewer viewer;

    public static class Wishlist {

        @SerializedName("data")
        public WishListItem data;
    }


    public static class OtherUser {

        @SerializedName("data")
        public UserItem data;
    }

    public static class Viewer {

        @SerializedName("data")
        public UserItem data;
    }

    public AddressBookItem getAddressBook(){
        AddressBookItem addressBookItem = new AddressBookItem();
        addressBookItem.address_label = address_label;
        addressBookItem.receiver_name = receiver_name;
        addressBookItem.phone_country_dial_code = phone_country_dial_code;
        addressBookItem.phone_country_code = phone_country_code;
        addressBookItem.phone_number = phone_number;
        addressBookItem.zip_code = zip_code;
        addressBookItem.state = state;
        addressBookItem.city = city;
        addressBookItem.street_address = street_address;
        addressBookItem.country = country;
        addressBookItem.country_code = country_code;
        addressBookItem.address = address;
        return addressBookItem;
    }
}
