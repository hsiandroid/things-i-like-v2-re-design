package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

public class ChangeEmailItem {


    @SerializedName("id")
    public int id;

    @SerializedName("is_online")
    public boolean isOnline;

    @SerializedName("online_description")
    public String onlineDescription;

    @SerializedName("chat_id")
    public String chatId;

    @SerializedName("common_name")
    public String commonName;

    @SerializedName("name")
    public String name;

    @SerializedName("username")
    public String username;

    @SerializedName("email")
    public String email;

    @SerializedName("fb_id")
    public Object fbId;

    @SerializedName("image")
    public String image;

    @SerializedName("bday_reminder_id")
    public int bdayReminderId;

    @SerializedName("is_verified")
    public boolean isVerified;

    @SerializedName("is_birthday")
    public boolean isBirthday;

    @SerializedName("is_greeted")
    public boolean isGreeted;

    @SerializedName("is_event_greeted")
    public boolean isEventGreeted;

    @SerializedName("birthdate")
    public String birthdate;

    @SerializedName("bday_display")
    public String bdayDisplay;

    @SerializedName("allow_greeting")
    public boolean allowGreeting;

    @SerializedName("reward_crystal")
    public String rewardCrystal;

    @SerializedName("lykagem")
    public String lykagem;

    @SerializedName("lykagem_display")
    public String lykagemDisplay;

    @SerializedName("reward_fragment")
    public String rewardFragment;

    @SerializedName("max_fragment")
    public int maxFragment;
}
