package com.thingsilikeapp.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class TimelineItem implements Parcelable {

    @SerializedName("id")
    public int id;

    @SerializedName("wishlist_id")
    public int wishlist_id;

    @SerializedName("user_id")
    public int user_id;

    @SerializedName("content")
    public String content;

    @SerializedName("time_passed")
    public String time_passed;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.wishlist_id);
        dest.writeInt(this.user_id);
        dest.writeString(this.content);
        dest.writeString(this.time_passed);
    }

    public TimelineItem() {
    }

    protected TimelineItem(Parcel in) {
        this.id = in.readInt();
        this.wishlist_id = in.readInt();
        this.user_id = in.readInt();
        this.content = in.readString();
        this.time_passed = in.readString();
    }

    public static final Parcelable.Creator<TimelineItem> CREATOR = new Parcelable.Creator<TimelineItem>() {
        @Override
        public TimelineItem createFromParcel(Parcel source) {
            return new TimelineItem(source);
        }

        @Override
        public TimelineItem[] newArray(int size) {
            return new TimelineItem[size];
        }
    };
}
