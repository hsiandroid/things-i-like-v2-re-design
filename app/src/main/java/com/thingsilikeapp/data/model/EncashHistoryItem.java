package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class EncashHistoryItem {

    @SerializedName("id")
    public int id;

    @SerializedName("refno")
    public String refno;

    @SerializedName("user_id")
    public String userId;

    @SerializedName("bank_name")
    public String bankName;

    @SerializedName("bank_address")
    public String bankAddress;

    @SerializedName("email")
    public String email;

    @SerializedName("account_name")
    public String accountName;

    @SerializedName("account_number")
    public String accountNumber;

    @SerializedName("contact_number")
    public String contactNumber;

    @SerializedName("country_iso")
    public String countryIso;

    @SerializedName("country_code")
    public String countryCode;

    @SerializedName("gem_qty")
    public String gemQty;

    @SerializedName("rate")
    public String rate;

    @SerializedName("currency")
    public String currency;

    @SerializedName("final_amount")
    public String finalAmount;

    @SerializedName("service_fee")
    public String serviceFee;

    @SerializedName("service_fee_value")
    public double serviceFeeValue;

    @SerializedName("service_fee_percentage")
    public String serviceFeePercentage;

    @SerializedName("request_date")
    public String requestDate;

    @SerializedName("approval_date")
    public String approvalDate;

    @SerializedName("release_date")
    public String releaseDate;

    @SerializedName("status")
    public String status;

    @SerializedName("remarks")
    public String remarks;

    @SerializedName("history")
    public History history;

    public static class History {
        @SerializedName("data")
        public List<LogsItem> data;

    }
}
