package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class BankCardItem {


    @SerializedName("id")
    public int id;

    @SerializedName("card_number")
    public String cardNumber;

    @SerializedName("card_type")
    public String cardType;

    @SerializedName("card_expiry_month")
    public String cardExpiryMonth;

    @SerializedName("card_expiry_year")
    public String cardExpiryYear;

    @SerializedName("is_selected")
    public boolean isSelected;
}
