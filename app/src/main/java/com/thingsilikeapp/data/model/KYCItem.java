package com.thingsilikeapp.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class KYCItem implements Parcelable {


    @SerializedName("email")
    public String email;

    @SerializedName("email_status")
    public String emailStatus;

    @SerializedName("email_status_display")
    public String emailStatusDisplay;

    @SerializedName("email_approval_date")
    public String emailApprovalDate;

    @SerializedName("email_remarks")
    public String emailRemarks;

    @SerializedName("country_iso")
    public String countryIso;

    @SerializedName("country_code")
    public String countryCode;

    @SerializedName("contact_number")
    public String contactNumber;

    @SerializedName("contact_number_status")
    public String contactNumberStatus;

    @SerializedName("contact_number_status_display")
    public String contactNumberStatusDisplay;

    @SerializedName("contact_number_approval_date")
    public String contactNumberApprovalDate;

    @SerializedName("contact_number_remarks")
    public String contactNumberRemarks;

    @SerializedName("id")
    public Id id;

    @SerializedName("id_status")
    public String idStatus;

    @SerializedName("id_status_display")
    public String idStatusDisplay;

    @SerializedName("id_type")
    public String idType;

    @SerializedName("id_approval_date")
    public String idApprovalDate;

    @SerializedName("id_remarks")
    public String idRemarks;

    @SerializedName("selfie")
    public Selfie selfie;

    @SerializedName("selfie_status")
    public String selfieStatus;

    @SerializedName("selfie_status_display")
    public String selfieStatusDisplay;

    @SerializedName("selfie_approval_date")
    public String selfieApprovalDate;

    @SerializedName("selfie_remarks")
    public String selfieRemarks;

    @SerializedName("address")
    public Address address;

    @SerializedName("address_status")
    public String addressStatus;

    @SerializedName("address_status_display")
    public String addressStatusDisplay;

    @SerializedName("address_approval_date")
    public String addressApprovalDate;

    @SerializedName("address_remarks")
    public String addressRemarks;

    protected KYCItem(Parcel in) {
        email = in.readString();
        emailStatus = in.readString();
        emailStatusDisplay = in.readString();
        emailApprovalDate = in.readString();
        emailRemarks = in.readString();
        countryIso = in.readString();
        countryCode = in.readString();
        contactNumber = in.readString();
        contactNumberStatus = in.readString();
        contactNumberStatusDisplay = in.readString();
        contactNumberApprovalDate = in.readString();
        contactNumberRemarks = in.readString();
        idStatus = in.readString();
        idStatusDisplay = in.readString();
        idType = in.readString();
        idApprovalDate = in.readString();
        idRemarks = in.readString();
        selfieStatus = in.readString();
        selfieStatusDisplay = in.readString();
        selfieApprovalDate = in.readString();
        selfieRemarks = in.readString();
        addressStatus = in.readString();
        addressStatusDisplay = in.readString();
        addressApprovalDate = in.readString();
        addressRemarks = in.readString();
    }

    public static final Creator<KYCItem> CREATOR = new Creator<KYCItem>() {
        @Override
        public KYCItem createFromParcel(Parcel in) {
            return new KYCItem(in);
        }

        @Override
        public KYCItem[] newArray(int size) {
            return new KYCItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(emailStatus);
        dest.writeString(emailStatusDisplay);
        dest.writeString(emailApprovalDate);
        dest.writeString(emailRemarks);
        dest.writeString(countryIso);
        dest.writeString(countryCode);
        dest.writeString(contactNumber);
        dest.writeString(contactNumberStatus);
        dest.writeString(contactNumberStatusDisplay);
        dest.writeString(contactNumberApprovalDate);
        dest.writeString(contactNumberRemarks);
        dest.writeString(idStatus);
        dest.writeString(idStatusDisplay);
        dest.writeString(idType);
        dest.writeString(idApprovalDate);
        dest.writeString(idRemarks);
        dest.writeString(selfieStatus);
        dest.writeString(selfieStatusDisplay);
        dest.writeString(selfieApprovalDate);
        dest.writeString(selfieRemarks);
        dest.writeString(addressStatus);
        dest.writeString(addressStatusDisplay);
        dest.writeString(addressApprovalDate);
        dest.writeString(addressRemarks);
    }

    public static class Id {
        @SerializedName("path")
        public String path;

        @SerializedName("filename")
        public String filename;

        @SerializedName("directory")
        public String directory;

        @SerializedName("full_path")
        public String fullPath;

        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class Selfie {
        @SerializedName("path")
        public String path;

        @SerializedName("filename")
        public String filename;

        @SerializedName("directory")
        public String directory;

        @SerializedName("full_path")
        public String fullPath;

        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class Address {
        @SerializedName("path")
        public String path;

        @SerializedName("filename")
        public String filename;

        @SerializedName("directory")
        public String directory;

        @SerializedName("full_path")
        public String fullPath;

        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
