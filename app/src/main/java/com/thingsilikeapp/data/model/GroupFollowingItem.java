package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI05 on 2/7/2017.
 */

public class GroupFollowingItem {

    @SerializedName("title")
    public String title;

    public boolean selected;

    @SerializedName("followers")
    public Followers followers;

    public static class Followers{
            @SerializedName("data")
            public List<UserItem> data;
    }
}
