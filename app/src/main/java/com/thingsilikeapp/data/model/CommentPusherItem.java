package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI05 on 2/7/2017.
 */

public class CommentPusherItem {

    @SerializedName("id")
    public int id;

    @SerializedName("wishlist_id")
    public int wishlist_id;

    @SerializedName("content")
    public String content;

    @SerializedName("time_passed")
    public String time_passed;

    @SerializedName("author")
    public Author author;

    @SerializedName("tagged_user")
    public TaggedUser tagged_user;

    public class Author{

        @SerializedName("id")
        public int id;

        @SerializedName("name")
        public String name;

        @SerializedName("avatar")
        public String avatar;

        @SerializedName("fb_id")
        public String fb_id;

        public String getAvatar(){
            try{
                if(avatar.equals("/resized/") || avatar.equals("") || avatar.equals("/")){
                    return "https://graph.facebook.com/" + fb_id + "/picture?type=large";
                }else{
                    return avatar;
                }
            }catch (NullPointerException e){
                return "/";
            }
        }
    }

    public class TaggedUser{

        @SerializedName("id")
        public int id;

        @SerializedName("name")
        public String name;

        @SerializedName("avatar")
        public String avatar;

        @SerializedName("fb_id")
        public String fb_id;

        public String getAvatar(){
            try{
                if(avatar.equals("/resized/") || avatar.equals("") || avatar.equals("/")){
                    return "https://graph.facebook.com/" + fb_id + "/picture?type=large";
                }else{
                    return avatar;
                }
            }catch (NullPointerException e){
                return "/";
            }
        }
    }

}
