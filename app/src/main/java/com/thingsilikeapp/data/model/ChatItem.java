package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class ChatItem {

    @SerializedName("is_add")
    public boolean is_add;

    @SerializedName("id")
    public int id;

    @SerializedName("owner_user_id")
    public int ownerUserId;

    @SerializedName("title")
    public String title;

    @SerializedName("info")
    public Info info;

    @SerializedName("latest_message")
    public LatestMessage latestMessage;


    public static class Info {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("date_created")
            public DateCreated dateCreated;
            @SerializedName("avatar")
            public Avatar avatar;

            public static class DateCreated {
                @SerializedName("date_db")
                public String dateDb;
                @SerializedName("month_year")
                public String monthYear;
                @SerializedName("time_passed")
                public String timePassed;
                @SerializedName("timestamp")
                public Timestamp timestamp;

                public static class Timestamp {
                    @SerializedName("date")
                    public String date;
                    @SerializedName("timezone_type")
                    public int timezoneType;
                    @SerializedName("timezone")
                    public String timezone;
                }
            }

            public static class Avatar {
                @SerializedName("path")
                public String path;
                @SerializedName("filename")
                public String filename;
                @SerializedName("directory")
                public String directory;
                @SerializedName("full_path")
                public String fullPath;
                @SerializedName("thumb_path")
                public String thumbPath;
            }
        }
    }

    public static class LatestMessage {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("id")
            public int idX;
            @SerializedName("sender_user_id")
            public int senderUserId;
            @SerializedName("content")
            public String content;
            @SerializedName("type")
            public String type;
            @SerializedName("info")
            public Info info;

            public static class Info {
                @SerializedName("data")
                public DataX data;

                public static class DataX {
                    @SerializedName("date_created")
                    public DateCreated dateCreated;

                    public static class DateCreated {
                        @SerializedName("date_db")
                        public String dateDb;
                        @SerializedName("month_year")
                        public String monthYear;
                        @SerializedName("time_passed")
                        public String timePassed;
                        @SerializedName("timestamp")
                        public Timestamp timestamp;

                        public static class Timestamp {
                            @SerializedName("date")
                            public String date;
                            @SerializedName("timezone_type")
                            public int timezoneType;
                            @SerializedName("timezone")
                            public String timezone;
                        }
                    }
                }
            }
        }
    }

}
