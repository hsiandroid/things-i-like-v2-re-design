package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class BankItem {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("owner")
    public String owner;

    @SerializedName("refid")
    public String refid;

    @SerializedName("avatar")
    public String avatar;

    @SerializedName("user_id")
    public String userId;

    @SerializedName("bank_name")
    public String bankName;

    @SerializedName("bank_address")
    public String bankAddress;

    @SerializedName("account_name")
    public String accountName;

    @SerializedName("account_number")
    public String accountNumber;

    @SerializedName("contact_number")
    public String contactNumber;

    @SerializedName("country_iso")
    public String countryIso;

    @SerializedName("country_code")
    public String countryCode;

    @SerializedName("email")
    public String email;

    @SerializedName("is_primary")
    public String isPrimary;

    @SerializedName("status")
    public String status;
}
