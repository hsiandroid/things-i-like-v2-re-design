package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class RewardCategoryItem {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("selected")
    public boolean selected = false;

    @SerializedName("image")
    public String image;

}
