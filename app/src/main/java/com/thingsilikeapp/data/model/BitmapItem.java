package com.thingsilikeapp.data.model;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.io.File;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class BitmapItem {

    @SerializedName("id")
    public int id;

    @SerializedName("bitmap")
    public Bitmap bitmap;

    @SerializedName("origBitmap")
    public Bitmap origBitmap;

    @SerializedName("isSelected")
    public boolean is_selected = false;

    @SerializedName("selectedFilter")
    public String selectedFilter = "Normal";
}
