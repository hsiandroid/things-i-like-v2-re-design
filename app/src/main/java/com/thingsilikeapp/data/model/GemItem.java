package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class GemItem {


    @SerializedName("max")
    public int max;

    @SerializedName("current")
    public String current;

    @SerializedName("progress")
    public int progress;

    @SerializedName("lykagem")
    public double lykagem = 0.0;

    @SerializedName("lykagem_display")
    public String lykagemDisplay;
}
