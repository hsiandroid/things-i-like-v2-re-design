package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class OrderItem {

    @SerializedName("id")
    public int id;

    @SerializedName("display_id")
    public String displayId;

    @SerializedName("user_id")
    public int userId;

    @SerializedName("payment_status")
    public String paymentStatus;

    @SerializedName("transaction_status")
    public String transactionStatus;

    @SerializedName("status_display")
    public String statusDisplay;

    @SerializedName("total")
    public String total;

    @SerializedName("purchased_date")
    public PurchasedDate purchasedDate;

    @SerializedName("addressbook_id")
    public int addressbookId;

    @SerializedName("delivery_street")
    public String deliveryStreet;

    @SerializedName("delivery_city")
    public String deliveryCity;

    @SerializedName("delivery_state")
    public String deliveryState;

    @SerializedName("delivery_country")
    public String deliveryCountry;

    @SerializedName("delivery_zip")
    public String deliveryZip;

    @SerializedName("contact_number")
    public String contactNumber;

    @SerializedName("note")
    public String note;

    @SerializedName("created_at")
    public CreatedAt createdAt;

    @SerializedName("detail")
    public Detail detail;

    @SerializedName("history")
    public History history;

    public static class PurchasedDate {
        @SerializedName("date")
        public String date;

        @SerializedName("timezone_type")
        public int timezoneType;

        @SerializedName("timezone")
        public String timezone;
    }

    public static class CreatedAt {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public Timestamp timestamp;

        public static class Timestamp {
            @SerializedName("date")
            public String date;

            @SerializedName("timezone_type")
            public int timezoneType;

            @SerializedName("timezone")
            public String timezone;
        }
    }

    public static class Detail {
        @SerializedName("data")
        public List<OrderDetailItem> data;
    }

    public static class History {
        @SerializedName("data")
        public List<OrderHistoryItem> data;

    }
}
