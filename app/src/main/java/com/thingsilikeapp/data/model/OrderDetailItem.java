package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class OrderDetailItem {

    @SerializedName("id")
    public int id;

    @SerializedName("order_id")
    public String orderId;

    @SerializedName("product_id")
    public String productId;

    @SerializedName("title")
    public String title;

    @SerializedName("code")
    public String code;

    @SerializedName("price")
    public String price;

    @SerializedName("uom")
    public String uom;

    @SerializedName("currency")
    public String currency;

    @SerializedName("item_qty")
    public String itemQty;

    @SerializedName("sub_total")
    public String subTotal;

    @SerializedName("filename")
    public String filename;

    @SerializedName("path")
    public String path;

    @SerializedName("directory")
    public String directory;

    @SerializedName("full_path")
    public String fullPath;

    @SerializedName("thumb_path")
    public String thumbPath;
}
