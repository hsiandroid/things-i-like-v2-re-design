package com.thingsilikeapp.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.vendor.server.base.AndroidModel;


/**
 * Created by Labyalo on 8/12/2017.
 */

public class ImageModel implements Parcelable {

    @SerializedName("id")
    public int id;

    @SerializedName("promotion_id")
    public int promotionId;

    @SerializedName("user_id")
    public int userId;

    @SerializedName("filename")
    public String filename;

    @SerializedName("path")
    public String path;

    @SerializedName("type")
    public String type;

    @SerializedName("directory")
    public String directory;

    @SerializedName("full_path")
    public String fullPath;

    @SerializedName("thumb_path")
    public String thumbPath;

    @SerializedName("orientation")
    public String orientation;

    protected ImageModel(Parcel in) {
        id = in.readInt();
        promotionId = in.readInt();
        userId = in.readInt();
        filename = in.readString();
        path = in.readString();
        type = in.readString();
        directory = in.readString();
        fullPath = in.readString();
        thumbPath = in.readString();
        orientation = in.readString();
    }

    public static final Creator<ImageModel> CREATOR = new Creator<ImageModel>() {
        @Override
        public ImageModel createFromParcel(Parcel in) {
            return new ImageModel(in);
        }

        @Override
        public ImageModel[] newArray(int size) {
            return new ImageModel[size];
        }
    };

    public String getImage() {
        return fullPath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(promotionId);
        parcel.writeInt(userId);
        parcel.writeString(filename);
        parcel.writeString(path);
        parcel.writeString(type);
        parcel.writeString(directory);
        parcel.writeString(fullPath);
        parcel.writeString(thumbPath);
        parcel.writeString(orientation);
    }
}
