package com.thingsilikeapp.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class WishListTransactionItem implements Parcelable {

    @SerializedName("id")
    public int id;

    @SerializedName("wishlist_id")
    public int wishlist_id;

    @SerializedName("parent_id")
    public int parent_id;

    @SerializedName("title")
    public String title;

    @SerializedName("description_format")
    public String description_format = "";

    @SerializedName("category")
    public String category;

    @SerializedName("is_sent")
    public String is_sent;

    @SerializedName("is_received")
    public String is_received;

    @SerializedName("status")
    public String status;

    @SerializedName("role")
    public String role;

    @SerializedName("time_passed")
    public String time_passed;

    @SerializedName("info")
    public Info info;

    @SerializedName("image")
    public Image image;

    @SerializedName("owner")
    public Owner owner;

    @SerializedName("sender")
    public Sender sender;

    @SerializedName("logs")
    public Logs logs;

    @SerializedName("tracker")
    public WishListItem.Tracker tracker;

    @SerializedName("delivery_info")
    public DeliveryInfo delivery_info;

    public WishListTransactionItem() {
    }

    public static class Info implements Parcelable {

        @SerializedName("data")
        public InfoData data;

        public static class InfoData implements Parcelable {

            @SerializedName("content")
            public String content;

            @SerializedName("url")
            public String url;

            @SerializedName("url_title")
            public String url_title;

            @SerializedName("url_description")
            public String url_description;

            @SerializedName("url_image")
            public String url_image;

            @SerializedName("dedication_message")
            public String dedication_message;

            @SerializedName("appreciation_message")
            public String appreciation_message;

            @SerializedName("created_at")
            public CreatedAt created_at;

            public static class CreatedAt implements Parcelable {

                @SerializedName("date_db")
                public String date_db;

                @SerializedName("month_year")
                public String month_year;

                @SerializedName("time_passed")
                public String time_passed;

                @SerializedName("timestamp")
                public Timestamp timestamp;

                public static class Timestamp implements Parcelable {

                    @SerializedName("date")
                    public String date;

                    @SerializedName("timezone_type")
                    public int timezone_type;

                    @SerializedName("timezone")
                    public String timezone;

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {
                        dest.writeString(this.date);
                        dest.writeInt(this.timezone_type);
                        dest.writeString(this.timezone);
                    }

                    public Timestamp() {
                    }

                    protected Timestamp(Parcel in) {
                        this.date = in.readString();
                        this.timezone_type = in.readInt();
                        this.timezone = in.readString();
                    }

                    public static final Parcelable.Creator<Timestamp> CREATOR = new Parcelable.Creator<Timestamp>() {
                        @Override
                        public Timestamp createFromParcel(Parcel source) {
                            return new Timestamp(source);
                        }

                        @Override
                        public Timestamp[] newArray(int size) {
                            return new Timestamp[size];
                        }
                    };
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.date_db);
                    dest.writeString(this.month_year);
                    dest.writeString(this.time_passed);
                    dest.writeParcelable(this.timestamp, flags);
                }

                public CreatedAt() {
                }

                protected CreatedAt(Parcel in) {
                    this.date_db = in.readString();
                    this.month_year = in.readString();
                    this.time_passed = in.readString();
                    this.timestamp = in.readParcelable(Timestamp.class.getClassLoader());
                }

                public static final Parcelable.Creator<CreatedAt> CREATOR = new Parcelable.Creator<CreatedAt>() {
                    @Override
                    public CreatedAt createFromParcel(Parcel source) {
                        return new CreatedAt(source);
                    }

                    @Override
                    public CreatedAt[] newArray(int size) {
                        return new CreatedAt[size];
                    }
                };
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.content);
                dest.writeString(this.url);
                dest.writeString(this.url_title);
                dest.writeString(this.url_description);
                dest.writeString(this.url_image);
                dest.writeString(this.dedication_message);
                dest.writeString(this.appreciation_message);
                dest.writeParcelable(this.created_at, flags);
            }

            public InfoData() {
            }

            protected InfoData(Parcel in) {
                this.content = in.readString();
                this.url = in.readString();
                this.url_title = in.readString();
                this.url_description = in.readString();
                this.url_image = in.readString();
                this.dedication_message = in.readString();
                this.appreciation_message = in.readString();
                this.created_at = in.readParcelable(CreatedAt.class.getClassLoader());
            }

            public static final Parcelable.Creator<InfoData> CREATOR = new Parcelable.Creator<InfoData>() {
                @Override
                public InfoData createFromParcel(Parcel source) {
                    return new InfoData(source);
                }

                @Override
                public InfoData[] newArray(int size) {
                    return new InfoData[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.data, flags);
        }

        public Info() {
        }

        protected Info(Parcel in) {
            this.data = in.readParcelable(InfoData.class.getClassLoader());
        }

        public static final Parcelable.Creator<Info> CREATOR = new Parcelable.Creator<Info>() {
            @Override
            public Info createFromParcel(Parcel source) {
                return new Info(source);
            }

            @Override
            public Info[] newArray(int size) {
                return new Info[size];
            }
        };
    }

    public static class Image implements Parcelable {

        @SerializedName("data")
        public ImageData data;

        public static class ImageData implements Parcelable {

            @SerializedName("path")
            public String path;

            @SerializedName("filename")
            public String filename;

            @SerializedName("directory")
            public String directory;

            @SerializedName("full_path")
            public String full_path;

            @SerializedName("thumb_path")
            public String thumb_path;

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.path);
                dest.writeString(this.filename);
                dest.writeString(this.directory);
                dest.writeString(this.full_path);
                dest.writeString(this.thumb_path);
            }

            public ImageData() {
            }

            protected ImageData(Parcel in) {
                this.path = in.readString();
                this.filename = in.readString();
                this.directory = in.readString();
                this.full_path = in.readString();
                this.thumb_path = in.readString();
            }

            public static final Parcelable.Creator<ImageData> CREATOR = new Parcelable.Creator<ImageData>() {
                @Override
                public ImageData createFromParcel(Parcel source) {
                    return new ImageData(source);
                }

                @Override
                public ImageData[] newArray(int size) {
                    return new ImageData[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.data, flags);
        }

        public Image() {
        }

        protected Image(Parcel in) {
            this.data = in.readParcelable(ImageData.class.getClassLoader());
        }

        public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
            @Override
            public Image createFromParcel(Parcel source) {
                return new Image(source);
            }

            @Override
            public Image[] newArray(int size) {
                return new Image[size];
            }
        };
    }

    public static class Owner implements Parcelable {

        @SerializedName("data")
        public UserItem data;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.data, flags);
        }

        public Owner() {
        }

        protected Owner(Parcel in) {
            this.data = in.readParcelable(UserItem.class.getClassLoader());
        }

        public static final Parcelable.Creator<Owner> CREATOR = new Parcelable.Creator<Owner>() {
            @Override
            public Owner createFromParcel(Parcel source) {
                return new Owner(source);
            }

            @Override
            public Owner[] newArray(int size) {
                return new Owner[size];
            }
        };
    }

    public static class Sender implements Parcelable {

        @SerializedName("data")
        public UserItem data;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.data, flags);
        }

        public Sender() {
        }

        protected Sender(Parcel in) {
            this.data = in.readParcelable(UserItem.class.getClassLoader());
        }

        public static final Parcelable.Creator<Sender> CREATOR = new Parcelable.Creator<Sender>() {
            @Override
            public Sender createFromParcel(Parcel source) {
                return new Sender(source);
            }

            @Override
            public Sender[] newArray(int size) {
                return new Sender[size];
            }
        };
    }

    public static class DeliveryInfo implements Parcelable {

        @SerializedName("data")
        public DeliveryInfoData data;

        public static class DeliveryInfoData implements Parcelable {

            @SerializedName("address")
            public String address;

            @SerializedName("contact_number")
            public String contact_number;

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.address);
                dest.writeString(this.contact_number);
            }

            public DeliveryInfoData() {
            }

            protected DeliveryInfoData(Parcel in) {
                this.address = in.readString();
                this.contact_number = in.readString();
            }

            public static final Parcelable.Creator<DeliveryInfoData> CREATOR = new Parcelable.Creator<DeliveryInfoData>() {
                @Override
                public DeliveryInfoData createFromParcel(Parcel source) {
                    return new DeliveryInfoData(source);
                }

                @Override
                public DeliveryInfoData[] newArray(int size) {
                    return new DeliveryInfoData[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.data, flags);
        }

        public DeliveryInfo() {
        }

        protected DeliveryInfo(Parcel in) {
            this.data = in.readParcelable(DeliveryInfoData.class.getClassLoader());
        }

        public static final Parcelable.Creator<DeliveryInfo> CREATOR = new Parcelable.Creator<DeliveryInfo>() {
            @Override
            public DeliveryInfo createFromParcel(Parcel source) {
                return new DeliveryInfo(source);
            }

            @Override
            public DeliveryInfo[] newArray(int size) {
                return new DeliveryInfo[size];
            }
        };
    }

    public static class Logs implements Parcelable {

        @SerializedName("data")
        public List<TimelineItem> data;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(this.data);
        }

        public Logs() {
        }

        protected Logs(Parcel in) {
            this.data = in.createTypedArrayList(TimelineItem.CREATOR);
        }

        public static final Creator<WishListItem.Logs> CREATOR = new Creator<WishListItem.Logs>() {
            @Override
            public WishListItem.Logs createFromParcel(Parcel source) {
                return new WishListItem.Logs(source);
            }

            @Override
            public WishListItem.Logs[] newArray(int size) {
                return new WishListItem.Logs[size];
            }
        };
    }

    public static class Tracker implements Parcelable {

        @SerializedName("data")
        public List<TrackerItem> data;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(this.data);
        }

        public Tracker() {
        }

        protected Tracker(Parcel in) {
            this.data = in.createTypedArrayList(TrackerItem.CREATOR);
        }

        public static final Creator<Tracker> CREATOR = new Creator<Tracker>() {
            @Override
            public Tracker createFromParcel(Parcel source) {
                return new Tracker(source);
            }

            @Override
            public Tracker[] newArray(int size) {
                return new Tracker[size];
            }
        };
    }

    protected WishListTransactionItem(Parcel in) {
        id = in.readInt();
        wishlist_id = in.readInt();
        parent_id = in.readInt();
        title = in.readString();
        description_format = in.readString();
        category = in.readString();
        is_sent = in.readString();
        is_received = in.readString();
        status = in.readString();
        role = in.readString();
        time_passed = in.readString();
        info = in.readParcelable(Info.class.getClassLoader());
        image = in.readParcelable(Image.class.getClassLoader());
        owner = in.readParcelable(Owner.class.getClassLoader());
        sender = in.readParcelable(Sender.class.getClassLoader());
        logs = in.readParcelable(Logs.class.getClassLoader());
        tracker = in.readParcelable(WishListItem.Tracker.class.getClassLoader());
        delivery_info = in.readParcelable(DeliveryInfo.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(wishlist_id);
        dest.writeInt(parent_id);
        dest.writeString(title);
        dest.writeString(description_format);
        dest.writeString(category);
        dest.writeString(is_sent);
        dest.writeString(is_received);
        dest.writeString(status);
        dest.writeString(role);
        dest.writeString(time_passed);
        dest.writeParcelable(info, flags);
        dest.writeParcelable(image, flags);
        dest.writeParcelable(owner, flags);
        dest.writeParcelable(sender, flags);
        dest.writeParcelable(logs, flags);
        dest.writeParcelable(tracker, flags);
        dest.writeParcelable(delivery_info, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WishListTransactionItem> CREATOR = new Creator<WishListTransactionItem>() {
        @Override
        public WishListTransactionItem createFromParcel(Parcel in) {
            return new WishListTransactionItem(in);
        }

        @Override
        public WishListTransactionItem[] newArray(int size) {
            return new WishListTransactionItem[size];
        }
    };
}
