package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class FriendsItem {

    @SerializedName("name")
    public String name;

    @SerializedName("full_path")
    public String full_path;

}
