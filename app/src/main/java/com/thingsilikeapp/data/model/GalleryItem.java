package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.File;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class GalleryItem {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("date")
    public String date;

    @SerializedName("absolutePath")
    public String absolutePath;

    @SerializedName("position")
    public int position = 0;

    @SerializedName("file")
    public File file;

    @SerializedName("isCamera")
    public boolean isCamera = false;

    @SerializedName("isSelected")
    public boolean is_selected = false;
}
