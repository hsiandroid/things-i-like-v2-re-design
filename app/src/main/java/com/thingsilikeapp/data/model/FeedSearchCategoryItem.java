package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class FeedSearchCategoryItem {
    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("icon")
    public int icon;

    @SerializedName("description")
    public String description;

    @SerializedName("code")
    public String code;
}
