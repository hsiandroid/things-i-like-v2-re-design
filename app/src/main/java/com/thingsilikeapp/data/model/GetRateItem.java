package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class GetRateItem {

    @SerializedName("id")
    public int id;

    @SerializedName("country_name")
    public String countryName;

    @SerializedName("country_code")
    public String countryCode;

    @SerializedName("currency")
    public String currency;

    @SerializedName("rate")
    public String rate;
}
