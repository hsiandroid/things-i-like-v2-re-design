package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class PayloadItem {

    @SerializedName("max")
    public int max = 0;

    @SerializedName("current")
    public int current = 0;

    @SerializedName("progress")
    public double progress;
}
