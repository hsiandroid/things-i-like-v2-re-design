package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class AppSettingsItem {
    @SerializedName("id")
    public int id;

    @SerializedName("version_name")
    public String version_name;

    @SerializedName("major_version")
    public int major_version;

    @SerializedName("minor_version")
    public int minor_version;

    @SerializedName("changelogs")
    public String changelogs;

    @SerializedName("is_maintenance")
    public boolean is_maintenance = true;

    @SerializedName("next_update")
    public NextUpdate next_update;

    public class NextUpdate{

        @SerializedName("date")
        public String date;

        @SerializedName("timezone_type")
        public String timezone_type;

        @SerializedName("timezone")
        public String timezone;
    }
}
