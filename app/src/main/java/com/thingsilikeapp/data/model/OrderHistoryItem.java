package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class OrderHistoryItem {

    @SerializedName("id")
    public int id;

    @SerializedName("order_id")
    public String orderId;

    @SerializedName("order_code")
    public String orderCode;

    @SerializedName("transaction_date")
    public String transactionDate;

    @SerializedName("user_id")
    public String userId;

    @SerializedName("remarks")
    public String remarks;

    @SerializedName("created_at")
    public CreatedAt createdAt;

    public static class CreatedAt {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public Timestamp timestamp;

        public static class Timestamp {
            @SerializedName("date")
            public String date;

            @SerializedName("timezone_type")
            public int timezoneType;

            @SerializedName("timezone")
            public String timezone;
        }
    }
}
