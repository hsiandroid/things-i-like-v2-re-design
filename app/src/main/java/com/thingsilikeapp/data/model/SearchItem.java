package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class SearchItem {

    @SerializedName("id")
    public int id;

    @SerializedName("keyword")
    public String keyword;
}
