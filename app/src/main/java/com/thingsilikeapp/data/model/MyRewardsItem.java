package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class MyRewardsItem {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("content")
    public String content;

    @SerializedName("note")
    public String note;

    @SerializedName("reward_type")
    public String reward_type;

    @SerializedName("value")
    public String value;

    @SerializedName("status")
    public String status;

    @SerializedName("is_redeemable")
    public String is_redeemable;

    @SerializedName("image")
    public Image image;

    @SerializedName("created_at")
    public CreatedAt created_at;

    public static class Image{

        @SerializedName("data")
        public Data data;

        public class Data{

            @SerializedName("full_path")
            public String full_path;
        }
    }

    public static class CreatedAt{
        @SerializedName("time_passed")
        public String time_passed;

        @SerializedName("month_year")
        public String month_year;

        @SerializedName("date_db")
        public String date_db;
    }

}
