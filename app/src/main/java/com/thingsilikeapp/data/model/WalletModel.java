package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class WalletModel {

    @SerializedName("lykagem")
    public String lykagem;

    @SerializedName("lykagem_display")
    public String lykagemDisplay;

    @SerializedName("progress")
    public String progress;

    @SerializedName("allow_deposit")
    public boolean allowDeposit;

    @SerializedName("allow_withdraw")
    public boolean allowWithdraw;

    @SerializedName("passcode_configured")
    public boolean passcodeConfigured = false;

    @SerializedName("lock_send")
    public boolean lockSend = false;
}
