package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

public class WalletNotificationItem {


    @SerializedName("msg")
    public String msg;

    @SerializedName("status")
    public boolean status;

    @SerializedName("status_code")
    public String statusCode;

    @SerializedName("data")
    public Data data;

    public static class Data {
        @SerializedName("notification_num")

        public String notificationNum;
        @SerializedName("wallet_notification")

        public boolean walletNotification;
    }
}
