package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class ReportPostItem {


    @SerializedName("id")
    public int id;
    @SerializedName("wishlist_id")
    public int wishlistId;

    @SerializedName("date")
    public Date date;

    @SerializedName("wishlist")
    public Wishlist wishlist;

    public static class Date {

        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public Timestamp timestamp;

        public static class Timestamp {

            @SerializedName("date")
            public String date;

            @SerializedName("timezone_type")
            public int timezoneType;

            @SerializedName("timezone")
            public String timezone;
        }
    }

    public static class Wishlist {

        @SerializedName("data")
        public DataX data;

        public static class DataX {

            @SerializedName("id")
            public int id;

            @SerializedName("pattern")
            public int pattern;

            @SerializedName("parent_id")
            public int parentId;

            @SerializedName("title")
            public String title;

            @SerializedName("category")
            public String category;

            @SerializedName("status")
            public String status;

            @SerializedName("comment_count")
            public int commentCount;

            @SerializedName("for_display_comment")
            public String forDisplayComment;

            @SerializedName("ongoing_viewer_count")
            public int ongoingViewerCount;

            @SerializedName("for_display_ongoing_viewer")
            public String forDisplayOngoingViewer;

            @SerializedName("liker_count")
            public int likerCount;

            @SerializedName("for_display_liker")
            public String forDisplayLiker;

            @SerializedName("repost_count")
            public String repostCount;

            @SerializedName("for_display_repost")
            public String forDisplayRepost;

            @SerializedName("time_passed")
            public String timePassed;

            @SerializedName("is_sent")
            public boolean isSent;

            @SerializedName("is_liked")
            public boolean isLiked;

            @SerializedName("diamond_sent")
            public int diamondSent;

            @SerializedName("same_title")
            public boolean sameTitle;

            @SerializedName("post_type")
            public String postType;

            @SerializedName("image")
            public Image image;

            public static class Image {

                @SerializedName("data")
                public Data data;

                public static class Data {

                    @SerializedName("path")
                    public String path;

                    @SerializedName("filename")
                    public String filename;

                    @SerializedName("directory")
                    public String directory;

                    @SerializedName("full_path")
                    public String fullPath;

                    @SerializedName("thumb_path")
                    public String thumbPath;

                    @SerializedName("preview")
                    public String preview;

                    @SerializedName("temp_preview")
                    public String tempPreview;

                    @SerializedName("width")
                    public String width;

                    @SerializedName("height")
                    public String height;
                }
            }
        }
    }
}
