package com.thingsilikeapp.data.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class CatalogueItem {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @Nullable
    @SerializedName("content")
    public String content;

    @Nullable
    @SerializedName("delivery_details")
    public String delivery_details;

    @SerializedName("description")
    public String description = "";

    @Nullable
    @SerializedName("note")
    public String note;

    @SerializedName("reward_type")
    public String reward_type;

    @SerializedName("remaining")
    public int remaining = 0;

    @SerializedName("value")
    public double value;

    @SerializedName("is_redeemable")
    public String is_redeemable;

    @SerializedName("image")
    public Image image;

    public class Image{

        @SerializedName("data")
        public Data data;

        public class Data{

            @SerializedName("full_path")
            public String full_path;
        }
    }

}
