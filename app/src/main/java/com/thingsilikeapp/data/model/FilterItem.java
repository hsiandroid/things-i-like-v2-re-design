package com.thingsilikeapp.data.model;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class FilterItem {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("bitmap")
    public Bitmap bitmap;

}
