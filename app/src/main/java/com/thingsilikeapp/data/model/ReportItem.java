package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class ReportItem {

    @SerializedName("id")
    public int id;

    @SerializedName("user_id")
    public int user_id;

    @SerializedName("reference_id")
    public int reference_id;

    @SerializedName("type")
    public String type;

    @SerializedName("is_removed")
    public boolean is_removed;
}
