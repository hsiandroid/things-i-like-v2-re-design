package com.thingsilikeapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class DiscoverItem {

    @SerializedName("is_bday")
    public boolean isBday;

    @SerializedName("upcoming_bday_num")
    public String upcomingBdayNum;

    @SerializedName("today_bday_num")
    public String todayBdayNum;

    @SerializedName("bday_greetings_num")
    public String bdayGreetingsNum;

    @SerializedName("notification_num")
    public String notificationNum;
}
