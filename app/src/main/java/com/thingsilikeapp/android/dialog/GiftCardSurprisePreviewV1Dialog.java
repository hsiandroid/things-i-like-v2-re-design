package com.thingsilikeapp.android.dialog;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.GreetingItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.gift.RandomRequest;
import com.thingsilikeapp.server.request.gift.SayThanksRequest;
import com.thingsilikeapp.server.transformer.birthday.CelebrationTransformer;
import com.thingsilikeapp.server.transformer.birthday.GreetingTransformer;
import com.thingsilikeapp.vendor.android.base.BaseV1Dialog;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.andengine.model.BalloonPopResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class GiftCardSurprisePreviewV1Dialog extends BaseV1Dialog implements
        View.OnClickListener{

	public static final String TAG = GiftCardSurprisePreviewV1Dialog.class.getName();
    private Callback callback;

	@BindView(R.id.previewIMG)		ImageView previewIMG;
	@BindView(R.id.thanksBTN)       View thanksBTN;
	@BindView(R.id.cancelBTN)       View cancelBTN;
	@BindView(R.id.progressBar)     View progressBar;

	public static GiftCardSurprisePreviewV1Dialog newInstance(Callback callback) {
		GiftCardSurprisePreviewV1Dialog fragment = new GiftCardSurprisePreviewV1Dialog();
        fragment.callback = callback;
		return fragment;
	}
	@Override
	public int onLayoutSet() {
		return R.layout.dialog_gift_card_surprise;
	}

	@Override
	public void onViewReady() {
        thanksBTN.setOnClickListener(this);
        cancelBTN.setOnClickListener(this);
        thanksBTN.setVisibility(View.GONE);
	}

    @Override
    public void onResume() {
        super.onResume();
        getGreetings();
    }

    private void displayData(GreetingItem greetingItem){

        Glide.with(getContext())
                .load(greetingItem.image.full_path)
//                .listener(new RequestListener<String, GlideDrawable>() {
//                    @Override
//                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        if (progressBar != null) {
//                            progressBar.setVisibility(View.GONE);
//                        }
//                        return false;
//                    }
//                })
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        if (progressBar != null) {
                            progressBar.setVisibility(View.GONE);
                        }
                        return false;
                    }
                })
                .into(previewIMG);
        thanksBTN.setVisibility(View.VISIBLE);
        thanksBTN.setTag(greetingItem.id);
        EventBus.getDefault().post(new BalloonPopResponse(greetingItem));
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        if(callback != null){
            callback.onClose();
        }
        super.onDismiss(dialog);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.thanksBTN:
                sayThanks((int) view.getTag());
                break;
            case R.id.cancelBTN:
                dismiss();
                break;
        }
    }

    public interface Callback{
        void onClose();
    }

    private void getGreetings(){
        progressBar.setVisibility(View.VISIBLE);
        RandomRequest randomRequest = new RandomRequest(getContext());
        randomRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }

    @Subscribe
    public void onResponse(RandomRequest.ServerResponse responseData) {
        GreetingTransformer baseTransformer = responseData.getData(CelebrationTransformer.class);
        if(baseTransformer.status){
            UserData.insert(UserData.GREETINGS, baseTransformer.unread);
            displayData(baseTransformer.data);
        }
    }

	private void sayThanks(int id){
		SayThanksRequest randomRequest = new SayThanksRequest(getContext());
		randomRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.addParameters(Keys.server.key.GREETINGS_ID, id)
				.execute();
	}

	@Subscribe
	public void onResponse(SayThanksRequest.ServerResponse responseData) {
		GreetingTransformer baseTransformer = responseData.getData(GreetingTransformer.class);
		if(baseTransformer.status){
			dismiss();
			ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
		}else{
			ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
		}
	}
}
