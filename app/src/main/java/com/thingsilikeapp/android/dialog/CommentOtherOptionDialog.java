package com.thingsilikeapp.android.dialog;

import android.view.View;

import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.CommentItem;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class CommentOtherOptionDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = CommentOtherOptionDialog.class.getName();

	private CommentCallback commentCallback;
	private CommentItem commentItem;
	@State String commentTemp;

	@BindView(R.id.openBTN)			View openBTN;
	@BindView(R.id.hideBTN)			View hideBTN;
	@BindView(R.id.cancelBTN)		View cancelBTN;

	public static CommentOtherOptionDialog newInstance(CommentItem commentItem, CommentCallback commentCallback) {
		CommentOtherOptionDialog commentOtherOptionDialog = new CommentOtherOptionDialog();
		commentOtherOptionDialog.commentCallback = commentCallback;
		commentOtherOptionDialog.commentTemp = new Gson().toJson(commentItem);
		return commentOtherOptionDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_comment_other_option;
	}

	@Override
	public void onViewReady() {
		openBTN.setOnClickListener(this);
		hideBTN.setOnClickListener(this);
		cancelBTN.setOnClickListener(this);

		commentItem = new Gson().fromJson(commentTemp, CommentItem.class);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		if(commentCallback != null){
			switch (v.getId()){
				case R.id.openBTN:
					dismiss();
					commentCallback.editComment(commentItem);
					break;
				case R.id.hideBTN:
					dismiss();
					commentCallback.deleteComment(commentItem);
					break;
				case R.id.cancelBTN:
					dismiss();
					break;
			}
		}
	}

	public interface CommentCallback{
		void editComment(CommentItem commentItem);
		void deleteComment(CommentItem commentItem);
	}
}
