package com.thingsilikeapp.android.dialog;

import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.CatalogueItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

import butterknife.BindView;
import icepick.State;

public class RedeemRewardSuccessDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = RedeemRewardSuccessDialog.class.getName();

	private CatalogueItem catalogueItem;

	@BindView(R.id.redeemableIV)			ImageView redeemableIV;
	@BindView(R.id.nameTXT)                 TextView nameTXT;
	@BindView(R.id.pointsTXT)               TextView pointsTXT;
	@BindView(R.id.typeIV)               	ImageView typeIV;
	@BindView(R.id.pointsValueTXT)          TextView pointsValueTXT;
	@BindView(R.id.closeBTN)                View closeBTN;
	@BindView(R.id.continueBTN)             View continueBTN;

    @State  String catalogueItemRaw;

	public static RedeemRewardSuccessDialog newInstance(CatalogueItem catalogueItem) {
		RedeemRewardSuccessDialog redeemRewardSuccessDialog = new RedeemRewardSuccessDialog();
		redeemRewardSuccessDialog.catalogueItemRaw = new Gson().toJson(catalogueItem);
		redeemRewardSuccessDialog.setCancelable(true);
		return redeemRewardSuccessDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_redeem_reward_success;
	}

	@Override
	public void onViewReady() {
        closeBTN.setOnClickListener(this);
		continueBTN.setOnClickListener(this);
        catalogueItem = new Gson().fromJson(catalogueItemRaw, CatalogueItem.class);
        displayData(catalogueItem);
	}

	private void displayData(CatalogueItem catalogueItem){
	    nameTXT.setText(catalogueItem.title);
//		pointsTXT.setText("Remaining " + catalogueItem.value);
		if (catalogueItem.reward_type.equalsIgnoreCase("crystal")) {
			typeIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_lyka_gems));
			pointsValueTXT.setText(UserData.getRewardCrystal());
		} else {
			typeIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_lyka_chips));
			pointsValueTXT.setText(UserData.getRewardFragment());
		}

        Glide.with(getContext())
                .load(catalogueItem.image.data.full_path)
				.apply(new RequestOptions()
                .placeholder(R.drawable.logo_blue_new)
                .error(R.drawable.logo_blue_new))
                .into(redeemableIV);
    }

	@Override
	public void onStart() {
		super.onStart();
//        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setDialogWrapContent();
	}

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.closeBTN:
                dismiss();
                break;
			case R.id.continueBTN:
                dismiss();
                ((RouteActivity) getContext()).startRewardsActivity("treasury", "treasury");
				((RouteActivity) getContext()).startRewardsActivity("reward_details", catalogueItem.id);
				break;
        }
    }
}
