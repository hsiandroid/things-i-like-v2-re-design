package com.thingsilikeapp.android.dialog;

import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.CategoryAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.wishlist.CategoryRequest;
import com.thingsilikeapp.server.transformer.wishlist.CategoryTransformer;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.widget.CustomViewPager;

import org.andengine.entity.text.Text;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

public class CategoryDialog extends BaseDialog implements SwipeRefreshLayout.OnRefreshListener, CategoryAdapter.ClickListener, View.OnClickListener{
	public static final String TAG = CategoryDialog.class.getName();

	private CategoryAdapter categoryAdapter;
	private CategoryRequest categoryRequest;
	private List<CategoryItem> categoryItems;

	private Callback callback;
    private Timer timer;

	@BindView(R.id.searchET)				EditText searchET;
	@BindView(R.id.customET)				EditText customET;
	@BindView(R.id.pickBTN)					TextView pickBTN;
	@BindView(R.id.categoryLV) 				GridView categoryLV;
	@BindView(R.id.mainBackButtonIV)		View mainBackButtonIV;
	@BindView(R.id.myMomentsCON)			View myMomentsCON;
	@BindView(R.id.categoryIMG) 			ImageView categoryIMG;
	@BindView(R.id.categorySRL)  			SwipeRefreshLayout categorySRL;

	public static CategoryDialog newInstance(Callback callback) {
		CategoryDialog categoryDialog = new CategoryDialog();
		categoryDialog.callback = callback;
		return categoryDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_category;
	}

	@Override
	public void onViewReady() {
		mainBackButtonIV.setOnClickListener(this);
		categoryAdapter = new CategoryAdapter(getContext());
		categoryAdapter.setOnItemClickListener(this);
		categoryLV.setAdapter(categoryAdapter);
		pickBTN.setOnClickListener(this);
		categoryLV.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 120));
		myMomentsCON.setOnClickListener(this);
		Picasso.with(getContext()).load("http://www.readersdigest.com.au/sites/default/files/slides/friends-jumping-beach-GettyImages-916194264-770.jpg").centerCrop().fit().placeholder(R.color.headerColor).into(categoryIMG);
		searchET.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (timer != null) {
                    timer.cancel();
                }
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable charSequence) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
						if (getActivity() != null) {
							getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									categoryRequest
											.showSwipeRefreshLayout(true)
											.clearParameters()
											.addParameters(Keys.server.key.KEYWORD, searchET.getText().toString())
											.first();
								}
							});
						}
					}
                }, 600);
				categoryAdapter.getFilter().filter(charSequence);

			}
		});

		initSuggestionAPI();
	}


	private void initSuggestionAPI(){

		categorySRL.setColorSchemeResources(R.color.colorPrimary);
		categorySRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
		categorySRL.setOnRefreshListener(this);

		categoryItems = new ArrayList<>();
		categoryAdapter.setNewData(categoryItems);
		categoryLV.setAdapter(categoryAdapter);

		categoryRequest = new CategoryRequest(getContext());
		categoryRequest
				.setSwipeRefreshLayout(categorySRL)
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.showSwipeRefreshLayout(true)
				.setPerPage(10000);
	}

	private void refreshList(){
		categoryItems.clear();
		categoryAdapter.setNewData(categoryItems);
		categoryRequest
				.showSwipeRefreshLayout(true)
				.clearParameters()
				.addParameters(Keys.server.key.KEYWORD, searchET.getText().toString())
				.first();
	}

	@Override
	public void onRefresh() {
		refreshList();
	}

	@Override
	public void onResume() {
		super.onResume();
		refreshList();
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Override
	public void onItemClick(CategoryItem categoryItem) {

		if (categoryItem.title.equalsIgnoreCase("Others")){
			Keyboard.showForceKeyboard(getActivity(), customET);
		}else{
			if(callback != null){
				callback.callback(categoryItem.title);
			}
			dismiss();
		}

	}

	@Subscribe
	public void onResponse(CategoryRequest.ServerResponse responseData) {
		CategoryTransformer categoryTransformer = responseData.getData(CategoryTransformer.class);
		if(categoryTransformer.status){
			if(responseData.isNext()){
				for(CategoryItem categoryItem :  categoryTransformer.data){
					categoryItems.add(categoryItem);
				}
			}else{
				categoryItems = categoryTransformer.data;
			}
			categoryAdapter.setNewData(categoryItems);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.mainBackButtonIV:
				dismiss();
				break;
			case R.id.myMomentsCON:
				if(callback != null){
					callback.callback("My Moments");
					dismiss();
				}
				break;
			case R.id.pickBTN:
				if (customET.getText().toString().equalsIgnoreCase("")){
					ToastMessage.show(getContext(), "Please enter a category first", ToastMessage.Status.FAILED);
				}else{
					if(callback != null){
						callback.callback(customET.getText().toString());
						dismiss();
					}

				}

				break;
		}
	}

	public interface Callback{
		void callback(String categoryItem);
	}
}
