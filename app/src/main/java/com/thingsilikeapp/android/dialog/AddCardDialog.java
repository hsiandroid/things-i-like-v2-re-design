package com.thingsilikeapp.android.dialog;


import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.cardform.OnCardFormSubmitListener;
import com.braintreepayments.cardform.utils.CardType;
import com.braintreepayments.cardform.view.CardEditText;
import com.braintreepayments.cardform.view.CardForm;
import com.braintreepayments.cardform.view.SupportedCardTypesView;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.BankCardItem;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


import butterknife.BindView;

public class AddCardDialog extends BaseDialog implements
        View.OnClickListener, OnCardFormSubmitListener,
        CardEditText.OnCardTypeChangedListener{

	public static final String TAG = AddCardDialog.class.getName();

    private static final CardType[] SUPPORTED_CARD_TYPES = { CardType.VISA, CardType.MASTERCARD,
            CardType.JCB, CardType.UNIONPAY };

	private Callback callback;

    @BindView(R.id.supported_card_types)    SupportedCardTypesView supported_card_types;
    @BindView(R.id.card_form)               CardForm card_form;
    @BindView(R.id.saveBTN)                 TextView saveBTN;
    @BindView(R.id.termsBTN)                TextView termsBTN;
    @BindView(R.id.mainBackButtonIV)        ImageView exitBTN;
    @BindView(R.id.mainTitleTXT)                TextView titleCON;

	public static AddCardDialog newInstance(Callback callback) {
		AddCardDialog addCardDialog = new AddCardDialog();
		addCardDialog.callback = callback;
		return addCardDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_add_card;
	}

	@Override
	public void onViewReady() {
        card_form.cardRequired(true)
                .maskCardNumber(true)
                .maskCvv(true)
                .expirationRequired(true)
                .cvvRequired(true)
//                .mobileNumberExplanation("Make sure SMS is enabled for this mobile number")
                .actionLabel(getString(R.string.purchase))
                .setup(getActivity());
        card_form.setOnCardFormSubmitListener(this);
        card_form.setOnCardTypeChangedListener(this);
        saveBTN.setOnClickListener(this);
        termsBTN.setOnClickListener(this);
        exitBTN.setOnClickListener(this);

        // Failure to set FLAG_SECURE exposes your app to screenshots allowing other apps to steal card information.
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
	}

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        setDialogMatchParent();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onResume() {
        super.onResume();
    }


	@Override
	public void onClick(View v) {
		switch (v.getId()){
            case R.id.saveBTN:
                setSaveBTN();
                break;
            case R.id.termsBTN:
                WebViewDialog.newInstance("http://www.mylyka.com/terms-of-use").show(getChildFragmentManager(), WebViewDialog.TAG);
                break;
            case R.id.mainBackButtonIV:
                dismiss();
                break;
		}
	}


    @Subscribe
    public void onResponse(Wallet.AddCardResponse tranformerResponse) {
        SingleTransformer<BankCardItem> transformer = tranformerResponse.getData(SingleTransformer.class);
        if (transformer.status) {
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.SUCCESS);
            if (callback != null){
                callback.onAddCardSuccess();
                dismiss();
            }
        }else{
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.FAILED);
        }
    }

    public void setSaveBTN(){
        int month = Integer.parseInt(card_form.getExpirationMonth());
        Wallet.getDefault().addCard(getContext(), card_form.getCardNumber(), month, card_form.getExpirationYear());
    }

    @Override
    public void onCardFormSubmit() {
        if (card_form.isValid())
        {
            Toast.makeText(getActivity(), R.string.valid, Toast.LENGTH_SHORT).show();
        }
        else
        {
            card_form.validate();
            Toast.makeText(getActivity(), R.string.invalid, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCardTypeChanged(CardType cardType) {
        if (cardType == CardType.EMPTY) {
            supported_card_types.setSupportedCardTypes(SUPPORTED_CARD_TYPES);
        }
        else
        {
            supported_card_types.setSupportedCardTypes(cardType);
        }
    }

    public interface Callback {
        void onAddCardSuccess();
    }
}
