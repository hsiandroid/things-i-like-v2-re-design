package com.thingsilikeapp.android.dialog;

import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.GroupAdapter;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.user.GroupRequest;
import com.thingsilikeapp.server.transformer.user.GroupCollectionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class GroupEditDialog extends BaseDialog implements
		View.OnClickListener{
	public static final String TAG = GroupEditDialog.class.getName();

	private Callback callback;
	private GroupAdapter groupAdapter;
	private GroupRequest groupRequest;

    @BindView(R.id.imageCIV)				ImageView imageCIV;
    @BindView(R.id.headerTXT)			    TextView headerTXT;
    @BindView(R.id.subHeaderTXT)			TextView subHeaderTXT;
    @BindView(R.id.commonNameTXT)			TextView commonNameTXT;
    @BindView(R.id.cancelBTN)				View cancelBTN;
    @BindView(R.id.followBTN)				TextView followBTN;
    @BindView(R.id.groupEHLV)               ListView groupEHLV;
    @BindView(R.id.groupPB)					ProgressBar groupPB;

    @State int id;
	@State String raw;
	@State String group;

	public static GroupEditDialog newInstance(UserItem userItem, Callback callback) {
		GroupEditDialog groupEditDialog = new GroupEditDialog();
		groupEditDialog.callback  = callback;
		groupEditDialog.id = userItem.id;
		groupEditDialog.group = userItem.social.data.group;
		groupEditDialog.raw = new Gson().toJson(userItem);
		return groupEditDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_edit_group;
	}

	@Override
	public void onViewReady() {

		cancelBTN.setOnClickListener(this);
		followBTN.setOnClickListener(this);

		displayData();

		setUpGroupListView();
		attemptGetGroups();

	}

	private void attemptGetGroups(){
		followBTN.setEnabled(false);
		groupPB.setVisibility(View.VISIBLE);
		groupRequest = new GroupRequest(getContext());
		groupRequest
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.execute();
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	private void setUpGroupListView(){
		groupAdapter = new GroupAdapter(getContext());
		groupEHLV.setAdapter(groupAdapter);
	}

	private void displayData(){
		UserItem userItem = new Gson().fromJson(raw, UserItem.class);
		commonNameTXT.setText(userItem.common_name);

        String header = (StringFormatter.isEmpty(userItem.social.data.group) ? "ASSIGN " : "RE-ASSIGN ");

        String 	subHeader =
				header +
                userItem.pronoun("HIM ", "HER ") +
                "TO A GROUP";

        headerTXT.setText(header + " GROUP");
        subHeaderTXT.setText(subHeader);

		Glide.with(getContext())
				.load(userItem.image)
				.apply(new RequestOptions()

						.placeholder(R.drawable.placeholder_avatar)
				.error(R.drawable.placeholder_avatar)
				.fitCenter()
				.dontAnimate()
				.diskCacheStrategy(DiskCacheStrategy.ALL)
				.skipMemoryCache(true))
				.into(imageCIV);
	}

	@Override
	public void onStart() {
		super.onStart();
        setDialogMatchParent();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.cancelBTN:
				dismiss();
				break;
			case R.id.followBTN: {
				onSaveClicked();
				break;
			}
		}
	}

	private void onSaveClicked(){
        String group = groupAdapter.getSelectedItemName();
        if(group.equalsIgnoreCase("custom")){
            ToastMessage.show(getContext(), "Custom field is required.", ToastMessage.Status.FAILED);
        }else{
            callback.onGroupEdit(groupAdapter.getSelectedItemName(), id);
            dismiss();
        }
	}

	public interface Callback{
		void onGroupEdit(String groupName, int id);
	}

	@Subscribe
	public void onResponse(GroupRequest.ServerResponse responseData) {
		GroupCollectionTransformer groupCollectionTransformer = responseData.getData(GroupCollectionTransformer.class);
		if (groupCollectionTransformer.status) {
			groupAdapter.setNewData(groupCollectionTransformer.data);
			groupAdapter.setItemSelected(group);

            followBTN.setEnabled(true);
            groupPB.setVisibility(View.GONE);
		}else{
            dismiss();
        }
	}
}
