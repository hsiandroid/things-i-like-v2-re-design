package com.thingsilikeapp.android.dialog;

import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.CatalogueItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class RedeemRewardFailedDialog extends BaseDialog implements View.OnClickListener, GemsBuyDialog.Callback{
	public static final String TAG = RedeemRewardFailedDialog.class.getName();

	private CatalogueItem catalogueItem;

    @BindView(R.id.redeemableIV)            ImageView redeemableIV;
    @BindView(R.id.pointsIV)                ImageView pointsIV;
    @BindView(R.id.pointsValueTXT)          TextView pointsValueTXT;
    @BindView(R.id.typeIV)                  ImageView typeIV;
    @BindView(R.id.valueTXT)                TextView valueTXT;
    @BindView(R.id.nameTXT)                 TextView nameTXT;
    @BindView(R.id.closeBTN)                View closeBTN;
    @BindView(R.id.backBTN)                 View backBTN;
    @BindView(R.id.buyTXT)                  View buyTXT;
    @BindView(R.id.buyIV)                   View buyIV;

    @State String catalogueItemRaw;

    public static RedeemRewardFailedDialog newInstance(CatalogueItem catalogueItem) {
        RedeemRewardFailedDialog redeemRewardFailedDialog = new RedeemRewardFailedDialog();
        redeemRewardFailedDialog.catalogueItemRaw = new Gson().toJson(catalogueItem);
        redeemRewardFailedDialog.setCancelable(true);
		return redeemRewardFailedDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_redeem_reward_failed;
	}

	@Override
	public void onViewReady() {
        closeBTN.setOnClickListener(this);
        backBTN.setOnClickListener(this);
        catalogueItem = new Gson().fromJson(catalogueItemRaw, CatalogueItem.class);
        displayData(catalogueItem);
        buyTXT.setOnClickListener(this);
        buyIV.setOnClickListener(this);
	}

	private void displayData(CatalogueItem catalogueItem){
        if (catalogueItem.reward_type.equalsIgnoreCase("crystal")) {
            pointsIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_lyka_gems));
            typeIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_lyka_gems));
            pointsValueTXT.setText(UserData.getRewardCrystal());
        } else {
            pointsIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_lyka_chips));
            typeIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_lyka_chips));
            pointsValueTXT.setText(UserData.getRewardFragment());
        }
        nameTXT.setText(catalogueItem.title);
        valueTXT.setText(String.valueOf(catalogueItem.value));

        Glide.with(getContext())
                .load(catalogueItem.image.data.full_path)
                .apply(new RequestOptions()

                        .placeholder(R.drawable.logo_blue_new)
                .error(R.drawable.logo_blue_new))
                .into(redeemableIV);
    }

	@Override
	public void onStart() {
		super.onStart();
        setDialogWrapContent();
	}

    private void openBuyNow(){
        GemsBuyDialog.newInstance(this).show(getFragmentManager(), TAG);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.closeBTN:
                dismiss();
                break;
            case R.id.backBTN:
                dismiss();
                break;
            case R.id.buyTXT:
            case R.id.buyIV:
                openBuyNow();
                break;
        }
    }

    @Override
    public void onGemBuySuccess() {
        dismiss();
    }
}
