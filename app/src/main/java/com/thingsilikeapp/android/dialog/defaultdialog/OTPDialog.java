package com.thingsilikeapp.android.dialog.defaultdialog;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dpizarro.pinview.library.PinView;
import com.goodiebag.pinview.Pinview;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MainActivity;
import com.thingsilikeapp.android.activity.NewRegistrationActivity;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import butterknife.BindView;

import static com.thingsilikeapp.config.Keys.server.key.PHONE_NUMBER;

public class OTPDialog extends BaseDialog implements  View.OnClickListener {
	public static final String TAG = OTPDialog.class.getName();

	private NewRegistrationActivity registrationActivity;

	String username;
	String gender;
	String birthdate;
	String name;
	String email;
	String type;
	String country;
	String code;
	String otp_value;

    @BindView(R.id.continueBTN)             TextView continueBTN;
    @BindView(R.id.mobilenoTXT)             TextView mobilenoTXT;
    @BindView(R.id.pinView) 				Pinview pinView;
    @BindView(R.id.exitBTN)    				ImageView extiBTN;


	public static OTPDialog Builder(String username, String gender, String birthdate, String name, String email, String type, String country, String code, String otp_value) {
		OTPDialog infoDialog = new OTPDialog();
		infoDialog.username = username;
		infoDialog.gender = gender;
		infoDialog.birthdate = birthdate;
		infoDialog.name = name;
		infoDialog.type = type;
		infoDialog.email = email;
		infoDialog.country  = country;
		infoDialog.code = code;
		infoDialog.otp_value = otp_value;
		return infoDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_otp_passcode;
	}

	@Override
	public void onViewReady() {
		registrationActivity = (NewRegistrationActivity) getContext();
        continueBTN.setOnClickListener(this);
        extiBTN.setOnClickListener(this);
		mobilenoTXT.setText(email);
//		smsInterceptor.setPhoneNumber(email);
	}

	@Override
	public void onStart() {
		super.onStart();
//        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		setDialogMatchParent();
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onStop() {
		super.onStop();
	}

	public void build(FragmentManager manager){
        show(manager, TAG);
	}

	@Override
	public void onClick(View view) {
		switch(view.getId()){
			case R.id.continueBTN:
				if (pinView.getValue().equalsIgnoreCase(otp_value)){registrationActivity.openGFragment(username,gender,birthdate,name,email, "contact", country, code);
					ToastMessage.show(getContext(), "Success", ToastMessage.Status.SUCCESS);
					dismiss();
				}else{
					ToastMessage.show(getContext(), "Invalid Code, please the code that you receive via sms.", ToastMessage.Status.SUCCESS);
				}

				break;
			case R.id.exitBTN:
				dismiss();
				break;
		}
	}

}


