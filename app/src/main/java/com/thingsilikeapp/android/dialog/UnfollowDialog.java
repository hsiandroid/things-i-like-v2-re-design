package com.thingsilikeapp.android.dialog;

import android.content.DialogInterface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class UnfollowDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = UnfollowDialog.class.getName();
	private Callback callback;
	@State String userName;
	@State int userID;
	@State boolean isDismiss = true;

	public static UnfollowDialog newInstance(int userID, String userName, Callback callback) {
		UnfollowDialog unfollowDialog = new UnfollowDialog();
		unfollowDialog.callback = callback;
		unfollowDialog.userName = userName;
		unfollowDialog.userID = userID;
		return unfollowDialog;
	}

	@BindView(R.id.userNameTXT) 		TextView userNameTXT;
	@BindView(R.id.negativeBTN) 		View negativeBTN;
	@BindView(R.id.positiveBTN) 		View positiveBTN;

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_unfollow;
	}

	@Override
	public void onViewReady() {
		negativeBTN.setOnClickListener(this);
		positiveBTN.setOnClickListener(this);
		userNameTXT.setText(getMessage());
	}

	private SpannableString getMessage(){
		String message = "Unfollow ";
		SpannableString spannableString = new SpannableString(message + userName + "?");
		ClickableSpan clickableSpan = new ClickableSpan() {
			@Override
			public void onClick(View widget) {

			}

			public void updateDrawState(TextPaint ds) {
				ds.setUnderlineText(false);
				ds.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 16, getResources().getDisplayMetrics()));
				ds.setTypeface(TypefaceUtils.load(getContext().getAssets(),  getString(R.string.typeface_heading)));
			}
		};
		spannableString.setSpan(clickableSpan, message.length(), message.length() + userName.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		return spannableString;
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.negativeBTN:
				if(callback != null){
					callback.onCancel(userID);
					isDismiss = false;
				}
				dismiss();
				break;
			case R.id.positiveBTN:
				if(callback != null){
					callback.onAccept(userID);
					isDismiss = false;
				}
				dismiss();
				break;
		}
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		if(callback != null && isDismiss){
			callback.onCancel(userID);
		}
		super.onDismiss(dialog);
	}

	public interface Callback{
		void onAccept(int userID);
		void onCancel(int userID);
	}
}
