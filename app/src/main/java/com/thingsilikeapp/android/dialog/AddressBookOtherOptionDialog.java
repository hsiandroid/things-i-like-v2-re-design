package com.thingsilikeapp.android.dialog;

import android.view.View;

import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.AddressBookItem;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class AddressBookOtherOptionDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = AddressBookOtherOptionDialog.class.getName();

	private AddressBookCallBack addressBookCallBack;

	private AddressBookItem addressBookItem;
	@State String addressBookTemp;

	@BindView(R.id.openBTN)			View openBTN;
	@BindView(R.id.hideBTN)			View hideBTN;
	@BindView(R.id.cancelBTN)		View cancelBTN;
	@BindView(R.id.defaultBTN)		View defaultBTN;

	public static AddressBookOtherOptionDialog newInstance(AddressBookItem addressBookItem, AddressBookCallBack addressBookCallBack) {
		AddressBookOtherOptionDialog addressBookOtherOptionDialog = new AddressBookOtherOptionDialog();
		addressBookOtherOptionDialog.addressBookTemp = new Gson().toJson(addressBookItem);
		addressBookOtherOptionDialog.addressBookCallBack = addressBookCallBack;
		return addressBookOtherOptionDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_address_book_other_option;
	}

	@Override
	public void onViewReady() {
		openBTN.setOnClickListener(this);
		hideBTN.setOnClickListener(this);
		cancelBTN.setOnClickListener(this);
		defaultBTN.setOnClickListener(this);

		addressBookItem = new Gson().fromJson(addressBookTemp, AddressBookItem.class);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		if(addressBookCallBack != null){
			switch (v.getId()){
				case R.id.openBTN:
					dismiss();
					addressBookCallBack.editAddress(addressBookItem);
					break;
				case R.id.hideBTN:
					dismiss();
					addressBookCallBack.deleteAddress(addressBookItem);
					break;
				case R.id.cancelBTN:
					dismiss();
					break;
				case R.id.defaultBTN:
					dismiss();
					addressBookCallBack.setDefaultAddressComment(addressBookItem);
					break;
			}
		}
	}

	public interface AddressBookCallBack{
		void editAddress(AddressBookItem addressBookItem);
		void deleteAddress(AddressBookItem addressBookItem);
		void setDefaultAddressComment(AddressBookItem addressBookItem);
	}
}
