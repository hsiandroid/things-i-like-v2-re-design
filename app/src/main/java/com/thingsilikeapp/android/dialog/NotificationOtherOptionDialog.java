package com.thingsilikeapp.android.dialog;

import android.view.View;

import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.NotificationItem;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class NotificationOtherOptionDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = NotificationOtherOptionDialog.class.getName();

	private NotificationCallback notificationCallback;
	private NotificationItem notificationItem;

	@BindView(R.id.openBTN)			View openBTN;
	@BindView(R.id.hideBTN)			View hideBTN;
	@BindView(R.id.turnOffBTN)		View turnOffBTN;
	@BindView(R.id.cancelBTN)		View cancelBTN;

	@State String notificationTemp;

	public static NotificationOtherOptionDialog newInstance(NotificationItem notificationItem, NotificationCallback notificationCallback) {
		NotificationOtherOptionDialog notificationOtherOptionDialog = new NotificationOtherOptionDialog();
		notificationOtherOptionDialog.notificationCallback = notificationCallback;
		notificationOtherOptionDialog.notificationTemp = new Gson().toJson(notificationItem);
		return notificationOtherOptionDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_notification_other_option;
	}

	@Override
	public void onViewReady() {
		openBTN.setOnClickListener(this);
		hideBTN.setOnClickListener(this);
		turnOffBTN.setOnClickListener(this);
		cancelBTN.setOnClickListener(this);

		notificationItem = new Gson().fromJson(notificationTemp, NotificationItem.class);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		if(notificationCallback != null){
			switch (v.getId()){
				case R.id.openBTN:
					dismiss();
					notificationCallback.openNotification(notificationItem);
					break;
				case R.id.hideBTN:
					dismiss();
					notificationCallback.hideNotification(notificationItem);
					break;
				case R.id.turnOffBTN:
					dismiss();
					notificationCallback.turnOffNotification(notificationItem);
					break;
				case R.id.cancelBTN:
					dismiss();
					break;
			}
		}
	}

	public interface NotificationCallback{
		void openNotification(NotificationItem notificationItem);
		void hideNotification(NotificationItem notificationItem);
		void turnOffNotification(NotificationItem notificationItem);
	}
}
