package com.thingsilikeapp.android.dialog;

import android.app.ProgressDialog;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.CatalogueItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.reward.RewardCatalogueRedeemRequest;
import com.thingsilikeapp.server.transformer.reward.RewardCatalogueShowTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class RedeemRewardConfirmationDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = RedeemRewardConfirmationDialog.class.getName();

    private long mLastClickTime = 0;
    private Callback callback;

	@BindView(R.id.redeemableIV)            ImageView redeemableIV;
	@BindView(R.id.typeIV)					ImageView typeIV;
	@BindView(R.id.nameTXT)                 TextView nameTXT;
	@BindView(R.id.pointsValueTXT)          TextView pointsValueTXT;
	@BindView(R.id.closeBTN)                View closeBTN;
	@BindView(R.id.redeemBTN)               View redeemBTN;

	@State String catalogueItemRaw;

	public static RedeemRewardConfirmationDialog newInstance(CatalogueItem catalogueItem, Callback callback) {
		RedeemRewardConfirmationDialog redeemRewardConfirmationDialog = new RedeemRewardConfirmationDialog();
		redeemRewardConfirmationDialog.catalogueItemRaw = new Gson().toJson(catalogueItem);
		redeemRewardConfirmationDialog.callback = callback;
		redeemRewardConfirmationDialog.setCancelable(true);
		return redeemRewardConfirmationDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_redeem_reward_confirmation;
	}

	@Override
	public void onViewReady() {
        closeBTN.setOnClickListener(this);
		redeemBTN.setOnClickListener(this);
        displayData(getCatalogue());
	}

	private CatalogueItem getCatalogue(){
		return new Gson().fromJson(catalogueItemRaw, CatalogueItem.class);
	}

	private void displayData(CatalogueItem catalogueItem){
	    nameTXT.setText("You are about to purchase " + catalogueItem.title);
        pointsValueTXT.setText(String.valueOf(catalogueItem.value));

        Glide.with(getContext())
                .load(catalogueItem.image.data.full_path)
				.apply(new RequestOptions()
				.placeholder(R.color.light_gray)
				.error(R.color.light_gray))
				.into(redeemableIV);

		typeIV.setImageDrawable(
				ActivityCompat.getDrawable(getContext(),
						catalogueItem.reward_type.equalsIgnoreCase("crystal") ?
								R.drawable.icon_lyka_gems :
								R.drawable.icon_lyka_chips));
	}

    @Override
    public void onStart() {
        super.onStart();
        setDialogWrapContent();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.closeBTN:
                dismiss();
                break;
            case R.id.redeemBTN:
				if (SystemClock.elapsedRealtime() - mLastClickTime < 300){
					return;
				}
				mLastClickTime = SystemClock.elapsedRealtime();
//                attemptRedeem(getCatalogue());
				if (callback != null){
					callback.onRedeemSuccess();
					dismiss();
				}
                break;
        }
    }

    private void attemptRedeem(CatalogueItem catalogueItem){
		RewardCatalogueRedeemRequest rewardCatalogueRedeemRequest = new RewardCatalogueRedeemRequest(getContext());
		rewardCatalogueRedeemRequest
				.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.addParameters(Keys.server.key.CATALOGUE_ID, catalogueItem.id)
				.addParameters(Keys.server.key.INCLUDE, "image")
				.execute();
    }

    @Subscribe
    public void onResponse(RewardCatalogueRedeemRequest.ServerResponse responseData) {
        RewardCatalogueShowTransformer rewardCatalogueShowTransformer = responseData.getData(RewardCatalogueShowTransformer.class);
        if(rewardCatalogueShowTransformer.status){
            UserData.updateRewardCrystal(rewardCatalogueShowTransformer.user.reward_crystal + "");
            UserData.updateRewardFragment(rewardCatalogueShowTransformer.user.reward_fragment + "");
            dismiss();
            RedeemRewardSuccessDialog.newInstance(rewardCatalogueShowTransformer.data).show(((BaseActivity) getContext()).getSupportFragmentManager(), RedeemRewardConfirmationDialog.TAG);
        }else {
            ToastMessage.show(getContext(), rewardCatalogueShowTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    public interface Callback{
		void onRedeemSuccess();
	}
}
