package com.thingsilikeapp.android.dialog.defaultdialog;

import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.goodiebag.pinview.Pinview;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.Passcode;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class PasscodeVerificationDialog extends BaseDialog implements  View.OnClickListener {
	public static final String TAG = PasscodeVerificationDialog.class.getName();

    @BindView(R.id.continueBTN)             TextView continueBTN;
    @BindView(R.id.pinView) 				Pinview pinView;
	@BindView(R.id.exitBTN) 				ImageView extiBTN;

	private Callback callback;

	public static PasscodeVerificationDialog Builder(Callback callback) {
		PasscodeVerificationDialog infoDialog = new PasscodeVerificationDialog();
		infoDialog.callback = callback;
		return infoDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_change_passcode;
	}

	@Override
	public void onViewReady() {
       continueBTN.setOnClickListener(this);
       extiBTN.setOnClickListener(this);
//				PasswordEditTextManager.addShowPassword(getContext(), pinView);

	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
		setDialogMatchParent();
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

		public void build(FragmentManager manager){
        show(manager, TAG);
	}

	@Subscribe
	public void onResponse(Passcode.PasscodeResponse responseData) {
		SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
		if(singleTransformer.status){
			ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
			Passcode.getDefault().unlock_gem(getContext());
		}else{
			ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
		}
	}

	@Subscribe
	public void onResponse(Passcode.UnlockResponse responseData) {
		SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
		if(singleTransformer.status){
			ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
			if (callback!=null){
				callback.onUnlock();
				dismiss();
			}
		}else{
			ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
		}
	}

	@Override
	public void onClick(View view) {
		switch(view.getId()){
			case R.id.continueBTN:
				if (pinView.getValue().equals("")){
					ToastMessage.show(getContext(), "Please input your 4 - digit passcode", ToastMessage.Status.FAILED);
				}else{
					Passcode.getDefault().validate_passcode(getContext(), pinView.getValue());
				}
				break;
			case R.id.exitBTN:
				dismiss();
				break;
		}
	}

	public interface Callback{
		void onUnlock();
	}
}


