package com.thingsilikeapp.android.dialog;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.EncashHistoryItem;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import java.text.DecimalFormat;

import butterknife.BindView;

public class EncashmentDetailDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = EncashmentDetailDialog.class.getName();

	private EncashHistoryItem encashHistoryItem;

	@BindView(R.id.refTXT) 				TextView refTXT;
	@BindView(R.id.statusTXT)           TextView statusTXT;
	@BindView(R.id.remarksTXT)          TextView reamarksTXT;
	@BindView(R.id.dateTXT)             TextView dateTXT;
	@BindView(R.id.gemTXT)              TextView gemTXT;
	@BindView(R.id.finalAmountTXT)      TextView finalAmountTXT;
	@BindView(R.id.serviceFeeTXT)       TextView serviceFeeTXT;
	@BindView(R.id.rateTXT)       		TextView rateTXT;
	@BindView(R.id.exitBTN) 			ImageView extiBTN;

	public static EncashmentDetailDialog newInstance(EncashHistoryItem encashHistoryItem) {
		EncashmentDetailDialog fragment = new EncashmentDetailDialog();
		fragment.encashHistoryItem = encashHistoryItem;
		return fragment;
	}
	@Override
	public int onLayoutSet() {
		return R.layout.dialog_encashment_detail;
	}

	@Override
	public void onViewReady() {
		DecimalFormat precision = new DecimalFormat("0.00");
// dblVariable is a number variable and not a String in this case

		refTXT.setText(encashHistoryItem.refno);
		statusTXT.setText(encashHistoryItem.status);
		reamarksTXT.setText(encashHistoryItem.remarks);
		dateTXT.setText(encashHistoryItem.requestDate);
		gemTXT.setText(encashHistoryItem.gemQty);
		finalAmountTXT.setText(encashHistoryItem.currency + " " +  precision.format(Double.parseDouble(encashHistoryItem.finalAmount)));
		serviceFeeTXT.setText(encashHistoryItem.currency + " " +  precision.format(Double.parseDouble(encashHistoryItem.serviceFee)));
		rateTXT.setText(encashHistoryItem.currency + " " + precision.format(Double.parseDouble(encashHistoryItem.rate)));



		switch (encashHistoryItem.status){
			case "close":
				statusTXT.setBackgroundResource(R.color.gray);
				break;
			case "completed":
			case "approved":
				statusTXT.setBackgroundResource(R.color.completed);
				break;
			case "decline":
			case "cancelled":
				statusTXT.setBackgroundResource(R.color.failed);
				break;
			case "pending":
				statusTXT.setBackgroundResource(R.color.gold);
				break;
		}

		extiBTN.setOnClickListener(this);

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View view) {
		switch(view.getId()){
			case R.id.exitBTN:
				dismiss();
				break;
		}
	}
}
