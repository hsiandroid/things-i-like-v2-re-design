package com.thingsilikeapp.android.dialog;

import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;

public class WalkThroughFinalDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = WalkThroughFinalDialog.class.getName();

    private Handler handler;
    private Runnable runnable;

	@BindView(R.id.dialogCON)               View dialogCON;
    @BindView(R.id.rewardCON)               View rewardCON;
    @BindView(R.id.rewardIV)                View rewardIV;

	public static WalkThroughFinalDialog newInstance() {
		WalkThroughFinalDialog fragment = new WalkThroughFinalDialog();
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_walkthrough_final;
	}

	@Override
	public void onViewReady() {
        dialogCON.setOnClickListener(this);


        RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

//Setup anim with desired properties
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE); //Repeat animation indefinitely
        anim.setDuration(10000); //Put desired duration per anim cycle here, in milliseconds

//Start animation
        rewardCON.startAnimation(anim);
	}

    @Override
    public void onResume() {
        super.onResume();
        timer();
    }

    private void timer(){
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                UserData.insert(UserData.WALKTHRU_CURRENT, 10);
                UserData.insert(UserData.WALKTHRU_DONE, true);
                UserData.insert(UserData.BIRTHDAY_COUNT, 3);
                UserData.insert(UserData.POST_SHOW, 3);
                UserData.insert(UserData.REQUEST_ADDRESS_SHOW, 3);
                UserData.insert(UserData.REQUEST_LIKE_SHOW, 3);
                UserData.insert(UserData.CREATE_POST_SHOW, 3);
                UserData.insert(UserData.SHARE_POST_SHOW, 3);
                UserData.insert(UserData.LIKE_POST_SHOW, 3);
                dismiss();
            }
        };
        handler.postDelayed(runnable, 5000);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (handler != null){
            handler.removeCallbacks(runnable);
        }
    }

    @Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dialogCON:
                UserData.insert(UserData.WALKTHRU_CURRENT, 10);
                UserData.insert(UserData.WALKTHRU_CURRENT_PROFILE, false);
                UserData.insert(UserData.WALKTHRU_DONE, true);
                UserData.insert(UserData.BIRTHDAY_COUNT, 3);
                UserData.insert(UserData.POST_SHOW, 3);
                UserData.insert(UserData.REQUEST_ADDRESS_SHOW, 3);
                UserData.insert(UserData.REQUEST_LIKE_SHOW, 3);
                UserData.insert(UserData.CREATE_POST_SHOW, 3);
                UserData.insert(UserData.SHARE_POST_SHOW, 3);
                UserData.insert(UserData.LIKE_POST_SHOW, 3);
                    dismiss();
            break;
        }
    }
}
