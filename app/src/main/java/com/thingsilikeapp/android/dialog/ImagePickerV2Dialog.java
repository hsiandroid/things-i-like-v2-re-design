package com.thingsilikeapp.android.dialog;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.system.ErrnoException;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;

import com.theartofdev.edmodo.cropper.CropImageView;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.AlbumAdapter;
import com.thingsilikeapp.android.adapter.GalleryAdapter;
import com.thingsilikeapp.data.model.GalleryItem;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.ImageQualityManager;
import com.thingsilikeapp.vendor.android.java.InfiniteScrollListener;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;
import com.thingsilikeapp.vendor.android.java.image.AlbumItem;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class ImagePickerV2Dialog extends BaseDialog implements View.OnClickListener,
        CropImageView.OnSetImageUriCompleteListener,
        CropImageView.OnCropImageCompleteListener,
        GalleryAdapter.ClickListener{

    public static final String TAG = ImagePickerV2Dialog.class.getName();

    private static final int REQUEST_CAMERA = 10001;
    private static final int REQUEST_GALLERY = 10002;

    private static final int PERMISSION_CAMERA = 201;
    private static final int PERMISSION_GALLERY = 202;
    private File photoFile;

    public ImageCallback imageCallback;
    private GalleryAdapter galleryAdapter;
    private AlbumAdapter albumAdapter;

    @BindView(R.id.cropImageView)		CropImageView cropImageView;
    @BindView(R.id.cameraBTN)			View cameraBTN;
    @BindView(R.id.galleryBTN) 			View galleryBTN;
    @BindView(R.id.rotateLeftBTN) 		View rotateLeftBTN;
    @BindView(R.id.rotateRightBTN) 		View rotateRightBTN;
    @BindView(R.id.pickerBTN)			View pickerBTN;
    @BindView(R.id.imageCON)			View imageCON;
    @BindView(R.id.mainBackButtonIV)	View mainBackButtonIV;
    @BindView(R.id.placeHolderIV)		View placeHolderIV;
    @BindView(R.id.fromCameraBTN)		View fromCameraBTN;
    @BindView(R.id.fromGalleryBTN)		View fromGalleryBTN;
    @BindView(R.id.titleTXT)			TextView titleTXT;
    @BindView(R.id.galleryGV)			GridView galleryGV;
    @BindView(R.id.albumSPR)			Spinner albumSPR;

    @State String title;
    @State boolean isAspectRatio = false;
    private String path = "";
    private Uri newURI ;

    public Uri uri;

    public static ImagePickerV2Dialog newInstance(String title, boolean isAspectRatio, ImageCallback imageCallback) {
        ImagePickerV2Dialog imagePickerV2Dialog = new ImagePickerV2Dialog();
        imagePickerV2Dialog.title = title;
        imagePickerV2Dialog.isAspectRatio = isAspectRatio;
        imagePickerV2Dialog.imageCallback = imageCallback;
        return imagePickerV2Dialog;
    }

    public static ImagePickerV2Dialog newInstance(String title, boolean isAspectRatio, Uri uri, ImageCallback imageCallback) {
        ImagePickerV2Dialog fragment = new ImagePickerV2Dialog();
        fragment.title = title;
        fragment.isAspectRatio = isAspectRatio;
        fragment.uri = uri;
        fragment.imageCallback = imageCallback;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.dialog_image_picker;
    }

    @Override
    public void onViewReady() {
        pickerBTN.setOnClickListener(this);
        cameraBTN.setOnClickListener(this);
        galleryBTN.setOnClickListener(this);
        rotateLeftBTN.setOnClickListener(this);
        rotateRightBTN.setOnClickListener(this);
        mainBackButtonIV.setOnClickListener(this);
        imageCON.setOnClickListener(this);
        fromCameraBTN.setOnClickListener(this);
        fromGalleryBTN.setOnClickListener(this);
//		mainTitleTXT.setText("Upload a Photo");

        if(title == null){
            titleTXT.setText(getString(R.string.app_name));
        }else{
            titleTXT.setText(title);
        }

        cropImageView.setFixedAspectRatio(isAspectRatio);

        if(uri != null){
            showImage(uri);
        }

        if(PermissionChecker.checkPermissions(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_GALLERY)){
            initGallery();
            setAlbum();
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    private void setAlbum(){
        albumAdapter = new AlbumAdapter(getContext());
        albumAdapter.setNewData(getAlbumList());
        albumSPR.setAdapter(albumAdapter);
        albumSPR.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                galleryAdapter.setNewData(getAllImagesByBucket(albumAdapter.getData(position).id));
                if (galleryAdapter.getCount() > 0 && albumAdapter.getCount() > 0){
                    path = galleryAdapter.getGalleryItemByPosition(0).absolutePath;
                    newURI = Uri.fromFile(new File(galleryAdapter.getGalleryItemByPosition(0).absolutePath));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        cropImageView.setOnSetImageUriCompleteListener(this);
        cropImageView.setOnCropImageCompleteListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        gallerySelected();
    }

    @Override
    public void  onStop() {
        super.onStop();
        cropImageView.setOnSetImageUriCompleteListener(null);
        cropImageView.setOnCropImageCompleteListener(null);
    }

    private void gallerySelected(){
        fromGalleryBTN.setSelected(true);
        fromCameraBTN.setSelected(false);
    }

    private void cameraSelected(){
        fromGalleryBTN.setSelected(false);
        fromCameraBTN.setSelected(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pickerBTN:
                cropImageView.getCroppedImageAsync();
                break;
            case R.id.cameraBTN:
            case R.id.imageCON:
                openCamera();
                break;
            case R.id.galleryBTN:
                openGallery();
                break;
            case R.id.rotateLeftBTN:
                cropImageView.rotateImage(-90);
                break;
            case R.id.rotateRightBTN:
                cropImageView.rotateImage(90);
                break;
            case R.id.mainBackButtonIV:
                dismiss();
                break;
            case R.id.fromCameraBTN:
                openCamera();
                cameraSelected();
                break;
            case R.id.fromGalleryBTN:
//                imagePickerSV.scrollTo(0, 0);
                break;
        }
    }

    private void initGallery(){
        galleryAdapter = new GalleryAdapter(getContext());
        galleryAdapter.setOnItemClickListener(this);
        galleryGV.setAdapter(galleryAdapter);
        galleryGV.setOnScrollListener(new InfiniteScrollListener(5) {
            @Override
            public void loadMore(int page, int totalItemsCount) {
                galleryAdapter.addPage(page);
            }
        });

//        imagePickerSV.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                if(v.getChildAt(v.getChildCount() - 1) != null) {
//                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
//                            scrollY > oldScrollY) {
//                    		page++;
//                            galleryAdapter.addPage(page);
//                    }
//                }
//            }
//        });

    }

    public List<GalleryItem> getAllImagesByBucket(String id) {

        int indexData;
        int indexName;

        List<GalleryItem> listOfAllImages = new ArrayList<>();
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = { MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

        String search = MediaStore.Images.ImageColumns.BUCKET_ID + "=\"" + id + "\"";

        Cursor cursor = getContext().getContentResolver().query(uri, projection, search, null, MediaStore.Images.Media.DATE_ADDED + " DESC");

        indexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        indexName = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        GalleryItem galleryItem;

        while (cursor.moveToNext()) {
            galleryItem = new GalleryItem();
            galleryItem.absolutePath = cursor.getString(indexData);
            galleryItem.name = cursor.getString(indexName);
            listOfAllImages.add(galleryItem);
        }

        if(listOfAllImages.size() > 0 && this.uri == null){
            showImage(Uri.fromFile(new File(listOfAllImages.get(0).absolutePath)));
        }

        return listOfAllImages;
    }

    private List<AlbumItem> getAlbumList(){

        List<AlbumItem> albumItems = new ArrayList<>();
        AlbumItem albumItem;

        String[] PROJECTION_BUCKET = {
                MediaStore.Images.ImageColumns.BUCKET_ID,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.DATA};

        String BUCKET_GROUP_BY = "1) GROUP BY 1,(2";
        String BUCKET_ORDER_BY = "MAX(datetaken) DESC";
        Uri images = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;


        Cursor cursor = getContext().getContentResolver().query(images, PROJECTION_BUCKET, BUCKET_GROUP_BY, null, BUCKET_ORDER_BY);
        if (cursor != null || !cursor.equals("") || !cursor.equals(null)) {
            if (cursor.moveToFirst()) {
                do {
                    albumItem = new AlbumItem();
                    albumItem.id = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_ID));
                    albumItem.album = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME));
                    albumItem.date = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN));
                    albumItem.thumbnail = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                    albumItems.add(albumItem);
                } while (cursor.moveToNext());
            }
        }

        return albumItems;
    }

    @Override
    public void onImageClickListener(GalleryItem galleryItem) {
        if(galleryItem.isCamera){
            openCamera();
        }else{
            showImage(Uri.fromFile(new File(galleryItem.absolutePath)));
        }
        Log.e("OriginalPath", ">>>" + galleryItem.absolutePath);
        path = galleryItem.absolutePath;
        newURI = Uri.fromFile(new File(galleryItem.absolutePath));
        Log.e("NewURI", ">>>" + newURI);
        uri = null;
//        imagePickerSV.scrollTo(0, 0);
    }

    @Override
    public void onSelectedClickListener(int position) {

    }

    @Override
    public void onVideoClickListener(GalleryItem galleryItem) {

    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        if (result != null) {
            if (result.isSuccessful()) {
                File imageFile = getImageFile(view.getCroppedImage());
                if (imageFile != null) {
                    String newPath = ImageQualityManager.compressImage(getContext(), imageFile.getAbsolutePath());
//                    String newPath1 = ImageQualityManager.compressImageFinal(getContext(), newPath);
                    Log.e("NewPath", ">>>" + newPath);
//                    Log.e("NewPath1", ">>>" +  newPath1);
                    File cropFile = new File(newPath);
                    if (imageCallback != null) {
                        imageCallback.result(cropFile);
                    }
                    dismiss();
                }
            }
        }
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {

    }

    public interface ImageCallback{
        void result(File file);
    }

    private File getImageFile(Bitmap bitmap) {
        String imageFileName = "ThingsILikeCacheImage";
        File storageDir = new File(getContext().getCacheDir(), getString(R.string.app_name));
        if ( !storageDir.exists() ) {
            storageDir.mkdirs();
        }

        final int MAX_IMAGE_SIZE = 700 * 1024; // max final file size in kilobytes
//        final int MAX_IMAGE_SIZE = 480 * 858; // max final file size in kilobytes
//        final int MAX_IMAGE_SIZE = 360 * 640; // max final file size in kilobytes
        int compressQuality = 100; // quality decreasing by 5 every loop.
        int streamLength;
        do{
            ByteArrayOutputStream bmpStream = new ByteArrayOutputStream();
            Log.d("compressBitmap", "Quality: " + compressQuality);
            bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream);
            byte[] bmpPicByteArray = bmpStream.toByteArray();
            streamLength = bmpPicByteArray.length;
            compressQuality -= 5;
            Log.d("compressBitmap", "Size: " + streamLength/1024+" kb");
        }while (streamLength >= MAX_IMAGE_SIZE);


        File image = null;
//        Log.e("Bytes" ,">>>" + bitmap.getRowBytes() * bitmap.getHeight());
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            FileOutputStream bmpFile = new FileOutputStream(image);
            bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpFile);
            bmpFile.flush();
            bmpFile.close();

//                FileOutputStream bmpFile = new FileOutputStream(image);
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bmpFile);
//                bmpFile.flush();
//                bmpFile.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    private void openGallery(){
        if(PermissionChecker.checkPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_GALLERY)){
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent, REQUEST_GALLERY);
        }
    }

    private void openCamera(){
        if(PermissionChecker.checkPermissions(getActivity(), new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CAMERA)){
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                photoFile = createImageFile();
                if (photoFile != null) {
                    Uri uri = Uri.fromFile(photoFile);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                            | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    if (uri != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    }
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            }
        }
    }

    private File createImageFile() {
        String imageFileName = "I_LIKE_TEMP";
        File storageDir = new File(Environment.getExternalStorageDirectory(), getString(R.string.app_name) +"/data/img/temp");
        if ( !storageDir.exists() ) {
            storageDir.mkdirs();
        }
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".png",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    @Override
    public  void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_CAMERA:
            case REQUEST_GALLERY:
                if(resultCode == Activity.RESULT_OK){
                    showImage(getPickImageResultUri(data));
                }
                break;
        }
    }

    public void showImage(Uri imageUri){
        boolean requirePermissions = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                isUriRequiresPermissions(imageUri)) {

            requirePermissions = true;
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
        }

        if (!requirePermissions) {
            cropImageView.setImageUriAsync(imageUri);
        }
        cropImageView.setVisibility(View.VISIBLE);
        placeHolderIV.setVisibility(View.GONE);
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public Uri getPickImageResultUri(Intent  data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null  && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ?  getCaptureImageOutputUri() : data.getData();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        if (photoFile != null) {
            outputFileUri = Uri.fromFile(photoFile);
        }
        return outputFileUri;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CAMERA) {
            if(getBaseActivity().isAllPermissionResultGranted(grantResults)){
                openCamera();
            }
        }
        if (requestCode == PERMISSION_GALLERY) {
            if(getBaseActivity().isAllPermissionResultGranted(grantResults)){
                initGallery();
                setAlbum();
            }else{
                dismiss();
            }
        }
    }
}
