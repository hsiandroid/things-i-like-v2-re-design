package com.thingsilikeapp.android.dialog;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.FragmentViewPagerAdapter;
import com.thingsilikeapp.android.fragment.tutorial.Tutorial1Fragment;
import com.thingsilikeapp.android.fragment.tutorial.Tutorial2Fragment;
import com.thingsilikeapp.android.fragment.tutorial.Tutorial3Fragment;
import com.thingsilikeapp.android.fragment.tutorial.Tutorial4Fragment;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;

public class AppTutorialDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = AppTutorialDialog.class.getName();

	private FragmentViewPagerAdapter fragmentViewPagerAdapter;

	@BindView(R.id.continueBTN)		TextView continueBTN;
	@BindView(R.id.neverShowCHBX)	CheckBox neverShowCHBX;
	@BindView(R.id.tutorialVP)		ViewPager tutorialVP;

	public static AppTutorialDialog newInstance() {
		AppTutorialDialog appTutorialDialog = new AppTutorialDialog();
		return appTutorialDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_app_tutorial;
	}

	@Override
	public void onViewReady() {
		continueBTN.setOnClickListener(this);

		setupViewPager();
	}

//	public void setupSlider(){
//		sliderLayout.removeAllSliders();
//		sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
//		sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
//		sliderLayout.setCustomAnimation(new DescriptionAnimation());
//		sliderLayout.setDuration(5000);
//		sliderLayout.addOnPageChangeListener(this);
//	}
//
//	public void addImage(){
//		List<Integer> resources = new ArrayList<>();
//		resources.add(R.drawable.tutorial_1);
//		resources.add(R.drawable.tutorial_2);
//		resources.add(R.drawable.tutorial_3);
//		resources.add(R.drawable.tutorial_4);
//		for (Integer resource : resources){
//			sliderLayout.addSlider(new TextSliderView(getContext())
//					.image(resource)
//					.setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
//					.setOnSliderClickListener(this));
//		}
//	}

	private void setupViewPager(){
		fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getChildFragmentManager());
		fragmentViewPagerAdapter.addFragment(Tutorial1Fragment.newInstance());
		fragmentViewPagerAdapter.addFragment(Tutorial2Fragment.newInstance());
		fragmentViewPagerAdapter.addFragment(Tutorial3Fragment.newInstance());
		fragmentViewPagerAdapter.addFragment(Tutorial4Fragment.newInstance());
		tutorialVP.setPageMargin(20);
		tutorialVP.setPageMarginDrawable(null);
		tutorialVP.setAdapter(fragmentViewPagerAdapter);
		tutorialVP.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

			}

			@Override
			public void onPageSelected(int position) {
				switch (position){
					case 0:
						break;
					case 1:
						break;
				}
			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});

		tutorialVP.setOffscreenPageLimit(2);

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
//
//	@Override
//	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//		Log.e("Slider Demo", "Pag eScroll State Changed: " + position);
//	}
//
//	@Override
//	public void onPageSelected(int position) {
//		if (sliderLayout != null) {
//			if (position == 3) {
//				sliderLayout.stopAutoCycle();
//			} else {
//
//				sliderLayout.startAutoCycle(5000, 5000, true);
//			}
//		}
//	}
//
//	@Override
//	public void onPageScrollStateChanged(int state) {
//
//	}
//
//	@Override
//	public void onSliderClick(BaseSliderView sliderLayout) {
//
//	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.continueBTN:
				if (neverShowCHBX.isChecked()){
					UserData.insert(UserData.FIRST_OPEN,false);
					dismiss();
				}else {
					dismiss();
				}
				break;
		}
	}
}
