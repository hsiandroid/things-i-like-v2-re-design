package com.thingsilikeapp.android.dialog;

import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.user.BirthdayReminderRequest;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class BirthdayRemindMeDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = BirthdayRemindMeDialog.class.getName();

	private Callback callback;

	@BindView(R.id.headerTXT)		TextView headerTXT;
	@BindView(R.id.imageCIV)		ImageView imageCIV;
	@BindView(R.id.commonNameTXT)	TextView commonNameTXT;
	@BindView(R.id.sendBTN)	        View sendBTN;
	@BindView(R.id.cancelBTN)	    View cancelBTN;

	@BindView(R.id.groupRG)         RadioGroup groupRG;

	@BindView(R.id.set1RB)          RadioButton set1RB;
    @BindView(R.id.set2RB)	        RadioButton set2RB;
    @BindView(R.id.set3RB)	        RadioButton set3RB;
    @BindView(R.id.set4RB)	        RadioButton set4RB;

	@BindView(R.id.set1CON)	        View set1CON;
	@BindView(R.id.set2CON)	        View set2CON;
	@BindView(R.id.set3CON)	        View set3CON;

	@State String raw;

	public static BirthdayRemindMeDialog newInstance(UserItem userItem, Callback callback) {
		BirthdayRemindMeDialog fragment = new BirthdayRemindMeDialog();
		fragment.raw = new Gson().toJson(userItem);
		fragment.callback = callback;
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_birthday_remind_me;
	}


	@Override
	public void onViewReady() {
		sendBTN.setOnClickListener(this);
		cancelBTN.setOnClickListener(this);
		set1CON.setOnClickListener(this);
		set2CON.setOnClickListener(this);
		set3CON.setOnClickListener(this);
		displayData(new Gson().fromJson(raw, UserItem.class));
	}

	private void displayData(UserItem userItem){
		String gender = userItem.info.data.gender.equalsIgnoreCase("male") ? "HIS" : "HER";
		headerTXT.setText("NOTIFY ME ON " + gender + " SPECIAL DAY!");
		commonNameTXT.setText(userItem.common_name);

		sendBTN.setTag(userItem.id);
        Log.e("Log", ">>>" + userItem.id);
        Log.e("Log", ">>>" + userItem.social.data.bday_reminder_id);

		Glide.with(getContext())
				.load(userItem.getAvatar())
				.apply(new RequestOptions()
				.placeholder(R.drawable.placeholder_avatar)
				.error(R.drawable.placeholder_avatar)
				.fitCenter()
				.dontAnimate()
				.diskCacheStrategy(DiskCacheStrategy.ALL)
				.skipMemoryCache(true))
				.into(imageCIV);
		int reminder = userItem.social.data.bday_reminder_id;
        setSelected(reminder);
		set4RB.setVisibility(reminder > 0 ? View.VISIBLE : View.GONE);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.sendBTN:
				remindMe((int) view.getTag());
				break;
			case R.id.cancelBTN:
				dismiss();
				break;
			case R.id.set1CON:
                setSelected(1);
				break;
			case R.id.set2CON:
                setSelected(2);
				break;
			case R.id.set3CON:
                setSelected(3);
				break;
		}
	}

	private int getSelectedReminder(){
        if(set4RB.isChecked()){
            return 0;
        }

        if(set1RB.isChecked()){
            return 1;
        }

        if(set2RB.isChecked()){
            return 2;
        }

        if(set3RB.isChecked()){
            return 3;
        }
        return 1;
    }

	private void setSelected(int pos){
        switch (pos){
            case 0:
                groupRG.check(R.id.set4RB);
                break;
            case 2:
                groupRG.check(R.id.set2RB);
                break;
            case 3:
                groupRG.check(R.id.set3RB);
                break;
            case 1:
            default:
                groupRG.check(R.id.set1RB);
                break;
        }
    }

	public interface Callback{
		void remindMe(UserItem userItem);
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	private void remindMe(int id){

        Log.e("BDAY_REMINDER_ID", ">>>" + getSelectedReminder());

		BirthdayReminderRequest randomRequest = new BirthdayReminderRequest(getContext());
		randomRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.addParameters(Keys.server.key.INCLUDE, "info,social")
				.addParameters(Keys.server.key.USER_ID, id)
				.addParameters(Keys.server.key.BDAY_REMINDER_ID, getSelectedReminder())
				.execute();
	}

	@Subscribe
	public void onResponse(BirthdayReminderRequest.ServerResponse responseData) {
		UserTransformer baseTransformer = responseData.getData(UserTransformer.class);
		if(baseTransformer.status){
			if(callback != null){
				callback.remindMe(baseTransformer.userItem);
			}
			dismiss();
			ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
		}else{
			ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
		}
	}
}
