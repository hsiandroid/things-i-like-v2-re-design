package com.thingsilikeapp.android.dialog;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.server.request.auth.VerificationCodeRequest;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ForgotPasswordDialog extends BaseDialog implements View.OnClickListener{

    public static final String TAG = ForgotPasswordDialog.class.getName();


    @BindView(R.id.requestEmailTV)      TextView requestEmailTV;
    @BindView(R.id.verificationCodeTV)  TextView verificationCodeTV;
    @BindView(R.id.closeBTN)            View closeBTN;
    @BindView(R.id.emailET)             EditText emailET;

    public static ForgotPasswordDialog newInstance() {
        ForgotPasswordDialog forgotPasswordDialog = new ForgotPasswordDialog();
        return forgotPasswordDialog;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.dialog_forgot_password;
    }

    @Override
    public void onViewReady() {

        requestEmailTV.setOnClickListener(this);
        verificationCodeTV.setOnClickListener(this);
        closeBTN.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.requestEmailTV:
                attemptRequestVerification();
                break;
            case R.id.verificationCodeTV:
//                landingActivity.openResetPasswordFragment(emailET.getText().toString());
                ResetPasswordDialog.newInstance(emailET.getText().toString(),this).show(getChildFragmentManager(),ResetPasswordDialog.TAG);
                break;
            case R.id.closeBTN:
                dismiss();
                break;
        }
    }

    private void attemptRequestVerification(){
        VerificationCodeRequest verificationCodeRequest = new VerificationCodeRequest(getContext());
        verificationCodeRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Checking your email...", false, false))
                .addParameters(Keys.server.key.EMAIL, emailET.getText().toString())
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        setDialogMatchParent();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(VerificationCodeRequest.ServerResponse responseData) {
        BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            ResetPasswordDialog.newInstance(emailET.getText().toString(),this).show(getChildFragmentManager(),ResetPasswordDialog.TAG);
        }else {
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}
