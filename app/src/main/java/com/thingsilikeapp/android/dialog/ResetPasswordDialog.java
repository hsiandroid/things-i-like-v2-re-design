package com.thingsilikeapp.android.dialog;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.server.request.auth.ResetPasswordRequest;
import com.thingsilikeapp.server.request.auth.VerificationCodeRequest;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class ResetPasswordDialog extends BaseDialog implements View.OnClickListener{

    public static final String TAG = ResetPasswordDialog.class.getName();


    @BindView(R.id.confirmNewPasswordTV)        TextView confirmNewPasswordTV;
    @BindView(R.id.resendVerificationCodeTV)    TextView resendVerificationCodeTV;
    @BindView(R.id.emailET)                     EditText emailET;
    @BindView(R.id.verificationCodeET)          EditText verificationCodeET;
    @BindView(R.id.passwordET)                  EditText passwordET;
    @BindView(R.id.showPasswordBTN)             ImageView showPasswordBTN;
    @BindView(R.id.confirmPasswordET)           EditText confirmPasswordET;
    @BindView(R.id.showConfirmPasswordBTN)      ImageView showConfirmPasswordBTN;
    @BindView(R.id.closeBTN)                    ImageView closeBTN;
    @BindView(R.id.showPasswordCHBX)            CheckBox showPasswordCHBX;
    @BindView(R.id.showPassword1CHBX)            CheckBox showPassword1CHBX;

    @State String email;

    private ForgotPasswordDialog forgotPasswordDialog;

    public static ResetPasswordDialog newInstance(String email,ForgotPasswordDialog forgotPasswordDialog) {
        ResetPasswordDialog resetPasswordDialog = new ResetPasswordDialog();
        resetPasswordDialog.forgotPasswordDialog = forgotPasswordDialog;
        resetPasswordDialog.email = email;
        return resetPasswordDialog;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.dialog_reset_password;
    }

    @Override
    public void onViewReady() {

        emailET.setText(email);
        confirmNewPasswordTV.setOnClickListener(this);
        resendVerificationCodeTV.setOnClickListener(this);
        closeBTN.setOnClickListener(this);

        PasswordEditTextManager.addShowPassword(getContext(), passwordET, showPassword1CHBX);
        PasswordEditTextManager.addShowPassword(getContext(), confirmPasswordET, showPasswordCHBX);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.confirmNewPasswordTV:
                attemptRequestVerification();
                break;
            case R.id.resendVerificationCodeTV:
                attemptResendRequestVerification();
                dismiss();
                break;
            case R.id.closeBTN:
                dismiss();
                break;
        }
    }

    private void attemptRequestVerification(){
        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest(getContext());
        resetPasswordRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Resetting password...", false, false))
                .addParameters(Keys.server.key.EMAIL, emailET.getText().toString())
                .addParameters(Keys.server.key.VALIDATION_TOKEN, verificationCodeET.getText().toString())
                .addParameters(Keys.server.key.PASSWORD, passwordET.getText().toString())
                .addParameters(Keys.server.key.PASSWORD_CONFIRMATION, confirmPasswordET.getText().toString())
                .execute();
    }

    private void attemptResendRequestVerification(){
        VerificationCodeRequest verificationCodeRequest = new VerificationCodeRequest(getContext());
        verificationCodeRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Resending verification code...", false, false))
                .addParameters(Keys.server.key.EMAIL, emailET.getText().toString())
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        setDialogMatchParent();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(VerificationCodeRequest.ServerResponse responseData) {
        BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(getActivity(), baseTransformer.msg,ToastMessage.Status.SUCCESS);
            forgotPasswordDialog.dismiss();
        }
    }

    @Subscribe
    public void onResponse(ResetPasswordRequest.ServerResponse responseData) {
        BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            dismiss();
            forgotPasswordDialog.dismiss();
        }else {
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}
