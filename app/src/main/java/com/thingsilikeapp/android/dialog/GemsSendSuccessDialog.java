package com.thingsilikeapp.android.dialog;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.WalletItem;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class GemsSendSuccessDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = GemsSendSuccessDialog.class.getName();

	private WalletItem walletItem;
	private Callback callback;

	@BindView(R.id.avatarCIV) 			         ImageView avatarCIV;
	@BindView(R.id.nameTXT)                      TextView nameTXT;
	@BindView(R.id.refIDTXT)                     TextView refIDTXT;
	@BindView(R.id.receivedCountTXT)             TextView receivedCountTXT;
	@BindView(R.id.continueBTN)                  TextView continueBTN;

	@State String avatar;

	public static GemsSendSuccessDialog newInstance(WalletItem walletItem, String avatar, Callback callback) {
		GemsSendSuccessDialog gemsSendConfirmationDialog = new GemsSendSuccessDialog();
		gemsSendConfirmationDialog.walletItem = walletItem;
		gemsSendConfirmationDialog.callback = callback;
		gemsSendConfirmationDialog.avatar =avatar;
		return gemsSendConfirmationDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_gems_send_success;
	}

	@Override
	public void onViewReady() {
        continueBTN.setOnClickListener(this);

        if(walletItem != null){
            Picasso.with(getContext())
                    .load(avatar)
                    .placeholder(R.drawable.placeholder_avatar)
                    .error(R.drawable.placeholder_avatar)
                    .into(avatarCIV);

            nameTXT.setText(walletItem.name);
            receivedCountTXT.setText(walletItem.amount);
            refIDTXT.setText("Ref.ID: " + walletItem.refid);
        }
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.continueBTN:
				if(callback != null){
					callback.onSuccess();
					dismiss();
				}
				break;
		}
	}

	public interface Callback {
		void onSuccess();
	}
}
