package com.thingsilikeapp.android.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class SendGiftDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = SendGiftDialog.class.getName();

	private Callback callback;
	@State WishListItem wishListItem;
	@State String title;
	@State int id;

//	public static SendGiftDialog newInstance(String title, WishListItem wishListItem, Callback callback) {
//		SendGiftDialog sendGiftDialog = new SendGiftDialog();
//		sendGiftDialog.title = title;
//		sendGiftDialog.callback = callback;
//		sendGiftDialog.wishListItem = wishListItem;
//		sendGiftDialog.id = wishListItem.id;
//		return sendGiftDialog;
//	}

	public static SendGiftDialog newInstance(String title, int id, Callback callback) {
		SendGiftDialog sendGiftDialog = new SendGiftDialog();
		sendGiftDialog.title = title;
		sendGiftDialog.callback = callback;
		sendGiftDialog.id = id;
		return sendGiftDialog;
	}

	@BindView(R.id.dedicationET) 	EditText dedicationET;
	@BindView(R.id.confirmBTN) 		TextView confirmBTN;
	@BindView(R.id.titleTXT) 		TextView titleTXT;
	@BindView(R.id.noteTXT) 		TextView noteTXT;

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_send_gift;
	}

	@Override
	public void onViewReady() {
		confirmBTN.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.confirmBTN:
				if(callback != null){
					callback.onAccept(id, dedicationET.getText().toString());
				}
				dismiss();
				break;
		}
	}

	public interface Callback{
		void onAccept(int wishlistID, String dedication);
	}
}
