package com.thingsilikeapp.android.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.adapter.CardRecyclerViewAdapter;
import com.thingsilikeapp.android.adapter.CardRecyclerViewAdapter;
import com.thingsilikeapp.data.model.BankCardItem;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.GemHelper;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class GemsBuyOptionDialog extends BaseDialog implements
        View.OnClickListener,
        WebViewGemPaymentTypeDialog.Callback,
        CardRecyclerViewAdapter.ClickListener,
        EndlessRecyclerViewScrollListener.Callback, AddCardDialog.Callback{

	public static final String TAG = GemsBuyOptionDialog.class.getName();

	private RewardsActivity activity;
	private Callback callback;
    private CardRecyclerViewAdapter adapter;
    private LinearLayoutManager manager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private APIRequest apiRequest;
    private String selectedCard = "";

	@BindView(R.id.exitBTN)                 View exitBTN;
	@BindView(R.id.cardRV)                  RecyclerView cardRV;
	@BindView(R.id.addCardBTN)              TextView addCardBTN;
	@BindView(R.id.proceedBTN)              TextView proceedBTN;
	@BindView(R.id.lykaGemTXT)              TextView lykaGemTXT;
	@BindView(R.id.amountTXT)               TextView amountTXT;

	@State String url;
	@State String amount;
	@State String totalgem;
	@State String totalamount;

	public static GemsBuyOptionDialog newInstance(String url, String amount, String totalgem, String totalamount, Callback callback) {
		GemsBuyOptionDialog gemsSendConfirmationDialog = new GemsBuyOptionDialog();
		gemsSendConfirmationDialog.url = url;
		gemsSendConfirmationDialog.amount = amount;
		gemsSendConfirmationDialog.totalgem = totalgem;
		gemsSendConfirmationDialog.totalamount = totalamount;
		gemsSendConfirmationDialog.callback = callback;
		return gemsSendConfirmationDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_gem_buy_option;
	}

	@Override
	public void onViewReady() {
	    activity = (RewardsActivity) getContext();
        exitBTN.setOnClickListener(this);
        addCardBTN.setOnClickListener(this);
        proceedBTN.setOnClickListener(this);
        lykaGemTXT.setText(totalgem);
        amountTXT.setText(totalamount);
        setUpCard();
        adapter.addNewData(getDefaultData());
	}
    private void setUpCard(){
        adapter = new CardRecyclerViewAdapter(getContext());
        manager = new LinearLayoutManager(getContext());
        manager.setReverseLayout(true);
        cardRV.setLayoutManager(manager);
        cardRV.setAdapter(adapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(manager, this);
        cardRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        adapter.setOnItemClickListener(this);
    }

    private List<BankCardItem> getDefaultData(){
	    int id = adapter.getItemCount() + 1;
        List<BankCardItem> androidModels = new ArrayList<>();
        BankCardItem defaultItem;
            defaultItem = new BankCardItem();
            defaultItem.id = id;
            defaultItem.cardType = "paypal";
            androidModels.add(defaultItem);
        return androidModels;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        setDialogMatchParent();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void refreshList(){
        apiRequest = Wallet.getDefault().allCard(getContext());
        apiRequest.first();
    }

    @Subscribe
    public void onResponse(Wallet.AllCardResponse colletionResponse) {
        CollectionTransformer<BankCardItem> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (colletionResponse.isNext()) {
                adapter.addNewData(collectionTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                adapter.setNewData(collectionTransformer.data);
            }
        }
    }

    @Subscribe
    public void onResponse(Wallet.BuyCardResponse tranformerResponse) {
        SingleTransformer<BankCardItem> transformer = tranformerResponse.getData(SingleTransformer.class);
        if (transformer.status) {
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.SUCCESS);
            if (callback != null){
                callback.onGemOptionSuccess();
                dismiss();
            }
        }else{
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.FAILED);
        }
    }

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.exitBTN:
				dismiss();
				break;
            case R.id.addCardBTN:
                AddCardDialog.newInstance(this).show(getFragmentManager(), TAG);
                break;
            case R.id.proceedBTN:
                setProceedBTN();
                break;
		}
	}

    private void setProceedBTN() {
            if (!selectedCard.equals("")){
                if (adapter.getCardType().equalsIgnoreCase("paypal")){
                    WebViewGemPaymentTypeDialog.newInstance("Buy Gem", url, this).show(getFragmentManager(), TAG);
                } else{
                    Wallet.getDefault().buyCard(getContext(), amount, Integer.parseInt(adapter.getSelectedItems()));
                }
            } else {
                ToastMessage.show(activity,"Please select a card", ToastMessage.Status.FAILED);
            }


    }

    @Override
    public void onGemPaymentSuccess() {
        if(callback != null){
            callback.onGemOptionSuccess();
            dismiss();
        }
    }

    @Override
    public void onItemClick(BankCardItem bankCardItem) {
        selectedCard = bankCardItem.cardType;
        adapter.setSelected(bankCardItem.id);
    }

    @Override
    public void onItemLongClick(BankCardItem bankCardItem) {

    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
	    apiRequest.nextPage();
    }

    @Override
    public void onAddCardSuccess() {
        refreshList();
        adapter.addNewData(getDefaultData());
    }

    public interface Callback {
        void onGemOptionSuccess();
    }
}
