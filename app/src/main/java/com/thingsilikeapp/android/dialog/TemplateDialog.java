package com.thingsilikeapp.android.dialog;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

public class TemplateDialog extends BaseDialog {
	public static final String TAG = TemplateDialog.class.getName();

	public static TemplateDialog newInstance() {
		TemplateDialog fragment = new TemplateDialog();
		return fragment;
	}
	@Override
	public int onLayoutSet() {
		return R.layout.dialog_template;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
}
