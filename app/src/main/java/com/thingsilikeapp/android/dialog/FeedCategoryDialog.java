package com.thingsilikeapp.android.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.FeedSearchCategoryRecycleViewAdapter;
import com.thingsilikeapp.data.model.FeedSearchCategoryItem;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class FeedCategoryDialog extends BaseDialog implements
        FeedSearchCategoryRecycleViewAdapter.ClickListener, View.OnClickListener, SearchDialog.Callback{
	public static final String TAG = FeedCategoryDialog.class.getName();

	private Callback callback;

	private FeedSearchCategoryRecycleViewAdapter feedSearchCategoryRecycleViewAdapter;
	private LinearLayoutManager linearLayoutManager;
	private String keyword= "";

	@BindView(R.id.feedCategoryRV)              RecyclerView feedCategoryRV;
	@BindView(R.id.viewCON)                     View viewCON;

	public static FeedCategoryDialog newInstance(Callback callback) {
		FeedCategoryDialog feedCategoryDialog = new FeedCategoryDialog();
		feedCategoryDialog.callback = callback;
		return feedCategoryDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_feed_category;
	}

	@Override
	public void onViewReady() {
        setUpFeedSearchCategoryListView();

        viewCON.setOnClickListener(this);
	}

	private void setUpFeedSearchCategoryListView(){
	    linearLayoutManager = new LinearLayoutManager(getContext());
	    feedSearchCategoryRecycleViewAdapter = new FeedSearchCategoryRecycleViewAdapter(getContext());
	    feedCategoryRV.setLayoutManager(linearLayoutManager);
	    feedCategoryRV.setAdapter(feedSearchCategoryRecycleViewAdapter);
	    feedSearchCategoryRecycleViewAdapter.setOnItemClickListener(this);
	    feedSearchCategoryRecycleViewAdapter.setNewData(getFeedSearchCategoryData());
    }

    private List<FeedSearchCategoryItem> getFeedSearchCategoryData(){
        List<FeedSearchCategoryItem> feedSearchCategoryItems = new ArrayList<>();
        FeedSearchCategoryItem feedSearchCategoryItem = new FeedSearchCategoryItem();
        feedSearchCategoryItem.id = 1;
        feedSearchCategoryItem.name = "RECENT POSTS";
        feedSearchCategoryItem.icon = R.drawable.icon_recent_post;
        feedSearchCategoryItem.code = feedSearchCategoryItem.name.toLowerCase();
        feedSearchCategoryItem.description = "Wishlists that have been uploaded lately";
        feedSearchCategoryItems.add(feedSearchCategoryItem);

//        feedSearchCategoryItem = new FeedSearchCategoryItem();
//        feedSearchCategoryItem.id = 2;
//        feedSearchCategoryItem.name = "MOST LIKES";
//        feedSearchCategoryItem.icon = R.drawable.icon_most_likes;
//        feedSearchCategoryItem.code = feedSearchCategoryItem.name.toLowerCase();
//        feedSearchCategoryItem.description = "A lot of people likes these, dont you?";
//        feedSearchCategoryItems.add(feedSearchCategoryItem);

        feedSearchCategoryItem = new FeedSearchCategoryItem();
        feedSearchCategoryItem.id = 2;
        feedSearchCategoryItem.name = "HOT ITEMS";
        feedSearchCategoryItem.icon = R.drawable.icon_most_likes;
        feedSearchCategoryItem.code = feedSearchCategoryItem.name.toLowerCase();
        feedSearchCategoryItem.description = "A lot of people likes these, dont you?";
        feedSearchCategoryItems.add(feedSearchCategoryItem);

//        feedSearchCategoryItem = new FeedSearchCategoryItem();
//        feedSearchCategoryItem.id = 3;
//        feedSearchCategoryItem.name = "MOST REPOSTS";
//        feedSearchCategoryItem.icon = R.drawable.icon_most_repost;
//        feedSearchCategoryItem.code = feedSearchCategoryItem.name.toLowerCase();
//        feedSearchCategoryItem.description = "Most grabbed items, reposted on others accounts";
//        feedSearchCategoryItems.add(feedSearchCategoryItem);

        feedSearchCategoryItem = new FeedSearchCategoryItem();
        feedSearchCategoryItem.id = 3;
        feedSearchCategoryItem.name = "TRENDING";
        feedSearchCategoryItem.icon = R.drawable.icon_most_repost;
        feedSearchCategoryItem.code = feedSearchCategoryItem.name.toLowerCase();
        feedSearchCategoryItem.description = "Most grabbed items, reposted on others accounts";
        feedSearchCategoryItems.add(feedSearchCategoryItem);

        feedSearchCategoryItem = new FeedSearchCategoryItem();
        feedSearchCategoryItem.id = 4;
        feedSearchCategoryItem.name = "SHUFFLE";
        feedSearchCategoryItem.icon = R.drawable.icon_shuffle_post;
        feedSearchCategoryItem.code = feedSearchCategoryItem.name.toLowerCase();
        feedSearchCategoryItem.description = "Take a spin, check out what others like";
        feedSearchCategoryItems.add(feedSearchCategoryItem);

        feedSearchCategoryItem = new FeedSearchCategoryItem();
        feedSearchCategoryItem.id = 5;
        feedSearchCategoryItem.name = "MORE";
        feedSearchCategoryItem.icon = R.drawable.icon_more;
        feedSearchCategoryItem.code = feedSearchCategoryItem.name.toLowerCase();
        feedSearchCategoryItem.description = "Let's get down to specifics";
        feedSearchCategoryItems.add(feedSearchCategoryItem);

        return feedSearchCategoryItems;
    }

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow()
				.getAttributes().windowAnimations = R.style.DialogAnimation;
		return dialog;
	}

    @Override
    public void onItemClick(FeedSearchCategoryItem feedSearchCategoryItem) {
	    if (feedSearchCategoryItem.name.equalsIgnoreCase("more")){
	        SearchDialog.newInstance(this).show(getFragmentManager(), SearchDialog.TAG);
        }else {
            if (callback != null){
                callback.onItemSelect(feedSearchCategoryItem.name.toUpperCase(), false);
                dismiss();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.viewCON:
                dismiss();
                break;
        }
    }

    @Override
    public void searchTextChange(String keyword) {
        this.keyword = keyword;
        if (callback != null){
            callback.onItemSelect(keyword, true);
            dismiss();
        }
    }

    public interface Callback{
	    void onItemSelect(String keyword, boolean isOthers);
    }
}
