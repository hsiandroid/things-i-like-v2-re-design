package com.thingsilikeapp.android.dialog;

import android.os.Handler;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;

public class WalkThroughMiddleDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = WalkThroughMiddleDialog.class.getName();

    private Handler handler;
    private Runnable runnable;

	public Callback callback;

	@BindView(R.id.dialogCON)               View dialogCON;

	public static WalkThroughMiddleDialog newInstance(Callback callback) {
		WalkThroughMiddleDialog fragment = new WalkThroughMiddleDialog();
		fragment.callback = callback;
		return fragment;
	}
	@Override
	public int onLayoutSet() {
		return R.layout.dialog_walkthrough_middle;
	}

	@Override
	public void onViewReady() {
        dialogCON.setOnClickListener(this);

	}

    @Override
    public void onResume() {
        super.onResume();
        timer();
    }

    private void timer(){
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (callback != null){
                    callback.onMiddleDismiss(WalkThroughMiddleDialog.this);
                }
            }
        };
        handler.postDelayed(runnable, 5000);
    }

    @Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

    @Override
    public void onPause() {
        super.onPause();

        if (handler != null){
            handler.removeCallbacks(runnable);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dialogCON:
                if (callback != null){
                    callback.onMiddleDismiss(this);
                }
            break;
        }
    }

    public interface Callback{
		void onMiddleDismiss(WalkThroughMiddleDialog walkThroughMiddleDialog);
	}
}
