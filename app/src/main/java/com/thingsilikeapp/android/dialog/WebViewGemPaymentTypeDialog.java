package com.thingsilikeapp.android.dialog;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.Formatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import icepick.State;

public class WebViewGemPaymentTypeDialog extends BaseDialog implements View.OnClickListener,PopupMenu.OnMenuItemClickListener{
	public static final String TAG = WebViewGemPaymentTypeDialog.class.getName();

	private Callback callback;
	private JavaScriptInterface javaScriptInterface;

	@State	String url;
	@State String title;
	@State String newURL;

	@BindView(R.id.webView)			WebView webView;
	@BindView(R.id.titleBarTxt) 	TextView titleBarTxt;
	@BindView(R.id.otherOptionBTN) 	View otherOptionBTN;
	@BindView(R.id.progressBar) 	ProgressBar progressBar;
	@BindView(R.id.titleBarURLTxt) 	TextView titleBarURLTxt;
	@BindView(R.id.closeBTN) 		View closeBTN;

	public static WebViewGemPaymentTypeDialog newInstance(String title, String url, Callback callback) {
		WebViewGemPaymentTypeDialog webViewDialog = new WebViewGemPaymentTypeDialog();
		webViewDialog.title = title;
		webViewDialog.url = url;
		webViewDialog.callback = callback;
		return webViewDialog;
	}

	public static WebViewGemPaymentTypeDialog newInstance(String url) {
		WebViewGemPaymentTypeDialog fragment = new WebViewGemPaymentTypeDialog();
		fragment.url = url;
		return fragment;
	}
	@Override
	public int onLayoutSet() {
		return R.layout.dialog_web_view_gem_payment_type;
	}

	@Override
	public void onViewReady() {
        closeBTN.setOnClickListener(this);
		otherOptionBTN.setOnClickListener(this);

		if(title == null){
			title = getString(R.string.app_name);
		}

		progressBar.setMax(100);
		progressBar.setProgress(0);

		WebSettings webSettings = webView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(true);

		javaScriptInterface = new JavaScriptInterface(getContext());
		webView.addJavascriptInterface(javaScriptInterface, "JSInterface");

		webView.setWebChromeClient(new WebChromeClient(){
			@Override
			public void onProgressChanged(WebView view, int newProgress) {

				if(progressBar != null){
					if(newProgress < 100){
						progressBar.setVisibility(View.VISIBLE);
						progressBar.setProgress(newProgress);
					}else{
						progressBar.setVisibility(View.GONE);
						titleBarTxt.setText(view.getTitle());
						titleBarURLTxt.setText(Formatter.getDomainFromURL(view.getOriginalUrl()));
						newURL = view.getOriginalUrl();
					}
				}
				super.onProgressChanged(view, newProgress);
			}
		});

		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Map<String, String> headers = new HashMap<>();
				headers.put("Authorization", "Bearer" + UserData.getString(UserData.AUTHORIZATION));
				view.loadUrl(url,headers);
				Log.d(TAG, "shouldOverrideUrlLoading1: " + headers);
				return false;
			}
		});

        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer" + UserData.getString(UserData.AUTHORIZATION));
		webView.loadUrl(url, headers);
        Log.d(TAG, "shouldOverrideUrlLoading: " + headers);

		getDialog().setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				if (webView.canGoBack()) {
					webView.goBack();
				} else {
					dialog.dismiss();
				}
			}
		});

		getDialog().setOnKeyListener(new Dialog.OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface arg0, int keyCode,
								 KeyEvent event) {
				// TODO Auto-generated method stub
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					if (webView.canGoBack()) {
						webView.goBack();
					} else {
						getDialog().dismiss();
					}
				}
				return true;
			}
		});
	}

	private void otherOption(View v){
		PopupMenu popup = new PopupMenu(getActivity(), v);
		popup.getMenuInflater().inflate(R.menu.webview_other_option, popup.getMenu());
		popup.setOnMenuItemClickListener(this);
		popup.show();
	}

	public void copyToClipBoard(String label, String value){
		ClipboardManager clipboardManager = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
		ClipData clipData = ClipData.newPlainText(label, value);
		clipboardManager.setPrimaryClip(clipData);
		ToastMessage.show(getActivity(), label + " Copied to clipboard.", ToastMessage.Status.SUCCESS);
	}

	private void openBrowser(String link){
		Intent webIntent = new Intent(Intent.ACTION_VIEW,
				Uri.parse(link));
		try {
			startActivity(webIntent);
		} catch (ActivityNotFoundException ex) {
			startActivity(webIntent);
		}
	}


	@Override
	public void onDismiss(DialogInterface dialog) {
		if(webView != null){
			webView.stopLoading();
		}
		super.onDismiss(dialog);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.closeBTN:
				if(callback != null){
					callback.onGemPaymentSuccess();
					dismiss();
				}
				break;
			case R.id.otherOptionBTN:
				otherOption(v);
				break;
		}
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()){
			case R.id.copyBTN:
				copyToClipBoard("URL", newURL);
				break;
			case R.id.openBTN:
				openBrowser(newURL);
				break;
		}
		return false;
	}

	public interface Callback {
		void onGemPaymentSuccess();
	}

	public class JavaScriptInterface {
		Context context;

		public JavaScriptInterface(Context context) {
			this.context = context;
		}

		@JavascriptInterface
		public void closeWindow(){
			if(callback != null){
				callback.onGemPaymentSuccess();
				dismiss();
			}
		}

	}


}
