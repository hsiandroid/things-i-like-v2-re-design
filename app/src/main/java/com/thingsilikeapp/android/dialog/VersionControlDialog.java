package com.thingsilikeapp.android.dialog;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class VersionControlDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = VersionControlDialog.class.getName();

	private final int DOWNLOAD_UPDATE = 800;

	@State boolean isRequired;

	private ClickListener clickListener;

	public static VersionControlDialog newInstance(boolean isRequired, ClickListener clickListener) {
		VersionControlDialog versionControlDialog = new VersionControlDialog();
		versionControlDialog.isRequired = isRequired;
		versionControlDialog.clickListener = clickListener;
		return versionControlDialog;
	}

	@BindView(R.id.negativeBTN)		View negativeBTN;
	@BindView(R.id.positiveBTN)		View positiveBTN;

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_version_control;
	}

	@Override
	public void onViewReady() {
		negativeBTN.setOnClickListener(this);
		positiveBTN.setOnClickListener(this);
		if(!isRequired){
			negativeBTN.setEnabled(true);
		}else{
			negativeBTN.setEnabled(false);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.negativeBTN:
				if(clickListener != null){
					clickListener.later(this);
				}
				break;
			case R.id.positiveBTN:
				if(clickListener != null){
					clickListener.update(this);
				}
				break;
		}
	}

	public void openGooglePlayStore(){
		Uri uri = Uri.parse("market://details?id=" + getContext().getPackageName());
		Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
		goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
				Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
				Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
		try {
			startActivityForResult(goToMarket, DOWNLOAD_UPDATE);
		} catch (ActivityNotFoundException e) {
			startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getContext().getPackageName())), DOWNLOAD_UPDATE);
		}
	}

	public interface ClickListener {
		void later(VersionControlDialog dialogFragment);
		void update(VersionControlDialog dialogFragment);
	}
}
