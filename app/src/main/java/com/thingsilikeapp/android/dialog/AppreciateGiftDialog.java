package com.thingsilikeapp.android.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

import butterknife.BindView;
import icepick.State;

public class AppreciateGiftDialog extends BaseDialog implements View.OnClickListener{

	public static final String TAG = AppreciateGiftDialog.class.getName();

	private Callback callback;
	@State int wishListID;
	@State String title;

	public static AppreciateGiftDialog newInstance(String title, int wishListID, Callback callback) {
		AppreciateGiftDialog appreciateGiftDialog = new AppreciateGiftDialog();
		appreciateGiftDialog.title = title;
		appreciateGiftDialog.callback = callback;
		appreciateGiftDialog.wishListID = wishListID;
		return appreciateGiftDialog;
	}

	@BindView(R.id.dedicationET) 	EditText dedicationET;
	@BindView(R.id.confirmBTN) 		TextView confirmBTN;
	@BindView(R.id.titleTXT) 		TextView titleTXT;

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_appreciate_gift;
	}

	@Override
	public void onViewReady() {
		confirmBTN.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View v) {
		((RouteActivity)getContext()).disableMultipleClick();
		switch (v.getId()){
			case R.id.confirmBTN:
				if(callback != null){
					callback.onAccept(wishListID, dedicationET.getText().toString());
				}
				dismiss();
				break;
		}
	}

	public interface Callback{
		void onAccept(int wishListID, String dedication);
	}
}
