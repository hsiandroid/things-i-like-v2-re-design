package com.thingsilikeapp.android.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

import butterknife.BindView;
import icepick.State;

public class AdvanceGreetingDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = AdvanceGreetingDialog.class.getName();

	private Callback callback;
    private RouteActivity routeActivity;

	@BindView(R.id.headerTXT)		TextView headerTXT;
	@BindView(R.id.imageCIV)		ImageView imageCIV;
	@BindView(R.id.commonNameTXT)	TextView commonNameTXT;
	@BindView(R.id.messageTXT)	    TextView messageTXT;
	@BindView(R.id.noteTXT)	    	TextView noteTXT;
	@BindView(R.id.sendBTN)	        View sendBTN;
	@BindView(R.id.cancelBTN)	    View cancelBTN;

	@State String raw;

	public static AdvanceGreetingDialog newInstance(UserItem userItem, Callback callback) {
		AdvanceGreetingDialog fragment = new AdvanceGreetingDialog();
		fragment.raw = new Gson().toJson(userItem);
		fragment.callback = callback;
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_advance_greeting;
	}

	@Override
	public void onViewReady() {
        routeActivity = (RouteActivity) getContext();
        sendBTN.setOnClickListener(this);
        cancelBTN.setOnClickListener(this);
        commonNameTXT.setOnClickListener(this);
        imageCIV.setOnClickListener(this);
		displayData(new Gson().fromJson(raw, UserItem.class));
	}

	private void displayData(UserItem userItem){
		headerTXT.setText("GREET " + userItem.name + "?");
		commonNameTXT.setText(userItem.common_name);
        messageTXT.setText("Today is not "+ userItem.pronoun("his", "her") +" birthday yet...");
		noteTXT.setText("Your greeting will be delivered on "+ userItem.pronoun("his", "her") + " actual birthday.");

        sendBTN.setTag(userItem);
        commonNameTXT.setTag(userItem.id);
        imageCIV.setTag(R.id.imageCIV, userItem.id);

		Glide.with(getContext())
				.load(userItem.getAvatar())
				.apply(new RequestOptions()

						.placeholder(R.drawable.placeholder_avatar)
				.error(R.drawable.placeholder_avatar)
				.fitCenter()
				.dontAnimate()
				.diskCacheStrategy(DiskCacheStrategy.ALL)
				.skipMemoryCache(true))
				.into(imageCIV);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.sendBTN:
                if(callback != null){
                    callback.advanceGreeting((UserItem) view.getTag());
                }
                dismiss();
                break;
            case R.id.cancelBTN:
                dismiss();
                break;
            case R.id.commonNameTXT:
                routeActivity.startProfileActivity((int) view.getTag());
                break;
            case R.id.imageCIV:
                routeActivity.startProfileActivity((int) view.getTag(R.id.imageCIV));
                break;
        }
    }

    public interface Callback{
		void advanceGreeting(UserItem userItem);
	}
}
