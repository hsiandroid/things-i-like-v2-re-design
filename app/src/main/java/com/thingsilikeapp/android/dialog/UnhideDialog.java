package com.thingsilikeapp.android.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.HidePostItem;
import com.thingsilikeapp.data.model.ReportPostItem;
import com.thingsilikeapp.server.HidePost;
import com.thingsilikeapp.server.ReportPost;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class UnhideDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = UnhideDialog.class.getName();

	@BindView(R.id.confirmBTN) 	TextView confirmBTN;
	@BindView(R.id.cancelBTN) 	TextView cancelBTN;
	@BindView(R.id.descTXT) 	TextView descTXT;


	private HidePostItem hidePostItem;
	private Callback callback;

	public static UnhideDialog newInstance(HidePostItem hidePostItem, Callback callback) {
		UnhideDialog logoutDialog = new UnhideDialog();
		logoutDialog.hidePostItem = hidePostItem;
		logoutDialog.callback = callback;
		return logoutDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_setting;
	}

	@Override
	public void onViewReady() {
		descTXT.setText("Are you sure you want to unhide this post?");
		confirmBTN.setOnClickListener(this);
		cancelBTN.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
			EventBus.getDefault().register(this);
			setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.confirmBTN:
                HidePost.getDefault().getUnhidePost(getContext(), hidePostItem.wishlistId);
				Analytics.trackEvent("dialog_unhideDialog_confirmBTN");
				break;
			case R.id.cancelBTN:
				dismiss();
				Analytics.trackEvent("dialog_unhideDialog_cancelBTN");
				break;
		}
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Subscribe
	public void onResponse(HidePost.UnhideResponse responseData) {
		SingleTransformer<HidePostItem> singleTransformer = responseData.getData(SingleTransformer.class);
		if(singleTransformer.status){
			ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
			if (callback!=null){
				callback.onSuccess();
				dismiss();
			}
		}else{
			ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
		}
	}

	public interface Callback{
		void onSuccess();
	}

}
