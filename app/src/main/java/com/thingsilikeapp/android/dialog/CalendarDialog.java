package com.thingsilikeapp.android.dialog;

import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import icepick.State;

public class CalendarDialog extends BaseDialog implements
		View.OnClickListener {
	public static final String TAG = CalendarDialog.class.getName();

	private DateTimePickerListener dateTimePickerListener;
    private Calendar calendar;

	@BindView(R.id.positiveBTN)	    TextView positiveBTN;
	@BindView(R.id.negativeBTN)	    TextView negativeBTN;
	@BindView(R.id.birthdayDP)		DatePicker birthdayDP;

	@State String birthday;
	@State String format = "MM/dd/yyyy";

	public static CalendarDialog newInstance(DateTimePickerListener dateTimePickerListener, String birthday) {
		CalendarDialog calendarDialog = new CalendarDialog();
		calendarDialog.dateTimePickerListener = dateTimePickerListener;
		calendarDialog.birthday = birthday;
		return calendarDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_calendar;
	}

	@Override
	public void onViewReady() {
        positiveBTN.setOnClickListener(this);
        negativeBTN.setOnClickListener(this);

		SimpleDateFormat dayFormat = new SimpleDateFormat(format);
		calendar = Calendar.getInstance();
		try {
			calendar.setTime(dayFormat.parse(birthday));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int year = calendar.get(Calendar.YEAR) - 12;
		int month = calendar.get(Calendar.MONTH);

        birthdayDP.init(year, month, day, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(year, monthOfYear, dayOfMonth);
            }

        });
        birthdayDP.setMaxDate(new Date().getTime());
	}

	@Override
	public void onStart() {
		super.onStart();
        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.positiveBTN:
				save();
				break;
			case R.id.negativeBTN:
				dismiss();
				break;
		}
	}

	private void save(){

        birthdayDP.clearFocus();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                callback();
            }
        }, 100);
	}

	private void callback(){
        if(dateTimePickerListener != null){
            if (calendar != null) {
                dateTimePickerListener.forDisplay(new SimpleDateFormat(format).format(calendar.getTime()));
            }
        }
        dismiss();
    }

	public interface DateTimePickerListener{
		void forDisplay(String date);
	}
}
