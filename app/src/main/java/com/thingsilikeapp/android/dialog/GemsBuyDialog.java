package com.thingsilikeapp.android.dialog;

import android.app.ProgressDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.BirthdayItem;
import com.thingsilikeapp.data.model.BuyGemItem;
import com.thingsilikeapp.data.model.EncashHistoryItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.server.request.wallet.ValidateBuyRequest;
import com.thingsilikeapp.server.transformer.wallet.ValidateBuyTransformer;
import com.thingsilikeapp.vendor.android.java.GemHelper;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.GemHelpers;
import com.thingsilikeapp.vendor.android.java.NumberFormatter;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import butterknife.BindView;
import icepick.State;

public class GemsBuyDialog extends BaseDialog implements View.OnClickListener,
		GemsSendConfirmationDialog.ConfirmationCallback,
        GemsBuyOptionDialog.Callback{

	public static final String TAG = GemsBuyDialog.class.getName();

	private Callback callback;

	@BindView(R.id.amountET)                    EditText amountET;
	@BindView(R.id.continueBTN)                 TextView continueBTN;
	@BindView(R.id.exitBTN)                 	View exitBTN;
	@BindView(R.id.gemTXT)                      TextView gemTXT;
	@BindView(R.id.rateTXT)                     TextView rateTXT;
	@BindView(R.id.bonusTXT)                    TextView bonusTXT;

	@State String bearer;
	@State String currency;
	@State double rate;
	@State double bonus;
	@State double totalgem;
	@State double totalamount;
	@State double bonusRate;

	public static GemsBuyDialog newInstance(Callback callback) {
		GemsBuyDialog gemsSendConfirmationDialog = new GemsBuyDialog();
		gemsSendConfirmationDialog.callback = callback;
		return gemsSendConfirmationDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_gems_buy;
	}

	@Override
	public void onViewReady() {
        continueBTN.setOnClickListener(this);
		exitBTN.setOnClickListener(this);

		Wallet.getDefault().buyGem(getContext());

		rate = UserData.getUserItem().info.data.rate;
        currency = UserData.getUserItem().info.data.currency;

        rateTXT.setText(String.valueOf(GemHelper.formatGem(String.valueOf(rate * 10)) + " " + currency).toUpperCase() );
        gemTXT.setText("0 " + UserData.getUserItem().info.data.currency);

		amountET.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void afterTextChanged(Editable editable) {
					double amount = NumberFormatter.parseDouble(amountET.getText().toString()) * rate;
					gemTXT.setText(String.valueOf(GemHelper.formatGem(String.valueOf(amount)) + " " + currency).toUpperCase());
					DecimalFormat precision = new DecimalFormat("0.00");
					if (!amountET.getText().toString().equalsIgnoreCase("")) {
						Double gemAmount = Double.valueOf(amountET.getText().toString());
						bonus = gemAmount * bonusRate;
						BigDecimal gem = new BigDecimal(bonus);
						bonusTXT.setText(precision.format(gem));
						totalgem = NumberFormatter.parseDouble(amountET.getText().toString()) + bonus;
					}
				Log.e("TOTALGEM", "total gem: " + totalgem);
			}
		});
	}

    @Override
	public void onStart() {
		super.onStart();
        EventBus.getDefault().register(this);
		setDialogMatchParent();
	}

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.continueBTN:
                attemptSend();
				break;
			case R.id.exitBTN:
				dismiss();
				break;
		}
	}


	@Subscribe
	public void onResponse(Wallet.BuyGemResponse tranformerResponse) {
		SingleTransformer<BuyGemItem> transformer = tranformerResponse.getData(SingleTransformer.class);
		if (transformer.status) {
			bonusRate = transformer.data.bonusGemRate;
		}
	}


    private void attemptSend(){
	    if(StringFormatter.isEmpty(amountET.getText().toString())){
            ToastMessage.show(getContext(), "Incomplete or invalid input!", ToastMessage.Status.FAILED);
        }else{
            new ValidateBuyRequest(getContext())
                    .setDeviceRegID(getBaseActivity().getDeviceRegID())
                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                    .addParameters(Keys.server.key.AMOUNT, amountET.getText().toString())
                    .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                    .execute();
        }
    }

	@Override
	public void confirmationCallback() {
        attemptSend();
	}

    @Subscribe
    public void onResponse(ValidateBuyRequest.ServerResponse responseData) {
        ValidateBuyTransformer validatebuy = responseData.getData(ValidateBuyTransformer.class);
        if (validatebuy.status) {
			DecimalFormat precision = new DecimalFormat("0.00");
			Log.e("TOTALGEM", "total gem: " + totalgem);
            GemsBuyOptionDialog.newInstance(validatebuy.web_checkout, amountET.getText().toString(), precision.format(totalgem), gemTXT.getText().toString(),  this).show(getFragmentManager(), GemsBuyOptionDialog.TAG);
        }else {
        	ToastMessage.show(getContext(), validatebuy.msg, ToastMessage.Status.FAILED);
		}
    }

    @Override
    public void onGemOptionSuccess() {
        if(callback != null){
            callback.onGemBuySuccess();
            dismiss();
        }
    }

    public interface Callback {
		void onGemBuySuccess();
	}
}
