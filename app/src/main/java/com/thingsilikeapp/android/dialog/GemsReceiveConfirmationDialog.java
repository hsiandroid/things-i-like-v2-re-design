package com.thingsilikeapp.android.dialog;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;

public class GemsReceiveConfirmationDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = GemsReceiveConfirmationDialog.class.getName();

	@BindView(R.id.avatarCIV) 			         ImageView avatarCIV;
	@BindView(R.id.nameTXT)                      TextView nameTXT;
	@BindView(R.id.receivedCountTXT)             TextView receivedCountTXT;
	@BindView(R.id.continueBTN)                  TextView continueBTN;

	public static GemsReceiveConfirmationDialog newInstance() {
		GemsReceiveConfirmationDialog gemsReceiveConfirmationDialog = new GemsReceiveConfirmationDialog();
		return gemsReceiveConfirmationDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_gems_receive_confirmation;
	}

	@Override
	public void onViewReady() {

	    continueBTN.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.continueBTN:
                dismiss();
                break;
        }
    }
}
