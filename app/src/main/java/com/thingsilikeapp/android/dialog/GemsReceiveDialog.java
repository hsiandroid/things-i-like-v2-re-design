package com.thingsilikeapp.android.dialog;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.config.App;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.DownloadFileManager;
import com.thingsilikeapp.vendor.android.java.QRCode;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import icepick.State;

public class GemsReceiveDialog extends BaseDialog implements View.OnClickListener, DownloadFileManager.DownloadCallback {
	public static final String TAG = GemsReceiveDialog.class.getName();

    private DownloadFileManager downloadFileManager;
    private Callback callback;

    private int seconds=0;

	@BindView(R.id.barcodeIV)           ImageView barcodeIV;
	@BindView(R.id.avatarCIV)           ImageView avatarCIV;
	@BindView(R.id.exitBTN)             ImageView exitBTN;
	@BindView(R.id.nameTXT)             TextView nameTXT;
	@BindView(R.id.copyBTN)             View copyBTN;
	@BindView(R.id.printBTN)            View printBTN;
	@BindView(R.id.emailBTN)            View emailBTN;

	@State String code = "";
	@State String url = "";
	@State String username = "";
    @State long downloadReferenceId;

	public static GemsReceiveDialog newInstance(Callback callback) {
		GemsReceiveDialog gemsReceiveConfirmationDialog = new GemsReceiveDialog();
		gemsReceiveConfirmationDialog.callback = callback;
		return gemsReceiveConfirmationDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_gems_receive;
	}

	@Override
	public void onViewReady() {
        exitBTN.setOnClickListener(this);

        copyBTN.setOnClickListener(this);
        printBTN.setOnClickListener(this);
        emailBTN.setOnClickListener(this);
        code = encrypt(UserData.getUserId() + "");
        username = UserData.getUserItem().username;
        setUpDownloadManager();
        displayData();
	}

	private void setUpDownloadManager(){
        seconds = getTime();
        downloadFileManager = DownloadFileManager.newInstance(getContext(), UserData.getUserItem().username.toLowerCase()+ "-" +  seconds +  ".pdf", this);

    }

	private int getTime(){
        Calendar c=Calendar.getInstance();
        c.setTime(new Date());
        return c.get(Calendar.MILLISECOND);
    }

    private String encrypt(String s) {
        byte[] data = new byte[0];
        try {
            data = s.getBytes(StandardCharsets.UTF_8);
        } finally {
            return Base64.encodeToString(data, Base64.DEFAULT);
        }
    }

	private void displayData(){
	    Glide.with(getContext())
                .load(UserData.getUserItem().getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(avatarCIV);

	    nameTXT.setText(UserData.getUserItem().name);
        barcodeIV.setImageBitmap(QRCode.encodeToQrCode(300, StringFormatter.isEmpty(code) ? "-" : code.trim()));
    }

    private void copyToClipBoard(){
        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", username);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getContext(), "Copied to Clipboard", Toast.LENGTH_SHORT).show();
    }

    private void share(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "LYKA");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, App.SHARE_QR
                .replace("{name}", UserData.getUserItem().common_name)
                .replace("{username}", UserData.getUserItem().username) );
        startActivityForResult(Intent.createChooser(sharingIntent, "Share"), 212);
    }

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.exitBTN:
                exit();
                break;
            case R.id.continueBTN:
                dismiss();
                break;
            case R.id.copyBTN:
                copyToClipBoard();
                break;
            case R.id.printBTN:
                downloadFile();
                break;
            case R.id.emailBTN:
                share();
                break;
        }
    }

    private void exit(){
	    if(callback != null){
            callback.onReceiveCallback();
        }
        dismiss();
    }

    private void downloadFile(){
        String path = getString(R.string.base_url) + "/api/v2/wallet/print.json" + "?uid=" + code;
        downloadFileManager.downloadWithPermissionGranted(path);
    }

    @Override
    public void onFileDownloadCompleted() {
        ToastMessage.show(getContext(),"Download Completed", ToastMessage.Status.SUCCESS);
        setUpDownloadManager();
    }

    public interface Callback {
        void onReceiveCallback();
    }
}
