package com.thingsilikeapp.android.dialog;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.LandingActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.BankCardItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.server.request.auth.SignUpRequest;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class DeleteCardDialog extends BaseDialog implements View.OnClickListener{

    public static final String TAG = DeleteCardDialog.class.getName();

    private BankCardItem card;
    private Callback callback;

    @BindView(R.id.mainBackButtonIV)                      ImageView mainBackButtonIV;
    @BindView(R.id.deleteCardBTN)                         TextView deleteCardBTN;
    @BindView(R.id.expirationDateTXT)                     TextView expirationDateTXT;
    @BindView(R.id.lastDigitTXT)                          TextView lastDigitTXT;
    @BindView(R.id.cardIV)                                ImageView cardIV;

    public static DeleteCardDialog newInstance(BankCardItem card, Callback callback){
        DeleteCardDialog signUpDialog = new DeleteCardDialog();
        signUpDialog.card = card;
        signUpDialog.callback = callback;
        return signUpDialog;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.dialog_delete_card;
    }

    @Override
    public void onViewReady() {
        switch (card.cardType){
            case "visa":
                cardIV.setImageResource(R.drawable.icon_card_visa);
                break;
            case "diners":
                cardIV.setImageResource(R.drawable.icon_card_diners_club);
                break;
            case "discover":
                cardIV.setImageResource(R.drawable.icon_card_discover);
                break;
            case "jcb":
                cardIV.setImageResource(R.drawable.icon_card_jcb);
                break;
            case "mastercard":
                cardIV.setImageResource(R.drawable.icon_card_master_card);
                 break;
            case "amex":
                cardIV.setImageResource(R.drawable.icon_card_amex);
                break;
        }
        lastDigitTXT.setText(card.cardNumber);
        expirationDateTXT.setText(card.cardExpiryMonth + "/" + card.cardExpiryYear);
        mainBackButtonIV.setOnClickListener(this);
        deleteCardBTN.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.mainBackButtonIV:
                dismiss();
                break;
            case R.id.deleteCardBTN:
                Wallet.getDefault().removeCard(getContext(), card.id);
                break;
        }
    }

    @Subscribe
    public void onResponse(Wallet.AddCardResponse tranformerResponse) {
        SingleTransformer<BankCardItem> transformer = tranformerResponse.getData(SingleTransformer.class);
        if (transformer.status) {
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.SUCCESS);
            if (callback != null){
                callback.onSuccess();
                dismiss();
            }
        }else{
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setDialogMatchParent();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public interface Callback{
        void onSuccess();
    }
}
