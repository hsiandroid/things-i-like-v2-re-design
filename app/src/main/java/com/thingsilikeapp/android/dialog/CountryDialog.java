package com.thingsilikeapp.android.dialog;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.CountryAdapter;
import com.thingsilikeapp.data.preference.CountryData;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class CountryDialog extends BaseDialog implements CountryAdapter.ClickListener, View.OnClickListener{
	public static final String TAG = CountryDialog.class.getName();

	private CountryAdapter countryAdapter;

	private CountryPickerListener countryPickerListener;

	@BindView(R.id.searchET)				EditText searchET;
	@BindView(R.id.countryLV)				ListView countryLV;
	@BindView(R.id.mainBackButtonIV)		View mainBackButtonIV;

	@State int requestCode;

	public static CountryDialog newInstance(CountryPickerListener countryPickerListener, int requestCode) {
		CountryDialog countryDialog = new CountryDialog();
		countryDialog.countryPickerListener = countryPickerListener;
		countryDialog.requestCode = requestCode;
		return countryDialog;
	}

	public static CountryDialog newInstance(CountryPickerListener countryPickerListener) {
		CountryDialog countryDialog = new CountryDialog();
		countryDialog.countryPickerListener = countryPickerListener;
		countryDialog.requestCode = 0;
		return countryDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_country;
	}

	@Override
	public void onViewReady() {
		mainBackButtonIV.setOnClickListener(this);
		countryAdapter = new CountryAdapter(getContext());
		countryAdapter.setOnItemClickListener(this);
		countryLV.setAdapter(countryAdapter);

		searchET.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable charSequence) {
				countryAdapter.getFilter().filter(charSequence);
			}
		});

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
		super.onStart();
	}

	@Override
	public void onItemClick(CountryData.Country country) {
		if(countryPickerListener != null){
			countryPickerListener.onSelectCountry(country, requestCode);
		}
		dismiss();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.mainBackButtonIV:
				dismiss();
				break;
		}
	}

	public interface CountryPickerListener{
		void onSelectCountry(CountryData.Country country, int requestCode);
	}
}
