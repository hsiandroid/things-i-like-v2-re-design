package com.thingsilikeapp.android.dialog;

import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MessageActivity;
import com.thingsilikeapp.android.adapter.AddMemberRecyclerViewAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.ChatUserModel;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.Chat;
import com.thingsilikeapp.server.request.social.FollowersRequest;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class GroupChatAddMemberDialog extends BaseDialog implements View.OnClickListener, AddMemberRecyclerViewAdapter.ClickListener {
	public static final String TAG = GroupChatAddMemberDialog.class.getName();

	public static GroupChatAddMemberDialog newInstance(int chat_id, Callback callback) {
		GroupChatAddMemberDialog mentorshipSuccessDialog = new GroupChatAddMemberDialog();
		mentorshipSuccessDialog.chat_id = chat_id;
		mentorshipSuccessDialog.callback = callback;
		return mentorshipSuccessDialog;
	}

	private AddMemberRecyclerViewAdapter addMemberRecyclerViewAdapter;
	private LinearLayoutManager menteesLLM;
	private MessageActivity gcActivity;
	private Callback callback;

	private FollowersRequest followersRequest;

	@State int userID = UserData.getUserId();
	@BindView(R.id.exitBTN) View exitBTN;
	@BindView(R.id.menteesRV) RecyclerView menteesRV;
	@BindView(R.id.gcCreateBTN) TextView gcCreateBTN;
	@BindView(R.id.searchET) EditText searchET;
	@BindView(R.id.searchBTN) ImageView searchBTN;
	@BindView(R.id.progressPB)
	ProgressBar progressBar;

	@State String keyWord;
	private int chat_id;

	@Override
	public int onLayoutSet() {return R.layout.dialog_add_group_chat_member;}

	@Override
	public void onViewReady() {
        exitBTN.setOnClickListener(this);
        setSearch();
        gcActivity = (MessageActivity) getContext();
        gcCreateBTN.setOnClickListener(this);
		gcCreateBTN.setClickable(false);
		gcCreateBTN.setBackgroundResource(R.color.gray);
		searchBTN.setOnClickListener(this);
		searchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
				search();
					return true;
				}
				return false;
			}
		});
		initSuggestionAPI();
	}

	private void setSearch(){
		menteesLLM = new LinearLayoutManager(getContext());
		addMemberRecyclerViewAdapter = new AddMemberRecyclerViewAdapter(getContext());
		menteesRV.setLayoutManager(menteesLLM);
		menteesRV.setAdapter(addMemberRecyclerViewAdapter);
		menteesRV.setNestedScrollingEnabled(false);
		addMemberRecyclerViewAdapter.setOnItemClickListener(this);
	}

	@Subscribe
	public void onResponse(FollowersRequest.ServerResponse responseData) {
		SuggestionTransformer suggestionTransformer = responseData.getData(SuggestionTransformer.class);
		if(suggestionTransformer.status){
			if(responseData.isNext()){
				addMemberRecyclerViewAdapter.addNewData(suggestionTransformer.data);
			}else{
				addMemberRecyclerViewAdapter.setNewData(suggestionTransformer.data);
			}
			progressBar.setVisibility(View.GONE);
		}
	}


	@Subscribe
	public void onResponse(Chat.SingleUserResponse responseData) {
		SingleTransformer<ChatUserModel> singleTransformer = responseData.getData(SingleTransformer.class);
		if(singleTransformer.status){
			ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
			if(callback != null){
				callback.onSuccess();
				dismiss();
			}
		}else{
			ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);

		}
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
		setDialogMatchParent();
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.exitBTN:
                dismiss();
                break;
			case R.id.gcCreateBTN:
				Chat.getDefault().addParticipant(getContext(), chat_id, addMemberRecyclerViewAdapter.getSelectedItems());
				break;
			case R.id.searchBTN:
				search();
				break;
        }
    }

    public void search(){
		addMemberRecyclerViewAdapter.reset();
		followersRequest
				.showSwipeRefreshLayout(true)
				.clearParameters()
				.addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
				.addParameters(Keys.server.key.USER_ID, userID)
				.addParameters(Keys.server.key.KEYWORD, searchET.getText().toString())
				.first();
	}

	private void initSuggestionAPI(){
		addMemberRecyclerViewAdapter.reset();
		followersRequest = new FollowersRequest(getContext());
		followersRequest
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
				.addParameters(Keys.server.key.USER_ID, userID)
				.setPerPage(20)
		.first();
	}

	@Override
	public void onMenteeClick(UserItem userModel) {
	}

	@Override
	public void onChecked(int position) {
		addMemberRecyclerViewAdapter.setSelected(position);
		Log.e("SELECTED", addMemberRecyclerViewAdapter.getSelectedItems());
		gcCreateBTN.setClickable(addMemberRecyclerViewAdapter.getSelectedItemSize() > 0);
		gcCreateBTN.setBackgroundColor(addMemberRecyclerViewAdapter.getSelectedItemSize() > 0 ? ActivityCompat.getColor(getContext(), R.color.colorPrimary) : ActivityCompat.getColor(getContext(), R.color.gray));
	}

	public interface Callback {
		void onSuccess();
	}
}
