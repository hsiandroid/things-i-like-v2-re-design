package com.thingsilikeapp.android.dialog.defaultdialog;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class ConfirmationBakDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = ConfirmationBakDialog.class.getName();

    @State int icon;
    @State String title;
    @State String description;
    @State String note;
    @State String positiveButton;
    @State String negativeButton;
    @State boolean showNoteLabel = true;

    @BindView(R.id.iconIV)                  ImageView iconIV;
    @BindView(R.id.descriptionTXT)          TextView descriptionTXT;
    @BindView(R.id.noteTXT)          		TextView noteTXT;
    @BindView(R.id.positiveBTN)             TextView positiveBTN;
    @BindView(R.id.negativeBTN)             TextView negativeBTN;

    private View.OnClickListener positiveClickListener;
    private View.OnClickListener negativeClickListener;

	public static ConfirmationBakDialog Builder() {
		ConfirmationBakDialog confirmationDialog = new ConfirmationBakDialog();
		return confirmationDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_confirmation_bak;
	}

	@Override
	public void onViewReady() {
        if(icon != 0){
            iconIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), icon));
        }

        if(description != null){
            descriptionTXT.setText(description);
        }

        if ((noteTXT != null)){
			String fullNote = showNoteLabel ? ("Note: " + note) : note;
			noteTXT.setText(fullNote);
		}

        if(positiveBTN != null){
            positiveBTN.setText(positiveButton);
        }
        if(negativeBTN != null){
            negativeBTN.setText(negativeButton);
        }

        if(positiveClickListener != null){
            positiveBTN.setOnClickListener(positiveClickListener);
        }
        if(negativeClickListener != null){
            negativeBTN.setOnClickListener(negativeClickListener);
        }else{
			negativeBTN.setOnClickListener(this);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	public ConfirmationBakDialog setIcon(int icon){
		this.icon = icon;
		return this;
	}

	public ConfirmationBakDialog setTitle(String title){
        this.title = title;
		return this;
	}

	public ConfirmationBakDialog setDescription(String description){
        this.description = description;
		return this;
	}

	public ConfirmationBakDialog setNote(String note){
        this.note = note;
		return this;
	}

	public ConfirmationBakDialog showNoteLabel(boolean showNoteLabel){
        this.showNoteLabel = showNoteLabel;
		return this;
	}

	public ConfirmationBakDialog setPositiveButtonText(String positiveButton){
        this.positiveButton = positiveButton;
		return this;
	}

	public ConfirmationBakDialog setNegativeButtonText(String negativeButton){
        this.negativeButton = negativeButton;
		return this;
	}

	public ConfirmationBakDialog setPositiveButtonClickListener(View.OnClickListener positiveClickListener){
        this.positiveClickListener = positiveClickListener;
		return this;
	}

	public ConfirmationBakDialog setNegativeButtonClickListener(View.OnClickListener negativeClickListener){
        this.negativeClickListener = negativeClickListener;
		return this;
	}

	public void build(FragmentManager manager){
        show(manager, TAG);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.positiveBTN:

				break;
			case R.id.negativeBTN:
				dismiss();
				break;
		}
	}
}


