package com.thingsilikeapp.android.dialog;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.server.request.auth.ResetPasswordRequest;
import com.thingsilikeapp.server.request.auth.VerificationCodeRequest;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class ResetPasscodeDialog extends BaseDialog implements View.OnClickListener{

    public static final String TAG = ResetPasscodeDialog.class.getName();



    @BindView(R.id.emailET)                     EditText emailET;
    @BindView(R.id.phoneET)                     EditText phoneET;
    @BindView(R.id.resetPasscodeBTN)            TextView resetPasscodeBTN;

    @State String email;

    private ForgotPasswordDialog forgotPasswordDialog;

    public static ResetPasscodeDialog newInstance(String email, ForgotPasswordDialog forgotPasswordDialog) {
        ResetPasscodeDialog resetPasswordDialog = new ResetPasscodeDialog();
        resetPasswordDialog.forgotPasswordDialog = forgotPasswordDialog;
        resetPasswordDialog.email = email;
        return resetPasswordDialog;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.dialog_reset_password;
    }

    @Override
    public void onViewReady() {

        emailET.setText(email);
        resetPasscodeBTN.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.confirmNewPasswordTV:
                attemptRequestVerification();
                break;
            case R.id.resendVerificationCodeTV:
                attemptResendRequestVerification();
                dismiss();
                break;
            case R.id.closeBTN:
                dismiss();
                break;
        }
    }

    private void attemptRequestVerification(){
        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest(getContext());
        resetPasswordRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Resetting passcode...", false, false))
                .addParameters(Keys.server.key.EMAIL, emailET.getText().toString())
//                .addParameters(Keys.server.key.VALIDATION_TOKEN, verificationCodeET.getText().toString())
//                .addParameters(Keys.server.key.PASSWORD, passwordET.getText().toString())
//                .addParameters(Keys.server.key.PASSWORD_CONFIRMATION, confirmPasswordET.getText().toString())
                .execute();
    }

    private void attemptResendRequestVerification(){
        VerificationCodeRequest verificationCodeRequest = new VerificationCodeRequest(getContext());
        verificationCodeRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Resending verification code...", false, false))
                .addParameters(Keys.server.key.EMAIL, emailET.getText().toString())
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        setDialogMatchParent();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(VerificationCodeRequest.ServerResponse responseData) {
        BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(getActivity(), baseTransformer.msg,ToastMessage.Status.SUCCESS);
            forgotPasswordDialog.dismiss();
        }
    }

    @Subscribe
    public void onResponse(ResetPasswordRequest.ServerResponse responseData) {
        BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            dismiss();
            forgotPasswordDialog.dismiss();
        }else {
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}
