package com.thingsilikeapp.android.dialog;

import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.model.WishListTransactionItem;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DecimalFormat;

import butterknife.BindView;
import icepick.State;

public class ExchangeToCashDialog extends BaseDialog implements  View.OnClickListener {
	public static final String TAG = ExchangeToCashDialog.class.getName();

    @BindView(R.id.continueBTN)             TextView continueBTN;
    @BindView(R.id.nameTXT)             	TextView nameTXT;
    @BindView(R.id.titleTXT)             	TextView titleTXT;
    @BindView(R.id.refTXT)             		TextView refTXT;
    @BindView(R.id.rateTXT)            		TextView rateTXT;
    @BindView(R.id.gemTXT)             		TextView gemTXT;
    @BindView(R.id.conversionTXT)           TextView conversionTXT;
    @BindView(R.id.serviceFeeTXT)           TextView serviceFeeTXT;
    @BindView(R.id.receivingAmountTXT)      TextView receivingAmountTXT;
    @BindView(R.id.exitBTN)    				ImageView extiBTN;

	@State String currency;
	@State String amount;
	@State String totalAmount;
	@State String serviceFee;
	@State String conversion;
	@State String rate;
	private BankItem bankItem;

	public static ExchangeToCashDialog Builder(BankItem bankItem, String currency, String amount, String totalAmount, String serviceFee, String rate, String conversion) {
		ExchangeToCashDialog infoDialog = new ExchangeToCashDialog();
		infoDialog.currency = currency;
		infoDialog.amount = amount;
		infoDialog.totalAmount = totalAmount;
		infoDialog.bankItem = bankItem;
		infoDialog.serviceFee = serviceFee;
		infoDialog.rate = rate;
		infoDialog.conversion = conversion;
		return infoDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_exchange_cash;
	}

	@Override
	public void onViewReady() {
		DecimalFormat precision = new DecimalFormat("0.00");
		Double totalServiceFee = Double.valueOf((Integer.parseInt(amount) / 100) * Double.parseDouble(serviceFee)) * Double.parseDouble(conversion);
		Double rateX10 = Double.parseDouble(conversion) *10;
       continueBTN.setOnClickListener(this);
       extiBTN.setOnClickListener(this);
       titleTXT.setText(bankItem.bankName);
       refTXT.setText(bankItem.accountNumber);
       gemTXT.setText(amount);
       rateTXT.setText(currency + " " + precision.format(rateX10));
       conversionTXT.setText(currency  + " " + precision.format(Double.parseDouble(rate)));
       nameTXT.setText(bankItem.accountName);
       serviceFeeTXT.setText( currency  + " " + precision.format(totalServiceFee));
       receivingAmountTXT.setText(currency + " " + precision.format(Double.parseDouble(totalAmount)));

		Log.e("RATE", "rate: " + rate);
		Log.e("CONVERSION", "conversion: " + conversion);

	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
//        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		setDialogMatchParent();
	}

	@Subscribe
	public void onResponse(Wallet.RequestResponse responseData) {
		BaseTransformer transformer = responseData.getData(BaseTransformer.class);
		if(transformer.status){
			ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.SUCCESS);
			((RouteActivity) getContext()).startRewardsActivity("treasury");
		}else{
			ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.FAILED);
		}
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}


	public void build(FragmentManager manager){
        show(manager, TAG);
	}

	@Override
	public void onClick(View view) {
		switch(view.getId()){
			case R.id.continueBTN:
				Wallet.getDefault().request(getContext(), bankItem.id, amount, currency);
				break;
			case R.id.exitBTN:
				dismiss();
				break;
		}
	}
}


