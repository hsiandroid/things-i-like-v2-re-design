package com.thingsilikeapp.android.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.AddressBookItem;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class AddressInfoDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = AddressInfoDialog.class.getName();

	@BindView(R.id.labelTXT)			TextView labelTXT;
	@BindView(R.id.nameTXT)				TextView nameTXT;
	@BindView(R.id.streetAddressTXT)	TextView streetAddressTXT;
	@BindView(R.id.cityTXT)				TextView cityTXT;
	@BindView(R.id.stateTXT)			TextView stateTXT;
	@BindView(R.id.countryTXT)			TextView countryTXT;
	@BindView(R.id.zipTXT)				TextView zipTXT;
	@BindView(R.id.contactTXT)			TextView contactTXT;
	@BindView(R.id.cancelBTN)			TextView cancelBTN;

	@State String addressBookRaw;

	public static AddressInfoDialog newInstance(AddressBookItem addressBookItem) {
		AddressInfoDialog addressInfoDialog = new AddressInfoDialog();
		addressInfoDialog.addressBookRaw = addressBookItem.toString();
		return addressInfoDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_address_info;
	}

	@Override
	public void onViewReady() {
		cancelBTN.setOnClickListener(this);
		displayData(AddressBookItem.fromJson(addressBookRaw));
	}

	private void displayData(AddressBookItem addressBookItem){
		labelTXT.setText(addressBookItem.address_label);
		nameTXT.setText(addressBookItem.receiver_name);
		streetAddressTXT.setText(addressBookItem.street_address);
		cityTXT.setText(addressBookItem.city);
		stateTXT.setText(addressBookItem.state);
		countryTXT.setText(addressBookItem.country);
		zipTXT.setText(addressBookItem.zip_code);
		contactTXT.setText(addressBookItem.phone_country_dial_code + addressBookItem.phone_number);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.cancelBTN:
				dismiss();
				break;
		}
	}
}
