package com.thingsilikeapp.android.dialog.defaultdialog;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dpizarro.pinview.library.PinView;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import butterknife.BindView;
import icepick.State;

public class SetPasscodeDialog extends BaseDialog implements  View.OnClickListener {
	public static final String TAG = SetPasscodeDialog.class.getName();

    @BindView(R.id.continueBTN)             TextView continueBTN;
	@BindView(R.id.pinView) 				PinView pinView;
    @BindView(R.id.exitBTN)    				ImageView extiBTN;


	public static SetPasscodeDialog Builder() {
		SetPasscodeDialog infoDialog = new SetPasscodeDialog();
		return infoDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_set_passcode;
	}

	@Override
	public void onViewReady() {
       continueBTN.setOnClickListener(this);
       extiBTN.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
//        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		setDialogMatchParent();
	}


	public void build(FragmentManager manager){
        show(manager, TAG);
	}

	@Override
	public void onClick(View view) {
		switch(view.getId()){
			case R.id.continueBTN:
				dismiss();
				ToastMessage.show(getContext(), "Success", ToastMessage.Status.SUCCESS);
				break;
			case R.id.exitBTN:
				dismiss();
				break;
		}
	}
}


