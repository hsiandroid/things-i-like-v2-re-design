package com.thingsilikeapp.android.dialog;

import android.Manifest;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import java.util.Date;

import butterknife.BindView;
import icepick.State;

public class ViewImageDialog extends BaseDialog implements View.OnClickListener {
    public static final String TAG = ViewImageDialog.class.getName();

    private DownloadManager downloadManager;
    private BroadcastReceiver onComplete;
    private final int STORAGE_PERMISSION = 111;

    @BindView(R.id.backBTN)                 ImageView backBTN;
    @BindView(R.id.imageIV)                 ImageView imageIV;
    @BindView(R.id.nameTXT)                 TextView nameTXT;
    @BindView(R.id.downloadBTN)             ImageView downloadBTN;

    @State String image;
    @State String name;
    @State long downloadReferenceId;

    public static ViewImageDialog newInstance(String image, String name) {
        ViewImageDialog viewImageDialog = new ViewImageDialog();
        viewImageDialog.image = image;
        viewImageDialog.name = name;
        return viewImageDialog;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.dialog_view_image;
    }

    @Override
    public void onViewReady() {

        nameTXT.setText(name);

        nameTXT.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        nameTXT.setSelected(true);
        nameTXT.setSingleLine(true);

        backBTN.setOnClickListener(this);
        downloadBTN.setOnClickListener(this);
        downloadManager = (DownloadManager) getContext().getSystemService(Context.DOWNLOAD_SERVICE);

        Glide.with(getContext())
                .load(image)
                .apply(new RequestOptions()

                        .dontAnimate()

                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(imageIV);
    }

    @Override
    public void onStart() {
        super.onStart();
        setDialogMatchParent();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backBTN:
                dismiss();
                break;
            case R.id.downloadBTN:
                downloadWithPermissionGranted();
                break;
        }
    }
    public  void downloadWithPermissionGranted() {
        if(PermissionChecker.checkPermissions(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE, STORAGE_PERMISSION)){
            downloadImage();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == STORAGE_PERMISSION){
            if(((BaseActivity) getContext()) .isAllPermissionResultGranted(grantResults)){
                downloadImage();
            }
        }
    }

    private void downloadImage(){

        onComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (downloadReferenceId == -1)
                    return;

                Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                Uri mostRecentDownload = downloadManager.getUriForDownloadedFile(downloadReferenceId);

                String mimeType = downloadManager.getMimeTypeForDownloadedFile(downloadReferenceId);
                fileIntent.setDataAndType(mostRecentDownload, mimeType);
                fileIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                ToastMessage.show(getContext(), "Download complete", ToastMessage.Status.SUCCESS);

                try {
                    getContext().startActivity(fileIntent);
                } catch (ActivityNotFoundException e) {

                }

                downloadReferenceId = -1;
            }
        };


        getContext().registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        Uri downloadUri = Uri.parse(image);
        String downloadName = "LYKA" + new Date().getTime() + ".png";
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle("Downloading " + downloadName);
        request.setDescription("Downloading " + downloadName);
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/LYKA/"  + downloadName);

        downloadReferenceId = downloadManager.enqueue(request);
        Log.e("downloadImage", "here" + downloadReferenceId);
    }
}
