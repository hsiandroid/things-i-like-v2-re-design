package com.thingsilikeapp.android.dialog;

import android.os.CountDownTimer;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import icepick.State;

public class MaintenanceDialog extends BaseDialog {
	public static final String TAG = MaintenanceDialog.class.getName();

	@State String nextUpdate;
	@State String timeZone;

	@BindView(R.id.timerBTN)	TextView timerBTN;

	private CountDownTimer countDownTimer;
	private TimerCallback timerCallback;

	public static MaintenanceDialog newInstance(String nextUpdate, String timeZone, TimerCallback timerCallback) {
		MaintenanceDialog maintenanceDialog = new MaintenanceDialog();
		maintenanceDialog.nextUpdate = nextUpdate;
		maintenanceDialog.timeZone = timeZone;
		maintenanceDialog.timerCallback = timerCallback;
		maintenanceDialog.setCancelable(false);
		return maintenanceDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_maintenance;
	}

	@Override
	public void onViewReady() {
	}

	private void startTimer(){
		SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		sourceFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

		SimpleDateFormat destFormat = new SimpleDateFormat("HH:mm:ss");
		destFormat.setTimeZone(TimeZone.getDefault());

		try {
			Date parsed = sourceFormat.parse(nextUpdate);
			long counter = parsed.getTime() - new Date().getTime();
			countDownTimer = new CountDownTimer(counter, 1000) {

				public void onTick(final long millisUntilFinished) {
					final String time = String.format("%d hrs : %d mins : %d secs",
							TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
							TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
							TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));

					((RouteActivity)getContext()).runOnUiThread(new Runnable() {
						@Override
						public void run() {
							timerBTN.setText(time);
						}
					});
				}

				public void onFinish() {
					if(timerCallback != null){
						timerCallback.maintenanceTimer();
					}
					dismiss();
				}
			}.start();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private void stopTimer(){
		countDownTimer.cancel();
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onResume() {
		super.onResume();
		startTimer();
	}

	@Override
	public void onPause() {
		super.onPause();
		stopTimer();
	}

	public interface TimerCallback{
		void maintenanceTimer();
	}
}
