package com.thingsilikeapp.android.dialog;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.RelatedTempRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.SearchAdapter;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.SearchItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.like.LikeRequest;
import com.thingsilikeapp.server.request.report.ReportRequest;
import com.thingsilikeapp.server.request.social.FollowRequest;
import com.thingsilikeapp.server.request.social.RecentSearchRequest;
import com.thingsilikeapp.server.request.social.SuggestedSearchRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.wishlist.BlockRequest;
import com.thingsilikeapp.server.request.wishlist.WishListRecommendedRequest;
import com.thingsilikeapp.server.transformer.social.RecentSearchTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListRelatedTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.decoration.SpacesItemDecoration;
import com.thingsilikeapp.vendor.android.widget.ExpandableHeightListView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class SearchDialog extends BaseDialog implements
		SearchAdapter.ClickListener, View.OnClickListener,
        RelatedTempRecycleViewAdapter.ClickListener,
        GroupDialog.Callback, SwipeRefreshLayout.OnRefreshListener{
	public static final String TAG = SearchDialog.class.getName();

	private Callback callback;
	private String hint;
	boolean isSearch = true;

	private RecentSearchRequest recentSearchRequest;
	private SuggestedSearchRequest suggestedSearchRequest;

	private SearchAdapter recentSearchAdapter;
	private SearchAdapter suggestedSearchAdapter;

    private RelatedTempRecycleViewAdapter socialFeedRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private WishListRecommendedRequest wishListRelatedRequest;

	@BindView(R.id.mainBackButtonIV)			ImageView mainBackButtonIV;
	@BindView(R.id.searchBTN)					ImageView searchBTN;
	@BindView(R.id.searchBarTXT)				TextView searchBarTXT;
	@BindView(R.id.searchBarET)					EditText searchBarET;
	@BindView(R.id.recentPB)					ProgressBar recentPB;
	@BindView(R.id.suggestedPB)					ProgressBar suggestedPB;
	@BindView(R.id.recommendedPB)				ProgressBar recommendedPB;
	@BindView(R.id.recentSearchEHLV)			ExpandableHeightListView recentSearchEHLV;
	@BindView(R.id.suggestedSearchEHLV)			ExpandableHeightListView suggestedSearchEHLV;
	@BindView(R.id.recommmendedRV)              RecyclerView recommmendedRV;
	@BindView(R.id.loadingIV)                   View loadingIV;
	@BindView(R.id.searchSRL)                   SwipeRefreshLayout searchSRL;
	@BindView(R.id.relatedCON)                  View relatedCON;
	@BindView(R.id.recommendedCON)              View recommendedCON;

	public static SearchDialog newInstance(Callback callback) {
		SearchDialog searchDialog = new SearchDialog();
		searchDialog.callback = callback;
		return searchDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_search;
	}

	@Override
	public void onViewReady() {
		searchBTN.setOnClickListener(this);
		mainBackButtonIV.setOnClickListener(this);
		initSearchET();

		setUpRecentSearchListView();
		setUpSuggestedListView();

		initSearchAPI();
		initSuggestedSearchAPI();

		setupRecommendedItemView();

		initFeedAPI();
	}

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void refreshList(){
        recommendedPB.setVisibility(View.VISIBLE);
        suggestedPB.setVisibility(View.VISIBLE);
        loadingIV.setVisibility(View.VISIBLE);
        recentPB.setVisibility(View.VISIBLE);
        wishListRelatedRequest
                .execute();

        suggestedSearchRequest
                .execute();

        recentSearchRequest
                .execute();
    }

    private void initFeedAPI() {
        searchSRL.setColorSchemeResources(R.color.colorPrimary);
        searchSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        searchSRL.setOnRefreshListener(this);
        wishListRelatedRequest = new WishListRecommendedRequest(getContext());
        wishListRelatedRequest
                .clearParameters()
                .setDeviceRegID(((RouteActivity) getContext()).getDeviceRegID())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                .setPerPage(15)
                .setSwipeRefreshLayout(searchSRL)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    private void setupRecommendedItemView() {
        socialFeedRecyclerViewAdapter = new RelatedTempRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recommmendedRV.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.social_feed_spacing)));
        recommmendedRV.setLayoutManager(linearLayoutManager);

        recommmendedRV.setAdapter(socialFeedRecyclerViewAdapter);
        socialFeedRecyclerViewAdapter.setOnItemClickListener(this);
//        ViewCompat.setNestedScrollingEnabled(relatedRV, true);
    }

	private void initSearchAPI(){
		recentSearchRequest = new RecentSearchRequest(getContext());
		recentSearchRequest
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.setPerPage(20)
				.execute();

	}

	private void initSuggestedSearchAPI(){
		suggestedSearchRequest = new SuggestedSearchRequest(getContext());
		suggestedSearchRequest
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.setPerPage(20)
				.execute();
	}

	private void initSearchET(){
//        discoverCategoryCON.setVisibility(searchBarET.isFocused() ? View.GONE : View.VISIBLE);
		searchBarTXT.setEllipsize(TextUtils.TruncateAt.MARQUEE);
		searchBarTXT.setSelected(true);
		searchBarTXT.setSingleLine(true);

		hint = searchBarTXT.getText().toString();
		searchBarET.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if(s.length() > 0){
//					setActionErase();
					searchBarTXT.setText("");
				}else{
//					setActionSearch();
					searchBarTXT.setText(hint);
				}
			}
		});

		searchBarET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					searchNow();
					return true;
				}
				return false;
			}
		});
	}

	public void searchNow(){
		searchNow(searchBarET.getText().toString());
	}

	public void searchNow(String keyword){
		searchBarET.setText(keyword);
//		if(callback != null){
//			callback.searchTextChange(keyword);
//		}
	}

	public void setActionErase() {
		isSearch = false;
//		searchBTN.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_close_btn2_blue));
//        searchBarET.setEnabled(false);
	}

	public void setActionSearch() {
		isSearch = true;
//		searchBTN.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.menu_search));
		searchBarET.setEnabled(true);
	}

	private void setUpRecentSearchListView(){
		recentPB.setVisibility(View.VISIBLE);

		recentSearchAdapter = new SearchAdapter(getContext());
		recentSearchEHLV.setAdapter(recentSearchAdapter);
		recentSearchAdapter.setOnItemClickListener(this);
	}

	private void setUpSuggestedListView(){
		suggestedPB.setVisibility(View.VISIBLE);

		suggestedSearchAdapter = new SearchAdapter(getContext());
		suggestedSearchEHLV.setAdapter(suggestedSearchAdapter);
		suggestedSearchAdapter.setOnItemClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Override
	public void onSearchItemClick(SearchItem searchItem) {
		searchBarET.setText(searchItem.keyword);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.searchBTN:
				if (callback != null){
					if (!searchBarET.getText().toString().equalsIgnoreCase("") || !searchBarET.getText().toString().equalsIgnoreCase(null)){
						callback.searchTextChange(searchBarET.getText().toString());
						dismiss();
					}
				}
				break;
			case R.id.mainBackButtonIV:
				dismiss();
				break;
		}
	}

	@Subscribe
	public void onResponse(RecentSearchRequest.ServerResponse responseData) {
		RecentSearchTransformer recentSearchTransformer = responseData.getData(RecentSearchTransformer.class);
		if (recentSearchTransformer.status) {
			if (responseData.isNext()) {
				recentSearchAdapter.addNewData(recentSearchTransformer.data);
			} else {
				recentSearchAdapter.setNewData(recentSearchTransformer.data);
			}
		}
        recentSearchEHLV.setVisibility(View.VISIBLE);
		recentPB.setVisibility(View.GONE);
	}

	@Subscribe
	public void onResponse(SuggestedSearchRequest.ServerResponse responseData) {
		RecentSearchTransformer suggestedSearchTransformer = responseData.getData(SuggestedSearchRequest.class);
		if (suggestedSearchTransformer.status) {
			if (responseData.isNext()) {
				suggestedSearchAdapter.addNewData(suggestedSearchTransformer.data);
			} else {
				suggestedSearchAdapter.setNewData(suggestedSearchTransformer.data);
			}
		}
        suggestedSearchEHLV.setVisibility(View.VISIBLE);
		suggestedPB.setVisibility(View.GONE);
	}

    @Override
    public void onRefresh() {
        refreshList();
    }

    public interface Callback{
		void searchTextChange(String keyword);
	}
    @Override
    public void onItemClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startItemActivity(wishListItem.id, wishListItem.owner.data.id, "i_like");
    }

    @Override
    public void onCommentClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startCommentActivity(wishListItem.id, wishListItem.title);
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        ((RouteActivity) getContext()).startProfileActivity(userItem.id);
    }

    @Override
    public void onGrabClick(WishListItem wishListItem) {
        String data = new Gson().toJson(wishListItem);
        ((RouteActivity) getContext()).startWishListActivity(data, "create_grab_other");
    }

    @Override
    public void onHideClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to hide?")
                .setNote("You will no longer see this post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Hide")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BlockRequest blockRequest = new BlockRequest(getContext());
                        blockRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());

    }

    @Override
    public void onUnfollowClick(final WishListItem wishListItem) {
        if(wishListItem.owner.data.social.data.is_following.equals("yes")){
            UnfollowDialog.newInstance(wishListItem.owner.data.id, wishListItem.owner.data.name, new UnfollowDialog.Callback() {
                @Override
                public void onAccept(int userID) {
                    UnFollowRequest unFollowRequest = new UnFollowRequest(getContext());
                    unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                            .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                            .addParameters(Keys.server.key.USER_ID, wishListItem.owner.data.id)
                            .execute();
                }

                @Override
                public void onCancel(int userID) {

                }
            }).show(((BaseActivity) getContext()).getSupportFragmentManager(), UnfollowDialog.TAG);
        }else{

            GroupDialog.newInstance(wishListItem.owner.data, this).show(getFragmentManager(),GroupDialog.TAG);
        }
    }

    @Override
    public void onReportClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to report?")
                .setNote("You are about to report a post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Report")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportRequest reportRequest = new ReportRequest(getContext());
                        reportRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.TYPE, "wishlist")
                                .addParameters(Keys.server.key.REFERENCE_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
    }

    @Override
    public void onHeartClick(WishListItem wishListItem) {

        LikeRequest likeRequest = new LikeRequest(getContext());
        likeRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                .execute();
    }

    @Override
    public void onRepostClick(WishListItem wishListItem) {
		String data = new Gson().toJson(wishListItem);
		((RouteActivity) getContext()).startWishListActivity(data, "create_grab_other");
//        ((RouteActivity) getContext()).startWishListActivity(wishListItem, "create_grab_other");
    }

    @Override
    public void onRepostUsersClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startSearchActivity(wishListItem.id, wishListItem.owner.data.common_name, "repost");
    }

    @Override
    public void onHeaderClick(EventItem eventItem) {
        ((RouteActivity) getContext()).startBirthdayActivity(eventItem, "event");
    }

    @Override
    public void onHeaderShowClick(EventItem eventItem) {
        ((RouteActivity) getContext()).startBirthdayActivity(eventItem, "greeting");
    }

    @Override
    public void onGroupSelect(String groupName, int id) {
        Log.e("Group Name",">>>" + groupName);
        FollowRequest followRequest = new FollowRequest(getContext());
        followRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .addParameters(Keys.server.key.USER_ID, id)
                .addParameters(Keys.server.key.GROUP,groupName)
                .execute();
    }

    @Override
    public void onCancel(String groupName, int id) {
//        loadingPB.setVisibility(View.GONE);
//        commandBTN.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onResponse(BlockRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            socialFeedRecyclerViewAdapter.removeItem(wishListInfoTransformer.wishListItem);
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
//            ownerInfo(userTransformer.userItem);
        }
//        loadingPB.setVisibility(View.GONE);
//        commandTXT.setVisibility(View.VISIBLE);
//        commandBTN.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onResponse(FollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
//            ownerInfo(userTransformer.userItem);
        }
//        loadingPB.setVisibility(View.GONE);
//        commandBTN.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onResponse(WishListRecommendedRequest.ServerResponse responseData) {
        WishListRelatedTransformer wishListRelatedTransformer = responseData.getData(WishListRelatedTransformer.class);
        if (wishListRelatedTransformer.status) {
            if (responseData.isNext()) {
                socialFeedRecyclerViewAdapter.addNewData(wishListRelatedTransformer.data);
            } else {
                socialFeedRecyclerViewAdapter.setNewData(wishListRelatedTransformer.data);
            }
        }

        loadingIV.setVisibility(View.GONE);
        recommendedPB.setVisibility(View.GONE);
        recommmendedRV.setVisibility(View.VISIBLE);
        if (socialFeedRecyclerViewAdapter.getItemCount() == 0) {
            relatedCON.setVisibility(View.GONE);
            recommendedCON.setVisibility(View.GONE);
        } else {
            relatedCON.setVisibility(View.VISIBLE);
            recommendedCON.setVisibility(View.VISIBLE);
        }
    }
}
