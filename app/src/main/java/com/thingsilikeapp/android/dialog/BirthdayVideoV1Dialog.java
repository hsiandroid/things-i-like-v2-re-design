package com.thingsilikeapp.android.dialog;

import android.app.Dialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.MediaController;
import android.widget.VideoView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.BirthdaySurpriseActivity;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.gift.CelebrationRequest;
import com.thingsilikeapp.server.transformer.birthday.CelebrationTransformer;
import com.thingsilikeapp.vendor.android.base.BaseV1Dialog;
import com.thingsilikeapp.vendor.android.java.SoundManager;

import org.andengine.ui.IGameInterface;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class BirthdayVideoV1Dialog extends BaseV1Dialog implements
		MediaPlayer.OnInfoListener,
		MediaPlayer.OnCompletionListener,
        View.OnClickListener{
	public static final String TAG = BirthdayVideoV1Dialog.class.getName();

    private OnVideoFinished onVideoFinished;
    private IGameInterface.OnCreateSceneCallback pOnCreateSceneCallback;
    private BirthdaySurpriseActivity birthdaySurpriseActivity;

	@BindView(R.id.birthdayVV)	VideoView birthdayVV;
	@BindView(R.id.skipBTN)	    View skipBTN;

	@State String path;
//	@State int stopPosition = 64000;
	@State int stopPosition = 0;

	public static BirthdayVideoV1Dialog newInstance(BirthdaySurpriseActivity birthdaySurpriseActivity, IGameInterface.OnCreateSceneCallback pOnCreateSceneCallback, OnVideoFinished onVideoFinished) {
		BirthdayVideoV1Dialog fragment = new BirthdayVideoV1Dialog();
        fragment.onVideoFinished = onVideoFinished;
        fragment.pOnCreateSceneCallback = pOnCreateSceneCallback;
        fragment.birthdaySurpriseActivity = birthdaySurpriseActivity;
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_birthday_video;
	}

	@Override
	public void onViewReady() {
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
        UserData.insert(UserData.GREETINGS, 0);

        skipBTN.setOnClickListener(this);
        setUpVideo();
		playVideo();
        getGreetings();
	}

	private void setSkipButton(){
        if(UserData.getBoolean(UserData.BIRTHDAY_VIDEO_2017, false)){
            skipBTN.setOnClickListener(this);
            skipBTN.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopPosition = birthdayVV.getCurrentPosition();
        birthdayVV.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        birthdayVV.seekTo(stopPosition);
        birthdayVV.start();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        Dialog dialog = new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed(){
                dismiss();
                birthdaySurpriseActivity.finish();
            }
        };
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
	public void onStart() {
		super.onStart();
        EventBus.getDefault().register(this);
		setDialogMatchParent();
	}

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void setUpVideo(){
//		path = "android.resource://" + getContext().getPackageName() + "/" + R.raw.birthday2017;
		MediaController mc = new MediaController(getActivity());
		mc.setVisibility(View.GONE);
		birthdayVV.setMediaController(mc);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			birthdayVV.setOnInfoListener(this);
		}

		birthdayVV.setOnCompletionListener(this);

	}

	private void playVideo(){
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				birthdayVV.setVideoURI(Uri.parse(path));
				birthdayVV.start();
                Log.e("Duration", ">>>" + birthdayVV.getDuration());
                SoundManager.volume(getContext(), 60);
			}
		});
	}


    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        finishVideo();
    }

    private void finishVideo(){
        SoundManager.mute(getContext());
        if(onVideoFinished != null){
            onVideoFinished.onCompletion(pOnCreateSceneCallback);
        }

        UserData.insert(UserData.BIRTHDAY_VIDEO_2017, true);
        dismiss();
    }

    public void setOnVideoFinished(OnVideoFinished onVideoFinished) {
        this.onVideoFinished = onVideoFinished;
    }

	@Override
	public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
		return false;
	}

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.skipBTN:
                finishVideo();
                break;
        }
    }

    public interface OnVideoFinished{
        void onCompletion(IGameInterface.OnCreateSceneCallback pOnCreateSceneCallback);
    }

    private void getGreetings(){
        CelebrationRequest sendGreetRequest = new CelebrationRequest(getContext());
        sendGreetRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }

    @Subscribe
    public void onResponse(CelebrationRequest.ServerResponse responseData) {
        CelebrationTransformer baseTransformer = responseData.getData(CelebrationTransformer.class);
        if(baseTransformer.status){
            Log.e("Count", ">>>" + baseTransformer.unread);
            UserData.insert(UserData.GREETINGS, baseTransformer.unread);
            setSkipButton();
        }
    }

}
