package com.thingsilikeapp.android.dialog;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.GreetingItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.gift.SayThanksRequest;
import com.thingsilikeapp.server.transformer.birthday.GreetingTransformer;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class GiftCardPreviewDialog extends BaseDialog implements
		View.OnClickListener{
	public static final String TAG = GiftCardPreviewDialog.class.getName();
    private GreetingItem greetingItem;

	@BindView(R.id.previewIMG)		ImageView previewIMG;
	@BindView(R.id.thanksBTN)		View thanksBTN;
	@BindView(R.id.cancelBTN)		View cancelBTN;
	@BindView(R.id.progressBar)		View progressBar;

	public static GiftCardPreviewDialog newInstance(GreetingItem greetingItem) {
		GiftCardPreviewDialog fragment = new GiftCardPreviewDialog();
        fragment.greetingItem = greetingItem;
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_gift_card;
	}

	@Override
	public void onViewReady() {
		thanksBTN.setOnClickListener(this);
        cancelBTN.setOnClickListener(this);
        Glide.with(getContext())
                .load(greetingItem.image.full_path)
//				.listener(new RequestListener<String, GlideDrawable>() {
//					@Override
//					public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//						return false;
//					}
//
//					@Override
//					public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//						if (progressBar != null) {
//							progressBar.setVisibility(View.GONE);
//						}
//						return false;
//					}
//				})
				.listener(new RequestListener<Drawable>() {
					@Override
					public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
						return false;
					}

					@Override
					public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
						if (progressBar != null) {
							progressBar.setVisibility(View.GONE);
						}
						return false;
					}
				})
                .into(previewIMG);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.thanksBTN:
				sayThanks();
				break;
			case R.id.cancelBTN:
				dismiss();
				break;
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	private void sayThanks(){
		SayThanksRequest randomRequest = new SayThanksRequest(getContext());
		randomRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.addParameters(Keys.server.key.GREETINGS_ID, greetingItem.id)
				.execute();
	}

	@Subscribe
	public void onResponse(SayThanksRequest.ServerResponse responseData) {
		GreetingTransformer baseTransformer = responseData.getData(GreetingTransformer.class);
		if(baseTransformer.status){
			dismiss();
			ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
		}else{
			ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
		}
	}

}
