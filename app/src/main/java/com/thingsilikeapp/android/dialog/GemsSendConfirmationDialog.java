package com.thingsilikeapp.android.dialog;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.dialog.defaultdialog.PasscodeVerificationDialog;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import java.text.DecimalFormat;

import butterknife.BindView;
import icepick.State;

public class GemsSendConfirmationDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = GemsSendConfirmationDialog.class.getName();

	private ConfirmationCallback confirmationCallback;
	private Data data;

	private ExchangeActivity rewardsActivity;

	@BindView(R.id.avatarCIV) 			         ImageView avatarCIV;
	@BindView(R.id.nameTXT)                      TextView nameTXT;
	@BindView(R.id.receivedCountTXT)             TextView receivedCountTXT;
	@BindView(R.id.cancelBTN)                    TextView cancelBTN;
	@BindView(R.id.continueBTN)                  TextView continueBTN;

	@State String avatar;
	@State boolean password_configured;
	@State boolean locksend;
	public static GemsSendConfirmationDialog newInstance(Data data, String avatar, ConfirmationCallback confirmationCallback) {
		GemsSendConfirmationDialog gemsSendConfirmationDialog = new GemsSendConfirmationDialog();
		gemsSendConfirmationDialog.confirmationCallback = confirmationCallback;
		gemsSendConfirmationDialog.data = data;
		gemsSendConfirmationDialog.avatar = avatar;
		return gemsSendConfirmationDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_gems_send_confirmation;
	}

	@Override
	public void onViewReady() {
        cancelBTN.setOnClickListener(this);
        continueBTN.setOnClickListener(this);

        displayData();
	}

	private void displayData(){
        if(data != null){

//            Glide.with(getContext())
//                    .load(data.getAvatar())
//                    .placeholder(R.drawable.placeholder_avatar)
//                    .error(R.drawable.placeholder_avatar)
//                    .into(avatarCIV);

			Picasso.with(getContext())
					.load(avatar)
					.placeholder(R.drawable.placeholder_avatar)
					.error(R.drawable.placeholder_avatar)
					.into(avatarCIV);

            nameTXT.setText(data.name);

            double value = Double.parseDouble(data.amount);
			DecimalFormat df = new DecimalFormat("####0.00");
			receivedCountTXT.setText(df.format(value));
//            receivedCountTXT.setText(data.amount);
        }
    }

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.cancelBTN:
                dismiss();
				break;
			case R.id.continueBTN:
                confirm();
                break;
		}
	}

	private void confirm(){
	    if(confirmationCallback != null){
	        confirmationCallback.confirmationCallback();
        }
        dismiss();
    }

    public static class Data{
	    String name;
	    String avatar;
	    String amount;

        public String getAvatar() {
            if(StringFormatter.isEmpty(avatar)){
                avatar = "/";
            }
            return avatar;
        }
    }

	public interface ConfirmationCallback{
		void confirmationCallback();
	}
}
