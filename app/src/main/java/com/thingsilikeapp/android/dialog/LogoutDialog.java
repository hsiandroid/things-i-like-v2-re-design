package com.thingsilikeapp.android.dialog;

import android.app.ProgressDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.auth.LogoutRequest;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class LogoutDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = LogoutDialog.class.getName();

	@BindView(R.id.confirmBTN) TextView confirmBTN;
	@BindView(R.id.cancelBTN) TextView cancelBTN;

	public static LogoutDialog newInstance() {
		LogoutDialog logoutDialog = new LogoutDialog();
		return logoutDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_logout;
	}

	@Override
	public void onViewReady() {
		confirmBTN.setOnClickListener(this);
		cancelBTN.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
			EventBus.getDefault().register(this);
			setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.confirmBTN:
				attemptLogout();
				break;
			case R.id.cancelBTN:
				dismiss();
				break;
		}
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	private void attemptLogout(){
		LogoutRequest refreshTokenRequest = new LogoutRequest(getContext());
		refreshTokenRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Logging out...", false, false))
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.execute();
	}

	@Subscribe
	public void onResponse(LogoutRequest.ServerResponse responseData) {
		BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);
		if(baseTransformer.status){
			((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT,FacebookCustomLoggerMessage.LOGOUT);
			LoginManager.getInstance().logOut();
			boolean firstOpen = UserData.getBoolean(UserData.FIRST_OPEN);
			boolean walkthruDone = UserData.getBoolean(UserData.WALKTHRU_DONE);
			UserData.clearData();
			UserData.insert(UserData.FIRST_OPEN, firstOpen);
			UserData.insert(UserData.WALKTHRU_DONE, walkthruDone);
			dismiss();
			((RouteActivity) getContext()).startLandingActivity();
			((RouteActivity) getContext()).finish();
			ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
		}else{
			ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
		}
	}
}
