package com.thingsilikeapp.android.dialog;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.system.ErrnoException;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableGridView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.nineoldandroids.view.ViewHelper;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.GalleryAdapter;
import com.thingsilikeapp.data.model.GalleryItem;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.ImageQualityManager;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class ImagePickerDialog extends BaseDialog implements View.OnClickListener,
		CropImageView.OnSetImageUriCompleteListener,
		CropImageView.OnCropImageCompleteListener,
        GalleryAdapter.ClickListener,
        ObservableScrollViewCallbacks {

	public static final String TAG = ImagePickerDialog.class.getName();

	private static final int REQUEST_CAMERA = 10001;
	private static final int REQUEST_GALLERY = 10002;

	private static final int PERMISSION_CAMERA = 101;
	private static final int PERMISSION_GALLERY = 102;
	private File photoFile;

	private ImageCallback imageCallback;
	private GalleryAdapter galleryAdapter;
    private int mParallaxImageHeight;

	@BindView(R.id.cropImageView)		CropImageView cropImageView;
	@BindView(R.id.cameraBTN)			View cameraBTN;
	@BindView(R.id.galleryBTN) 			View galleryBTN;
	@BindView(R.id.rotateLeftBTN) 		View rotateLeftBTN;
	@BindView(R.id.rotateRightBTN) 		View rotateRightBTN;
	@BindView(R.id.pickerBTN)			View pickerBTN;
	@BindView(R.id.cancelBTN)			View cancelBTN;
	@BindView(R.id.imageCON)			View imageCON;
	@BindView(R.id.placeHolderIV)		View placeHolderIV;
	@BindView(R.id.headerCON)		    View headerCON;
	@BindView(R.id.mainCon)		        View mainCon;
	@BindView(R.id.backgroundCON)		View backgroundCON;
	@BindView(R.id.titleTXT)			TextView titleTXT;
	@BindView(R.id.galleryOGV)          ObservableGridView galleryOGV;

	@State String title;
	@State boolean isAspectRatio = false;

	private Uri uri;

	public static ImagePickerDialog newInstance(String title, boolean isAspectRatio, ImageCallback imageCallback) {
		ImagePickerDialog imagePickerDialog = new ImagePickerDialog();
		imagePickerDialog.title = title;
		imagePickerDialog.isAspectRatio = isAspectRatio;
		imagePickerDialog.imageCallback = imageCallback;
		return imagePickerDialog;
	}

	public static ImagePickerDialog newInstance(String title, boolean isAspectRatio, Uri uri, ImageCallback imageCallback) {
		ImagePickerDialog imagePickerDialog = new ImagePickerDialog();
		imagePickerDialog.title = title;
		imagePickerDialog.isAspectRatio = isAspectRatio;
		imagePickerDialog.uri = uri;
		imagePickerDialog.imageCallback = imageCallback;
		return imagePickerDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_image_picker_v2;
	}

	@Override
	public void onViewReady() {
		pickerBTN.setOnClickListener(this);
		cameraBTN.setOnClickListener(this);
		galleryBTN.setOnClickListener(this);
		rotateLeftBTN.setOnClickListener(this);
		rotateRightBTN.setOnClickListener(this);
		cancelBTN.setOnClickListener(this);
		imageCON.setOnClickListener(this);

		if(title == null){
			titleTXT.setText(getString(R.string.app_name));
		}else{
			titleTXT.setText(title);
		}

		cropImageView.setFixedAspectRatio(isAspectRatio);

		if(uri != null){
			showImage(uri);
		}

		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());

        initGallery();
	}

	private void initGallery(){
        if(PermissionChecker.checkPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_GALLERY)){
            mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen.image_picker_total_size);

            galleryOGV.setScrollViewCallbacks(this);
            View paddingView = new View(getContext());
            AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, mParallaxImageHeight);
            paddingView.setLayoutParams(lp);

            galleryOGV.addHeaderView(paddingView);
            galleryAdapter = new GalleryAdapter(getContext());
            galleryAdapter.setOnItemClickListener(this);
            galleryOGV.setAdapter(galleryAdapter);
//            galleryOGV.setOnScrollListener(new InfiniteScrollListener(5) {
//                @Override
//                public void loadMore(int page, int totalItemsCount) {
//                    galleryAdapter.addPage(page);
//                }
//            });

            galleryAdapter.setNewData(getAllImages());
        }
	}

    private void resetFlexibleSpace() {
        galleryOGV.post(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= 11) {
                    galleryOGV.smoothScrollToPositionFromTop(0, 0);
                } else {
                    galleryOGV.setSelection(0);
                }
            }
        });
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        ViewHelper.setTranslationY(headerCON, -scrollY / 2);
        ViewHelper.setTranslationY(backgroundCON, Math.max(0, -scrollY + mParallaxImageHeight));
    }

    @Override
    public void onDownMotionEvent() {
        Log.e("onDownMotionEvent", ">>>onDownMotionEvent");
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        Log.e("ScrollState", ">>>" + scrollState);
    }

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		cropImageView.setOnSetImageUriCompleteListener(this);
		cropImageView.setOnCropImageCompleteListener(this);
	}

	@Override
	public void  onStop() {
		super.onStop();
		cropImageView.setOnSetImageUriCompleteListener(null);
		cropImageView.setOnCropImageCompleteListener(null);
	}

    @Override
    public void onImageClickListener(GalleryItem galleryItem) {
        if(galleryItem.isCamera){
            openCamera();
        }else{
            showImage(Uri.fromFile(new File(galleryItem.absolutePath)));
            resetFlexibleSpace();
        }
    }

	@Override
	public void onSelectedClickListener(int position) {

	}

	@Override
	public void onVideoClickListener(GalleryItem galleryItem) {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.pickerBTN:
				cropImageView.getCroppedImageAsync();
				break;
			case R.id.cameraBTN:
			case R.id.imageCON:
				openCamera();
				break;
			case R.id.galleryBTN:
				openGallery();
				break;
			case R.id.rotateLeftBTN:
				cropImageView.rotateImage(-90);
				break;
			case R.id.rotateRightBTN:
				cropImageView.rotateImage(90);
				break;
			case R.id.cancelBTN:
				dismiss();
				break;
		}
	}

	@Override
	public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
		if(result.isSuccessful()){
			File imageFile = getImageFile(view.getCroppedImage());
			String newPath = ImageQualityManager.compressImage(getContext(), imageFile.getAbsolutePath());
			File cropFile = new File(newPath);
			if(imageCallback != null){
				imageCallback.result(cropFile);
			}
			dismiss();
		}
	}

	public  List<GalleryItem> getAllImages() {

		int indexData;
		int indexName;

		List<GalleryItem> listOfAllImages = new ArrayList<>();
		Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

		String[] projection = { MediaStore.MediaColumns.DATA,
				MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

		Cursor cursor = getContext().getContentResolver().query(uri, projection, null, null, MediaStore.Images.Media.DATE_ADDED + " DESC");

		indexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
		indexName = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
		GalleryItem galleryItem;

		while (cursor.moveToNext()) {
			galleryItem = new GalleryItem();
			galleryItem.absolutePath = cursor.getString(indexData);
			galleryItem.name = cursor.getString(indexName);
			listOfAllImages.add(galleryItem);
		}

		return listOfAllImages;
	}

	@Override
	public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {

	}

    public interface ImageCallback{
		void result(File file);
	}

	private File createImageDir(){
        File storageDir = new File(Environment.getExternalStorageDirectory(), "Android/media/com.thingsilikeapp");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        return storageDir;
    }

	private File getImageFile(Bitmap bitmap) {
		String imageFileName = "ThingsILikeCacheImage";
		File image = null;
		try {
			image = File.createTempFile(
					imageFileName,  /* prefix */
					".jpg",         /* suffix */
					createImageDir()      /* directory */
			);

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
			byte[] bitmapData = bos.toByteArray();

			FileOutputStream fos = new FileOutputStream(image);
			fos.write(bitmapData);
			fos.flush();
			fos.close();


		} catch (IOException e) {
			e.printStackTrace();
		}

		return image;
	}

	private void openGallery(){
		if(PermissionChecker.checkPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_GALLERY)){
			Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			intent.setType("image/*");
			startActivityForResult(intent, REQUEST_GALLERY);
		}
	}

	private void openCamera(){
		if(PermissionChecker.checkPermissions(getActivity(), new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CAMERA)){
			Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
				photoFile = createImageFile();
				if (photoFile != null) {
					Uri uri = Uri.fromFile(photoFile);
					takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
							| Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
					if (uri != null) {
						takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
					}
					startActivityForResult(takePictureIntent, REQUEST_CAMERA);
				}
			}
		}
	}

	private File createImageFile() {
		String imageFileName = "I_LIKE_TEMP";
		File image = null;
		try {
			image = File.createTempFile(
					imageFileName,  /* prefix */
					".png",         /* suffix */
                    createImageDir()       /* directory */
			);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return image;
	}

	@Override
	public  void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode){
			case REQUEST_CAMERA:
			case REQUEST_GALLERY:
				if(resultCode == Activity.RESULT_OK){
					showImage(getPickImageResultUri(data));
				}
				break;
		}
	}

	public void showImage(Uri imageUri){
		boolean requirePermissions = false;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
				getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
				isUriRequiresPermissions(imageUri)) {

			requirePermissions = true;
			requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
		}

		if (!requirePermissions) {
			cropImageView.setImageUriAsync(imageUri);
		}
		cropImageView.setVisibility(View.VISIBLE);
		placeHolderIV.setVisibility(View.GONE);
	}

	public boolean isUriRequiresPermissions(Uri uri) {
		try {
			ContentResolver resolver = getActivity().getContentResolver();
			InputStream stream = resolver.openInputStream(uri);
			stream.close();
			return false;
		} catch (FileNotFoundException e) {
			if (e.getCause() instanceof ErrnoException) {
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}

	public Uri getPickImageResultUri(Intent  data) {
		boolean isCamera = true;
		if (data != null && data.getData() != null) {
			String action = data.getAction();
			isCamera = action != null  && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
		}
		return isCamera ?  getCaptureImageOutputUri() : data.getData();
	}

	private Uri getCaptureImageOutputUri() {
		Uri outputFileUri = null;
		if (photoFile != null) {
			outputFileUri = Uri.fromFile(photoFile);
		}
		return outputFileUri;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == PERMISSION_CAMERA) {
			if(getBaseActivity().isAllPermissionResultGranted(grantResults)){
				openCamera();
			}
		}
		if (requestCode == PERMISSION_GALLERY) {
			if(getBaseActivity().isAllPermissionResultGranted(grantResults)){
                initGallery();
			}else{
                dismiss();
            }
		}
	}
}
