package com.thingsilikeapp.android.dialog;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WalletItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.user.UserInfoRequest;
import com.thingsilikeapp.server.request.user.UserInfoScanRequest;
import com.thingsilikeapp.server.request.user.UserInfoUsernameRequest;
import com.thingsilikeapp.server.request.wallet.WalletSendRequest;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.server.transformer.wallet.WalletTransformer;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.DecimalDigitsInputFilter;
import com.thingsilikeapp.vendor.android.java.GemHelper;
import com.thingsilikeapp.vendor.android.java.NumberFormatter;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import butterknife.BindView;
import icepick.State;

public class GemsSendsDialog extends BaseDialog implements View.OnClickListener,
		GemsSendConfirmationDialog.ConfirmationCallback, GemsSendSuccessDialog.Callback{
	public static final String TAG = GemsSendsDialog.class.getName();

	private UserItem userItem;
	private Callback callback;

    @BindView(R.id.resultET)                    EditText resultET;
	@BindView(R.id.amountHintTXT)               TextView amountHintTXT;
	@BindView(R.id.amountET)                    EditText amountET;
	@BindView(R.id.continueBTN)                 TextView continueBTN;
	@BindView(R.id.exitBTN)                 	View exitBTN;
	@BindView(R.id.friendBTN)                 	View friendBTN;
	@BindView(R.id.scanBTN)                 	View scanBTN;
	@BindView(R.id.gemTXT)                      TextView gemTXT;
	@State String  username;

	public static GemsSendsDialog newInstance(Callback callback) {
		GemsSendsDialog gemsSendConfirmationDialog = new GemsSendsDialog();
		gemsSendConfirmationDialog.callback = callback;
		return gemsSendConfirmationDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_gems_send;
	}

	@Override
	public void onViewReady() {
        continueBTN.setOnClickListener(this);
        friendBTN.setOnClickListener(this);
		scanBTN.setOnClickListener(this);
		exitBTN.setOnClickListener(this);
        gemTXT.setText(GemHelper.getCurrentGem());

        amountET.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(8,2)});

		amountET.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void afterTextChanged(Editable editable) {
				amountHintTXT.setVisibility(amountET.getText().length() > 0 ? View.GONE : View.VISIBLE);
			}
		});

        resultET.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                userItem = null;
			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();
        EventBus.getDefault().register(this);
		setDialogMatchParent();
	}

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.continueBTN:
                openConfirmation();
				break;
			case R.id.scanBTN:
				((RouteActivity) getContext()).startGemActivity("scanner");
				break;
			case R.id.friendBTN:
                ((RouteActivity) getContext()).startSearchActivityResult();
				break;
			case R.id.exitBTN:
				dismiss();
				break;
		}
	}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RouteActivity.GEM_CODE && resultCode == Activity.RESULT_OK){
            String id = decrypt(data.getStringExtra("result"));
            if(id.matches("-?\\d+")){
                getUserByID(Integer.parseInt(id));
            }
        }else if(requestCode == RouteActivity.FRIEND_CODE && resultCode == Activity.RESULT_OK){
            UserItem userItem = new Gson().fromJson(data.getStringExtra("result"), UserItem.class);
            if(userItem != null){
                this.userItem = userItem;
                resultET.setText(userItem.username);
                resultET.setSelection(resultET.getText().length());
                resultET.setError(null);
                Log.e("EEEEEEEEE", ">>>>>>" +userItem.username );
            }
        }
    }

    private String decrypt(String raw){
		byte[] dataDec = Base64.decode(raw, Base64.DEFAULT);
		String decodedString = "";
		try {

			decodedString = new String(dataDec, StandardCharsets.UTF_8);
		} finally {

			return decodedString;
		}
    }

    private void getUserByID(int id){
        new UserInfoScanRequest(getContext())
                .setDeviceRegID(getBaseActivity().getDeviceRegID())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.USER_ID, id)
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .execute();
    }

    private void getUserByUsername(String username){
        new UserInfoUsernameRequest(getContext())
                .setDeviceRegID(getBaseActivity().getDeviceRegID())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.USERNAME, username)
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .execute();
    }

    private void attemptSend(){
        new WalletSendRequest(getContext())
                .setDeviceRegID(getBaseActivity().getDeviceRegID())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.USERNAME, userItem.username)
                .addParameters(Keys.server.key.AMOUNT, amountET.getText().toString())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .execute();
    }

    private void openConfirmation(){
	    boolean hasError = false;

	    if(StringFormatter.isEmpty(resultET.getText().toString())){
            hasError = true;
            resultET.setError("Field is required");
        }

	    if(StringFormatter.isEmpty(amountET.getText().toString())){
            hasError = true;
            amountET.setError("Field is required");
        }

	    if(NumberFormatter.parseDouble(GemHelper.getCurrentGem()) < NumberFormatter.parseDouble(amountET.getText().toString())){
            hasError = true;
            amountET.setError("Insufficient amount");
        }

        if(hasError){
            ToastMessage.show(getContext(), "Incomplete or invalid input!", ToastMessage.Status.FAILED);
	        return;
        }

	    if(userItem == null){
            getUserByUsername(resultET.getText().toString());
        }else{
            proceedConfirmation();
        }
	}

	@Override
	public void confirmationCallback() {
        attemptSend();
	}

    @Subscribe
    public void onResponse(UserInfoScanRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            userItem = userTransformer.userItem;
            resultET.setText(userItem.username);
            resultET.setSelection(resultET.getText().length());
            resultET.setError(null);
        }else{
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(UserInfoUsernameRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            userItem = userTransformer.userItem;
            proceedConfirmation();
        }else{
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(WalletSendRequest.ServerResponse responseData) {
        WalletTransformer walletTransformer = responseData.getData(WalletTransformer.class);
        if (walletTransformer.status) {

            WalletItem walletItem = new WalletItem();
            walletItem.avatar = walletTransformer.userItem.getAvatar();
            walletItem.name = walletTransformer.userItem.common_name;
            walletItem.amount = walletTransformer.amount_sent;
            walletItem.refid = walletTransformer.refid;
            GemsSendSuccessDialog.newInstance(walletItem, walletItem.avatar, this).show(getFragmentManager(), GemsSendSuccessDialog.TAG);
        }else{
            ToastMessage.show(getContext(), walletTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    private void proceedConfirmation(){
        GemsSendConfirmationDialog.Data data = new GemsSendConfirmationDialog.Data();
        data.avatar = userItem.getAvatar();
        data.name = userItem.common_name;
        data.amount = amountET.getText().toString();
        GemsSendConfirmationDialog.newInstance(data, data.getAvatar(), this).show(getFragmentManager(), GemsSendConfirmationDialog.TAG);
    }

    @Override
    public void onSuccess() {
        if(callback != null){
            callback.onSuccess();
            dismiss();
        }
    }

    public interface Callback {
        void onSuccess();
    }
}
