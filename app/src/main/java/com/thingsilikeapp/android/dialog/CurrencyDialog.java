package com.thingsilikeapp.android.dialog;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.CurrencyRecycleViewAdapter;
import com.thingsilikeapp.data.model.GetRateItem;
import com.thingsilikeapp.data.model.CurrencyItem;
import com.thingsilikeapp.data.model.GetRateItem;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class CurrencyDialog extends BaseDialog implements  View.OnClickListener, CurrencyRecycleViewAdapter.ClickListener {
	public static final String TAG = CurrencyDialog.class.getName();

	private CurrencyRecycleViewAdapter currencyRecycleViewAdapter;
	private LinearLayoutManager linearLayoutManager;
	private Callback callback;

	@BindView(R.id.currencyRV) 				RecyclerView currencyRV;
    @BindView(R.id.exitBTN)    				ImageView extiBTN;

    @State int service_fee;

	public static CurrencyDialog Builder(Callback callback) {
		CurrencyDialog infoDialog = new CurrencyDialog();
		infoDialog.callback = callback;
		return infoDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_currency;
	}

	@Override
	public void onViewReady() {
       extiBTN.setOnClickListener(this);
       setUpListView();
       Wallet.getDefault().rate(getContext());
	}

	private void setUpListView(){
		currencyRecycleViewAdapter = new CurrencyRecycleViewAdapter(getContext());
		linearLayoutManager = new LinearLayoutManager(getContext());
		linearLayoutManager.setReverseLayout(true);
		currencyRV.setLayoutManager(linearLayoutManager);
		currencyRV.setAdapter(currencyRecycleViewAdapter);
		currencyRecycleViewAdapter.setOnItemClickListener(this);
		currencyRecycleViewAdapter.setOnItemClickListener(this);
	}

	@Subscribe
	public void onResponse(Wallet.RateResponse colletionResponse) {
		CollectionTransformer<GetRateItem> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
		if (collectionTransformer.status) {
			if (colletionResponse.isNext()) {
				currencyRecycleViewAdapter.addNewData(collectionTransformer.data);
			} else {
				currencyRecycleViewAdapter.setNewData(collectionTransformer.data);
			}
			service_fee = collectionTransformer.service_fee;
			Log.e("FEE", collectionTransformer.selected_currency + " " + collectionTransformer.service_fee);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
//        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		setDialogMatchParent();
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	public void build(FragmentManager manager){
        show(manager, TAG);
	}

	@Override
	public void onClick(View view) {
		switch(view.getId()){
			case R.id.exitBTN:
				dismiss();
				break;
		}
	}

	@Override
	public void onItemClick(GetRateItem currencyItem) {
		if(callback != null){
			callback.onSuccess(currencyItem, service_fee);
			dismiss();
		}
	}

	@Override
	public void onItemLongClick(GetRateItem currencyItem) {

	}

	public interface Callback{
		void onSuccess(GetRateItem rateItem, int service_fee);
	}
}


