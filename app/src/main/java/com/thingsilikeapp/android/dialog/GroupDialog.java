package com.thingsilikeapp.android.dialog;

import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.GroupAdapter;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.user.GroupRequest;
import com.thingsilikeapp.server.transformer.user.GroupCollectionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class GroupDialog extends BaseDialog implements
		View.OnClickListener {
	public static final String TAG = GroupDialog.class.getName();

	private Callback callback;
	private GroupAdapter groupAdapter;
	private GroupRequest groupRequest;

    @BindView(R.id.headerTXT)               TextView headerTXT;
    @BindView(R.id.subHeaderTXT)            TextView subHeaderTXT;
    @BindView(R.id.imageCIV)				ImageView imageCIV;
    @BindView(R.id.commonNameTXT)			TextView commonNameTXT;
    @BindView(R.id.cancelBTN)				View cancelBTN;
    @BindView(R.id.followBTN)				TextView followBTN;
    @BindView(R.id.groupEHLV)               ListView groupEHLV;
    @BindView(R.id.groupPB)					ProgressBar groupPB;

	@State int id;
    @State
    String raw;

	public static GroupDialog newInstance(UserItem userItem, Callback callback) {
		GroupDialog groupDialog = new GroupDialog();
		groupDialog.id = userItem.id;
        groupDialog.raw = new Gson().toJson(userItem);
        groupDialog.callback = callback;
        return groupDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_group;
	}

	@Override
	public void onViewReady() {

		cancelBTN.setOnClickListener(this);
		followBTN.setOnClickListener(this);

		displayData();
		setUpGroupListView();
		attemptGetGroups();
	}

	private void attemptGetGroups(){
		followBTN.setEnabled(false);
		groupPB.setVisibility(View.VISIBLE);
		groupRequest = new GroupRequest(getContext());
        groupRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	private void setUpGroupListView(){
		groupAdapter = new GroupAdapter(getContext());
		groupEHLV.setAdapter(groupAdapter);
	}

	private void displayData(){
        UserItem userItem = new Gson().fromJson(raw, UserItem.class);
        headerTXT.setText("FOLLOW " + userItem.name + " ?");
        subHeaderTXT.setText("ASSIGN " + userItem.pronoun("HIM", "HER") + " TO A GROUP");
        commonNameTXT.setText(userItem.common_name);

		Glide.with(getContext())
                .load(userItem.image)
				.apply(new RequestOptions()
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
				.fitCenter()
				.dontAnimate()
				.diskCacheStrategy(DiskCacheStrategy.ALL)
				.skipMemoryCache(true))
				.into(imageCIV);
	}

	@Override
	public void onStart() {
		super.onStart();
        setDialogMatchParent();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.cancelBTN:
				dismiss();
				break;
			case R.id.followBTN: {
                onSaveClicked();
				break;
			}
		}
	}

    @Override
    public void dismiss() {
        if (callback != null){
            callback.onCancel("", id);
        }
        super.dismiss();
    }

    private void onSaveClicked(){
        String group = groupAdapter.getSelectedItemName();
        if(group.equalsIgnoreCase("custom")){
            ToastMessage.show(getContext(), "Custom field is required.", ToastMessage.Status.FAILED);
        }else{
            callback.onGroupSelect(groupAdapter.getSelectedItemName(), id);
            dismiss();
        }
    }

	public interface Callback{
		void onGroupSelect(String groupName, int id);
		void onCancel(String groupName, int id);
	}

	@Subscribe
	public void onResponse(GroupRequest.ServerResponse responseData) {
        GroupCollectionTransformer groupCollectionTransformer = responseData.getData(GroupCollectionTransformer.class);
        if (groupCollectionTransformer.status) {
            groupAdapter.setNewData(groupCollectionTransformer.data);
            groupAdapter.setItemSelected("");
            followBTN.setEnabled(true);
            groupPB.setVisibility(View.GONE);
        }else{
            dismiss();
        }
	}
}
