package com.thingsilikeapp.android.dialog;

import android.os.Handler;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseDialog;

import butterknife.BindView;

public class WalkThroughIntroDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = WalkThroughIntroDialog.class.getName();

    private Handler handler;

	public Callback callback;

	@BindView(R.id.dialogCON)               View dialogCON;

	public static WalkThroughIntroDialog newInstance(Callback callback) {
		WalkThroughIntroDialog fragment = new WalkThroughIntroDialog();
		fragment.callback = callback;
		return fragment;
	}
	@Override
	public int onLayoutSet() {
		return R.layout.dialog_walkthrough_intro;
	}

	@Override
	public void onViewReady() {
        dialogCON.setOnClickListener(this);
//        handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (callback != null){
//                    callback.onDismiss();
//                    UserData.insert(UserData.CREATE_POST_SHOW, 1);
//                    UserData.insert(UserData.WALKTHRU_CURRENT, 1);
//                    UserData.insert(UserData.BIRTHDAY_COUNT, 1);
//                    UserData.insert(UserData.POST_SHOW, 1);
//                    UserData.insert(UserData.SHARE_POST_SHOW, 1);
//                    UserData.insert(UserData.SHARE_POST, 1);
//                    UserData.insert(UserData.REQUEST_LIKE_SHOW, 1);
//                    UserData.insert(UserData.REQUEST_ADDRESS_SHOW, 1);
//                    UserData.insert(UserData.LIKE_POST_SHOW, 1);
//                    UserData.insert(UserData.PROFILE_CURRENT, false);
//                    UserData.insert(UserData.WALKTHRU_RESPONSE, true);
//                    dismiss();
//                }
//            }
//        }, 5000);
	}

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dialogCON:
                UserData.insert(UserData.CREATE_POST_SHOW, 1);
                UserData.insert(UserData.WALKTHRU_CURRENT, 1);
                UserData.insert(UserData.BIRTHDAY_COUNT, 1);
                UserData.insert(UserData.POST_SHOW, 1);
                UserData.insert(UserData.REQUEST_ADDRESS_SHOW, 1);
                UserData.insert(UserData.REQUEST_LIKE_SHOW, 1);
                UserData.insert(UserData.SHARE_POST_SHOW, 1);
                UserData.insert(UserData.SHARE_POST, 1);
                UserData.insert(UserData.LIKE_POST_SHOW, 1);
                UserData.insert(UserData.PROFILE_CURRENT, false);
                UserData.insert(UserData.WALKTHRU_CREATE_DONE, false);
                UserData.insert(UserData.WALKTHRU_CURRENT_PROFILE, false);
                UserData.insert(UserData.WALKTHRU_RESPONSE, true);
                if (callback != null){
                    callback.onDismiss(this);
                }
                dismiss();
            break;
        }
    }

    public interface Callback{
		void onDismiss(WalkThroughIntroDialog walkThroughIntroDialog);
	}
}
