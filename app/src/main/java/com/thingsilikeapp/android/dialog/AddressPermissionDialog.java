package com.thingsilikeapp.android.dialog;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.AddressPickerRecycleViewAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.AddressBookItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.address.AllAddressRequest;
import com.thingsilikeapp.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class AddressPermissionDialog extends BaseDialog implements
		View.OnClickListener, AddressPickerRecycleViewAdapter.ClickListener{
	public static final String TAG = AddressPermissionDialog.class.getName();

	private AddressPickerRecycleViewAdapter addressPickerRecycleViewAdapter;
	private LinearLayoutManager linearLayoutManager;

	private AllAddressRequest allAddressRequest;

	@BindView(R.id.positiveBTN)			TextView positiveBTN;
	@BindView(R.id.negativeBTN)			TextView negativeBTN;
	@BindView(R.id.addBTN)				TextView addBTN;
	@BindView(R.id.descriptionTXT)		TextView descriptionTXT;
	@BindView(R.id.mainBackButtonIV) 	ImageView mainBackButtonIV;
	@BindView(R.id.mainTitleTXT)		TextView mainTitleTXT;
	@BindView(R.id.savedAddressRV)		RecyclerView savedAddressRV;
	@BindView(R.id.addressBookPB)		ProgressBar addressBookPB;

	private AddressPermissionListener addressPermissionListener;

	public static AddressPermissionDialog newInstance(AddressPermissionListener addressPermissionListener) {
		AddressPermissionDialog addressPermissionDialog = new AddressPermissionDialog();
		addressPermissionDialog.setCancelable(false);
		addressPermissionDialog.addressPermissionListener = addressPermissionListener;
		return addressPermissionDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_address_permission;
	}

	@Override
	public void onViewReady() {
		mainTitleTXT.setText(getString(R.string.address_select));
		positiveBTN.setOnClickListener(this);
		negativeBTN.setOnClickListener(this);
		addBTN.setOnClickListener(this);
		mainBackButtonIV.setOnClickListener(this);
		descriptionTXT.setText(getDescription());
		descriptionTXT.setMovementMethod(LinkMovementMethod.getInstance());

		setUpAddressBookListView();

		initAPICall();
	}

	private void setUpAddressBookListView(){
		addressPickerRecycleViewAdapter = new AddressPickerRecycleViewAdapter(getContext());
		linearLayoutManager = new LinearLayoutManager(getContext());
		savedAddressRV.setLayoutManager(linearLayoutManager);
		savedAddressRV.setAdapter(addressPickerRecycleViewAdapter);
		addressPickerRecycleViewAdapter.setOnItemClickListener(this);
	}

	private void initAPICall(){
		allAddressRequest = new AllAddressRequest(getContext());
		allAddressRequest
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.setPerPage(10)
				.execute();
	}

	private SpannableString getDescription(){
		String privacy = "Privacy Policy";
		String review = "Review our Privacy Policy for more details.";
        String message = "Are you sure you want to share your postal address to this follower?\n" + review;
		SpannableString spannableString = new SpannableString(message);

		ClickableSpan nameSpan = new ClickableSpan() {
			@Override
			public void onClick(View textView) {
                openUrl(Keys.server.route.PRIVACY_POLICY);
			}

			public void updateDrawState(TextPaint ds) {
				ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
				ds.setTypeface(TypefaceUtils.load(getContext().getAssets(), getContext().getString(R.string.typeface_display_name)));
			}
		};

        spannableString.setSpan(new AbsoluteSizeSpan(12, true), message.indexOf(review), message.indexOf(review) + review.length(), SpannableString.SPAN_INCLUSIVE_INCLUSIVE);
		spannableString.setSpan(nameSpan, message.indexOf(privacy), message.indexOf(privacy) + privacy.length(), Spanned.SPAN_INTERMEDIATE);

		return spannableString;
	}

    public void openUrl(String url){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        getContext().startActivity(browserIntent);
    }

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
//		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		setDialogMatchParent();
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

    @Override
    public void onResume() {
        super.onResume();
		addressBookPB.setVisibility(View.VISIBLE);
        allAddressRequest.first();
    }

    @Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.positiveBTN:
				if(addressPermissionListener != null){
					addressPermissionListener.positiveClicked(addressPickerRecycleViewAdapter.getSelectedID());
				}
				dismiss();
				break;
			case R.id.negativeBTN:
				dismiss();
				break;
			case R.id.mainBackButtonIV:
				dismiss();
				break;
			case R.id.addBTN:
				((RouteActivity) getContext()).startAddressBookActivity("", "create");
				break;
		}
	}

	@Override
	public void onItemClick(AddressBookItem addressBookItem) {
		addressPickerRecycleViewAdapter.setItemSelected(addressBookItem.id);
	}

    public interface AddressPermissionListener{
		void positiveClicked(int addressID);
	}

	@Subscribe
	public void onResponse(AllAddressRequest.ServerResponse responseData) {
		CollectionTransformer<AddressBookItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
		if(collectionTransformer.status){
				if (UserData.getUserItem().is_verified){
					AddressBookItem addressBookItem = new AddressBookItem();
					addressBookItem.id = -1;
					addressBookItem.address_label = "Pick up center";
					addressBookItem.street_address = "C/O Highly Succeed Inc., 28F Tower 2 Enterprise Center, Ayala-Paseo de Roxas";
					addressBookItem.phone_number = "09062708663";
					addressBookItem.state = "Metro Manila";
					addressBookItem.city = "Makati";
					addressBookItem.country = "Philippines";
					addressBookItem.zip_code = "1226";
					addressBookItem.is_default = "no";
					addressBookItem.selected = false;
					collectionTransformer.data.add(0, addressBookItem);
				}
			if(responseData.isNext()){
				addressPickerRecycleViewAdapter.addNewData(collectionTransformer.data);
			}else{
				addressPickerRecycleViewAdapter.setNewData(collectionTransformer.data);
			}
			addressPickerRecycleViewAdapter.setDefaultSelected();
		}

		addressBookPB.setVisibility(View.GONE);
		if (addressPickerRecycleViewAdapter.getItemCount() == 0){
//			((RouteActivity) getContext()).startAddressBookActivity("", "create");
		}
	}
}
