package com.thingsilikeapp.android.dialog;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.LandingActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.auth.SignUpRequest;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.BaseDialog;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class SignUpDialog extends BaseDialog implements View.OnClickListener{

    public static final String TAG = SignUpDialog.class.getName();

    public LandingActivity landingActivity;

    private TextWatcher tt = null;

    @BindView(R.id.nameET)                      EditText nameET;
    @BindView(R.id.displayNameET)               EditText displayNameET;
    @BindView(R.id.contactET)                   EditText contactET;
    @BindView(R.id.emailET)                     EditText emailET;
    @BindView(R.id.passwordET)                  EditText passwordET;
    @BindView(R.id.showPasswordBTN)             ImageView showPasswordBTN;
    @BindView(R.id.confirmPasswordET)           EditText confirmPasswordET;
    @BindView(R.id.showConfirmPasswordBTN)      ImageView showConfirmPasswordBTN;
    @BindView(R.id.signUpTV)                    TextView signUpTV;
    @BindView(R.id.termsBTN)                    TextView termsBTN;
    @BindView(R.id.showPasswordCHBX)            CheckBox showPasswordCHBX;
    @BindView(R.id.showPassword1CHBX)            CheckBox showPassword1CHBX;

    public static SignUpDialog newInstance(){
        SignUpDialog signUpDialog = new SignUpDialog();
        return signUpDialog;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.dialog_signup;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
        signUpTV.setOnClickListener(this);
        termsBTN.setOnClickListener(this);
        if (showPasswordCHBX.isChecked()){
            showPassword1CHBX.setChecked(true);
        }
        else {
            showPassword1CHBX.setChecked(false);
        }
        PasswordEditTextManager.addShowPassword(getContext(), passwordET, showPassword1CHBX);
        PasswordEditTextManager.addShowPassword(getContext(), confirmPasswordET, showPasswordCHBX);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.signUpTV:
                attemptSignUp();
                break;
            case R.id.termsBTN:
                openUrl("http://privacypolicy.technology/kpp.php");
                break;
        }
    }

    public void openUrl(String url){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    private void attemptSignUp(){
        SignUpRequest signUpRequest = new SignUpRequest(getContext());
        signUpRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Signing up...", false, false))
                .setDeviceRegID(landingActivity.getDeviceRegID())
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.NAME, nameET.getText().toString())
                .addParameters(Keys.server.key.USERNAME, displayNameET.getText().toString())
                .addParameters(Keys.server.key.CONTACT_NUMBER, contactET.getText().toString())
                .addParameters(Keys.server.key.EMAIL, emailET.getText().toString())
                .addParameters(Keys.server.key.PASSWORD, passwordET.getText().toString())
                .addParameters(Keys.server.key.PASSWORD_CONFIRMATION, confirmPasswordET.getText().toString())
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        Window window = getDialog().getWindow();

        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(0));
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(SignUpRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
            UserData.insert(userTransformer.userItem);
            UserData.insert(UserData.AUTHORIZATION, userTransformer.token);
            UserData.insert(UserData.FIRST_LOGIN, true);
            UserData.insert(UserData.SHOW_SHARE, true);
            UserData.insert(UserData.TOKEN_EXPIRED, false);
            UserData.insert(UserData.TOTAL_POST, userTransformer.userItem.statistics.data.total_wishlist);
            landingActivity.startMainActivity("home");
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.SUCCESS);
            landingActivity.facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT,FacebookCustomLoggerMessage.SIGN_UP);
        }else{
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.FAILED);
            if(userTransformer.hasRequirements()){
                ErrorResponseManger.first(nameET, userTransformer.requires.name);
                ErrorResponseManger.first(displayNameET, userTransformer.requires.username);
                ErrorResponseManger.first(contactET, userTransformer.requires.contact_number);
                ErrorResponseManger.first(emailET, userTransformer.requires.email);
                ErrorResponseManger.first(passwordET, userTransformer.requires.password);
                ErrorResponseManger.first(confirmPasswordET, userTransformer.requires.password);
            }
        }
    }
}
