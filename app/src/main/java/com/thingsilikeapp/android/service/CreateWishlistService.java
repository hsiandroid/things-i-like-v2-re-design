package com.thingsilikeapp.android.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;

import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.wishlist.CreateWishListRequest;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.android.activity.MainActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;

/**
 * Created by BCTI 3 on 6/6/2017.
 */

public class CreateWishlistService extends Service {

    private Context context;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mainService();
        PostItem postItem = intent.getParcelableExtra("post_item");

        if(postItem != null){
            showIndicator();
            attemptCreate(postItem);
            Log.e("CreateWishlistService", ">>>start create");
        }else{
            Log.e("CreateWishlistService", ">>>Failed no item to upload");
            closeIndicator();
            stopSelf();
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        closeIndicator();
        super.onDestroy();
    }

    private void mainService(){
        context = this;
    }

    private void showIndicator(){
        Intent mainIntent = new Intent(this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 1, mainIntent, 0);
        Notification notification  = new Notification.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Uploading...")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntent)
                .setAutoCancel(false)
//                .setOngoing(true)
                .build();


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(12, notification);

    }

    private void showFailedIndicator(){
        Intent mainIntent = new Intent(this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 1, mainIntent, 0);
        Notification notification  = new Notification.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Upload Failed")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntent)
                .setAutoCancel(false)
//                .setOngoing(true)
                .build();


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(12, notification);

    }

    private void closeIndicator(){
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(12);
    }

    private void attemptCreate(PostItem postItem){

        File file = new File(postItem.image);

        CreateWishListRequest createWishListRequest = new CreateWishListRequest(context);
        if(file != null && file.exists()){
            createWishListRequest.addMultipartBody(Keys.server.key.FILE, file);
        }

        createWishListRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addMultipartBody(Keys.server.key.INCLUDE, "info,image,owner,delivery_info")
                .addMultipartBody(Keys.server.key.TITLE, postItem.title)
                .addMultipartBody(Keys.server.key.CATEGORY, postItem.category)
                .addMultipartBody(Keys.server.key.CONTENT, postItem.content)
                .addMultipartBody(Keys.server.key.STREET_ADDRESS, postItem.streetAddress)
                .addMultipartBody(Keys.server.key.CITY, postItem.city)
                .addMultipartBody(Keys.server.key.STATE, postItem.state)
                .addMultipartBody(Keys.server.key.COUNTRY, postItem.country)
                .addMultipartBody(Keys.server.key.CONTACT_NUMBER, postItem.contactNumber)
                .addMultipartBody(Keys.server.key.URL, postItem.url)
                .addMultipartBody(Keys.server.key.URL_TITLE, postItem.urlTitle)
                .addMultipartBody(Keys.server.key.URL_DESCRIPTION, postItem.urlDescription)
                .addMultipartBody(Keys.server.key.URL_IMAGE, postItem.urlImage)
                .execute();
    }

    @Subscribe
    public void onResponse(CreateWishListRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if(wishListInfoTransformer.status){
            closeIndicator();
            stopSelf();
        }else{
            showFailedIndicator();
        }
    }

    public static class PostItem implements Parcelable {
        public String image = "";
        public String title = "";
        public String category = "";
        public String content = "";
        public String streetAddress = "";
        public String city = "";
        public String state = "";
        public String country = "";
        public String contactNumber = "";
        public String url = "";
        public String urlTitle = "";
        public String urlDescription = "";
        public String urlImage = "";

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.image);
            dest.writeString(this.title);
            dest.writeString(this.category);
            dest.writeString(this.content);
            dest.writeString(this.streetAddress);
            dest.writeString(this.city);
            dest.writeString(this.state);
            dest.writeString(this.country);
            dest.writeString(this.contactNumber);
            dest.writeString(this.url);
            dest.writeString(this.urlTitle);
            dest.writeString(this.urlDescription);
            dest.writeString(this.urlImage);
        }

        public PostItem() {
        }

        protected PostItem(Parcel in) {
            this.image = in.readString();
            this.title = in.readString();
            this.category = in.readString();
            this.content = in.readString();
            this.streetAddress = in.readString();
            this.city = in.readString();
            this.state = in.readString();
            this.country = in.readString();
            this.contactNumber = in.readString();
            this.url = in.readString();
            this.urlTitle = in.readString();
            this.urlDescription = in.readString();
            this.urlImage = in.readString();
        }

        public static final Parcelable.Creator<PostItem> CREATOR = new Parcelable.Creator<PostItem>() {
            @Override
            public PostItem createFromParcel(Parcel source) {
                return new PostItem(source);
            }

            @Override
            public PostItem[] newArray(int size) {
                return new PostItem[size];
            }
        };
    }

}
