package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.GreetingItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BirthdayGreetingsAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<GreetingItem> data;
    private LayoutInflater layoutInflater;

	public BirthdayGreetingsAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<GreetingItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public List<GreetingItem> getData() {
        return data;
    }

    public void reset(){
        List<GreetingItem> temp = new ArrayList<>();
        temp.addAll(data);
        this.data = new ArrayList<>();
        setNewData(temp);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public GreetingItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_birthday_greeting, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.greetingItem = data.get(position);
        holder.view.setOnClickListener(this);
        Glide.with(context)
                .load(holder.greetingItem.image.full_path)
                .apply(new RequestOptions()
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.previewIMG);

		return convertView;
	}

	public void reInsert(int id){
        data.add(data.get(id));
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.show(((ViewHolder)v.getTag()).greetingItem);
        }
    }


    public class ViewHolder{
        GreetingItem greetingItem;
        @BindView(R.id.previewIMG)      ImageView previewIMG;
        @BindView(R.id.progressBar)     View progressBar;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void show(GreetingItem greetingItem);
    }
} 
