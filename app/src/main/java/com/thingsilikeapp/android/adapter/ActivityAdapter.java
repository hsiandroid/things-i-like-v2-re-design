package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.NotificationItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<NotificationItem> data;
    private LayoutInflater layoutInflater;
    private int newDataCount;
    private Handler handler;
    private Runnable runnable;

	public ActivityAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(final List<NotificationItem> data){
        this.data = new ArrayList<>();
        notifyDataSetChanged();

        newDataCount = 0;

        handler = new Handler();
        if(runnable != null){
            handler.removeCallbacks(runnable);
        }

        runnable = new Runnable() {
            @Override
            public void run() {
                if(newDataCount < data.size()) {
                    ActivityAdapter.this.data.add(data.get(newDataCount));
                    newDataCount++;
                    notifyDataSetChanged();
                    handler.postDelayed(runnable,20);
                }else{
                    handler.removeCallbacks(runnable);
                }
            }
        };

        runnable.run();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_activity, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else
            {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.notificationItem = data.get(position);

        holder.view.setOnClickListener(this);

        holder.titleTXT.setText(holder.notificationItem.content);
        holder.dateTXT.setText(holder.notificationItem.time_passed);
//        if(holder.notificationItem.is_read.equals("yes")){
//            holder.indicatorCON.setVisibility(View.GONE);
//        }else{
//            holder.indicatorCON.setVisibility(View.VISIBLE);
//        }


        Glide.with(context)
                .load(holder.notificationItem.thumbnail)
                .into(holder.iLikeIV);

        Log.e("ActivityAdapter", ">>>" + holder.notificationItem.thumbnail);

		return convertView;
	}

    public class ViewHolder{
        NotificationItem notificationItem;

        @BindView(R.id.titleTXT)        TextView titleTXT;
        @BindView(R.id.dateTXT)         TextView dateTXT;
        @BindView(R.id.iLikeIV)         ImageView iLikeIV;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onItemClick(data.indexOf(viewHolder.notificationItem), v);
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
} 
