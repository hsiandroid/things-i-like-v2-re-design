package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.BitmapItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditPhotoListRecycleViewAdapters extends RecyclerView.Adapter<EditPhotoListRecycleViewAdapters.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<BitmapItem> data;
    private LayoutInflater layoutInflater;

	public EditPhotoListRecycleViewAdapters(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<BitmapItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<BitmapItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(BitmapItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(BitmapItem newData){
        for(BitmapItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    public void updateBitmap(BitmapItem bitmapItem){
        int pos = getData().indexOf(bitmapItem);
        if (pos >= 0){
            getData().set(pos, bitmapItem);
            notifyItemChanged(pos);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_photo_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.galleryItem = data.get(position);
        holder.view.setTag(holder.galleryItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);

        holder.adapterCON.setTag(holder.galleryItem);
        holder.adapterCON.setOnClickListener(this);

        holder.imageView.setImageBitmap(holder.galleryItem.bitmap);
    }

    public Bitmap getImage(){
        return data.get(0).bitmap;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        BitmapItem galleryItem;

        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.imageIV)             ImageView imageView;


        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((BitmapItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public BitmapItem getItem(int position){
        return getData().get(position);
    }

    public List<BitmapItem> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }

    public interface ClickListener {
        void onItemClick(BitmapItem galleryItem);
    }
} 
