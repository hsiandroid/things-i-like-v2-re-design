package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.BirthdayItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.vendor.android.java.decoration.HeaderItemDecoration;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BirthdayCelebrantRecycleViewAdapter extends RecyclerView.Adapter<BirthdayCelebrantRecycleViewAdapter.ViewHolder>
        implements View.OnClickListener,
        HeaderItemDecoration.StickyHeaderInterface {

    private Context context;
    private LayoutInflater layoutInflater;

    private List<ReferenceData> referenceData;
    private List<BirthdayItem> birthdayItems;

    public static final int HEADER = -1;
    public static final int SUB_HEADER = -2;
    public static final int SHOW_MORE = -3;

    public static final int VIEW_1 = -4;

    public BirthdayCelebrantRecycleViewAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.initAdapter();
    }

    private void initAdapter() {
        referenceData = new ArrayList<>();
        birthdayItems = new ArrayList<>();
    }

    public void setGroup(List<BirthdayItem> group) {
        this.birthdayItems = group;
    }

    public void build(List<BirthdayItem> group) {
        setGroup(group);
        build();
    }

    public void build() {
        referenceData.clear();
        for (BirthdayItem item : birthdayItems) {
            buildGroup(item);
        }
        notifyDataSetChanged();
    }

    public void updateUserItem(UserItem userItem) {

        for (ReferenceData refData : referenceData) {
            if (refData.viewType == VIEW_1 && refData.userItem.id == userItem.id) {
                refData.userItem = userItem;
                notifyItemChanged(referenceData.indexOf(refData));
                break;
            }
        }

        for (BirthdayItem birthdayItem : birthdayItems) {
            for (UserItem item : birthdayItem.users.data) {
                if (userItem.id == item.id) {
                    birthdayItem.users.data.set(birthdayItem.users.data.indexOf(item), userItem);
                    return;
                }
            }
        }
    }

    private void buildGroup(BirthdayItem item) {
        if (item.users.data.size() > 0) {
            referenceData.add(new ReferenceData(HEADER, item.header));

            for (int i = 0; i < item.users.data.size(); i++) {
                UserItem currentUser = item.users.data.get(i);
                boolean isToday = item.header.toLowerCase().contains("today");
                if (!isToday) {
                    if (i == 0) {
                        referenceData.add(new ReferenceData(SUB_HEADER, item.users.data.get(i).bday_display));
                    } else {
                        UserItem beforeUser = item.users.data.get(i - 1);
                        if (!currentUser.birthdate.equals(beforeUser.birthdate)) {
                            referenceData.add(new ReferenceData(SUB_HEADER, item.users.data.get(i).bday_display));
                        }
                    }
                }

                referenceData.add(new ReferenceData(VIEW_1, String.valueOf(currentUser.id), isToday, currentUser));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return referenceData.get(position).viewType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER:
                return new ViewHolder(layoutInflater.inflate(R.layout.adapter_celebrant_header, parent, false)).bindHeader();
            case SUB_HEADER:
                return new ViewHolder(layoutInflater.inflate(R.layout.adapter_celebrant_subheader, parent, false)).bindSubHeader();
            case SHOW_MORE:
                return new ViewHolder(layoutInflater.inflate(R.layout.adapter_celebrant_show_more, parent, false)).bindShowMore();
            case VIEW_1:
                return new ViewHolder(layoutInflater.inflate(R.layout.adapter_celebrant_view_1, parent, false)).bindView1();
            default:
                return new ViewHolder(layoutInflater.inflate(R.layout.adapter_template, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.referenceData = referenceData.get(position);
        switch (holder.referenceData.viewType) {
            case HEADER:
                headerBinder(holder);
                break;
            case SUB_HEADER:
                subHeaderBinder(holder);
                break;
            case SHOW_MORE:
                moreBinder(holder);
                break;
            case VIEW_1:
                view1Binder(holder);
                break;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public int getItemCount() {
        return referenceData.size();
    }

    private void headerBinder(ViewHolder holder) {
        holder.headerView.headerTXT.setText(holder.referenceData.reference);
    }

    private void subHeaderBinder(ViewHolder holder) {
        holder.subHeaderView.headerTXT.setText(holder.referenceData.reference);
    }

    private void moreBinder(ViewHolder holder) {

    }

    private void view1Binder(ViewHolder holder) {

        Glide.with(context)
                .load(holder.referenceData.userItem.getAvatar())
                .apply(new RequestOptions()
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.view1.imageCIV);

        holder.view1.nameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.referenceData.userItem.name, holder.referenceData.userItem.is_verified, 12), TextView.BufferType.SPANNABLE);
        holder.view1.commonNameTXT.setText(holder.referenceData.userItem.common_name);

        if (StringFormatter.isEmpty(holder.referenceData.userItem.social.data.group)) {
            holder.view1.groupTXT.setVisibility(View.GONE);
        } else {
            holder.view1.groupTXT.setVisibility(View.VISIBLE);
            holder.view1.groupTXT.setText(holder.referenceData.userItem.social.data.group.toUpperCase());
        }

        if (holder.referenceData.userItem.allow_greeting) {
            if (holder.referenceData.isToday) {
                if (holder.referenceData.userItem.is_greeted) {
                    setUpGreetButton(
                            holder.view1.greetTXT,
                            "GREET NOW!",
                            R.drawable.bg_follow_gray,
                            R.color.text_gray,
                            0);
                    holder.view1.checkIMG.setVisibility(View.VISIBLE);
                } else {
                    setUpGreetButton(
                            holder.view1.greetTXT,
                            "GREET NOW!",
                            R.drawable.bg_follow,
                            R.color.white,
                            0);
                    holder.view1.checkIMG.setVisibility(View.GONE);
                }
            } else {
                if (holder.referenceData.userItem.is_greeted) {
                    setUpGreetButton(
                            holder.view1.greetTXT,
                            "GREET!",
                            R.drawable.bg_follow_gray,
                            R.color.text_gray,
                            R.drawable.icon_clock_gray);
                    holder.view1.checkIMG.setVisibility(View.VISIBLE);
                } else {
                    setUpGreetButton(
                            holder.view1.greetTXT,
                            "GREET!",
                            R.drawable.bg_unfollow,
                            R.color.colorPrimary,
                            R.drawable.icon_clock);
                    holder.view1.checkIMG.setVisibility(View.GONE);
                }
            }
        } else {
            setUpGreetButton(
                    holder.view1.greetTXT,
                    "REMIND ME",
                    R.drawable.bg_follow_gray,
                    R.color.text_gray,
                    0);
            if (holder.referenceData.userItem.social.data.bday_reminder_id > 0) {
                holder.view1.checkIMG.setVisibility(View.VISIBLE);
            } else {
                holder.view1.checkIMG.setVisibility(View.GONE);
            }
        }

        holder.view1.greetTXT.setTag(holder.referenceData);
        holder.view1.greetTXT.setOnClickListener(this);

        holder.view1.imageCIV.setTag(R.id.imageCIV, holder.referenceData.userItem);
        holder.view1.imageCIV.setOnClickListener(this);
        holder.view1.nameTXT.setTag(holder.referenceData.userItem);
        holder.view1.nameTXT.setOnClickListener(this);
        holder.view1.commonNameTXT.setTag(holder.referenceData.userItem);
        holder.view1.commonNameTXT.setOnClickListener(this);
        holder.view1.otherOptionBTN.setTag(holder.referenceData);
        holder.view1.otherOptionBTN.setOnClickListener(this);
    }

    private void setUpGreetButton(TextView greetTXT, String text, int background, int textColor, int drawable) {
        greetTXT.setText(text);
        greetTXT.setBackground(ActivityCompat.getDrawable(context, background));
        greetTXT.setTextColor(ActivityCompat.getColor(context, textColor));
        greetTXT.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        int headerPosition = 0;
        do {
            if (this.isHeader(itemPosition)) {
                headerPosition = itemPosition;
                break;
            }
            itemPosition -= 1;
        } while (itemPosition >= 0);
        return headerPosition;
    }

    @Override
    public int getHeaderLayout(int headerPosition) {
        return R.layout.adapter_celebrant_header;
    }

    @Override
    public void bindHeaderData(View header, int headerPosition) {
        TextView headerTXT = header.findViewById(R.id.headerTXT);
        headerTXT.setText(referenceData.get(headerPosition).reference);
    }

    @Override
    public boolean isHeader(int itemPosition) {
        return referenceData.get(itemPosition).viewType == HEADER;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ReferenceData referenceData;

        HeaderView headerView;
        SubHeaderView subHeaderView;
        ShowMoreView showMoreView;
        View1 view1;

        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }

        public ViewHolder bindHeader() {
            headerView = new HeaderView();
            return this;
        }

        public ViewHolder bindSubHeader() {
            subHeaderView = new SubHeaderView();
            return this;
        }

        public ViewHolder bindShowMore() {
            showMoreView = new ShowMoreView();
            return this;
        }

        public ViewHolder bindView1() {
            view1 = new View1();
            return this;
        }

        public class HeaderView {

            @BindView(R.id.headerTXT)
            TextView headerTXT;

            public HeaderView() {
                ButterKnife.bind(this, view);
            }
        }

        public class SubHeaderView {

            @BindView(R.id.headerTXT)
            TextView headerTXT;

            public SubHeaderView() {
                ButterKnife.bind(this, view);
            }
        }

        public class ShowMoreView {

            public ShowMoreView() {
                ButterKnife.bind(this, view);
            }
        }

        public class View1 {
            @BindView(R.id.imageCIV)
            ImageView imageCIV;
            @BindView(R.id.nameTXT)
            TextView nameTXT;
            @BindView(R.id.commonNameTXT)
            TextView commonNameTXT;
            @BindView(R.id.groupTXT)
            TextView groupTXT;
            @BindView(R.id.greetTXT)
            TextView greetTXT;
            @BindView(R.id.checkIMG)
            View checkIMG;
            @BindView(R.id.otherOptionBTN)
            View otherOptionBTN;

            public View1() {
                ButterKnife.bind(this, view);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.greetTXT:
                if (clickListener != null) {
                    ReferenceData greetRef = (ReferenceData) v.getTag();
                    if (greetRef.userItem.allow_greeting) {
                        clickListener.onGreetClick(greetRef.userItem, greetRef.isToday);
                    } else {
                        clickListener.onRemindClick(greetRef.userItem);
                    }
                }
                break;
            case R.id.imageCIV:
                if (clickListener != null) {
                    clickListener.onProfileClick((UserItem) v.getTag(R.id.imageCIV));
                }
                break;
            case R.id.nameTXT:
            case R.id.commonNameTXT:
                if (clickListener != null) {
                    clickListener.onProfileClick((UserItem) v.getTag());
                }
                break;
            case R.id.otherOptionBTN:
                otherOptionClick(v);
                break;
        }
    }

    private void otherOptionClick(View view) {
        final ReferenceData referenceData = (ReferenceData) view.getTag();
        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.greet_menu, popup.getMenu());
        popup.getMenu().findItem(R.id.sendBTN).setVisible(referenceData.userItem.allow_greeting);
        popup.getMenu().findItem(R.id.remindBTN).setVisible(referenceData.userItem.allow_greeting && !referenceData.isToday);
        popup.getMenu().findItem(R.id.profileBTN).setVisible(!referenceData.userItem.allow_greeting && !referenceData.isToday);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.sendBTN:
                        if (clickListener != null) {
                            clickListener.onProfileClick(referenceData.userItem);
                        }
                        break;
                    case R.id.remindBTN:
                        if (clickListener != null) {
                            clickListener.onRemindClick(referenceData.userItem);
                        }
                        break;
                    case R.id.profileBTN:
                        if (clickListener != null) {
                            clickListener.onProfileClick(referenceData.userItem);
                        }
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(BirthdayItem birthdayItem);

        void onGreetClick(UserItem userItem, boolean isToday);

        void onProfileClick(UserItem userItem);

        void onSendGiftClick(UserItem userItem);

        void onRemindClick(UserItem userItem);
    }

    public class ReferenceData {

        public int viewType;
        public String reference;
        public boolean isToday;

        public ReferenceData(int viewType, String reference) {
            this.viewType = viewType;
            this.reference = reference;
        }

        public ReferenceData(int viewType, String reference, boolean isToday, UserItem userItem) {
            this.viewType = viewType;
            this.reference = reference;
            this.isToday = isToday;
            this.userItem = userItem;
        }

        public UserItem userItem;
    }
} 
