package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.BankCardItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardRecycleViewAdapter extends RecyclerView.Adapter<CardRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<BankCardItem> data;
    private LayoutInflater layoutInflater;

	public CardRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<BankCardItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<BankCardItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(BankCardItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(BankCardItem newData){
        for(BankCardItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_card_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bankCardItem = data.get(position);
        holder.view.setTag(holder.bankCardItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.adapterCON.setTag(holder.bankCardItem);
        holder.adapterCON.setOnClickListener(this);

        holder.lastDigitTXT.setText(holder.bankCardItem.cardNumber);
        switch (holder.bankCardItem.cardType) {
            case "visa":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_visa);
                break;
            case "diners":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_diners_club);
                break;
            case "discover":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_discover);
                break;
            case "jcb":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_jcb);
                break;
            case "mastercard":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_master_card);
                break;
            case "amex":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_amex);
                break;

        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        BankCardItem bankCardItem;

        @BindView(R.id.masterCardIV)        ImageView masterCardIV;
        @BindView(R.id.lastDigitTXT)        TextView lastDigitTXT;
        @BindView(R.id.adapterCON)          View adapterCON;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((BankCardItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick((BankCardItem) v.getTag());
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(BankCardItem bankCardItem);
        void onItemLongClick(BankCardItem bankCardItem);
    }
}