package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.PrivacyItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SpinnerAdapter extends ArrayAdapter {
	private Context context;
	private List<PrivacyItem> data;
    private LayoutInflater layoutInflater;

	public SpinnerAdapter(Context context) {
        super(context, R.layout.adapter_spinner);
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<PrivacyItem> data){
        this.data = data;
        notifyDataSetChanged();

    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.adapter_spinner, parent, false);

        TextView spinner = convertView.findViewById(R.id.spinnerTXT);
        spinner.setText(data.get(position).name);
        spinner.setTextSize(14);
        return spinner;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_spinner, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.urgencyLevel = data.get(position);
        holder.spinnerTXT.setText(holder.urgencyLevel.name);
        holder.spinnerTXT.setTextColor(ActivityCompat.getColor(getContext(),R.color.text_gray));
        holder.spinnerTXT.setTextSize(14);
		return convertView;
	}

	public class ViewHolder{
        PrivacyItem urgencyLevel;

        @BindView(R.id.spinnerTXT)  TextView spinnerTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}
} 
