package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.GroupItem;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.StringFormatter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<GroupItem> data;
    private LayoutInflater layoutInflater;

	public GroupAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<GroupItem> data){
        GroupItem groupItem = new GroupItem();
        groupItem.title = "NONE";
        data.add(0, groupItem);

        groupItem = new GroupItem();
        groupItem.title = "CUSTOM";
        data.add(groupItem);

        this.data = data;
        notifyDataSetChanged();
    }

    public void reset(){
        this.data = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setItemSelected(String title){
        if (StringFormatter.isEmpty(title) && data.size() > 0) {
            title = data.get(0).title;
        }

        for(GroupItem groupItem : data){
            groupItem.selected = groupItem.title.equalsIgnoreCase(title);
        }
        notifyDataSetChanged();
    }

    public String getSelectedItemName(){
        for(GroupItem groupItem : data){
            if(groupItem.selected){
                if(groupItem.title.equalsIgnoreCase("custom")){
                    if(groupItem.other.equals("")){
                        return groupItem.title;
                    }
                    return groupItem.other;
                }
                if(groupItem.title.equalsIgnoreCase("none")){
                    return "";
                }
                return groupItem.title;
            }
        }
        return "";
    }

    @Override
    public int getCount() {
        return data.size();
    }

    public Object getItemData(int position){
        return data.get(position);
    }
    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_group, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.groupItem = data.get(position);
        holder.view.setOnClickListener(this);
        holder.groupRDBTN.setOnClickListener(this);
        holder.groupRDBTN.setTag(holder);
        holder.groupRDBTN.setChecked(holder.groupItem.selected);

        holder.nameTXT.setText(holder.groupItem.title);

        if(holder.groupItem.title.equalsIgnoreCase("custom")){
            holder.othersET.setVisibility(View.VISIBLE);
            holder.othersET.setText(holder.groupItem.other);
            holder.othersET.setEnabled(holder.groupItem.selected);
            holder.othersET.requestFocus();
            holder.othersET.setTag(position);
            editTextSetup(holder.othersET);
        }else{
            holder.othersET.setVisibility(View.GONE);
        }

		return convertView;
	}

	private void editTextSetup(final EditText editText){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int pos = (int) editText.getTag();
                GroupItem groupItem = data.get(pos);
                groupItem.other = editable.toString();
                data.set(pos, groupItem);
            }
        });
    }

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        setItemSelected(viewHolder.groupItem.title);
        if(clickListener != null){
            clickListener.onGroupItemClick(viewHolder.groupItem.title);
        }
    }

    public class ViewHolder{
        GroupItem groupItem;

        @BindView(R.id.groupRDBTN)           RadioButton groupRDBTN;
        @BindView(R.id.nameTXT)              TextView nameTXT;
        @BindView(R.id.othersET)             EditText othersET;
        @BindView(R.id.adapterCON)           View adapterCON;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onGroupItemClick(String title);
    }
} 
