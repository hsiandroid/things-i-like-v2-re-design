package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.NotificationSettingsItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationSettingsRecycleViewAdapter extends RecyclerView.Adapter<NotificationSettingsRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener,
        View.OnLongClickListener {
	private Context context;
	private List<NotificationSettingsItem> data;
    private LayoutInflater layoutInflater;
    private boolean multipleSelect;
    private boolean selectAll;

	public NotificationSettingsRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<NotificationSettingsItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void setItemSelected(List<NotificationSettingsItem> notificationSettingsItems, int position, boolean selected){
        this.refresh(notificationSettingsItems);
        for(NotificationSettingsItem notificationSettingsItem :  data){
            if (notificationSettingsItem.id == data.get(position).id){
                data.get(position).selected = selected;
            }
        }
        notifyItemChanged(position);
    }

    public void addNewData(List<NotificationSettingsItem> data){
        for(NotificationSettingsItem notificationSettingsItem :  data){
            this.data.add(notificationSettingsItem);
        }
        notifyDataSetChanged();
    }

    public void reset(){
        this.data = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void refresh(List<NotificationSettingsItem> notificationSettingsItem){
        this.data = notificationSettingsItem;
        notifyDataSetChanged();
    }

    public void removeData(NotificationSettingsItem item){
        for(NotificationSettingsItem notificationSettingsItem : data){
            if(item.id.equals(notificationSettingsItem.id)){
                data.remove(notificationSettingsItem);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void removeData(String id){
        for(NotificationSettingsItem notificationSettingsItem : data){
            if(id.equals(notificationSettingsItem.id)){
                data.remove(notificationSettingsItem);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public List<NotificationSettingsItem> getData(){
        return data;
    }

    public boolean isSelectAll() {
        return selectAll;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_notification_settings, parent, false));
    }

    public NotificationSettingsItem getItem(int position){
        return data.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.notificationSettingsItem = data.get(position);

        holder.view.setOnClickListener(this);
        holder.addressBookRDBTN.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);

        holder.view.setTag(position);
        holder.addressBookRDBTN.setTag(position);
        holder.addressBookRDBTN.setChecked(holder.notificationSettingsItem.selected);

        holder.addressBookNameTXT.setText(holder.notificationSettingsItem.name);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        NotificationSettingsItem notificationSettingsItem;

        @BindView(R.id.addressBookRDBTN)               RadioButton addressBookRDBTN;
        @BindView(R.id.addressBookNameTXT)             TextView addressBookNameTXT;

        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            defaultConfig();
        }

        private void defaultConfig(){

        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addressBookRDBTN:
                if(clickListener != null){
                    clickListener.onItemClick((int)v.getTag(), v);
                }
                break;
        }
        if(clickListener != null){
        clickListener.onItemClick((int)v.getTag(), v);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick((int)v.getTag(), v);
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
} 
