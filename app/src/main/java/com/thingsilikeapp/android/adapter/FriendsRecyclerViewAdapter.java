package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.FriendsItem;
import com.thingsilikeapp.data.model.UserItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FriendsRecyclerViewAdapter extends RecyclerView.Adapter<FriendsRecyclerViewAdapter.ViewHolder>  implements View.OnClickListener{
	private Context context;
	private List<FriendsItem> data;
    private LayoutInflater layoutInflater;

	public FriendsRecyclerViewAdapter(Context context, ArrayList<FriendsItem> friendsItems, int i) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<FriendsItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_friends, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.thingsILikeItem = data.get(position);

        holder.view.setTag(position);
        holder.view.setOnClickListener(this);
        holder.imageCIV.setTag(position);
        holder.imageCIV.setOnClickListener(this);

        holder.nameTXT.setText(holder.thingsILikeItem.name);
        Glide.with(context)
                .load(holder.thingsILikeItem.full_path)
                .into(holder.imageCIV);
    }

    public Object getItemData(int position){
        return data.get(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        FriendsItem thingsILikeItem;

        @BindView(R.id.imageCIV)     ImageView imageCIV;
        @BindView(R.id.nameTXT)    TextView nameTXT;
        @BindView(R.id.commandTXT)    TextView commandTXT;
        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.imageCIV:
//                if(clickListener != null){
//                    clickListener.onAvatarClick(data.get((int)v.getTag()).owner);
//                }
//                break;
//            default:
//                if(clickListener != null){
//                    clickListener.onItemClick((int)v.getTag(), v);
//                }
//        }
    }
    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onAvatarClick(UserItem userItem);
    }
} 
