package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.model.CategoryItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryOwnedRecycleViewAdapter extends RecyclerView.Adapter<CategoryOwnedRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener{
	private Context context;
	private List<CategoryItem> data;
    private LayoutInflater layoutInflater;

	public CategoryOwnedRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<CategoryItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<CategoryItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(CategoryItem commentItem :  data){
            if(isDataUnique(commentItem)){
                this.data.add(commentItem);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }


    private boolean isDataUnique(CategoryItem newData) {
        return !data.contains(newData);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_exp_trending_c, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.categoryItem = data.get(position);
        holder.view.setTag(holder.categoryItem);
        holder.adapterCON.setTag(holder.categoryItem);
        holder.adapterCON.setOnClickListener(this);
        holder.view.setOnClickListener(this);
//        holder.view.setOnLongClickListener(this);
        holder.categoryTXT.setText(holder.categoryItem.title);

        Glide.with(context).load(holder.categoryItem.image).apply(new RequestOptions().centerCrop()).into(holder.photosIV);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        CategoryItem categoryItem;

        @BindView(R.id.photosIV)           ImageView photosIV;
        @BindView(R.id.categoryTXT)        TextView categoryTXT;
        @BindView(R.id.adapterCON)         View adapterCON;



        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((CategoryItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(CategoryItem categoryItem);
        void onItemLongClick(CategoryItem categoryItem);
    }
} 
