package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.ContactsItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactsRecycleViewAdapter extends RecyclerView.Adapter<ContactsRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener,
        View.OnLongClickListener,
        Filterable {

    private Context context;
    private List<ContactsItem> data;
    private List<ContactsItem> originalData;
    private LayoutInflater layoutInflater;
    private ValueFilter valueFilter;
    private boolean multipleSelect;
    private boolean selectAll;

    public ContactsRecycleViewAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    public void refreshData(List<ContactsItem> contactsItems){
        this.data = contactsItems;
        notifyDataSetChanged();
    }

    public void setNewData(List<ContactsItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<ContactsItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(ContactsItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(ContactsItem newData){
        for(ContactsItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    public void reset(){
        this.data = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void removeItem(ContactsItem data) {
        for (ContactsItem item : this.data) {
            if (data.id == (item.id)) {
                int pos = this.data.indexOf(item);
                this.data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    public void updateData(ContactsItem data){
        for(ContactsItem item : this.data){
            if(data.id == item.id){
                int pos = this.data.indexOf(item);
                this.data.set(pos, data);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    public List<ContactsItem> getData(){
        return data;
    }

    public void setMultipleSelect(boolean multipleSelect){
        this.multipleSelect = multipleSelect;
        unSelectAll();
        notifyDataSetChanged();
    }

    public void setItemSelected(int position, boolean selected){
        data.get(position).selected = selected;
        notifyItemChanged(position);
    }

    public void selectAll(){
        selectAll = true;
        for(ContactsItem contactItem : data){
            contactItem.selected = selectAll;
        }
        notifyDataSetChanged();
    }

    public void unSelectAll(){
        selectAll = false;
        for(ContactsItem contactItem : data){
            contactItem.selected = selectAll;
        }
        notifyDataSetChanged();
    }

    public String getPhoneNumber(){
        String phoneNumber = "";
        for(ContactsItem contactItem : data){
            if(contactItem.selected){
                phoneNumber = phoneNumber + contactItem.number + ";";
            }
        }
        return phoneNumber;
    }

    public boolean isMultipleSelect() {
        return multipleSelect;
    }

    public boolean isSelectAll() {
        return selectAll;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_contacts, parent, false));
    }

    public ContactsItem getItem(int position){
        return data.get(position);
    }

    public int getSelectedCount(){
        int count = 0;
        for(ContactsItem contactItem : data){
            if(contactItem.selected){
                count++;
            }
        }
        return count;
    }

    public boolean hasCount(){
        boolean valid = false;
        for(ContactsItem contactItem : data){
            if(contactItem.selected){
                return true;
            }
        }
        return valid;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.contactsItem = data.get(position);

        holder.view.setOnClickListener(this);
        holder.view.setTag(position);

        if(multipleSelect){
            holder.multipleSelectRB.setTag(position);
            holder.multipleSelectRB.setVisibility(View.VISIBLE);
            holder.multipleSelectRB.setOnClickListener(this);
            holder.multipleSelectRB.setChecked(holder.contactsItem.selected);
            holder.view.setOnLongClickListener(null);

            holder.commandTXT.setVisibility(View.GONE);
            holder.commandTXT.setOnClickListener(null);
        }else{
            holder.multipleSelectRB.setVisibility(View.GONE);
            holder.view.setOnLongClickListener(this);

            holder.commandTXT.setVisibility(View.VISIBLE);
            holder.commandTXT.setTag(data.get(position));
            holder.commandTXT.setOnClickListener(this);
        }

        holder.numberTXT.setText(holder.contactsItem.number);
        holder.nameTXT.setText(holder.contactsItem.name);
        Glide.with(context)
                .load(holder.contactsItem.image)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true))
                .into(holder.imageCIV);
    }

    @Override
    public int getItemCount() {
        if (data != null) {
            return data.size();
        }else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ContactsItem contactsItem;

        @BindView(R.id.imageCIV)           ImageView imageCIV;
        @BindView(R.id.nameTXT)            TextView nameTXT;
        @BindView(R.id.numberTXT)          TextView numberTXT;
        @BindView(R.id.commandTXT)         View commandTXT;
        @BindView(R.id.multipleSelectRB)   RadioButton multipleSelectRB;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.multipleSelectRB:
            case R.id.activityCON:
                if(multipleSelect){
                    int pos = (int) v.getTag();
                    data.get(pos).selected = !data.get(pos).selected;

                    if (clickListener != null){
                        clickListener.onItemSelect(pos);
                    }

                    notifyItemChanged(pos);
                }
                break;
            case R.id.commandTXT:
                if (clickListener != null){
                    clickListener.onInviteClick((ContactsItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick((int)v.getTag());
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onInviteClick(ContactsItem contactsItem);
        void onItemLongClick(int position);
        void onItemSelect(int position);
    }


    @Override
    public Filter getFilter() {
        if(valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }


    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (originalData == null){
                originalData = data;
            }

            FilterResults results = new FilterResults();
            if(constraint == null && constraint.length() == 0){
                results.count = originalData.size();
                results.values = originalData;
            }else{
                ArrayList<ContactsItem> filterList = new ArrayList<>();
                for(int i=0; i < originalData.size();i++){
                    ContactsItem categoryItem = originalData.get(i);
                    if((categoryItem.name.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(categoryItem);
                    }else if((categoryItem.number.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(categoryItem);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data = (List<ContactsItem>) results.values;
            notifyDataSetChanged();
        }
    }
} 
