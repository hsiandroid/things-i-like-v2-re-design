package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.GetRateItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrencyRecycleViewAdapter extends RecyclerView.Adapter<CurrencyRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<GetRateItem> data;
    private LayoutInflater layoutInflater;

	public CurrencyRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<GetRateItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<GetRateItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(GetRateItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(GetRateItem newData){
        for(GetRateItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.custom_spinner_items, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.currencyItem = data.get(position);
        holder.view.setTag(holder.currencyItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.adapterCON.setTag(holder.currencyItem);
        holder.adapterCON.setOnClickListener(this);
        holder.textView.setText(holder.currencyItem.countryName + " (" + holder.currencyItem.currency + ")");

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        GetRateItem currencyItem;

        @BindView(R.id.imageView)           ImageView imageView;
        @BindView(R.id.textView)            TextView textView;
        @BindView(R.id.adapterCON)          View adapterCON;


        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((GetRateItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public GetRateItem getItem(int position){
        return getData().get(position);
    }

    public List<GetRateItem> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }



    public interface ClickListener {
        void onItemClick(GetRateItem currencyItem);
        void onItemLongClick(GetRateItem currencyItem);

    }
} 
