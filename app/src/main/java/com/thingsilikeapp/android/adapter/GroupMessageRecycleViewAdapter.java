package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.ChatModel;
import com.thingsilikeapp.data.model.ChatModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupMessageRecycleViewAdapter extends RecyclerView.Adapter<GroupMessageRecycleViewAdapter.ViewHolder>  implements View.OnClickListener {
	private Context context;
	private List<ChatModel> data;
    private LayoutInflater layoutInflater;

	public GroupMessageRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public List<ChatModel> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }

    public void addNewData(List<ChatModel> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(ChatModel defaultItem : data){
            if(isDataUnique(defaultItem)){
                this.data.add(defaultItem);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(ChatModel item){
        for(ChatModel defaultItem : this.data){
            if(item.id == defaultItem.id){
                return false;
            }
        }
        return true;
    }

    public void setNewData(List<ChatModel> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_message, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.chatItem = data.get(position);

        holder.view.setTag(position);
        holder.view.setOnClickListener(this);
        holder.adapterCON.setTag(holder.chatItem);
        holder.adapterCON.setOnClickListener(this);

            holder.nameTXT.setText(holder.chatItem.title);
            holder.timeTXT.setText(holder.chatItem.latestMessage.data.info.data.dateCreated.timePassed);
            holder.messageTXT.setText(holder.chatItem.latestMessage.data.content);

            if (holder.chatItem.info.data.avatar.fullPath.equalsIgnoreCase("http://demo.mylyka.com/placeholder/group.jpg")) {

                String word = holder.chatItem.title;
                String letter = word.substring(0, 1);
//                holder.letterTXT.setText(letter.toUpperCase());

                switch (letter.toUpperCase()) {
                    case "A":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.A)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("A");
                        break;
                    case "B":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.B)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("B");
                        break;
                    case "C":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.C)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("C");
                        break;
                    case "D":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.D)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("D");
                        break;
                    case "E":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.E)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("E");
                        break;
                    case "F":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.F)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("F");
                        break;
                    case "G":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.G)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("G");
                        break;
                    case "H":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.H)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("H");
                        break;
                    case "I":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.I)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("I");
                        break;
                    case "J":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.J)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("J");
                        break;
                    case "K":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.K)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("K");
                        break;
                    case "L":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.L)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("L");
                        break;
                    case "M":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.M)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("M");
                        break;
                    case "N":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.N)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("N");
                        break;
                    case "O":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.O)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("O");
                        break;
                    case "P":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.P)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("P");
                        break;
                    case "Q":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.Q)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("Q");
                        break;
                    case "R":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.R)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("R");
                        break;
                    case "S":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.S)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("S");
                        break;
                    case "T":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.T)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("T");
                        break;
                    case "U":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.U)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("U");
                        break;
                    case "V":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.V)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("V");
                        break;
                    case "W":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.W)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("W");
                        break;
                    case "X":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.X)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("X");
                        break;
                    case "Y":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.Y)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("Y");
                        break;
                    case "Z":
                        Picasso.with(context)
                                .load(holder.chatItem.info.data.avatar.fullPath)
                                .placeholder(R.color.Z)
                                .into(holder.profileCIV);
                        holder.letterTXT.setText("Z");
                        break;

                }
            }else{
                Picasso.with(context)
                    .load(holder.chatItem.info.data.avatar.fullPath)
//                    .placeholder(R.drawable.placeholder_avatar)
                    .into(holder.profileCIV);
                holder.letterTXT.setVisibility(View.GONE);
            }

//        holder.messageTXT.setText(holder.chatItem.latestMessage.data.content);
//        holder.timeTXT.setText(holder.chatItem.latestMessage.data.info.data.dateCreated.timePassed);
//        holder.onlineBTN.setBackground(holder.chatItem.is_online ? ActivityCompat.getDrawable(getContext(), R.drawable.bg_circle_online) : ActivityCompat.getDrawable(getContext(), R.drawable.bg_circle_offline));

//        holder.titleTXT.setText(holder.chatItem.title);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ChatModel chatItem;

        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.messageTXT)          TextView messageTXT;
        @BindView(R.id.timeTXT)             TextView timeTXT;
        @BindView(R.id.profileCIV)          ImageView profileCIV;
        @BindView(R.id.onlineBTN)           View onlineBTN;
//        @BindView(R.id.letterCON)           ImageView letterCON;
        @BindView(R.id.letterTXT)           TextView letterTXT;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((ChatModel) v.getTag());
                }
                break;
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(ChatModel chatItem);
    }
} 
