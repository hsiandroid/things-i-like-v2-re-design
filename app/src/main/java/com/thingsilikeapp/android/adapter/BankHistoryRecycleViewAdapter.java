package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.EncashHistoryItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BankHistoryRecycleViewAdapter extends RecyclerView.Adapter<BankHistoryRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<EncashHistoryItem> data;
    private LayoutInflater layoutInflater;

	public BankHistoryRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<EncashHistoryItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<EncashHistoryItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(EncashHistoryItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(EncashHistoryItem newData){
        for(EncashHistoryItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_encashment_history, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.encashHistoryItem = data.get(position);
        holder.view.setTag(holder.encashHistoryItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);

        holder.adapterCON.setTag(holder.encashHistoryItem);
        holder.adapterCON.setOnClickListener(this);

        holder.refTXT.setText("Ref.no. " +holder.encashHistoryItem.refno);
        holder.statusTXT.setText(holder.encashHistoryItem.status);
        holder.reamarksTXT.setText(holder.encashHistoryItem.remarks);
        holder.dateTXT.setText(holder.encashHistoryItem.requestDate);
        holder.gemTXT.setText(holder.encashHistoryItem.gemQty);
        holder.finalAmountTXT.setText( holder.encashHistoryItem.currency + " " + holder.encashHistoryItem.finalAmount);

        switch (holder.encashHistoryItem.status){
            case "close":
                holder.statusTXT.setBackgroundResource(R.color.gray);
                break;
            case "completed":
            case "approved":
                holder.statusTXT.setBackgroundResource(R.color.completed);
                break;
            case "decline":
            case "cancelled":
                holder.statusTXT.setBackgroundResource(R.color.failed);
                break;
            case "pending":
                holder.statusTXT.setBackgroundResource(R.color.gold);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        EncashHistoryItem encashHistoryItem;

        @BindView(R.id.refTXT)              TextView refTXT;
        @BindView(R.id.statusTXT)           TextView statusTXT;
        @BindView(R.id.remarksTXT)          TextView reamarksTXT;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.gemTXT)              TextView gemTXT;
        @BindView(R.id.finalAmountTXT)      TextView finalAmountTXT;
        @BindView(R.id.adapterCON)          View adapterCON;


        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((EncashHistoryItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public EncashHistoryItem getItem(int position){
        return getData().get(position);
    }

    public List<EncashHistoryItem> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }


    public interface ClickListener {
        void onItemClick(EncashHistoryItem encashHistoryItem);
        void onItemLongClick(EncashHistoryItem encashHistoryItem);
        void onEditClick(EncashHistoryItem encashHistoryItem);
        void onRemoveClick(EncashHistoryItem encashHistoryItem);
    }
} 
