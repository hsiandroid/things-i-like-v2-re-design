package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.java.NumberFormatter;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;
import com.thingsilikeapp.vendor.android.widget.image.FixedWidthImageView;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RelatedTempRecycleViewAdapter extends RecyclerView.Adapter<RelatedTempRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener{

	private Context context;
	private List<WishListItem> data;
    private LayoutInflater layoutInflater;
    private EventItem eventItem;
    private int userID = 0;

	public RelatedTempRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        userID = UserData.getUserId();
	}

    public void setNewData(List<WishListItem> data) {
        this.data.clear();
        for (WishListItem wishListItem : data) {
            if (isDataUnique(wishListItem) && wishListItem.id != -1) {
                this.data.add(wishListItem.minimal(userID));
            }
        }
        notifyDataSetChanged();
    }

    public void addNewData(List<WishListItem> data) {
        int initialCount = this.data.size();
        int newCount = 0;
        for (WishListItem wishListItem : data) {
            if (isDataUnique(wishListItem)) {
                this.data.add(wishListItem.minimal(userID));
                newCount++;
            }
        }
        notifyItemRangeInserted(initialCount, newCount);
    }

    public void removeItem(WishListItem item) {
        for (WishListItem wishListItem : data) {
            if (item.id == (wishListItem.id)) {
                int pos = data.indexOf(wishListItem);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    private boolean isDataUnique(WishListItem newData) {
        return !data.contains(newData);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_social_feed_related, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.wishListItem = data.get(position);

        holder.view.setTag(position);
        holder.view.setOnClickListener(this);
        holder.avatarCIV.setTag(R.id.avatarCIV, position);
        holder.commentBTN.setTag(R.id.commentBTN,position);
        holder.commentCountTXT.setTag(R.id.commentCountTXT, position);
        holder.otherOptionBTN.setTag(R.id.otherOptionBTN, position);
        holder.socialFeedCON.setTag(R.id.socialFeedCON, position);

        holder.avatarCIV.setOnClickListener(this);
        holder.commentBTN.setOnClickListener(this);
        holder.commentCountTXT.setOnClickListener(this);
        holder.otherOptionBTN.setOnClickListener(this);
        holder.socialFeedCON.setOnClickListener(this);
        holder.heartSB.pressOnTouch(false);

        resetGridViewSocialFeed(holder.socialFeedCON);

//        holder.requestListener = new RequestListener<String, GlideDrawable>() {
//            @Override
//            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                return false;
//            }
//
//            @Override
//            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//
//                return false;
//            }
//        };

        holder.requestListener = new RequestListener() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                showGridViewData(holder, position);
                return false;
            }
        };

        holder.gridViewSparkEventListener = new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean buttonState) {
                holder.heartSB.playAnimation();
                if(clickListener != null){
                    clickListener.onHeartClick(data.get(position));
                }
            }

            @Override
            public void onEventAnimationEnd(ImageView button, boolean buttonState) {

            }

            @Override
            public void onEventAnimationStart(ImageView button, boolean buttonState) {

            }
        };

        holder.userNameTXT.setText("");
        holder.iLikeTXT.setText("");
        holder.dateTXT.setText("");
        holder.titleTXT.setText("");
        holder.commentCountTXT.setText("");
        holder.heartCountTXT.setText("");
        holder.socialFeedCON.setAlpha(0.5f);

        holder.userNameTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.light_gray));
        holder.iLikeIV.setImageDrawable(null);

        holder.heartSB.setEventListener(holder.gridViewSparkEventListener);

        Glide.with(context)
                .load(holder.wishListItem.wishListMin.like_image)
                .listener(holder.requestListener)
                .apply(new RequestOptions()
                .fitCenter()
                .placeholder(R.color.light_gray)
                .error(R.color.light_gray)
                .dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.iLikeIV);
    }

    private void showGridViewData(ViewHolder holder, int viewPosition){
        if(data.size() > viewPosition){
            holder.wishListItem = data.get(viewPosition);
            holder.socialFeedCON.setAlpha(1f);
            holder.heartSB.setChecked(holder.wishListItem.wishListMin.is_liked);
            holder.otherOptionBTN.setVisibility(holder.wishListItem.wishListMin.owned ? View.GONE : View.VISIBLE);
            holder.userNameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.wishListItem.wishListMin.name, holder.wishListItem.wishListMin.is_verified, 12));
            holder.iLikeTXT.setText(holder.wishListItem.wishListMin.category.toUpperCase());
            holder.dateTXT.setText(holder.wishListItem.wishListMin.time_passed);
            holder.titleTXT.setText(holder.wishListItem.wishListMin.title);
            holder.commentCountTXT.setText(NumberFormatter.format(holder.wishListItem.wishListMin.comment_count));
            holder.heartCountTXT.setText(NumberFormatter.format(holder.wishListItem.wishListMin.liker_count));

            holder.userNameTXT.setBackground(null);

            holder.heartSB.setVisibility(View.VISIBLE);
            holder.commentBTN.setVisibility(View.VISIBLE);

            Glide.with(context)
                    .load(holder.wishListItem.wishListMin.avatar)
                    .apply(new RequestOptions()
                    .placeholder(R.drawable.placeholder_avatar)
                    .error(R.drawable.placeholder_avatar)
                    .fitCenter()
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true))
                    .into(holder.avatarCIV);

            holder.heartSB.setVisibility(View.VISIBLE);
            holder.commentBTN.setVisibility(View.VISIBLE);
            holder.userNameTXT.setVisibility(View.VISIBLE);
            holder.iLikeTXT.setVisibility(View.VISIBLE);
            holder.dateTXT.setVisibility(View.VISIBLE);
            holder.titleTXT.setVisibility(View.VISIBLE);
            holder.commentCountTXT.setVisibility(View.VISIBLE);
            holder.heartCountTXT.setVisibility(View.VISIBLE);
        }
    }

    public void resetGridViewSocialFeed(View view){
        Drawable colorDrawable;
        ColorDrawable color = new ColorDrawable(ActivityCompat.getColor(context, R.color.text_gray));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            RippleDrawable rippleDrawable = new RippleDrawable(ColorStateList.valueOf(color.getColor()), null, null);
            colorDrawable = rippleDrawable;
        } else {
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.setEnterFadeDuration(200);
            stateListDrawable.setExitFadeDuration(200);
            stateListDrawable.addState(new int[]{android.R.attr.state_pressed}, color);
            colorDrawable = stateListDrawable;
        }
        view.setBackground(colorDrawable);
    }

    @Override
    public int getItemCount() {
        return data !=null ? data.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        WishListItem wishListItem;

        public RequestListener requestListener;
        public SparkEventListener gridViewSparkEventListener;

        @BindView(R.id.iLikeIV)                     FixedWidthImageView iLikeIV;
        @BindView(R.id.iLikeTXT)                    TextView iLikeTXT;
        @BindView(R.id.userNameTXT)                 TextView userNameTXT;
        @BindView(R.id.avatarCIV)                   ImageView avatarCIV;
        @BindView(R.id.dateTXT)                     TextView dateTXT;
        @BindView(R.id.titleTXT)                    TextView titleTXT;
        @BindView(R.id.otherOptionBTN)              View otherOptionBTN;
        @BindView(R.id.commentCountTXT)             TextView commentCountTXT;
        @BindView(R.id.commentBTN)                  View commentBTN;
        @BindView(R.id.socialFeedCON)               View socialFeedCON;
        @BindView(R.id.heartSB)                     SparkButton heartSB;
        @BindView(R.id.heartCountTXT)               TextView heartCountTXT;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.avatarCIV:
                if (clickListener != null) {
                    clickListener.onAvatarClick(getItem((int) v.getTag(R.id.avatarCIV)).owner.data);
                }
                break;
            case R.id.imageIV:
                if (clickListener != null) {
                    clickListener.onHeaderClick(eventItem);
                }
                break;
            case R.id.showBTN:
                if (clickListener != null) {
                    clickListener.onHeaderShowClick(eventItem);
                }
                break;
            case R.id.commentCountTXT:
                if (clickListener != null) {
                    clickListener.onCommentClick(getItem((int) v.getTag(R.id.commentCountTXT)));
                }
                break;
            case R.id.commentBTN:
                if (clickListener != null) {
                    clickListener.onCommentClick(getItem((int) v.getTag(R.id.commentBTN)));
                }
                break;
            case R.id.socialFeedCON:
                if (clickListener != null) {
                    clickListener.onItemClick(getItem((int) v.getTag(R.id.socialFeedCON)));
                }
                break;
            case R.id.iLikeIV:
                if (clickListener != null) {
                    clickListener.onItemClick(getItem((int) v.getTag(R.id.iLikeIV)));
                }
                break;
            case R.id.otherOptionBTN:
                otherOptionClick(v);
                break;
            case R.id.rePostBTN:
                if (clickListener != null) {
                    clickListener.onRepostClick(getItem((int) v.getTag(R.id.rePostBTN)));
                }
                break;
            case R.id.rePostCountTXT:
                if (clickListener != null) {
                    clickListener.onRepostUsersClick(getItem((int) v.getTag(R.id.rePostCountTXT)));
                }
                break;
            default:
                break;
        }
    }

    private void otherOptionClick(View view) {

        final WishListItem wishListItem = getItem((int) view.getTag(R.id.otherOptionBTN));
        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.social_feed_menu, popup.getMenu());
        popup.getMenu().findItem(R.id.unfollowBTN).setTitle(wishListItem.owner.data.social.data.is_following.equals("yes") ? "Unfollow" : "Follow" );

        String report = popup.getMenu().findItem(R.id.reportBTN).getTitle().toString();
        SpannableString spannableString = new SpannableString(report);
        spannableString.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(context, R.color.failed)), 0, spannableString.length(), 0);
        popup.getMenu().findItem(R.id.reportBTN).setTitle(spannableString);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.repostBTN:
                        if (clickListener != null) {
                            clickListener.onGrabClick(wishListItem);
                        }
                        break;
                    case R.id.hideBTN:
                        if (clickListener != null){
                            clickListener.onHideClick(wishListItem);
                        }
                        break;
                    case R.id.unfollowBTN:
                        if (clickListener != null) {
                            clickListener.onUnfollowClick(wishListItem);
                        }
                        break;
                    case R.id.reportBTN:
                        if (clickListener != null) {
                            clickListener.onReportClick(wishListItem);
                        }
                        break;
                }

                return false;
            }
        });
        popup.show();
    }


    public WishListItem getItem(int position){
        if(data.size() >0){
            return data.get(position);
        }else {
            return new WishListItem();
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(WishListItem wishListItem);

        void onCommentClick(WishListItem wishListItem);

        void onAvatarClick(UserItem userItem);

        void onGrabClick(WishListItem wishListItem);

        void onHideClick(WishListItem wishListItem);

        void onUnfollowClick(WishListItem wishListItem);

        void onReportClick(WishListItem wishListItem);

        void onHeartClick(WishListItem wishListItem);

        void onRepostClick(WishListItem wishListItem);

        void onRepostUsersClick(WishListItem wishListItem);

        void onHeaderClick(EventItem eventItem);

        void onHeaderShowClick(EventItem eventItem);
    }
} 
