package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.BankCardItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardRecyclerViewAdapter extends RecyclerView.Adapter<CardRecyclerViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<BankCardItem> data;
    private LayoutInflater layoutInflater;

	public CardRecyclerViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<BankCardItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<BankCardItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(BankCardItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    public String getSelectedItems(){
        String total = "";
        for (BankCardItem bankCardItem : data){
            if (bankCardItem.isSelected){
                total += bankCardItem.id;
            }
        }

        return total;
    }

    public String getCardType(){
        String total = "";
        for (BankCardItem bankCardItem : data){
            if (bankCardItem.isSelected){
                total += bankCardItem.cardType;
            }
        }

        return total;
    }

    public void setSelected(int id){
        for(BankCardItem item : data){
            item.isSelected = item.id == id;
        }
        notifyDataSetChanged();
    }

    private boolean isDataUnique(BankCardItem newData){
        for(BankCardItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_card, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bankCardItem = data.get(position);
        holder.view.setTag(holder.bankCardItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.adapterCON.setTag(holder.bankCardItem);
        holder.radioBTN.setTag(holder.bankCardItem);
        holder.adapterCON.setOnClickListener(this);
        holder.radioBTN.setOnClickListener(this);
        holder.lastDigitTXT.setText ("**** **** **** " + holder.bankCardItem.cardNumber);
        switch (holder.bankCardItem.cardType) {
            case "visa":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_visa);
                break;
            case "diners":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_diners_club);
                break;
            case "discover":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_discover);
                break;
            case "jcb":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_jcb);
                break;
            case "mastercard":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_master_card);
                break;
            case "amex":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_amex);
                break;
            case "paypal":
                holder.masterCardIV.setImageResource(R.drawable.icon_paypal);
                holder.lastDigitTXT.setText("Paypal");
                break;

        }

        if (holder.bankCardItem.isSelected){
            holder.radioBTN.setChecked(true);
        }else{
            holder.radioBTN.setChecked(false);
        }

//        holder.radioBTN.setEnabled(false);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        BankCardItem bankCardItem;

        @BindView(R.id.masterCardIV)        ImageView masterCardIV;
        @BindView(R.id.lastDigitTXT)        TextView lastDigitTXT;
        @BindView(R.id.radioBTN)            RadioButton radioBTN;
        @BindView(R.id.adapterCON)          View adapterCON;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.radioBTN:
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((BankCardItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(BankCardItem bankCardItem);
        void onItemLongClick(BankCardItem bankCardItem);
    }
}