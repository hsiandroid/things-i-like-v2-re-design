package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.WishListItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HotItemsRecycleViewAdapter extends RecyclerView.Adapter<HotItemsRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<WishListItem> data;
    private LayoutInflater layoutInflater;

	public HotItemsRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<WishListItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<WishListItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(WishListItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(WishListItem newData){
        for(WishListItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_exp_hot, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.wishListItem = data.get(position);
        holder.view.setTag(holder.wishListItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.adapterCON.setTag(holder.wishListItem);
        holder.adapterCON.setOnClickListener(this);

        holder.titleTXT.setText(holder.wishListItem.title);
//        Picasso.with(context).load(holder.wishListItem.image.data.thumb_path).placeholder(R.drawable.explorebg).error(R.drawable.explorebg).fit().centerCrop().into(holder.photosIV);
	    Picasso.with(context).load(holder.wishListItem.image.data.thumb_path).placeholder(R.color.light_gray).error(R.color.light_gray).fit().centerCrop().into(holder.photosIV);
	    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        WishListItem wishListItem;

        @BindView(R.id.photosIV)            ImageView photosIV;
        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.titleTXT)            TextView titleTXT;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((WishListItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(WishListItem wishListItem);
        void onItemLongClick(WishListItem wishListItem);
    }
}