package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListTransactionItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class GiftRecycleViewAdapter extends RecyclerView.Adapter<GiftRecycleViewAdapter.ViewHolder>  implements View.OnClickListener{
	private Context context;
	private List<WishListTransactionItem> data;
    private LayoutInflater layoutInflater;
    private DisplayType displayType;

    public enum DisplayType{
        RECEIVED,
        GAVE
    }

	public GiftRecycleViewAdapter(Context context, DisplayType displayType) {
		this.context = context;
		this.displayType = displayType;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<WishListTransactionItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<WishListTransactionItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(WishListTransactionItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    public void updateData(WishListTransactionItem data) {
        for (WishListTransactionItem item : this.data) {
            if (data.id == item.id) {
                int pos = this.data.indexOf(item);
                this.data.set(pos, data);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    public void updateStatus(int id, String status) {
        for (WishListTransactionItem item : this.data) {
            if (id== item.id) {
                int pos = this.data.indexOf(item);
                item.status = status;
                this.data.set(pos, item);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    public void updateStatusByWishList(int id, String status) {
        Log.e("Pos", ">>>" + id);
        for (WishListTransactionItem item : this.data) {
            if (id == item.wishlist_id) {
                int pos = this.data.indexOf(item);
                item.status = status;
                this.data.set(pos, item);
                Log.e("Pos", ">>>" + pos);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    private boolean isDataUnique(WishListTransactionItem newData){
        for(WishListTransactionItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_gift, parent, false));
    }

    public Object getItemData(int position){
        return data.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.wishListItem = data.get(position);

        holder.iLikeIV.setTag(R.id.iLikeIV, position);
        holder.iLikeIV.setOnClickListener(this);

        holder.avatarCIV.setOnClickListener(this);

        String description_format = holder.wishListItem.description_format;
        boolean hasFormat = description_format.length() > 0;

        switch (displayType){
            case RECEIVED:
                holder.avatarCIV.setTag(R.id.avatarCIV, holder.wishListItem.sender.data);
                Glide.with(context)
                        .load(holder.wishListItem.sender.data.getAvatar())
                        .apply(new RequestOptions()

                                .dontAnimate()
                        .skipMemoryCache(true)
                        .dontTransform()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                        .into(holder.avatarCIV);
                switch (holder.wishListItem.status){
                    case "ongoing":
                        holder.statusTXT.setVisibility(View.VISIBLE);
                        holder.statusTXT.setText("waiting for gift");
                        holder.statusTXT.setOnClickListener(null);

                        holder.descriptionTXT.setText(
                            formatDescription(
                                hasFormat ? description_format : "Your {wish} is still on its way from {user}.",
                                position, holder.wishListItem.sender.data,
                                holder.wishListItem.sender.data.name,
                                holder.wishListItem.title)
                            );
                        break;
                    case "pending":
                        holder.statusTXT.setVisibility(View.VISIBLE);
                        holder.statusTXT.setText("pending");
                        holder.statusTXT.setTag(position);
                        holder.statusTXT.setOnClickListener(this);
                        holder.descriptionTXT.setText(
                            formatDescription(
                                    hasFormat ? description_format : "You have not received the {wish} yet from {user}.",
                                    position, holder.wishListItem.sender.data,
                                    holder.wishListItem.sender.data.name,
                                    holder.wishListItem.title)
                            );
                        break;
                    default:
                        holder.descriptionTXT.setText(
                            formatDescription(
                                    hasFormat ? description_format : "You have already received the {wish} from {user}.",
                                    position, holder.wishListItem.sender.data,
                                    holder.wishListItem.sender.data.name,
                                    holder.wishListItem.title)
                            );
                        holder.statusTXT.setVisibility(View.GONE);
                        holder.statusTXT.setOnClickListener(null);
                }
                break;
            case GAVE:
                holder.avatarCIV.setTag(R.id.avatarCIV, holder.wishListItem.owner.data);
                Glide.with(context)
                        .load(holder.wishListItem.owner.data.getAvatar())
                        .apply(new RequestOptions()

                                .dontAnimate()
                        .skipMemoryCache(true)
                        .dontTransform()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                        .into(holder.avatarCIV);
                switch (holder.wishListItem.status){
                    case "ongoing":
                        holder.statusTXT.setVisibility(View.VISIBLE);
                        holder.statusTXT.setText("send gift now!");
                        holder.statusTXT.setTag(position);
                        holder.statusTXT.setOnClickListener(this);
                        holder.descriptionTXT.setText(
                            formatDescription(
                                    hasFormat ? description_format : "{user} is still waiting for the {wish}.",
                                    position, holder.wishListItem.owner.data,
                                    holder.wishListItem.owner.data.name,
                                    holder.wishListItem.title)
                            );
                        break;
                    case "pending":
                        holder.statusTXT.setVisibility(View.VISIBLE);
                        holder.statusTXT.setText("pending");
                        holder.statusTXT.setOnClickListener(null);
                        holder.descriptionTXT.setText(
                            formatDescription(
                                    hasFormat ? description_format : "{user} has not received the {wish} yet.",
                                    position, holder.wishListItem.owner.data,
                                    holder.wishListItem.owner.data.name,
                                    holder.wishListItem.title)
                            );
                        break;
                    default:
                        holder.statusTXT.setVisibility(View.GONE);
                        holder.statusTXT.setOnClickListener(null);
                        holder.descriptionTXT.setText(
                            formatDescription(
                                hasFormat ? description_format : "{user} has already received the {wish}.",
                                position, holder.wishListItem.owner.data,
                                holder.wishListItem.owner.data.name,
                                holder.wishListItem.title)
                            );
                }
                break;
        }


        holder.descriptionTXT.setMovementMethod(LinkMovementMethod.getInstance());
        holder.dateTXT.setText(holder.wishListItem.time_passed);

        Glide.with(context)
                .load(holder.wishListItem.image.data.full_path)
                .apply(new RequestOptions()

                        .dontAnimate()
                .skipMemoryCache(true)
                .dontTransform()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(holder.iLikeIV);

    }

    public SpannableString formatDescription(String format, final int position, final UserItem owner, String ownerName, String wishName){
        String findUserName = "{user}";
        String findWishList = "{wish}";

        String finalMessage = format.replace(findUserName, ownerName).replace(findWishList, wishName);
        SpannableString spannableString = new SpannableString(finalMessage);

        if(format.contains(findWishList)){
            int wishListStart = finalMessage.indexOf(wishName);
            int wishListEnd = wishListStart + wishName.length();
            ClickableSpan productSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    clickListener.onItemClick(position, textView);
                }

                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                    ds.setColor(ActivityCompat.getColor(context, R.color.colorPrimary));
                }
            };
            spannableString.setSpan(productSpan,wishListStart , wishListEnd, Spanned.SPAN_INTERMEDIATE);
        }

        if(format.contains(findUserName)){
            int userNameStart = finalMessage.indexOf(ownerName);
            int userNameEnd = userNameStart + ownerName.length();
            ClickableSpan nameSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    if(owner != null){
                        clickListener.onAvatarClick(owner);
                    }
                }

                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                    ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_display_name)));
                    ds.setColor(ActivityCompat.getColor(context, R.color.colorPrimary));
                }
            };
            spannableString.setSpan(nameSpan, userNameStart, userNameEnd, Spanned.SPAN_INTERMEDIATE);
        }

        return spannableString;
    }

    public SpannableString formatDescription(final ViewHolder holder, final int position, final UserItem owner){
        String extraMessage = "";
        String article = "a";
        String userName = "-";

        if(holder.wishListItem.title.length() > 0){
            switch (holder.wishListItem.title.substring(0, 1).toLowerCase()){
                case "a":
                case "e":
                case "i":
                case "o":
                case "u":
                    article = "an";
                    break;
            }
        }

        String finalMessage = "";
        switch (displayType){
            case RECEIVED:
                userName = "Your";
                extraMessage = " gave you " + article + " ";
                finalMessage = userName + extraMessage + holder.wishListItem.title + ".";
                break;
            case GAVE:
                userName = owner.name;
                extraMessage = " received " + article + " ";
                finalMessage = userName + extraMessage + holder.wishListItem.title + ".";
                break;
        }

        SpannableString spannableString = new SpannableString(finalMessage);

        ClickableSpan nameSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onAvatarClick(owner);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_display_name)));
            }
        };

        spannableString.setSpan(nameSpan, 0, userName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan productSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onItemClick(position, textView);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.colorPrimary));
            }
        };

        spannableString.setSpan(productSpan,userName.length() + extraMessage.length(), userName.length() + extraMessage.length() + holder.wishListItem.title.length(), Spanned.SPAN_INTERMEDIATE);

        return spannableString;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        WishListTransactionItem wishListItem;

        @BindView(R.id.iLikeIV)             ImageView iLikeIV;
        @BindView(R.id.descriptionTXT)      TextView descriptionTXT;
        @BindView(R.id.statusTXT)           TextView statusTXT;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.avatarCIV)           ImageView avatarCIV;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            switch (v.getId()){
                case R.id.iLikeIV:
                    clickListener.onItemClick((int)v.getTag(R.id.iLikeIV), v);
                    break;
                case R.id.avatarCIV:
                    clickListener.onAvatarClick((UserItem) v.getTag(R.id.avatarCIV));
                    break;
                case R.id.statusTXT:
                    statusClicked((int) v.getTag());
                    break;
            }
        }
    }

    private void statusClicked(int position){
        WishListTransactionItem item = data.get(position);
        switch (displayType){
            case RECEIVED:
                if(clickListener != null){
                    clickListener.onStatusClick("received", item.id, item.sender.data.id);
                }
                break;
            case GAVE:
                if(clickListener != null){
                    clickListener.onStatusClick("gave", item.wishlist_id, item.sender.data.id);
                }
                break;

        }
    }


    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onAvatarClick(UserItem userItem);
        void onStatusClick(String status, int wishlistID, int userID);
    }
} 
