package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.OrderItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderRecycleViewAdapter extends RecyclerView.Adapter<OrderRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<OrderItem> data;
    private LayoutInflater layoutInflater;

	public OrderRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<OrderItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<OrderItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(OrderItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(OrderItem newData){
        for(OrderItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_order_card, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.orderItem = data.get(position);
        holder.view.setTag(holder.orderItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.adapterCON.setTag(holder.orderItem);
        holder.adapterCON.setOnClickListener(this);

        holder.orderTXT.setText(holder.orderItem.displayId);
        String upperString = holder.orderItem.statusDisplay.substring(0,1).toUpperCase() + holder.orderItem.statusDisplay.substring(1);
        String format = upperString.replace("_", " ");
        holder.statusTXT.setText(format);
        holder.dateTXT.setText(holder.orderItem.purchasedDate.date);

        if (holder.orderItem.detail.data != null){
            holder.itemNameTXT.setText(holder.orderItem.detail.data.get(0).title);
            holder.qtyTXT.setText("QTY: " + holder.orderItem.detail.data.get(0).itemQty);
            Picasso.with(context).load(holder.orderItem.detail.data.get(0).thumbPath).centerCrop().fit().into(holder.photosIV);
        }else{
            holder.itemNameTXT.setText("null");
            holder.qtyTXT.setText("null");
            Picasso.with(context).load(R.drawable.placeholder_request).centerCrop().fit().into(holder.photosIV);
        }

        switch (holder.orderItem.statusDisplay){
            case "cancelled":
            case "dispute":
            case "refund":
                holder.statusTXT.setBackgroundResource(R.color.red);

                break;
            case "pending":
            case "for_approval":
                holder.statusTXT.setBackgroundResource(R.color.gray);

                break;
            case "closed":
                holder.statusTXT.setBackgroundResource(R.color.gray);
                break;
            case "delivered":
            case "completed":
                holder.statusTXT.setBackgroundResource(R.color.completed);
                break;
            case "in_transit":
                holder.statusTXT.setBackgroundResource(R.color.orange);
                break;
            case "for_delivery":
                holder.statusTXT.setBackgroundResource(R.color.colorPrimary);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        OrderItem orderItem;

        @BindView(R.id.photosIV)            ImageView photosIV;
        @BindView(R.id.orderTXT)            TextView orderTXT;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.itemNameTXT)         TextView itemNameTXT;
        @BindView(R.id.qtyTXT)         TextView qtyTXT;
        @BindView(R.id.statusTXT)         TextView statusTXT;
        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.statusColorCON)      View statusColorCON;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((OrderItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(OrderItem orderItem);
        void onItemLongClick(OrderItem orderItem);
    }
}