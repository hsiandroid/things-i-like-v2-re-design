package com.thingsilikeapp.android.adapter.recyler;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListViewerItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class RequestOwnedRecyclerViewAdapter extends RecyclerView.Adapter<RequestOwnedRecyclerViewAdapter.ViewHolder>  implements
        View.OnClickListener{

	private Context context;
	private List<WishListViewerItem> data;
    private LayoutInflater layoutInflater;

	public RequestOwnedRecyclerViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<WishListViewerItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void removeData(WishListViewerItem item) {
        for (WishListViewerItem oldItem : data) {
            if (item.id == (oldItem.id)) {
                int pos = data.indexOf(oldItem);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    public void updateData(WishListViewerItem commentItem){
        for(WishListViewerItem item : data){
             if(commentItem.id == item.id){
                int pos = data.indexOf(item);
                data.set(pos, commentItem);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    public void addNewData(List<WishListViewerItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(WishListViewerItem commentItem :  data){
            if(isDataUnique(commentItem)){
                this.data.add(commentItem);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(WishListViewerItem newData){
        for(WishListViewerItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_owned_request, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.wishListViewerItem = data.get(position);

        holder.wishListViewerItem = data.get(position);

        holder.iLikeIV.setOnClickListener(this);
        holder.avatarCIV.setOnClickListener(this);

        holder.acceptTXT.setOnClickListener(this);

        holder.iLikeIV.setTag(R.id.iLikeIV, holder.wishListViewerItem);
        holder.avatarCIV.setTag(R.id.avatarCIV, holder.wishListViewerItem.wishlist.data.owner.data);
        holder.acceptTXT.setTag(holder.wishListViewerItem);

        holder.descriptionTXT.setText(formatDescription(holder, position));
        holder.descriptionTXT.setMovementMethod(LinkMovementMethod.getInstance());

        holder.dateTXT.setText(holder.wishListViewerItem.time_passed);
        Glide.with(context)
                .load(holder.wishListViewerItem.wishlist.data.image.data.full_path)
                .into(holder.iLikeIV);

        Glide.with(context)
                .load(holder.wishListViewerItem.wishlist.data.owner.data.getAvatar())
                .apply(new RequestOptions()
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate())
                .into(holder.avatarCIV);
    }

    public SpannableString formatDescription(final ViewHolder holder, final int position){
        String extraMessage = "You are requesting permission to view the delivery information of ";
        holder.extraMessageTXT.setVisibility(View.GONE);
        String userName = holder.wishListViewerItem.wishlist.data.owner.data.name;

        String finalMessage = extraMessage + userName +  ", " + holder.wishListViewerItem.wishlist.data.title;
        SpannableString spannableString = new SpannableString(finalMessage);

        ClickableSpan nameSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onYourProfileClick(data.get(position).wishlist.data.owner.data);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_display_name)));
            }
        };
        spannableString.setSpan(nameSpan, 0, "You".length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan otherNameCS = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onAvatarClick(data.get(position).wishlist.data.owner.data);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_display_name)));
            }
        };
        spannableString.setSpan(otherNameCS, extraMessage.length() , extraMessage.length() + userName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan wishListCS = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onItemClick(data.get(position));
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.colorPrimary));
            }
        };

        spannableString.setSpan(wishListCS, userName.length() + extraMessage.length() + ", ".length(), finalMessage.length(), Spanned.SPAN_INTERMEDIATE);

        return spannableString;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        WishListViewerItem wishListViewerItem;

        @BindView(R.id.iLikeIV)             ImageView iLikeIV;
        @BindView(R.id.acceptTXT)           TextView acceptTXT;
        @BindView(R.id.descriptionTXT)      TextView descriptionTXT;
        @BindView(R.id.extraMessageTXT)     TextView extraMessageTXT;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.avatarCIV)           ImageView avatarCIV;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}
    @Override
    public void onClick(View v) {
        if(clickListener != null){
            switch (v.getId()){
                case R.id.avatarCIV:
                    clickListener.onAvatarClick((UserItem) v.getTag(R.id.avatarCIV));
                    break;
                case R.id.acceptTXT:
                    clickListener.onCancelClick((WishListViewerItem) v.getTag());
                    break;
                case R.id.iLikeIV:
                    clickListener.onItemClick((WishListViewerItem) v.getTag(R.id.iLikeIV));
                    break;
            }
        }
    }


    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(WishListViewerItem thingsILikeItem);
        void onAvatarClick(UserItem userItem);
        void onYourProfileClick(UserItem userItem);
        void onCancelClick(WishListViewerItem thingsILikeItem);
    }
} 
