package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.OrderHistoryItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderTimelineAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<OrderHistoryItem> data;
    private LayoutInflater layoutInflater;

	public OrderTimelineAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<OrderHistoryItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_timeline, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.orderHistoryItem = data.get(position);

        holder.nameTXT.setText(holder.orderHistoryItem.remarks);
        holder.dateTXT.setText(holder.orderHistoryItem.transactionDate);

        holder.indicatorUp.setVisibility(position == 0 ?  View.INVISIBLE : View.VISIBLE);
        holder.indicatorDown.setVisibility(data.size() - 1  == position ?  View.INVISIBLE : View.VISIBLE);
//        holder.indicatorCenter.setBackground(ActivityCompat.getDrawable(context, holder.orderHistoryItem.completed ? R.drawable.bg_circle_small_primary : R.drawable.bg_circle_small_stroke));

        if (holder.orderHistoryItem.transactionDate.equals("")){
            holder.timeLineCON.setBackgroundColor(ActivityCompat.getColor(context, R.color.background1));
            holder.timeLineCON.setAlpha(.6f);
            holder.nameTXT.setTextColor(ActivityCompat.getColor(context, R.color.white_dark_gray));
            holder.separatorView.setBackgroundColor(ActivityCompat.getColor(context, R.color.white_dark_gray));
        }else {
            holder.timeLineCON.setBackgroundColor(ActivityCompat.getColor(context, R.color.background));
            holder.timeLineCON.setAlpha(1f);
            holder.nameTXT.setTextColor(ActivityCompat.getColor(context, R.color.colorPrimary));
            holder.separatorView.setBackgroundColor(ActivityCompat.getColor(context, R.color.colorPrimary));
        }
		return convertView;
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onTemplateItemClickListener(viewHolder.orderHistoryItem);
        }
    }


    public class ViewHolder{
        OrderHistoryItem orderHistoryItem;

        @BindView(R.id.nameTXT)         TextView nameTXT;
        @BindView(R.id.dateTXT)         TextView dateTXT;
        @BindView(R.id.indicatorUp)     View indicatorUp;
        @BindView(R.id.indicatorCenter) View indicatorCenter;
        @BindView(R.id.indicatorDown)   View indicatorDown;
        @BindView(R.id.timeLineCON)     View timeLineCON;
        @BindView(R.id.separatorView)   View separatorView;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onTemplateItemClickListener(OrderHistoryItem orderHistoryItem);
    }
} 
