package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.NotificationItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityRecycleViewAdapter extends RecyclerView.Adapter<ActivityRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener,
        View.OnLongClickListener {
	private Context context;
	private List<NotificationItem> data;
    private LayoutInflater layoutInflater;
    private boolean multipleSelect;
    private boolean selectAll;

	public ActivityRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<NotificationItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<NotificationItem> data){
        for(NotificationItem notificationItem :  data){
            notificationItem.selected = selectAll;
            this.data.add(notificationItem);
        }
        notifyDataSetChanged();
    }

    public void reset(){
        this.data = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void removeData(NotificationItem item){
        for(NotificationItem notificationItem : data){
            if(item.id.equals(notificationItem.id)){
                data.remove(notificationItem);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void removeData(String id){
        for(NotificationItem notificationItem : data){
            if(id.equals(notificationItem.id)){
                data.remove(notificationItem);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void setRead(String id){
        for(NotificationItem notificationItem : data){
            if(id.equals(notificationItem.id)){
                int pos = data.indexOf(notificationItem);
                notificationItem.is_read = "yes";
                data.set(pos, notificationItem);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    public void removeSelected(){
        for(String id : getSelectedIds()){
            removeData(id);
        }
    }

    public List<NotificationItem> getData(){
        return data;
    }

    public void setMultipleSelect(boolean multipleSelect){
        this.multipleSelect = multipleSelect;
        unSelectAll();
        notifyDataSetChanged();
    }

    public void setItemSelected(int position, boolean selected){
        data.get(position).selected = selected;
        notifyItemChanged(position);
    }

    public void selectAll(){
        selectAll = true;
        for(NotificationItem notificationItem : data){
            notificationItem.selected = selectAll;
        }
        notifyDataSetChanged();
    }

    public void unSelectAll(){
        selectAll = false;
        for(NotificationItem notificationItem : data){
            notificationItem.selected = selectAll;
        }
        notifyDataSetChanged();
    }

    public boolean isMultipleSelect() {
        return multipleSelect;
    }

    public boolean isSelectAll() {
        return selectAll;
    }

    public List<NotificationItem> getSelectedItems(){
        List<NotificationItem> items = new ArrayList<>();
        for(NotificationItem notificationItem : data){
            if(notificationItem.selected){
                items.add(notificationItem);
            }
        }
        return items;
    }

    public List<String> getSelectedIds(){
        List<String> items = new ArrayList<>();
        for(NotificationItem notificationItem : data){
            if(notificationItem.selected){
                items.add(notificationItem.id);
            }
        }
        return items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_activity, parent, false));
    }

    public NotificationItem getItem(int position){
        return data.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.notificationItem = data.get(position);

        holder.view.setOnClickListener(this);
        holder.view.setTag(position);

        if(multipleSelect){
            holder.multipleSelectRB.setTag(position);
            holder.multipleSelectRB.setVisibility(View.VISIBLE);
            holder.multipleSelectRB.setOnClickListener(this);
            holder.multipleSelectRB.setChecked(holder.notificationItem.selected);
            holder.view.setOnLongClickListener(null);
        }else{
            holder.multipleSelectRB.setVisibility(View.GONE);
            holder.view.setOnLongClickListener(this);
        }

        holder.removeTXT.setPaintFlags(holder.removeTXT.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        holder.titleTXT.setText(holder.notificationItem.content);
        holder.dateTXT.setText(holder.notificationItem.time_passed);
        holder.removeTXT.setText(holder.notificationItem.deleted_display.toLowerCase());

        if(holder.notificationItem.is_read.equals("yes")){
            holder.activityCON.setBackgroundColor(ActivityCompat.getColor(context, R.color.white));
        }else{
            holder.activityCON.setBackgroundColor(ActivityCompat.getColor(context, R.color.colorSecondaryFade));
        }

        if (holder.notificationItem.is_deleted){
            holder.removeTXT.setVisibility(View.VISIBLE);
        }else {
            holder.removeTXT.setVisibility(View.GONE);
        }

        if(holder.notificationItem.thumbnail.equals("")){
            holder.notificationItem.thumbnail = "/sample.png";
        }

        Glide.with(context)
                .load(holder.notificationItem.thumbnail)
                .apply(new RequestOptions()
                .placeholder(R.drawable.logo_blue_new)
                .error(R.drawable.logo_blue_new)
                .fitCenter()
                .dontAnimate())
                .into(holder.iLikeIV);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        NotificationItem notificationItem;

        @BindView(R.id.titleTXT)            TextView titleTXT;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.removeTXT)           TextView removeTXT;
        @BindView(R.id.activityCON)         View activityCON;
        @BindView(R.id.iLikeIV)             ImageView iLikeIV;
        @BindView(R.id.multipleSelectRB)    RadioButton multipleSelectRB;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            defaultConfig();
        }

        private void defaultConfig(){

        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activityCON:
                if(multipleSelect){
                    int pos = (int) v.getTag();
                    data.get(pos).selected = !data.get(pos).selected;
                    notifyItemChanged(pos);
                }else{
                    if(clickListener != null){
                        clickListener.onItemClick((int)v.getTag(), v);
                    }
                }
                break;
            case R.id.multipleSelectRB:
                int pos = (int) v.getTag();
                data.get(pos).selected = !data.get(pos).selected;
                notifyItemChanged(pos);
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick((int)v.getTag(), v);
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
} 
