package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.thingsilikeapp.android.fragment.messages.FollowerssFragment;
import com.thingsilikeapp.android.fragment.messages.FollowingsFragment;
import com.thingsilikeapp.android.fragment.messages.GroupMessageFragment;
import com.thingsilikeapp.android.fragment.messages.PeopleFragment;
import com.thingsilikeapp.android.fragment.messages.PrivateMessageFragment;
import com.thingsilikeapp.data.preference.UserData;


/**
 * Created by Ken Drew on 11/11/2017.
 */

public class NewMessagesPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;
    private Context context;

    public NewMessagesPagerAdapter(FragmentManager fragmentManager, Context c) {
        super(fragmentManager);
        context = c;
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return PeopleFragment.newInstance();
            case 1:
                return FollowerssFragment.newInstance(UserData.getUserId());
            case 2:
                return FollowingsFragment.newInstance(UserData.getUserId());
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_UNCHANGED;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "People";
            case 1:
                return "Followers";
            case 2:
                return "Following";
            default:
                return "Page" + position;

        }

    }

}