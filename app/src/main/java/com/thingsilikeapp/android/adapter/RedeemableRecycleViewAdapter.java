package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.CatalogueItem;
import com.thingsilikeapp.vendor.android.java.GemHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RedeemableRecycleViewAdapter extends RecyclerView.Adapter<RedeemableRecycleViewAdapter.ViewHolder>  implements View.OnClickListener {
	private Context context;
	private List<CatalogueItem> data;
    private LayoutInflater layoutInflater;
    private int amount;

	public RedeemableRecycleViewAdapter(Context context) {
		this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.data = new ArrayList<>();
	}
    public void setNewData(List<CatalogueItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_redeemable, parent, false));
    }

    public void addNewData(List<CatalogueItem> data){
        for(CatalogueItem item :  data){
            this.data.add(item);
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.myRewardsItem = data.get(position);

        holder.redeemBTN.setTag(holder.myRewardsItem);
        holder.redeemBTN.setOnClickListener(this);

        holder.adapterCON.setTag(holder.myRewardsItem);
        holder.adapterCON.setOnClickListener(this);

        Glide.with(context)
                .load(holder.myRewardsItem.image.data.full_path)
                .apply(new RequestOptions()

                        .fitCenter()
                .placeholder(R.color.light_gray)
                .error(R.color.light_gray)
                .dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.redeemableIV);

        holder.typeIV.setImageDrawable(
                ActivityCompat.getDrawable(context,
                        holder.myRewardsItem.reward_type.equalsIgnoreCase("crystal") ?
                                R.drawable.icon_lyka_gems :
                                R.drawable.icon_lyka_chips));

        holder.nameTXT.setText(holder.myRewardsItem.title);
        double doubles = holder.myRewardsItem.value;
        int ints = (int) doubles;
        holder.rewardPointsTXT.setText(String.valueOf(ints));
//        holder.valueTXT.setText(GemHelper.formatGem(holder.myRewardsItem.value));
        holder.valueTXT.setText(String.valueOf(ints));
        if (holder.myRewardsItem.remaining == 0){
            holder.remainingTXT.setText("SOLD OUT");
        }else{holder.remainingTXT.setText(holder.myRewardsItem.remaining + " left");}
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        CatalogueItem myRewardsItem;

        @BindView(R.id.redeemableIV)           ImageView redeemableIV;
        @BindView(R.id.typeIV)                 ImageView typeIV;
        @BindView(R.id.nameTXT)                TextView nameTXT;
        @BindView(R.id.rewardPointsTXT)        TextView rewardPointsTXT;
        @BindView(R.id.valueTXT)               TextView valueTXT;
        @BindView(R.id.remainingTXT)           TextView remainingTXT;
        @BindView(R.id.redeemBTN)              View redeemBTN;
        @BindView(R.id.adapterCON)             View adapterCON;
        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.redeemBTN:
                if(clickListener != null){
                    clickListener.onItemClick((CatalogueItem) v.getTag());
                }
                break;

                case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onDetailClick((CatalogueItem) v.getTag());
                }
                break;
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(CatalogueItem catalogueItem);
        void onDetailClick(CatalogueItem catalogueItem);
    }
} 
