package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.GalleryItem;

import net.igenius.customcheckbox.CustomCheckBox;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GalleryAdapter extends BaseAdapter implements View.OnClickListener{
	private Context context;
	private List<GalleryItem> data;
	private List<GalleryItem> allData;
    private LayoutInflater layoutInflater;
    private int perPage = 500;
    private int page = 1;

	public GalleryAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
		this.allData = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<GalleryItem> data){
        this.allData = data;
        this.data.clear();
//        addCamera();
        addPage(1);
    }

    public void updateItem(GalleryItem item){
        for(GalleryItem defaultItem : data){
            if (item.id == defaultItem.id) {
                int pos = data.indexOf(defaultItem);
                data.set(pos, item);
                notifyDataSetChanged();
                break;
            }
        }
    }


    public List<GalleryItem> getSelectedAlbums(){
        List<GalleryItem> albumsModels = new ArrayList<>();
        for (GalleryItem albumsModel : data){
            if (albumsModel.is_selected){
                albumsModels.add(albumsModel);
            }
        }
        return albumsModels;
    }

    public List<String> getSelectedPicture(){
        List<String> albumsModels = new ArrayList<>();
        for (GalleryItem albumsModel : data){
                if (albumsModel.is_selected){
                    albumsModels.add(albumsModel.absolutePath);
            }

        }
        return albumsModels;
    }

    public int getSelectedItemCount(){
	    int selectedItem = 1;
        for (GalleryItem albumsModel : data){
            if (albumsModel.is_selected){
                selectedItem++;
            }
      }
	    return selectedItem;
    }


    public GalleryItem getAlbumByID(int id){
        GalleryItem albumsModel = new GalleryItem();

        for (GalleryItem albumsModel1 : data){
            if (albumsModel1.id == id){
                albumsModel = albumsModel1;
            }
        }
        return albumsModel;
    }


    public GalleryItem getGalleryItemByPosition(int position){
	    return data.get(position);
    }

    public void addCamera(){
        GalleryItem galleryItem = new GalleryItem();
        galleryItem.isCamera = true;
        data.add(galleryItem);
    }

    public void addPage(int page) {
        if(perPage <= 0 || page <= 0) {
            return;
        }

        int fromIndex = (page - 1) * perPage;
        if(allData == null || allData.size() -1 < fromIndex){
            return;
        }
        for(GalleryItem item : allData.subList(fromIndex, Math.min(fromIndex + perPage, allData.size()))){
            item.isCamera = false;
            data.add(item);
        }
        notifyDataSetChanged();
        page++;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_gallery, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.view.setOnClickListener(this);
        holder.galleryItem = data.get(position);


        if(holder.galleryItem.isCamera){
            holder.imageIV.setVisibility(View.GONE);
            holder.cameraIV.setVisibility(View.VISIBLE);
        }else{

            Glide.with(context)
                    .load(new File(holder.galleryItem.absolutePath))
                    .apply(new RequestOptions()
                    .override(200, 200)
                    .centerCrop())
                    .into(holder.imageIV);

            holder.imageIV.setVisibility(View.VISIBLE);


            holder.checkBox.setChecked(holder.galleryItem.is_selected);
            holder.checkBox.setEnabled(false);
            holder.cameraIV.setVisibility(View.GONE);
        }

		return convertView;
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onImageClickListener(viewHolder.galleryItem);
                }
                if(clickListener != null){
                    clickListener.onSelectedClickListener(viewHolder.galleryItem.id);
                }
                if(clickListener != null){
                    clickListener.onVideoClickListener(viewHolder.galleryItem);
                }
                break;
        }

    }

    public class ViewHolder{
        GalleryItem galleryItem;
        @BindView(R.id.imageIV)         ImageView imageIV;
        @BindView(R.id.cameraIV)        ImageView cameraIV;
        @BindView(R.id.checkbox)        CustomCheckBox checkBox;

        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onImageClickListener(GalleryItem galleryItem);
        void onSelectedClickListener(int position);
        void onVideoClickListener(GalleryItem galleryItem);
    }

} 
