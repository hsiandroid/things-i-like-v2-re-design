package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.HidePostItem;
import com.thingsilikeapp.data.model.HidePostItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HiddenPostRecycleViewAdapter extends RecyclerView.Adapter<HiddenPostRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {

    private Context context;
	private List<HidePostItem> data;
    private LayoutInflater layoutInflater;

	public HiddenPostRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<HidePostItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<HidePostItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(HidePostItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(HidePostItem newData){
        for(HidePostItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_hide_post, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.hidePostItem = data.get(position);
        holder.view.setTag(holder.hidePostItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.adapterCON.setTag(holder.hidePostItem);
        holder.adapterCON.setOnClickListener(this);

        Glide.with(context)
                .load(holder.hidePostItem.wishlist.data.image.data.fullPath)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true))
                .into(holder.imageCIV);

        holder.nameTXT.setText(holder.hidePostItem.wishlist.data.title);
        holder.commonNameTXT.setText( holder.hidePostItem.wishlist.data.category);

        holder.nameTXT.setTag(holder.hidePostItem);
        holder.nameTXT.setOnClickListener(this);

        holder.commonNameTXT.setTag(holder.hidePostItem);
        holder.commonNameTXT.setOnClickListener(this);

        holder.unhideBTN.setTag(holder.hidePostItem);
        holder.unhideBTN.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        HidePostItem hidePostItem;

        @BindView(R.id.imageCIV)            ImageView imageCIV;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.commonNameTXT)       TextView commonNameTXT;
//        @BindView(R.id.groupTXT)            TextView groupTXT;
        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.unhideBTN)           TextView unhideBTN;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((HidePostItem) v.getTag());
                }
                break;

            case R.id.unhideBTN:
                if(clickListener !=null){
                    clickListener.onUnhideClick((HidePostItem)v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(HidePostItem hidePostItem);
        void onUnhideClick(HidePostItem hidePostItem);
        void onItemLongClick(HidePostItem hidePostItem);
    }
}