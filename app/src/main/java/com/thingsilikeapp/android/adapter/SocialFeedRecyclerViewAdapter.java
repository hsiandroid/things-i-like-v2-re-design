package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.java.NumberFormatter;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;
import com.thingsilikeapp.vendor.android.widget.RoundedRelativeLayout;
import com.thingsilikeapp.vendor.android.widget.image.FixedWidthImageView;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SocialFeedRecyclerViewAdapter extends RecyclerView.Adapter<SocialFeedRecyclerViewAdapter.ViewHolder>
        implements View.OnClickListener {

    private Context context;
    private List<WishListItem> data;
    private LayoutInflater layoutInflater;
    private ClickListener clickListener;

    private boolean enableUserClick = true;
    private int userID;

    public static final int LIST = 1;
    public static final int GRID = 2;
    public static final int HEADER = 3;
    public static int viewType;
    private EventItem eventItem;

    public SocialFeedRecyclerViewAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        userID = UserData.getUserId();
        viewType = GRID;
        setHasStableIds(true);
    }

    public class WishListItemDiffUtilCallback extends DiffUtil.Callback {
        List<WishListItem> oldList;
        List<WishListItem> newList;

        WishListItemDiffUtilCallback(List<WishListItem> newList, List<WishListItem> oldList) {
            this.newList = newList;
            this.oldList = oldList;
        }

        @Override
        public int getOldListSize() {
            return oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).id == (newList.get(newItemPosition).id);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).liker_count == (newList.get(newItemPosition).liker_count);
        }

        @Nullable
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
            WishListItem newContact = newList.get(newItemPosition);
            WishListItem oldContact = oldList.get(oldItemPosition);

            Bundle diff = new Bundle();
            if(newContact.liker_count != oldContact.liker_count){
                diff.putString("liker_count", newContact.liker_count + "");
            }

            if (diff.size()==0){
                return null;
            }
            return diff;
        }
    }

    public void setEnableUserClick(boolean b) {
        enableUserClick = b;
    }

    public void setFeedView(int viewType){
        SocialFeedRecyclerViewAdapter.viewType = viewType;
        notifyDataSetChanged();
    }

    public void setEvent(EventItem eventItem) {
        this.eventItem = eventItem;
        showHeader();
    }

    public void setNewData(List<WishListItem> data) {
        this.data.clear();
        for (WishListItem wishListItem : data) {
            if (isDataUnique(wishListItem) && wishListItem.id != -1) {
                this.data.add(wishListItem.minimal(userID));
            }
        }
        notifyDataSetChanged();
    }

    public void addNewData(List<WishListItem> data) {
        int initialCount = this.data.size();
        int newCount = 0;
        for (WishListItem wishListItem : data) {
            if (isDataUnique(wishListItem)) {
                this.data.add(wishListItem.minimal(userID));
                newCount++;
            }
        }
        notifyItemRangeInserted(initialCount, newCount);
    }

    public void addNewItem(WishListItem data) {
        this.data.add(0, data.minimal(userID));
        notifyItemRangeChanged(0, 1);
    }

    public void updateData(final List<WishListItem> newData, final List<WishListItem> oldData) {

        final WishListItemDiffUtilCallback diffCallback = new WishListItemDiffUtilCallback(oldData, newData);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.data = newData;
        diffResult.dispatchUpdatesTo(SocialFeedRecyclerViewAdapter.this);
    }

    public void updateData(WishListItem data) {
        List<WishListItem> wishListItems = new ArrayList<>();
        for (WishListItem item : this.data) {
            if (data.id == item.id) {
                WishListItem minimalData  = data.minimal(userID);
                minimalData.wishListMin.for_update = true;
                wishListItems.add(minimalData);
            }else{
                wishListItems.add(item);
            }
        }
        updateData(wishListItems, this.data);
    }

    public void removeItem(WishListItem item) {
        for (WishListItem wishListItem : data) {
            if (item.id == (wishListItem.id)) {
                int pos = data.indexOf(wishListItem);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    public void removeItemByID(int id) {
        for (WishListItem wishListItem : data) {
            if (id == (wishListItem.id)) {
                int pos = data.indexOf(wishListItem);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).id;
    }

    public void unFollow(int id) {
        for (WishListItem wishListItem : data) {
            if (id == wishListItem.owner.data.id) {
                int pos = data.indexOf(wishListItem);
                data.remove(pos);
                notifyItemRemoved(pos);
                unFollow(id);
                break;
            }
        }
    }

    public void follow(int id) {
        for (WishListItem wishListItem : data) {
            if (id == wishListItem.owner.data.id) {
                int pos = data.indexOf(wishListItem);
                wishListItem.owner.data.social.data.is_following = "yes";
                notifyItemChanged(pos);
            }
        }
    }

    private boolean isDataUnique(WishListItem newData) {
        return !data.contains(newData);
    }

    public WishListItem getItem(int position){
        if(data.size() >0){
        return data.get(position);
        }else {
            return new WishListItem();
        }
    }

    public void setSelected(WishListItem wishListItems){
        for (WishListItem wishListItem : data){
            if (wishListItem.id == wishListItems.id){
                wishListItem.is_liked = !wishListItems.is_liked;
//                updateData(wishListItem);
                break;
            }
        }
    }

    private void showHeader() {
        WishListItem wishListItem = new WishListItem();
        wishListItem.id = -1;

        if (isDataUnique(wishListItem) && eventItem.id != 0) {
            this.data.add(0, wishListItem);
            notifyItemInserted(0);
        } else {
            removeItemByID(-1);
        }
    }

    public List<WishListItem> getData() {
        return data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        ViewHolder viewHolder;
        switch (type){
            case HEADER:
                viewHolder = new ViewHolder(layoutInflater.inflate(R.layout.adapter_social_feed_header, parent, false)).bindHeaderView();
                ((StaggeredGridLayoutManager.LayoutParams) viewHolder.itemView.getLayoutParams()).setFullSpan(true);
                break;
            case LIST:
                viewHolder = new ViewHolder(layoutInflater.inflate(R.layout.adapter_socialfeed_a, parent, false)).bindListView();
                ((StaggeredGridLayoutManager.LayoutParams) viewHolder.itemView.getLayoutParams()).setFullSpan(false);
                break;
            case GRID:
            default:
                viewHolder = new ViewHolder(layoutInflater.inflate(R.layout.adapter_social_feed_default, parent, false)).bindGridView();
                ((StaggeredGridLayoutManager.LayoutParams) viewHolder.itemView.getLayoutParams()).setFullSpan(false);
        }
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).id == -1) {
            return HEADER;
        } else {
            return viewType;
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.wishListItem = data.get(position);

        if (holder.wishListItem.id == -1) {
            headerViewBinder(holder);
        } else {
            switch (viewType) {
                case LIST:
                    listViewBinder(holder, position);
                    break;
                case GRID:
                    gridViewBinder(holder, position);
                    break;
            }
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()){
            super.onBindViewHolder(holder, position, payloads);
        }else {
            Bundle o = (Bundle) payloads.get(0);
            for (String key : o.keySet()) {
                if(key.equals("liker_count")){
                    switch (viewType) {
                        case LIST:
                            holder.listView.heartCountTXT.setText(String.valueOf(data.get(position).wishListMin.liker_count));
                            break;
                        case GRID:
                            holder.gridView.heartCountTXT.setText(String.valueOf(data.get(position).wishListMin.liker_count));
                            break;
                    }

                }
            }
        }
    }

    private void headerViewBinder(final ViewHolder holder) {
        Glide.with(context)
                .load(eventItem.image)
                .apply(new RequestOptions()
                .fitCenter()
                .dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .listener(holder.requestListener)
                .into(holder.headerView.imageIV);

        holder.headerView.imageIV.autoResize(true);

        holder.headerView.showBTN.setVisibility(eventItem.is_today ? View.VISIBLE : View.GONE);


        holder.headerView.imageIV.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
        holder.headerView.showBTN.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
    }

    private void listViewBinder(final ViewHolder holder, final int position){
        holder.listView.avatarCIV.setTag(R.id.avatarCIV,position);
        holder.listView.commentBTN.setTag(R.id.commentBTN,position);
        holder.listView.commentCountTXT.setTag(R.id.commentCountTXT, position);
        holder.listView.otherOptionBTN.setTag(R.id.otherOptionBTN, position);
        holder.listView.socialFeedCON.setTag(R.id.socialFeedCON, position);
        holder.listView.rePostBTN.setTag(R.id.rePostBTN, position);
        holder.listView.rePostCountTXT.setTag(R.id.rePostCountTXT, position);
        holder.listView.iLikeIV.setTag(R.id.iLikeIV, position);
        holder.listView.shareBTN.setTag(R.id.shareBTN, position);

        holder.listView.heartSB.pressOnTouch(false);

        resetListViewSocialFeed(holder.listView.socialFeedCON);

//        holder.requestListener = new RequestListener<String, GlideDrawable>() {
//            @Override
//            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                return false;
//            }
//
//            @Override
//            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                showListViewData(holder, position);
//                return false;
//            }
//        };

        holder.requestListener = new RequestListener() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                showListViewData(holder, position);
                return false;
            }
        };

        holder.listViewSparkEventListener = new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean buttonState) {
                holder.listView.heartSB.playAnimation();
                if(clickListener != null){
                    clickListener.onHeartClick(data.get(position));
                }
            }

            @Override
            public void onEventAnimationEnd(ImageView button, boolean buttonState) {

            }

            @Override
            public void onEventAnimationStart(ImageView button, boolean buttonState) {

            }
        };

        holder.listView.userNameTXT.setText("");
        holder.listView.iLikeTXT.setText("");
        holder.listView.dateTXT.setText("");
        holder.listView.titleTXT.setText("");
        holder.listView.commentCountTXT.setText("");
        holder.listView.heartCountTXT.setText("");
        holder.listView.rePostCountTXT.setText("");
        holder.listView.socialFeedCON.setAlpha(0.5f);

        holder.listView.userNameTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.light_gray));
        holder.listView.iLikeIV.setImageDrawable(null);

        holder.listView.iLikeIV.autoResize(holder.wishListItem.wishListMin.width == 0);
        holder.listView.iLikeIV.setImageSize(holder.wishListItem.wishListMin.width, holder.wishListItem.wishListMin.height);
        holder.listView.heartSB.setEventListener(holder.listViewSparkEventListener);

        holder.listView.heartSB.setVisibility(View.INVISIBLE);
        holder.listView.commentBTN.setVisibility(View.INVISIBLE);
        holder.listView.rePostBTN.setVisibility(View.INVISIBLE);

        holder.listView.heartSB.setVisibility(View.GONE);
        holder.listView.rePostBTN.setVisibility(View.GONE);
        holder.listView.commentBTN.setVisibility(View.GONE);
        holder.listView.userNameTXT.setVisibility(View.GONE);
        holder.listView.iLikeTXT.setVisibility(View.GONE);
        holder.listView.dateTXT.setVisibility(View.GONE);
        holder.listView.titleTXT.setVisibility(View.GONE);
        holder.listView.commentCountTXT.setVisibility(View.GONE);
        holder.listView.heartCountTXT.setVisibility(View.GONE);

        Glide.with(context)
                .load(holder.wishListItem.wishListMin.like_image)
                .apply(new RequestOptions()
                .fitCenter()
                .placeholder(R.color.light_gray)
                .error(R.color.light_gray)
                .dontAnimate()
                .skipMemoryCache(true)
                .dontTransform()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .listener(holder.requestListener)
                .into(holder.listView.iLikeIV);
    }

    private void gridViewBinder(final ViewHolder holder, final int position){

        holder.gridView.avatarCIV.setTag(R.id.avatarCIV, position);
        holder.gridView.commentBTN.setTag(R.id.commentBTN,position);
        holder.gridView.commentCountTXT.setTag(R.id.commentCountTXT, position);
        holder.gridView.otherOptionBTN.setTag(R.id.otherOptionBTN, position);
        holder.gridView.socialFeedCON.setTag(R.id.socialFeedCON, position);

        holder.gridView.heartSB.pressOnTouch(false);

        resetGridViewSocialFeed(holder.gridView.socialFeedCON);

//        holder.requestListener = new RequestListener<String, GlideDrawable>() {
//            @Override
//            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                return false;
//            }
//
//            @Override
//            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                showGridViewData(holder, position);
//                return false;
//            }
//        };

        holder.requestListener = new RequestListener() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                showGridViewData(holder, position);
                return false;
            }
        };

        holder.gridViewSparkEventListener = new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean buttonState) {
                holder.gridView.heartSB.playAnimation();
                if(clickListener != null){
                    clickListener.onHeartClick(data.get(position));
                }
            }

            @Override
            public void onEventAnimationEnd(ImageView button, boolean buttonState) {

            }

            @Override
            public void onEventAnimationStart(ImageView button, boolean buttonState) {

            }
        };

        holder.gridView.userNameTXT.setText("");
        holder.gridView.iLikeTXT.setText("");
        holder.gridView.dateTXT.setText("");
        holder.gridView.titleTXT.setText("");
        holder.gridView.commentCountTXT.setText("");
        holder.gridView.heartCountTXT.setText("");
        holder.gridView.socialFeedCON.setAlpha(0.5f);

        holder.gridView.userNameTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.light_gray));
        holder.gridView.iLikeIV.setImageDrawable(null);

        holder.gridView.iLikeIV.autoResize(holder.wishListItem.wishListMin.width == 0);
        holder.gridView.iLikeIV.setImageSize(holder.wishListItem.wishListMin.width, holder.wishListItem.wishListMin.height);
        holder.gridView.heartSB.setEventListener(holder.gridViewSparkEventListener);

        holder.gridView.heartSB.setVisibility(View.GONE);
        holder.gridView.commentBTN.setVisibility(View.GONE);
        holder.gridView.userNameTXT.setVisibility(View.GONE);
        holder.gridView.iLikeTXT.setVisibility(View.GONE);
        holder.gridView.dateTXT.setVisibility(View.GONE);
        holder.gridView.titleTXT.setVisibility(View.GONE);
        holder.gridView.commentCountTXT.setVisibility(View.GONE);
        holder.gridView.heartCountTXT.setVisibility(View.GONE);

        Glide.with(context)
                .load(holder.wishListItem.wishListMin.like_image)
                .apply(new RequestOptions()
                .fitCenter()
                .placeholder(R.color.light_gray)
                .error(R.color.light_gray)
                .dontAnimate()
                .skipMemoryCache(true)
                .dontTransform()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .listener(holder.requestListener)
                .into(holder.gridView.iLikeIV);

    }

    public void resetGridViewSocialFeed(View view){
        Drawable colorDrawable;
        ColorDrawable color = new ColorDrawable(ActivityCompat.getColor(context, R.color.text_gray));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            RippleDrawable rippleDrawable = new RippleDrawable(ColorStateList.valueOf(color.getColor()), null, null);
            colorDrawable = rippleDrawable;
        } else {
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.setEnterFadeDuration(200);
            stateListDrawable.setExitFadeDuration(200);
            stateListDrawable.addState(new int[]{android.R.attr.state_pressed}, color);
            colorDrawable = stateListDrawable;
        }
        view.setBackground(colorDrawable);
    }

    public void resetListViewSocialFeed(View view){
        Drawable colorDrawable;
        ColorDrawable color = new ColorDrawable(ActivityCompat.getColor(context, R.color.text_gray));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            RippleDrawable rippleDrawable = new RippleDrawable(ColorStateList.valueOf(color.getColor()), null, null);
            colorDrawable = rippleDrawable;
        } else {
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.setEnterFadeDuration(200);
            stateListDrawable.setExitFadeDuration(200);
            stateListDrawable.addState(new int[]{android.R.attr.state_pressed}, color);
            colorDrawable = stateListDrawable;
        }
        view.setBackground(colorDrawable);
    }

    private void showGridViewData(ViewHolder holder, int viewPosition){
        if(data.size() > viewPosition){
            holder.wishListItem = data.get(viewPosition);
            holder.gridView.socialFeedCON.setAlpha(1f);
//            holder.gridView.heartSB.setChecked(holder.wishListItem.wishListMin.is_liked);
            holder.gridView.otherOptionBTN.setVisibility(holder.wishListItem.wishListMin.owned ? View.GONE : View.VISIBLE);
            holder.gridView.userNameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.wishListItem.wishListMin.name, holder.wishListItem.wishListMin.is_verified, 12));
            holder.gridView.iLikeTXT.setText(holder.wishListItem.wishListMin.category.toUpperCase());
            holder.gridView.dateTXT.setText(holder.wishListItem.wishListMin.time_passed);
            holder.gridView.titleTXT.setText(holder.wishListItem.wishListMin.title);
            holder.gridView.commentCountTXT.setText(NumberFormatter.format(holder.wishListItem.wishListMin.comment_count));
            holder.gridView.heartCountTXT.setText(NumberFormatter.format(holder.wishListItem.wishListMin.liker_count));

            holder.gridView.userNameTXT.setBackground(null);

            holder.gridView.heartSB.setVisibility(View.VISIBLE);
            holder.gridView.commentBTN.setVisibility(View.VISIBLE);

            Glide.with(context)
                    .load(holder.wishListItem.wishListMin.avatar)
                    .apply(new RequestOptions()
                    .placeholder(R.drawable.placeholder_avatar)
                    .error(R.drawable.placeholder_avatar)
                    .fitCenter()
                    .dontAnimate()
                    .skipMemoryCache(true)
                    .dontTransform()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                    .into(holder.gridView.avatarCIV);

            holder.gridView.heartSB.setVisibility(View.VISIBLE);
            holder.gridView.commentBTN.setVisibility(View.VISIBLE);
            holder.gridView.userNameTXT.setVisibility(View.VISIBLE);
            holder.gridView.iLikeTXT.setVisibility(View.VISIBLE);
            holder.gridView.dateTXT.setVisibility(View.VISIBLE);
            holder.gridView.titleTXT.setVisibility(View.VISIBLE);
            holder.gridView.commentCountTXT.setVisibility(View.GONE);
            holder.gridView.heartCountTXT.setVisibility(View.VISIBLE);
        }
    }

    private void showListViewData(ViewHolder holder, int viewPosition){
        if(data.size() > viewPosition){
            holder.wishListItem = data.get(viewPosition);
            holder.listView.socialFeedCON.setAlpha(1f);
            holder.listView.heartSB.setChecked(holder.wishListItem.wishListMin.is_liked);
            holder.listView.otherOptionBTN.setVisibility(holder.wishListItem.wishListMin.owned ? View.GONE : View.VISIBLE);
            holder.listView.userNameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.wishListItem.wishListMin.name, holder.wishListItem.wishListMin.is_verified, 12));
            holder.listView.iLikeTXT.setText(holder.wishListItem.wishListMin.category.toUpperCase());
            holder.listView.dateTXT.setText(holder.wishListItem.wishListMin.time_passed);
            holder.listView.titleTXT.setText(holder.wishListItem.wishListMin.title);

            holder.listView.userNameTXT.setBackground(null);
            holder.listView.heartCountTXT.setText(holder.wishListItem.for_display_liker + "");
//            holder.listView.commentCountTXT.setText(holder.wishListItem.comment_count + "") ;
            holder.listView.rePostCountTXT.setText(holder.wishListItem.repost_count + "");

            holder.listView.commentCountTXT.setVisibility(View.GONE);
            holder.listView.heartSB.setVisibility(View.VISIBLE);
            holder.listView.commentBTN.setVisibility(View.VISIBLE);
            holder.listView.rePostBTN.setVisibility(View.VISIBLE);

            Glide.with(context)
                    .load(holder.wishListItem.wishListMin.avatar)
                    .apply(new RequestOptions()
                    .placeholder(R.drawable.placeholder_avatar)
                    .error(R.drawable.placeholder_avatar)
                    .fitCenter()
                    .dontTransform()
                    .dontAnimate()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                    .into(holder.listView.avatarCIV);

            holder.listView.heartSB.setVisibility(View.VISIBLE);
            holder.listView.rePostBTN.setVisibility(View.VISIBLE);
            holder.listView.commentBTN.setVisibility(View.VISIBLE);
            holder.listView.userNameTXT.setVisibility(View.VISIBLE);
            holder.listView.iLikeTXT.setVisibility(View.VISIBLE);
            holder.listView.dateTXT.setVisibility(View.VISIBLE);
            holder.listView.titleTXT.setVisibility(View.VISIBLE);
            holder.listView.commentCountTXT.setVisibility(View.VISIBLE);
            holder.listView.heartCountTXT.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        WishListItem wishListItem;
        HeaderView headerView;
        GridView gridView;
        ListView listView;

        public class HeaderView {

            @BindView(R.id.imageIV)                     FixedWidthImageView imageIV;
            @BindView(R.id.showBTN)                     View showBTN;

            public HeaderView() {
                ButterKnife.bind(this, view);

                imageIV.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                showBTN.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
            }
        }

        public class GridView {
            @BindView(R.id.iLikeIV)                     FixedWidthImageView iLikeIV;
            @BindView(R.id.iLikeTXT)                    TextView iLikeTXT;
            @BindView(R.id.userNameTXT)                 TextView userNameTXT;
            @BindView(R.id.avatarCIV)                   ImageView avatarCIV;
            @BindView(R.id.dateTXT)                     TextView dateTXT;
            @BindView(R.id.titleTXT)                    TextView titleTXT;
            @BindView(R.id.otherOptionBTN)              View otherOptionBTN;
            @BindView(R.id.commentCountTXT)             TextView commentCountTXT;
            @BindView(R.id.commentBTN)                  View commentBTN;
            @BindView(R.id.socialFeedCON)               RoundedRelativeLayout socialFeedCON;
            @BindView(R.id.heartSB)                     SparkButton heartSB;
            @BindView(R.id.heartCountTXT)               TextView heartCountTXT;

            public GridView() {
                ButterKnife.bind(this, view);

                avatarCIV.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                commentBTN.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                commentCountTXT.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                otherOptionBTN.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                socialFeedCON.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
            }
        }

        public class ListView{
            @BindView(R.id.iLikeIV)                     FixedWidthImageView iLikeIV;
            @BindView(R.id.iLikeTXT)                    TextView iLikeTXT;
            @BindView(R.id.userNameTXT)                 TextView userNameTXT;
            @BindView(R.id.avatarCIV)                   ImageView avatarCIV;
            @BindView(R.id.dateTXT)                     TextView dateTXT;
            @BindView(R.id.titleTXT)                    TextView titleTXT;
            @BindView(R.id.otherOptionBTN)              View otherOptionBTN;
            @BindView(R.id.commentCountTXT)             TextView commentCountTXT;
            @BindView(R.id.commentBTN)                  View commentBTN;
            @BindView(R.id.shareBTN)                    LinearLayout shareBTN;
            @BindView(R.id.socialFeedCON)               View socialFeedCON;
            @BindView(R.id.heartSB)                     SparkButton heartSB;
            @BindView(R.id.heartCountTXT)               TextView heartCountTXT;
            @BindView(R.id.rePostBTN)                   View rePostBTN;
            @BindView(R.id.rePostCountTXT)              TextView rePostCountTXT;
            @BindView(R.id.footerCON)                   View footerCON;

            public ListView(){
                ButterKnife.bind(this, view);
                avatarCIV.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                commentBTN.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                commentCountTXT.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                otherOptionBTN.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                rePostBTN.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                shareBTN.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                rePostCountTXT.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                footerCON.setOnClickListener(SocialFeedRecyclerViewAdapter.this);
                iLikeIV.setOnClickListener(SocialFeedRecyclerViewAdapter.this);

            }
        }

        public ViewHolder bindHeaderView() {
            headerView = new HeaderView();
            return this;
        }

        public ViewHolder bindGridView(){
            gridView = new GridView();
            return this;
        }

        public ViewHolder bindListView(){
            listView = new ListView();
            return this;
        }

        public RequestListener requestListener;
        public SparkEventListener listViewSparkEventListener;
        public SparkEventListener gridViewSparkEventListener;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.avatarCIV:
                if (clickListener != null && enableUserClick) {
                    clickListener.onAvatarClick(getItem((int) v.getTag(R.id.avatarCIV)).owner.data);
                }
                break;
            case R.id.imageIV:
                if (clickListener != null) {
                    clickListener.onHeaderClick(eventItem);
                }
                break;
            case R.id.showBTN:
                if (clickListener != null) {
                    clickListener.onHeaderShowClick(eventItem);
                }
                break;
            case R.id.commentCountTXT:
                if (clickListener != null) {
                    clickListener.onCommentClick(getItem((int) v.getTag(R.id.commentCountTXT)));
                }
                break;
            case R.id.commentBTN:
                if (clickListener != null) {
                    clickListener.onCommentClick(getItem((int) v.getTag(R.id.commentBTN)));
                }
                break;
            case R.id.socialFeedCON:
                if (clickListener != null) {
                    clickListener.onItemClick(getItem((int) v.getTag(R.id.socialFeedCON)));
                }
                break;
            case R.id.iLikeIV:
                if (clickListener != null) {
                    clickListener.onItemClick(getItem((int) v.getTag(R.id.iLikeIV)));
                }
                break;
            case R.id.otherOptionBTN:
                otherOptionClick(v);
                break;
            case R.id.rePostBTN:
                if (clickListener != null) {
                clickListener.onRepostClick(getItem((int) v.getTag(R.id.rePostBTN)));
                }
            break;
            case R.id.rePostCountTXT:
                if (clickListener != null) {
                clickListener.onRepostUsersClick(getItem((int) v.getTag(R.id.rePostCountTXT)));
            }
                break;
            case R.id.shareBTN:
                if (clickListener != null) {
                    clickListener.onShareClick(getItem((int) v.getTag(R.id.shareBTN)));
                }
                break;
            default:
                break;
        }
    }

    private void otherOptionClick(View view) {

        final WishListItem wishListItem = getItem((int) view.getTag(R.id.otherOptionBTN));
        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.social_feed_menu, popup.getMenu());
        popup.getMenu().findItem(R.id.unfollowBTN).setVisible(enableUserClick);
        if(enableUserClick){
            popup.getMenu().findItem(R.id.unfollowBTN).setTitle(wishListItem.owner.data.social.data.is_following.equals("yes") ? "Unfollow" : "Follow" );
        }

        String report = popup.getMenu().findItem(R.id.reportBTN).getTitle().toString();
        SpannableString spannableString = new SpannableString(report);
        spannableString.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(context, R.color.failed)), 0, spannableString.length(), 0);
        popup.getMenu().findItem(R.id.reportBTN).setTitle(spannableString);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.repostBTN:
                            if (clickListener != null) {
                                clickListener.onGrabClick(wishListItem);
                            }
                            break;
                        case R.id.hideBTN:
                            if (clickListener != null){
                                clickListener.onHideClick(wishListItem);
                            }
                            break;
                        case R.id.unfollowBTN:
                            if (clickListener != null) {
                                clickListener.onUnfollowClick(wishListItem);
                            }
                            break;
                        case R.id.reportBTN:
                            if (clickListener != null) {
                                clickListener.onReportClick(wishListItem);
                            }
                            break;
                        case R.id.shareBTN:
                            if (clickListener != null) {
                                clickListener.onShareClick(wishListItem);
                            }
                            break;
                    }

                return false;
            }
        });
        popup.show();
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(WishListItem wishListItem);

        void onCommentClick(WishListItem wishListItem);

        void onAvatarClick(UserItem userItem);

        void onGrabClick(WishListItem wishListItem);

        void onHideClick(WishListItem wishListItem);

        void onUnfollowClick(WishListItem wishListItem);

        void onReportClick(WishListItem wishListItem);

        void onShareClick(WishListItem wishListItem);

        void onHeartClick(WishListItem wishListItem);

        void onRepostClick(WishListItem wishListItem);

        void onRepostUsersClick(WishListItem wishListItem);

        void onHeaderClick(EventItem eventItem);

        void onHeaderShowClick(EventItem eventItem);
    }
} 
