package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.thingsilikeapp.android.fragment.messages.groupChat.FollowerssFragment;
import com.thingsilikeapp.android.fragment.messages.groupChat.FollowingsFragment;
import com.thingsilikeapp.android.fragment.messages.groupChat.PeopleFragment;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;


/**
 * Created by Ken Drew on 11/11/2017.
 */

public class NewCreateMessagesPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;
    private Context context;
    public Callback callback;
    public PeopleFragment peopleFragment;
    public FollowingsFragment followingsFragment;
    public FollowerssFragment followerssFragment;

    public NewCreateMessagesPagerAdapter(FragmentManager fragmentManager, Context c) {
        super(fragmentManager);
        context = c;
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                peopleFragment = PeopleFragment.newInstance();
                peopleFragment.callback = new PeopleFragment.Callback() {
                    @Override
                    public void onSuccess(UserItem userItem) {
                        if (callback != null){
                            callback.onSuccess(userItem);
                        }
                    }
                };
                return peopleFragment;
            case 1:
                followerssFragment = FollowerssFragment.newInstance(UserData.getUserId());
                followerssFragment.callback = new FollowerssFragment.Callback() {
                    @Override
                    public void onSuccess(UserItem userItem) {
                        if (callback != null){
                            callback.onSuccess(userItem);
                        }
                    }
                };
                return followerssFragment;
            case 2:
                followingsFragment = FollowingsFragment.newInstance(UserData.getUserId());
                followingsFragment.callback = new FollowingsFragment.Callback() {
                    @Override
                    public void onSuccess(UserItem userItem) {
                        if (callback != null){
                            callback.onSuccess(userItem);
                        }
                    }
                };
                return followingsFragment;
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_UNCHANGED;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "People";
            case 1:
                return "Followers";
            case 2:
                return "Following";
            default:
                return "Page" + position;

        }

    }

    public interface Callback{
        void onSuccess(UserItem userItem);
    }

}