package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.java.image.AlbumItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlbumAdapter extends BaseAdapter{
	private Context context;
	private List<AlbumItem> data;
    private LayoutInflater layoutInflater;

	public AlbumAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<AlbumItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public AlbumItem getData(int position){
        return data.get(position);
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_album, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.albumItem = data.get(position);
        holder.albumTXT.setText(holder.albumItem.album);

		return convertView;
	}

    public class ViewHolder{
        AlbumItem albumItem;
        @BindView(R.id.albumTXT)    TextView albumTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

} 
