package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.BankItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrendingCRecycleViewAdapter extends RecyclerView.Adapter<TrendingCRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<BankItem> data;
    private LayoutInflater layoutInflater;

	public TrendingCRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<BankItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_exp_trending_c, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bankItem = data.get(position);
        holder.view.setTag(holder.bankItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);

        Picasso.with(context).load(holder.bankItem.avatar).placeholder(R.drawable.placeholder_avatar).error(R.drawable.placeholder_avatar).fit().centerCrop().into(holder.photosIV);}

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        BankItem bankItem;

        @BindView(R.id.photosIV)            ImageView photosIV;
        @BindView(R.id.adapterCON)          View adapterCON;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick(data.get((int)v.getTag()));
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(BankItem bankItem);
        void onItemLongClick(BankItem bankItem);
    }
} 
