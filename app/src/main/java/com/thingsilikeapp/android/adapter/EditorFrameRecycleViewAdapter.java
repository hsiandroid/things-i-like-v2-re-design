package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.FrameItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditorFrameRecycleViewAdapter extends RecyclerView.Adapter<EditorFrameRecycleViewAdapter.ViewHolder>  implements View.OnClickListener {
	private Context context;
	private List<FrameItem> data;
    private LayoutInflater layoutInflater;

	public EditorFrameRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<FrameItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_editor_frame, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.frameItem = data.get(position);

        holder.view.setTag(holder.frameItem);
        holder.view.setOnClickListener(this);

        Glide.with(context)
                .load(holder.frameItem.getImage())
                .apply(new RequestOptions()
                        .dontAnimate())
                .into(holder.frameIMG);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        FrameItem frameItem;

        @BindView(R.id.frameIMG)    ImageView frameIMG;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onFrameSelected((FrameItem) v.getTag());
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onFrameSelected(FrameItem frameItem);
    }
} 
