package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.server.request.comment.CreateCommentRequest;
import com.thingsilikeapp.server.request.comment.DeleteCommentRequest;
import com.thingsilikeapp.server.transformer.comment.CommentSingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SocialFeedRecyclerView2Adapter extends RecyclerView.Adapter<SocialFeedRecyclerView2Adapter.ViewHolder> implements View.OnClickListener,
        View.OnLongClickListener {
    private Context context;
    private List<WishListItem> data;
    private LayoutInflater layoutInflater;
    private boolean enableUserClick = true;

    public SocialFeedRecyclerView2Adapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        EventBus.getDefault().register(this);
    }

    public void setEnableUserClick(boolean b) {
        enableUserClick = b;
    }


    public void setNewData(List<WishListItem> data) {
        this.data.clear();
        for (WishListItem wishListItem : data) {
            if (isDataUnique(wishListItem)) {
                this.data.add(wishListItem);
            }
        }
        notifyDataSetChanged();
    }


    public void addNewData(List<WishListItem> data) {
        int initialCount = this.data.size();
        int newCount = 0;
        for (WishListItem wishListItem : data) {
            if (isDataUnique(wishListItem)) {
                this.data.add(wishListItem);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    public void addNewItem(WishListItem data) {
        this.data.add(0, data);
        notifyItemRangeChanged(0, 1);
    }

    public void updateData(WishListItem data) {
        for (WishListItem item : this.data) {
            if (data.id == item.id) {
                int pos = this.data.indexOf(item);
                this.data.set(pos, data);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    public void removeItem(WishListItem item) {
        for (WishListItem wishListItem : data) {
            if (item.id == (wishListItem.id)) {
                int pos = data.indexOf(wishListItem);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    public void removeItemByID(int id) {
        for (WishListItem wishListItem : data) {
            if (id == (wishListItem.id)) {
                int pos = data.indexOf(wishListItem);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    public void unFollow(int id) {
        for (WishListItem wishListItem : data) {
            if (id == wishListItem.owner.data.id) {
                int pos = data.indexOf(wishListItem);
                data.remove(pos);
                notifyItemRemoved(pos);
                unFollow(id);
                break;
            }
        }
    }

    private boolean isDataUnique(WishListItem newData) {
        for (WishListItem oldData : this.data) {
            if (oldData.id == newData.id) {
                return false;
            }
        }
        return true;
    }

    public List<WishListItem> getData() {
        return data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        ViewHolder viewHolder = new ViewHolder(layoutInflater.inflate(R.layout.adapter_social_feed_defaultv2, parent, false));
        ((StaggeredGridLayoutManager.LayoutParams) viewHolder.itemView.getLayoutParams()).setFullSpan(false);
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.wishListItem = data.get(position);

        Glide.with(context)
                .asBitmap()
                .load(holder.wishListItem.image.data.full_path)
                .apply(new RequestOptions()
                .fitCenter())
                .into(holder.iLikeIV);
    }

    public Object getItemData(int position) {
        return data.get(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        WishListItem wishListItem;

        @BindView(R.id.iLikeIV)                     ImageView iLikeIV;

        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.avatarCIV:
                if (clickListener != null && enableUserClick) {
                    clickListener.onAvatarClick(data.get((int) v.getTag()).owner.data);
                }
                break;
            case R.id.profileBadgeIV:
            case R.id.commentBTN:
                if (clickListener != null) {
                    clickListener.onCommentClick(data.get((int) v.getTag()));
                }
                break;
            case R.id.socialFeedCON:
                if (clickListener != null) {
                    clickListener.onItemClick(data.get((int) v.getTag()));
                }
                break;
            case R.id.otherOptionBTN:
                otherOptionClick(v);
                break;
            default:
                break;
        }
    }

    private void otherOptionClick(View view) {
        final WishListItem wishListItem = data.get((int) view.getTag());

        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.social_feed_menu, popup.getMenu());
        popup.getMenu().getItem(2).setVisible(enableUserClick);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (clickListener != null) {
                    switch (item.getItemId()) {
                        case R.id.repostBTN:
                            clickListener.onGrabClick(wishListItem);
                            break;
                        case R.id.hideBTN:
                            clickListener.onHideClick(wishListItem);
                            break;
                        case R.id.unfollowBTN:
                            clickListener.onUnfollowClick(wishListItem);
                            break;
                        case R.id.reportBTN:
                            clickListener.onReportClick(wishListItem);
                            break;
                    }
                }
                return false;
            }
        });
        popup.show();
    }

    @Override
    public boolean onLongClick(View v) {

        switch (v.getId()) {
            case R.id.avatarCIV:
                if (clickListener != null) {
                    clickListener.onItemLongClick(data.get((int) v.getTag()));
                }
                return true;
        }
        return false;
    }

//    @Subscribe
//    public void onResponse(CreateWishListRequest.ServerResponse responseData) {
//        WishListInfoTransformer wishlistInfoTransformer = responseData.getData(WishListInfoTransformer.class);
//        if(wishlistInfoTransformer.status){
//            addNewItem(wishlistInfoTransformer.wishListItem);
//        }
//    }
//
//    @Subscribe
//    public void onResponse(EditWishListRequest.ServerResponse responseData) {
//        WishListInfoTransformer wishlistInfoTransformer = responseData.getData(WishListInfoTransformer.class);
//        if(wishlistInfoTransformer.status){
//            updateData(wishlistInfoTransformer.wishListItem);
//        }
//    }
//
//    @Subscribe
//    public void onResponse(WishListDeleteRequest.ServerResponse responseData) {
//        WishListInfoTransformer wishlistInfoTransformer = responseData.getData(WishListInfoTransformer.class);
//        if(wishlistInfoTransformer.status){
//            removeItem(wishlistInfoTransformer.wishListItem);
//        }
//    }

    @Subscribe
    public void onResponse(CreateCommentRequest.ServerResponse responseData) {
        CommentSingleTransformer commentSingleTransformer = responseData.getData(CommentSingleTransformer.class);
        if (commentSingleTransformer.status) {
            for (WishListItem wishListItem : data) {
                if (commentSingleTransformer.data.wishlist_id == wishListItem.id) {
                    int pos = data.indexOf(wishListItem);
                    wishListItem.comment_count = wishListItem.comment_count + 1;
                    data.set(pos, wishListItem);
                    notifyItemChanged(pos);
                    break;
                }
            }
        }
    }

    @Subscribe
    public void onResponse(DeleteCommentRequest.ServerResponse responseData) {
        CommentSingleTransformer commentSingleTransformer = responseData.getData(CommentSingleTransformer.class);
        if (commentSingleTransformer.status) {
            for (WishListItem wishListItem : data) {
                if (commentSingleTransformer.data.wishlist_id == wishListItem.id) {
                    int pos = data.indexOf(wishListItem);
                    wishListItem.comment_count = wishListItem.comment_count - 1;
                    data.set(pos, wishListItem);
                    notifyItemChanged(pos);
                    break;
                }
            }
        }
    }

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(WishListItem wishListItem);

        void onItemLongClick(WishListItem wishListItem);

        void onCommentClick(WishListItem wishListItem);

        void onAvatarClick(UserItem userItem);

        void onGrabClick(WishListItem wishListItem);

        void onHideClick(WishListItem wishListItem);

        void onUnfollowClick(WishListItem wishListItem);

        void onReportClick(WishListItem wishListItem);
    }
} 
