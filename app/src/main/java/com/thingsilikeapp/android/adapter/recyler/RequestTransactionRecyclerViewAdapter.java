package com.thingsilikeapp.android.adapter.recyler;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListTransactionItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class RequestTransactionRecyclerViewAdapter extends RecyclerView.Adapter<RequestTransactionRecyclerViewAdapter.ViewHolder>  implements
        View.OnClickListener{

	private Context context;
	private List<WishListTransactionItem> data;
    private LayoutInflater layoutInflater;

	public RequestTransactionRecyclerViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<WishListTransactionItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void removeData(WishListTransactionItem item) {
        for (WishListTransactionItem oldItem : data) {
            if (item.id == (oldItem.id)) {
                int pos = data.indexOf(oldItem);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    public void updateData(WishListTransactionItem commentItem){
        for(WishListTransactionItem item : data){
             if(commentItem.id == item.id){
                int pos = data.indexOf(item);
                data.set(pos, commentItem);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    public void addNewData(List<WishListTransactionItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(WishListTransactionItem commentItem :  data){
            if(isDataUnique(commentItem)){
                this.data.add(commentItem);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(WishListTransactionItem newData){
        for(WishListTransactionItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_viewer_request, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.wishListTransactionItem = data.get(position);

        holder.iLikeIV.setOnClickListener(this);
        holder.avatarCIV.setOnClickListener(this);

        holder.declineTXT.setOnClickListener(this);
        holder.acceptTXT.setOnClickListener(this);

        holder.iLikeIV.setTag(R.id.iLikeIV, holder.wishListTransactionItem);
        holder.avatarCIV.setTag(R.id.avatarCIV, holder.wishListTransactionItem.sender.data);
        holder.declineTXT.setTag(holder.wishListTransactionItem);
        holder.acceptTXT.setTag(holder.wishListTransactionItem);

        holder.descriptionTXT.setText(formatDescription(holder, position));
        holder.descriptionTXT.setMovementMethod(LinkMovementMethod.getInstance());

        holder.dateTXT.setText(holder.wishListTransactionItem.time_passed);

        Glide.with(context)
                .load(holder.wishListTransactionItem.image.data.full_path)
                .apply(new RequestOptions()

                        .dontAnimate())
                .into(holder.iLikeIV);

        Glide.with(context)
                .load(holder.wishListTransactionItem.sender.data.getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate())
                .into(holder.avatarCIV);
    }

    public SpannableString formatDescription(final ViewHolder holder, final int position){
        String  extraMessage = " just sent you ";
        holder.extraMessageTXT.setVisibility(View.VISIBLE);
        holder.extraMessageTXT.setText("Have you received the actual item?");

        String userName = holder.wishListTransactionItem.sender.data.name;

        String finalMessage = userName + extraMessage + holder.wishListTransactionItem.title;
        SpannableString spannableString = new SpannableString(finalMessage);

        ClickableSpan nameSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onAvatarClick(data.get(position).sender.data);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_display_name)));
            }
        };
        spannableString.setSpan(nameSpan, 0, userName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan productSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onItemClick(data.get(position));
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.colorPrimary));
            }
        };

        spannableString.setSpan(productSpan, userName.length() + extraMessage.length(), finalMessage.length(), Spanned.SPAN_INTERMEDIATE);

        return spannableString;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        WishListTransactionItem wishListTransactionItem;

        @BindView(R.id.iLikeIV)             ImageView iLikeIV;
        @BindView(R.id.declineTXT)          TextView declineTXT;
        @BindView(R.id.acceptTXT)           TextView acceptTXT;
        @BindView(R.id.descriptionTXT)      TextView descriptionTXT;
        @BindView(R.id.extraMessageTXT)     TextView extraMessageTXT;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.avatarCIV)           ImageView avatarCIV;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            switch (v.getId()){
                case R.id.avatarCIV:
                    clickListener.onAvatarClick((UserItem) v.getTag(R.id.avatarCIV));
                    break;
                case R.id.acceptTXT:
                    clickListener.onYesClick((WishListTransactionItem) v.getTag());
                    break;
                case R.id.declineTXT:
                    clickListener.onNoClick((WishListTransactionItem) v.getTag());
                    break;
                case R.id.iLikeIV:
                    clickListener.onItemClick((WishListTransactionItem) v.getTag(R.id.iLikeIV));
                    break;
            }
        }
    }


    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(WishListTransactionItem wishListTransactionItem);
        void onAvatarClick(UserItem userItem);
        void onYesClick(WishListTransactionItem wishListTransactionItem);
        void onNoClick(WishListTransactionItem wishListTransactionItem);
    }
} 
