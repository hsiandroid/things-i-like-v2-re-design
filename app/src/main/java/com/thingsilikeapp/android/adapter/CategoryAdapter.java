package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.CategoryItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends BaseAdapter implements View.OnClickListener, Filterable{
	private Context context;
	private List<CategoryItem> data;
    private List<CategoryItem> originalData;
    private LayoutInflater layoutInflater;
    private ValueFilter valueFilter;
    private String searchText = "";

	public CategoryAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<CategoryItem> data){
        this.data = data;
        originalData = null;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_category, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.view.setOnClickListener(this);
        holder.categoryItem = data.get(position);

        if (!holder.categoryItem.title.equalsIgnoreCase("My Moments")){
            holder.subCategoryTXT.setVisibility(holder.categoryItem.parent.equals("") ? View.GONE : View.VISIBLE);
            highlightSearchText(holder.categoryTXT, holder.categoryItem.title, searchText);
            highlightSearchText(holder.subCategoryTXT, holder.categoryItem.parent, searchText);

            switch (holder.categoryItem.title){
                case "Travel":
                    Picasso.with(context).load(R.drawable.category_travel).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
                case "Birthday":
                    Picasso.with(context).load(R.drawable.category_birthday).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
                case "Wedding":
                    Picasso.with(context).load(R.drawable.category_wedding).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
                case "Christmas":
                    Picasso.with(context).load(R.drawable.category_christmas).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
                case "Anniversary":
                    Picasso.with(context).load(R.drawable.category_anniversary).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
                case "Thanksgiving":
                    Picasso.with(context).load(R.drawable.category_thanksgiving).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
                case "Valentines":
                    Picasso.with(context).load(R.drawable.category_rose).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
                case "I Just Like It":
                    Picasso.with(context).load(R.drawable.category_i_just_like_it).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
                case "Others":
                    Picasso.with(context).load(R.drawable.category_graduation).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
                case "Likee":
                    Picasso.with(context).load(R.drawable.category_lykee).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
                case "Christmas Gift":
                    Picasso.with(context).load(R.drawable.category_christmas_gift).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
                case "Birthday Gift":
                    Picasso.with(context).load(R.drawable.category_birthday_gift).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
                case "Items I Want":
                    Picasso.with(context).load(R.drawable.category_items_i_want).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                    break;
               default:
                   Picasso.with(context).load(holder.categoryItem.image).centerCrop().fit().placeholder(R.color.headerColor).into(holder.categoryIMG);
                   break;
            }
        }



        return convertView;
	}

    public void highlightSearchText(TextView textView, String full, String search){
        if(search == null){
            highlightSearchTextDefault(textView, full);
            return;
        }

        String fullTXT = full.toLowerCase();
        String searchTXT = search.toLowerCase();

        if(fullTXT.contains(searchTXT)){
            int startPos = fullTXT.indexOf(searchTXT);
            int endPos = startPos + searchTXT.length();

            Spannable spannable = new SpannableString(full);
            spannable.setSpan(new BackgroundColorSpan(ActivityCompat.getColor(context, R.color.colorAccent)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(context, R.color.white)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setText(spannable);
        }else{
            highlightSearchTextDefault(textView, full);
        }

    }

    public void highlightSearchTextDefault(TextView textView, String fullTxt){
        textView.setText(fullTxt);
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).categoryItem);
        }
    }

    public class ViewHolder{
        CategoryItem categoryItem;

        @BindView(R.id.categoryTXT)     TextView categoryTXT;
        @BindView(R.id.subCategoryTXT)  TextView subCategoryTXT;
        @BindView(R.id.categoryIMG)     ImageView categoryIMG;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(CategoryItem categoryItem);
    }

    @Override
    public Filter getFilter() {
        if(valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }


    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (originalData == null){
                originalData = data;
            }

            FilterResults results = new FilterResults();
            if(constraint == null && constraint.length() == 0){
                results.count = originalData.size();
                results.values = originalData;
            }else{
                ArrayList<CategoryItem> filterList = new ArrayList<>();
                for(int i=0; i < originalData.size();i++){
                    CategoryItem categoryItem = originalData.get(i);
                    if((categoryItem.title.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(categoryItem);
                    }else if((categoryItem.parent.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(categoryItem);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data = (List<CategoryItem>) results.values;
            searchText = constraint.toString();
            notifyDataSetChanged();
        }
    }

} 
