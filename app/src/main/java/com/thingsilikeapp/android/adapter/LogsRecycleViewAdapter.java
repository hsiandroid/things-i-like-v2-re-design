package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.LogsItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LogsRecycleViewAdapter extends RecyclerView.Adapter<LogsRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<LogsItem> data;
    private LayoutInflater layoutInflater;

	public LogsRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<LogsItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<LogsItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(LogsItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(LogsItem newData){
        for(LogsItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_encashment_logs, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.logsItem = data.get(position);
        holder.view.setTag(holder.logsItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);

        holder.adapterCON.setTag(holder.logsItem);
        holder.adapterCON.setOnClickListener(this);

        holder.reamarksTXT.setText(holder.logsItem.remarks);
        holder.dateTXT.setText(holder.logsItem.createdAt.timePassed);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        LogsItem logsItem;

        @BindView(R.id.remarksTXT)          TextView reamarksTXT;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.adapterCON)          View adapterCON;


        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((LogsItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public LogsItem getItem(int position){
        return getData().get(position);
    }

    public List<LogsItem> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }


    public interface ClickListener {
        void onItemClick(LogsItem logsItem);
        void onItemLongClick(LogsItem logsItem);
        void onEditClick(LogsItem logsItem);
        void onRemoveClick(LogsItem logsItem);
    }
} 
