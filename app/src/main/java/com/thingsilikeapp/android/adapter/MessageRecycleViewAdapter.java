package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.ChatItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageRecycleViewAdapter extends RecyclerView.Adapter<MessageRecycleViewAdapter.ViewHolder>  implements View.OnClickListener {
	private Context context;
	private List<ChatItem> data;
    private LayoutInflater layoutInflater;

	public MessageRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<ChatItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_message, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.chatItem = data.get(position);

        holder.view.setTag(position);
        holder.view.setOnClickListener(this);
        holder.adapterCON.setTag(holder.chatItem);
        holder.adapterCON.setOnClickListener(this);
        holder.nameTXT.setText(holder.chatItem.title);
//        holder.messageTXT.setText(holder.chatItem.latestMessage.data.content);
//        holder.timeTXT.setText(holder.chatItem.latestMessage.data.info.data.dateCreated.timePassed);
//        holder.onlineBTN.setBackground(holder.chatItem.is_online ? ActivityCompat.getDrawable(getContext(), R.drawable.bg_circle_online) : ActivityCompat.getDrawable(getContext(), R.drawable.bg_circle_offline));

//        Picasso.with(context)
//                .load(holder.chatItem.info.data.avatar.fullPath)
//                .placeholder(R.drawable.placeholder_avatar)
//                .into(holder.profileCIV);

//        holder.titleTXT.setText(holder.chatItem.title);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ChatItem chatItem;

        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.messageTXT)          TextView messageTXT;
        @BindView(R.id.timeTXT)             TextView timeTXT;
        @BindView(R.id.profileCIV)          ImageView profileCIV;
        @BindView(R.id.onlineBTN)           View onlineBTN;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((ChatItem) v.getTag());
                }
                break;
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(ChatItem chatItem);
    }
} 
