package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.BlockUserItem;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BlockUserRecycleViewAdapter extends RecyclerView.Adapter<BlockUserRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
    private Context context;
    private List<BlockUserItem> data;
    private LayoutInflater layoutInflater;

    public BlockUserRecycleViewAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }


    public void setNewData(List<BlockUserItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<BlockUserItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(BlockUserItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(BlockUserItem newData){
        for(BlockUserItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_block_user, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.blockUserItem = data.get(position);
        holder.view.setTag(holder.blockUserItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.adapterCON.setTag(holder.blockUserItem);
        holder.adapterCON.setOnClickListener(this);

        Glide.with(context)
                .load(holder.blockUserItem.user.data.image)
                .apply(new RequestOptions()
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true))
                .into(holder.imageCIV);

        holder.nameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.blockUserItem.user.data.name, holder.blockUserItem.user.data.isVerified, 12));
        holder.commonNameTXT.setText( holder.blockUserItem.user.data.commonName);

        holder.nameTXT.setTag(holder.blockUserItem);
        holder.nameTXT.setOnClickListener(this);

        holder.commonNameTXT.setTag(holder.blockUserItem);
        holder.commonNameTXT.setOnClickListener(this);

        holder.unblockBTN.setTag(holder.blockUserItem);
        holder.unblockBTN.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        BlockUserItem blockUserItem;

        @BindView(R.id.imageCIV)            ImageView imageCIV;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.commonNameTXT)       TextView commonNameTXT;
        @BindView(R.id.groupTXT)            TextView groupTXT;
        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.unblockBTN)          TextView unblockBTN;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((BlockUserItem) v.getTag());
                }
                break;
            case R.id.unblockBTN:
                if(clickListener !=null){
                    clickListener.onUnblockClick((BlockUserItem)v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick((BlockUserItem)v.getTag());
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(BlockUserItem blockUserItem);
        void onUnblockClick(BlockUserItem blockUserItem);
        void onItemLongClick(BlockUserItem blockUserItem);

    }
}