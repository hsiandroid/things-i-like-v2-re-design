package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.GetRateItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrencySpinnerAdapter extends ArrayAdapter {
	private Context context;
	private List<GetRateItem> data;
    private LayoutInflater layoutInflater;

	public CurrencySpinnerAdapter(Context context) {
        super(context, R.layout.custom_spinner_items);
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<GetRateItem> data){
        this.data = data;
        notifyDataSetChanged();

    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.custom_spinner_items, parent, false);

        TextView spinner = convertView.findViewById(R.id.textView);
        spinner.setText(data.get(position).countryName + " (" + data.get(position).countryCode + ")");
        spinner.setTextSize(14);
        return spinner;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.custom_spinner_items, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.getRateItem = data.get(position);
        holder.textView.setText(holder.getRateItem.countryName + " (" + holder.getRateItem.countryCode + ")");
        holder.textView.setTextColor(ActivityCompat.getColor(getContext(),R.color.text_gray));
        holder.textView.setTextSize(14);
		return convertView;
	}

	public class ViewHolder{
        GetRateItem getRateItem;

        @BindView(R.id.textView)  TextView textView;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}
} 
