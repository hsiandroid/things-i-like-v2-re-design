package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.report.ReportRequest;
import com.thingsilikeapp.server.request.social.FollowRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.social.UpdateGroupRequest;
import com.thingsilikeapp.server.transformer.report.ReportTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventRecycleViewAdapter extends RecyclerView.Adapter<EventRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener{

	private Context context;
	private List<UserItem> data;
    private LayoutInflater layoutInflater;
    private int ownedId = 0;

    public EventRecycleViewAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        EventBus.getDefault().register(this);
        ownedId = UserData.getUserId();
    }

    public void setNewData(List<UserItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<UserItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(UserItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(UserItem newData){
        for(UserItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    public void reset(){
        this.data = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void removeItem(UserItem data) {
        for (UserItem item : this.data) {
            if (data.id == (item.id)) {
                int pos = this.data.indexOf(item);
                this.data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    public void updateData(UserItem data){
        for(UserItem item : this.data){
             if(data.id == item.id){
                int pos = this.data.indexOf(item);
                this.data.set(pos, data);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    public List<UserItem> getData(){
        return data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_following_event, parent, false));
    }

    public UserItem getItem(int position){
        return data.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.userItem = data.get(position);

        Glide.with(context)
                .load(holder.userItem.getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true))
                .into(holder.imageCIV);

        holder.nameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.userItem.name, holder.userItem.is_verified, 12));
        holder.commonNameTXT.setText(holder.userItem.common_name);

        holder.nameTXT.setTag(holder.userItem);
        holder.nameTXT.setOnClickListener(this);

        holder.commonNameTXT.setTag(holder.userItem);
        holder.commonNameTXT.setOnClickListener(this);

        holder.imageCIV.setTag(R.id.imageCIV, holder.userItem);
        holder.imageCIV.setOnClickListener(this);

        if (StringFormatter.isEmpty(holder.userItem.social.data.group)) {
            holder.groupTXT.setVisibility(View.GONE);
        } else {
            holder.groupTXT.setVisibility(View.VISIBLE);
            holder.groupTXT.setText(holder.userItem.social.data.group.toUpperCase());
        }

        if (holder.userItem.is_event_greeted) {
            setUpGreetButton(
                    holder.greetTXT,
                    "GREET!",
                    R.drawable.bg_follow_gray,
                    R.color.text_gray,
                    R.drawable.icon_clock_gray);
            holder.checkIMG.setVisibility(View.VISIBLE);
        } else {
            setUpGreetButton(
                    holder.greetTXT,
                    "GREET!",
                    R.drawable.bg_unfollow,
                    R.color.colorPrimary,
                    R.drawable.icon_clock);
            holder.checkIMG.setVisibility(View.GONE);
        }


        holder.greetTXT.setTag(holder.userItem);
        holder.greetTXT.setOnClickListener(this);
    }

    private void setUpGreetButton(TextView greetTXT, String text, int background, int textColor, int drawable) {
        greetTXT.setText(text);
        greetTXT.setBackground(ActivityCompat.getDrawable(context, background));
        greetTXT.setTextColor(ActivityCompat.getColor(context, textColor));
        greetTXT.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{
        UserItem userItem;

        @BindView(R.id.imageCIV)            ImageView imageCIV;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.commonNameTXT)       TextView commonNameTXT;
        @BindView(R.id.groupTXT)            TextView groupTXT;
        @BindView(R.id.greetTXT)            TextView greetTXT;
        @BindView(R.id.checkIMG)            View checkIMG;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.greetTXT:
                onCommandClicked((UserItem) v.getTag());
                break;
            case R.id.nameTXT:
            case R.id.commonNameTXT:
                onUserClick((UserItem) v.getTag());
                break;
            case R.id.imageCIV:
                onUserClick((UserItem) v.getTag(R.id.imageCIV));
                break;
        }
    }

    private void onCommandClicked(UserItem userItem){
        if(clickListener != null){
            userItem.is_loading_follow = true;
            updateData(userItem);
            clickListener.onCommandClick(userItem);
        }
    }

    private void onUserClick(UserItem userItem){
        ((RouteActivity)context).startProfileActivity(userItem.id);
        if(clickListener != null){
            clickListener.onUserClick(userItem);
        }
    }

    @Subscribe
    public void onResponse(FollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
            updateData(userTransformer.userItem);
            UserData.updateStatistic(1);
        }
    }

    @Subscribe
    public void onResponse(ReportRequest.ServerResponse responseData) {
        ReportTransformer reportTransformer = responseData.getData(ReportTransformer.class);
        if (reportTransformer.status) {
            ToastMessage.show(context, reportTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(context, reportTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(UpdateGroupRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
            ToastMessage.show(context,userTransformer.msg, ToastMessage.Status.SUCCESS);
            updateData(userTransformer.userItem);
            UserData.updateStatistic(1);
        }else {
            ToastMessage.show(context,userTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
            updateData(userTransformer.userItem);
            UserData.updateStatistic(-1);
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onCommandClick(UserItem userItem);
        void onUserClick(UserItem userItem);
    }
} 
