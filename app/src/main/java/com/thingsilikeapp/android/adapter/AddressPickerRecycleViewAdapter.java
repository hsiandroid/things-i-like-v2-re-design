package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.AddressBookItem;
import com.thingsilikeapp.data.model.BankCardItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddressPickerRecycleViewAdapter extends RecyclerView.Adapter<AddressPickerRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener{
	private Context context;
	private List<AddressBookItem> data;
    private LayoutInflater layoutInflater;

	public AddressPickerRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<AddressBookItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void setItemSelected(int id){
        for(AddressBookItem addressBookItem :  data){
            addressBookItem.selected = addressBookItem.id == id;
        }
        notifyDataSetChanged();
    }

    public int getSelectedID(){
        for (AddressBookItem addressBookItem : data){
            if (addressBookItem.selected){
                return addressBookItem.id;
            }
        }
        return 0;
    }

    public void setDefaultSelected(){
        for(AddressBookItem addressBookItem :  data){
            addressBookItem.selected = addressBookItem.is_default.equals("yes");
        }
        notifyDataSetChanged();
    }

    public void addNewData(List<AddressBookItem> data){
        for(AddressBookItem addressBookItem :  data){
            this.data.add(addressBookItem);
        }
        notifyDataSetChanged();
    }

    public void removeData(AddressBookItem item){
        for(AddressBookItem addressBookItem : data){
            if(item.id == addressBookItem.id){
                data.remove(addressBookItem);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void removeData(String id){
        for(AddressBookItem addressBookItem : data){
            if(id.equals(addressBookItem.id)){
                data.remove(addressBookItem);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public List<AddressBookItem> getData(){
        return data;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_address_picker, parent, false));
    }

    public AddressBookItem getItem(int position){
        return data.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.addressBookItem = data.get(position);

        holder.view.setTag(position);
        holder.addressBookRDBTN.setTag(position);

        holder.view.setOnClickListener(this);
        holder.addressBookRDBTN.setOnClickListener(this);

        holder.addressBookRDBTN.setChecked(holder.addressBookItem.selected);
        holder.nameTXT.setText(holder.addressBookItem.address_label);
        holder.contactTXT.setText(holder.addressBookItem.phone_number);
        holder.addressTXT.setText(holder.addressBookItem.street_address + " " + holder.addressBookItem.city + ", "
                + holder.addressBookItem.state + ", " + holder.addressBookItem.country);

        if (holder.addressBookItem.selected){
            holder.addressBookRDBTN.setChecked(true);
        }else{
            holder.addressBookRDBTN.setChecked(false);
        }

        holder.addressBookRDBTN.setEnabled(false);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        AddressBookItem addressBookItem;

        @BindView(R.id.addressBookRDBTN)                RadioButton addressBookRDBTN;
        @BindView(R.id.nameTXT)                         TextView nameTXT;
        @BindView(R.id.contactTXT)                      TextView contactTXT;
        @BindView(R.id.addressTXT)                      TextView addressTXT;

        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addressBookRDBTN:
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick(data.get((int)v.getTag()));
                }
                break;
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(AddressBookItem addressBookItem);
    }
} 
