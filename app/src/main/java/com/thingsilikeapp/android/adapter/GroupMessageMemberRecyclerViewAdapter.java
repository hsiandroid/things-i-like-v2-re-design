package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.ChatUserModel;
import com.thingsilikeapp.data.preference.UserData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupMessageMemberRecyclerViewAdapter extends RecyclerView.Adapter<GroupMessageMemberRecyclerViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<ChatUserModel> data;
    private LayoutInflater layoutInflater;
    private int myId;

	public GroupMessageMemberRecyclerViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        myId = UserData.getUserId();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<ChatUserModel> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<ChatUserModel> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(ChatUserModel item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(ChatUserModel newData){
        for(ChatUserModel oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_group_chat_member, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.chatUserModel = data.get(position);
        holder.view.setTag(holder.chatUserModel);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);

        holder.adapterCON.setTag(holder.chatUserModel);

        holder.nameTXT.setText(holder.chatUserModel.nickname);
        holder.positionTXT.setText(holder.chatUserModel.role);

        Picasso.with(context)
                .load(holder.chatUserModel.author.data.info.data.avatar.fullPath)
                .placeholder(R.drawable.placeholder_avatar)
                .into(holder.profileCIV);

        if(holder.chatUserModel.userId == myId){
            holder.moreBTN.setVisibility(View.GONE);
        }else {
            holder.moreBTN.setVisibility(View.VISIBLE);
        }
        holder.moreBTN.setVisibility(View.GONE);
        holder.moreBTN.setTag(position);
        holder.moreBTN.setOnClickListener(this);
        holder.profileCIV.setTag(holder.chatUserModel);
        holder.profileCIV.setOnClickListener(this);

	}

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ChatUserModel chatUserModel;

        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.positionTXT)         TextView positionTXT;
        @BindView(R.id.profileCIV)          ImageView profileCIV;
        @BindView(R.id.moreBTN)             ImageView moreBTN;
        @BindView(R.id.onlineBTN)           View onlineBTN;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.moreBTN:
                showOtherOption(v);
                break;
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onMenteeClick((ChatUserModel) v.getTag());
                }
                break;
            case R.id.profileCIV:
                if (clickListener != null){
                    clickListener.onAvatarClick((ChatUserModel) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
//        if(clickListener != null){
//            clickListener.onItemLongClick(data.get((int)v.getTag()));
//        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public List<ChatUserModel> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }

    public ChatUserModel getItem(int position){
        return getData().get(position);
    }

    public void showOtherOption(View v){
        final ChatUserModel chatUserModel = getItem((int) v.getTag());
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.promote:
                        if (clickListener != null){
                            clickListener.onPromote(chatUserModel);
                        }
                        return true;
                    case R.id.remove:
                        if (clickListener != null){
                            clickListener.onRemoveClick(chatUserModel);
                        }
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.inflate(R.menu.gc_popup);
        popup.show();
    }

    public interface ClickListener {
        void onMenteeClick(ChatUserModel chatUserModel);
        void onPromote(ChatUserModel chatUserModel);
        void onRemoveClick(ChatUserModel chatUserModel);
        void onAvatarClick(ChatUserModel mentorshipUserModel);
    }
}