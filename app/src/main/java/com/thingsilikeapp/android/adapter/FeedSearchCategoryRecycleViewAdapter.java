package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.FeedSearchCategoryItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedSearchCategoryRecycleViewAdapter extends RecyclerView.Adapter<FeedSearchCategoryRecycleViewAdapter.ViewHolder>  implements View.OnClickListener {
	private Context context;
	private List<FeedSearchCategoryItem> data;
    private LayoutInflater layoutInflater;

	public FeedSearchCategoryRecycleViewAdapter(Context context) {
		this.context = context;
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<FeedSearchCategoryItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_feed_search_category, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.feedSearchCategoryItem = data.get(position);

        holder.view.setTag(holder.feedSearchCategoryItem);
        holder.view.setOnClickListener(this);


        holder.nameTXT.setText(holder.feedSearchCategoryItem.name);
        holder.descriptionTXT.setText(holder.feedSearchCategoryItem.description);

        Glide.with(context)
                .load(holder.feedSearchCategoryItem.icon)
                .apply(new RequestOptions()
                .placeholder(R.drawable.placeholder_avatar))
                .into(holder.iconIV);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        FeedSearchCategoryItem feedSearchCategoryItem;

        @BindView(R.id.iconIV)                        ImageView iconIV;
        @BindView(R.id.nameTXT)                       TextView nameTXT;
        @BindView(R.id.descriptionTXT)                TextView descriptionTXT;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((FeedSearchCategoryItem)v.getTag());
        }
    }


    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(FeedSearchCategoryItem feedSearchCategoryItem);
    }
} 
