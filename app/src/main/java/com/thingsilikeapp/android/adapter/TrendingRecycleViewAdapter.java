package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.vendor.android.widget.image.FixedWidthImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrendingRecycleViewAdapter extends RecyclerView.Adapter<TrendingRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<WishListItem> data;
    private LayoutInflater layoutInflater;

	public TrendingRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void addNewData(List<WishListItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(WishListItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(WishListItem newData){
        for(WishListItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    public void setNewData(List<WishListItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_exp_trending, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.wishListItem = data.get(position);
        holder.view.setTag(holder.wishListItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.titleTXT.setText(holder.wishListItem.title);
//        Picasso.with(context).load(holder.wishListItem.image.data.full_path).placeholder(R.drawable.placeholder_avatar).error(R.drawable.placeholder_avatar).fit().centerCrop().into(holder.photosIV);
//	    Picasso.with(context).load(holder.wishListItem.image.data.thumb_path).placeholder(R.color.light_gray).error(R.color.light_gray).fit().centerCrop().into(holder.photosIV);
        holder.photosIV.setImageDrawable(null);
        holder.photosIV.autoResize(Integer.parseInt(holder.wishListItem.image.data.width) == 0);
        holder.photosIV.setImageSize(Integer.parseInt(holder.wishListItem.image.data.width), Integer.parseInt(holder.wishListItem.image.data.height));
        Glide.with(context)
                .load(holder.wishListItem.image.data.thumb_path)
                .apply(new RequestOptions()
                        .fitCenter()
                        .placeholder(R.color.light_gray)
                        .error(R.color.light_gray)
                        .dontAnimate()
                        .skipMemoryCache(true)
                        .dontTransform()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(holder.photosIV);
	    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        WishListItem wishListItem;

        @BindView(R.id.photosIV)            FixedWidthImageView photosIV;
        @BindView(R.id.titleTXT)            TextView titleTXT;
        @BindView(R.id.adapterCON)          View adapterCON;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((WishListItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(WishListItem wishListItem);
        void onItemLongClick(WishListItem wishListItem);
    }
}