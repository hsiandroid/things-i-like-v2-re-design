package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.GemTransactionHistoryItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GemTransactionHistoryRecycleViewAdapter extends RecyclerView.Adapter<GemTransactionHistoryRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<GemTransactionHistoryItem> data;
    private LayoutInflater layoutInflater;

	public GemTransactionHistoryRecycleViewAdapter(Context context) {
		this.context = context;
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<GemTransactionHistoryItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_gem_transaction_history, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.gemTransactionHistoryItem = data.get(position);

        holder.view.setTag(position);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);


        holder.titleTXT.setText(holder.gemTransactionHistoryItem.title);
        holder.dateTXT.setText(holder.gemTransactionHistoryItem.date + "Ref No. " + holder.gemTransactionHistoryItem.reference_number);

        holder.currencyFromTXT.setText(holder.gemTransactionHistoryItem.currency_from + " to ");
        holder.currencyToTXT.setText(holder.gemTransactionHistoryItem.currency_to);

        if (holder.gemTransactionHistoryItem.amount.equalsIgnoreCase("")){

        }else {
            holder.amountTXT.setText("+ " + holder.gemTransactionHistoryItem.amount);
        }
        holder.amountTXT.setVisibility(holder.gemTransactionHistoryItem.amount.equalsIgnoreCase("") ? View.GONE : View.VISIBLE);
        holder.exchangeCON.setVisibility(holder.gemTransactionHistoryItem.amount.equalsIgnoreCase("") ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        GemTransactionHistoryItem gemTransactionHistoryItem;

//        @BindView(R.id.moduleCIV) ImageView imageView;
        @BindView(R.id.titleTXT)                    TextView titleTXT;
        @BindView(R.id.dateTXT)                     TextView dateTXT;
        @BindView(R.id.amountTXT)                   TextView amountTXT;
        @BindView(R.id.currencyFromTXT)             TextView currencyFromTXT;
        @BindView(R.id.currencyToTXT)               TextView currencyToTXT;
        @BindView(R.id.exchangeCON)                 View exchangeCON;
        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((int)v.getTag(), v);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick((int)v.getTag(), v);
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
} 
