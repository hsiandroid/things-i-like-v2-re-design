package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Switch;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.SettingItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<SettingItem> data;
    private LayoutInflater layoutInflater;

	public SettingAdapter(Context context, List<SettingItem> data) {
		this.context = context;
		this.data = data;
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<SettingItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void setSwitchClicked(int id){
        for(SettingItem settingItem :  data){
            if (settingItem.id == id){
                settingItem.enable = !settingItem.enable;
                if(clickListener != null){
                    clickListener.onNotificationChanged(settingItem.enable);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void setSwitchClicked(int id, boolean b){
        for(SettingItem settingItem :  data){
            if (settingItem.id == id){
                settingItem.enable = b;
                if(clickListener != null){
                    clickListener.onNotificationChanged(settingItem.enable);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    public Object getItemData(int position){
        return data.get(position);
    }
    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_setting, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.view.setOnClickListener(this);
        holder.switchBTN.setOnClickListener(this);
        holder.switchBTN.setTag(holder);
        holder.settingItem = data.get(position);
        holder.nameTXT.setText(holder.settingItem.name);
        holder.descriptionTXT.setText(holder.settingItem.description);
        holder.switchBTN.setVisibility(holder.settingItem.show_enabled ? View.VISIBLE : View.GONE);
        if (holder.settingItem.enable != null) {
        holder.switchBTN.setChecked(holder.settingItem.enable);
        }
        holder.switchBTN.setTag(holder);
        holder.switchBTN.setOnClickListener(this);

		return convertView;
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        switch (v.getId()){
            case R.id.switchBTN:
                setSwitchClicked(viewHolder.settingItem.id);
                break;
            default:
                if(clickListener != null){
                    clickListener.onSettingItemClickListener(viewHolder.settingItem.id);
                }

        }
    }


    public class ViewHolder{
        SettingItem settingItem;

        @BindView(R.id.nameTXT)         TextView nameTXT;
        @BindView(R.id.descriptionTXT)  TextView descriptionTXT;
        @BindView(R.id.switchBTN)       Switch switchBTN;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}
    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onSettingItemClickListener(int position);
        void onNotificationChanged(boolean active);
    }
} 
