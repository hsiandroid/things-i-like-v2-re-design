package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.ChatThreadModel;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.server.base.AndroidModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InboxRecyclerViewAdapter extends RecyclerView.Adapter<InboxRecyclerViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private ArrayList <ReferenceData> data;
    private List<ChatThreadModel> messageData;
    private LayoutInflater layoutInflater;

    private int myId;

    private static final int MESSAGE_IN = 1;
    private static final int MESSAGE_OUT = 2;
    private static final int ANNOUNCEMENT = 3;


    public InboxRecyclerViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        this.initAdapter();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<ChatThreadModel> data){
        getData().clear();
        this.messageData = data;
        groupMessageByDate();
        notifyDataSetChanged();
    }

    public void addNewData(List<ChatThreadModel> data){
        getData().clear();
        int initialCount = this.messageData.size();
        int newCount = 0;
        for(ChatThreadModel item :  data) {
            if(isDataUnique(item)){
            this.messageData.add(item);
            newCount++;
        }
        }
        groupMessageByDate();
        notifyItemRangeChanged(initialCount, newCount);
    }

    private void  groupMessageByDate() {
        for (int i = 0; i < messageData.size(); i++) {
            ChatThreadModel item = messageData.get(i);

            switch (item.type){
                case "announcement":
                    getData().add(new ReferenceData(ANNOUNCEMENT, String.valueOf(item.id), item));
                    break;
                default:
                    if (item.senderUserId == myId) {
                        getData().add(new ReferenceData(MESSAGE_OUT, String.valueOf(item.id), item));
                    } else {
                        getData().add(new ReferenceData(MESSAGE_IN, String.valueOf(item.id), item));
                    }
                    break;
            }
        }
    }

    public void  newMessage(ChatThreadModel messageModel){
        Log.e("Inbox", "new message temp id: " + messageModel.temp_id);
        Log.e("Inbox", "new message new id: " + messageModel.id);
        if (messageModel.senderUserId == myId) {
            Log.e("Inbox", "My message");
            getData().add(0, new ReferenceData(MESSAGE_OUT, String.valueOf(messageModel.id), messageModel));
        } else {
            Log.e("Inbox", "not my message");
            getData().add(0, new ReferenceData(MESSAGE_IN, String.valueOf(messageModel.id), messageModel));
        }
        notifyItemInserted(0);
    }

    public void updateMessage(ChatThreadModel item){
        Log.e("Inbox", "UPDATE MESSAGE" + item.content);
        for(int pos = 0; pos < getData().size(); pos++){
            ReferenceData referenceData = getData().get(pos);
                Log.e("Inbox", "message temp id: " + item.temp_id);
                Log.e("Inbox", "message new id: " + item.id);
                Log.e("Inbox", "reference chat id.: " + referenceData.chatConversationModel.temp_id);
            if (item.temp_id == referenceData.chatConversationModel.temp_id && item.temp_id != 0) {
                Log.e("Inbox", "Temp Id is the same.");
                referenceData.chatConversationModel = item;
                Log.e("Inbox", "new reference chat id.: " + referenceData.chatConversationModel.id);
                Log.e("Inbox", "pos: " + pos);
                getData().set(pos, referenceData);
                notifyItemChanged(pos);
                break;
            }
        }

    }

    public void updateMessages(ChatThreadModel messageModel){
        int pos = messageData.indexOf(messageModel);
        if (pos >= 0){
            messageData.set(pos, messageModel);
            notifyItemChanged(pos);
        }
    }

    private void initAdapter() {
        myId = UserData.getUserId();
        messageData = new ArrayList<>();
    }


    private boolean isDataUnique(ChatThreadModel newData){
        for(ChatThreadModel oldData : this.messageData){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    public LayoutInflater getLayoutInflater() {
        return layoutInflater;
    }

    private View getDefaultView(ViewGroup parent, int resLayout){
        return getLayoutInflater().inflate(resLayout, parent, false);
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        switch (viewType){
            case MESSAGE_OUT:
                return new ViewHolder(getDefaultView(parent, R.layout.adapter_message_out));
            case MESSAGE_IN:
                return new ViewHolder(getDefaultView(parent, R.layout.adapter_message_in));
            case ANNOUNCEMENT:
            default:
                return new ViewHolder(getDefaultView(parent, R.layout.adapter_message_announcement));
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch(data.get(position).chatConversationModel.type){
            case "announcement":
                return ANNOUNCEMENT;
            default:
                if(data.get(position).chatConversationModel.senderUserId == myId){
                    return MESSAGE_OUT;
                }else{
                    return MESSAGE_IN;
                }
        }
    }

    public List<ReferenceData> getData() {
        if(data == null){
            data = new ArrayList<>();
        }
        return data;
    }

    public ReferenceData getItem(int position){
        return getData().get(position);
    }

    @Override
    public void onBindViewHolder(@NotNull final ViewHolder holder, int position) {
        holder.setItem(getItem(position));

        ChatThreadModel chatThreadModel = holder.getItems().chatConversationModel;

        holder.view.setTag(chatThreadModel);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);

        Picasso.with(context)
                .load(chatThreadModel.author.data.image)
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .into(holder.profileCIV);

        holder.nameTXT.setText(chatThreadModel.author.data.name);
        holder.dateTXT.setText(chatThreadModel.info.data.dateCreated.timePassed);
        holder.messageTXT.setText(chatThreadModel.content);
        holder.onlineBTN.setBackground(chatThreadModel.author.data.is_online ? ActivityCompat.getDrawable(context, R.drawable.bg_circle_online) : ActivityCompat.getDrawable(context, R.drawable.bg_circle_offlinee));
        holder.statusTXT.setText(chatThreadModel.status);

        switch(chatThreadModel.type){
            case "image":
                holder.imageIV.setVisibility(View.VISIBLE);
                holder.fileCON.setVisibility(View.GONE);
                Picasso.with(context)
                        .load(chatThreadModel.info.data.attachment.fullPath)
                        .error(R.drawable.placeholder_avatar)
                        .centerCrop()
                        .fit()
                        .into(holder.imageIV);
                holder.messageTXT.setVisibility(View.GONE);
                break;
            case "file":
                holder.imageIV.setVisibility(View.GONE);
                holder.fileCON.setVisibility(View.VISIBLE);
                holder.fileTXT.setText(chatThreadModel.content);
                holder.messageTXT.setVisibility(View.GONE);
                break;
            default:
                holder.imageIV.setVisibility(View.GONE);
                holder.fileCON.setVisibility(View.GONE);
                break;
        }

        holder.adapterCON.setOnLongClickListener(this);
        holder.adapterCON.setOnClickListener(this);
        holder.adapterCON.setTag(position);
        holder.imageIV.setTag(chatThreadModel);
        holder.imageIV.setOnClickListener(this);
        holder.fileCON.setOnClickListener(this);
        holder.fileCON.setTag(chatThreadModel);

        if (chatThreadModel.senderUserId != UserData.getUserId()){
            holder.profileCIV.setTag(chatThreadModel);
            holder.profileCIV.setOnClickListener(this);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        AndroidModel androidModel;

        @BindView(R.id.nameTXT)         TextView nameTXT;
        @BindView(R.id.dateTXT)         TextView dateTXT;
        @BindView(R.id.messageTXT)      TextView messageTXT;
        @BindView(R.id.profileCIV)      ImageView profileCIV;
        @BindView(R.id.imageIV)         ImageView imageIV;
        @BindView(R.id.fileCON)         LinearLayout fileCON;
        @BindView(R.id.fileTXT)         TextView fileTXT;
        @BindView(R.id.statusTXT)       TextView statusTXT;
        @BindView(R.id.adapterCON)      View adapterCON;
        @BindView(R.id.onlineBTN)       View onlineBTN;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }

        public ReferenceData getItems() {
            return (ReferenceData) getItem();
        }

        public void setItem(AndroidModel androidModel) {
            this.androidModel = androidModel;
        }

        public AndroidModel getItem() {
            return androidModel;
        }
    }

    public class ReferenceData extends AndroidModel {

        String reference;
        public int viewType;

        ReferenceData(int viewType, String reference, ChatThreadModel chatConversationModel) {
            this.viewType = viewType;
            this.reference = reference;
            this.chatConversationModel = chatConversationModel;
        }

        ChatThreadModel chatConversationModel;
    }

    public boolean isMe(){
        if(!getData().isEmpty()){
            return getData().get(0).chatConversationModel.senderUserId == UserData.getUserId();
        }
        return false;
    }

    private void otherOptionClick(final View view, int position) {
        final ReferenceData chatThreadModel = data.get(position);
        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.pop_menu_chat_other_option, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.deleteBTN:
                        if (clickListener != null){
                            clickListener.onDeleteClick(chatThreadModel.chatConversationModel);
                        }
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fileCON:
                if(clickListener != null){
                    clickListener.onFileClick((ChatThreadModel) v.getTag());
                }
                break;
            case R.id.imageIV:
                if(clickListener != null){
                    clickListener.onImageClick((ChatThreadModel) v.getTag());
                }
                break;
            case R.id.profileCIV:
                if(clickListener != null){
                    clickListener.onAvatarClick((ChatThreadModel) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                int position  = (int) v.getTag();
                if (isMe()) {
                    otherOptionClick(v, position);
                }
                break;
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int sampleModel);
        void onDeleteClick(ChatThreadModel sampleModel);
        void onFileClick(ChatThreadModel sampleModel);
        void onImageClick(ChatThreadModel sampleModel);
        void onAvatarClick(ChatThreadModel sampleModel);
    }
}