package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.data.model.BankItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BankAccountRecycleViewAdapter extends RecyclerView.Adapter<BankAccountRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<BankItem> data;
    private LayoutInflater layoutInflater;

	public BankAccountRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<BankItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<BankItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(BankItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(BankItem newData){
        for(BankItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_bank_account, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bankItem = data.get(position);
        holder.view.setTag(holder.bankItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.moreBTN.setTag(position);
        holder.moreBTN.setOnClickListener(this);
        holder.adapterCON.setTag(holder.bankItem);
        holder.adapterCON.setOnClickListener(this);

        holder.titleTXT.setText(holder.bankItem.bankName);
        holder.nameTXT.setText(holder.bankItem.accountName);
        holder.refTXT.setText("Acc.no. " +holder.bankItem.accountNumber);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        BankItem bankItem;

        @BindView(R.id.moreBTN)             ImageView moreBTN;
        @BindView(R.id.titleTXT)            TextView titleTXT;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.refTXT)              TextView refTXT;
        @BindView(R.id.adapterCON)          View adapterCON;


        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.moreBTN:
                if(clickListener != null){
                    showOtherOption(v);
                }
                break;
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((BankItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public BankItem getItem(int position){
        return getData().get(position);
    }

    public List<BankItem> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }

    public void showOtherOption(View v){
        final BankItem bankItem = getItem((int) v.getTag());
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.edit:
                        if (clickListener != null){
                            clickListener.onEditClick(bankItem);
                        }
                        return true;
                    case R.id.remove:
                        if (clickListener != null){
                            clickListener.onRemoveClick(bankItem);
                        }
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.inflate(R.menu.bank_popup);
        popup.show();
    }

    public interface ClickListener {
        void onItemClick(BankItem bankItem);
        void onItemLongClick(BankItem bankItem);
        void onEditClick(BankItem bankItem);
        void onRemoveClick(BankItem bankItem);
    }
} 
