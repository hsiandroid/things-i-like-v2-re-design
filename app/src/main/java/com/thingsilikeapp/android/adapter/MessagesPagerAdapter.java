package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.thingsilikeapp.android.fragment.messages.GroupMessageFragment;
import com.thingsilikeapp.android.fragment.messages.PrivateMessageFragment;


/**
 * Created by Ken Drew on 11/11/2017.
 */

public class MessagesPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 2;
    private Context context;

    public MessagesPagerAdapter(FragmentManager fragmentManager, Context c) {
        super(fragmentManager);
        context = c;
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return PrivateMessageFragment.newInstance();
            case 1:
                return GroupMessageFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_UNCHANGED;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Messages";
            case 1:
                return "Groups";
            default:
                return "Page" + position;

        }

    }

}