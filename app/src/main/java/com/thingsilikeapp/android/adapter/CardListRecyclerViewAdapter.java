package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.BankCardItem;

import java.util.List;

/**
 * Created by Kenneth Sunday on 10/20/2018.
 */

public class CardListRecyclerViewAdapter extends RecyclerView.Adapter<CardListRecyclerViewAdapter.ViewHolder> implements View.OnClickListener {

    private List<BankCardItem> itemList;
    private Context context;
    private ItemClickListener clickListener;

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                clickListener.click(v.getTag().toString());
                break;
        }
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout adapterCON;
        ImageView masterCardIV;
        TextView lastDigitTXT;

        public ViewHolder(View itemView) {
            super(itemView);
            adapterCON = itemView.findViewById(R.id.adapterCON);
            masterCardIV = itemView.findViewById(R.id.masterCardIV);
            lastDigitTXT = itemView.findViewById(R.id.lastDigitTXT);
        }
    }

    public CardListRecyclerViewAdapter(Context context, List<BankCardItem> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public CardListRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_card_list, parent, false);
        return new CardListRecyclerViewAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CardListRecyclerViewAdapter.ViewHolder holder, final int position) {

        holder.adapterCON.setTag(itemList.get(position));
        holder.adapterCON.setOnClickListener(this);
        switch (itemList.get(position).cardType){
            case "visa":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_visa);
                break;
            case "diners":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_diners_club);
                break;
            case "discover":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_discover);
                break;
            case "jcb":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_jcb);
                break;
            case "mastercard":
                holder.masterCardIV.setImageResource(R.drawable.icon_card_master_card);
                break;
        }
//        holder.lastDigitTXT.setText(itemList.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.adapter_card_list;
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public interface ItemClickListener {
        void click(String card);
    }
}
