package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.SearchItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<SearchItem> data;
    private LayoutInflater layoutInflater;

	public SearchAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<SearchItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<SearchItem> data){
        for(SearchItem notificationItem :  data){
            this.data.add(notificationItem);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    public Object getItemData(int position){
        return data.get(position);
    }
    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_search, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.searchItem = data.get(position);
        holder.view.setOnClickListener(this);
        holder.nameTXT.setText(holder.searchItem.keyword);

		return convertView;
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
                if(clickListener != null){
                    clickListener.onSearchItemClick(viewHolder.searchItem);
        }
    }


    public class ViewHolder{
        SearchItem searchItem;

        @BindView(R.id.nameTXT)         TextView nameTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}
    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onSearchItemClick(SearchItem searchItem);
    }
} 
