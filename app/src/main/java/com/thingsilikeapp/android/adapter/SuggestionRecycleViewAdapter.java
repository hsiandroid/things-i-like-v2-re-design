package com.thingsilikeapp.android.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.dialog.GroupDialog;
import com.thingsilikeapp.android.dialog.GroupEditDialog;
import com.thingsilikeapp.android.dialog.UnfollowDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Settings;
import com.thingsilikeapp.server.request.report.ReportRequest;
import com.thingsilikeapp.server.request.social.FollowRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.social.UpdateGroupRequest;
import com.thingsilikeapp.server.transformer.report.ReportTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;
import com.thingsilikeapp.vendor.android.java.WalkThrough;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.deanwild.materialshowcaseview.IShowcaseListener;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class SuggestionRecycleViewAdapter extends RecyclerView.Adapter<SuggestionRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener,
        UnfollowDialog.Callback,
        GroupDialog.Callback,
        GroupEditDialog.Callback{

	private Context context;
	private List<UserItem> data;
    private LayoutInflater layoutInflater;
    private int ownedId = 0;
    private boolean walkthrough_done;
    private boolean isRequest;

    public SuggestionRecycleViewAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        EventBus.getDefault().register(this);
        ownedId = UserData.getUserId();
        walkthrough_done = UserData.getBoolean(UserData.WALKTHRU_DONE);
    }

    public void setNewData(List<UserItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<UserItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(UserItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    public void setRequest(boolean request) {
        isRequest = request;
    }

    private boolean isDataUnique(UserItem newData){
        for(UserItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    public void reset(){
        this.data = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void removeItem(UserItem data) {
        for (UserItem item : this.data) {
            if (data.id == (item.id)) {
                int pos = this.data.indexOf(item);
                this.data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    public void updateData(UserItem data){
        for(UserItem item : this.data){
             if(data.id == item.id){
                int pos = this.data.indexOf(item);
                this.data.set(pos, data);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    public List<UserItem> getData(){
        return data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_suggestion, parent, false));
    }

    public UserItem getItem(int position){
        return data.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.userItem = data.get(position);

        Glide.with(context)
                .load(holder.userItem.getAvatar())
                .apply(new RequestOptions()
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true))
                .into(holder.imageCIV);

        holder.nameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.userItem.name, holder.userItem.is_verified, 12));
        holder.commonNameTXT.setText(holder.userItem.common_name);

        holder.commandTXT.setTag(holder.userItem);
        holder.commandTXT.setOnClickListener(this);

        holder.nameTXT.setTag(holder.userItem);
        holder.nameTXT.setOnClickListener(this);

        holder.commonNameTXT.setTag(holder.userItem);
        holder.commonNameTXT.setOnClickListener(this);

        holder.otherOptionBTN.setTag(holder.userItem);
        holder.otherOptionBTN.setOnClickListener(this);

        holder.imageCIV.setTag(R.id.imageCIV, holder.userItem);
        holder.imageCIV.setOnClickListener(this);

        if (StringFormatter.isEmpty(holder.userItem.social.data.group)) {
            holder.groupTXT.setVisibility(View.GONE);
        } else {
            holder.groupTXT.setVisibility(View.VISIBLE);
            holder.groupTXT.setText(holder.userItem.social.data.group.toUpperCase());
        }

        if(holder.userItem.is_loading_follow){
            holder.loadingPB.setVisibility(View.VISIBLE);
            holder.commandTXT.setVisibility(View.GONE);
            holder.otherOptionBTN.setVisibility(View.GONE);
        }else{
            holder.loadingPB.setVisibility(View.GONE);
            holder.commandTXT.setVisibility(View.VISIBLE);
            holder.otherOptionBTN.setVisibility(View.VISIBLE);

            if (UserData.getUserId() == holder.userItem.id){
                Log.e("Boolean",">>>" + "true");
                holder.commandTXT.setVisibility(View.GONE);
                holder.otherOptionBTN.setVisibility(View.GONE);
            }else {
                Log.e("Boolean",">>>" + "false");
                holder.commandTXT.setVisibility(View.VISIBLE);
                holder.otherOptionBTN.setVisibility(View.VISIBLE);
            }



//            if (holder.userItem.walkthrough_on_process_avatar){
//                attemptGetAvatarWalkThrough(holder.imageCIV);
//            }

            if (holder.userItem.social.data.is_following.equals("yes")) {
                holder.commandTXT.setBackground(ActivityCompat.getDrawable(context, R.drawable.bg_follow));
                holder.commandTXT.setTextColor(ActivityCompat.getColor(context, R.color.white));
                holder.commandTXT.setText("FOLLOWING");
                holder.commandTXT.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);

            } else {
//                if (holder.userItem.walkthrough_on_process){
//                    attemptFollowThrough(holder.commandTXT);
//                }

                holder.commandTXT.setBackground(ActivityCompat.getDrawable(context, R.drawable.bg_unfollow));
                holder.commandTXT.setTextColor(ActivityCompat.getColor(context, R.color.colorPrimary));
                holder.commandTXT.setText("FOLLOW");
                holder.commandTXT.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_add_blue,0,0,0);
            }
        }
    }

    public void attemptFollowThrough(View view){
        if (!walkthrough_done){
            WalkThrough.follow(context, view, new IShowcaseListener() {
                @Override
                public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {

                }

                @Override
                public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
                    materialShowcaseView.removeAllViews();

                }
            });
        }
    }

    public void attemptGetAvatarWalkThrough(View view){
        if (!walkthrough_done){
            WalkThrough.profileAvatar(context, view, new IShowcaseListener() {
                @Override
                public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {
                    UserData.insert(UserData.PEOPLE_PROCESS, false);
                }

                @Override
                public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
                    materialShowcaseView.removeAllViews();

                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{
        UserItem userItem;

        @BindView(R.id.imageCIV)            ImageView imageCIV;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.commonNameTXT)       TextView commonNameTXT;
        @BindView(R.id.groupTXT)            TextView groupTXT;
        @BindView(R.id.commandTXT)          TextView commandTXT;
        @BindView(R.id.loadingPB)           View loadingPB;
        @BindView(R.id.otherOptionBTN)      View otherOptionBTN;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.commandTXT:
                onCommandClicked((UserItem) v.getTag());
                break;
            case R.id.otherOptionBTN:
                otherOptionClick(v);
                break;
            case R.id.nameTXT:
            case R.id.commonNameTXT:
                onUserClick((UserItem) v.getTag());
                break;
            case R.id.imageCIV:
                onUserClick((UserItem) v.getTag(R.id.imageCIV));
                break;
        }
    }

    private void onCommandClicked(UserItem userItem){
        if(clickListener != null){
            userItem.is_loading_follow = true;
            updateData(userItem);
            clickListener.onCommandClick(userItem);
        }
    }

    private void onUserClick(UserItem userItem){
        if(!isRequest){
            ((RouteActivity)context).startProfileActivity(userItem.id);
        }
        if(clickListener != null){
            clickListener.onUserClick(userItem);
            UserData.insert(UserData.WALKTHRU_CURRENT, 4);
            UserData.insert(UserData.USER_ID, userItem.id);
        }
    }

    private void otherOptionClick(View view) {
        if(clickListener != null){
            final UserItem userItem = (UserItem) view.getTag();

            PopupMenu popup = new PopupMenu(context, view);
            popup.getMenuInflater().inflate(R.menu.social_group_menu, popup.getMenu());
            if (userItem.social.data.is_following.equals("yes")) {
                popup.getMenu().findItem(R.id.unfollowBTN).setTitle("UNFOLLOW");
                popup.getMenu().findItem(R.id.reAssignBTN).setTitle(StringFormatter.isEmpty(userItem.social.data.group) ? "ASSIGN GROUP" : "RE-ASSIGN");
            } else {
                popup.getMenu().findItem(R.id.reAssignBTN).setVisible(false);
                popup.getMenu().findItem(R.id.unfollowBTN).setTitle("FOLLOW");
            }

            String report = popup.getMenu().findItem(R.id.reportBTN).getTitle().toString();
            SpannableString spannableString = new SpannableString(report);
            spannableString.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(context, R.color.failed)), 0, spannableString.length(), 0);
            popup.getMenu().findItem(R.id.reportBTN).setTitle(spannableString);

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.reAssignBTN:
                            updateGroup(userItem);
                            break;
                        case R.id.unfollowBTN:
                            onCommandClicked(userItem);
                            break;
                        case R.id.reportBTN:
                            blockUser(userItem);
                            break;
                    }

                    return false;
                }
            });
            popup.show();
        }
    }

    public void followUser(UserItem userItem){
        GroupDialog.newInstance(userItem, this).show(((BaseActivity) context).getSupportFragmentManager(),GroupDialog.TAG);
    }

    @Override
    public void onGroupSelect(String groupName, int id) {
        MaterialShowcaseView.resetAll(context);
        FollowRequest followRequest = new FollowRequest(context);
        followRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .setProgressDialog(new ProgressDialog(context).show(context, "", "Please wait...", false, false))
                .addParameters(Keys.server.key.USER_ID, id)
                .addParameters(Keys.server.key.GROUP,groupName)
                .execute();

        for(UserItem item : this.data){
            if(id == item.id){
                int pos = this.data.indexOf(item);
                item.walkthrough_on_process = false;
                this.data.set(pos, item);
                notifyItemChanged(pos);
//                if (UserData.getBoolean(UserData.PEOPLE_PROCESS)){
//                    UserData.insert(UserData.PEOPLE_PROCESS, false);
//                    notifyDataSetChanged();
//                }
                break;
            }
        }

    }

    @Override
    public void onCancel(String groupName, int id) {
        for(UserItem item : this.data){
            if(id == item.id){
                int pos = this.data.indexOf(item);
                item.is_loading_follow = false;
                this.data.set(pos, item);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    public void unFollowUser(UserItem userItem){
        UnfollowDialog.newInstance(userItem.id, userItem.name, this).show(((BaseActivity) context).getSupportFragmentManager(), UnfollowDialog.TAG);
    }

    public void updateGroup(UserItem userItem){
        GroupEditDialog.newInstance(userItem, this).show(((BaseActivity) context).getSupportFragmentManager(),GroupDialog.TAG);
    }

    @Override
    public void onGroupEdit(String groupName, int id) {
        UpdateGroupRequest updateGroupRequest = new UpdateGroupRequest(context);
        updateGroupRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .setProgressDialog(new ProgressDialog(context).show(context, "", "Please wait...", false, false))
                .addParameters(Keys.server.key.USER_ID, id)
                .addParameters(Keys.server.key.GROUP,groupName)
                .execute();
    }

    @Subscribe
    public void onResponse(FollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
            UserItem userItem = userTransformer.userItem;
            userItem.walkthrough_on_process = false;
            updateData(userItem);

            UserData.updateStatistic(1);
        }
        UserData.insert(UserData.WALKTHRU_CURRENT, 3);
        if (clickListener != null){
            clickListener.onFollowSuccessClick();
        }
    }

    private void blockUser(final UserItem userItem){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to block " + StringFormatter.first(StringFormatter.firstWord(userItem.common_name)) + "?")
                .setNote("You are about to block this user.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Block")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Settings.getDefault().blockUser(context,userItem.id)
                                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(context).show(context, "", "Please wait...", false, false))
                                .addParameter(Keys.server.key.TYPE, "user")
                                .addParameter(Keys.server.key.REFERENCE_ID, userItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(((BaseActivity) context).getSupportFragmentManager());
    }


    @Subscribe
    public void onResponse(ReportRequest.ServerResponse responseData) {
        ReportTransformer reportTransformer = responseData.getData(ReportTransformer.class);
        if (reportTransformer.status) {
            ToastMessage.show(context, reportTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(context, reportTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(UpdateGroupRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
            ToastMessage.show(context,userTransformer.msg, ToastMessage.Status.SUCCESS);
            updateData(userTransformer.userItem);
            UserData.updateStatistic(1);
        }else {
            ToastMessage.show(context,userTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
            updateData(userTransformer.userItem);
            UserData.updateStatistic(-1);
        }
    }

    @Override
    public void onAccept(int userID) {
        UnFollowRequest unFollowRequest = new UnFollowRequest(context);
        unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, userID)
                .execute();
    }

    @Override
    public void onCancel(int userID) {
        for(UserItem item : this.data){
            if(userID == item.id){
                int pos = this.data.indexOf(item);
//                Log.e("True ", userID + "");
                item.is_loading_follow = false;
                this.data.set(pos, item);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onCommandClick(UserItem userItem);
        void onUserClick(UserItem userItem);
        void onFollowSuccessClick();
    }
} 
