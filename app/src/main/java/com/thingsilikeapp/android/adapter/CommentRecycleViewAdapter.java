package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.data.model.CommentItem;
import com.thingsilikeapp.data.model.UserItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class CommentRecycleViewAdapter extends RecyclerView.Adapter<CommentRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<CommentItem> data;
    private LayoutInflater layoutInflater;
    private int myID;

	public CommentRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        myID = UserData.getUserId();
	}

    public void setNewData(List<CommentItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void newCommnent(CommentItem data){
        this.data.add(0, data);
        notifyItemInserted(0);
    }

    public void removeComment(CommentItem item) {
        for (CommentItem notificationItem : data) {
            if (item.id == (notificationItem.id)) {
                int pos = data.indexOf(notificationItem);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    public void updateData(CommentItem commentItem){
        for(CommentItem item : data){
            if(item.id == 0){
                if(item.temp_id == commentItem.temp_id){
                    int pos = data.indexOf(item);
                    data.set(pos, commentItem);
                    notifyItemChanged(pos);
                    break;
                }
            }else if(commentItem.id == item.id){
                int pos = data.indexOf(item);
                data.set(pos, commentItem);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    public void addNewData(List<CommentItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(CommentItem commentItem :  data){
            if(isDataUnique(commentItem)){
                this.data.add(commentItem);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(CommentItem newData){
        for(CommentItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_conversation_in, parent, false));
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.commentItem = data.get(position);

        holder.view.setTag(holder.commentItem);
        holder.commentCON.setTag(holder.commentItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.commentCON.setOnLongClickListener(this);


        if(holder.commentItem.tagged_user_id == 0){
            holder.messageTXT.setText(formatDescription(holder.commentItem));
        }else{
            holder.messageTXT.setText(formatDescriptionWithMention(holder.commentItem));
        }


        holder.dateTXT.setText(holder.commentItem.time_passed);

        holder.messageTXT.setMovementMethod(LinkMovementMethod.getInstance());

        Glide.with(context)
                .load(holder.commentItem.author.data.getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .dontTransform()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(holder.avatarCIV);

        holder.replyBTN.setVisibility(myID == holder.commentItem.author.data.id ? View.GONE : View.VISIBLE);
        holder.replyBTN.setTag(holder.commentItem.author.data);
        holder.replyBTN.setOnClickListener(this);

        holder.avatarCIV.setTag(R.id.avatarCIV, holder.commentItem.author.data);
        holder.avatarCIV.setOnClickListener(this);
    }

    public SpannableString formatDescription(final CommentItem commentItem){
        String userName = commentItem.author.data.username + "  ";

        if(TextUtils.isEmpty(userName)){
            userName = "-";
        }

        String finalMessage = userName + commentItem.content;
        SpannableString spannableString = new SpannableString(finalMessage);

        ClickableSpan nameSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if(clickListener != null){
                    clickListener.onAvatarClick(commentItem.author.data);
                }
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.black));
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_heading)));
            }
        };

        spannableString.setSpan(nameSpan, 0, userName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan messageSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_paragraph)));
            }
        };

        spannableString.setSpan(messageSpan,userName.length(), finalMessage.length(), Spanned.SPAN_INTERMEDIATE);

        return spannableString;
    }

    public String formatContent(final CommentItem commentItem){
        return "@" + commentItem.author.data.username + " " + commentItem.content;
    }

    public SpannableString formatDescriptionWithMention(final CommentItem commentItem){
        String authorName = commentItem.author.data.username + " ";
        String taggedUserName = commentItem.tagged_user.data.username + "  ";
        String mention = "mentioned ";

        if(TextUtils.isEmpty(authorName)){
            authorName = "-";
        }

        String finalMessage = authorName + mention + taggedUserName + "\n" + commentItem.content;

        SpannableString spannableString = new SpannableString(finalMessage);

        ClickableSpan nameSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if(clickListener != null){
                    clickListener.onAvatarClick(commentItem.author.data);
                }
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.black));
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_heading)));
            }
        };

        spannableString.setSpan(nameSpan, 0, authorName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan taggedSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if(clickListener != null){
                    clickListener.onAvatarClick(commentItem.tagged_user.data);
                }
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.black));
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_heading)));
            }
        };

        spannableString.setSpan(taggedSpan, authorName.length() + mention.length(), mention.length() +authorName.length() + taggedUserName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan messageSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_paragraph)));
            }
        };

        spannableString.setSpan(messageSpan, authorName.length() + taggedUserName.length(), finalMessage.length(), Spanned.SPAN_INTERMEDIATE);

        return spannableString;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        CommentItem commentItem;

        @BindView(R.id.messageTXT)      TextView messageTXT;
        @BindView(R.id.dateTXT)         TextView dateTXT;
        @BindView(R.id.replyBTN)        View replyBTN;
        @BindView(R.id.commentCON)      View commentCON;
        @BindView(R.id.avatarCIV)       ImageView avatarCIV;
        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            switch (v.getId()){
                case R.id.replyBTN:
                    clickListener.onReplyClick((UserItem) v.getTag());
                    break;
                case R.id.avatarCIV:
                    clickListener.onAvatarClick((UserItem) v.getTag(R.id.avatarCIV));
                    break;
                default:
                    clickListener.onItemClick((CommentItem) v.getTag());
            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()){
            case R.id.commentCON:if(clickListener != null){
                clickListener.onItemLongClick((CommentItem) v.getTag());
            }
                break;
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(CommentItem commentItem);
        void onItemLongClick(CommentItem commentItem);
        void onAvatarClick(UserItem author);
        void onReplyClick(UserItem author);
    }
} 
