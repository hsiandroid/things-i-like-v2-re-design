package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.GroupFollowingItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupFollowingRecycleViewAdapter extends RecyclerView.Adapter<GroupFollowingRecycleViewAdapter.ViewHolder> implements
        View.OnClickListener,
        View.OnLongClickListener {
    private Context context;
    private List<GroupFollowingItem> data;
    private LayoutInflater layoutInflater;

    public GroupFollowingRecycleViewAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    public void setNewData(List<GroupFollowingItem> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void reset() {
        this.data = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setSelected(String title) {
        for (GroupFollowingItem groupFollowingItem : data) {
            groupFollowingItem.selected = groupFollowingItem.title.equalsIgnoreCase(title);
        }
        notifyDataSetChanged();
    }

    public String getSelectedTitle() {
        for (GroupFollowingItem groupFollowingItem : data) {
            if (groupFollowingItem.selected) {
                return groupFollowingItem.title;
            }
        }
        return "";
    }

    public List<GroupFollowingItem> getData() {
        return data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_group_following, parent, false));
    }

    public GroupFollowingItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.groupFollowingItem = data.get(position);
        holder.view.setTag(holder.groupFollowingItem);
        holder.view.setOnClickListener(this);
        holder.headerTXT.setSelected(holder.groupFollowingItem.selected);
        holder.headerTXT.setText(holder.groupFollowingItem.title);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        GroupFollowingItem groupFollowingItem;

        @BindView(R.id.headerTXT)
        TextView headerTXT;

        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public void onClick(View v) {
        GroupFollowingItem groupItem = (GroupFollowingItem) v.getTag();
        setSelected(groupItem.title);

        if (clickListener != null) {
            clickListener.onGroupItemClick(groupItem);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onGroupItemClick(GroupFollowingItem groupFollowingItem);
    }
} 
