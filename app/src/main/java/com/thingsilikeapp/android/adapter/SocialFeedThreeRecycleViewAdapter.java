package com.thingsilikeapp.android.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.view.ViewGroup.LayoutParams;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.ImageModel;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.java.CustonParentLayout;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;
import com.thingsilikeapp.vendor.android.widget.CustomVideoView;
import com.thingsilikeapp.vendor.android.widget.OneItemListener;
import com.thingsilikeapp.vendor.android.widget.image.FixedWidthImageView;
import com.varunest.sparkbutton.SparkButton;
import com.willy.ratingbar.RotationRatingBar;
import com.yqritc.scalablevideoview.ScalableType;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SocialFeedThreeRecycleViewAdapter extends RecyclerView.Adapter<SocialFeedThreeRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener,
        View.OnLongClickListener,
        BaseSliderView.OnSliderClickListener,
        OneItemListener
{
	private Context context;
	private List<WishListItem> data;
    private LayoutInflater layoutInflater;
    private int userID;
    private MediaController controller;
    private RecyclerView recylerView;
    private boolean enableUserClick = true;

    private static final int PATTERN_A = 1;
    private static final int PATTERN_B = 2;
    private static final int PATTERN_C = 3;
    private static final int PATTERN_D = 4;
    private static final int PATTERN_E = 5;
    private static final int PATTERN_F = 6;

	public SocialFeedThreeRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        userID = UserData.getUserId();
        controller = new MediaController(context);
        controller.setVisibility(View.GONE);
        layoutInflater = LayoutInflater.from(context);
	}

    private boolean isDataUnique(WishListItem newData) {
        return !data.contains(newData);
    }

    public void setNewData(List<WishListItem> data) {
        this.data.clear();
        for (WishListItem wishListItem : data) {
            if (isDataUnique(wishListItem) && wishListItem.id != -1) {
                this.data.add(wishListItem.minimal(userID));
            }
        }
        notifyDataSetChanged();
    }

    public WishListItem getItem(int position){
        if(data.size() >0){
            return data.get(position);
        }else {
            return new WishListItem();
        }
    }

    public void updateData(WishListItem data) {
        List<WishListItem> wishListItems = new ArrayList<>();
        for (WishListItem item : this.data) {
            if (data.id == item.id) {
                WishListItem minimalData  = data.minimal(userID);
                minimalData.wishListMin.for_update = true;
                wishListItems.add(minimalData);
            }else{
                wishListItems.add(item);
            }
        }
        updateData(wishListItems, this.data);
    }
    public void updateData(final List<WishListItem> newData, final List<WishListItem> oldData) {

        final SocialFeedThreeRecycleViewAdapter.WishListItemDiffUtilCallback diffCallback = new SocialFeedThreeRecycleViewAdapter.WishListItemDiffUtilCallback(oldData, newData);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.data = newData;
        diffResult.dispatchUpdatesTo(SocialFeedThreeRecycleViewAdapter.this);
    }

    public class WishListItemDiffUtilCallback extends DiffUtil.Callback {
        List<WishListItem> oldList;
        List<WishListItem> newList;

        WishListItemDiffUtilCallback(List<WishListItem> newList, List<WishListItem> oldList) {
            this.newList = newList;
            this.oldList = oldList;
        }

        @Override
        public int getOldListSize() {
            return oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).id == (newList.get(newItemPosition).id);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).liker_count == (newList.get(newItemPosition).liker_count);
        }

        @Nullable
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
            WishListItem newContact = newList.get(newItemPosition);
            WishListItem oldContact = oldList.get(oldItemPosition);

            Bundle diff = new Bundle();
            if(newContact.liker_count != oldContact.liker_count){
                diff.putString("liker_count", newContact.liker_count + "");
            }

            if (diff.size()==0){
                return null;
            }
            return diff;
        }
    }

    public void addNewData(List<WishListItem> data) {
        int initialCount = this.data.size();
        int newCount = 0;
        for (WishListItem wishListItem : data) {
            if (isDataUnique(wishListItem)) {
                this.data.add(wishListItem.minimal(userID));
                newCount++;
            }
        }
        notifyItemRangeInserted(initialCount, newCount);
    }

    public LayoutInflater getLayoutInflater() {
        return layoutInflater;
    }

    public View getDefaultView(ViewGroup parent, int resLayout){
        return getLayoutInflater().inflate(resLayout, parent, false);
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        switch (viewType){
            case PATTERN_A:
                return new SocialFeedThreeRecycleViewAdapter.ViewHolder(getDefaultView(parent, R.layout.adapter_socialfeed_f));
            case PATTERN_B:
                return new SocialFeedThreeRecycleViewAdapter.ViewHolder(getDefaultView(parent, R.layout.adapter_socialfeed_f));
            case PATTERN_C:
                return new SocialFeedThreeRecycleViewAdapter.ViewHolder(getDefaultView(parent, R.layout.adapter_socialfeed_f));
            case PATTERN_D:
                return new SocialFeedThreeRecycleViewAdapter.ViewHolder(getDefaultView(parent, R.layout.adapter_socialfeed_g));
            case PATTERN_E:
                return new SocialFeedThreeRecycleViewAdapter.ViewHolder(getDefaultView(parent, R.layout.adapter_socialfeed_g));
            case PATTERN_F:
                return new SocialFeedThreeRecycleViewAdapter.ViewHolder(getDefaultView(parent, R.layout.adapter_socialfeed_h));
            default:
                return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch(data.get(position).pattern) {
            case 0:
                return PATTERN_A;
            case 1:
                return PATTERN_B;
            case 2:
                return PATTERN_C;
            case 3:
                return PATTERN_D;
            case 4:
                return PATTERN_E;
            case 5:
                return PATTERN_F;
            default:
                if (data.get(position).pattern == 0) {
                    return PATTERN_A;
                } else if (data.get(position).pattern == 1) {
                    return PATTERN_B;
                } else if (data.get(position).pattern == 2) {
                    return PATTERN_C;
                } else if(data.get(position).pattern == 3) {
                    return PATTERN_D;
                } else if(data.get(position).pattern == 4) {
                    return PATTERN_E;
                }else {
                    return PATTERN_F;
                }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NotNull final ViewHolder holder, final int position) {
        holder.wishListItem = data.get(position);
        holder.view.setTag(holder.wishListItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);

        holder.avatarCIV.setTag(R.id.avatarCIV,position);
        holder.commentBTN.setTag(R.id.commentBTN,position);
        holder.commentCountTXT.setTag(R.id.commentCountTXT, position);
        holder.otherOptionBTN.setTag(R.id.otherOptionBTN, position);
        holder.socialFeedCON.setTag(R.id.socialFeedCON, position);
        holder.rePostBTN.setTag(R.id.rePostBTN, position);
        holder.rePostCountTXT.setTag(R.id.rePostCountTXT, position);
        holder.iLikeIV.setTag(R.id.iLikeIV, position);
        holder.sliderLayout.setTag(R.id.sliderLayout, position);
        holder.lykaRB.setTag(R.id.lykaRB, position);
        holder.shareBTN.setTag(R.id.shareBTN, position);
        holder.titleTTXT.setTag(R.id.titleTTXT, position);
        holder.contentTXT.setTag(R.id.contentTXT, position);
        holder.parentCON.setTag(R.id.parentCON, position);
        holder.diamondRateTXT.setTag(R.id.rateTXT, position);
        holder.soundsOFFBTN.setTag(R.id.soundsOFFBTN, position);
        holder.soundsONBTN.setTag(R.id.soundsONBTN, position);
        holder.parentCON.setDisableChildrenTouchEvents(true);

        holder.socialFeedCON.setAlpha(1f);
        holder.heartSB.setChecked(holder.wishListItem.wishListMin.is_liked);
//        holder.otherOptionBTN.setVisibility(holder.wishListItem.wishListMin.owned ? View.GONE : View.VISIBLE);
        holder.otherOptionBTN.setVisibility(View.GONE);
        holder.userNameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.wishListItem.wishListMin.name, holder.wishListItem.wishListMin.is_verified, 12));
        holder.iLikeTXT.setText(holder.wishListItem.wishListMin.category.toUpperCase());
        holder.dateTXT.setText(holder.wishListItem.wishListMin.time_passed);
        if (holder.wishListItem.info.data.content.equalsIgnoreCase("") || holder.wishListItem.info.data.content.length() == 0){
            holder.contentTXT.setVisibility(View.GONE);
        }else{
            holder.contentTXT.setVisibility(View.VISIBLE);
        }

        String str = holder.wishListItem.info.data.content;
        String strs = str.replace("\n", " ");

        if (strs.length() < 62){
            holder.contentTXT.setText(str);
        }else{
            String text = getSafeSubstring(strs, 62) + "....more";
            String textToBeColored = "....more";
            String htmlText = text.replace(textToBeColored,"<font color='#686868'>"+textToBeColored +"</font>");

            holder.contentTXT.setText(Html.fromHtml(htmlText));
        }

        if (holder.wishListItem.wishListMin.title.equalsIgnoreCase("") || holder.wishListItem.wishListMin.title == null){
            holder.titleTTXT.setVisibility(View.GONE);
        }else{
            holder.titleTTXT.setVisibility(View.VISIBLE);
        }
        holder.titleTTXT.setText(holder.wishListItem.wishListMin.title);
        holder.userNameTXT.setBackground(null);
        holder.diamondRateTXT.setText("( "  + holder.wishListItem.for_display_liker +" )" );
        holder.commentCountTXT.setText(holder.wishListItem.comment_count + " Comments") ;
        holder.rePostCountTXT.setText(holder.wishListItem.repost_count + " Reposts");
        holder.commentCountTXT.setVisibility(View.VISIBLE);
        holder.heartSB.setVisibility(View.VISIBLE);
        holder.commentBTN.setVisibility(View.VISIBLE);
        holder.rePostBTN.setVisibility(View.VISIBLE);
        holder.lykaRB.setRating(holder.wishListItem.diamond_sent);
        if (holder.wishListItem.diamond_sent ==  0) {
            holder.lykaRB.setEnabled(true);
            holder.lykaRB.setIsIndicator(false);
        }else{
            holder.lykaRB.setEnabled(false);
            holder.lykaRB.setIsIndicator(true);
        }

        holder.lykaRB.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()){
                    case MotionEvent.ACTION_UP:
                        Log.e("ACTION", "RELEASE");
                        if(clickListener != null){
                            clickListener.onRatingClick(data.get(position), (int) holder.lykaRB.getRating());
                        }
                        break;
                }
                return false;
            }
        });

        Glide.with(context)
                .load(holder.wishListItem.wishListMin.avatar)
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .fitCenter()
                .dontTransform()
                .dontAnimate()
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(holder.avatarCIV);

        Glide.with(context)
                .load(holder.wishListItem.image.data.full_path)
                .apply(new RequestOptions()
                        .centerCrop()
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(holder.coverIV);

//        Picasso.with(context).load(holder.wishListItem.image.data.full_path).centerCrop().fit().into(holder.coverIV);
//        Glide.with(context)
//                .load(holder.wishListItem.wishListMin.like_image)
//                .placeholder(R.color.light_gray)
//                .error(R.color.light_gray)
//                .centerCrop()
//                .listener(holder.requestListener)
//                .dontAnimate()
//                .skipMemoryCache(true)
//                .dontTransform()
//                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                .into(holder.iLikeIV);

        holder.sliderLayout.removeAllSliders();

        if (holder.wishListItem.post_type.equalsIgnoreCase("video")){
            holder.sliderLayout.setVisibility(View.GONE);
//            holder.videoView.setVisibility(View.VISIBLE);
            holder.iLikeIV.setVisibility(View.GONE);

            Uri uri = Uri.parse(holder.wishListItem.images.data.get(0).fullPath);
            //            try{
//                holder.videoView.setMediaController(controller);
//                holder.videoView.setVideoURI(uri);
//            } catch (Exception e){
//                Log.e("Error", e.getMessage());
//                e.printStackTrace();
//            }
////            holder.videoView.setZOrderOnTop(true);
//
//            holder.videoView.requestFocus();
//            holder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer mp) {
//                    holder.videoView.start();
//                    mp.setLooping(true);
//                }
//            });
//            Uri uri = Uri.parse("https://teststreaming-aase.streaming.media.azure.net/7618d3df-886b-4f0c-b701-43c208445e3d/Samsung%20HD%20Demo%20-%20Colour%20Of%20Chin.ism/manifest(format=m3u8-aapl)");
            setVideo(holder.videoView, uri);
            holder.videoView.setScalableType(ScalableType.CENTER_CROP);

        }else{
            if (holder.wishListItem.images.data.size() == 0 || holder.wishListItem.images.data.size() == 1){
                holder.sliderLayout.setVisibility(View.GONE);
                holder.videoView.setVisibility(View.GONE);
                holder.iLikeIV.setVisibility(View.VISIBLE);
            }else {
                holder.sliderLayout.setVisibility(View.VISIBLE);
                holder.videoView.setVisibility(View.GONE);
                holder.iLikeIV.setVisibility(View.GONE);

//                holder.sliderLayout.setLayoutParams(new LayoutParams(500, 500));

                LayoutParams lp = holder.sliderLayout.getLayoutParams();
                if (holder.wishListItem.orientation.equalsIgnoreCase("landscape")){
                    lp.height = 750;
                }else{
                    lp.height = 1450;
                }

                lp.width = LayoutParams.MATCH_PARENT;
                holder.sliderLayout.requestLayout();

                holder.sliderLayout.setPresetTransformer(SliderLayout.Transformer.Fade);
                holder.sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                holder.sliderLayout.setCustomAnimation(new DescriptionAnimation());
                holder.sliderLayout.setDuration(2000);

                for (ImageModel imageModel : holder.wishListItem.images.getImages()) {
                    DefaultSliderView featuredEventTextView = new DefaultSliderView(context);
                    featuredEventTextView
                            .image(imageModel.fullPath)
                            .setScaleType(BaseSliderView.ScaleType.CenterCrop);
                    holder.sliderLayout.addSlider(featuredEventTextView);
                }
            }

        }
//        holder.lykaRB.setOnRatingChangeListener(new BaseRatingBar.OnRatingChangeListener() {
//            @Override
//            public void onRatingChange(BaseRatingBar baseRatingBar, float v) {
//                if(v > holder.lykaRB.getRating()){
//                    holder.diamondRateTXT.setText(String.valueOf (Float.parseFloat(holder.wishListItem.for_display_liker)+ v));
//                }else if (v < holder.lykaRB.getRating()){
//                    holder.diamondRateTXT.setText(String.valueOf (Float.parseFloat(holder.wishListItem.for_display_liker) - v));
//                }
//            }
//        });


//        holder.playBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {
//                    holder.videoView.prepare(new MediaPlayer.OnPreparedListener() {
//                        @Override
//                        public void onPrepared(MediaPlayer mp) {
//                            holder.videoView.setVisibility(View.VISIBLE);
//                            holder.videoView.start();
//                        }
//                    });
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    Log.e("ZZZZZZZZ", ">>>>>>" + e);
//
//                }
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        holder.coverIV.setVisibility(View.INVISIBLE);
//                        holder.playBTN.setVisibility(View.INVISIBLE);
//                    }
//                }, 800);
//            }
//        });
//
//        holder.soundsONBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.e("EEEEE", "ON");
//                holder.soundsONBTN.setVisibility(View.GONE);
//                holder.soundsOFFBTN.setVisibility(View.VISIBLE);
//                holder.videoView.setVolume(0,0);
//            }
//        });
//        holder.soundsOFFBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.e("EEEEE", "OFF");
//                holder.soundsONBTN.setVisibility(View.VISIBLE);
//                holder.soundsOFFBTN.setVisibility(View.GONE);
//                holder.videoView.setVolume(10,10);
//            }
//        });
//        Picasso.with(context).load(holder.wishListItem.wishListMin.like_image).centerCrop().fit().placeholder(R.color.light_gray).into(holder.iLikeIV);

        holder.iLikeIV.setImageDrawable(null);
//        holder.iLikeIV.autoResize(Integer.parseInt(holder.wishListItem.image.data.width) == 0);
//        holder.iLikeIV.setImageSize(Integer.parseInt(holder.wishListItem.image.data.width), Integer.parseInt(holder.wishListItem.image.data.height));
        Glide.with(context)
                .load(holder.wishListItem.image.data.full_path)
                .thumbnail(0.1f)
                .apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(R.color.light_gray)
                        .error(R.color.light_gray)
                        .dontAnimate()
                        .skipMemoryCache(true)
                        .dontTransform()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(holder.iLikeIV);
    }

    public String getSafeSubstring(String s, int maxLength){
        if(!TextUtils.isEmpty(s)){
            if(s.length() >= maxLength){
                return s.substring(0, maxLength);
            }
        }
        return s;
    }

    @Override
    public void selectItemAt(int position) {
        Log.e("OneItem", "selectItemAt: " + position);
        final ViewHolder viewHolder = findViewHolderForAdapterPosition(position);
        if(viewHolder != null){
            if (viewHolder.wishListItem.post_type.equalsIgnoreCase("video")){
                try {
                    Uri uri = Uri.parse(viewHolder.wishListItem.images.data.get(0).fullPath);
                    viewHolder.videoView.setDataSource(context, uri);
                    viewHolder.videoView.setLooping(true);
                    viewHolder.videoView.requestFocus();
                    viewHolder.videoView.setVolume(0,0);
                    viewHolder.videoView.setScalableType(ScalableType.CENTER_CROP);

                    viewHolder.videoView.prepare(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            viewHolder.coverIV.setVisibility(View.GONE);
                            viewHolder.playBTN.setVisibility(View.VISIBLE);
                            viewHolder.videoView.setVisibility(View.VISIBLE);
                            viewHolder.videoView.start();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("HAHAHHAA", ">>>>>>" + e);
                }
            }
        }else{
            Log.e("OneItem", "adapter Null: " + position);
        }
    }



    @Override
    public void unSelectItemAt(int position) {
        Log.e("OneItem", "unSelectItemAt: " + position);
        final ViewHolder viewHolder = findViewHolderForAdapterPosition(position);
        if(viewHolder != null){
            if (viewHolder.wishListItem.post_type.equalsIgnoreCase("video")){

                viewHolder.coverIV.setVisibility(View.VISIBLE);
                viewHolder.playBTN.setVisibility(View.GONE);
                viewHolder.videoView.setVisibility(View.GONE);

                if (viewHolder.videoView.isPlaying()){
                    viewHolder.videoView.stop();
                }
            }
        }else{
            Log.e("OneItem", "adapter Null: " + position);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recylerView = recyclerView;
    }

    public void updateLikeCount(int id, String count, int diamond_sent){
        int position = findPositionByID(id);
        if(recylerView == null){
            return;
        }

        ViewHolder adapterViewHolder = findViewHolderForAdapterPosition(position);
        if(adapterViewHolder != null){
            adapterViewHolder.diamondRateTXT.setText("( " +count +" )");
            if (diamond_sent ==  0) {
                adapterViewHolder.lykaRB.setEnabled(true);
                adapterViewHolder.lykaRB.setIsIndicator(false);
            }else{
                adapterViewHolder.lykaRB.setEnabled(false);
                adapterViewHolder.lykaRB.setIsIndicator(true);
            }
        }
    }

    private int findPositionByID(int id){
        for(int i = 0; i < data.size(); i++){
            if (id == data.get(i).id) {
                return i;
            }
        }
        return 0;
    }

    private ViewHolder findViewHolderForAdapterPosition(int position){

        if(recylerView == null){
            return null;
        }

        return  (ViewHolder) recylerView.findViewHolderForAdapterPosition(position);
    }


    private class SliderTask extends AsyncTask<Void, Void, Void> {
        WishListItem wishListItem;
        SliderLayout sliderLayout;


        @Override
        protected void onPreExecute() {
            //set message of the dialog
            //show dialog
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //hide the dialog
            Log.e("EEEEEEEEE", "SUCCESS!");

            super.onPostExecute(result);
        }
    }

    public static int getScreenWidth(Context c) {
        int screenWidth = 0; // this is part of the class not the method
        if (screenWidth == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        }

        return screenWidth;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        WishListItem wishListItem;
        @BindView(R.id.iLikeIV)                     ImageView iLikeIV;
        @BindView(R.id.iLikeTXT)                    TextView iLikeTXT;
        @BindView(R.id.userNameTXT)                 TextView userNameTXT;
        @BindView(R.id.avatarCIV)                   ImageView avatarCIV;
        @BindView(R.id.dateTXT)                     TextView dateTXT;
        @BindView(R.id.contentTXT)                  TextView contentTXT;
        @BindView(R.id.otherOptionBTN)              View otherOptionBTN;
        @BindView(R.id.commentCountTXT)             TextView commentCountTXT;
        @BindView(R.id.commentBTN)                  View commentBTN;
        @BindView(R.id.shareBTN)                    View shareBTN;
        @BindView(R.id.socialFeedCON)               View socialFeedCON;
        @BindView(R.id.heartSB)                     SparkButton heartSB;
        @BindView(R.id.heartCountTXT)               TextView heartCountTXT;
        @BindView(R.id.rePostBTN)                   View rePostBTN;
        @BindView(R.id.rePostCountTXT)              TextView rePostCountTXT;
        @BindView(R.id.titleTTXT)                   TextView titleTTXT;
        @BindView(R.id.diamondRateTXT)              TextView diamondRateTXT;
        @BindView(R.id.footerCON)                   View footerCON;
        @BindView(R.id.parentCON)                   CustonParentLayout parentCON;
        @BindView(R.id.lykaRB)                      RotationRatingBar lykaRB;
        @BindView(R.id.sliderLayout)                SliderLayout sliderLayout;
        @BindView(R.id.videoView)                   CustomVideoView videoView;
        @BindView(R.id.soundsOFFBTN)                ImageView soundsOFFBTN;
        @BindView(R.id.soundsONBTN)                 ImageView soundsONBTN;
        @BindView(R.id.playBTN)                     ImageView playBTN;
        @BindView(R.id.coverIV)                     ImageView coverIV;

        public RequestListener requestListener;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            avatarCIV.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            commentBTN.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            commentCountTXT.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            otherOptionBTN.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            rePostBTN.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            shareBTN.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            rePostCountTXT.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            footerCON.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            socialFeedCON.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            iLikeIV.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            lykaRB.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            titleTTXT.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            sliderLayout.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            parentCON.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            diamondRateTXT.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            contentTXT.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
//            playBTN.setOnClickListener(SocialFeedThreeRecycleViewAdapter.this);
            soundsONBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("EEEEE", "ON");
                    soundsONBTN.setVisibility(View.GONE);
                    soundsOFFBTN.setVisibility(View.VISIBLE);
                    videoView.setVolume(0,0);
                }
            });
            soundsOFFBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("EEEEE", "OFF");
                    soundsONBTN.setVisibility(View.VISIBLE);
                    soundsOFFBTN.setVisibility(View.GONE);
                    videoView.setVolume(10,10);
                }
            });
        }

	}



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.avatarCIV:
                if (clickListener != null && enableUserClick) {
                    clickListener.onAvatarClick(getItem((int) v.getTag(R.id.avatarCIV)).owner.data);
                }
                break;
            case R.id.commentCountTXT:
                if (clickListener != null) {
                    clickListener.onCommentClick(getItem((int) v.getTag(R.id.commentCountTXT)));
                }
                break;
            case R.id.diamondRateTXT:
                if (clickListener != null) {
                    clickListener.onRateClick(getItem((int) v.getTag(R.id.rateTXT)));
                }
                break;
//            case R.id.playBTN:
//                if (clickListener != null) {
//                    clickListener.onPlayClick(getItem((int) v.getTag(R.id.playBTN)));
//                }
//                break;
            case R.id.commentBTN:
                if (clickListener != null) {
                    clickListener.onCommentClick(getItem((int) v.getTag(R.id.commentBTN)));
                }
                break;
//            case R.id.socialFeedCON:
//                if (clickListener != null) {
//                    clickListener.onItemClick(getItem((int) v.getTag(R.id.socialFeedCON)));
//                }
//                break;
            case R.id.parentCON:
                if (clickListener != null) {
                    clickListener.onItemClick(getItem((int) v.getTag(R.id.parentCON)));
                }
                break;
            case R.id.contentTXT:
                if (clickListener != null) {
                    clickListener.onItemClick(getItem((int) v.getTag(R.id.contentTXT)));
                }
                break;
            case R.id.titleTTXT:
                if (clickListener != null) {
                    clickListener.onItemClick(getItem((int) v.getTag(R.id.titleTTXT)));
                }
                break;
            case R.id.otherOptionBTN:
                otherOptionClick(v);
                break;
            case R.id.rePostBTN:
                if (clickListener != null) {
                    clickListener.onRepostClick(getItem((int) v.getTag(R.id.rePostBTN)));
                }
                break;
            case R.id.rePostCountTXT:
                if (clickListener != null) {
                    clickListener.onRepostUsersClick(getItem((int) v.getTag(R.id.rePostCountTXT)));
                }
                break;
            case R.id.shareBTN:
                if (clickListener != null) {
                    clickListener.onShareClick(getItem((int) v.getTag(R.id.shareBTN)));
                }
                break;
            default:
                break;
        }
    }

    private void otherOptionClick(View view) {

        final WishListItem wishListItem = getItem((int) view.getTag(R.id.otherOptionBTN));
        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.social_feed_menu, popup.getMenu());
        popup.getMenu().findItem(R.id.unfollowBTN).setVisible(enableUserClick);
        if(enableUserClick){
            popup.getMenu().findItem(R.id.unfollowBTN).setTitle(wishListItem.owner.data.social.data.is_following.equals("yes") ? "Unfollow" : "Follow" );
        }

        String report = popup.getMenu().findItem(R.id.reportBTN).getTitle().toString();
        SpannableString spannableString = new SpannableString(report);
        spannableString.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(context, R.color.failed)), 0, spannableString.length(), 0);
        popup.getMenu().findItem(R.id.reportBTN).setTitle(spannableString);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.repostBTN:
                        if (clickListener != null) {
                            clickListener.onGrabClick(wishListItem);
                        }
                        break;
                    case R.id.hideBTN:
                        if (clickListener != null){
                            clickListener.onHideClick(wishListItem);
                        }
                        break;
                    case R.id.unfollowBTN:
                        if (clickListener != null) {
                            clickListener.onUnfollowClick(wishListItem);
                        }
                        break;
                    case R.id.reportBTN:
                        if (clickListener != null) {
                            clickListener.onReportClick(wishListItem);
                        }
                        break;
                    case R.id.shareBTN:
                        if (clickListener != null) {
                            clickListener.onShareClick(wishListItem);
                        }
                        break;
                }

                return false;
            }
        });
        popup.show();
    }

    private void setVideo(final CustomVideoView videoView, Uri uri) {
        try {
            videoView.setDataSource(context,uri);
            videoView.setLooping(true);
            videoView.requestFocus();
//            videoView.prepare(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer mp) {
//                    videoView.start();
//                }
//            });

        } catch (IOException ioe) {
            //ignore
        }
    }

    @Override
    public boolean onLongClick(View v) {
//        if(clickListener != null){
//            clickListener.onItemLongClick(data.get((int)v.getTag()));
//        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {

        void onItemClick(WishListItem wishListItem);

        void onCommentClick(WishListItem wishListItem);

        void onAvatarClick(UserItem userItem);

        void onGrabClick(WishListItem wishListItem);

        void onHideClick(WishListItem wishListItem);

        void onUnfollowClick(WishListItem wishListItem);

        void onReportClick(WishListItem wishListItem);

        void onShareClick(WishListItem wishListItem);

        void onHeartClick(WishListItem wishListItem);

        void onRatingClick(WishListItem wishListItem, int rate);

        void onRateClick(WishListItem wishListItem);

        void onPlayClick(WishListItem wishListItem);

        void onRepostClick(WishListItem wishListItem);

        void onRepostUsersClick(WishListItem wishListItem);

    }
}