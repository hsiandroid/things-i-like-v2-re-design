package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.GalleryItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditPhotoListRecycleViewAdapter extends RecyclerView.Adapter<EditPhotoListRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<GalleryItem> data;
    private LayoutInflater layoutInflater;

	public EditPhotoListRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<GalleryItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<GalleryItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(GalleryItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    public File getImage(){
	    return data.get(0).file.getAbsoluteFile();
    }

    public String getImagePath(){
        return data.get(0).file.getAbsolutePath();
    }

    private boolean isDataUnique(GalleryItem newData){
        for(GalleryItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_photo_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.galleryItem = data.get(position);
        holder.view.setTag(holder.galleryItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);

        holder.adapterCON.setTag(holder.galleryItem);
        holder.adapterCON.setOnClickListener(this);

//        Picasso.with(context).load(holder.galleryItem.file)
//                .into(holder.imageView);

        Glide.with(context)
                .load(holder.galleryItem.file)
                .apply(new RequestOptions()
                .fitCenter())
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        GalleryItem galleryItem;

        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.imageIV)             ImageView imageView;


        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((GalleryItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public GalleryItem getItem(int position){
        return getData().get(position);
    }

    public List<GalleryItem> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }

    public interface ClickListener {
        void onItemClick(GalleryItem galleryItem);
    }
} 
