package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.ReportUserItem;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportUserRecycleViewAdapter extends RecyclerView.Adapter<ReportUserRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
    private Context context;
    private List<ReportUserItem> data;
    private LayoutInflater layoutInflater;

    public ReportUserRecycleViewAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }


    public void setNewData(List<ReportUserItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<ReportUserItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(ReportUserItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(ReportUserItem newData){
        for(ReportUserItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_report_user, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.reportUserItem = data.get(position);
        holder.view.setTag(holder.reportUserItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.adapterCON.setTag(holder.reportUserItem);
        holder.adapterCON.setOnClickListener(this);

        Glide.with(context)
                .load(holder.reportUserItem.user.data.image)
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true))
                .into(holder.imageCIV);

        holder.nameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.reportUserItem.user.data.name, holder.reportUserItem.user.data.isVerified, 12));
        holder.commonNameTXT.setText( holder.reportUserItem.user.data.commonName);

        holder.nameTXT.setTag(holder.reportUserItem);
        holder.nameTXT.setOnClickListener(this);

        holder.commonNameTXT.setTag(holder.reportUserItem);
        holder.commonNameTXT.setOnClickListener(this);


        holder.unreportBTN.setTag(holder.reportUserItem);
        holder.unreportBTN.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ReportUserItem reportUserItem;

        @BindView(R.id.imageCIV)            ImageView imageCIV;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.commonNameTXT)       TextView commonNameTXT;
        @BindView(R.id.groupTXT)            TextView groupTXT;
        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.unreportBTN)         TextView unreportBTN;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((ReportUserItem) v.getTag());
                }
                break;
            case R.id.unreportBTN:
                if(clickListener !=null){
                    clickListener.onUnreportUserClick((ReportUserItem)v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(ReportUserItem reportUserItem);
        void onUnreportUserClick(ReportUserItem reportUserItem);
        void onItemLongClick(ReportUserItem reportUserItem);
    }
}