package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.widget.image.FixedHeightImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedCategoryItemRecyclerViewAdapter extends RecyclerView.Adapter<FeedCategoryItemRecyclerViewAdapter.ViewHolder>  implements View.OnClickListener {
	private Context context;
	private List<WishListItem> data;
    private LayoutInflater layoutInflater;
    private int userID = 0;

	public FeedCategoryItemRecyclerViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        userID = UserData.getUserId();
	}

    public void setNewData(List<WishListItem> data){
        this.data.clear();
        Collections.reverse(data);
        for (WishListItem wishListItem : data) {
            this.data.add(wishListItem.minimal(userID));
        }
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_i_like_category_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.wishListItem = data.get(position);

        holder.view.setTag(R.id.iLikeIV, holder.wishListItem);
        holder.view.setOnClickListener(this);

        holder.iLikeIV.autoResize(holder.wishListItem.wishListMin.width == 0);
        holder.iLikeIV.setImageSize(holder.wishListItem.wishListMin.width, holder.wishListItem.wishListMin.height);

        Glide.with(context)
                .load(holder.wishListItem.wishListMin.like_image)
                .apply(new RequestOptions()

                        .fitCenter()
                .placeholder(R.color.light_gray)
                .error(R.color.light_gray)
                .dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.iLikeIV);
        
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        WishListItem wishListItem;

        @BindView(R.id.iLikeIV)
        FixedHeightImageView iLikeIV;
        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((WishListItem) v.getTag(R.id.iLikeIV));
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(WishListItem wishListItem);
    }
} 
