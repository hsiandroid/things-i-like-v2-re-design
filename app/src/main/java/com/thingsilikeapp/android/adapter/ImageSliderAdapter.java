package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.thingsilikeapp.R;
import com.bumptech.glide.Glide;
import com.thingsilikeapp.data.model.ImageModel;

import java.util.List;

public class ImageSliderAdapter extends PagerAdapter implements View.OnClickListener {

    private Context context;
    private LayoutInflater inflater;
    private int COUNT;
    private ClickListener clickListener;

    // list of images
    private List<ImageModel> productImages;

    public ImageSliderAdapter(Context context, List<ImageModel> productImages){
        this.context = context;
        this.productImages = productImages;
        this.COUNT = productImages.size();
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adapter_slide_image,container,false);
        ImageView imageView = view.findViewById(R.id.imageView);
        Glide.with(context)
                .load(productImages.get(position).fullPath)
                .into(imageView);
        imageView.setTag(R.id.imageView,position);
        imageView.setOnClickListener(this);
        container.addView(view);
        return view;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(int pos);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageView:
                clickListener.onItemClick(Integer.parseInt(v.getTag(R.id.imageView).toString()));
                break;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}