package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thingsilikeapp.R;

import java.util.List;

import butterknife.ButterKnife;

public class TemplateRecycleViewAdapter extends RecyclerView.Adapter<TemplateRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<String> data;
    private LayoutInflater layoutInflater;

	public TemplateRecycleViewAdapter(Context context, List<String> data) {
		this.context = context;
		this.data = data;
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<String> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_template, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.string = data.get(position);

        holder.view.setTag(position);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);


//        holder.titleTXT.setText(holder.string.title);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        String string;

//        @BindView(R.id.moduleCIV) ImageView imageView;
//        @BindView(R.id.moduleTV)  TextView timeTXT;
        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((int)v.getTag(), v);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick((int)v.getTag(), v);
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
} 
