package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.CategoryItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapterBak extends RecyclerView.Adapter<CategoryAdapterBak.ViewHolder> implements View.OnClickListener, Filterable{
    private Context context;
    private List<CategoryItem> data;
    private List<CategoryItem> originalData;
    private LayoutInflater layoutInflater;
    private ValueFilter valueFilter;
    private String searchText = "";

    public CategoryAdapterBak(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    public void setNewData(List<CategoryItem> data){
        this.data = data;
        originalData = null;
        notifyDataSetChanged();
    }

//    @Override
//    public int getCount() {
//        return data.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return data.get(position);
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_category_bak, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.categoryItem = data.get(position);
        holder.view.setOnClickListener(this);
        holder.subCategoryTXT.setVisibility(holder.categoryItem.parent.equals("") ? View.GONE : View.VISIBLE);
        highlightSearchText(holder.categoryTXT, holder.categoryItem.title, searchText);
        highlightSearchText(holder.subCategoryTXT, holder.categoryItem.parent, searchText);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return 0;
    }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder holder;
//        if (convertView == null) {
//            convertView = layoutInflater.inflate(R.layout.adapter_category_bak, parent, false);
//            holder = new ViewHolder(convertView);
//            convertView.setTag(holder);
//        }else{
//            holder = (ViewHolder) convertView.getTag();
//        }
//        holder.view.setOnClickListener(this);
//        holder.categoryItem = data.get(position);
//        holder.subCategoryTXT.setVisibility(holder.categoryItem.parent.equals("") ? View.GONE : View.VISIBLE);
//        highlightSearchText(holder.categoryTXT, holder.categoryItem.title, searchText);
//        highlightSearchText(holder.subCategoryTXT, holder.categoryItem.parent, searchText);
//        return convertView;
//    }

    public void highlightSearchText(TextView textView, String full, String search){
        if(search == null){
            highlightSearchTextDefault(textView, full);
            return;
        }

        String fullTXT = full.toLowerCase();
        String searchTXT = search.toLowerCase();

        if(fullTXT.contains(searchTXT)){
            int startPos = fullTXT.indexOf(searchTXT);
            int endPos = startPos + searchTXT.length();

            Spannable spannable = new SpannableString(full);
            spannable.setSpan(new BackgroundColorSpan(ActivityCompat.getColor(context, R.color.colorAccent)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(context, R.color.white)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setText(spannable);
        }else{
            highlightSearchTextDefault(textView, full);
        }

    }

    public void highlightSearchTextDefault(TextView textView, String fullTxt){
        textView.setText(fullTxt);
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).categoryItem);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CategoryItem categoryItem;

        @BindView(R.id.categoryTXT)     TextView categoryTXT;
        @BindView(R.id.subCategoryTXT)  TextView subCategoryTXT;
        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(CategoryItem categoryItem);
    }

    @Override
    public Filter getFilter() {
        if(valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }


    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (originalData == null){
                originalData = data;
            }

            FilterResults results = new FilterResults();
            if(constraint == null && constraint.length() == 0){
                results.count = originalData.size();
                results.values = originalData;
            }else{
                ArrayList<CategoryItem> filterList = new ArrayList<>();
                for(int i=0; i < originalData.size();i++){
                    CategoryItem categoryItem = originalData.get(i);
                    if((categoryItem.title.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(categoryItem);
                    }else if((categoryItem.parent.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(categoryItem);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data = (List<CategoryItem>) results.values;
            searchText = constraint.toString();
            notifyDataSetChanged();
        }
    }

}
