package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.AddressBookItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddressBookRecycleViewAdapter extends RecyclerView.Adapter<AddressBookRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener{
	private Context context;
	private List<AddressBookItem> data;
    private LayoutInflater layoutInflater;

	public AddressBookRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<AddressBookItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void setItemDefault(int id){
        for(AddressBookItem addressBookItem :  data){
            addressBookItem.is_default = addressBookItem.id == id ? "yes" : "no";
        }

        notifyDataSetChanged();
    }

    public int getSelectedID(){
        for (AddressBookItem addressBookItem : data){
            if (addressBookItem.selected){
                return addressBookItem.id;
            }
        }
        return 0;
    }

    public void addNewData(List<AddressBookItem> data){
        for(AddressBookItem addressBookItem :  data){
            this.data.add(addressBookItem);
        }
        notifyDataSetChanged();
    }

    public void removeData(AddressBookItem item){
        for(AddressBookItem addressBookItem : data){
            if(item.id == addressBookItem.id){
                data.remove(addressBookItem);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void removeData(String id){
        for(AddressBookItem addressBookItem : data){
            if(id.equals(addressBookItem.id)){
                data.remove(addressBookItem);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void updateData(AddressBookItem addressBookItem){
        for(AddressBookItem item : data){
            if(item.id == 0){
                if(item.id == addressBookItem.id){
                    int pos = data.indexOf(item);
                    data.set(pos, addressBookItem);
                    notifyItemChanged(pos);
                    break;
                }
            }else if(addressBookItem.id == item.id){
                int pos = data.indexOf(item);
                data.set(pos, addressBookItem);
                notifyItemChanged(pos);
                break;
            }
        }
    }

    public List<AddressBookItem> getData(){
        return data;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_address_book, parent, false));
    }

    public AddressBookItem getItem(int position){
        return data.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.addressBookItem = data.get(position);

        holder.view.setTag(position);
        holder.optionBTN.setTag(position);


        holder.view.setOnClickListener(this);
        holder.optionBTN.setOnClickListener(this);
//        holder.view.setOnLongClickListener(this);

        holder.nameTXT.setText(holder.addressBookItem.address_label);
        holder.contactTXT.setText(holder.addressBookItem.phone_country_dial_code + holder.addressBookItem.phone_number);
        holder.addressTXT.setText(holder.addressBookItem.street_address + " " + holder.addressBookItem.city + ", "
                + holder.addressBookItem.state + ", " + holder.addressBookItem.country);
        holder.defaultTXT.setText(holder.addressBookItem.is_default.equals("yes") ? "default" : "");
        holder.defaultTXT.setTextColor(ActivityCompat.getColor(context, holder.addressBookItem.is_default.equals("yes") ? R.color.colorPrimary : R.color.text_gray));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

//    @Override
//    public boolean onLongClick(View view) {
//        switch (view.getId()){
//            case R.id.adapterCON:
//                if(clickListener != null){
//                    clickListener.onItemLongClick(data.get((int) view.getTag()));
//                }
//                break;
//        }
//        return true;
//    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        AddressBookItem addressBookItem;

        @BindView(R.id.nameTXT)                 TextView nameTXT;
        @BindView(R.id.contactTXT)              TextView contactTXT;
        @BindView(R.id.defaultTXT)              TextView defaultTXT;
        @BindView(R.id.addressTXT)              TextView addressTXT;
        @BindView(R.id.optionBTN)               ImageView optionBTN;

        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addressBookRDBTN:
                break;
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick(data.get((int)v.getTag()));
                }
                break;
            case R.id.optionBTN:
                if(clickListener != null){
                    clickListener.onClick(data.get((int)v.getTag()));
                }
                break;
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(AddressBookItem addressBookItem);
        void onItemLongClick(AddressBookItem addressBookItem);
        void onClick(AddressBookItem addressBookItem);
    }
}
