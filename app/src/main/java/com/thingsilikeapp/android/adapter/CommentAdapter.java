package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.CommentItem;
import com.thingsilikeapp.data.preference.UserData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class CommentAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<CommentItem> data;
    private LayoutInflater layoutInflater;
    private int myID;

	public CommentAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        myID = UserData.getUserId();
	}

    public void addNewData(List<CommentItem> data){
        for(CommentItem commentItem :  data){
            this.data.add(commentItem);
        }
        notifyDataSetChanged();
    }

    public void setNewData(List<CommentItem> data){
        this.data.clear();
        for(CommentItem commentItem :  data){
            this.data.add(commentItem);
            if(this.data.size() >= 2){
                break;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_conversation_in1, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.commentItem = data.get(position);

        if(holder.commentItem.tagged_user_id == 0){
            holder.messageTXT.setText(formatDescription(holder.commentItem));
        }else{
            holder.messageTXT.setText(formatDescriptionWithMention(holder.commentItem));
        }

        holder.dateTXT.setText(holder.commentItem.time_passed);

        holder.messageTXT.setMovementMethod(LinkMovementMethod.getInstance());

        Glide.with(context)
                .load(holder.commentItem.author.data.getAvatar())
                .apply(new RequestOptions()
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .dontTransform()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(holder.avatarCIV);

		return convertView;
	}

    public SpannableString formatDescription(final CommentItem commentItem){
        String userName = commentItem.author.data.username + "  ";
        Log.e("Username", ">>>" + userName);
        if(TextUtils.isEmpty(userName) || userName == null){
            userName = "-";
        }

        String finalMessage = userName + commentItem.content;
        SpannableString spannableString = new SpannableString(finalMessage);

        ClickableSpan nameSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.black));
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_heading)));
            }
        };

        spannableString.setSpan(nameSpan, 0, userName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan messageSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_paragraph)));
            }
        };

        spannableString.setSpan(messageSpan,userName.length(), finalMessage.length(), Spanned.SPAN_INTERMEDIATE);

        return spannableString;
    }

    public String formatContent(final CommentItem commentItem){
        return "@" + commentItem.author.data.username + " " + commentItem.content;
    }

    public SpannableString formatDescriptionWithMention(final CommentItem commentItem){
        String authorName = commentItem.author.data.username + " ";
        String taggedUserName = commentItem.tagged_user.data.username + "  ";
        String mention = "mentioned ";

        if(TextUtils.isEmpty(authorName)){
            authorName = "-";
        }

        String finalMessage = authorName + mention + taggedUserName + commentItem.content;

        SpannableString spannableString = new SpannableString(finalMessage);

        ClickableSpan nameSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.black));
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_heading)));
            }
        };

        spannableString.setSpan(nameSpan, 0, authorName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan taggedSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.black));
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_heading)));
            }
        };

        spannableString.setSpan(taggedSpan, authorName.length() + mention.length(), mention.length() +authorName.length() + taggedUserName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan messageSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_paragraph)));
            }
        };

        spannableString.setSpan(messageSpan, authorName.length() + taggedUserName.length(), finalMessage.length(), Spanned.SPAN_INTERMEDIATE);

        return spannableString;
    }


    @Override
    public void onClick(View v) {
    }


    public class ViewHolder{
        CommentItem commentItem;

        @BindView(R.id.messageTXT)      TextView messageTXT;
        @BindView(R.id.dateTXT)         TextView dateTXT;
        @BindView(R.id.replyBTN)        View replyBTN;
        @BindView(R.id.commentCON)      View commentCON;
        @BindView(R.id.avatarCIV)       ImageView avatarCIV;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onTemplateItemClickListener(CommentItem string);
    }
} 
