package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.RewardCategoryItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RewardCategoryRecycleViewAdapter extends RecyclerView.Adapter<RewardCategoryRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener,
        View.OnLongClickListener {
	private Context context;
	private List<RewardCategoryItem> data;
    private LayoutInflater layoutInflater;

	public RewardCategoryRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<RewardCategoryItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<RewardCategoryItem> data){
        for(RewardCategoryItem item :  data){
            this.data.add(item);
        }
        notifyDataSetChanged();
    }

    public void reset(){
        this.data = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setSelected(String title){
        for(RewardCategoryItem rewardCategoryItem : data){
            rewardCategoryItem.selected = rewardCategoryItem.title.equalsIgnoreCase(title);
        }
        notifyDataSetChanged();
    }

    public String getSelectedTitle(){
        for(RewardCategoryItem rewardCategoryItem : data){
            if(rewardCategoryItem.selected){
                return rewardCategoryItem.title;
            }
        }
        return "";
    }

    public List<RewardCategoryItem> getData(){
        return data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_reward_category, parent, false));
    }

    public RewardCategoryItem getItem(int position){
        return data.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.groupFollowingItem = data.get(position);
        holder.view.setTag(holder.groupFollowingItem);
        holder.view.setOnClickListener(this);
        holder.headerTXT.setSelected(holder.groupFollowingItem.selected);
        holder.headerTXT.setText(holder.groupFollowingItem.title);
        Picasso.with(context).load(holder.groupFollowingItem.image).centerCrop().fit().into(holder.imageIV);

//        holder.headerTXT.setTextColor(holder.groupFollowingItem.selected ? ActivityCompat.getColor(context, R.color.colorPrimary) : ActivityCompat.getColor(context, R.color.text_gray));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        RewardCategoryItem groupFollowingItem;

        @BindView(R.id.headerTXT)    TextView headerTXT;
        @BindView(R.id.imageIV)      ImageView imageIV;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        RewardCategoryItem groupItem = (RewardCategoryItem) v.getTag();
        setSelected(groupItem.title);

        if(clickListener != null){
            clickListener.onCategoryItemClick(groupItem);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onCategoryItemClick(RewardCategoryItem rewardCategoryItem);
    }
} 
