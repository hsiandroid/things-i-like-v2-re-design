package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.ImageModel;
import com.thingsilikeapp.data.model.WishListItem;

import java.util.ArrayList;
import java.util.List;

public class ItemSliderAdapter extends PagerAdapter {

    private List<ImageModel> IMAGES;
    private LayoutInflater inflater;
    private Context context;


    public ItemSliderAdapter(Context context,List<ImageModel> IMAGES) {
        this.context = context;
        this.IMAGES=IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.image_slider_adapter, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.imageView);
        final TextView textView = (TextView) imageLayout.findViewById(R.id.textView);

//        imageView.setImageResource(IMAGES.get(position));

        Log.e("EEEEEE", ">>>>>>>>>>>>" +  IMAGES.get(position).getImage());
        Picasso.with(context).load(IMAGES.get(position).getImage()).fit().centerCrop().into(imageView);
        textView.setText("Position:" + position);

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
