package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListTransactionItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class RequestTransactionAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<WishListTransactionItem> data;
    private LayoutInflater layoutInflater;

	public RequestTransactionAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<WishListTransactionItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_transaction_request, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.wishListTransactionItem = data.get(position);

        holder.iLikeIV.setOnClickListener(this);
        holder.avatarCIV.setOnClickListener(this);

        holder.declineTXT.setOnClickListener(this);
        holder.acceptTXT.setOnClickListener(this);

        holder.iLikeIV.setTag(holder.wishListTransactionItem);
        holder.avatarCIV.setTag(holder.wishListTransactionItem.sender.data);
        holder.declineTXT.setTag(holder.wishListTransactionItem);
        holder.acceptTXT.setTag(holder.wishListTransactionItem);

        holder.descriptionTXT.setText(formatDescription(holder, position));
        holder.descriptionTXT.setMovementMethod(LinkMovementMethod.getInstance());

        holder.dateTXT.setText(holder.wishListTransactionItem.time_passed);
        Glide.with(context)
                .load(holder.wishListTransactionItem.image.data.full_path)
                .into(holder.iLikeIV);

        Glide.with(context)
                .load(holder.wishListTransactionItem.sender.data.getAvatar())
                        .apply(new RequestOptions()

                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar))
                .into(holder.avatarCIV);

		return convertView;
	}

    public SpannableString formatDescription(final ViewHolder holder, final int position){
        String  extraMessage = " just sent you ";
        holder.extraMessageTXT.setVisibility(View.VISIBLE);
        holder.extraMessageTXT.setText("Have you received the actual item?");

        String userName = holder.wishListTransactionItem.sender.data.name;

        String finalMessage = userName + extraMessage + holder.wishListTransactionItem.title;
        SpannableString spannableString = new SpannableString(finalMessage);

        ClickableSpan nameSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onAvatarClick(data.get(position).sender.data);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_display_name)));
            }
        };
        spannableString.setSpan(nameSpan, 0, userName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan productSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onItemClick(data.get(position));
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.colorPrimary));
            }
        };

        spannableString.setSpan(productSpan, userName.length() + extraMessage.length(), finalMessage.length(), Spanned.SPAN_INTERMEDIATE);

        return spannableString;
    }

    public void removeItem(WishListTransactionItem wishListTransactionItem){
        for(WishListTransactionItem item : data){
            if(item.id == wishListTransactionItem.id){
                data.remove(item);
                notifyDataSetChanged();
                break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){

            switch (v.getId()){
                case R.id.avatarCIV:
                    clickListener.onAvatarClick((UserItem) v.getTag());
                    break;
                case R.id.acceptTXT:
                    clickListener.onYesClick((WishListTransactionItem) v.getTag());
                    break;
                case R.id.declineTXT:
                    clickListener.onNoClick((WishListTransactionItem) v.getTag());
                    break;
                case R.id.iLikeIV:
                    clickListener.onItemClick((WishListTransactionItem) v.getTag());
                    break;
            }
        }
    }


    public class ViewHolder{
        WishListTransactionItem wishListTransactionItem;

        @BindView(R.id.iLikeIV)             ImageView iLikeIV;
        @BindView(R.id.declineTXT)          TextView declineTXT;
        @BindView(R.id.acceptTXT)           TextView acceptTXT;
        @BindView(R.id.descriptionTXT)      TextView descriptionTXT;
        @BindView(R.id.extraMessageTXT)     TextView extraMessageTXT;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.avatarCIV)           ImageView avatarCIV;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(WishListTransactionItem wishListTransactionItem);
        void onAvatarClick(UserItem userItem);
        void onYesClick(WishListTransactionItem wishListTransactionItem);
        void onNoClick(WishListTransactionItem wishListTransactionItem);
    }
} 
