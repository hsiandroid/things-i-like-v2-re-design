package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.FeedCategoryItem;
import com.thingsilikeapp.data.model.WishListItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedCategoryRecyclerViewAdapter extends RecyclerView.Adapter<FeedCategoryRecyclerViewAdapter.ViewHolder>  implements
        View.OnClickListener,
        FeedCategoryItemRecyclerViewAdapter.ClickListener{
	private Context context;
	private List<FeedCategoryItem> data;
    private LayoutInflater layoutInflater;

	public FeedCategoryRecyclerViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<FeedCategoryItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<FeedCategoryItem> data) {
        int initialCount = this.data.size();
        int newCount = 0;
        for (FeedCategoryItem feedCategoryItem : data) {
            if (isDataUnique(feedCategoryItem)) {
                this.data.add(feedCategoryItem);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(FeedCategoryItem newData) {
        return !data.contains(newData);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_i_like_category, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.feedCategoryItem = data.get(position);

        holder.view.setTag(holder.feedCategoryItem);
        holder.view.setOnClickListener(this);

        holder.typeTXT.setText(holder.feedCategoryItem.type);
        holder.feedCategoryItemRecyclerViewAdapter.setNewData(holder.feedCategoryItem.data);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onItemClick(WishListItem wishListItem) {
        if(clickListener != null){
            clickListener.onWishListClick(wishListItem);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        FeedCategoryItem feedCategoryItem;
        FeedCategoryItemRecyclerViewAdapter feedCategoryItemRecyclerViewAdapter;
        LinearLayoutManager linearLayoutManager;

        @BindView(R.id.typeTXT)         TextView typeTXT;
        @BindView(R.id.wishlistRV)      RecyclerView wishlistRV;
        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            linearLayoutManager = new LinearLayoutManager(context);
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            linearLayoutManager.setReverseLayout(false);

            feedCategoryItemRecyclerViewAdapter = new FeedCategoryItemRecyclerViewAdapter(context);
            feedCategoryItemRecyclerViewAdapter.setOnItemClickListener(FeedCategoryRecyclerViewAdapter.this);
            wishlistRV.setNestedScrollingEnabled(false);
            wishlistRV.setLayoutManager(linearLayoutManager);
            wishlistRV.setAdapter(feedCategoryItemRecyclerViewAdapter);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((FeedCategoryItem) v.getTag());
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(FeedCategoryItem feedCategoryItem);
        void onWishListClick(WishListItem wishListItem);
    }
} 
