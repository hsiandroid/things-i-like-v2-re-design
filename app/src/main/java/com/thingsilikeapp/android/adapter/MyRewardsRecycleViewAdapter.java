package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.MyRewardsItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyRewardsRecycleViewAdapter extends RecyclerView.Adapter<MyRewardsRecycleViewAdapter.ViewHolder>  implements View.OnClickListener {
	private Context context;
	private List<MyRewardsItem> data;
    private LayoutInflater layoutInflater;

	public MyRewardsRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<MyRewardsItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<MyRewardsItem> data){
        for(MyRewardsItem item :  data){
            this.data.add(item);
        }
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_my_rewards, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.myRewardsItem = data.get(position);

        holder.view.setTag(holder.myRewardsItem);
        holder.view.setOnClickListener(this);

        Glide.with(context)
                .load(holder.myRewardsItem.image.data.full_path)
                .apply(new RequestOptions()
                .placeholder(R.color.light_gray)
                .error(R.color.light_gray))
                .into(holder.imageIV);
        holder.typeIV.setImageDrawable(
                ActivityCompat.getDrawable(context,
                        holder.myRewardsItem.reward_type.equalsIgnoreCase("crystal") ?
                                R.drawable.icon_lyka_gems :
                                R.drawable.icon_lyka_chips));

//        Log.e("Type", ">>>" + holder.myRewardsItem.reward_type);

        holder.titleTXT.setText(holder.myRewardsItem.title);
        holder.dateTXT.setText(holder.myRewardsItem.created_at.time_passed);
        holder.statusTXT.setText(holder.myRewardsItem.status);
        holder.valueTXT.setText(holder.myRewardsItem.value);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        MyRewardsItem myRewardsItem;

        @BindView(R.id.imageIV)                 ImageView imageIV;
        @BindView(R.id.typeIV)                 ImageView typeIV;
        @BindView(R.id.titleTXT)                TextView titleTXT;
        @BindView(R.id.dateTXT)                 TextView dateTXT;
        @BindView(R.id.statusTXT)               TextView statusTXT;
        @BindView(R.id.valueTXT)                TextView valueTXT;
        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((MyRewardsItem) v.getTag());
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(MyRewardsItem myRewardsItem);
    }
} 
