package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupFollowersRecycleViewAdapter extends RecyclerView.Adapter<GroupFollowersRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<UserItem> data;
    private LayoutInflater layoutInflater;

	public GroupFollowersRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public List<UserItem> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }

    public void removeItem(int id) {
//        for (UserItem defaultItem : data) {
//            if (id == defaultItem.id) {
                int pos = id;
                data.remove(pos);
                notifyItemRemoved(pos);
                notifyItemRangeChanged(pos, data.size());
//                break;
//            }
//        }
    }

    public void reset(){
        this.data = new ArrayList<>();
        notifyDataSetChanged();
    }

    public String getSelectedItems(){
        String total = "";
        for (UserItem userModel : data){
            total = total + userModel.id + ",";
        }
        return total;
    }

    public void setNewData(List<UserItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<UserItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(UserItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(UserItem newData){
        for(UserItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_group_followers, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.userItem = data.get(position);
        holder.view.setTag(holder.userItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.adapterCON.setTag(holder.userItem);
        holder.adapterCON.setOnClickListener(this);

        holder.removeCV.setTag(position);
        holder.removeCV.setOnClickListener(this);

        Glide.with(context)
                .load(holder.userItem.getAvatar())
                .apply(new RequestOptions()
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true))
                .into(holder.imageCIV);

        holder.nameTXT.setText( holder.userItem.name);
        holder.nameTXT.setTag(holder.userItem);
        holder.nameTXT.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        UserItem userItem;

        @BindView(R.id.imageCIV)            ImageView imageCIV;
        @BindView(R.id.removeCV)            ImageView removeCV;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.adapterCON)          View adapterCON;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.removeCV:
                if(clickListener != null){
                    clickListener.onRemoveClick((int) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onRemoveClick(int position);
        void onItemLongClick(UserItem userItem);
    }
}