package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.explore.ExploreFragment;
import com.thingsilikeapp.data.model.BirthdayItem;
import com.thingsilikeapp.data.model.CommentItem;
import com.thingsilikeapp.data.model.UserItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TopUserRecycleViewAdapter extends RecyclerView.Adapter<TopUserRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<UserItem> data;

    private LayoutInflater layoutInflater;

	public TopUserRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<UserItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public UserItem getSelectedItems(){
        UserItem userItem = null;
        for (UserItem specialtyModel : data){
                userItem = specialtyModel;
        }

        return userItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_exp_top_trending, parent, false));

    }

    public void addNewData(List<UserItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(UserItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    public void removeItem(int id) {
        for (UserItem defaultItem : data) {
            if (id == defaultItem.id) {
                int pos = data.indexOf(defaultItem);
                data.remove(pos);
                notifyItemRemoved(pos);
                notifyItemRangeChanged(pos, data.size());
                break;
            }
        }
    }

    private boolean isDataUnique(UserItem newData){
        for(UserItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.userItem = data.get(position);
        holder.view.setTag(holder.userItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.adapterCON.setTag(holder.userItem);
        holder.adapterCON.setOnClickListener(this);
        holder.followBTN.setTag(holder.userItem);
        holder.followBTN.setOnClickListener(this);
        holder.nameTXT.setText(holder.userItem.name);
        if (holder.userItem.social.data.is_following.equals("yes")){
         holder.followBTN.setText("Following");
         holder.followBTN.setEnabled(false);
        } else {
            holder.followBTN.setText("Follow");
            holder.followBTN.setEnabled(true);
        }

//        Picasso.with(context).load(holder.userItem.image).placeholder(R.drawable.explorebg).error(R.drawable.explorebg).into(holder.avatarIV);
	    Picasso.with(context).load(holder.userItem.image).placeholder(R.drawable.placeholder_avatar).error(R.drawable.placeholder_avatar).into(holder.avatarIV);
	}

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        UserItem userItem;

        @BindView(R.id.profileCV)           ImageView avatarIV;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.followBTN)           TextView followBTN;
        @BindView(R.id.adapterCON)          View adapterCON;


        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);

        }
	}

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((UserItem) v.getTag());
                }
                break;
            case R.id.followBTN:
                if(clickListener != null){
                    clickListener.onFollowClick((UserItem) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick(data.get((int)v.getTag()));
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(UserItem userItem);
        void onFollowClick(UserItem userItem);
        void onItemLongClick(UserItem userItem);
    }
} 
