package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.thingsilikeapp.android.fragment.create_post.GalleryPostFragment;
import com.thingsilikeapp.android.fragment.create_post.LinkPostFragment;
import com.thingsilikeapp.android.fragment.create_post.PhotoPostFragment;
import com.thingsilikeapp.android.fragment.create_post.VideoPostFragment;
import com.thingsilikeapp.android.fragment.messages.GroupMessageFragment;
import com.thingsilikeapp.android.fragment.messages.PrivateMessageFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;


/**
 * Created by Ken Drew on 11/11/2017.
 */

public class CreatePostPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 4;
    private Context context;
    public GalleryPostFragment gallery;
    public PhotoPostFragment photo;
    public VideoPostFragment videos;

    public Callback callback;

    public CreatePostPagerAdapter(FragmentManager fragmentManager, Context c) {
        super(fragmentManager);
        context = c;
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                gallery = GalleryPostFragment.newInstance();
                gallery.callback = new GalleryPostFragment.Callback() {
//                    @Override
//                    public void onCropView(boolean crop){
//                        if(callback!=null){
//                            callback.onCropView(crop);
//                        }
//                    }
                };
                return gallery;
            case 1:
                photo = PhotoPostFragment.newInstance();

                photo.callback = new PhotoPostFragment.Callback() {
                    @Override
                    public void onCaptureSuccess(boolean capture) {
                        if(callback != null){
                            callback.onCaptureSuccess(true);
                        }
                    }

                    @Override
                    public void onFlash(boolean flash) {
                        if(callback != null){
                            callback.onFlash(flash);
                        }
                    }

                    @Override
                    public void onCameraView(boolean facing) {
                        if(callback != null){
                            callback.onCameraView(facing);
                        }
                    }


                };

                return photo;
            case 2:
                videos = VideoPostFragment.newInstance();

                videos.callback = new VideoPostFragment.Callback() {
                    @Override
                    public void onVideoSuccess(boolean capture) {
                        if(callback != null){
                            callback.onVideoSuccess(capture);
                        }
                    }

                    @Override
                    public void onFlash(boolean flash) {
                        if(callback != null){
                            callback.onFlash(flash);
                        }
                    }

                    @Override
                    public void onCameraView(boolean facing) {
                        if(callback != null){
                            callback.onCameraView(facing);
                        }
                    }
//                    @Override
//                    public void onCropView(boolean crop){
//                        if(callback!=null){
//                            callback.onCropView(crop);
//                        }
//                    }



                };

                return videos;
            case 3:
                return LinkPostFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_UNCHANGED;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Gallery";
            case 1:
                return "Photo";
            case 2:
                return "Video";
            case 3:
                return "Link";
            default:
                return "Page" + position;

        }

    }

    public interface Callback {
        void onCaptureSuccess(boolean capture);
        void onFlash(boolean flash);
        void onCameraView(boolean cameraView);
        void onVideoSuccess(boolean capture);
    }
}