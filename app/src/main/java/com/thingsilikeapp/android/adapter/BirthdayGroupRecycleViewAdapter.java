package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.GroupItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BirthdayGroupRecycleViewAdapter extends RecyclerView.Adapter<BirthdayGroupRecycleViewAdapter.ViewHolder>  implements View.OnClickListener{
	private Context context;
	private List<GroupItem> data;
    private LayoutInflater layoutInflater;

	public BirthdayGroupRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<GroupItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void setSelected(String title){
        for(GroupItem groupItem : data){
            groupItem.selected = groupItem.title.equalsIgnoreCase(title);
        }
        notifyDataSetChanged();
    }

    public String getSelectedTitle(){
        for(GroupItem groupItem : data){
            if(groupItem.selected){
                return groupItem.title;
            }
        }
        return "";
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_celebrant_group, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.groupItem = data.get(position);

        holder.view.setTag(holder.groupItem);
        holder.view.setOnClickListener(this);
        holder.headerTXT.setSelected(holder.groupItem.selected);
        holder.headerTXT.setText(holder.groupItem.title);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        GroupItem groupItem;

        @BindView(R.id.headerTXT)    TextView headerTXT;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        GroupItem groupItem = (GroupItem) v.getTag();
        setSelected(groupItem.title);

        if(clickListener != null){
            clickListener.onItemClick(groupItem);
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(GroupItem groupItem);
    }
} 
