package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.SearchItem;
import com.thingsilikeapp.data.preference.UserData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchRecycleViewAdapter extends RecyclerView.Adapter<SearchRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<SearchItem> data;
    private LayoutInflater layoutInflater;
    private int myID;

	public SearchRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        myID = UserData.getUserId();
	}

    public void setNewData(List<SearchItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<SearchItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(SearchItem searchItem :  data){
            if(isDataUnique(searchItem)){
                this.data.add(searchItem);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(SearchItem newData){
        for(SearchItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_search, parent, false));
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.searchItem = data.get(position);

        holder.view.setTag(holder.searchItem);
        holder.view.setOnClickListener(this);
        holder.nameTXT.setText(holder.searchItem.keyword);

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        SearchItem searchItem;

        @BindView(R.id.nameTXT)      TextView nameTXT;
        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            switch (v.getId()){
                default:
                    clickListener.onItemClick((SearchItem) v.getTag());
            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()){
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(SearchItem searchItem);
        void onItemLongClick(SearchItem searchItem);
    }
} 
