package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.RewardHistoryItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RewardHistoryRecycleViewAdapter extends RecyclerView.Adapter<RewardHistoryRecycleViewAdapter.ViewHolder>  implements View.OnClickListener {
	private Context context;
	private List<RewardHistoryItem> data;
    private LayoutInflater layoutInflater;

	public RewardHistoryRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        this.layoutInflater = LayoutInflater.from(context);
	}

    public void addNewData(List<RewardHistoryItem> data){
        for(RewardHistoryItem item :  data){
            this.data.add(item);
        }
        notifyDataSetChanged();
    }

    public void setNewData(List<RewardHistoryItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_reward_history, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.rewardHistoryItem = data.get(position);

        holder.view.setTag(holder.rewardHistoryItem);
        holder.view.setOnClickListener(this);

        holder.activityCON.setTag(holder.rewardHistoryItem);
        holder.activityCON.setOnClickListener(this);

        holder.titleTXT.setText(holder.rewardHistoryItem.title);
        holder.descriptionTXT.setText(holder.rewardHistoryItem.remarks);
        holder.dateTXT.setText(holder.rewardHistoryItem.createdAt.dateDb);
        holder.timeTXT.setText(holder.rewardHistoryItem.createdAt.time);

//        holder.typeIV.setImageDrawable(
//                ActivityCompat.getDrawable(context,
//                        holder.rewardHistoryItem.reward_type.equalsIgnoreCase("crystal") ?
//                                R.drawable.icon_lyka_gems :
//                                R.drawable.icon_lyka_chips));

        boolean isCredit = holder.rewardHistoryItem.type.equalsIgnoreCase("credit");
        String value = String.valueOf(isCredit ? "+ " : "- ") + holder.rewardHistoryItem.value;
        holder.valueTXT.setText(value);
        holder.valueTXT.setTextColor(ActivityCompat.getColor(context, isCredit ? R.color.earnAdd : R.color.earnLess));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        RewardHistoryItem rewardHistoryItem;

        @BindView(R.id.titleTXT)                TextView titleTXT;
        @BindView(R.id.descriptionTXT)          TextView descriptionTXT;
        @BindView(R.id.dateTXT)                 TextView dateTXT;
        @BindView(R.id.timeTXT)                 TextView timeTXT;
        @BindView(R.id.valueTXT)                TextView valueTXT;
        @BindView(R.id.activityCON)             View activityCON;


        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
	    switch (v.getId()){
            case R.id.activityCON:
                if(clickListener != null){
                    clickListener.onItemClick((RewardHistoryItem) v.getTag());
                }
                break;
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(RewardHistoryItem rewardHistoryItem);
    }
} 
