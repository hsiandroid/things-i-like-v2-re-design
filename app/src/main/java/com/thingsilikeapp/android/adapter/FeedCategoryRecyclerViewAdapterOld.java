package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.FeedCategoryItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedCategoryRecyclerViewAdapterOld extends RecyclerView.Adapter<FeedCategoryRecyclerViewAdapterOld.ViewHolder>  implements View.OnClickListener {
	private Context context;
	private List<FeedCategoryItem> data;
    private LayoutInflater layoutInflater;

	public FeedCategoryRecyclerViewAdapterOld(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<FeedCategoryItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<FeedCategoryItem> data) {
        int initialCount = this.data.size();
        int newCount = 0;
        for (FeedCategoryItem wishListItem : data) {
            this.data.add(wishListItem);
            newCount++;
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_i_like_category, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.feedCategoryItem = data.get(position);

        holder.view.setTag(position);
        holder.view.setOnClickListener(this);

        holder.typeTXT.setText(holder.feedCategoryItem.type);
        holder.feedCategoryAdapter.setNewData(holder.feedCategoryItem.data);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        FeedCategoryItem feedCategoryItem;
        FeedCategoryAdapter feedCategoryAdapter;

        @BindView(R.id.typeTXT)         TextView typeTXT;
        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);

//            feedCategoryAdapter = new FeedCategoryAdapter(context, wishlistTWV);
//            wishlistTWV.setNestedScrollingEnabled(true);
//            wishlistTWV.setAdapter(feedCategoryAdapter);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((int)v.getTag(), v);
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }
} 
