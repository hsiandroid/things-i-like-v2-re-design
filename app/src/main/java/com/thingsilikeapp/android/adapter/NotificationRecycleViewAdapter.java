package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListTransactionItem;
import com.thingsilikeapp.data.model.WishListViewerItem;
import com.thingsilikeapp.vendor.android.java.decoration.HeaderItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class NotificationRecycleViewAdapter extends RecyclerView.Adapter<NotificationRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener,
        HeaderItemDecoration.StickyHeaderInterface {

	private Context context;
    private LayoutInflater layoutInflater;
    private float density;

    //data
    private List<Data> data;
    private List<WishListViewerItem> addressItems;
    private List<WishListTransactionItem> giftItems;
    private List<WishListViewerItem> ongoingItems;
    private int requestCount = 0;

    //controls
    private ClickListener clickListener;

	public NotificationRecycleViewAdapter(Context context) {
		this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.initAdapter();
        this.density = context.getResources().getDisplayMetrics().density;
	}

	private void initAdapter(){
        data = new ArrayList<>();
        addressItems = new ArrayList<>();
        giftItems = new ArrayList<>();
        ongoingItems = new ArrayList<>();
    }

    public void compactData(List<WishListViewerItem> addressItems, List<WishListTransactionItem> giftItems, List<WishListViewerItem> ongoingItems){

        this.data.clear();
        this.addressItems = addressItems;
        this.giftItems = giftItems;
        this.ongoingItems = ongoingItems;

        if(this.addressItems.size() > 0){
            this.data.add(new Data(Data.HEADER, Data.ADDRESS));
            for(WishListViewerItem item : this.addressItems){
                this.data.add(new Data(Data.ADDRESS, item.id, item));
            }
        }

        if(this.giftItems.size() > 0){
            this.data.add(new Data(Data.HEADER, Data.GIFT));
            for(WishListTransactionItem item : this.giftItems){
                this.data.add(new Data(Data.GIFT, item.id, item));
            }
        }

        if(this.ongoingItems.size() > 0){
            this.data.add(new Data(Data.HEADER, Data.ONGOING));
            for(WishListViewerItem item : this.ongoingItems){
                this.data.add(new Data(Data.ONGOING, item.id, item));
            }
        }

        notifyDataSetChanged();
    }

    //-------------------------
    //    address controls
    //-------------------------

    public void addressAddAll(List<WishListViewerItem> addressItems){
        int start = findHeaderIndex(Data.ADDRESS);
        if(start == -1 && addressItems.size() > 0){
            this.data.add(0, new Data(Data.HEADER, Data.ADDRESS));
        }
        start = findHeaderIndex(Data.ADDRESS) + this.addressItems.size() + 1;
        for(WishListViewerItem item : addressItems){
            this.data.add(start, new Data(Data.ADDRESS, item.id, item));
            this.addressItems.add(item);
            start++;
        }
        notifyDataSetChanged();
    }

    public void removeAddressItem(int referenceId){

        for(Data item : data){
            if(item.referenceId == referenceId && item.viewType == Data.ADDRESS){
                int pos = data.indexOf(item);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }

        for(WishListViewerItem item : addressItems){
            if(item.id == referenceId){
                this.addressItems.remove(addressItems.indexOf(item));
                break;
            }
        }

        if(this.addressItems.size() == 0){
            clearAddress();
        }
    }

    private int getLastAddressItemPosition(){
        int start = findHeaderIndex(Data.ADDRESS);
        if(start > -1){
            return start + this.addressItems.size();
        }
        return -1;
    }

    public void clearAddress(){
        displayShowMore(Data.ADDRESS, false);
        int start = findHeaderIndex(Data.ADDRESS);
        if(start > -1){
            int end = start + addressItems.size();
            if(start == end){
                data.remove(start);
                notifyItemRemoved(start);
            }else{
                removeDataRange(start, end);
                notifyDataSetChanged();
            }
            addressItems.clear();
        }
    }

    //-------------------------
    //    address controls
    //-------------------------



    //-------------------------
    //    gift controls
    //-------------------------

    public void giftAddAll(List<WishListTransactionItem> giftItems){
        int start = findHeaderIndex(Data.GIFT);
        if(start == -1 && giftItems.size() > 0){
            int addressPos = getLastAddressItemPosition();
            addressPos = addressPos == -1 ? 0 : addressPos + 1;
            this.data.add(addressPos, new Data(Data.HEADER, Data.GIFT));
        }
        start = findHeaderIndex(Data.GIFT) + this.giftItems.size() + 1;
        for(WishListTransactionItem item : giftItems){
            this.data.add(start, new Data(Data.GIFT, item.id, item));
            this.giftItems.add(item);
            start++;
        }
        notifyDataSetChanged();
    }


    public void removeGiftItem(int referenceId){

        for(Data item : data){
            if(item.referenceId == referenceId && item.viewType == Data.GIFT){
                int pos = data.indexOf(item);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }

        for(WishListTransactionItem item : giftItems){
            if(item.id == referenceId){
                this.giftItems.remove(giftItems.indexOf(item));
                break;
            }
        }

        if(this.giftItems.size() == 0){
            clearGift();
        }
    }

    private int getLastGiftItemPosition(){
        int start = findHeaderIndex(Data.GIFT);
        if(start > -1){
            return start + this.giftItems.size();
        }else{
            return getLastAddressItemPosition();
        }
    }

    public void clearGift(){
        displayShowMore(Data.GIFT, false);
        int start = findHeaderIndex(Data.GIFT);
        if(start > -1){
            int end = start + giftItems.size();
            if(start == end){
                data.remove(start);
                notifyItemRemoved(start);
            }else{
                removeDataRange(start, end);
                notifyDataSetChanged();
            }
            giftItems.clear();
        }
    }

    //-------------------------
    //    gift controls
    //-------------------------


    //-------------------------
    //    ongoing controls
    //-------------------------

    public void onGoingAddAll(List<WishListViewerItem> ongoingItems){
        int start = findHeaderIndex(Data.ONGOING);
        if(start == -1 && ongoingItems.size() > 0){
            int addressPos = getLastGiftItemPosition();
            addressPos = addressPos == -1 ? 0 : addressPos + 1;
            this.data.add(addressPos, new Data(Data.HEADER, Data.ONGOING));
        }
        start = findHeaderIndex(Data.ONGOING) + this.ongoingItems.size() + 1;
        for(WishListViewerItem item : ongoingItems){
            this.data.add(start, new Data(Data.ONGOING, item.id, item));
            this.ongoingItems.add(item);
            start++;
        }
        notifyDataSetChanged();
    }

    public void removeOnGoingItem(int referenceId){

        for(Data item : data){
            if(item.referenceId == referenceId && item.viewType == Data.ONGOING){
                int pos = data.indexOf(item);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }

        for(WishListViewerItem item : ongoingItems){
            if(item.id == referenceId){
                this.ongoingItems.remove(ongoingItems.indexOf(item));
                break;
            }
        }

        if(this.ongoingItems.size() == 0){
            clearOnGoing();
        }
    }

    public void clearOnGoing(){
        displayShowMore(Data.ONGOING, false);
        int start = findHeaderIndex(Data.ONGOING);
        if(start > -1){
            int end = start + ongoingItems.size();
            if(start == end){
                data.remove(start);
                notifyItemRemoved(start);
            }else{
                removeDataRange(start, end);
                notifyDataSetChanged();
            }
            ongoingItems.clear();
        }
    }

    //-------------------------
    //    ongoing controls
    //-------------------------

    public void displayShowMore(int viewType, boolean show){
        int pos = findShowMoreIndex(viewType);
        if(show){
            if(pos == -1){
                pos = findHeaderIndex(viewType);
                if(pos > -1){
                    switch (viewType){
                        case Data.ADDRESS:
                            pos = pos + addressItems.size() + 1;
                            break;
                        case Data.GIFT:
                            pos = pos + giftItems.size() + 1;
                            break;
                        case Data.ONGOING:
                            pos = pos + ongoingItems.size() + 1;
                            break;
                    }
                    this.data.add(pos, new Data(Data.SHOW_MORE, viewType));
                    notifyDataSetChanged();
                }
            }
        }else{
            if(pos > -1){
                data.remove(pos);
                notifyDataSetChanged();
            }
        }
    }

    public boolean hasShowMoreButton(int viewType){
        return findShowMoreIndex(viewType) != -1;
    }


    private int findHeaderIndex(int referenceId){
        for(Data item : data){
            if(item.viewType == Data.HEADER && item.referenceId == referenceId){
                return data.indexOf(item);
            }
        }
        return -1;
    }

    private int findShowMoreIndex(int referenceId){
        for(Data item : data){
            if(item.viewType == Data.SHOW_MORE && item.referenceId == referenceId){
                return data.indexOf(item);
            }
        }
        return -1;
    }

    public void showMoreLoading(int viewType, boolean show){
        int pos = findShowMoreIndex(viewType);
        if(pos > -1){
            data.get(pos).showLoading = show;
            notifyItemChanged(pos);
        }
    }

    private void removeDataRange(int start, int end){
        if(start >= data.size()){
            return;
        }

        if(end >= data.size()){
            return;
        }

        if(start >= end){
            return;
        }

        while (start != end + 1){
            data.remove(end);
            end--;
        }
    }

    public void setRequestCount(int requestCount) {
        this.requestCount = requestCount;
    }

    public int getRequestCount() {
        return requestCount;
    }

    public int getRequestCount(int less) {
        return requestCount + less;
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).viewType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case Data.HEADER:
                return new ViewHolder(layoutInflater.inflate(R.layout.adapter_notification_header, parent, false)).bindHeader();
            case Data.SHOW_MORE:
                return new ViewHolder(layoutInflater.inflate(R.layout.adapter_notification_more, parent, false)).bindShowMore();
            case Data.ADDRESS:
                return new ViewHolder(layoutInflater.inflate(R.layout.adapter_viewer_request, parent, false)).bindAddress();
            case Data.GIFT:
                return new ViewHolder(layoutInflater.inflate(R.layout.adapter_transaction_request, parent, false)).bindGift();
            case Data.ONGOING:
                return new ViewHolder(layoutInflater.inflate(R.layout.adapter_owned_request, parent, false)).bindOnGoing();
            default:
                return new ViewHolder(layoutInflater.inflate(R.layout.adapter_template, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.data = data.get(position);
        switch (holder.data.viewType){
            case Data.HEADER:
                headerBinder(holder, position);
                break;
            case Data.SHOW_MORE:
                moreBinder(holder);
                break;
            case Data.ADDRESS:
                addressBinder(holder, position);
                break;
            case Data.GIFT:
                giftBinder(holder, position);
                break;
            case Data.ONGOING:
                ongoingBinder(holder, position);
                break;
        }
    }

    private void headerBinder(ViewHolder holder, int position){
        switch (holder.data.referenceId){
            case Data.ADDRESS:
                holder.headerView.headerTXT.setText("Delivery Information Requests");
                break;
            case Data.GIFT:
                holder.headerView.headerTXT.setText("Pending Gifts");
                break;
            case Data.ONGOING:
                holder.headerView.headerTXT.setText("Outgoing Requests");
                break;
        }
    }

    private void moreBinder(ViewHolder holder){
        holder.showMoreView.moreBTN.setTag(holder.data.referenceId);
        holder.showMoreView.moreBTN.setOnClickListener(this);

        holder.showMoreView.moreTXT.setVisibility(holder.data.showLoading ? View.GONE : View.VISIBLE);
        holder.showMoreView.morePB.setVisibility(holder.data.showLoading ? View.VISIBLE : View.GONE);
    }

    private void addressBinder(ViewHolder holder, int position){
        holder.view.setTag(position);
        holder.view.setOnClickListener(this);

        holder.addressView.iLikeIV.setOnClickListener(this);
        holder.addressView.avatarCIV.setOnClickListener(this);

        holder.addressView.declineTXT.setOnClickListener(this);
        holder.addressView.acceptTXT.setOnClickListener(this);

        holder.addressView.iLikeIV.setTag(R.id.iLikeIV, holder.data.addressItem);
        holder.addressView.avatarCIV.setTag(R.id.avatarCIV, holder.data.addressItem.viewer.data);
        holder.addressView.declineTXT.setTag(holder.data);
        holder.addressView.acceptTXT.setTag(holder.data);

        holder.addressView.descriptionTXT.setText(formatAddressDescription(holder, position));
        holder.addressView.descriptionTXT.setMovementMethod(LinkMovementMethod.getInstance());

        holder.addressView.dateTXT.setText(holder.data.addressItem.time_passed);
        Glide.with(context)
                .load(holder.data.addressItem.wishlist.data.image.data.full_path)
                .into(holder.addressView.iLikeIV);

        Glide.with(context)
                .load(holder.data.addressItem.viewer.data.getAvatar())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .dontTransform()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(holder.addressView.avatarCIV);
    }

    private void giftBinder(ViewHolder holder, int position){
        holder.giftView.iLikeIV.setOnClickListener(this);
        holder.giftView.avatarCIV.setOnClickListener(this);

        holder.giftView.declineTXT.setOnClickListener(this);
        holder.giftView.acceptTXT.setOnClickListener(this);

        holder.giftView.iLikeIV.setTag(R.id.iLikeIV, holder.data.giftItem);
        holder.giftView.avatarCIV.setTag(R.id.avatarCIV, holder.data.giftItem.sender.data);
        holder.giftView.declineTXT.setTag(holder.data);
        holder.giftView.acceptTXT.setTag(holder.data);

        holder.giftView.descriptionTXT.setText(formatGiftDescription(holder, position));
        holder.giftView.descriptionTXT.setMovementMethod(LinkMovementMethod.getInstance());

        holder.giftView.dateTXT.setText(holder.data.giftItem.time_passed);
        Glide.with(context)
                .load(holder.data.giftItem.image.data.full_path)
                .into(holder.giftView.iLikeIV);

        Glide.with(context)
                .load(holder.data.giftItem.sender.data.getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .dontAnimate()
                .dontTransform()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .error(R.drawable.placeholder_avatar))
                .into(holder.giftView.avatarCIV);
    }

    private void ongoingBinder(ViewHolder holder, int position){
        holder.ongoingView.iLikeIV.setOnClickListener(this);
        holder.ongoingView.avatarCIV.setOnClickListener(this);

        holder.ongoingView.acceptTXT.setOnClickListener(this);

        holder.ongoingView.iLikeIV.setTag(R.id.iLikeIV, holder.data.onGoingItem);
        holder.ongoingView.avatarCIV.setTag(R.id.avatarCIV, holder.data.onGoingItem.wishlist.data.owner.data);
        holder.ongoingView.acceptTXT.setTag(holder.data);

        holder.ongoingView.descriptionTXT.setText(formatOngoingDescription(holder, position));
        holder.ongoingView.descriptionTXT.setMovementMethod(LinkMovementMethod.getInstance());

        holder.ongoingView.dateTXT.setText(holder.data.onGoingItem.time_passed);
        Glide.with(context)
                .load(holder.data.onGoingItem.wishlist.data.image.data.full_path)
                .into(holder.ongoingView.iLikeIV);

        Glide.with(context)
                .load(holder.data.onGoingItem.wishlist.data.owner.data.getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .dontTransform()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(holder.ongoingView.avatarCIV);

    }

    public SpannableString formatAddressDescription(final ViewHolder holder, final int position){
        String extraMessage = " would like to view the details of your gift, ";
        holder.addressView.extraMessageTXT.setVisibility(View.GONE);
        String userName = holder.data.addressItem.viewer.data.name;

        String finalMessage = userName + extraMessage + holder.data.addressItem.wishlist.data.title;
        SpannableString spannableString = new SpannableString(finalMessage);

        ClickableSpan nameSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if(clickListener != null){
                    clickListener.onAvatarClick(data.get(position).addressItem.viewer.data);
                }
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_display_name)));
            }
        };
        spannableString.setSpan(nameSpan, 0, userName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan productSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if(clickListener != null){
                    clickListener.onItemClick(data.get(position).addressItem);
                }
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.colorPrimary));
            }
        };

        spannableString.setSpan(productSpan, userName.length() + extraMessage.length(), finalMessage.length(), Spanned.SPAN_INTERMEDIATE);

        return spannableString;
    }

    public SpannableString formatGiftDescription(final ViewHolder holder, final int position){
        String  extraMessage = " just sent you ";
        holder.giftView.extraMessageTXT.setVisibility(View.VISIBLE);
        holder.giftView.extraMessageTXT.setText("Have you received the actual item?");

        String userName = holder.data.giftItem.sender.data.name;

        String finalMessage = userName + extraMessage + holder.data.giftItem.title;
        SpannableString spannableString = new SpannableString(finalMessage);

        ClickableSpan nameSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onAvatarClick(data.get(position).giftItem.sender.data);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_display_name)));
            }
        };
        spannableString.setSpan(nameSpan, 0, userName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan productSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onItemClick(data.get(position).giftItem);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.colorPrimary));
            }
        };

        spannableString.setSpan(productSpan, userName.length() + extraMessage.length(), finalMessage.length(), Spanned.SPAN_INTERMEDIATE);

        return spannableString;
    }

    public SpannableString formatOngoingDescription(final ViewHolder holder, final int position){
        String extraMessage = "You are requesting permission to view the delivery information of ";
        holder.ongoingView.extraMessageTXT.setVisibility(View.GONE);
        String userName = holder.data.onGoingItem.wishlist.data.owner.data.name;

        String finalMessage = extraMessage + userName +  ", " + holder.data.onGoingItem.wishlist.data.title;
        SpannableString spannableString = new SpannableString(finalMessage);

        ClickableSpan nameSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onAvatarClick(data.get(position).onGoingItem.wishlist.data.owner.data);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_display_name)));
            }
        };
        spannableString.setSpan(nameSpan, 0, "You".length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan otherNameCS = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onAvatarClick(data.get(position).onGoingItem.wishlist.data.owner.data);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_display_name)));
            }
        };
        spannableString.setSpan(otherNameCS, extraMessage.length() , extraMessage.length() + userName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan wishListCS = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onItemClick(data.get(position).onGoingItem);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.colorPrimary));
            }
        };

        spannableString.setSpan(wishListCS, userName.length() + extraMessage.length() + ", ".length(), finalMessage.length(), Spanned.SPAN_INTERMEDIATE);

        return spannableString;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        int headerPosition = 0;
        do {
            if (this.isHeader(itemPosition)) {
                headerPosition = itemPosition;
                break;
            }
            itemPosition -= 1;
        } while (itemPosition >= 0);
        return headerPosition;
    }

    @Override
    public int getHeaderLayout(int headerPosition) {
        return R.layout.adapter_notification_header;
    }

    @Override
    public void bindHeaderData(View header, int headerPosition) {
        TextView headerTXT = header.findViewById(R.id.headerTXT);
        switch (data.get(headerPosition).referenceId){
            case Data.ADDRESS:
                headerTXT.setText("Delivery Information Requests");
                break;
            case Data.GIFT:
                headerTXT.setText("Pending Gifts");
                break;
            case Data.ONGOING:
                headerTXT.setText("Outgoing Requests");
                break;
        }
    }

    @Override
    public boolean isHeader(int itemPosition) {
        return data.get(itemPosition).viewType == Data.HEADER;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        Data data;
        View view;

        HeaderView headerView;
        ShowMoreView showMoreView;
        AddressView addressView;
        GiftView giftView;
        OngoingView ongoingView;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
        }

        public ViewHolder bindHeader(){
            headerView = new HeaderView();
            return this;
        }

        public ViewHolder bindShowMore(){
            showMoreView = new ShowMoreView();
            return this;
        }

        public ViewHolder bindAddress(){
            addressView = new AddressView();
            return this;
        }

        public ViewHolder bindGift(){
            giftView = new GiftView();
            return this;
        }

        public ViewHolder bindOnGoing(){
            ongoingView = new OngoingView();
            return this;
        }

        public class HeaderView{

            @BindView(R.id.headerTXT)           TextView headerTXT;
            @BindView(R.id.headerCON)           View headerCON;

            public HeaderView(){
                ButterKnife.bind(this, view);
            }
        }

        public class ShowMoreView{

            @BindView(R.id.moreBTN)             View moreBTN;
            @BindView(R.id.morePB)              View morePB;
            @BindView(R.id.moreTXT)             TextView moreTXT;

            public ShowMoreView(){
                ButterKnife.bind(this, view);
            }
        }

        public class AddressView{

            @BindView(R.id.iLikeIV)             ImageView iLikeIV;
            @BindView(R.id.declineTXT)          TextView declineTXT;
            @BindView(R.id.acceptTXT)           TextView acceptTXT;
            @BindView(R.id.descriptionTXT)      TextView descriptionTXT;
            @BindView(R.id.extraMessageTXT)     TextView extraMessageTXT;
            @BindView(R.id.dateTXT)             TextView dateTXT;
            @BindView(R.id.avatarCIV)           ImageView avatarCIV;

            public AddressView(){
                ButterKnife.bind(this, view);
            }
        }

        public class GiftView{

            @BindView(R.id.iLikeIV)             ImageView iLikeIV;
            @BindView(R.id.declineTXT)          TextView declineTXT;
            @BindView(R.id.acceptTXT)           TextView acceptTXT;
            @BindView(R.id.descriptionTXT)      TextView descriptionTXT;
            @BindView(R.id.extraMessageTXT)     TextView extraMessageTXT;
            @BindView(R.id.dateTXT)             TextView dateTXT;
            @BindView(R.id.avatarCIV)           ImageView avatarCIV;

            public GiftView(){
                ButterKnife.bind(this, view);
            }
        }

        public class OngoingView {

            @BindView(R.id.iLikeIV)             ImageView iLikeIV;
            @BindView(R.id.acceptTXT)           TextView acceptTXT;
            @BindView(R.id.descriptionTXT)      TextView descriptionTXT;
            @BindView(R.id.extraMessageTXT)     TextView extraMessageTXT;
            @BindView(R.id.dateTXT)             TextView dateTXT;
            @BindView(R.id.avatarCIV)           ImageView avatarCIV;

            public OngoingView(){
                ButterKnife.bind(this, view);
            }
        }
	}

	public class Data {
        public static final int HEADER = 1;
        public static final int SHOW_MORE = 2;

        public static final int ADDRESS = -1;
        public static final int GIFT = -2;
        public static final int ONGOING = -3;

        public int viewType;
        public int referenceId;
        public boolean showLoading = false;

        private WishListViewerItem addressItem;
        private WishListTransactionItem giftItem;
        private WishListViewerItem onGoingItem;

        public Data(int viewType, int referenceId){
            this.viewType = viewType;
            this.referenceId = referenceId;
        }

        public Data(int viewType, int referenceId, WishListViewerItem item){
            this.viewType = viewType;
            this.referenceId = referenceId;
            if(viewType == ADDRESS){
                this.addressItem = item;
            }else{
                this.onGoingItem = item;
            }
        }

        public Data(int viewType, int referenceId, WishListTransactionItem giftItem){
            this.viewType = viewType;
            this.referenceId = referenceId;
            this.giftItem = giftItem;
        }

    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            switch (v.getId()){
                case R.id.avatarCIV:
                    clickListener.onAvatarClick((UserItem) v.getTag(R.id.avatarCIV));
                    break;
                case R.id.acceptTXT:
                    onAcceptClicked(v);
                    break;
                case R.id.declineTXT:
                    onDeclineClicked(v);
                    break;
                case R.id.iLikeIV:
                    onWishClicked(v);
                    break;
                case R.id.moreBTN:
                    clickListener.showMore((int) v.getTag());
                    break;
            }
        }
    }

    private void onWishClicked(View v){
        if(v.getTag(R.id.iLikeIV) instanceof WishListViewerItem){
            clickListener.onItemClick((WishListViewerItem) v.getTag(R.id.iLikeIV));
        }else{
            clickListener.onItemClick((WishListTransactionItem) v.getTag(R.id.iLikeIV));
        }
    }

    private void onAcceptClicked(View v){
        Data acceptData = (Data) v.getTag();
        switch (acceptData.viewType){
            case Data.ADDRESS:
                clickListener.onGrantClick(acceptData.addressItem);
                break;
            case Data.GIFT:
                clickListener.onYesClick(acceptData.giftItem);
                break;
            case Data.ONGOING:
                clickListener.onCancelClick(acceptData.onGoingItem);
                break;
        }
    }

    private void onDeclineClicked(View v){
        Data acceptData = (Data) v.getTag();
        switch (acceptData.viewType){
            case Data.ADDRESS:
                clickListener.onDenyClick(acceptData.addressItem);
                break;
            case Data.GIFT:
                clickListener.onNoClick(acceptData.giftItem);
                break;
            case Data.ONGOING:
                break;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(WishListTransactionItem wishListTransactionItem);
        void onItemClick(WishListViewerItem thingsILikeItem);
        void onAvatarClick(UserItem userItem);
        void onGrantClick(WishListViewerItem thingsILikeItem);
        void onDenyClick(WishListViewerItem thingsILikeItem);
        void onCancelClick(WishListViewerItem thingsILikeItem);
        void onYesClick(WishListTransactionItem wishListTransactionItem);
        void onNoClick(WishListTransactionItem wishListTransactionItem);
        void showMore(int viewType);
    }

} 
