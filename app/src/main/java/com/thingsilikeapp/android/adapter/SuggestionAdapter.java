package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.dialog.UnfollowDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.social.FollowRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SuggestionAdapter extends BaseAdapter implements View.OnClickListener, UnfollowDialog.Callback {
	private Context context;
	private List<UserItem> data;
    private LayoutInflater layoutInflater;
    private int px;

    public SuggestionAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        EventBus.getDefault().register(this);
        px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, context.getResources().getDisplayMetrics());
	}

    public void setNewData(List<UserItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<UserItem> data){
        for(UserItem notificationItem :  data){
            this.data.add(notificationItem);
        }
        notifyDataSetChanged();
    }

    public void reset(){
        this.data = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void removeData(UserItem item){
        for(UserItem notificationItem : data){
            if(item.id == notificationItem.id){
                data.remove(notificationItem);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public List<UserItem> getData(){
        return data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_suggestion, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.userItem = data.get(position);
        holder.nameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.userItem.name, holder.userItem.is_verified, 12), TextView.BufferType.SPANNABLE);
//        holder.nameTXT.setText(holder.userItem.name);

        holder.commonNameTXT.setText(holder.userItem.common_name);

        holder.commandTXT.setTag(holder);
        holder.commandTXT.setOnClickListener(this);

        holder.imageCIV.setTag(R.id.imageCIV, holder);
        holder.imageCIV.setOnClickListener(this);

        holder.nameTXT.setTag(holder);
        holder.nameTXT.setOnClickListener(this);

        holder.commonNameTXT.setTag(holder);
        holder.commonNameTXT.setOnClickListener(this);

//        if(holder.userItem.is_loading_follow){
//            holder.loadingPB.setVisibility(View.VISIBLE);
//            holder.followCON.setVisibility(View.GONE);
//        }else{
//            holder.loadingPB.setVisibility(View.GONE);
//
//            if(UserData.getUserItem().id == holder.userItem.id){
//                holder.followCON.setVisibility(View.GONE);
//            }else{
//                holder.followCON.setVisibility(View.VISIBLE);
//            }
//        }
//        if (holder.userItem.social.data.is_following.equals("yes")) {
//            holder.followCON.setBackground(ActivityCompat.getDrawable(context, R.drawable.bg_follow));
//            holder.commandTXT.setTextColor(ActivityCompat.getColor(context, R.color.white));
//            holder.commandTXT.setText("FOLLOWING");
//            holder.addBTN.setVisibility(View.GONE);
//        } else {
//            holder.followCON.setBackground(ActivityCompat.getDrawable(context, R.drawable.bg_unfollow));
//            holder.commandTXT.setTextColor(ActivityCompat.getColor(context, R.color.colorPrimary));
//            holder.commandTXT.setText("FOLLOW");
//            holder.addBTN.setVisibility(View.VISIBLE);
//        }

        Glide.with(context)
                .load(holder.userItem.getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.imageCIV);

		return convertView;
	}

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        switch (v.getId()){
            case R.id.commandTXT:
                if(clickListener != null){
                    viewHolder.userItem.is_loading_follow = true;
                    notifyDataSetChanged();
                    clickListener.onCommandClick(viewHolder.userItem);
                }
                break;
            case R.id.imageCIV:
                ViewHolder viewHolder1 = (ViewHolder) v.getTag(R.id.imageCIV);
                ((RouteActivity)context).startProfileActivity(viewHolder1.userItem.id);
                if(clickListener != null){
                    clickListener.onUserClick(viewHolder1.userItem);
                }
                Keyboard.hideKeyboard(context);
                break;
            case R.id.nameTXT:
            case R.id.commonNameTXT:
                ((RouteActivity)context).startProfileActivity(viewHolder.userItem.id);
                if(clickListener != null){
                    clickListener.onUserClick(viewHolder.userItem);
                }
                Keyboard.hideKeyboard(context);
                break;
        }

    }

    public void followUser(int id){
        FollowRequest followRequest = new FollowRequest(context);
        followRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, id)
                .execute();
    }

    public void unFollowUser(UserItem userItem){
        UnfollowDialog.newInstance(userItem.id, userItem.name, this).show(((BaseActivity) context).getSupportFragmentManager(), UnfollowDialog.TAG);
    }

    @Subscribe
    public void onResponse(FollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
            for (UserItem userItem : data){
                if(userItem.id == userTransformer.userItem.id){
                    data.set(data.indexOf(userItem), userTransformer.userItem);
                    notifyDataSetChanged();
                    break;
                }
            }
            UserData.updateStatistic(1);
        }
    }

    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
            for (UserItem userItem : data){
                if(userItem.id == userTransformer.userItem.id){
                    data.set(data.indexOf(userItem), userTransformer.userItem);
                    notifyDataSetChanged();
                    break;
                }
            }

            UserData.updateStatistic(-1);
        }
    }

    @Override
    public void onAccept(int userID) {
        UnFollowRequest unFollowRequest = new UnFollowRequest(context);
        unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, userID)
                .execute();
    }

    @Override
    public void onCancel(int userID) {
        for (UserItem userItem : data){
            if(userItem.id == userID){
                userItem.is_loading_follow = false;
                data.set(data.indexOf(userItem), userItem);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public class ViewHolder{
        UserItem userItem;

        @BindView(R.id.nameTXT)         TextView nameTXT;
        @BindView(R.id.commonNameTXT)   TextView commonNameTXT;
        @BindView(R.id.imageCIV)        ImageView imageCIV;
        @BindView(R.id.commandTXT)      TextView commandTXT;
        @BindView(R.id.loadingPB)       View loadingPB;
        @BindView(R.id.addBTN)          View addBTN;
//        @BindView(R.id.followCON)       View followCON;

        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onCommandClick(UserItem userItem);
        void onUserClick(UserItem userItem);
    }
} 
