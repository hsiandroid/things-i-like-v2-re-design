package com.thingsilikeapp.android.adapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.dialog.AddressInfoDialog;
import com.thingsilikeapp.android.dialog.AddressPermissionDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.AddressBookItem;
import com.thingsilikeapp.data.model.WishListViewerItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.wishlist.WishListGrantPermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListRevokePermissionRequest;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoViewerTransformer;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestOnGoingAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<WishListViewerItem> data;
    private LayoutInflater layoutInflater;

    private static String include = "wishlist.image,wishlist.owner.info,viewer.info,info,image,sender.info";

	public RequestOnGoingAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        EventBus.getDefault().register(this);
	}

    public void setNewData(List<WishListViewerItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<WishListViewerItem> data){
        for(WishListViewerItem item : data){
            this.data.add(item);
        }
        notifyDataSetChanged();
    }

    public void removeData(WishListViewerItem item) {
        for (WishListViewerItem wishListViewerItem : data) {
            if (item.id == (wishListViewerItem.id)) {
                data.remove(wishListViewerItem);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void updateData(WishListViewerItem wishListViewerItem){
        for(WishListViewerItem item : data){
            if(wishListViewerItem.id == item.id){
                data.set(data.indexOf(item), wishListViewerItem);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void updateStatus(int id, String status, String address_label){
        for(WishListViewerItem item : data){
            if(id == item.id){
                item.status = status;
                item.address_label = address_label;
                data.set(data.indexOf(item), item);
                notifyDataSetChanged();
                break;
            }
        }
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_request_ongoing, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.wishListViewerItem = data.get(position);
        holder.nameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.wishListViewerItem.viewer.data.name, holder.wishListViewerItem.viewer.data.is_verified, 12));
        holder.commonNameTXT.setText(holder.wishListViewerItem.viewer.data.common_name);
        holder.addressLabelTXT.setText(holder.wishListViewerItem.address_label);
        holder.statusTXT.setText(StringFormatter.first(holder.wishListViewerItem.status));

        switch (holder.wishListViewerItem.status){
            case "pending":
                holder.statusCON.setBackground(ActivityCompat.getDrawable(context, R.drawable.bg_request_pending));
                holder.statusTXT.setTextColor(ActivityCompat.getColor(context, R.color.text_gray));
                break;
            case "granted":
                holder.statusCON.setBackground(ActivityCompat.getDrawable(context, R.drawable.bg_request_granted));
                holder.statusTXT.setTextColor(ActivityCompat.getColor(context, R.color.white));
                break;
        }

        holder.imageCIV.setTag(R.id.imageCIV, holder);
        holder.imageCIV.setOnClickListener(this);
        holder.nameTXT.setTag(holder);

        holder.otherOptionBTN.setTag(holder.wishListViewerItem);
        holder.otherOptionBTN.setOnClickListener(this);

        holder.addressLabelTXT.setTag(holder.wishListViewerItem.getAddressBook());
        holder.addressLabelTXT.setOnClickListener(this);

        holder.statusTXT.setTag(holder.wishListViewerItem);
        holder.statusTXT.setOnClickListener(this);

        holder.commonNameTXT.setTag(holder);

        Glide.with(context)
                .load(holder.wishListViewerItem.viewer.data.getAvatar())
                .apply(new RequestOptions()
                .dontAnimate()
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar))
                .into(holder.imageCIV);

		return convertView;
	}

    private void otherOption(final View v){
        final WishListViewerItem wishListViewerItem = (WishListViewerItem) v.getTag();
        PopupMenu popup = new PopupMenu(context, v);
        popup.getMenuInflater().inflate(R.menu.request_menu, popup.getMenu());

        switch (wishListViewerItem.status) {
            case "granted":
                popup.getMenu().getItem(0).setVisible(false);
                popup.getMenu().getItem(1).setTitle("Revoke");
                break;
            case "denied":
            case "not_requested":
            case "pending":
                popup.getMenu().getItem(0).setVisible(true);
                popup.getMenu().getItem(1).setTitle("Decline");
                break;
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.grantBTN:
                        attemptGrantRequest(wishListViewerItem.wishlist.data.id, wishListViewerItem.viewer.data.id);
                        break;
                    case R.id.revokeBTN:
                        attemptRevokeRequest(wishListViewerItem.wishlist.data.id, wishListViewerItem.viewer.data.id);
                        break;
                    case R.id.profileBTN:
                        ((RouteActivity)context).startProfileActivity(wishListViewerItem.viewer.data.id);
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    private void attemptGrantRequest(final int wishlistID, final int userID){
        AddressPermissionDialog.newInstance(new AddressPermissionDialog.AddressPermissionListener() {
            @Override
            public void positiveClicked(int addressID) {
//                Log.e("Trap", ">>>" + include);
//                Log.e("Trap", ">>>" + wishlistID);
//                Log.e("Trap", ">>>" + addressID);
//                Log.e("Trap", ">>>" + userID);
                new WishListGrantPermissionRequest(context)
                        .setProgressDialog(new ProgressDialog(context).show(context, "", "Loading...", false, false))
                        .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                        .addParameters(Keys.server.key.INCLUDE, include)
                        .addParameters(Keys.server.key.WISHLIST_ID, wishlistID)
                        .addParameters(Keys.server.key.ADDRESS_ID, addressID)
                        .addParameters(Keys.server.key.USER_ID, userID)
                        .execute();
            }
        }).show((((RouteActivity) context)).getSupportFragmentManager(), AddressPermissionDialog.TAG);
    }

    private void attemptRevokeRequest(final int wishlistID, final int userID){

        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Deny Information Request?")
                .setNote("You are denying this person from requesting your delivery information.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Confirm")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new WishListRevokePermissionRequest(context)
                                .setProgressDialog(new ProgressDialog(context).show(context, "", "Loading...", false, false))
                                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .addParameters(Keys.server.key.INCLUDE, include)
                                .addParameters(Keys.server.key.WISHLIST_ID, wishlistID)
                                .addParameters(Keys.server.key.USER_ID, userID)
                                .execute();
                        confirmationDialog.dismiss();
                        ((RouteActivity) context).facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.DENY_REQUEST);
                    }
                })
                .setNegativeButtonText("Cancel")
                .build((((RouteActivity) context)).getSupportFragmentManager());
    }

    @Subscribe
    public void onResponse(WishListGrantPermissionRequest.ServerResponse responseData) {
        WishListInfoViewerTransformer wishlistInfoTransformer = responseData.getData(WishListInfoViewerTransformer.class);
        if(wishlistInfoTransformer.status){
            Log.e("address Label", ">>>" + wishlistInfoTransformer.wishListViewerItem.address_label);
            updateStatus(wishlistInfoTransformer.wishListViewerItem.id, wishlistInfoTransformer.wishListViewerItem.status, wishlistInfoTransformer.wishListViewerItem.address_label);
            ToastMessage.show(context, wishlistInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(context, wishlistInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(WishListRevokePermissionRequest.ServerResponse responseData) {
        WishListInfoViewerTransformer wishlistInfoTransformer = responseData.getData(WishListInfoViewerTransformer.class);
        if(wishlistInfoTransformer.status){
            removeData(wishlistInfoTransformer.wishListViewerItem);
            ToastMessage.show(context, wishlistInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(context, wishlistInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.statusTXT:
            case R.id.otherOptionBTN:
                otherOption(v);
                break;
            case R.id.imageCIV:
                ViewHolder viewHolder1 = (ViewHolder) v.getTag(R.id.imageCIV);
                ((RouteActivity)context).startProfileActivity(viewHolder1.wishListViewerItem.viewer.data.id);
                break;
            case R.id.addressLabelTXT:
                AddressBookItem addressBookItem = ((AddressBookItem) v.getTag());
                if(addressBookItem.address_label != null){
                    AddressInfoDialog.newInstance(addressBookItem).show(((RouteActivity)context).getSupportFragmentManager(), AddressInfoDialog.TAG);
                }
                break;
            case R.id.nameTXT:
            case R.id.commonNameTXT:
                ViewHolder viewHolder = (ViewHolder) v.getTag();
                ((RouteActivity)context).startProfileActivity(viewHolder.wishListViewerItem.viewer.data.id);
                break;
        }
    }

    public class ViewHolder{
        WishListViewerItem wishListViewerItem;

        @BindView(R.id.nameTXT)         TextView nameTXT;
        @BindView(R.id.commonNameTXT)   TextView commonNameTXT;
        @BindView(R.id.addressLabelTXT) TextView addressLabelTXT;
        @BindView(R.id.statusTXT)       TextView statusTXT;
        @BindView(R.id.imageCIV)        ImageView imageCIV;
        @BindView(R.id.otherOptionBTN)  View otherOptionBTN;
        @BindView(R.id.statusCON)       View statusCON;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}
} 
