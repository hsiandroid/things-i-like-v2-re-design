package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.CountryData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryAdapter extends BaseAdapter implements View.OnClickListener, Filterable {
	private Context context;
	private List<CountryData.Country> data;
    private LayoutInflater layoutInflater;

    private List<CountryData.Country> originalData;

    private ValueFilter valueFilter;
    private String searchText = "";

	public CountryAdapter(Context context) {
		this.context = context;
		this.data = CountryData.getCountryData();
        layoutInflater = LayoutInflater.from(context);
	}

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_country, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.view.setOnClickListener(this);
        holder.country = data.get(position);
        holder.countyNameTXT.setText(holder.country.country);
        holder.countyCodeTXT.setText(holder.country.code3);

        Glide.with(context)
                .load(holder.country.getFlag())
                .into(holder.countryFlagIV);

		return convertView;
	}

    @Override
    public void onClick(View v) {
        CountryAdapter.ViewHolder viewHolder = (CountryAdapter.ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onItemClick(viewHolder.country);
        }
    }

    public void highlightSearchText(TextView textView, String full, String search){
        if(search == null){
            highlightSearchTextDefault(textView, full);
            return;
        }

        String fullTXT = full.toLowerCase();
        String searchTXT = search.toLowerCase();

        if(fullTXT.contains(searchTXT)){
            int startPos = fullTXT.indexOf(searchTXT);
            int endPos = startPos + searchTXT.length();

            Spannable spannable = new SpannableString(full);
            spannable.setSpan(new BackgroundColorSpan(ActivityCompat.getColor(context, R.color.colorAccent)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(context, R.color.white)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setText(spannable);
        }else{
            highlightSearchTextDefault(textView, full);
        }

    }

    public void highlightSearchTextDefault(TextView textView, String fullTxt){
        textView.setText(fullTxt);
    }

    public class ViewHolder{
        CountryData.Country country;

        @BindView(R.id.countyNameTXT)   TextView countyNameTXT;
        @BindView(R.id.countyCodeTXT)   TextView countyCodeTXT;
        @BindView(R.id.countryFlagIV)   ImageView countryFlagIV;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(CountryData.Country counnty);
    }


    @Override
    public Filter getFilter() {
        if(valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }


    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (originalData == null){
                originalData = data;
            }

            FilterResults results = new FilterResults();
            if(constraint == null && constraint.length() == 0){
                results.count = originalData.size();
                results.values = originalData;
            }else{
                ArrayList<CountryData.Country> filterList = new ArrayList<>();
                for(int i=0; i < originalData.size();i++){
                    CountryData.Country categoryItem = originalData.get(i);
                    if((categoryItem.country.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(categoryItem);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data = (List<CountryData.Country>) results.values;
            searchText = constraint.toString();
            notifyDataSetChanged();
        }
    }
} 
