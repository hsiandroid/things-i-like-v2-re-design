package com.thingsilikeapp.android.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.thingsilikeapp.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class EventItemAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<String> data;
    private LayoutInflater layoutInflater;

    public enum Type {
        TODAY,
        COMING_SOON
    }

	public EventItemAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<String> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_template, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        onBindViewHolder(holder, position);
		return convertView;
	}

    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.string = data.get(position);
        holder.view.setTag(position);
        holder.view.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onTemplateItemClickListener(viewHolder.string);
        }
    }


    public class ViewHolder{
        String string;
//        @BindView(R.id.emotionTXT) TextView emotionTXT;
//        @BindView(R.id.genderTXT) TextView genderTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onTemplateItemClickListener(String string);
    }
} 
