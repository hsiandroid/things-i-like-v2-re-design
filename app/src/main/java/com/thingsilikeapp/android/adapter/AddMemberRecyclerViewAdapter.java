package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddMemberRecyclerViewAdapter extends RecyclerView.Adapter<AddMemberRecyclerViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<UserItem> data;
    private LayoutInflater layoutInflater;

	public AddMemberRecyclerViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void reset(){
        this.data = new ArrayList<>();
        notifyDataSetChanged();
    }

    public String getSelectedItems(){
        String total = "";
        for (UserItem userModel : data){
            if (userModel.isChecked){
                total = total + userModel.id + ",";
            }
        }
        return total;
    }


    public int getSelectedItemSize(){
        int total = 0;
        for (UserItem userModel : data){
            if (userModel.isChecked){
                total = total + userModel.id;
            }
        }
        return total;
    }

    public void setSelected(int position){
        UserItem userModel = data.get(position);
        userModel.isChecked = !userModel.isChecked;
        notifyItemChanged(position);
    }

    public void setNewData(List<UserItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<UserItem> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(UserItem item :  data){
            if(isDataUnique(item)){
                this.data.add(item);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    private boolean isDataUnique(UserItem newData){
        for(UserItem oldData : this.data){
            if(oldData.id == newData.id){
                return false;
            }
        }
        return true;
    }

    private boolean isChecked;

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_add_member, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.userItem = data.get(position);
        holder.view.setTag(holder.userItem);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);
        holder.adapterCON.setTag(holder.userItem);
        holder.adapterCON.setOnClickListener(this);
        holder.checkBox.setOnClickListener(this);
        holder.positionTXT.setText(holder.userItem.common_name);
        holder.checkBox.setTag(position);
        holder.checkBox.setChecked(holder.userItem.isChecked);
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });

        Glide.with(context)
                .load(holder.userItem.getAvatar())
                .apply(new RequestOptions()
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true))
                .into(holder.profileCIV);

        holder.nameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.userItem.name, holder.userItem.is_verified, 12));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        UserItem userItem;

        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.positionTXT)         TextView positionTXT;
        @BindView(R.id.profileCIV)          ImageView profileCIV;
        @BindView(R.id.onlineBTN)           View onlineBTN;
        @BindView(R.id.checkbox)            CheckBox checkBox;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onMenteeClick((UserItem) v.getTag());
                }
                break;
            case R.id.checkbox:
                if (clickListener != null){
                    clickListener.onChecked((int) v.getTag());
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
//        if(clickListener != null){
//            clickListener.onItemLongClick(data.get((int)v.getTag()));
//        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onMenteeClick(UserItem userModel);
        void onChecked(int position);
    }
}