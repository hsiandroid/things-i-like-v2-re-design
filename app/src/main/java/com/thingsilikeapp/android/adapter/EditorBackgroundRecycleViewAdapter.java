package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.BackgroundItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditorBackgroundRecycleViewAdapter extends RecyclerView.Adapter<EditorBackgroundRecycleViewAdapter.ViewHolder>  implements View.OnClickListener {
	private Context context;
	private List<BackgroundItem> data;
    private LayoutInflater layoutInflater;

	public EditorBackgroundRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<BackgroundItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_editor_frame, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.backgroundItem = data.get(position);

        holder.view.setTag(holder.backgroundItem);
        holder.view.setOnClickListener(this);

        Glide.with(context)
                .load(holder.backgroundItem.getImage())
                .apply(new RequestOptions()
                .dontAnimate())
                .into(holder.frameIMG);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        BackgroundItem backgroundItem;

        @BindView(R.id.frameIMG)    ImageView frameIMG;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onBackgroundSelected((BackgroundItem) v.getTag());
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onBackgroundSelected(BackgroundItem backgroundItem);
    }
} 
