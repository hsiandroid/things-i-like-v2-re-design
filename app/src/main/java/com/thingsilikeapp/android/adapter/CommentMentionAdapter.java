package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.hendraanggrian.widget.FilteredAdapter;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.CommentItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by BCTI 3 on 8/23/2017.
 */

public class CommentMentionAdapter extends FilteredAdapter<UserItem> implements View.OnClickListener {
    private LayoutInflater layoutInflater;
    private Context context;
    private List<UserItem> data;

    public CommentMentionAdapter(Context context) {
        super(context, 0, 0);
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public void add(UserItem item) {
        if(item.id == UserData.getUserId()){
            return;
        }

        for(UserItem userItem : data){
            if(userItem.id == item.id){
                return;
            }
        }
        data.add(item);
        super.add(item);
    }

    public void addNewData(List<CommentItem> data){
        for(CommentItem commentItem : data){
            add(commentItem.author.data);
        }
    }

    public void addNewUserData(List<UserItem> data){
        for(UserItem userItem : data){
            add(userItem);
        }
    }

    public void setNewUserData(List<UserItem> data){
        for(UserItem userItem : data){
            add(userItem);
        }
    }

    public void setNewData(List<CommentItem> data){
        clear();
        for(CommentItem commentItem : data){
            add(commentItem.author.data);
        }
    }

    public UserItem getUserByUserName(String username){
        for(UserItem userItem : data){
            if(userItem.username.equals(username)){
                return userItem;
            }
        }
        return new UserItem();
    }

    public UserItem getUserByID(int id){
        for(UserItem userItem : data){
            if(userItem.id == id){
                return userItem;
            }
        }
        return new UserItem();
    }

    @Override
    public void clear() {
        data.clear();
        super.clear();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_suggestion, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        else
            {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.userItem = getItem(position);
        holder.nameTXT.setText(VerifiedUserBadge.getFormattedName(context, holder.userItem.name, holder.userItem.is_verified, 12), TextView.BufferType.SPANNABLE);
        holder.commonNameTXT.setText(holder.userItem.common_name);

        holder.commandTXT.setVisibility(View.GONE);
        holder.loadingPB.setVisibility(View.GONE);

        holder.imageCIV.setTag(R.id.imageCIV, holder);
        holder.imageCIV.setOnClickListener(this);

        holder.nameTXT.setTag(holder);
        holder.nameTXT.setOnClickListener(this);

        holder.commonNameTXT.setTag(holder);
        holder.commonNameTXT.setOnClickListener(this);

        Glide.with(context)
                .load(holder.userItem.getAvatar())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontTransform()
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .skipMemoryCache(true))
                .into(holder.imageCIV);

		return convertView;
    }

    private Filter filter = new SocialFilter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((UserItem) resultValue).name;
        }
    };

    public Filter getFilter() {
        return filter;
    }

    @Override
    public void onClick(View view) {

    }

    public class ViewHolder{
        UserItem userItem;
        @BindView(R.id.nameTXT)         TextView nameTXT;
        @BindView(R.id.commonNameTXT)   TextView commonNameTXT;
        @BindView(R.id.imageCIV)        ImageView imageCIV;
        @BindView(R.id.commandTXT)      TextView commandTXT;
        @BindView(R.id.loadingPB)       View loadingPB;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

}
