package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;

import org.lucasr.twowayview.TwoWayLayoutManager;
import org.lucasr.twowayview.widget.SpannableGridLayoutManager;
import org.lucasr.twowayview.widget.TwoWayView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by BCTI 3 on 12/1/2017.
 */

public class FeedCategoryAdapter extends RecyclerView.Adapter<FeedCategoryAdapter.SimpleViewHolder> {
    private static final int COUNT = 50;

    private Context context;
    private TwoWayView mRecyclerView;
    private List<WishListItem> data;
    private int userID = 0;

    public FeedCategoryAdapter(Context context, TwoWayView recyclerView) {
        this.context = context;
        data = new ArrayList<>();
        mRecyclerView = recyclerView;
        userID = UserData.getUserId();
    }


    public void setNewData(List<WishListItem> data) {
        this.data.clear();
        for (WishListItem wishListItem : data) {
            this.data.add(wishListItem.minimal(userID));
        }
        notifyDataSetChanged();
    }


    public void addNewData(List<WishListItem> data) {
        int initialCount = this.data.size();
        int newCount = 0;
        for (WishListItem wishListItem : data) {
            this.data.add(wishListItem.minimal(userID));
            newCount++;
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.adapter_i_like_category_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {

        boolean isVertical = (mRecyclerView.getOrientation() == TwoWayLayoutManager.Orientation.VERTICAL);
        final View itemView = holder.itemView;

        final WishListItem wishListItem  = data.get(position);


        Glide.with(context)
                .load(wishListItem.wishListMin.like_image)
                .apply(new RequestOptions()
                .fitCenter()
                .placeholder(R.color.light_gray)
                .error(R.color.light_gray)
                .dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.iLikeIV);

        final SpannableGridLayoutManager.LayoutParams lp = (SpannableGridLayoutManager.LayoutParams) itemView.getLayoutParams();

//        final int span1 = (itemId == 0 || itemId == 3 ? 2 : 1);
//        final int span2 = (itemId == 0 ? 2 : (itemId == 3 ? 3 : 1));

//        final int colSpan = (isVertical ? span2 : span1);
//        final int rowSpan = (isVertical ? span1 : span2);
//
//        final int span1 = 2;
//        final int span2 = 1;



        int colSpan = 1;
        int rowSpan = 2;

//        if(position == 0 ){
//            colSpan = 2;
//            rowSpan = 1;
//        }
//
//        if(position == 1 ){
//            colSpan = 1;
//            rowSpan = 1;
//        }
//
//        if(position == 2 ){
//            colSpan = 1;
//            rowSpan = 1;
//        }
//
//        if(position == 3 ){
//            colSpan = 1;
//            rowSpan = 2;
//        }


        if (lp.rowSpan != rowSpan || lp.colSpan != colSpan) {
            lp.rowSpan = rowSpan;
            lp.colSpan = colSpan;
            itemView.setLayoutParams(lp);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iLikeIV)     ImageView iLikeIV;

        public SimpleViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}