package com.thingsilikeapp.android.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListViewerItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class RequestRecycleViewAdapter extends RecyclerView.Adapter<RequestRecycleViewAdapter.ViewHolder>  implements View.OnClickListener{
	private Context context;
	private List<WishListViewerItem> data;
    private LayoutInflater layoutInflater;

	public RequestRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<WishListViewerItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public WishListViewerItem getItem(int position){
        return data.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_request, parent, false));
    }

    public Object getItemData(int position){
        return data.get(position);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.wishListViewerItem = data.get(position);

        holder.view.setTag(position);
        holder.iLikeIV.setOnClickListener(this);
        holder.avatarCIV.setOnClickListener(this);

        holder.declineTXT.setOnClickListener(this);
        holder.acceptTXT.setOnClickListener(this);

        holder.iLikeIV.setTag(position);
        holder.avatarCIV.setTag(holder.wishListViewerItem.other_user.data);
        holder.declineTXT.setTag(holder.wishListViewerItem);
        holder.acceptTXT.setTag(holder.wishListViewerItem);

        holder.descriptionTXT.setText(formatDescription(holder, position));
        holder.descriptionTXT.setMovementMethod(LinkMovementMethod.getInstance());

        holder.dateTXT.setText(holder.wishListViewerItem.time_passed);
        Glide.with(context)
                .load(holder.wishListViewerItem.wishlist.data.image.data.full_path)
                .into(holder.iLikeIV);

        Glide.with(context)
                .load(holder.wishListViewerItem.other_user.data.getAvatar())
                .apply(new RequestOptions()
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar))
                .into(holder.avatarCIV);
    }

    public SpannableString formatDescription(final ViewHolder holder, final int position){
        Drawable d = ActivityCompat.getDrawable(context, R.drawable.icon_gift_small);
        String extraMessage = "";

        switch (holder.wishListViewerItem.type){
            case "wishlist_viewer":
                holder.indicatorIV.setImageDrawable(ActivityCompat.getDrawable(context, R.drawable.icon_gift_delivery_small));
                d = ActivityCompat.getDrawable(context, R.drawable.icon_gift_delivery_small);
                holder.declineTXT.setText("Deny");
                holder.acceptTXT.setText("Grant");
                extraMessage = " would like to view the details of your gift, ";
                holder.extraMessageTXT.setVisibility(View.GONE);
                break;
            case "wishlist_transaction":
                holder.indicatorIV.setImageDrawable(ActivityCompat.getDrawable(context, R.drawable.icon_gift_small));
                d = ActivityCompat.getDrawable(context, R.drawable.icon_gift_small);
                holder.declineTXT.setText("No");
                holder.acceptTXT.setText("Yes");
                extraMessage = " just sent you an ";
                holder.extraMessageTXT.setVisibility(View.VISIBLE);
                holder.extraMessageTXT.setText("Have you received it?");
                break;
        }

        String userName = holder.wishListViewerItem.other_user.data.name;

        String finalMessage = userName + extraMessage + holder.wishListViewerItem.wishlist.data.title;
        SpannableString spannableString = new SpannableString(finalMessage);

//        d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
//        spannableString.setSpan(new ImageSpan(d, ImageSpan.ALIGN_BOTTOM), 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

        ClickableSpan nameSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onAvatarClick(data.get(position).other_user.data);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setTypeface(TypefaceUtils.load(context.getAssets(), context.getString(R.string.typeface_display_name)));
            }
        };
        spannableString.setSpan(nameSpan, 0, userName.length(), Spanned.SPAN_INTERMEDIATE);

        ClickableSpan productSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                clickListener.onItemClick(position, textView);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ActivityCompat.getColor(context, R.color.colorPrimary));
            }
        };

        spannableString.setSpan(productSpan, userName.length() + extraMessage.length(), finalMessage.length(), Spanned.SPAN_INTERMEDIATE);

        return spannableString;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        WishListViewerItem wishListViewerItem;

        @BindView(R.id.iLikeIV)             ImageView iLikeIV;
        @BindView(R.id.declineTXT)          TextView declineTXT;
        @BindView(R.id.acceptTXT)           TextView acceptTXT;
        @BindView(R.id.descriptionTXT)      TextView descriptionTXT;
        @BindView(R.id.extraMessageTXT)     TextView extraMessageTXT;
        @BindView(R.id.dateTXT)             TextView dateTXT;
        @BindView(R.id.avatarCIV)           ImageView avatarCIV;
        @BindView(R.id.indicatorIV)         ImageView indicatorIV;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {

        if(clickListener != null){

            switch (v.getId()){
                case R.id.avatarCIV:
                    clickListener.onAvatarClick((UserItem) v.getTag());
                    break;
                case R.id.acceptTXT:
                    clickListener.onAcceptClick((WishListViewerItem) v.getTag());
                    break;
                case R.id.declineTXT:
                    clickListener.onDeclinedClick((WishListViewerItem) v.getTag());
                    break;
                case R.id.iLikeIV:
                    clickListener.onItemClick((int) v.getTag(), v);
                    break;
            }
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onAvatarClick(UserItem userItem);
        void onAcceptClick(WishListViewerItem thingsILikeItem);
        void onDeclinedClick(WishListViewerItem thingsILikeItem);
    }
} 
