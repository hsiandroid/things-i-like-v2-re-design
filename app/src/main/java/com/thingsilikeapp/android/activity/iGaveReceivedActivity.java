package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.profile.IGaveFragment;
import com.thingsilikeapp.android.fragment.profile.IReceivedFragment;
import com.thingsilikeapp.android.fragment.profile.ScanToFollowFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;
import icepick.State;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class iGaveReceivedActivity extends RouteActivity implements View.OnClickListener {


    @BindView(R.id.mainTitleTXT)                TextView mainTitleTXT;
    @BindView(R.id.BackButton)                  ImageView BackButton;


    @State int ids;


    @Override
    public int onLayoutSet() {
        return R.layout.activity_i_gave_received;
    }

    @Override
    public void onViewReady() {
        BackButton.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "Gave":
                openIGaveFragment(getFragmentBundle().getInt("id"));
                break;

            case "Received":
                openIReceivedFragment(getFragmentBundle().getInt("id"));
                break;

            case "scan":
                openScanFragnment();
                break;

        }
    }


    public void openIGaveFragment(int id){
        switchFragment(IGaveFragment.newInstance(id));
    }
    public void openIReceivedFragment(int id){
        switchFragment(IReceivedFragment.newInstance(id));
    }
    public void openScanFragnment(){
        switchFragment(ScanToFollowFragment.newInstance());
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BackButton:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
