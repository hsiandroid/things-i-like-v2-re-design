package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.settings.AboutFragment;
import com.thingsilikeapp.android.fragment.settings.BlockedUsersFragment;
import com.thingsilikeapp.android.fragment.settings.ChangePasswordFragment;
import com.thingsilikeapp.android.fragment.settings.EditContactNumberFragment;
import com.thingsilikeapp.android.fragment.settings.EditEmailFragment;
import com.thingsilikeapp.android.fragment.settings.EditProfileFragment;
import com.thingsilikeapp.android.fragment.settings.HiddenPostFragment;
import com.thingsilikeapp.android.fragment.settings.NotificationFragment;
import com.thingsilikeapp.android.fragment.settings.OTPVerificationEditContactNumberFragment;
import com.thingsilikeapp.android.fragment.settings.OthersFragment;
import com.thingsilikeapp.android.fragment.settings.ReportedPostsFragment;
import com.thingsilikeapp.android.fragment.settings.ReportedUsersFragment;
import com.thingsilikeapp.android.fragment.settings.SettingsFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class SettingsActivity extends RouteActivity implements View.OnClickListener {

    @BindView(R.id.mainTitleTXT)        TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)    ImageView mainBackButtonIV;
    @BindView(R.id.feedsBTN)            View feedsBTN;


    @Override
    public int onLayoutSet() {
        return R.layout.activity_settings;
    }

    @Override
    public void onViewReady() {
        mainBackButtonIV.setOnClickListener(this);
        feedsBTN.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "edit":
                openEditProfileFragment();
                break;
            case "password":
                openChangePasswordFragment();
                break;
            case "others":
                openOtherFragment();
                break;
            case "about":
                openAboutFragment();
                break;
            case "settings":
                openSettingsFragment();
                break;
            case "notification":
                openNotificationFragment();
                break;
            case "block":
                openBlockedUsersFragment();
                break;
            case "hide":
                openHiddenPostFragment();
                break;
            case "reportUser":
                openReportedUsersFragment();
                break;
            case "reportPost":
                openReportedPostFragment();
                break;
            case "editEmail":
                openEditEmailFragment();
                break;

        }
    }

    public void openChangePasswordFragment(){
        switchFragment(ChangePasswordFragment.newInstance());
    }

    public void openNotificationFragment(){
        switchFragment(NotificationFragment.newInstance());
    }

    public void openEditProfileFragment(){
        switchFragment(EditProfileFragment.newInstance());
    }
    public void openAboutFragment(){
        switchFragment(AboutFragment.newInstance());
    }
    public void openOtherFragment(){
        switchFragment(OthersFragment.newInstance());
    }
    public void openSettingsFragment(){
        switchFragment(SettingsFragment.newInstance());
    }
    public void openBlockedUsersFragment(){
        switchFragment(BlockedUsersFragment.newInstance());
    }
    public void openHiddenPostFragment(){
        switchFragment(HiddenPostFragment.newInstance());
    }
    public void openReportedUsersFragment(){
        switchFragment(ReportedUsersFragment.newInstance());
    }
    public void openReportedPostFragment(){
        switchFragment(ReportedPostsFragment.newInstance());
    }
    public void openEditEmailFragment(){switchFragment(EditEmailFragment.newInstance());}
    public void openOTPChangeNumber(String country, String contact, String contry_code){switchFragment(OTPVerificationEditContactNumberFragment.newInstance(country,contact,contry_code));}
    public void openEditContactNumberFragment(String country, String contact, String contry_code){switchFragment(EditContactNumberFragment.newInstance(country,contact,contry_code));}


    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;
            case R.id.feedsBTN:
                startMainActivity("home");
                break;
        }
    }
}
