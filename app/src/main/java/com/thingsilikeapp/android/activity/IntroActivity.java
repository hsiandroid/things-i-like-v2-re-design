package com.thingsilikeapp.android.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.intro.FifthFragment;
import com.thingsilikeapp.android.fragment.intro.FirstFragment;
import com.thingsilikeapp.android.fragment.intro.FourthFragment;
import com.thingsilikeapp.android.fragment.intro.SecondFragment;
import com.thingsilikeapp.android.fragment.intro.ThirdFragment;

public class IntroActivity extends AppIntro {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Note here that we DO NOT use setContentView();

        // Add your slide fragments here.
        // AppIntro will automatically generate the dots indicator and buttons.
        addSlide(FirstFragment.newInstance());
        addSlide(SecondFragment.newInstance());
        addSlide(ThirdFragment.newInstance());
        addSlide(FourthFragment.newInstance());
        addSlide(FifthFragment.newInstance());

        // Instead of fragments, you can also use our default slide.
        // Just create a `SliderPage` and provide title, description, background and image.
        // AppIntro will do the rest.
//        SliderPage sliderPage = new SliderPage();
//        sliderPage.setTitle(title);
//        sliderPage.setDescription(description);
//        sliderPage.setImageDrawable(image);
//        sliderPage.setBgColor(backgroundColor);
//        addSlide(AppIntroFragment.newInstance(sliderPage));

        // OPTIONAL METHODS
        // Override bar/separator color.
        setFlowAnimation();
        setBarColor(Color.parseColor("#00000000"));
        setSeparatorColor(Color.parseColor("#00000000"));

        setNavBarColor(R.color.transparent);

        // Hide Skip/Done button.
        showSkipButton(true);
        showDoneButton(true);
        setProgressButtonEnabled(true);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permission in Manifest.
        setVibrate(true);
        setVibrateIntensity(30);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        finish();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }
}