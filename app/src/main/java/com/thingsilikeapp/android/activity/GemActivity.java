package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.gem.BuyGemFragment;
import com.thingsilikeapp.android.fragment.gem.GemScannerFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class GemActivity extends RouteActivity implements View.OnClickListener  {
    public static final String TAG = GemActivity.class.getName();

    @BindView(R.id.mainTitleTXT)                TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)            ImageView mainBackButtonIV;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_gem;
    }

    @Override
    public void onViewReady() {
        mainBackButtonIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "buy":
                openBuyGemFragment();
                break;
            case "scanner":
                openGemScannerFragment();
                break;
        }
    }

    public void openBuyGemFragment() {
        switchFragment(BuyGemFragment.newInstance());
    }

    public void openGemScannerFragment() {
        switchFragment(GemScannerFragment.newInstance());
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;
        }
    }

}
