package com.thingsilikeapp.android.activity;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;


import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.create_post.CreatePostFragment;
import com.thingsilikeapp.android.fragment.create_post.EditGalleryListPostFragment;
import com.thingsilikeapp.android.fragment.create_post.EditGalleryPostFragment;
import com.thingsilikeapp.android.fragment.create_post.EditGalleryPostSingleFragment;
import com.thingsilikeapp.android.fragment.create_post.EditPostFragment;
import com.thingsilikeapp.android.fragment.create_post.EditVideoVideoFragment;
import com.thingsilikeapp.android.fragment.create_post.SendPostFragmentFragment;
import com.thingsilikeapp.android.fragment.create_post.SendPostLinkFragment;
import com.thingsilikeapp.android.fragment.create_post.SendPostListFragment;
import com.thingsilikeapp.android.fragment.create_post.SendPostVideoFragment;
import com.thingsilikeapp.data.model.BitmapItem;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

import java.io.File;
import java.util.List;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class CreatePostActivity extends RouteActivity {
    public static final String TAG = CreatePostActivity.class.getName();
    private Fragment currentFragment;
    @Override

    public int onLayoutSet() {
        return R.layout.activity_create_post;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "gallery":
                openGalleryFragment();
                break;
        }
    }


    public void openGalleryFragment() {
        switchFragment(CreatePostFragment.newInstance(0));
    }

    public void openEditPost(Bitmap file) { switchFragment(EditPostFragment.newInstance(file)); }

    public void openSendPostLink(String url) { switchFragment(SendPostLinkFragment.newInstance(url)); }

    public void openSendPost(File file) { switchFragment(SendPostFragmentFragment.newInstance(file)); }

    public void openSendPostList(Bitmap file, List<BitmapItem> galleryItems, int size) { switchFragment(SendPostListFragment.newInstance(file, galleryItems, size)); }

    public void openSendVideoPost(File file, File bitmap) { switchFragment(SendPostVideoFragment.newInstance(file, bitmap)); }

    public void openVideoPost(String file) { switchFragment(EditVideoVideoFragment.newInstance(file)); }

    public void openEditGalleryPost(Bitmap file) { switchFragment(EditGalleryPostFragment.newInstance(file)); }

    public void openEditGalleryPostSingle(BitmapItem bitmapItem, EditGalleryPostSingleFragment.FilteredCalback filteredCalback) { switchFragment(EditGalleryPostSingleFragment.newInstance(bitmapItem, filteredCalback)); }

    public void openEditGalleryPost(List<BitmapItem> galleryItems) { switchFragment(EditGalleryListPostFragment.newInstance(galleryItems)); }


    @Override
    public void onBackPressed() {
        int backStackCount = getBackStackCount();

        if (backStackCount > 1) {
            int index = backStackCount - 2;
            FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
            String tag = backEntry.getName();
            currentFragment = getSupportFragmentManager().findFragmentByTag(tag);
        }

        if (backStackCount <= 1) {
            if (isTaskRoot()) {
                exitConfirmation();
            } else {
                finish();
            }
        } else {
            super.onBackPressed();
        }
    }
}
