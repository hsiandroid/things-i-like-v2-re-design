package com.thingsilikeapp.android.activity;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.android.fragment.TemplateFragment;

import icepick.State;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class TemplateActivity extends RouteActivity {
    public static final String TAG = TemplateActivity.class.getName();

    @State
    public String sampleStr;

    @Override

    public int onLayoutSet() {
        return R.layout.activity_template;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        openTemplateFragment();
    }

    public void openTemplateFragment() {
        switchFragment(TemplateFragment.newInstance());
    }

}
