package com.thingsilikeapp.android.activity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.account.AccountEditProfileFragment;
import com.thingsilikeapp.android.fragment.account.AccountSearchUserFragment;
import com.thingsilikeapp.android.fragment.account.ShareAppFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import icepick.State;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class AccountActivityBak extends RouteActivity implements View.OnClickListener {
    public static final String TAG = AccountActivityBak.class.getName();
    public static final int CODE = 545;

    @BindView(R.id.mainTitleTXT)        TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)    View mainBackButtonIV;

    @BindView(R.id.titleCON)            View titleCON;
    @BindView(R.id.searchCON)           View searchCON;
    @BindView(R.id.searchBackBTN)       View searchBackBTN;
    @BindView(R.id.searchET)            EditText searchET;
    @BindView(R.id.clearBTN)            ImageView clearBTN;

    private Timer timer;

    @State boolean enableBack = true;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_account_bak;
    }

    @Override
    public void onViewReady() {
        mainBackButtonIV.setOnClickListener(this);
        searchBackBTN.setOnClickListener(this);
        clearBTN.setOnClickListener(this);
        initSearch();
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "suggestion":
                openAccountSuggestionFragment();
                break;
            case "profile":
                openAccountEditProfileFragment();
                break;
        }
    }

    public void openAccountSuggestionFragment(){
        switchFragment(AccountSearchUserFragment.newInstance());
    }

    public void openAccountEditProfileFragment(){
        switchFragment(AccountEditProfileFragment.newInstance());
    }

    public void openShareAppFragment(){
        switchFragment(ShareAppFragment.newInstance());
    }

    public void showSearchContainer(boolean b){
        if(b){
            titleCON.setVisibility(View.GONE);
            searchCON.setVisibility(View.VISIBLE);
        }else{
            titleCON.setVisibility(View.VISIBLE);
            searchCON.setVisibility(View.GONE);
        }
    }

    public void enableBack(boolean enableBack) {
        mainBackButtonIV.setVisibility(enableBack ? View.VISIBLE : View.GONE);
        searchBackBTN.setVisibility(enableBack ? View.VISIBLE : View.GONE);
        this.enableBack = enableBack;
    }
    public String getSearchET(){
        return searchET.getText().toString();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                onBackPressed();
                break;
            case R.id.searchBackBTN:
                onBackPressed();
                break;
            case R.id.clearBTN:
                searchET.setText("");
                break;
        }
    }

    public EditText getSearchEditText(){
        return searchET;
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (timer != null) {
            timer.cancel();
        }
    }

    public void initSearch(){
        if(searchET != null){
            searchET.setHint("Search people...");
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (timer != null) {
                        timer.cancel();
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if(searchListener != null){
                                if(searchET != null){
                                    searchListener.searchTextChange(searchET.getText().toString());
                                }
                            }
                        }
                    }, 600);
                }
            });
        }
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onBackPressed() {
        if(enableBack){
            super.onBackPressed();
        }
    }

    public void setSearchListener(SearchListener searchListener){
        this.searchListener = searchListener;
    }

    private SearchListener searchListener;

    public interface SearchListener{
        void searchTextChange(String keyword);
    }
}
