package com.thingsilikeapp.android.activity;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.landing.LoginFragment;
import com.thingsilikeapp.android.fragment.landing.LoginTILFragment;
import com.thingsilikeapp.android.fragment.landing.SplashFragment;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.firebase.MyFirebaseMessagingService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import icepick.State;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class  LandingActivity extends RouteActivity implements FacebookCallback<LoginResult>, GraphRequest.GraphJSONObjectCallback {
    public static final String TAG = LandingActivity.class.getName();

    private CallbackManager callbackManager;
    private ProgressDialog progressDialog;
    private FBCallback fbCallback;

    @State String accessToken;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_landing;
    }

    @Override
    public void onViewReady() {
        getImageExtra();
        initFacebook();
//        initDeepLink();
        introSlider();
//        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
    }

    public void introSlider() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                //  Initialize SharedPreferences
                SharedPreferences getPrefs = PreferenceManager
                        .getDefaultSharedPreferences(getBaseContext());

                //  Create a new boolean and preference and set it to true
                boolean isFirstStart = getPrefs.getBoolean("firstStart", true);

                //  If the activity has never started before...
                if (isFirstStart) {

                    //  Launch app intro
                    final Intent i = new Intent(LandingActivity.this, IntroActivity.class);

                    runOnUiThread(new Runnable() {
                        @Override public void run() {
                            startActivity(i);
                        }
                    });

                    //  Make a new preferences editor
                    SharedPreferences.Editor e = getPrefs.edit();

                    //  Edit preference to make it false because we don't want this to run again
                    e.putBoolean("firstStart", false);

                    //  Apply changes
                    e.apply();
                }
            }
        });

        // Start the thread
        t.start();
    }

    private void initDeepLink(){
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            Log.w(TAG, ">>>" + deepLink.getHost());
                        }

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        Log.w(TAG, "getDynamicLink:onFailure", e);
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        final Bundle extras = intent.getExtras();
        if(extras != null){
            Set<String> keySet = extras.keySet();
            Iterator<String> iterator = keySet.iterator();
            while (iterator.hasNext()) {
                final String key = iterator.next();
                final Object o = extras.get(key);
                Log.e("IntentData", ">>>" + key + ":" + o);
            }
        }

        int count = UserData.getInt(UserData.ERROR_COUNT);
        if(count > 2){
            UserData.insert(UserData.ERROR_COUNT, 0);
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText("App close due to some system error. Please restart the app and try again.");
            NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            mNotifyMgr.notify(001, mBuilder.build());
            finish();
        }
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        openSplashFragment();
    }

    public void openSplashFragment(){
        switchFragment(SplashFragment.newInstance(),false);
    }

    public void openLoginFragment(){
        switchFragment(LoginFragment.newInstance());
    }

    public void openSignUpFragment(){
        ((RouteActivity) getContext()).startRegistrationActivity("signup");
    }

    public void openForgotPasswordFragment(){
        ((RouteActivity) getContext()).startRegistrationActivity("forgot");
    }

    public void openLoginTILFragment(){
        switchFragment(LoginTILFragment.newInstance());
    }
    public void initFacebook(){
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, this);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {

        accessToken = AccessToken.getCurrentAccessToken().getToken();
        Log.e("Token", ">>>" +accessToken);
        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), this);

        Bundle parameters = new Bundle();
        parameters.putString("fields", "email, gender");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    public String getAccessToken(){
        if(accessToken == null){
            return "";
        }
        return accessToken;
    }

    @Override
    public void onCancel() {
        if(progressDialog != null){
            progressDialog.cancel();
        }
        if(fbCallback != null){
            fbCallback.failed();
        }
    }

    @Override
    public void onError(FacebookException error) {
        if(progressDialog != null){
            progressDialog.cancel();
        }
        if(fbCallback != null){
            fbCallback.failed();
        }
    }

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        Profile profile = Profile.getCurrentProfile();
        UserItem userItem = new UserItem();

        if ( profile != null ) {
            userItem.fb_id = profile.getId();
            userItem.name = profile.getName();
            if(object != null){
                try {
                    userItem.email = object.getString("email");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if(fbCallback != null){
                fbCallback.success(userItem);
            }
            Log.e("Login", ">>>FB Id: " + userItem.fb_id);
            Log.e("Login", ">>>email: " + userItem.email);
            Log.e("Login", ">>>name: " + userItem.name);
        }

        if(progressDialog != null){
            progressDialog.cancel();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(callbackManager!=null){
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void attemptFBLogin(FBCallback fbCallback){
        this.fbCallback = fbCallback;
        progressDialog = new ProgressDialog(getContext()).show(getContext(), "", "Connecting to Facebook...", false, false);
        LoginManager.getInstance().setLoginBehavior(LoginBehavior.DIALOG_ONLY);
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email,user_friends"));
    }

    public Uri getImageExtra(){

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                return (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
            }
        }

        return null;
    }

    public String getTextExtra(){

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)){
                Log.d("dsfasd", "safdasdf" + intent.getStringExtra(Intent.EXTRA_TEXT));
                return intent.getStringExtra(Intent.EXTRA_TEXT);
            }
        }

        return null;
    }

    public MyFirebaseMessagingService.FCMData getFCMData(){
        Intent intent = getIntent();
        final Bundle extras = intent.getExtras();
        if(extras != null && extras.containsKey("reference_id")){
           return new MyFirebaseMessagingService.FCMData(extras);
        }
        return null;
    }

    public boolean hasFCMData(){
        Intent intent = getIntent();
        final Bundle extras = intent.getExtras();
        return extras != null && extras.containsKey("reference_id");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public interface FBCallback{
        void success(UserItem userItem);
        void failed();
    }
}
