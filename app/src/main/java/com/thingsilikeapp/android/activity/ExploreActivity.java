package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.explore.ExploreFragment;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import java.util.List;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class ExploreActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = ExploreActivity.class.getName();

    @BindView(R.id.mainTitleTXT)            TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)        ImageView mainBackButtonIV;


    @Override

    public int onLayoutSet() {
        return R.layout.activity_explore;
    }

    @Override
    public void onViewReady() {
        mainBackButtonIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "explore":
//                openExploreFragment();
                break;
        }
    }


//    public void openExploreFragment(List<UserItem> user, List<WishListItem> likeee, List<WishListItem> recently) {
//        switchFragment(ExploreFragment.newInstance(user,likeee,recently));
//    }


    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;
        }
    }

}
