package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.messages.ChatChangeNameFragment;
import com.thingsilikeapp.android.fragment.messages.ChatEditFragment;
import com.thingsilikeapp.android.fragment.messages.CreateGroupChatFragment;
import com.thingsilikeapp.android.fragment.messages.GroupMessageFragment;
import com.thingsilikeapp.android.fragment.messages.MessageEditFragment;
import com.thingsilikeapp.android.fragment.messages.MessengerFragment;
import com.thingsilikeapp.android.fragment.messages.NewCreateMessageFragment;
import com.thingsilikeapp.android.fragment.messages.NewMessengerFragment;
import com.thingsilikeapp.android.fragment.messages.PeopleFragment;
import com.thingsilikeapp.android.fragment.messages.PrivateMessageFragment;
import com.thingsilikeapp.android.fragment.messages.GroupThreadFragment;
import com.thingsilikeapp.android.fragment.messages.PrivateThreadFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;
import icepick.State;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class MessageActivity extends RouteActivity implements View.OnClickListener {
//
//    @BindView(R.id.mainTitleTXT)                TextView mainTitleTXT;
//    @BindView(R.id.mainBackButtonIV)            ImageView mainBackButtonIV;
//    @BindView(R.id.infoBTN)                     ImageView infoBTN;
//    @BindView(R.id.newChatBTN)                  ImageView newChatBTN;

    @State int ids;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_messages;
    }

    @Override
    public void onViewReady() {
//        mainBackButtonIV.setOnClickListener(this);
//        infoBTN.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "message":
                openMessageFragment();
                break;
            case "private":
                openPrivateMessagesFragment();
                break;
            case "group":
                openGroupMessagesFragment();
                break;
            case "my_thread":
                openMyThreadFragment(getFragmentBundle().getInt("chat_id"), getFragmentBundle().getInt("user_id"), getFragmentBundle().getString("title"));
                break;
//            case "search":
//                openPeopleFragment();
//                break;

        }
    }

    public void openMessageFragment(){
        switchFragment(MessengerFragment.newInstance(0));
    }

    public void openNewMessageFragment(){ switchFragment(NewMessengerFragment.newInstance(0)); }

    public void openPrivateMessagesFragment(){ switchFragment(PrivateMessageFragment.newInstance()); }

    public void openGroupMessagesFragment(){ switchFragment(GroupMessageFragment.newInstance()); }

    public void openThreadFragment(int id){ switchFragment(GroupThreadFragment.newInstance(id)); }

    public void openMyThreadFragment(int id, int userid, String title){ switchFragment(PrivateThreadFragment.newInstance(id, userid, title)); }

    public void openEditFragment(int id){ switchFragment(ChatEditFragment.newInstance(id)); }

    public void openMessageEditFragment(int id){ switchFragment(MessageEditFragment.newInstance(id)); }

    public void openChangeNameFragment(int id){ switchFragment(ChatChangeNameFragment.newInstance(id)); }

    public void openCreateGroupChatFragment(){ switchFragment(CreateGroupChatFragment.newInstance()); }

    public void openNewCreateGroupChatFragment(){ switchFragment(NewCreateMessageFragment.newInstance(0)); }

//    public void openPeopleFragment(){
//        switchFragment(PeopleFragment.newInstance());
//    }

//    public void setTitle(String title){
//        mainTitleTXT.setText(title);
//    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;
//            case R.id.infoBTN:
//                openEditFragment(ids);
//                break;
//            case R.id.infoMessageBTN:
//                openMessageEditFragment(ids);
//                break;
        }
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }

    public void hideInfo(){
//        infoBTN.setVisibility(View.GONE);
//        newChatBTN.setVisibility(View.VISIBLE);
    }
    public void openInfo(){
//        infoBTN.setVisibility(View.VISIBLE);
//        newChatBTN.setVisibility(View.GONE);
    }
    public void openInfoMessage(){
//        newChatBTN.setVisibility(View.GONE);
}

    public void hideInfoMessage(){

    }

    public void infoID(int id){
        ids = id;
    }

}
