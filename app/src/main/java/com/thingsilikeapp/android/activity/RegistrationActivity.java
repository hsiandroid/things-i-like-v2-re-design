package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.registration.ForgotPasswordFragment;
import com.thingsilikeapp.android.fragment.registration.RegisterAFragment;
import com.thingsilikeapp.android.fragment.registration.ResetPasswordFragment;
import com.thingsilikeapp.android.fragment.registration.SignUpFragment;
import com.thingsilikeapp.android.fragment.registration.SuccessRequestFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class RegistrationActivity extends RouteActivity implements View.OnClickListener{
    public static final String TAG = RegistrationActivity.class.getName();

    @BindView(R.id.mainTitleTXT)        TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)    View backButtonIV;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_registration;
    }

    @Override
    public void onViewReady() {
        backButtonIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "signup":
                openSignUpFragment();
                break;
            case "forgot":
                openForgotPasswordFragment();
                break;
        }
    }

    public void openSignUpFragment() {
        switchFragment(SignUpFragment.newInstance());
    }


    public void openSuccessFragment(String email) { switchFragment(SuccessRequestFragment.newInstance(email)); }

    public void openForgotPasswordFragment() { switchFragment(ForgotPasswordFragment.newInstance()); }

    public void openResetPasswordFragment(String email) { switchFragment(ResetPasswordFragment.newInstance(email)); }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;
        }
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }
}
