package com.thingsilikeapp.android.activity;

import android.content.res.Configuration;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

//import com.github.tcking.viewquery.ViewQuery;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.item.TransactionItemFragment;
import com.thingsilikeapp.android.fragment.item.WishListItemFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

import butterknife.BindView;
//import tcking.github.com.giraffeplayer2.GiraffePlayer;
//import tcking.github.com.giraffeplayer2.PlayerManager;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class ItemActivity extends RouteActivity implements View.OnClickListener{
    public static final String TAG = ItemActivity.class.getName();

    private static final int SWIPE_MIN_DISTANCE = 250;
    private static final int SWIPE_MAX_OFF_PATH = 300;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
//    private ViewQuery $;

    private GestureDetector gestureDetector;
    private SwipeListener swipeListener;

    @BindView(R.id.mainTitleTXT)        TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)    View backButtonIV;
    @BindView(R.id.feedsBTN)            View feedsBTN;
    @BindView(R.id.optionBTN)           View optionBTN;
    @BindView(R.id.containerCON)        View containerCON;

    @Override
    public void onViewReady() {
        deepLink();
        backButtonIV.setOnClickListener(this);
        feedsBTN.setOnClickListener(this);
        gestureDetector = new GestureDetector( this, new SwipeDetector()); }

    @Override
    public int onLayoutSet() {
        return R.layout.activity_item;
    }


    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "i_like":
                openItemFragment(getFragmentBundle().getInt("things_i_like_id"));
                break;
            case "i_gave":
                openTransactionItem(getFragmentBundle().getInt("things_i_like_id"), getFragmentBundle().getInt("user_id", 0));
                break;
            case "i_received":
                openTransactionItem(getFragmentBundle().getInt("things_i_like_id"), getFragmentBundle().getInt("user_id", 0));
                break;
        }
    }

    public void openItemFragment(int id){
        switchFragment(WishListItemFragment.newInstance(id));
    }

    public void openTransactionItem(int id, int userID){
        switchFragment(TransactionItemFragment.newInstance(id, userID));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                onBackPressed();
                break;
            case R.id.feedsBTN:
                startMainActivity("home");
                break;
        }
    }

    public View getRePostTXT(){
        return optionBTN;
    }

    public View getContainer(){
        return containerCON;
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    public class SwipeDetector extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH){
                return false;
            }

            if( e2.getX() > e1.getX() ) {
                if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    if(swipeListener != null){
                        swipeListener.onSwipeRight();
                    }
                    return true;
                }
            }

            if( e1.getX() > e2.getX() ) {
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    if(swipeListener != null){
                        swipeListener.onSwipeLeft();
                    }
                    return true;
                }
            }
            return false;
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (gestureDetector != null) {
            if (gestureDetector.onTouchEvent(ev))
                return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)  {
        return gestureDetector.onTouchEvent(event);
    }

    public void setSwipeListener(SwipeListener swipeListener) {
        this.swipeListener = swipeListener;
    }

    public interface SwipeListener{
        void onSwipeRight();
        void onSwipeLeft();
    }

    @Override
    public void onBackPressed() {
//        if (PlayerManager.getInstance().onBackPressed()) {
//            return;
//        }

        super.onBackPressed();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        PlayerManager.getInstance().onConfigurationChanged(newConfig);
    }
}
