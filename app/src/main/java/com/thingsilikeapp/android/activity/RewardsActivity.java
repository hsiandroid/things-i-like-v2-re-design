package com.thingsilikeapp.android.activity;

import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.rewards.AddressBookCreateFragment;
import com.thingsilikeapp.android.fragment.rewards.ConfirmPurchaseFragment;
import com.thingsilikeapp.android.fragment.rewards.GeneralInfoFragment;
import com.thingsilikeapp.android.fragment.rewards.MyRewardDetailFragment;
import com.thingsilikeapp.android.fragment.rewards.OrderDetail.OrderDetailFragment;
import com.thingsilikeapp.android.fragment.rewards.RedeemableDetailFragment;
import com.thingsilikeapp.android.fragment.rewards.RedeemableFragment;
import com.thingsilikeapp.android.fragment.rewards.TreasuryFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.EncashmentCardFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.EncashmentMyCardFragment;
import com.thingsilikeapp.android.fragment.rewards.historyDetails.HistoryDetailsFragment;
import com.thingsilikeapp.android.fragment.rewards.verification.EditPurchaseFragment;
import com.thingsilikeapp.data.model.CatalogueItem;
import com.thingsilikeapp.data.model.OrderItem;
import com.thingsilikeapp.data.model.RewardHistoryItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.social.WalkThroughRequest;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.GemHelper;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.NumberFormatter;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class RewardsActivity extends RouteActivity implements View.OnClickListener, View.OnLongClickListener {

    @BindView(R.id.mainTitleTXT)                TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)            ImageView mainBackButtonIV;
    @BindView(R.id.titleCON)                    View titleCON;
    @BindView(R.id.titleCON2)                   View titleCON2;
    @BindView(R.id.mainBackButton2IV)           View mainBackButton2IV;
    @BindView(R.id.optionBTN)                   View optionBTN;
    @BindView(R.id.gemTXT)                      TextView gemTXT;
    @BindView(R.id.percentPB)                   ProgressBar percentPB;
    @BindView(R.id.chatBTN)                     View chatBTN;
    @BindView(R.id.walletBTN)                   View walletBTN;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_rewards;
    }

    @Override
    public void onViewReady() {
        mainBackButtonIV.setOnClickListener(this);
        mainBackButton2IV.setOnClickListener(this);
        chatBTN.setOnClickListener(this);
        optionBTN.setOnClickListener(this);
        walletBTN.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "shop":
                openRedeemableFragment();
                break;
            case "treasury":
                openMyTreasuryFragment();
                break;
            case "details":
                openShopDetailFragment(getFragmentBundle().getInt("id"));
                break;
            case "reward_details":
                openMyRewardDetailFragment(getFragmentBundle().getInt("id"));
                break;
//            case "history_Details":
//                openHistoryDetailsFragment();
//                break;

        }
    }

    public void updateReward(){
        if(gemTXT != null){
            gemTXT.setText(UserData.getDisplayCrystalFragment());
        }
        setRewardFragment(UserData.getRewardFragment());
    }

    public void setRewardFragment(String progress){
        percentPB.setMax(100);
        double finalProgress = NumberFormatter.parseDouble(progress);
        if (finalProgress == 0){
            percentPB.setProgress(1);
        }else if (finalProgress > 1 && finalProgress < 15){
            percentPB.setProgress(10);
        }else if (finalProgress > 16 && finalProgress < 25){
            percentPB.setProgress(20);
        }else if (finalProgress > 26 && finalProgress < 45){
            percentPB.setProgress(40);
        }else if (finalProgress > 46 && finalProgress < 65){
            percentPB.setProgress(60);
        }else if (finalProgress > 66 && finalProgress < 85){
            percentPB.setProgress(80);
        }else{
            percentPB.setProgress((int)finalProgress);
        }
    }

    public void setTitleDark(boolean dark){
        if(dark){
            titleCON.setVisibility(View.GONE);
            titleCON2.setVisibility(View.VISIBLE);
        }else{
            titleCON.setVisibility(View.VISIBLE);
            titleCON2.setVisibility(View.GONE);
        }
    }

    public void openRedeemableFragment(){
        switchFragment(RedeemableFragment.newInstance());
    }
    public void openEncashmentMyCardFragment(){ switchFragment (EncashmentMyCardFragment.newInstance()); }
    public void openOrderdetailFragment(int orderItem){ switchFragment (OrderDetailFragment.newInstance(orderItem)); }

    public void openEncashmentCardFragment(){
        switchFragment (EncashmentCardFragment.newInstance());
    }

    public void openHistoryDetailsFragment(RewardHistoryItem rewardHistoryItem){
        switchFragment(HistoryDetailsFragment.newInstance(rewardHistoryItem));
    }


    public void openMyTreasuryFragment(){
        String type = "";
        if (getFragmentBundle() != null){
            if (getFragmentBundle().getString("type") != null){
                type = getFragmentBundle().getString("type");
            } else {
                type = "";
            }
        }
        switchFragment(TreasuryFragment.newInstance(type));
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    public void openShopDetailFragment(int id){
        switchFragment(RedeemableDetailFragment.newInstance(id));
    }

    public void openConfirmPurchaseFragment(CatalogueItem catalogueItem){
        switchFragment(ConfirmPurchaseFragment.newInstance(catalogueItem));
    }

    public void openEditPurchaseFragment(OrderItem catalogueItem){
        switchFragment(EditPurchaseFragment.newInstance(catalogueItem));
    }

    public void openMyRewardDetailFragment(int id){ switchFragment(MyRewardDetailFragment.newInstance(id)); }

    public void openAddAddressFragment(AddressBookCreateFragment.Callback callback){ switchFragment(AddressBookCreateFragment.newInstance(callback)); }

    public void openGeneralInfoFragment(){
        switchFragment(GeneralInfoFragment.newInstance());
    }

    public void showOption(boolean show){
        optionBTN.setVisibility(show ? View.VISIBLE : View.GONE);
        if(!show){
            optionBTN.setOnClickListener(null);
        }
    }

    public void showWallet (boolean show){
        walletBTN.setVisibility(show ? View.VISIBLE : View.GONE);
        if(!show){
            walletBTN.setOnClickListener(null);
        }
    }


    public View getOptionBTN() {
        return optionBTN;
    }

    public View getWalletBTN(){
        return walletBTN;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButton2IV:
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;
            case R.id.optionBTN:
                break;
            case R.id.walletBTN:
                openEncashmentMyCardFragment();
                break;
            case R.id.chatBTN:
                startMessageActivity("message");
                break;
        }
    }

    @Override
    public void onBackPressed() {
         if (UserData.getBoolean(UserData.WALKTHRU_RESPONSE)){
            attemptGetWalkThrough();
        } else {
            super.onBackPressed();
        }
    }

    private void attemptGetWalkThrough(){
            WalkThroughRequest walkThroughRequest = new WalkThroughRequest(getContext());
            walkThroughRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                    .execute();

    }

    @Subscribe
    public void onResponse(WalkThroughRequest.ServerResponse responseData) {
        UserData.insert(UserData.WALKTHRU_RESPONSE, false);
        BaseTransformer wishListInfoTransformer = responseData.getData(BaseTransformer.class);
        if(wishListInfoTransformer.status){
            UserData.insert(UserData.WALKTHRU_ALREADY, false);
            onBackPressed();
        }else{
            UserData.insert(UserData.WALKTHRU_ALREADY, true);
            onBackPressed();
        }
    }

    public void showBackWalkThrough(){
        new MaterialShowcaseView.Builder(this)
                .setTarget(mainBackButtonIV)
                .setMaskColour(ActivityCompat.getColor(getContext(), R.color.walkthrough_bg1))
                .setTargetTouchable(true)
                .setShapePadding(10)
                .setDismissOnTargetTouch(true)
                .setContentTextColor(ActivityCompat.getColor(getContext(), R.color.white_dim))
                .setContentText("The list of items you can redeem with your Lyka Gems.")
                .show();
    }

    @Override
    public boolean onLongClick(View view) {
        switch(view.getId()){
            case R.id.walletBTN:

        }
        return false;
    }
}
