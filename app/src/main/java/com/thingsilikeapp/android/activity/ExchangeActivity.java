package com.thingsilikeapp.android.activity;

import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.rewards.exchange.AddBankAccountFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.ChangePasscodeFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.ConfirmPasscodeFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.EditBankAccountFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.ExchangeFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.ForgotPasscodeFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.HistoryAccountFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.NewPasscodeFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.SelectBankAccountFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.SetPasscodeFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.TransactionDetailFragment;
import com.thingsilikeapp.android.fragment.rewards.exchange.UpdatePasscodeFragment;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.data.model.EncashHistoryItem;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class ExchangeActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = ExchangeActivity.class.getName();

    @BindView(R.id.mainTitleTXT)            TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)        ImageView mainBackButtonIV;


    @Override

    public int onLayoutSet() {
        return R.layout.activity_exchange;
    }

    @Override
    public void onViewReady() {
        mainBackButtonIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "exchange":
                openExchangeFragment();
                break;

            case "change_pass":
                openChangePasscodeFragment();
                break;

            case "history":
                openHistoryFragment();
                break;

            case "set":
                openSetPasscodeFragment();
                break;

        }
    }


    public void openExchangeFragment() { switchFragment(ExchangeFragment.newInstance()); }
    public void openSelectBankFragment(String currency, String amount, String totalAmount, String serviceFee, String rate, String conversion) { switchFragment(SelectBankAccountFragment.newInstance(currency, amount, totalAmount, serviceFee, rate, conversion)); }
    public void openAddBankFragment(String currency, String amount, String totalAmount, String serviceFee, String rate, String conversion, AddBankAccountFragment.Callback callback) { switchFragment(AddBankAccountFragment.newInstance(currency, amount, totalAmount, serviceFee, rate, conversion, callback)); }
    public void openEditBankFragment(BankItem bankItem, String currency, String amount, String totalAmount, String serviceFee, String rate, String conversion) { switchFragment(EditBankAccountFragment.newInstance(bankItem, currency, amount, totalAmount, serviceFee, rate, conversion)); }
    public void openChangePasscodeFragment() { switchFragment(ChangePasscodeFragment.newInstance()); }
    public void openHistoryFragment() { switchFragment(HistoryAccountFragment.newInstance()); }
    public void openTransactionFragment(EncashHistoryItem encashHistoryItem) { switchFragment(TransactionDetailFragment.newInstance(encashHistoryItem)); }
    public void openSetPasscodeFragment() { switchFragment(SetPasscodeFragment.newInstance()); }
    public void openNewPasscodeFragment(String passcode) { switchFragment(NewPasscodeFragment.newInstance(passcode)); }
    public void openUpdatePasscodeFragment(String old_passcode, String passcode) { switchFragment(UpdatePasscodeFragment.newInstance(old_passcode, passcode)); }
    public void openConfirmPasscodeFragment(String passcode) { switchFragment(ConfirmPasscodeFragment.newInstance(passcode)); }
    public void openForgotPasscodeFragment(){switchFragment(ForgotPasscodeFragment.newInstance());}

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                this.finish();
//                onBackPressed();
                break;
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            this.finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
