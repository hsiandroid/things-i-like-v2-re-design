package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.rewards.verification.AddressVerificationFragment;
import com.thingsilikeapp.android.fragment.rewards.verification.EmailVerificationFragment;
import com.thingsilikeapp.android.fragment.rewards.verification.IdentityVerificationFragment;
import com.thingsilikeapp.android.fragment.rewards.verification.OTPVerificationFragment;
import com.thingsilikeapp.android.fragment.rewards.verification.OTPVerificationSFragment;
import com.thingsilikeapp.android.fragment.rewards.verification.PhoneVerificationFragment;
import com.thingsilikeapp.android.fragment.rewards.verification.PhoneVerificationSFragment;
import com.thingsilikeapp.android.fragment.rewards.verification.SelfieVerificationFragment;
import com.thingsilikeapp.android.fragment.rewards.verification.VerificationFragment;
import com.thingsilikeapp.android.fragment.rewards.verification.VerificationImageFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class VerificationActivity extends RouteActivity implements View.OnClickListener{
    public static final String TAG = VerificationActivity.class.getName();

    @BindView(R.id.mainTitleTXT) TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV) ImageView mainBackButtonIV;

    @Override

    public int onLayoutSet() {
        return R.layout.activity_verification;
    }

    @Override
    public void onViewReady() {
        mainBackButtonIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "verification":
                openVerificationFragment();
                break;
            case "verificationS":
                openPhoneSFragment();
                break;
        }
    }

    public void openVerificationFragment() { switchFragment(VerificationFragment.newInstance()); }

    public void openEmailFragment() { switchFragment(EmailVerificationFragment.newInstance()); }

    public void openOTPFragment(String contact, String country_code, String country) { switchFragment(OTPVerificationFragment.newInstance(contact, country_code, country)); }

    public void openOTPSFragment(String contact, String country_code, String country) { switchFragment(OTPVerificationSFragment.newInstance(contact, country_code, country)); }

    public void openPhoneFragment() { switchFragment(PhoneVerificationFragment.newInstance()); }

    public void openPhoneSFragment() { switchFragment(PhoneVerificationSFragment.newInstance()); }

    public void openIdentityFragment() { switchFragment(IdentityVerificationFragment.newInstance()); }

    public void openSelfieFragment() { switchFragment(SelfieVerificationFragment.newInstance()); }

    public void openAddressFragment() { switchFragment(AddressVerificationFragment.newInstance()); }

    public void openVerificationImageFragment(String url, String format, String type, String status) { switchFragment(VerificationImageFragment.newInstance(url, format, type, status)); }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;
        }
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    public void setBackBTN(boolean button){
        if (button){
           mainBackButtonIV.setVisibility(View.VISIBLE);
        }else{
            mainBackButtonIV.setVisibility(View.GONE);
        }
    }

}
