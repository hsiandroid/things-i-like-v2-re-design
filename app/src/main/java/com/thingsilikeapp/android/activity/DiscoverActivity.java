package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.main.KendraFragment;
import com.thingsilikeapp.android.fragment.main.MainSettingsFragment;
import com.thingsilikeapp.android.fragment.main.NotificationFragment;
import com.thingsilikeapp.android.fragment.main.PeopleFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;
import icepick.State;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class DiscoverActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = DiscoverActivity.class.getName();

    @BindView(R.id.mainTitleTXT)                TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)            ImageView mainBackButtonIV;

    @State public String sampleStr;

    @Override

    public int onLayoutSet() {
        return R.layout.activity_discover;
    }

    @Override
    public void onViewReady() {
        mainBackButtonIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "notif":
                openNotificationFragment(NotificationFragment.ACTIVITY_VIEW);
                break;
            case "setting":
                openSettingsFragment();
                break;
            case "people":
                openPeopleFragment();
                break;
//            case "kendra":
//                openKendraFragment(getFragmentBundle().getInt("chat_id"));
//                break;
        }
    }



    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    public void openNotificationFragment(int selectedView){
        switchFragment(NotificationFragment.newInstance(selectedView));
//        addProfileBadge(0);
    }

    public void openSettingsFragment(){
        switchFragment(MainSettingsFragment.newInstance());
    }

    public void openPeopleFragment(){
        switchFragment(PeopleFragment.newInstance()); }

    public void openKendraFragment(int id){
        switchFragment(KendraFragment.newInstance(id));
//        addProfileBadge(0);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;
        }
    }
}
