package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.request.RequestOngoingFragment;
import com.thingsilikeapp.android.fragment.request.RequestOwnedFragment;
import com.thingsilikeapp.android.fragment.request.RequestTransactionFragment;
import com.thingsilikeapp.android.fragment.request.RequestViewerFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class RequestActivity extends RouteActivity implements View.OnClickListener {

    @BindView(R.id.mainTitleTXT)        TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)    ImageView mainBackButtonIV;
    @BindView(R.id.feedsBTN)            View feedsBTN;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_request;
    }

    @Override
    public void onViewReady() {
        mainBackButtonIV.setOnClickListener(this);
        feedsBTN.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "request":
                openRequestOngoingFragment(getFragmentBundle().getInt("wishlist_id"), getFragmentBundle().getInt("count"), getFragmentBundle().getString("title"));
                break;
            case "viewer":
                openRequestViewerFragment();
                break;
            case "transaction":
                openRequestTransactionFragment();
                break;
            case "owned":
                openRequestOwnedFragment();
                break;
        }
    }

    public void openRequestOngoingFragment(int wishListID, int count, String title){
        switchFragment(RequestOngoingFragment.newInstance(wishListID, count, title));
    }

    public void openRequestViewerFragment(){
        switchFragment(RequestViewerFragment.newInstance());
    }

    public void openRequestTransactionFragment(){
        switchFragment(RequestTransactionFragment.newInstance());
    }

    public void openRequestOwnedFragment(){
        switchFragment(RequestOwnedFragment.newInstance());
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;
            case R.id.feedsBTN:
                startMainActivity("home");
                break;
        }
    }
}
