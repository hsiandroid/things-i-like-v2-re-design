package com.thingsilikeapp.android.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.plattysoft.leonids.ParticleSystem;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import com.pusher.client.util.HttpAuthorizer;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.dialog.WalkThroughIntroDialog;
import com.thingsilikeapp.android.dialog.WalkThroughMiddleDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.android.fragment.explore.ExploreFragment;
import com.thingsilikeapp.android.fragment.explore.SearchExploreFragment;
import com.thingsilikeapp.android.fragment.main.DiscoverFragment;
import com.thingsilikeapp.android.fragment.main.ProfileFragment;
import com.thingsilikeapp.android.fragment.main.SocialFeedFragment;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.GemItem;
import com.thingsilikeapp.data.model.NotificationSettingsItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WalletNotificationItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.explore.HighestRatedRequest;
import com.thingsilikeapp.server.request.explore.HotRequest;
import com.thingsilikeapp.server.request.explore.LikeeRequest;
import com.thingsilikeapp.server.request.explore.RecentlyRequest;
import com.thingsilikeapp.server.request.explore.RecommendedRequest;
import com.thingsilikeapp.server.request.explore.TrendingUserRequest;
import com.thingsilikeapp.server.request.notification.UnreadCountRequest;
import com.thingsilikeapp.server.request.notification.WalletNotificationRequest;
import com.thingsilikeapp.server.request.user.UserInfoRequest;
import com.thingsilikeapp.server.transformer.explore.ExploreTransformer;
import com.thingsilikeapp.server.transformer.explore.TrendingUserTransformer;
import com.thingsilikeapp.server.transformer.notification.UnreadNotificationTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.BadgeUtils;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.WalkThrough;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.android.java.firebase.MyFirebaseMessagingService;
import com.thingsilikeapp.vendor.android.java.pusher.Cred;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.microsoft.appcenter.distribute.Distribute;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import cn.nekocode.badge.BadgeDrawable;
import uk.co.deanwild.materialshowcaseview.IShowcaseListener;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class MainActivity extends RouteActivity implements View.OnClickListener,
        WalkThroughIntroDialog.Callback,
        WalkThroughMiddleDialog.Callback {

    private static final int PERMISSION_GALLERY = 202;
    private Fragment currentFragment;


    @BindView(R.id.socialFeedBTN)   ImageView socialFeedBTN;
    @BindView(R.id.peopleBTN)       ImageView peopleBTN;
    @BindView(R.id.createBTN)       ImageView createBTN;
    @BindView(R.id.profileBTN)      ImageView profileBTN;
    @BindView(R.id.socialFeedTXT)   TextView socialFeedTXT;
    @BindView(R.id.peopleTXT)       TextView peopleTXT;
    @BindView(R.id.createTXT)       TextView createTXT;
    @BindView(R.id.notificationTXT) TextView notificationTXT;
    @BindView(R.id.profileTXT)      TextView profileTXT;
    @BindView(R.id.birthDayCON)     View birthDayCON;
    @BindView(R.id.notificationBTN) ImageView notificationBTN;
    @BindView(R.id.createNewPostLBL) View createNewPostLBL;
    @BindView(R.id.createNewPostCON) View createNewPostCON;
    @BindView(R.id.profileBadgeIV)   ImageView profileBadgeIV;
    @BindView(R.id.container)        FrameLayout container;
    @BindView(R.id.walletBTN)       View walletBTN;
    @BindView(R.id.chatBTN)         View chatBTN;
    @BindView(R.id.rewardCON)       View rewardCON;
    @BindView(R.id.gemTXT)          TextView gemTXT;
    @BindView(R.id.percentPB)       ProgressBar percentPB;
    @BindView(R.id.walletBadgeIV)   ImageView walletBadgeIV;


    private Timer createPostAnim;
    private Animation createPost;
    private int width;
    private Pusher pusher;
    public WalletNotificationItem walletNotificationItem;
    public UserInfoRequest userInfoRequest;

    private TrendingUserRequest trendingUserRequest;
    private LikeeRequest likeeRequest;
    private RecentlyRequest recentlyRequest ;
    private RecommendedRequest recommendedRequest;
    private HotRequest hotRequest;
    private HighestRatedRequest highestRatedRequest;

    private List<UserItem> topuser;
    private List<WishListItem> likeee;
    private List<WishListItem> recently;
    private List<WishListItem> recommended;
    private List<WishListItem> hot;
    private List<WishListItem> highest;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady() {
        createBTN.setOnClickListener(this);
        createNewPostCON.setOnClickListener(this);

        socialFeedBTN.setOnClickListener(this);
        profileBTN.setOnClickListener(this);
        peopleBTN.setOnClickListener(this);
        notificationBTN.setOnClickListener(this);
        birthDayCON.setOnClickListener(this);

        walletBTN.setOnClickListener(this);
        chatBTN.setOnClickListener(this);
        rewardCON.setOnClickListener(this);
        gemTXT.setText(UserData.getDisplayCrystalFragment());
        setupPusherGem("user.wallet." + UserData.getUserId());
        percentPB.setMax(100);
        accountSetup();
//        showBirthday();

        if (getActivityBundle() != null) {
            Bundle bundle = getActivityBundle();
            if (bundle.containsKey("post_content")) {
                startWishListActivity(bundle.getString("post_content"), "create");
            } else if (bundle.containsKey("image_uri")) {
                startWishListActivity((Uri) bundle.getParcelable("image_uri"), "create");
            } else if (bundle.containsKey("fcm_data")) {
                MyFirebaseMessagingService.FCMData fcmData = bundle.getParcelable("fcm_data");
                redirect(fcmData.type, fcmData.referenceID, fcmData.event);
            }
        } else {
            deepLink();
        }
        width = getContext().getResources().getDisplayMetrics().widthPixels;

        divideView(socialFeedBTN);
        divideView(peopleBTN);
        divideView(createBTN);
        divideView(notificationBTN);
        divideView(profileBTN);

        divideView(socialFeedTXT);
        divideView(peopleTXT);
        divideView(createTXT);
        divideView(notificationTXT);
        divideView(profileTXT);

        AppCenter.setLogLevel(Log.VERBOSE);

        AppCenter.start(getApplication(), "3820ad1a-6b52-43df-82f8-d63aa3f71170",

                Analytics.class, Crashes.class, Distribute.class);
    }


    public void doAnimation() {
        container.setRotation(180);
        final Bitmap bitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.icon_lyka_chips),
                10, 10, false);

        new ParticleSystem(this, 70, bitmap, 5000)
                .setSpeedRange(.3f, .5f)
                .oneShot(findViewById(R.id.gemTXT), 70);
    }

    public double intFormatter(String value) {
        if (!value.equalsIgnoreCase("") && value != null) {
            return Double.parseDouble(value);
        }
        return 0;
    }

    private void divideView(View view) {
        ViewGroup.LayoutParams rlp = view.getLayoutParams();
        rlp.width = width / 5;
        view.setLayoutParams(rlp);
    }

    private void showBirthday() {
        if (UserData.getUserItem().is_birthday) {
            birthDayCON.setVisibility(View.VISIBLE);
        } else {
            birthDayCON.setVisibility(View.GONE);
        }
    }

    public void feedActive() {
        socialFeedBTN.setSelected(true);
        peopleBTN.setSelected(false);
        notificationBTN.setSelected(false);
        profileBTN.setSelected(false);
        createBTN.setSelected(false);
    }

    public void peopleActive() {
        socialFeedBTN.setSelected(false);
        peopleBTN.setSelected(true);
        notificationBTN.setSelected(false);
        profileBTN.setSelected(false);
        createBTN.setSelected(false);
    }

    public void notificationActive() {
        socialFeedBTN.setSelected(false);
        peopleBTN.setSelected(false);
        notificationBTN.setSelected(true);
        profileBTN.setSelected(false);
        createBTN.setSelected(false);
    }

    public void profileActive() {
        socialFeedBTN.setSelected(false);
        peopleBTN.setSelected(false);
        notificationBTN.setSelected(false);
        profileBTN.setSelected(true);
        createBTN.setSelected(false);
    }

    @Override
    public void redirect(String target, int id, String event) {
        switch (target) {
            case "WISHLIST_TRANSACTION":
                if (event.equalsIgnoreCase("App\\Laravel\\Notifications\\WishlistTransaction\\GiftSentNotification")) {
//                    openNotificationFragment(NotificationFragment.REQUEST_VIEW);
                } else {
                    startItemActivity(id, 0, "i_received");
                }
                break;
            case "WISHLIST_VIEWER":
//                openNotificationFragment(NotificationFragment.REQUEST_VIEW);
                break;
            case "USER":
                ((RouteActivity) getContext()).startProfileActivity(id);
                break;
            case "BIRTHDAY":
                ((RouteActivity) getContext()).startBirthdaySurpriseActivity("");
                break;
            case "CHAT":
                ((RouteActivity) getContext()).startMessageActivity("message");
                break;
            case "WALLET":
                ((RouteActivity) getContext()).startRewardsActivity("treasury");
                break;
            case "PROFILE":
                ((RouteActivity) getContext()).startProfileActivity(id);
                break;
            case "HOME":
                openSocialFeedFragment();
                break;
            case "COMMENT":
                ((RouteActivity) getContext()).startCommentActivity(id, event);
                break;
            default:
                super.redirect(target, id, event);
        }
    }

    public void setupPusher() {
        HashMap<String, String> authHeader = new HashMap<>();
        authHeader.put("Authorization", "Bearer " + UserData.getString(UserData.AUTHORIZATION));

        HttpAuthorizer authorizer = new HttpAuthorizer("http://www.mylyka.com/broadcasting/auth");
        authorizer.setHeaders(authHeader);

        PusherOptions options = new PusherOptions();
        options.setCluster(Cred.CLUSTER);
        options.setEncrypted(true);
        options.setWssPort(443);
        options.setAuthorizer(authorizer);

        Pusher pusher = new Pusher(Cred.KEY, options);

        Channel channel = pusher.subscribePrivate("private-user." + UserData.getUserId());

        channel.bind("NewNotification", new PrivateChannelEventListener() {
            @Override
            public void onSubscriptionSucceeded(String s) {
                Log.e("Pusher", ">>>" + s);
            }

            @Override
            public void onAuthenticationFailure(String s, Exception e) {
                Log.e("Pusher", ">>>" + e.getMessage());
            }

            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                Log.e("Pusher", ">>>" + data);
                UnreadNotificationTransformer unreadNotificationTransformer = new Gson().fromJson(data, UnreadNotificationTransformer.class);
                if (unreadNotificationTransformer.unread != -1) {
                    addProfileBadge(unreadNotificationTransformer.unread);
                }
            }
        });

        pusher.connect(new ConnectionEventListener() {
            @Override
            public void onConnectionStateChange(ConnectionStateChange change) {
                Log.e("Pusher", "State changed to " + change.getCurrentState() +
                        " from " + change.getPreviousState());
            }

            @Override
            public void onError(String message, String code, Exception e) {
                Log.e("Pusher", "Message" + message + "Code" + code);
            }
        }, ConnectionState.ALL);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setupPusher();
        getBadgeCount();
        gemTXT.setText(UserData.getDisplayCrystalFragment());
        setupPusherGem("user.wallet." + UserData.getUserId());
        setRewardFragments();
        userInfoRequest = new UserInfoRequest(getContext());
        userInfoRequest
                .setDeviceRegID(this.getDeviceRegID())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,statistics,social,wallet,kyc")
                .addParameters(Keys.server.key.USER_ID, UserData.getUserId())
                .execute();

        initTopUser();
        initRecently();
        initLikee();
        initHot();
        initMostRated();
        initRecommended();
//        showBirthday();
    }

    @Subscribe
    public void onResponse(UserInfoRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            UserData.insert(userTransformer.userItem);
            UserData.insert(UserData.LYKAGEM_DISPLAY, userTransformer.userItem.lykagem_display);
            gemTXT.setText(userTransformer.userItem.lykagem_display);
            Log.e("EEEEEEEEEEEE", "rate: " + userTransformer.userItem.id);
            Log.e("EEEEEEEEEEEE", "rate: " + userTransformer.userItem.kyc.data.contactNumberStatus);
        } else {
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    public void attemptGetCurrentWalkThrough() {
        Log.e("Current ", ">>>" + UserData.getInt(UserData.WALKTHRU_CURRENT));
//        if (this == null){
//            return;
//        }
//        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)) {
//            switch (UserData.getInt(UserData.WALKTHRU_CURRENT)) {
//                case 0:
//                    WalkThroughIntroDialog.newInstance(this).show(getSupportFragmentManager(), WalkThroughIntroDialog.TAG);
        //                    MaterialShowcaseView.resetAll(getContext());
//                    break;
//                case 1:
////                    MaterialShowcaseView.resetAll(getContext());
//                    showWalkThrough();
//                    MaterialShowcaseView.resetAll(getContext());
//                    break;
//                case 2:
//                    showFriendsWalkThrough();
//                    break;
//                case 3:
//                    showProfileWalkThrough();
//                    MaterialShowcaseView.resetAll(getContext());
//                    break;
//                case 4:
//                    startProfileActivity(UserData.getInt(UserData.USER_ID));
//                    MaterialShowcaseView.resetAll(getContext());
//                    break;
//                case 5:
//                    MaterialShowcaseView.resetAll(getContext());
//                    WalkThroughMiddleDialog.newInstance(this)
//                            .show(getSupportFragmentManager(), WalkThroughMiddleDialog.TAG);
//                    break;
//                case 6:
//                    break;
//                case 7:
//                    MaterialShowcaseView.resetAll(getContext());
//                    showGemsWalkThrough();
//                    break;
//                case 8:
//                    MaterialShowcaseView.resetAll(getContext());
//                    showGemsFinalWalkThrough();
//                    break;
//                case 9:
//                    MaterialShowcaseView.resetAll(getContext());
//                    if (UserData.getBoolean(UserData.WALKTHRU_ALREADY)){
//                    WalkThroughThankYouDialog.newInstance().show(getSupportFragmentManager(), WalkThroughThankYouDialog.TAG);
//                    }else {
//                        WalkThroughFinalDialog.newInstance().show(getSupportFragmentManager(), WalkThroughFinalDialog.TAG);
//                    }
//                    break;
//                default:
//            }
//        }else {
//            if (UserData.getBoolean(UserData.WALKTHRU_CURRENT_PROFILE)){
//                startProfileActivity(UserData.getUserId());
//            }
//        }
    }

    public void showWalkThrough() {
        WalkThrough.createPost(getContext(), createBTN, new IShowcaseListener() {
            @Override
            public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {
                UserData.insert(UserData.WALKTHRU_CREATE_POST_SHOW, false);
            }

            @Override
            public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
                UserData.insert(UserData.WALKTHRU_CREATE_POST_SHOW, true);
            }
        });

//        new SpotlightView.Builder(this)
//                .introAnimationDuration(400)
//                .enableRevealAnimation(true)
//                .performClick(false)
//                .fadeinTextDuration(400)
//                .headingTvColor(ActivityCompat.getColor(getContext(), R.color.white))
//                .headingTvSize(20)
//                .headingTvText("Love")
////                .subHeadingTvColor(Color.parseColor("#ffffff"))
////                .subHeadingTvSize(16)
//                .subHeadingTvText("")
//                .maskColor(ActivityCompat.getColor(getContext(), R.color.walkthru_bg))
//                .target(createBTN)
//                .lineAnimDuration(400)
//                .lineAndArcColor(ActivityCompat.getColor(getContext(), R.color.white))
//                .dismissOnTouch(false)
//                .targetPadding(10)
        //                .dismissOnBackPress(false)
//                .showTargetArc(false)
//                .lineStroke(2)
//                .enableDismissAfterShown(false)
//                .show();
    }

    public void showFriendsWalkThrough() {
        WalkThrough.friends(getContext(), peopleBTN, new IShowcaseListener() {
            @Override
            public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {

            }


            @Override
            public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {

            }
        });
    }

    public void showGemsWalkThrough() {
//            MaterialShowcaseView.resetAll(getContext());
//            WalkThrough.gems(getContext(), gemsBTN, new IShowcaseListener() {
//                @Override
//                public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {
//
//
//                    if (rewardsBTN != null){
//                        rewardsBTN.setOnClickListener(null);
//                    }
//
//                }
//
//                @Override
//                public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
////                            if (gemsBTN != null){
////                                gemsBTN.setOnClickListener(MainActivity.this);
////                            }
//                    if (gemListener !=null){
//                        gemListener.onGemsClick();
//                    }
//
//                }
//            });
    }

    public void showChipsWalkThrough() {

//            MaterialShowcaseView.resetAll(getContext());
//            WalkThrough.chips(getContext(), shardsCON ,new IShowcaseListener() {
//                @Override
//                public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {
//                    if (rewardsBTN != null){
//                        rewardsBTN.setOnClickListener(null);
//                    }
//                }
//
//                @Override
//                public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
//                    UserData.insert(UserData.WALKTHRU_CURRENT, 8);
//
//                Handler handler;
//                handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        attemptGetCurrentWalkThrough();
//                    }
//                }, 200);
//
////                    showGemsFinalWalkThrough();
//
//                }
//            });
    }


    public void showGemsFinalWalkThrough() {
//        MaterialShowcaseView.resetAll(getContext());
//            if (rewardsBTN != null){
//
//                new MaterialShowcaseView.Builder(this)
//                        .setTarget(rewardsBTN)
//                        .setMaskColour(ActivityCompat.getColor(getContext(), R.color.walkthrough_bg1))
//                        .setTargetTouchable(true)
//                        .setDismissOnTargetTouch(true)
////                        .setShapePadding(-50)
//                        .withRectangleShape()
//                        .setListener(new IShowcaseListener() {
//                            @Override
//                            public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {
////                                rewardsBTN.setOnClickListener(MainActivity.this);
//                            }
//
//                            @Override
//                            public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
//
//                                startRewardsActivity("shop");
//
//                            }
//                        })
//                        .singleUse("final")
//                        .setContentTextColor(ActivityCompat.getColor(getContext(), R.color.white_dim))
//                        .setContentText("Tap this section to see what products you can redeem with your Lyka Gems.")
//                        .show();
//
//            }else {
//                startRewardsActivity("shop");
//            }
    }

    public void showProfileWalkThrough() {
        WalkThrough.profile(getContext(), profileBTN, new IShowcaseListener() {
            @Override
            public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {

            }

            @Override
            public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {

            }
        });
    }

    public void showBirthdayWalkThrough() {
////        MaterialShowcaseView.resetAll(getContext());
//       if (!UserData.getBoolean(UserData.WALKTHRU_DONE)){
//           new MaterialShowcaseView.Builder(this)
//                   .setTarget(eventBTN)
//                   .singleUse("event")
//                   .setMaskColour(ActivityCompat.getColor(getContext(), R.color.walkthrough_bg1))
//                   .setTargetTouchable(true)
////                   .setDismissOnTargetTouch(true)
//                   .setListener(new IShowcaseListener() {
//                       @Override
//                       public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {
//                           eventBTN.setOnClickListener(null);
//                       }
//
//                       @Override
//                       public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
////                        ToastMessage.show(getContext(), "Success", ToastMessage.Status.SUCCESS);
//                           Log.e("Current3", ">>>" + UserData.getInt(UserData.WALKTHRU_CURRENT));
//                           eventBTN.setOnClickListener(MainActivity.this);
//                           UserData.insert(UserData.WALKTHRU_CURRENT, 6);
//                           UserData.insert(UserData.BIRTHDAY_COUNT, 2);
//                           if(getCurrentFragment() instanceof SocialFeedFragment){
//                                 getCurrentFragment().onResume();
//                           }else {
//                               openSocialFeedFragment();
//                           }
//                       }
//                   })
//                   .setContentTextColor(ActivityCompat.getColor(getContext(), R.color.white_dim))
//                   .setContentText("Tap the \"cake\" icon to make scheduled birthdays greetings")
//                   .show();
//       }
    }

    @Override
    public void onMiddleDismiss(WalkThroughMiddleDialog walkThroughMiddleDialog) {
        walkThroughMiddleDialog.dismiss();
//        if (UserData.getInt(UserData.BIRTHDAY_COUNT) == 1){
//            showBirthdayWalkThrough();
//        }
    }

    @Override
    public void onDismiss(WalkThroughIntroDialog walkThroughIntroDialog) {
        walkThroughIntroDialog.dismiss();
//        Handler handler;
//        handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (!UserData.getBoolean(UserData.WALKTHRU_DONE)){
//                    showWalkThrough();
////                    attemptGetCurrentWalkThrough();
//                }
//            }
//        }, 100);
    }

    private void accountSetup() {
        if (UserData.getUserItem() != null) {
            UserItem userItem = UserData.getUserItem();
            if (userItem.info != null || userItem.info.data != null) {
//                if (checkAddress(userItem.info.data)) {
//                    startAccountActivity("profile", AccountActivity.CODE);
//                } else
                if (userItem.statistics.data.following <= 0 && UserData.getBoolean(UserData.FIRST_LOGIN)) {
                    startAccountActivity("suggestion", AccountActivity.CODE);
                }
            } else {
                return;
            }
        } else {
            return;
        }
    }

    private boolean checkAddress(UserItem.Info.InfoData info) {

        if (info == null)
            return true;

        if (info.birthdate.equals("")) {
            return true;
        }

        return info.profession.equals("");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AccountActivity.CODE) {
            Log.e("onActivityResult", "trigger");
            if (getCurrentFragment() instanceof SocialFeedFragment) {
                ((SocialFeedFragment) getCurrentFragment()).onRefresh();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (createPostAnim != null) {
            createPostAnim.cancel();
        }
    }

    public void getBadgeCount() {
        UnreadCountRequest unreadCountRequest = new UnreadCountRequest(getContext());
        unreadCountRequest
                .setDeviceRegID(getDeviceRegID())
                .showNoInternetConnection(false)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }

    public void createYourOwnPostLabelVisibility(boolean show) {
        if (show) {
            setCreatePostAnim();
            createNewPostLBL.setVisibility(View.VISIBLE);
        } else {
            createNewPostLBL.setVisibility(View.GONE);
            if (createPostAnim != null) {
                createPostAnim.cancel();
            }
        }
    }

    private void setCreatePostAnim() {
        createPost = AnimationUtils.loadAnimation(getContext(), R.anim.attention);
        createPostAnim = new Timer();
        createPostAnim.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (createBTN != null) {
                            if (createBTN.isShown()) {
                                createBTN.clearAnimation();
                                createBTN.setAnimation(createPost);
                            }
                        }
                    }
                });
            }
        }, 0, 2000);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName) {
            case "notification":
//                openNotificationFragment(NotificationFragment.REQUEST_VIEW);
                break;
            case "profile":
                openProfileFragment();
                break;
            case "main":
                openSocialFeedFragment();
                break;
            default:
                openSocialFeedFragment();
        }
    }

    public void openSocialFeedFragment() {
        switchFragment(SocialFeedFragment.newInstance());
    }

    public void openCreatePostFragment() {
        openProfileFragment();
        startWishListActivity((WishListItem) null, "create");
    }

    public void openProfileFragment() {
        switchFragment(ProfileFragment.newInstance());
    }

    public void openExploreFragment(List<UserItem> user, List<WishListItem> likeee,List<WishListItem> recently) {
        switchFragment(ExploreFragment.newInstance(user,likeee,recently));
    }

    public void openSearchExploreFragment() {
        switchFragment(SearchExploreFragment.newInstance());
    }


//    public void openNotificationFragment(int selectedView){
//        switchFragment(NotificationFragment.newInstance(selectedView));
//        addProfileBadge(0);
//    }

    public void openDiscoverFragment() {
        switchFragment(DiscoverFragment.newInstance());
    }

//    public void openSettingsFragment(){
//        switchFragment(MainSettingsFragment.newInstance());
//    }

//    public void openPeopleFragment(){
//        switchFragment(PeopleFragment.newInstance());
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.socialFeedBTN:
                if (getCurrentFragment() instanceof SocialFeedFragment) {
                    ((SocialFeedFragment) getCurrentFragment()).shortcutClicked();
                } else {
                    openSocialFeedFragment();
                }
                facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT, FacebookCustomLoggerMessage.MainActivity.SOCIAL_FEED);
                break;
            case R.id.createNewPostCON:
            case R.id.createBTN:
//                openCreatePostFragment();
                if (UserData.getUserItem().kyc.data.contactNumberStatus.equalsIgnoreCase("pending")) {
                    Log.e("EEEEEEEEEEE", ">>>>" + UserData.getUserItem().kyc.data.contactNumberStatus);
                    final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
                    confirmationDialog
                            .setDescription("Activate Lyka Wallet")
                            .setNote("Input your mobile number to start earning LYKA Gems.")
                            .setIcon(R.drawable.icon_information)
                            .setPositiveButtonText("Proceed")
                            .setPositiveButtonClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startVerificationActivity("verificationS");
                                    confirmationDialog.dismiss();
                                }
                            })
                            .setNegativeButtonText("Ignore")
                            .build(getSupportFragmentManager());
                } else {
                    if (PermissionChecker.checkPermissions(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_GALLERY)) {
                        startCreatePostActivity("gallery");
                    }
                }

                facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT, FacebookCustomLoggerMessage.MainActivity.CREATE_POST);
                break;
            case R.id.eventBTN:
                startBirthdayActivity("celebrant");
                break;
            case R.id.profileBTN:
                openProfileFragment();
                facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT, FacebookCustomLoggerMessage.MainActivity.PROFILE);
                break;
            case R.id.notificationBTN:
//                openNotificationFragment(NotificationFragment.ACTIVITY_VIEW);
//                facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT,FacebookCustomLoggerMessage.MainActivity.NOTIFICATION);
//                BadgeUtils.clearBadge(getContext());
                openDiscoverFragment();
                break;
            case R.id.settingsBTN:
//                openSettingsFr
// agment();
                facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT, FacebookCustomLoggerMessage.MainActivity.SETTINGS);
                break;
            case R.id.feedsBTN:
                if (getCurrentFragment() instanceof SocialFeedFragment) {
                    ((SocialFeedFragment) getCurrentFragment()).shortcutClicked();
                } else {
                    openSocialFeedFragment();
                }
                facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT, FacebookCustomLoggerMessage.MainActivity.SOCIAL_FEED);
                break;
            case R.id.peopleBTN:
//                openPeopleFragment();
//                if (topuser != null && likeee != null && recently != null){
                    openExploreFragment(topuser, likeee, recently);
//                }else {
//                ToastMessage.show(getContext(), "Please wait we are still loading some data.", ToastMessage.Status.SUCCESS);
//                }
                facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT, FacebookCustomLoggerMessage.MainActivity.SEARCH);
                break;
            case R.id.birthDayCON:
                openBirthdaySurprise();
                break;
            case R.id.walletBTN:
                if (UserData.getUserItem().kyc.data.contactNumberStatus.equalsIgnoreCase("pending")) {
                    Log.e("EEEEEEEEEEE", ">>>>" + UserData.getUserItem().kyc.data.contactNumberStatus);
                    final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
                    confirmationDialog
                            .setDescription("Activate Lyka Wallet")
                            .setNote("Input your mobile number to start earning LYKA Gems.")
                            .setIcon(R.drawable.icon_information)
                            .setPositiveButtonText("Proceed")
                            .setPositiveButtonClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startVerificationActivity("verificationS");
                                    confirmationDialog.dismiss();
                                }
                            })
                            .setNegativeButtonText("Ignore")
                            .build(getSupportFragmentManager());
                } else {
                    startRewardsActivity("treasury");
                }
                break;
            case R.id.chatBTN:
                if (UserData.getUserItem().kyc.data.contactNumberStatus.equalsIgnoreCase("pending")) {
                    Log.e("EEEEEEEEEEE", ">>>>" + UserData.getUserItem().kyc.data.contactNumberStatus);
                    final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
                    confirmationDialog
                            .setDescription("Verify your account?")
                            .setNote("Your contact number is not yet verified.")
                            .setIcon(R.drawable.icon_information)
                            .setPositiveButtonText("Proceed")
                            .setPositiveButtonClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startVerificationActivity("verificationS");
                                    confirmationDialog.dismiss();
                                }
                            })
                            .setNegativeButtonText("Ignore")
                            .build(getSupportFragmentManager());
                } else {
                    startMessageActivity("message");
                }
                break;
            case R.id.rewardCON:
                startRewardsActivity("shop");
                break;

        }
    }

    public void updateReward(String lykagem, String progress, int max) {
        if (gemTXT != null) {
            gemTXT.setText(lykagem);
        }
        setRewardFragment(progress, max);
    }

    public void setRewardFragment(String progress, int max) {
        try {
            percentPB.setMax(max);
            double finalProgress = intFormatter(progress);
            if (finalProgress == 0) {
                percentPB.setProgress(1);
            } else if (finalProgress > 1 && finalProgress < 15) {
                percentPB.setProgress(10);
            } else if (finalProgress > 16 && finalProgress < 25) {
                percentPB.setProgress(20);
            } else if (finalProgress > 26 && finalProgress < 45) {
                percentPB.setProgress(40);
            } else if (finalProgress > 46 && finalProgress < 65) {
                percentPB.setProgress(60);
            } else if (finalProgress > 66 && finalProgress < 85) {
                percentPB.setProgress(80);
            } else {
                percentPB.setProgress((int) finalProgress);
            }
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
    }

    public void setRewardFragments() {
        double finalProgress = intFormatter(UserData.getRewardFragment());
        if (finalProgress == 0) {
            percentPB.setProgress(1);
        } else if (finalProgress > 1 && finalProgress < 15) {
            percentPB.setProgress(10);
        } else if (finalProgress > 16 && finalProgress < 25) {
            percentPB.setProgress(20);
        } else if (finalProgress > 26 && finalProgress < 45) {
            percentPB.setProgress(40);
        } else if (finalProgress > 46 && finalProgress < 65) {
            percentPB.setProgress(60);
        } else if (finalProgress > 66 && finalProgress < 85) {
            percentPB.setProgress(80);
        } else {
            percentPB.setProgress((int) finalProgress);
        }
    }

    private void setupPusherGem(String channelName) {
        PusherOptions options = new PusherOptions();
        options.setCluster(Cred.CLUSTER);
        pusher = new Pusher(Cred.KEY, options);
        Channel channel = pusher.subscribe(channelName);

        channel.bind("UpdatedLykaGems", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                processGemPusher(data, eventName);
            }
        });

        pusher.connect();
    }

    private void processGemPusher(final String data, final String eventName) {
        final GemItem gemItem = new Gson().fromJson(data, GemItem.class);

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (eventName) {
                    case "UpdatedLykaGems":
                        updateReward(gemItem.lykagemDisplay, gemItem.progress + "", gemItem.max);
                        UserData.updateDisplayRewardCrystal(gemItem.lykagemDisplay);
                        UserData.updateRewardCrystal(gemItem.lykagem + "");
                        break;

                }

            }
        });
        Log.e("EEEEEEE", "PUSHER TO" + eventName);
    }

    private void openBirthdaySurprise() {
        startBirthdaySurpriseActivity("");
    }

    private void addProfileBadge(final int count) {
        BadgeUtils.setBadge(getContext(), count);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (count == 0) {
                    if (profileBadgeIV != null) {
                        profileBadgeIV.setVisibility(View.GONE);
                    }
                    return;
                }

                String newCount;

                if (count >= 100) {
                    newCount = "99+";
                } else {
                    newCount = String.valueOf(count);
                }

                int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 9, getContext().getResources().getDisplayMetrics());

                if (profileBadgeIV != null) {
                    profileBadgeIV.setVisibility(View.VISIBLE);
                    profileBadgeIV.setImageDrawable(new BadgeDrawable.Builder()
                            .type(BadgeDrawable.TYPE_ONLY_ONE_TEXT)
                            .number(count)
                            .badgeColor(0)
                            .text1(" " + newCount + " ")
                            .textSize(px)
                            .build());
                }
            }
        });
    }

    private void addWalletBadge(boolean walletBadge) {
        if (walletBadge == walletNotificationItem.data.walletNotification == true) {
            walletBadgeIV.setVisibility(View.VISIBLE);
        } else {
            walletBadgeIV.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onResponse(UnreadCountRequest.ServerResponse responseData) {
        UnreadNotificationTransformer unreadNotificationTransformer = responseData.getData(UnreadNotificationTransformer.class);
        if (unreadNotificationTransformer.status) {
            addProfileBadge(unreadNotificationTransformer.unread);
        } else {
            addProfileBadge(0);
            addWalletBadge(false);

        }

    }

//    @Subscribe
//    public void onResponse(WalletNotificationRequest.AllOrderResponse responseData){
//        SingleTransformer<WalletNotificationItem> singleTransformer = responseData.getData(SingleTransformer.class);
//        if(singleTransformer.data.data.walletNotification){
//
//        }
//    }


    public void setGemsListener(GemListener gemListener) {
        this.gemListener = gemListener;
    }

    public void setFeedListener(FeedListener feedListener) {
        this.feedListener = feedListener;
    }

    private GemListener gemListener;
    private FeedListener feedListener;

    public interface GemListener {
        void onGemsClick();

        void onFinalClick();
    }

    public interface FeedListener {
        void onFeedClick();
    }

    @Override
    public void onBackPressed() {
        int backStackCount = getBackStackCount();

        if (backStackCount > 1) {
            int index = backStackCount - 2;
            FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
            String tag = backEntry.getName();
            currentFragment = getSupportFragmentManager().findFragmentByTag(tag);
        }

        if (backStackCount <= 1) {
            if (isTaskRoot()) {
                exitConfirmation();
            } else {
                finish();
            }
        } else {
            super.onBackPressed();
        }
    }

    private void initRecently(){
        recentlyRequest = new RecentlyRequest(getContext());
        recentlyRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner,delivery_info,transaction")
                .showSwipeRefreshLayout(true)
                .setPerPage(10)
                .execute();
    }

    @Subscribe
    public void onResponse(RecentlyRequest.ServerResponse responseData) {
        ExploreTransformer suggestionTransformer = responseData.getData(ExploreTransformer.class);
        if(suggestionTransformer.status){
            recently = suggestionTransformer.data;
        }
    }

    private void initLikee(){
        likeeRequest = new LikeeRequest(getContext());
        likeeRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner,delivery_info,transaction")
                .addParameters(Keys.server.key.CATEGORY, "I Just Like It")
                .showSwipeRefreshLayout(true)
                .setPerPage(10).first();
    }

    @Subscribe
    public void onResponse(LikeeRequest.ServerResponse responseData) {
        ExploreTransformer suggestionTransformer = responseData.getData(ExploreTransformer.class);
        if(suggestionTransformer.status){
           likeee = suggestionTransformer.data;
        }
    }

    private void initTopUser(){
        trendingUserRequest = new TrendingUserRequest(getContext());
        trendingUserRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,statistic,social")
                .showSwipeRefreshLayout(true)
                .setPerPage(10).first();
    }

    @Subscribe
    public void onResponse(TrendingUserRequest.ServerResponse responseData) {
        TrendingUserTransformer suggestionTransformer = responseData.getData(TrendingUserTransformer.class);
        if(suggestionTransformer.status){
            topuser = suggestionTransformer.data;
        }
    }

    private void initHot(){
        hotRequest = new HotRequest(getContext());
        hotRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner,delivery_info,transaction")
                .showSwipeRefreshLayout(true)
                .setPerPage(10)
                .execute();
    }

    @Subscribe
    public void onResponse(HotRequest.ServerResponse responseData) {
        ExploreTransformer suggestionTransformer = responseData.getData(ExploreTransformer.class);
        if(suggestionTransformer.status){
            hot = suggestionTransformer.data;
        }
    }

    private void initMostRated(){
        highestRatedRequest = new HighestRatedRequest(getContext());
        highestRatedRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner,delivery_info,transaction")
                .showSwipeRefreshLayout(true)
                .setPerPage(10)
                .first();
    }

    @Subscribe
    public void onResponse(HighestRatedRequest.ServerResponse responseData) {
        ExploreTransformer suggestionTransformer = responseData.getData(ExploreTransformer.class);
        if(suggestionTransformer.status){
           highest = suggestionTransformer.data;
        }
    }

    private void initRecommended(){
        recommendedRequest = new RecommendedRequest(getContext());
        recommendedRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner,delivery_info,transaction")
                .showSwipeRefreshLayout(true)
                .setPerPage(10)
                .execute();
    }

    @Subscribe
    public void onResponse(RecommendedRequest.ServerResponse responseData) {
        ExploreTransformer suggestionTransformer = responseData.getData(ExploreTransformer.class);
        if(suggestionTransformer.status){
          recommended = suggestionTransformer.data;
        }
    }
}