package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.registration.ForgotPasswordFragment;
import com.thingsilikeapp.android.fragment.registration.RegisterAFragment;
import com.thingsilikeapp.android.fragment.registration.RegisterBFragment;
import com.thingsilikeapp.android.fragment.registration.RegisterBMerchantFragment;
import com.thingsilikeapp.android.fragment.registration.RegisterCFragment;
import com.thingsilikeapp.android.fragment.registration.RegisterDFragment;
import com.thingsilikeapp.android.fragment.registration.RegisterEFragment;
import com.thingsilikeapp.android.fragment.registration.RegisterFFragment;
import com.thingsilikeapp.android.fragment.registration.RegisterGFragment;
import com.thingsilikeapp.android.fragment.registration.RegisterHFragment;
import com.thingsilikeapp.android.fragment.registration.ResetPasswordFragment;
import com.thingsilikeapp.android.fragment.registration.SignUpFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class NewRegistrationActivity extends RouteActivity{
    public static final String TAG = NewRegistrationActivity.class.getName();


    @Override
    public int onLayoutSet() {
        return R.layout.activity_new_registration;
    }

    @Override
    public void onViewReady() {
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "start":
                openAFragment();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void openAFragment() { switchFragment(RegisterAFragment.newInstance()); }
    public void openBFragment() { switchFragment(RegisterBFragment.newInstance()); }
    public void openBMerchantFragment() { switchFragment(RegisterBMerchantFragment.newInstance()); }

    public void openCFragment(String username, String name) { switchFragment(RegisterCFragment.newInstance(username, name)); }

    public void openDFragment(String username, String gender, String name) { switchFragment(RegisterDFragment.newInstance(username, gender, name)); }

    public void openEFragment(String username, String gender, String birthdate, String name) { switchFragment(RegisterEFragment.newInstance(username, gender, birthdate, name)); }

    public void openFFragment(String username, String gender, String birthdate, String name) { switchFragment(RegisterFFragment.newInstance(username, gender, birthdate, name)); }

    public void openGFragment(String username, String gender, String birthdate, String name, String email, String type, String country, String code) { switchFragment(RegisterGFragment.newInstance(username, gender, birthdate, name, email, type, country, code )); }

    public void openHFragment(String username, String gender, String birthdate, String name, String email, String password, String type, String country, String code) { switchFragment(RegisterHFragment.newInstance(username, gender, birthdate, name, email, password, type, country, code)); }

}
