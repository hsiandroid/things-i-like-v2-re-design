package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.comment.CommentFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class CommentActivity extends RouteActivity implements View.OnClickListener{
    public static final String TAG = CommentActivity.class.getName();

    @BindView(R.id.mainTitleTXT)        TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)    View backButtonIV;
    @BindView(R.id.feedsBTN)            View feedsBTN;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_comment;
    }

    @Override
    public void onViewReady() {
        backButtonIV.setOnClickListener(this);
        feedsBTN.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        openCommentFragment(getFragmentBundle().getInt("wishListID"), getFragmentBundle().getString("wishListName"));
    }

    public void openCommentFragment(int wishListID, String wishListName) {
        switchFragment(CommentFragment.newInstance(wishListID, wishListName));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;
            case R.id.feedsBTN:
                startMainActivity("home");
                break;
        }
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }
}
