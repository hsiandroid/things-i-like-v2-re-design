package com.thingsilikeapp.android.activity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.search.FriendsFragment;
import com.thingsilikeapp.android.fragment.search.LikesFragment;
import com.thingsilikeapp.android.fragment.search.RepostFragment;
import com.thingsilikeapp.android.fragment.search.SearchUserFragment;
import com.thingsilikeapp.android.fragment.search.SocialFragment;
import com.thingsilikeapp.android.fragment.search.ViewFacebookFriendsFragment;
import com.thingsilikeapp.android.fragment.search.ViewSearchFragment;
import com.thingsilikeapp.android.fragment.search.ViewTopUserFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class SearchActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = SearchActivity.class.getName();

    @BindView(R.id.mainTitleTXT)        TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)    View mainBackButtonIV;

    @BindView(R.id.searchBTN)           View searchBTN;
    @BindView(R.id.settingsBTN)         View settingsBTN;
    @BindView(R.id.feedsBTN)            View feedsBTN;
    @BindView(R.id.feeds1BTN)            View feeds1BTN;

    @BindView(R.id.titleCON)            View titleCON;
    @BindView(R.id.searchCON)           View searchCON;
    @BindView(R.id.searchBackBTN)       View searchBackBTN;
    @BindView(R.id.searchET)            EditText searchET;
    @BindView(R.id.clearBTN)            ImageView clearBTN;

    private Timer timer;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_search;
    }

    @Override
    public void onViewReady() {
        mainBackButtonIV.setOnClickListener(this);
        searchBTN.setOnClickListener(this);
        settingsBTN.setOnClickListener(this);
        feedsBTN.setOnClickListener(this);
        feeds1BTN.setOnClickListener(this);
        clearBTN.setOnClickListener(this);
        searchBackBTN.setOnClickListener(this);
        initSearch();
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "search_user":
                openSearchUserFragment();
                break;
            case "followers":
                openFollowersFragment(getFragmentBundle().getInt("user_id"), getFragmentBundle().getString("user_name"));
                break;
            case "following":
                openFollowingFragment(getFragmentBundle().getInt("user_id"), getFragmentBundle().getString("user_name"));
                break;
            case "all":
                openTopUserFragment();
                break;
            case "fb_friends":
                openFacebookFriendsFragment();
                break;
            case "search":
                openSearchFragment();
                break;
            case "likes":
                openLikesFragment(getFragmentBundle().getInt("user_id"));
                break;
            case "repost":
                openRepostFragment(getFragmentBundle().getInt("user_id"));
                break;
        }
    }

    public void openSearchUserFragment(){
        switchFragment(SearchUserFragment.newInstance());
    }

    public void openFriendsFragment(){
        switchFragment(FriendsFragment.newInstance());
    }

    public void openFacebookFriendsFragment(){
        switchFragment(ViewFacebookFriendsFragment.newInstance());
    }
    public void openSearchFragment(){
        switchFragment(ViewSearchFragment.newInstance());
    }
    public void openTopUserFragment(){
        switchFragment(ViewTopUserFragment.newInstance());
    }

    public void openFollowersFragment(int id, String name){
        switchFragment(SocialFragment.newInstance(id, name, 1));
    }

    public void openFollowingFragment(int id,String name){
        switchFragment(SocialFragment.newInstance(id, name, 2));
    }

    public void openLikesFragment(int id){
            switchFragment(LikesFragment.newInstance(id));
    }

    public void openRepostFragment(int id){
            switchFragment(RepostFragment.newInstance(id));
    }

    public void showSearchContainer(boolean b){
        if(b){
            titleCON.setVisibility(View.GONE);
            searchCON.setVisibility(View.VISIBLE);
            searchBTN.setVisibility(View.VISIBLE);
        }else{
            titleCON.setVisibility(View.VISIBLE);
            searchCON.setVisibility(View.GONE);
            searchBTN.setVisibility(View.GONE);
        }
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                onBackPressed();
                break;
            case R.id.searchBackBTN:
                onBackPressed();
                break;
            case R.id.feedsBTN:
            case R.id.feeds1BTN:
                startMainActivity("home");
                break;
            case R.id.clearBTN:
                searchET.setText("");
                break;
        }
    }

    public EditText getSearchEditText(){
        return searchET;
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (timer != null) {
            timer.cancel();
        }
    }

    public void initSearch(){
        if(searchET != null){
            searchET.setHint("Search people...");
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (timer != null) {
                        timer.cancel();
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if(searchListener != null){
                                if(!searchET.getText().toString().equals("") || !searchET.getText().toString().equals(null)){
                                    searchListener.searchTextChange(searchET.getText().toString());
                                }
                            }
                        }
                    }, 300);
                }
            });
        }
    }

    public void setSearchListener(SearchListener searchListener){
        this.searchListener = searchListener;
    }

    private SearchListener searchListener;

    public interface SearchListener{
        void searchTextChange(String keyword);
    }
}
