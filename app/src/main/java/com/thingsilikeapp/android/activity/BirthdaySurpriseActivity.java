package com.thingsilikeapp.android.activity;

import android.os.Bundle;
import android.util.DisplayMetrics;

import com.google.gson.Gson;
import com.thingsilikeapp.android.dialog.BirthdayVideoV1Dialog;
import com.thingsilikeapp.android.dialog.GiftCardSurprisePreviewV1Dialog;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.RouteManager;
import com.thingsilikeapp.vendor.android.java.SoundManager;
import com.thingsilikeapp.vendor.android.java.andengine.ResourcesManager;
import com.thingsilikeapp.vendor.android.java.andengine.SceneManager;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.FixedStepEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.util.FPSLogger;
import org.andengine.ui.activity.BaseGameActivity;

import java.io.IOException;

public class BirthdaySurpriseActivity extends BaseGameActivity implements
        BirthdayVideoV1Dialog.OnVideoFinished,
        GiftCardSurprisePreviewV1Dialog.Callback{

    public static int CAMERA_WIDTH = 720;
    public static int CAMERA_HEIGHT = 1280;
    private Camera camera;

    private int lastVolume;
    private boolean isBalloonPlayed;
    public Music balloonMusic;
    private int cameraId = -1;

    @Override
    public Engine onCreateEngine(EngineOptions pEngineOptions) {
        return new FixedStepEngine(pEngineOptions, 30);
    }

    @Override
    protected synchronized void onResume() {
        super.onResume();
//        if(isGameLoaded()){
//            if ( balloonMusic != null && isBalloonPlayed) {
//                playBalloonMusic();
//            }
//        }
        openGreetingActivity();
//        openGreetingCard();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(isGameLoaded()){
            if ( balloonMusic != null ) {
                balloonMusic.pause();
                SoundManager.clearHandler();
            }
        }
    }

    public void playBalloonMusic(){
        isBalloonPlayed = true;

        balloonMusic.play();
        SoundManager.fadInSoundVolume(this);
    }

    public void stopBalloonMusic(){
        if(balloonMusic != null){
            isBalloonPlayed = true;
            balloonMusic.stop();
        }
    }

    @Override
    public EngineOptions onCreateEngineOptions() {
        camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
        EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.PORTRAIT_FIXED,
                new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), camera);
        engineOptions.getAudioOptions().setNeedsMusic(true).setNeedsSound(true);
        engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
        return engineOptions;
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {

        balloonMusic = MusicFactory.createMusicFromAsset(mEngine.getMusicManager(), this,"audio/balloon.mp3");
        balloonMusic.setLooping(true);

        SoundManager.volume(this, 0);

        ResourcesManager.prepareManager(mEngine, this, camera, getVertexBufferObjectManager());
        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    @Override
    public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) {
        this.mEngine.registerUpdateHandler(new FPSLogger());
        BirthdayVideoV1Dialog.newInstance(this, pOnCreateSceneCallback, this).show(getFragmentManager(), BirthdayVideoV1Dialog.TAG);
//        SceneManager.getInstance().loadBalloonScene(pOnCreateSceneCallback);
    }

    @Override
    public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) {
        pOnPopulateSceneCallback.onPopulateSceneFinished();
    }

    public int getScreenWidth(){
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }

    public int getScreenHeight(){
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.heightPixels;
    }

    @Override
    public void onCompletion(OnCreateSceneCallback pOnCreateSceneCallback) {
        int greetingCount = UserData.getInt(UserData.GREETINGS);
        if(greetingCount > 0){
            SceneManager.getInstance().loadBalloonScene(pOnCreateSceneCallback);
        }else{
            openGreetingActivity();
        }
    }

    private void openGreetingActivity(){
        EventItem eventItem = new EventItem();
        Bundle bundle = new Bundle();
        bundle.putString("event_item", new Gson().toJson(eventItem));
        RouteManager.Route.with(this)
                .addActivityClass(BirthdayActivity.class)
                .addActivityTag("item")
                .addFragmentTag("greeting")
                .addFragmentBundle(bundle)
                .startActivity();
        finish();
    }

    private GiftCardSurprisePreviewV1Dialog giftCardSurprisePreviewDialog;

    public void openGreetingCard(){
        if(giftCardSurprisePreviewDialog == null){
            giftCardSurprisePreviewDialog = GiftCardSurprisePreviewV1Dialog.newInstance(this);
            giftCardSurprisePreviewDialog.show(getFragmentManager(), GiftCardSurprisePreviewV1Dialog.TAG);
        }
    }

    @Override
    public void onClose() {
        giftCardSurprisePreviewDialog = null;
        if (UserData.getInt(UserData.GREETINGS) == 0){
            openGreetingActivity();
        }
    }
}
