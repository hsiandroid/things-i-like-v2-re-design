package com.thingsilikeapp.android.activity;

import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.editor.CreateGreetingsFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class ImageEditorActivity extends RouteActivity implements View.OnClickListener{
    public static final String TAG = ImageEditorActivity.class.getName();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_image_editor;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "create":
                openCreateGreetingsFragment(getFragmentBundle().getString("event_item"), getFragmentBundle().getString("user_item"));
                break;
        }
    }

    public void openCreateGreetingsFragment(String event, String user) {
        switchFragment(CreateGreetingsFragment.newInstance(event, user));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                onBackPressed();
                break;
            case R.id.feedsBTN:
                startMainActivity("home");
                break;
        }
    }

//    public void setTitle(String title){
//        mainTitleTXT.setText(title);
//    }


}
