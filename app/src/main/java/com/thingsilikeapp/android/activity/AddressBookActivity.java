package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.addressbook.AddressBookCreateFragment;
import com.thingsilikeapp.android.fragment.addressbook.AddressBookEditFragment;
import com.thingsilikeapp.android.fragment.addressbook.AddressBookFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class AddressBookActivity extends RouteActivity implements View.OnClickListener {

    @BindView(R.id.mainTitleTXT)        TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)    ImageView mainBackButtonIV;
    @BindView(R.id.feedsBTN)            View feedsBTN;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_address_book;
    }

    @Override
    public void onViewReady() {
        mainBackButtonIV.setOnClickListener(this);
        feedsBTN.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "all":
                openAddressBookFragment();
                break;
            case "create":
                openAddressBookCreateFragment();
                break;
            case "edit":
                openAddressBookEditFragment(getFragmentBundle().getString("address_raw"));
                break;
            default:
                openAddressBookFragment();
        }
    }

    public void openAddressBookFragment(){
        switchFragment(AddressBookFragment.newInstance());
    }

    public void openAddressBookCreateFragment(){
        switchFragment(AddressBookCreateFragment.newInstance());
    }

    public void openAddressBookEditFragment(String addressRaw){
        switchFragment(AddressBookEditFragment.newInstance(addressRaw));
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;
            case R.id.feedsBTN:
                startMainActivity("home");
                break;
        }
    }
}
