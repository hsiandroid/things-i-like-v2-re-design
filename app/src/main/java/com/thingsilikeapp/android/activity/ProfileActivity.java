package com.thingsilikeapp.android.activity;

import android.content.Intent;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.comment.CommentFragment;
import com.thingsilikeapp.android.fragment.main.NotificationFragment;
import com.thingsilikeapp.android.fragment.profile.ProfileeFragment;
import com.thingsilikeapp.android.fragment.profile.VisitingProfileFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class ProfileActivity extends RouteActivity implements View.OnClickListener{


    @Override
    public void onViewReady() {

    }

    @Override
    public int onLayoutSet() {
        return R.layout.activity_profile;
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "visit":
                openVisitingProfileFragment(getFragmentBundle().getInt("user_id"));
                break;
            case "profile":
                openProfileFragment();
                break;

        }

    }

    public void openVisitingProfileFragment(int id){
        switchFragment(VisitingProfileFragment.newInstance(id));
    }

    public void openProfileFragment(){
        switchFragment(ProfileeFragment.newInstance());
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;

        }
    }


}
