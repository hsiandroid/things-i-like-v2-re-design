package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.birthday.CelebrantFragment;
import com.thingsilikeapp.android.fragment.birthday.DefaultEventFragment;
import com.thingsilikeapp.android.fragment.birthday.GreetingFragment;
import com.thingsilikeapp.android.fragment.birthday.TodayCelebrantFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class BirthdayActivity extends RouteActivity implements View.OnClickListener{
    public static final String TAG = BirthdayActivity.class.getName();

    @BindView(R.id.mainTitleTXT)            TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)        View backButtonIV;
    @BindView(R.id.feedsBTN)                View feedsBTN;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_birthday;
    }

    @Override
    public void onViewReady() {
        backButtonIV.setOnClickListener(this);
        feedsBTN.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "celebrant":
                openCelebrantFragment();
                break;
            case "today_celebrant":
                openTodayCelebrantFragment();
                break;
            case "greeting":
                openGreetingFragment(getFragmentBundle().getString("event_item"));
                break;
            case "event":
                openDefaultEventFragment(getFragmentBundle().getString("event_item"));
                break;
        }
    }

    public void openCelebrantFragment() {
        switchFragment(CelebrantFragment.newInstance());
    }

    public void openTodayCelebrantFragment() {
        switchFragment(TodayCelebrantFragment.newInstance());
    }

    public void openGreetingFragment(String event) {
        switchFragment(GreetingFragment.newInstance(event));
    }

    public void openDefaultEventFragment(String event) {
        switchFragment(DefaultEventFragment.newInstance(event));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                onBackPressed();
                break;
            case R.id.feedsBTN:
                startMainActivity("home");
                break;
        }
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }
}
