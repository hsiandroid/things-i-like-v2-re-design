package com.thingsilikeapp.android.activity;

import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.fragment.wishlist.CreatePostFragment;
import com.thingsilikeapp.android.fragment.wishlist.EditAddressFragment;
import com.thingsilikeapp.android.fragment.wishlist.EditWishListFragment;
import com.thingsilikeapp.android.fragment.wishlist.RePostFragment;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class WishListActivity extends RouteActivity implements View.OnClickListener{
    public static final String TAG = WishListActivity.class.getName();

    @BindView(R.id.mainTitleTXT)        TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)    View mainBackButtonIV;
    @BindView(R.id.feedsBTN)            View feedsBTN;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_wishlist;
    }

    @Override
    public void onViewReady() {
        mainBackButtonIV.setOnClickListener(this);
        feedsBTN.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName) {
            case "create":
                openCreatePostFragment();
                break;
            case "create_grab_other":
//                openCreatePostOtherFragment((WishListItem) getFragmentBundle().getParcelable("wishlist_item"));
                WishListItem wishlist_item= new Gson().fromJson(getFragmentBundle().getString("post_content"), WishListItem.class);
                openCreatePostOtherFragment(wishlist_item);
                break;
            case "edit":
                WishListItem wishlistitem= new Gson().fromJson(getFragmentBundle().getString("post_content"), WishListItem.class);
                openEditPostFragment(wishlistitem);
                break;
            case "address":
                WishListItem wishlistitems= new Gson().fromJson(getFragmentBundle().getString("post_content"), WishListItem.class);
                openEditAddressFragment(wishlistitems);
                break;
        }
    }

    public void openCreatePostFragment() {
        switchFragment(CreatePostFragment.newInstance());
    }

    public void openCreatePostOtherFragment(WishListItem wishListItem) {
        switchFragment(RePostFragment.newInstance(wishListItem));
    }

    public void openEditPostFragment(WishListItem wishListItem) {
        switchFragment(EditWishListFragment.newInstance(wishListItem));
    }
    public void openEditAddressFragment(WishListItem wishListItem) {
        switchFragment(EditAddressFragment.newInstance(wishListItem));
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                Keyboard.hideKeyboard(this);
                onBackPressed();
                break;
            case R.id.feedsBTN:
                startMainActivity("home");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(onBackPressedListener != null){
            int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
            if ( backStackCount <= 1 ) {
                onBackPressedListener.onBackPressed();
            } else {
                super.onBackPressed();
            }
        }else{
            super.onBackPressed();
        }
    }

    public onBackPressedListener onBackPressedListener;
    public void setOnBackPressedListener(WishListActivity.onBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    public interface onBackPressedListener{
        void onBackPressed();
    }
}
