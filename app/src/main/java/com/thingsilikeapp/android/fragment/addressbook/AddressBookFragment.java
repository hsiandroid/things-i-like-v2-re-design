package com.thingsilikeapp.android.fragment.addressbook;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.AddressBookActivity;
import com.thingsilikeapp.android.adapter.AddressBookRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.AddressBookOtherOptionDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.AddressBookItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.address.AllAddressRequest;
import com.thingsilikeapp.server.request.address.DefaultAddressRequest;
import com.thingsilikeapp.server.request.address.DeleteAddressRequest;
import com.thingsilikeapp.server.transformer.CollectionTransformer;
import com.thingsilikeapp.server.transformer.SingleTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class AddressBookFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener, View.OnClickListener,
        AddressBookRecycleViewAdapter.ClickListener,AddressBookOtherOptionDialog.AddressBookCallBack{

    public static final String TAG = AddressBookFragment.class.getName();

    private AddressBookActivity addressBookActivity;

    private AddressBookRecycleViewAdapter addressBookRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private AllAddressRequest allAddressRequest;

    @BindView(R.id.addressBookSRL)      SwipeRefreshLayout addressBookSRL;
    @BindView(R.id.addressBookRV)       RecyclerView addressBookRV;
    @BindView(R.id.addBTN)              TextView addBTN;
    @BindView(R.id.placeHolderCON)      View placeHolderCON;


    public static AddressBookFragment newInstance() {
        AddressBookFragment addressBookFragment = new AddressBookFragment();
        return addressBookFragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_address_book;
    }

    @Override
    public void onViewReady() {
        addressBookActivity = (AddressBookActivity) getActivity();
        addressBookActivity.setTitle(getString(R.string.address_book_title));

        addressBookSRL.setOnRefreshListener(this);

        addBTN.setOnClickListener(this);

        setUpAddressBookListView();
        initAPICall();

        Analytics.trackEvent("addressbook_AddressBookFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void setUpAddressBookListView(){

        addressBookSRL.setColorSchemeResources(R.color.colorPrimary);
        addressBookSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        addressBookSRL.setOnRefreshListener(this);

        addressBookRecycleViewAdapter = new AddressBookRecycleViewAdapter(getContext());
        addressBookRecycleViewAdapter.setOnItemClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        addressBookRV.setLayoutManager(linearLayoutManager);
        addressBookRV.setAdapter(addressBookRecycleViewAdapter);
    }

    private void initAPICall(){
        allAddressRequest = new AllAddressRequest(getContext());
        allAddressRequest
                .setSwipeRefreshLayout(addressBookSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(20);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void refreshList(){
        allAddressRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addBTN:
                addressBookActivity.startAddressBookActivity("", "create");
                Analytics.trackEvent("addressbook_AddressBookFragment_addBTN");
                break;
        }
    }

    @Override
    public void onItemClick(AddressBookItem addressBookItem) {
        addressBookActivity.startAddressBookActivity(addressBookItem.toString(), "edit");
    }

    @Override
    public void onItemLongClick(AddressBookItem addressBookItem) {
        AddressBookOtherOptionDialog.newInstance(addressBookItem, this).show(getChildFragmentManager(),AddressBookOtherOptionDialog.TAG);
    }

    @Override
    public void onClick(AddressBookItem addressBookItem) {
        AddressBookOtherOptionDialog.newInstance(addressBookItem, this).show(getChildFragmentManager(),AddressBookOtherOptionDialog.TAG);
    }

    @Override
    public void editAddress(AddressBookItem addressBookItem) {
        addressBookActivity.startAddressBookActivity(addressBookItem.toString(), "edit");
    }

    @Override
    public void deleteAddress(AddressBookItem addressBookItem) {
        deleteCommentDialog(addressBookItem.id);
    }

    @Override
    public void setDefaultAddressComment(AddressBookItem addressBookItem) {
        attemptSetDefault(addressBookItem.id);
    }

    private void deleteCommentDialog(final int id){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Address Book")
                .setNote("Are you sure you want to remove this address book?")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Remove")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        attemptDelete(id);
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        confirmationDialog.dismiss();
                    }
                })
                .build(getChildFragmentManager());
        Analytics.trackEvent("addressbook_AddressBookFragment_removeAddressBookDialog");

    }

    private void attemptDelete(int id){
        DeleteAddressRequest editAddressRequest = new DeleteAddressRequest(getContext());
        editAddressRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Deleting address...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.ADDRESS_ID, id)
                .execute();
    }

    private void attemptSetDefault(int id){
        DefaultAddressRequest defaultAddressRequest = new DefaultAddressRequest(getContext());
        defaultAddressRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.ADDRESS_ID, id)
                .execute();
    }

    @Subscribe
    public void onResponse(AllAddressRequest.ServerResponse responseData) {
        CollectionTransformer<AddressBookItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                addressBookRecycleViewAdapter.addNewData(collectionTransformer.data);
            }else{
                addressBookRecycleViewAdapter.setNewData(collectionTransformer.data);
            }
        }
        if (addressBookRecycleViewAdapter.getItemCount() == 0){
            placeHolderCON.setVisibility(View.VISIBLE);
            addressBookRV.setVisibility(View.GONE);
        }else {
            placeHolderCON.setVisibility(View.GONE);
            addressBookRV.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onResponse(DeleteAddressRequest.ServerResponse responseData) {
        SingleTransformer<AddressBookItem, AddressBookItem.Errors> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            addressBookRecycleViewAdapter.removeData(singleTransformer.data);
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(DefaultAddressRequest.ServerResponse responseData) {
        SingleTransformer<AddressBookItem, AddressBookItem.Errors> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            addressBookRecycleViewAdapter.setItemDefault(singleTransformer.data.id);
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}
