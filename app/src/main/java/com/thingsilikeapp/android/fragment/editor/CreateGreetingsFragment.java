package com.thingsilikeapp.android.fragment.editor;

import android.Manifest;
import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.data.model.BackgroundItem;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.FrameItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.request.gift.BackgroundRequest;
import com.thingsilikeapp.server.request.gift.FrameRequest;
import com.thingsilikeapp.server.request.gift.SendGreetRequest;
import com.thingsilikeapp.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;
import com.thingsilikeapp.vendor.android.java.CameraPreview;
import com.thingsilikeapp.vendor.android.java.ImageQualityManager;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.widget.image.FixedWidthImageView;
import com.thingsilikeapp.android.activity.ImageEditorActivity;
import com.thingsilikeapp.android.adapter.EditorBackgroundRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.EditorFrameRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import butterknife.BindView;
import icepick.State;

public class CreateGreetingsFragment extends BaseFragment implements
        View.OnClickListener,
        EditorFrameRecycleViewAdapter.ClickListener,
        EditorBackgroundRecycleViewAdapter.ClickListener{

    public static final String TAG = CreateGreetingsFragment.class.getName();

    private ImageEditorActivity imageEditorActivity;

    private EditorFrameRecycleViewAdapter editorFrameRecycleViewAdapter;
    private LinearLayoutManager frameLayoutManager;

    private EditorBackgroundRecycleViewAdapter editorBackgroundRecycleViewAdapter;
    private LinearLayoutManager backgroundLayoutManager;

    private CameraPreview cameraPreview;

    private static final int PERMISSION_GALLERY = 1;
    private static final int PERMISSION_CAMERA = 2;

    @BindView(R.id.messageBTN)          View messageBTN;
    @BindView(R.id.frameBTN)            View frameBTN;
    @BindView(R.id.backgroundBTN)       View backgroundBTN;
    @BindView(R.id.sendBTN)             View sendBTN;
    @BindView(R.id.frameIMG)            FixedWidthImageView frameIMG;
    @BindView(R.id.cameraFL)            FrameLayout cameraFL;
    @BindView(R.id.cameraActionCON)     View cameraActionCON;
    @BindView(R.id.cameraActionBTN)     View cameraActionBTN;
    @BindView(R.id.changeImageBTN)      View changeImageBTN;
    @BindView(R.id.backgroundPB)        View backgroundPB;

    @BindView(R.id.canvasCON)           FrameLayout canvasCON;

    // Message Control
    @BindView(R.id.textControl)         View textControl;
    @BindView(R.id.textEditorCON)       View textEditorCON;

    @BindView(R.id.greetingET)          EditText greetingET;
    @BindView(R.id.messageET)           EditText messageET;
    @BindView(R.id.closingET)           EditText closingET;

    @BindView(R.id.greetingTXT)         TextView greetingTXT;
    @BindView(R.id.messageTXT)          TextView messageTXT;
    @BindView(R.id.closingTXT)          TextView closingTXT;
    @BindView(R.id.imageCIV)            ImageView imageCIV;
    @BindView(R.id.nameTXT)             TextView nameTXT;

    // Frames Control
    @BindView(R.id.frameControl)        View frameControl;
    @BindView(R.id.framesRV)            RecyclerView framesRV;

    // Background Control
    @BindView(R.id.backgroundControl)   View backgroundControl;
    @BindView(R.id.backgroundRV)        RecyclerView backgroundRV;
    @BindView(R.id.backgroundIMG)       ImageView backgroundIMG;

    @BindView(R.id.textBTN)             TextView textBTN;
    @BindView(R.id.cameraBTN)           TextView cameraBTN;

    @State String raw;
    @State String eventRaw;

    private UserItem userItem;
    private EventItem eventItem;

    private String DEFAULT_GREETINGS = "To my dear friend,";
    private String DEFAULT_MESSAGE = "May today be filled with sunshine and smiles, laughter and love. ( Icing and Cake :)\n Happy Birthday!";
    private String DEFAULT_CLOSING = "Truly yours";

    public static CreateGreetingsFragment newInstance(String eventRaw, String raw) {
        CreateGreetingsFragment createGreetingsFragment = new CreateGreetingsFragment();
        createGreetingsFragment.raw = raw;
        createGreetingsFragment.eventRaw = eventRaw;
        return createGreetingsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_create_greetings;
    }

    @Override
    public void onViewReady() {
        imageEditorActivity = (ImageEditorActivity) getContext();

        messageBTN.setOnClickListener(this);
        frameBTN.setOnClickListener(this);
        backgroundBTN.setOnClickListener(this);
        canvasCON.setOnClickListener(this);
        sendBTN.setOnClickListener(this);
        textBTN.setOnClickListener(this);
        cameraBTN.setOnClickListener(this);
        cameraActionBTN.setOnClickListener(this);
        changeImageBTN.setOnClickListener(this);

        userItem = new Gson().fromJson(raw, UserItem.class);
        eventItem = new Gson().fromJson(eventRaw, EventItem.class);

        textActiveControl();
        setFrameControl();
        setBackgroundControl();
        setTextButton();
        fixCanvasSize();

        attemptGetFrames();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(cameraBTN.isSelected()){
            setCameraButton();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.messageBTN:
                showTextControl(!textControl.isShown());
                showFrameControl(false);
                showBackgroundControl(false);
                break;
            case R.id.frameBTN:
                showTextControl(false);
                showFrameControl(!frameControl.isShown());
                showBackgroundControl(false);
                Keyboard.hideKeyboard(getContext());
                break;
            case R.id.backgroundBTN:
                showTextControl(false);
                showFrameControl(false);
                showBackgroundControl(!backgroundControl.isShown());
                Keyboard.hideKeyboard(getContext());
                break;
            case R.id.canvasCON:
                hideAllControls();
                Keyboard.hideKeyboard(getContext());
                break;
            case R.id.sendBTN:
                hideAllControls();
                attemptSend();
                break;
            case R.id.textBTN:
                setTextButton();
                break;
            case R.id.cameraBTN:
                setCameraButton();
                break;
            case R.id.changeImageBTN:
                changePicture();
                break;
            case R.id.cameraActionBTN:
                pauseCamera();
                break;
        }
    }

    private void setTextButton(){
        hideAllControls();
        resetBackground();

        messageBTN.setEnabled(true);
        backgroundBTN.setEnabled(true);

        textEditorCON.setVisibility(View.VISIBLE);

        textBTN.setSelected(true);
        cameraBTN.setSelected(false);
        cameraFL.setVisibility(View.GONE);
        cameraActionCON.setVisibility(View.GONE);
        changeImageBTN.setVisibility(View.GONE);
        stopCamera();
    }

    private void setCameraButton(){
        hideAllControls();

        messageBTN.setEnabled(false);
        backgroundBTN.setEnabled(false);

        textEditorCON.setVisibility(View.GONE);

        textBTN.setSelected(false);
        cameraBTN.setSelected(true);
        cameraActionCON.setVisibility(View.VISIBLE);
        startCamera();

    }

    private void hideAllControls(){
        showTextControl(false);
        showFrameControl(false);
        showBackgroundControl(false);
    }

    private void showTextControl(boolean show){
        messageBTN.setSelected(show);
        if(show){
            textControl.setVisibility(View.VISIBLE);
            textEditorCON.setBackgroundColor(ActivityCompat.getColor(getContext(), R.color.white_gray));
        }else{
            textControl.setVisibility(View.GONE);
            textEditorCON.setBackgroundColor(ActivityCompat.getColor(getContext(), android.R.color.transparent));
        }
    }

    private void textActiveControl(){
        nameTXT.setText(UserData.getUserItem().common_name);

        Glide.with(getContext())
                .load(UserData.getUserItem().getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(imageCIV);

        cloneTextView(greetingET, greetingTXT);
        cloneTextView(messageET, messageTXT);
        cloneTextView(closingET, closingTXT);

        greetingTXT.setHint(DEFAULT_GREETINGS);
        messageTXT.setHint(DEFAULT_MESSAGE);
        closingTXT.setHint(DEFAULT_CLOSING);
    }

    private void cloneTextView(EditText editText, final TextView textBTN){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textBTN.setText(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void showFrameControl(boolean show){
        frameBTN.setSelected(show);
        frameControl.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void setFrameControl(){
        editorFrameRecycleViewAdapter = new EditorFrameRecycleViewAdapter(getContext());
        editorFrameRecycleViewAdapter.setOnItemClickListener(this);
        frameLayoutManager = new LinearLayoutManager(getContext());
        frameLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        framesRV.setLayoutManager(frameLayoutManager);
        framesRV.setAdapter(editorFrameRecycleViewAdapter);
    }

    private void showBackgroundControl(boolean show){
        backgroundBTN.setSelected(show);
        backgroundControl.setVisibility(show ? View.VISIBLE : View.GONE);

        if(editorBackgroundRecycleViewAdapter.getItemCount() == 0){
            attemptGetBackground();
        }
    }

    private void setBackgroundControl(){
        editorBackgroundRecycleViewAdapter = new EditorBackgroundRecycleViewAdapter(getContext());
        editorBackgroundRecycleViewAdapter.setOnItemClickListener(this);
        backgroundLayoutManager = new LinearLayoutManager(getContext());
        backgroundLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        backgroundRV.setLayoutManager(backgroundLayoutManager);
        backgroundRV.setAdapter(editorBackgroundRecycleViewAdapter);
    }

    private void attemptSend(){
        canvasCON.setDrawingCacheEnabled(true);
        canvasCON.buildDrawingCache();

        if(PermissionChecker.checkPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_GALLERY)){
            confirmSubmission();
        }
    }

    private void confirmSubmission(){

        String pronoun = "his/her";
        if(userItem != null){
            pronoun = userItem.pronoun("his", "her");
        }

        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Submit now?")
                .setNote("Your greeting will be delivered on "+ pronoun + " actual birthday.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Submit")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getBitmap();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
    }

    private void getBitmap(){
        final ProgressDialog progressDialog = new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false);
        ImageQualityManager.getFileFromBitmap(
                getContext(),
                canvasCON.getDrawingCache(),
                new ImageQualityManager.Callback() {
                    @Override
                    public void success(File file) {
                        progressDialog.cancel();
                        sendNow(file);
                    }
                });
    }

    private void sendNow(File file){
        SendGreetRequest sendGreetRequest = new SendGreetRequest(getContext());
        sendGreetRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Sending...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addMultipartBody(Keys.server.key.FILE, file)
                .addMultipartBody(Keys.server.key.USER_ID, userItem.id)
                .addMultipartBody(Keys.server.key.EVENT_ID, eventItem.id)
                .execute();
    }

    private void startCamera(){
        if(PermissionChecker.checkPermissions(getActivity(), new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CAMERA)){
            resetBackground();
            cameraFL.removeAllViews();
            cameraPreview = new CameraPreview(getContext());
            cameraFL.addView(cameraPreview);
            changeImageBTN.setVisibility(View.GONE);
            cameraActionCON.setVisibility(View.VISIBLE);
            cameraFL.setVisibility(View.VISIBLE);
            backgroundIMG.setVisibility(View.GONE);
        }
    }

    private void pauseCamera(){
        changeImageBTN.setVisibility(View.VISIBLE);
        cameraActionCON.setVisibility(View.INVISIBLE);

        if(cameraPreview != null){
            cameraPreview.saveBitmap(backgroundIMG, cameraFL);
        }
    }

    private void changePicture(){

        changeImageBTN.setVisibility(View.GONE);
        cameraActionCON.setVisibility(View.VISIBLE);
        cameraFL.setVisibility(View.VISIBLE);
        backgroundIMG.setVisibility(View.GONE);

        if(cameraPreview != null){
            cameraPreview.startPreview();
        }else{
            startCamera();
        }
    }

    private void stopCamera(){
        changeImageBTN.setVisibility(View.INVISIBLE);
        cameraActionCON.setVisibility(View.INVISIBLE);
        if(cameraPreview != null){
            cameraPreview.stopPreviewAndFreeCamera();
            cameraFL.removeAllViews();
            cameraFL.setVisibility(View.GONE);
            backgroundIMG.setVisibility(View.VISIBLE);
        }
    }

    private void resetBackground(){
        Glide.with(getContext())
                .load(R.color.white)
                .into(backgroundIMG);
    }

    private void fixCanvasSize(){
        frameIMG.autoResize(true);
        frameIMG.post(new Runnable() {
            @Override
            public void run() {
                if(canvasCON != null){
                    canvasCON.getLayoutParams().height = frameIMG.getHeight();
                }
            }
        });
    }

    private void attemptGetFrames(){
        FrameRequest frameRequest = new FrameRequest(getContext());
        frameRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.USER_ID, userItem.id)
                .addParameters(Keys.server.key.EVENT_ID, eventItem.id)
                .execute();
    }

    private void attemptGetBackground(){
        backgroundPB.setVisibility(View.VISIBLE);
        BackgroundRequest backgroundRequest = new BackgroundRequest(getContext());
        backgroundRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.USER_ID, userItem.id)
                .addParameters(Keys.server.key.EVENT_ID, eventItem.id)
                .execute();
    }

    @Subscribe
    public void onResponse(SendGreetRequest.ServerResponse responseData) {
        BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            imageEditorActivity.finish();
        }else{
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(FrameRequest.ServerResponse responseData) {
        CollectionTransformer<FrameItem> baseTransformer = responseData.getData(CollectionTransformer.class);
        if(baseTransformer.status){
            editorFrameRecycleViewAdapter.setNewData(baseTransformer.data);
            if(baseTransformer.data.size() > 0){
                onFrameSelected(baseTransformer.data.get(0));
            }
        }else{
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
            imageEditorActivity.finish();
        }
    }

    @Subscribe
    public void onResponse(BackgroundRequest.ServerResponse responseData) {

        backgroundPB.setVisibility(View.GONE);

        CollectionTransformer<BackgroundItem> baseTransformer = responseData.getData(CollectionTransformer.class);
        if(baseTransformer.status){
            editorBackgroundRecycleViewAdapter.setNewData(baseTransformer.data);
        }else{
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
            imageEditorActivity.finish();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_GALLERY) {
            if(imageEditorActivity.isAllPermissionResultGranted(grantResults)){
                confirmSubmission();
            }
        }

        if (requestCode == PERMISSION_CAMERA) {
            if(imageEditorActivity.isAllPermissionResultGranted(grantResults)){
                startCamera();
            }
        }
    }

    @Override
    public void onBackgroundSelected(BackgroundItem backgroundItem) {
        Glide.with(getContext())
                .load(backgroundItem.getImage())
                .into(backgroundIMG);
    }

    @Override
    public void onFrameSelected(FrameItem frameItem) {
        Glide.with(getContext())
                .load(frameItem.getImage())
                .into(frameIMG);

        greetingTXT.setHint(frameItem.heading);
        messageTXT.setHint(frameItem.content);
        closingTXT.setHint(frameItem.closing);
    }
}
