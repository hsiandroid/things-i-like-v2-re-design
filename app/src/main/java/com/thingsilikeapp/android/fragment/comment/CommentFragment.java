package com.thingsilikeapp.android.fragment.comment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.hendraanggrian.socialview.SocialView;
import com.hendraanggrian.widget.SocialAutoCompleteTextView;
import com.microsoft.appcenter.analytics.Analytics;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.CommentActivity;
import com.thingsilikeapp.android.activity.ProfileActivity;
import com.thingsilikeapp.android.adapter.CommentMentionAdapter;
import com.thingsilikeapp.android.adapter.CommentRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.CommentOtherOptionDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.CommentItem;
import com.thingsilikeapp.data.model.CommentPusherItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.comment.AllCommentRequest;
import com.thingsilikeapp.server.request.comment.CreateCommentRequest;
import com.thingsilikeapp.server.request.comment.DeleteCommentRequest;
import com.thingsilikeapp.server.request.comment.EditCommentRequest;
import com.thingsilikeapp.server.request.social.FollowingRequest;
import com.thingsilikeapp.server.request.wishlist.InfoWishListRequest;
import com.thingsilikeapp.server.transformer.comment.CommentCollectionTransformer;
import com.thingsilikeapp.server.transformer.comment.CommentSingleTransformer;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.pusher.Cred;
import com.thingsilikeapp.vendor.android.java.pusher.Sound;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import icepick.State;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;

public class CommentFragment extends BaseFragment implements
        View.OnClickListener,
        CommentRecycleViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        CommentOtherOptionDialog.CommentCallback{

    public static final String TAG = CommentFragment.class.getName();
    public static final String INCLUDE = "author.info,tagged_user.info";

    private CommentActivity commentActivity;

    private LinearLayoutManager linearLayoutManager;
    private CommentRecycleViewAdapter commentRecycleViewAdapter;
    private AllCommentRequest allCommentRequest;
    private Pusher pusher;
    private CommentMentionAdapter commentMentionAdapter;
    private FollowingRequest followingRequest;
    private Timer searchTimer;

    @BindView(R.id.commentRV)                  RecyclerView commentRV;
    @BindView(R.id.messageET)                  SocialAutoCompleteTextView messageET;
    @BindView(R.id.sendBTN)                    View sendBTN;
    @BindView(R.id.mentionCON)                 View mentionCON;
    @BindView(R.id.mentionCIV)                 ImageView mentionCIV;
    @BindView(R.id.mentionTXT)                 TextView mentionTXT;
    @BindView(R.id.mentionBTN)                 View mentionBTN;
    @BindView(R.id.commentPlaceHolderCON)      View commentPlaceHolderCON;
    @BindView(R.id.placeholder404CON)          View placeholder404CON;
    @BindView(R.id.commentCON)                 View commentCON;
    @BindView(R.id.commentSRL)                 MultiSwipeRefreshLayout commentSRL;

    @State int commentID;
    @State int wishListID;
    @State int tempID = 0;

    @State String wishListName;


    public static CommentFragment newInstance(int wishListID, String wishListName) {
        CommentFragment commentFragment = new CommentFragment();
        commentFragment.wishListID = wishListID;
        commentFragment.wishListName = wishListName;
        return commentFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_comment;
    }

    @Override
    public void onViewReady() {
        setupListView();
        initEditText();

        Analytics.trackEvent("comment_CommentFragment_onViewReady");

    }



    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupPusher();
    }

    private void setupListView(){

        commentActivity = (CommentActivity) getContext();
        commentActivity.setTitle(wishListName);

        sendBTN.setOnClickListener(this);
        mentionBTN.setOnClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        commentRV.setLayoutManager(linearLayoutManager);

        commentRecycleViewAdapter = new CommentRecycleViewAdapter(getContext());
        commentRecycleViewAdapter.setOnItemClickListener(this);
        commentRV.setAdapter(commentRecycleViewAdapter);
        initCommentAPI();
    }

    private void initEditText(){
        commentMentionAdapter = new CommentMentionAdapter(getContext());
        messageET.setMentionEnabled(true);
        messageET.setMentionColor(ActivityCompat.getColor(getContext(), R.color.black));
        messageET.setTextColor(ActivityCompat.getColor(getContext(), R.color.text_gray));
        messageET.setMentionAdapter(commentMentionAdapter);
        messageET.setMentionTextChangedListener(new Function2<SocialView, CharSequence, Unit>() {

            @Override
            public Unit invoke(SocialView socialView, final CharSequence charSequence) {
                if (searchTimer != null) {
                    searchTimer.cancel();
                }
                searchTimer = new Timer();
                searchTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        searchFollowing(charSequence.toString());
                    }
                }, 300);
                return null;
            }
        });
    }

    private void searchFollowing(String keyword){
        followingRequest = new FollowingRequest(getContext());
        followingRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, UserData.getUserId())
                .addParameters(Keys.server.key.KEYWORD, keyword)
                .setPerPage(20)
                .execute();
    }

    private void initCommentAPI(){
        commentSRL.setColorSchemeResources(R.color.colorPrimary);
        commentSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        commentSRL.setSwipeableChildren(R.id.commentRV, R.id.placeHolderCON);
        commentSRL.setOnRefreshListener(this);

        allCommentRequest = new AllCommentRequest(getContext());
        allCommentRequest
                .setSwipeRefreshLayout(commentSRL)
                .setDeviceRegID(commentActivity.getDeviceRegID())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, INCLUDE)
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .showSwipeRefreshLayout(true)
                .setPerPage(50)
                .first();

        if(wishListName.equals("") || wishListName != null){
            new InfoWishListRequest(getContext())
                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                    .addParameters(Keys.server.key.INCLUDE, "info")
                    .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                    .execute();
        }
    }

    public void attemptSend(){
        if(messageET.getText().toString().length() != 0){
            tempID ++;
            CommentItem commentItem = new CommentItem();
            commentItem.wishlist_id = wishListID;
            commentItem.temp_id = tempID;
            commentItem.content = messageET.getText().toString();
            commentItem.time_passed = "sending";
            commentItem.author.data = UserData.getUserItem();
            commentItem.tagged_user.data = getTaggedUser();
            commentItem.tagged_user_id = getTaggedUser().id;

            commentRecycleViewAdapter.newCommnent(commentItem);
            commentRV.smoothScrollToPosition(0);
            messageET.setText("");
            resetMention();
            setPlaceHolder();

            Log.e("temp_id", ">>>" + commentItem.temp_id);
            CreateCommentRequest createCommentRequest = new CreateCommentRequest(getContext());
            createCommentRequest
                    .showNoInternetConnection(false)
                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                    .addParameters(Keys.server.key.INCLUDE, INCLUDE)
                    .addParameters(Keys.server.key.WISHLIST_ID, commentItem.wishlist_id)
                    .addParameters(Keys.server.key.TEMP_ID, commentItem.temp_id)
                    .addParameters(Keys.server.key.CONTENT, commentItem.content)
                    .addParameters(Keys.server.key.TAGGED_USER_ID, commentItem.tagged_user.data.id)
                    .execute();
        }
    }

    private UserItem getTaggedUser() {
        int i = 0;

        if (messageET.getMentions().size() > i ) {
            return commentMentionAdapter.getUserByUserName(messageET.getMentions().get(i));
        }

        return new UserItem();

    }


//    private UserItem getTaggedID(){
//        if(messageET.getMentions().size() > 0){
//            return commentMentionAdapter.getUserByID(messageET.getId());
//        }
//        return new UserItem();
//    }

    public void attemptEdit(){
        if(messageET.getText().toString().length() != 0){

            CommentItem commentItem = new CommentItem();
            commentItem.id = commentID;
            commentItem.wishlist_id = wishListID;
            commentItem.content = messageET.getText().toString();
            commentItem.time_passed = "updating";
            commentItem.author.data = UserData.getUserItem();
            commentItem.tagged_user.data = getTaggedUser();

            commentRecycleViewAdapter.updateData(commentItem);
            messageET.setText("");
            resetMention();
            setPlaceHolder();

            EditCommentRequest editCommentRequest = new EditCommentRequest(getContext());
            editCommentRequest
                    .showNoInternetConnection(false)
                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                    .addParameters(Keys.server.key.INCLUDE, INCLUDE)
                    .addParameters(Keys.server.key.CONTENT, commentItem.content)
                    .addParameters(Keys.server.key.WISHLIST_COMMENT_ID, commentItem.id)
                    .addParameters(Keys.server.key.TAGGED_USER_ID, commentItem.tagged_user.data.id)
                    .execute();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainBackButtonIV:
                commentActivity.onBackPressed();
                break;
            case R.id.sendBTN:
                if (commentID != 0){
                    attemptEdit();
                }else {
                    attemptSend();
                }
                Analytics.trackEvent("comment_CommentFragment_sendBTN");
                break;
            case R.id.mentionBTN:
                resetMention();
                Analytics.trackEvent("comment_CommentFragment_mentionBTN");
                break;
        }
    }

    private void setupPusher(){
        PusherOptions options = new PusherOptions();
        options.setCluster(Cred.CLUSTER);
        pusher = new Pusher(Cred.KEY, options);
        Log.e("ID", ">>>" + wishListID);
        final String commentChannel = "comment." + wishListID;
        Channel channel = pusher.subscribe(commentChannel);

        channel.bind("NewCommentCreated", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                if(commentChannel.equals(channelName)){
                    Log.e("Comment", ">>>" + data);
                    processPusher(data);
                }
            }
        });
        pusher.connect();
    }

    private void processPusher(final String data){
        CommentPusherItem commentPusherItem = new Gson().fromJson(data, CommentPusherItem.class);
        final CommentItem commentItem = new CommentItem();
        commentItem.id = commentPusherItem.id;
        commentItem.wishlist_id = commentPusherItem.wishlist_id;
        commentItem.content = commentPusherItem.content;
        commentItem.author = new CommentItem.Author();
        commentItem.author.data.id = commentPusherItem.author.id;
        commentItem.author.data.fb_id = commentPusherItem.author.fb_id;
        commentItem.author.data.username = commentPusherItem.author.name;
        commentItem.author.data.name = commentPusherItem.author.name;
        commentItem.author.data.image = commentPusherItem.author.getAvatar();

        commentItem.tagged_user = new CommentItem.TaggedUser();
        commentItem.tagged_user.data.id = commentPusherItem.tagged_user.id;
        commentItem.tagged_user.data.fb_id = commentPusherItem.tagged_user.fb_id;
        commentItem.tagged_user.data.username = commentPusherItem.tagged_user.name;
        commentItem.tagged_user.data.name = commentPusherItem.tagged_user.name;
        commentItem.tagged_user.data.image = commentPusherItem.tagged_user.getAvatar();

        if(commentPusherItem.author.id != UserData.getUserId() && commentPusherItem.wishlist_id == wishListID){
            commentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Sound.playNotification(getContext());
                    commentRecycleViewAdapter.newCommnent(commentItem);
                    if(commentRV != null){
                        commentRV.smoothScrollToPosition(0);
                    }
                    setPlaceHolder();
                }
            });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(pusher != null){
            pusher.disconnect();
        }

        if (searchTimer != null) {
            searchTimer.cancel();
        }
    }

    private void setPlaceHolder(){
        if(commentPlaceHolderCON != null){
            if (commentRecycleViewAdapter.getItemCount() == 0){
                commentPlaceHolderCON.setVisibility(View.VISIBLE);
                commentRV.setVisibility(View.GONE);
            }else{
                commentPlaceHolderCON.setVisibility(View.GONE);
                commentRV.setVisibility(View.VISIBLE);
            }
        }
    }

    private void refreshList(){
        allCommentRequest
                .showSwipeRefreshLayout(allCommentRequest.hasMorePage())
                .nextPage();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(CommentItem commentItem) {

    }

    @Override
    public void onItemLongClick(CommentItem commentItem) {
        if(commentItem.id != 0){
            if (UserData.getUserItem().id == commentItem.author.data.id){
                CommentOtherOptionDialog.newInstance(commentItem, this).show(getChildFragmentManager(), CommentOtherOptionDialog.TAG);
            }
        }
    }

    @Override
    public void onAvatarClick(UserItem author) {
        commentActivity.startProfileActivity(author.id);

    }

    @Override
    public void onReplyClick(UserItem author) {
//        mentionUser(author);
        messageET.setText("@" + author.name + " ");
        messageET.setSelection(messageET.getText().length());
    }

    private void mentionUser(UserItem author){
//        taggedUser = author;
        mentionCON.setVisibility(View.VISIBLE);
        //mentionTXT.setText("mention @" + author.username);

        Glide.with(getContext())
                .load(author.getAvatar())
                .apply(new RequestOptions()
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(mentionCIV);
    }

    private void resetMention(){
//        taggedUser = new UserItem();
        mentionCON.setVisibility(View.GONE);
    }


    @Override
    public void editComment(CommentItem commentItem) {
        messageET.setText(commentItem.content);
        messageET.setSelection(messageET.getText().length());
        Keyboard.showKeyboard(getActivity(),messageET);
        commentID = commentItem.id;

        if(commentItem.tagged_user_id != 0){
//            mentionUser(commentItem.tagged_user.data);
            Keyboard.showKeyboard(getActivity(),messageET);
            messageET.setText(commentItem.content);
            messageET.setSelection(messageET.getText().length());
        }
    }

    @Override
    public void deleteComment(CommentItem commentItem) {
        deleteCommentDialog(commentItem.id);
    }

    private void resetEditComment(){
        resetMention();
        messageET.setText("");
        commentID = 0;
    }

    private void deleteCommentDialog(final int commentID){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Comment")
                .setNote("Are you sure you want to remove your comment?")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Remove")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteComment(commentID);
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("comment_CommentFragment_deleteCommentDialog");
    }

    private void deleteComment(int commentID){
        DeleteCommentRequest deleteCommentRequest = new DeleteCommentRequest(getContext());
        deleteCommentRequest
                .showNoInternetConnection(false)
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Removing...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "user")
                .addParameters(Keys.server.key.WISHLIST_COMMENT_ID, commentID)
                .execute();
    }

    @Subscribe
    public void onResponse(AllCommentRequest.ServerResponse responseData) {
        CommentCollectionTransformer commentCollectionTransformer = responseData.getData(CommentCollectionTransformer.class);
        if(commentCollectionTransformer.status) {
            if (responseData.isNext()) {
                commentRecycleViewAdapter.addNewData(commentCollectionTransformer.data);
            } else {
                commentRecycleViewAdapter.setNewData(commentCollectionTransformer.data);
            }
            commentMentionAdapter.addNewData(commentCollectionTransformer.data);
        }
        if(responseData.getCode() == 404){
            commentCON.setVisibility(View.GONE);
            placeholder404CON.setVisibility(View.VISIBLE);
            getActivity().finish();
        }else {
            commentCON.setVisibility(View.VISIBLE);
            placeholder404CON.setVisibility(View.GONE);
        }
        setPlaceHolder();
    }

    @Subscribe
    public void onResponse(FollowingRequest.ServerResponse responseData) {
        SuggestionTransformer suggestionTransformer = responseData.getData(SuggestionTransformer.class);
        if(suggestionTransformer.status) {
            commentMentionAdapter.addNewUserData(suggestionTransformer.data);
        }
    }

    @Subscribe
    public void onResponse(CreateCommentRequest.ServerResponse responseData) {
        CommentSingleTransformer commentSingleTransformer = responseData.getData(CommentSingleTransformer.class);
        if(commentSingleTransformer.status){
            commentSingleTransformer.data.temp_id = commentSingleTransformer.temp_id;
            commentRecycleViewAdapter.updateData(commentSingleTransformer.data);
        }
    }

    @Subscribe
    public void onResponse(DeleteCommentRequest.ServerResponse responseData) {
        CommentSingleTransformer commentSingleTransformer = responseData.getData(CommentSingleTransformer.class);
        if(commentSingleTransformer.status){
            commentRecycleViewAdapter.removeComment(commentSingleTransformer.data);
            ToastMessage.show(getContext(),commentSingleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else {
            ToastMessage.show(getContext(),commentSingleTransformer.msg, ToastMessage.Status.FAILED);
        }

        setPlaceHolder();
    }

    @Subscribe
    public void onResponse(EditCommentRequest.ServerResponse responseData) {
        CommentSingleTransformer commentSingleTransformer = responseData.getData(CommentSingleTransformer.class);
        if(commentSingleTransformer.status){
            commentRecycleViewAdapter.updateData(commentSingleTransformer.data);
            ToastMessage.show(getContext(),commentSingleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else {
            ToastMessage.show(getContext(),commentSingleTransformer.msg, ToastMessage.Status.FAILED);
        }
        resetEditComment();
        setPlaceHolder();
    }

    @Subscribe
    public void onResponse(InfoWishListRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            commentActivity.setTitle(wishListInfoTransformer.wishListItem.title);
        }
    }
}
