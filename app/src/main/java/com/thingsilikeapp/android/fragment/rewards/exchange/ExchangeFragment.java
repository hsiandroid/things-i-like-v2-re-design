package com.thingsilikeapp.android.fragment.rewards.exchange;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.adapter.CurrencySpinnerAdapter;
import com.thingsilikeapp.android.adapter.CustomAdapter;
import com.thingsilikeapp.android.dialog.CurrencyDialog;
import com.thingsilikeapp.data.model.BirthdayItem;
import com.thingsilikeapp.data.model.GetRateItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.GemHelper;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.BaseTransformer;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import butterknife.BindView;
import icepick.State;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ExchangeFragment extends BaseFragment implements View.OnClickListener, CurrencyDialog.Callback {

    public static final String TAG = ExchangeFragment.class.getName();

    public static ExchangeFragment newInstance() {
        ExchangeFragment fragment = new ExchangeFragment();
        return fragment;
    }

    @BindView(R.id.balanceTXT)          TextView balanceTXT;
    @BindView(R.id.currencySpinner)     Spinner currencySPR;
    @BindView(R.id.encashAmountET)      EditText encashmentAmountET;
    @BindView(R.id.flagIV)              ImageView flagIV;
    @BindView(R.id.toReceiveTXT)        TextView toReceiveTXT;
    @BindView(R.id.continueBTN)         TextView continueBTN ;
    @BindView(R.id.currencyBTN)    	    TextView currencyBTN;

    @State String currencyy;
    @State String serviceFee;
    @State Double amount;
    @State Double rate;
    @State Double totalServiceFee;
    @State Double conversion;

    private ExchangeActivity exchangeActivity;
    private CurrencySpinnerAdapter spinnerAdapter;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_exchange;
    }

    @Override
    public void onViewReady() {
        exchangeActivity = (ExchangeActivity)getContext();
        exchangeActivity.setTitle("Exchange to Cash");
        continueBTN.setOnClickListener(this);
        currencyBTN.setOnClickListener(this);
        balanceTXT.setText(UserData.getDisplayCrystalFragment());
//        if (toReceiveTXT.getText().toString().equalsIgnoreCase("Choose Currency")){
//            continueBTN.setEnabled(false);
//        }
        Wallet.getDefault().rate(getContext());
        Wallet.getDefault().currentRate(getContext(), "");
        setUpSpinner();
        setupEditText();
        Analytics.trackEvent("rewards_exchange_ExchangeFragment_onViewReady");
    }

    public void setUpSpinner() {
        spinnerAdapter = new CurrencySpinnerAdapter(getContext());
        currencySPR.setAdapter(spinnerAdapter);
    }

    @Subscribe
    public void onResponse(Wallet.RateResponse colletionResponse) {
        CollectionTransformer<GetRateItem> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            spinnerAdapter.setNewData(collectionTransformer.data);
        }
    }

    @Subscribe
    public void onResponse(Wallet.CurrentRateRequestResponse colletionResponse) {
        SingleTransformer<GetRateItem> singleTransformer = colletionResponse.getData(SingleTransformer.class);
        if (singleTransformer.status) {
            rate = Double.parseDouble(singleTransformer.data.rate);
            currencyy = singleTransformer.data.currency;
            currencyBTN.setText(singleTransformer.data.countryName + " (" + singleTransformer.data.currency + ")");
            serviceFee = String.valueOf(singleTransformer.service_fee);
        }
    }

    @Subscribe
    public void onResponse(Wallet.ValidateResponse responseData) {
        BaseTransformer transformer = responseData.getData(BaseTransformer.class);
        if(transformer.status){
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.SUCCESS);
            exchangeActivity.openSelectBankFragment(currencyy, encashmentAmountET.getText().toString(), String.valueOf(amount), serviceFee, String.valueOf(rate), String.valueOf(conversion));
        }else{
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.continueBTN:
                if (encashmentAmountET.getText().toString().equalsIgnoreCase("")){
                    ToastMessage.show(getContext(), "Please enter a valid amount!", ToastMessage.Status.FAILED);
                }else{
                    Wallet.getDefault().validate(getContext(), encashmentAmountET.getText().toString());
                }
                Analytics.trackEvent("rewards_exchange_ExchangeFragment_continueBTN");
                break;
            case R.id.currencyBTN:
                CurrencyDialog.Builder(this).show(getFragmentManager(), TAG);
                Analytics.trackEvent("rewards_exchange_ExchangeFragment_currencyBTN");
                break;
        }
    }

    @Override
    public void onSuccess(final GetRateItem rateItem, int service_fee) {
        DecimalFormat precision = new DecimalFormat("0.00");
        currencyBTN.setText(rateItem.countryName + " (" +rateItem.currency + ")");
        this.currencyy = rateItem.currency;
        this.serviceFee = String.valueOf(service_fee);
        this.rate = Double.parseDouble(rateItem.rate);
        if (!encashmentAmountET.getText().toString().equalsIgnoreCase("")){
            totalServiceFee = (Double.parseDouble(encashmentAmountET.getText().toString()) / 100) * Integer.parseInt(serviceFee);
            amount = (Double.parseDouble(encashmentAmountET.getText().toString()) - totalServiceFee) * rate;
            conversion = Double.parseDouble(encashmentAmountET.getText().toString()) * rate;
            BigDecimal amounts = new BigDecimal(amount);
            BigDecimal conversions = new BigDecimal(conversion);
            toReceiveTXT.setText(precision.format(conversions));
        }else {
            toReceiveTXT.setText("0");
        }
    }

    public void setupEditText (){
        DecimalFormat precision = new DecimalFormat("0.00");
        if (!encashmentAmountET.getText().toString().equalsIgnoreCase("")){
            totalServiceFee = (Double.parseDouble(encashmentAmountET.getText().toString()) / 100) * Integer.parseInt(serviceFee);
            amount = (Double.parseDouble(encashmentAmountET.getText().toString()) - totalServiceFee) * rate;
            conversion = Double.parseDouble(encashmentAmountET.getText().toString()) * rate;
            BigDecimal amounts = new BigDecimal(amount);
            BigDecimal conversions = new BigDecimal(conversion);
            toReceiveTXT.setText(precision.format(conversions));
        }else {
            toReceiveTXT.setText("0");
        }
        encashmentAmountET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                DecimalFormat precision = new DecimalFormat("0.00");
                if (!encashmentAmountET.getText().toString().equalsIgnoreCase("")){
                    totalServiceFee = (Double.parseDouble(encashmentAmountET.getText().toString()) / 100) * Integer.parseInt(serviceFee);
                    amount = (Double.parseDouble(encashmentAmountET.getText().toString()) - totalServiceFee) * rate;
                    conversion = Double.parseDouble(encashmentAmountET.getText().toString()) * rate;
                    BigDecimal amounts = new BigDecimal(amount);
                    BigDecimal conversions = new BigDecimal(conversion);
                    toReceiveTXT.setText(precision.format(conversions));
                }else {
                    toReceiveTXT.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
}
