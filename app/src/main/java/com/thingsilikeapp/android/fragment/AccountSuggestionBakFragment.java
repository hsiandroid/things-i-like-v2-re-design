package com.thingsilikeapp.android.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.request.social.FollowRequest;
import com.thingsilikeapp.server.request.social.SuggestionRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.user.SearchUserRequest;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.widget.ExpandableHeightListView;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;
import com.thingsilikeapp.android.activity.AccountActivity;
import com.thingsilikeapp.android.adapter.SuggestionAdapter;
import com.thingsilikeapp.android.adapter.SuggestionRecycleViewAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;

public class AccountSuggestionBakFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        SuggestionAdapter.ClickListener,SuggestionRecycleViewAdapter.ClickListener,
        View.OnClickListener,
        AccountActivity.SearchListener{

    public static final String TAG = AccountSuggestionBakFragment.class.getName();

    private AccountActivity accountActivity;

    private SuggestionAdapter facebookFriendsAdapter;
    private SuggestionRecycleViewAdapter topUserAdapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private EndlessRecyclerViewScrollListener searchEndlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayoutManager searchLinearLayoutManager;
    private SuggestionRecycleViewAdapter searchResultAdapter;
    private SuggestionRequest suggestionRequest;
    private SearchUserRequest searchUserRequest;

    @BindView(R.id.facebookFriendsEHLV) 		ExpandableHeightListView facebookFriendsEHLV;
    @BindView(R.id.topUserEHLV)                 RecyclerView topUserEHLV;
    @BindView(R.id.searchResultEHLV)			RecyclerView searchResultEHLV;
    @BindView(R.id.suggestionSRL)               MultiSwipeRefreshLayout suggestionSRL;
    @BindView(R.id.searchResultCON)             View searchResultCON;
    @BindView(R.id.suggestionCON)               View suggestionCON;
    @BindView(R.id.searchPB)                    View searchPB;
    @BindView(R.id.viewAllTopUserBTN)           View viewAllTopUserBTN;
    @BindView(R.id.viewAllFBFriendsBTN)         View viewAllFBFriendsBTN;
    @BindView(R.id.viewAllSearchResultBTN)      View viewAllSearchResultBTN;
    @BindView(R.id.nextBTN)                     View nextBTN;
    @BindView(R.id.loadMorePB)                  View loadMorePB;

    public static AccountSuggestionBakFragment newInstance() {
        AccountSuggestionBakFragment accountSuggestionFragment = new AccountSuggestionBakFragment();
        return accountSuggestionFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_account_suggestion_bak;
    }

    @Override
    public void onViewReady() {
        accountActivity = (AccountActivity) getActivity();
        accountActivity.showSearchContainer(true);
        accountActivity.enableBack(false);

        viewAllTopUserBTN.setOnClickListener(this);
        viewAllFBFriendsBTN.setOnClickListener(this);
        viewAllSearchResultBTN.setOnClickListener(this);
        nextBTN.setOnClickListener(this);

        setupFacebookFriendsListView();
        setupTopUserListView();
        setupSearchListView();
        initSuggestionAPI();

        accountActivity.getSearchEditText().clearFocus();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        accountActivity.setSearchListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void setupSearchListView() {
        searchLinearLayoutManager = new LinearLayoutManager(getContext());
        searchEndlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(searchLinearLayoutManager, new EndlessRecyclerViewScrollListener.Callback() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePB.setVisibility(searchUserRequest.hasMorePage() ? View.VISIBLE : View.GONE);
                searchUserRequest.nextPage();
            }
        });
        searchResultAdapter = new SuggestionRecycleViewAdapter(getContext());
        searchResultAdapter.setOnItemClickListener(this);
        searchResultEHLV.setLayoutManager(searchLinearLayoutManager);
        searchResultEHLV.addOnScrollListener(searchEndlessRecyclerViewScrollListener);
        searchResultEHLV.setAdapter(searchResultAdapter);
    }

    private void setupFacebookFriendsListView() {
        facebookFriendsAdapter = new SuggestionAdapter(getContext());
        facebookFriendsAdapter.setOnItemClickListener(this);
        facebookFriendsEHLV.setAdapter(facebookFriendsAdapter);
    }

    private void setupTopUserListView() {
        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, new EndlessRecyclerViewScrollListener.Callback() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePB.setVisibility(suggestionRequest.hasMorePage() ? View.VISIBLE : View.GONE);
                suggestionRequest.nextPage();
            }
        });

        topUserAdapter = new SuggestionRecycleViewAdapter(getContext());
        topUserEHLV.setLayoutManager(linearLayoutManager);
        topUserEHLV.addOnScrollListener(endlessRecyclerViewScrollListener);
        topUserAdapter.setOnItemClickListener(this);
        topUserEHLV.setAdapter(topUserAdapter);
    }

    private void initSuggestionAPI(){

        suggestionSRL.setColorSchemeResources(R.color.colorPrimary);
        suggestionSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        suggestionSRL.setOnRefreshListener(this);

        suggestionRequest = new SuggestionRequest(getContext());
        suggestionRequest
                .setSwipeRefreshLayout(suggestionSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .showSwipeRefreshLayout(true)
                .setPerPage(25)
                .execute();
    }

    private void attemptSearch(String text){
        searchPB.setVisibility(View.VISIBLE);
        searchResultAdapter.reset();

        searchUserRequest = new SearchUserRequest(getContext());
        searchUserRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,statistics,social")
                .addParameters(Keys.server.key.KEYWORD, text)
                .setPerPage(25)
                .execute();
    }

    private void refreshList(){
        facebookFriendsAdapter.setNewData(new ArrayList<UserItem>());
        topUserAdapter.setNewData(new ArrayList<UserItem>());

        suggestionRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Subscribe
    public void onResponse(SuggestionRequest.ServerResponse responseData) {
        SuggestionTransformer suggestionTransformer = responseData.getData(SuggestionTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                topUserAdapter.addNewData(suggestionTransformer.data);
            }else{
                topUserAdapter.setNewData(suggestionTransformer.data);
            }
        }
        loadMorePB.setVisibility(View.GONE);
    }

    @Subscribe
    public void onResponse(SearchUserRequest.ServerResponse responseData) {
        SuggestionTransformer suggestionTransformer = responseData.getData(SuggestionTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                searchResultAdapter.addNewData(suggestionTransformer.data);
            }else{
                searchResultAdapter.setNewData(suggestionTransformer.data);
            }
        }

        searchPB.setVisibility(View.GONE);
        loadMorePB.setVisibility(View.GONE);
    }

    @Override
    public void onCommandClick(UserItem userItem) {
        if(userItem.social.data.is_following.equals("yes")){
            topUserAdapter.unFollowUser(userItem);
        }else{
            topUserAdapter.followUser(userItem);
        }
    }

    @Override
    public void onUserClick(UserItem userItem) {

    }

    @Override
    public void onFollowSuccessClick() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.nextBTN:
                if(UserData.getUserItem().statistics.data.following > 1){
                    accountActivity.openShareAppFragment();
                }else{
                    ToastMessage.show(getContext(), "You must follow at least 1 user", ToastMessage.Status.FAILED);
                }
                break;
            case R.id.viewAllTopUserBTN:
                ((RouteActivity) getContext()).startSearchActivity("all");
                break;
        }
    }

    private void setNext(){
        if(UserData.getUserItem().statistics.data.following > 1){
            nextBTN.setVisibility(View.VISIBLE);
        }else{
            nextBTN.setVisibility(View.GONE);
        }
    }

    @Override
    public void searchTextChange(final String keyword) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(searchResultCON != null && suggestionCON != null){
                    if (keyword.equals("")){
                        searchResultCON.setVisibility(View.GONE);
                        suggestionCON.setVisibility(View.VISIBLE);
                    }else{
                        searchResultCON.setVisibility(View.VISIBLE);
                        suggestionCON.setVisibility(View.GONE);
                        attemptSearch(keyword);
                    }
                }
            }
        });
    }

    @Subscribe
    public void onResponse(FollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
            setNext();
        }
    }

    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
            setNext();
        }
    }
}
