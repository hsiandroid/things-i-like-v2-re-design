package com.thingsilikeapp.android.fragment.request;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RequestActivity;
import com.thingsilikeapp.android.adapter.RequestOnGoingAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.wishlist.WishListRevokePermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishlistPermissionRequest;
import com.thingsilikeapp.server.transformer.wishlist.PermissionTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoViewerTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.NumberFormatter;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class RequestOngoingFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = RequestOngoingFragment.class.getName();

    private RequestActivity requestActivity;
    private WishlistPermissionRequest wishlistPermissionRequest;
    private RequestOnGoingAdapter requestOnGoingAdapter;

    @BindView(R.id.requestedUserTXT)        TextView requestedUserTXT;
    @BindView(R.id.requestOnGoingEHLV)      ListView requestOnGoingEHLV;
    @BindView(R.id.requestSRL)              MultiSwipeRefreshLayout requestSRL;

    @State int wishListID;
    @State int count;
    @State String title;

    public static RequestOngoingFragment newInstance(int wishListID, int count, String title) {
        RequestOngoingFragment requestOngoingFragment = new RequestOngoingFragment();
        requestOngoingFragment.wishListID = wishListID;
        requestOngoingFragment.count = count;
        requestOngoingFragment.title = title;
        return requestOngoingFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_ongoing_request;
    }

    @Override
    public void onViewReady() {
        requestActivity = (RequestActivity) getContext();
        requestActivity.setTitle(title);
        setupRequestListView();
        initSuggestionAPI();
        requestedUserTXT.setText("(" + NumberFormatter.format(count) + ")");
        Analytics.trackEvent("request_RequestOnGoingFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void setupRequestListView() {
        requestOnGoingAdapter = new RequestOnGoingAdapter(getContext());
        requestOnGoingEHLV.setAdapter(requestOnGoingAdapter);
    }

    private void initSuggestionAPI(){

        requestSRL.setColorSchemeResources(R.color.colorPrimary);
        requestSRL.setSwipeableChildren(R.id.requestOnGoingEHLV);
        requestSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        requestSRL.setOnRefreshListener(this);
        wishlistPermissionRequest = new WishlistPermissionRequest(getContext());
        wishlistPermissionRequest
                .setSwipeRefreshLayout(requestSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "wishlist.image,wishlist.owner.info,viewer.info,info,image,sender.info")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .showSwipeRefreshLayout(true)
                .setPerPage(50);
    }

    private void refreshList(){
        wishlistPermissionRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(WishlistPermissionRequest.ServerResponse responseData) {
        PermissionTransformer permissionTransformer = responseData.getData(PermissionTransformer.class);
        if(permissionTransformer.status){
            requestOnGoingAdapter.setNewData(permissionTransformer.data);
        }
    }

    @Subscribe
    public void onResponse(WishListRevokePermissionRequest.ServerResponse responseData) {
        WishListInfoViewerTransformer wishlistInfoTransformer = responseData.getData(WishListInfoViewerTransformer.class);
        if(wishlistInfoTransformer.status){
            count = count - 1;
            requestedUserTXT.setText("(" + NumberFormatter.format(count) + ")");
        }
    }
}
