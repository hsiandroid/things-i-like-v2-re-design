package com.thingsilikeapp.android.fragment.notification;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.DiscoverActivity;
import com.thingsilikeapp.android.adapter.NotificationRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.AddressPermissionDialog;
import com.thingsilikeapp.android.dialog.AppreciateGiftDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListTransactionItem;
import com.thingsilikeapp.data.model.WishListViewerItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.wishlist.GeneralCompactPermissionRequest;
import com.thingsilikeapp.server.request.wishlist.PermissionAllRequest;
import com.thingsilikeapp.server.request.wishlist.PermissionOwnedRequest;
import com.thingsilikeapp.server.request.wishlist.PermissionPendingRequest;
import com.thingsilikeapp.server.request.wishlist.WishListCancelPermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListGrantPermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListReceivePermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListRejectGiftPermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListRevokePermissionRequest;
import com.thingsilikeapp.server.transformer.wishlist.CompactTransformer;
import com.thingsilikeapp.server.transformer.wishlist.PermissionTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransactionTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoViewerTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListTransactionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.decoration.HeaderItemDecoration;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class RequestFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        AppreciateGiftDialog.Callback,
        NotificationRecycleViewAdapter.ClickListener,
        View.OnClickListener{

    public static final String TAG = RequestFragment.class.getName();

    private DiscoverActivity mainActivity;

    private GeneralCompactPermissionRequest generalCompactPermissionRequest;
    private PermissionAllRequest addressRequest;
    private PermissionPendingRequest giftRequest;
    private PermissionOwnedRequest onGoingRequest;

    public NotificationRecycleViewAdapter notificationRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;

//    @State String include = "wishlist.image,wishlist.owner.info,viewer.info,info,image,sender.info";
    @State String include = "wishlist.image,wishlist.owner.info,viewer.info,image,sender.info";

    @BindView(R.id.requestRV)               RecyclerView requestRV;
    @BindView(R.id.requestSRL)              MultiSwipeRefreshLayout requestSRL;
    @BindView(R.id.placeHolderCON)          View placeHolderCON;
    @BindView(R.id.loadingIV)               View loadingIV;

    public static RequestFragment newInstance() {
        RequestFragment requestFragment = new RequestFragment();
        return requestFragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_request;
    }

    @Override
    public void onViewReady() {
        mainActivity = (DiscoverActivity) getContext();

        setupListView();
        initSuggestionAPI();
        addressAPI();
        giftAPI();
        ownedAPI();
        Analytics.trackEvent("notification_RequestFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void setupListView(){
        notificationRecycleViewAdapter = new NotificationRecycleViewAdapter(getContext());
        notificationRecycleViewAdapter.setOnItemClickListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        requestRV.setLayoutManager(linearLayoutManager);
        requestRV.addItemDecoration(new HeaderItemDecoration(requestRV, notificationRecycleViewAdapter));
        requestRV.setAdapter(notificationRecycleViewAdapter);
    }

    private void attemptReceivedRequest(int wishListID, String dedication){
        new WishListReceivePermissionRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Accepting Gift...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .addParameters(Keys.server.key.WISHLIST_TRANSACTION_ID, wishListID)
                .addParameters(Keys.server.key.APPRECIATION_MESSAGE, dedication)
                .execute();
    }

    private void attemptRejectGiftRequest(final int wishlistID, final int userID){
        Log.e("EEEEEEEEEEEEEEE", ">>>> " + wishlistID);
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Have you received the actual gift?")
                .setNote("You will notify this person that you have not received the gift.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Confirm")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new WishListRejectGiftPermissionRequest(getContext())
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading...", false, false))
                                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .addParameters(Keys.server.key.INCLUDE, include)
                                .addParameters(Keys.server.key.WISHLIST_TRANSACTION_ID, wishlistID)
                                .addParameters(Keys.server.key.USER_ID, userID)
                                .execute();
                        confirmationDialog.dismiss();
//                        ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.DiscoverActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.UNRECEIVED_GIFT);
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("notifcation_RequestFragment_attemptRejectGiftRequest");
    }

    private void attemptGrantRequest(final int wishlistID, final int userID){
        AddressPermissionDialog.newInstance(new AddressPermissionDialog.AddressPermissionListener() {
            @Override
            public void positiveClicked(int addressID) {
                new WishListGrantPermissionRequest(getContext())
                        .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading...", false, false))
                        .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                        .addParameters(Keys.server.key.INCLUDE, include)
                        .addParameters(Keys.server.key.WISHLIST_ID, wishlistID)
                        .addParameters(Keys.server.key.ADDRESS_ID, String.valueOf(addressID))
                        .addParameters(Keys.server.key.USER_ID, userID)
                        .execute();
                Log.e("Wishlist_id",">>>" + wishlistID);
                Log.e("user_id",">>>" + userID);
                Log.e("address_id",">>>" + addressID);
            }
        }).show(getChildFragmentManager(), AddressPermissionDialog.TAG);
        Analytics.trackEvent("notifcation_RequestFragment_attemptGrantRequest");

    }

    private void attemptCancelRequest(final int wishlistID, final int userID){

        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Cancel Request")
                .setNote("You are cancelling your request.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Confirm")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new WishListCancelPermissionRequest(getContext())
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading...", false, false))
                                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .addParameters(Keys.server.key.INCLUDE, include)
                                .addParameters(Keys.server.key.WISHLIST_ID, wishlistID)
                                .addParameters(Keys.server.key.USER_ID, userID)
                                .execute();
                        confirmationDialog.dismiss();
//                        ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.DiscoverActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.CANCEL_REQUEST);
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("notifcation_RequestFragment_attemptCancelRequest");

    }

    private void attemptRevokeRequest(final int wishlistID, final int userID){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Deny Information Request?")
                .setNote("You are denying this person from requesting your delivery information.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Confirm")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new WishListRevokePermissionRequest(getContext())
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading...", false, false))
                                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .addParameters(Keys.server.key.INCLUDE, include)
                                .addParameters(Keys.server.key.WISHLIST_ID, wishlistID)
                                .addParameters(Keys.server.key.USER_ID, userID)
                                .execute();
                        confirmationDialog.dismiss();
//                        ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.DiscoverActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.DENY_REQUEST);
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("notifcation_RequestFragment_attemptRevokeRequest");
    }

    @Override
    public void onItemClick(WishListViewerItem wishListViewerItem) {
        ((RouteActivity) getContext()).startItemActivity(wishListViewerItem.wishlist.data.id, wishListViewerItem.viewer.data.id, "i_like");
    }

    @Override
    public void onItemClick(WishListTransactionItem wishListTransactionItem) {
        ((RouteActivity) getContext()).startItemActivity(wishListTransactionItem.id, wishListTransactionItem.sender.data.id, "i_received");
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        ((RouteActivity)getActivity()).startProfileActivity(userItem.id);
    }

    @Override
    public void onCancelClick(WishListViewerItem thingsILikeItem) {
        attemptCancelRequest(thingsILikeItem.wishlist.data.id, thingsILikeItem.viewer.data.id);
    }

    @Override
    public void onYesClick(WishListTransactionItem wishListTransactionItem) {
        AppreciateGiftDialog.newInstance("Thank You Message", wishListTransactionItem.id, this).show(getChildFragmentManager(), AppreciateGiftDialog.TAG);
    }

    @Override
    public void onNoClick(WishListTransactionItem wishListTransactionItem) {
        attemptRejectGiftRequest(wishListTransactionItem.id, wishListTransactionItem.sender.data.id);
    }

    @Override
    public void showMore(int viewType) {
        notificationRecycleViewAdapter.showMoreLoading(viewType, true);
        switch (viewType){
            case NotificationRecycleViewAdapter.Data.ADDRESS:
                addressRequest.nextPage();
                break;
            case NotificationRecycleViewAdapter.Data.GIFT:
                giftRequest.nextPage();
                break;
            case NotificationRecycleViewAdapter.Data.ONGOING:
                onGoingRequest.nextPage();
                break;
        }
    }

    @Override
    public void onGrantClick(WishListViewerItem wishListViewerItem) {
//        Log.e("EEEEEEEEEEEEEEE", ">>>> " + wishListViewerItem.wishlist.data.id);
        attemptGrantRequest(wishListViewerItem.wishlist.data.id, wishListViewerItem.viewer.data.id);
    }

    @Override
    public void onDenyClick(WishListViewerItem wishListViewerItem) {
        attemptRevokeRequest(wishListViewerItem.wishlist.data.id, wishListViewerItem.viewer.data.id);
    }


    private void initSuggestionAPI(){

        requestSRL.setColorSchemeResources(R.color.colorPrimary);
        requestSRL.setSwipeableChildren(R.id.requestRV, R.id.placeHolderCON);
        requestSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        requestSRL.setOnRefreshListener(this);

        generalCompactPermissionRequest = new GeneralCompactPermissionRequest(getContext());
        generalCompactPermissionRequest
                .setSwipeRefreshLayout(requestSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .showSwipeRefreshLayout(true)
                .setPerPage(3);
    }

    private void addressAPI(){
        addressRequest = new PermissionAllRequest(getContext());
        addressRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .setPerPage(3);
    }

    private void giftAPI(){
        giftRequest = new PermissionPendingRequest(getContext());
        giftRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .setPerPage(3);
    }

    private void ownedAPI(){
        onGoingRequest = new PermissionOwnedRequest(getContext());
        onGoingRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .setPerPage(3);
    }
    
    private void refreshList(){
        requestRV.setVisibility(View.GONE);
        loadingIV.setVisibility(View.VISIBLE);
        notificationRecycleViewAdapter.clearAddress();
        notificationRecycleViewAdapter.clearGift();
        notificationRecycleViewAdapter.clearOnGoing();
        generalCompactPermissionRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(GeneralCompactPermissionRequest.ServerResponse responseData) {
        CompactTransformer compactTransformer = responseData.getData(CompactTransformer.class);
        if(compactTransformer.status){
            notificationRecycleViewAdapter.addressAddAll(compactTransformer.data.wishlist_viewer);
            notificationRecycleViewAdapter.onGoingAddAll(compactTransformer.data.owned_wishlist_viewer);
            notificationRecycleViewAdapter.giftAddAll(compactTransformer.data.wishlist_transaction);
            notificationRecycleViewAdapter.displayShowMore(NotificationRecycleViewAdapter.Data.ADDRESS, compactTransformer.data.wishlist_viewer_hasmore);
            notificationRecycleViewAdapter.displayShowMore(NotificationRecycleViewAdapter.Data.GIFT, compactTransformer.data.wishlist_transaction_hasmore);
            notificationRecycleViewAdapter.displayShowMore(NotificationRecycleViewAdapter.Data.ONGOING, compactTransformer.data.owned_wishlist_viewer_hasmore);
            notificationRecycleViewAdapter.setRequestCount(compactTransformer.data.requests_count);
            EventBus.getDefault().post(new RequestBadgeCount(notificationRecycleViewAdapter.getRequestCount()));
            showHideView();
        }

        addressRequest.setPage(1);
        addressRequest.setHasMorePage(compactTransformer.data.wishlist_viewer_hasmore);
        giftRequest.setPage(1);
        giftRequest.setHasMorePage(compactTransformer.data.wishlist_transaction_hasmore);
        onGoingRequest.setPage(1);
        onGoingRequest.setHasMorePage(compactTransformer.data.owned_wishlist_viewer_hasmore);
    }

    @Subscribe
    public void onResponse(PermissionAllRequest.ServerResponse responseData) {
        PermissionTransformer permissionTransformer = responseData.getData(PermissionTransformer.class);
        if(permissionTransformer.status){
            notificationRecycleViewAdapter.addressAddAll(permissionTransformer.data);
            notificationRecycleViewAdapter.displayShowMore(NotificationRecycleViewAdapter.Data.ADDRESS, permissionTransformer.has_morepages);
            notificationRecycleViewAdapter.showMoreLoading(NotificationRecycleViewAdapter.Data.ADDRESS, false);
            showHideView();
        }
    }

    @Subscribe
    public void onResponse(PermissionPendingRequest.ServerResponse responseData) {
        WishListTransactionTransformer permissionTransformer = responseData.getData(WishListTransactionTransformer.class);
        if(permissionTransformer.status){
            notificationRecycleViewAdapter.giftAddAll(permissionTransformer.data);
            notificationRecycleViewAdapter.displayShowMore(NotificationRecycleViewAdapter.Data.GIFT, permissionTransformer.has_morepages);
            notificationRecycleViewAdapter.showMoreLoading(NotificationRecycleViewAdapter.Data.GIFT, false);
            showHideView();
        }
    }

    @Subscribe
    public void onResponse(PermissionOwnedRequest.ServerResponse responseData) {
        PermissionTransformer permissionTransformer = responseData.getData(PermissionTransformer.class);
        if(permissionTransformer.status){
            notificationRecycleViewAdapter.onGoingAddAll(permissionTransformer.data);
            notificationRecycleViewAdapter.displayShowMore(NotificationRecycleViewAdapter.Data.ONGOING, permissionTransformer.has_morepages);
            notificationRecycleViewAdapter.showMoreLoading(NotificationRecycleViewAdapter.Data.ONGOING, false);
            showHideView();
        }
    }

    public void showHideView(){
        loadingIV.setVisibility(View.GONE);
        if(notificationRecycleViewAdapter.getItemCount() == 0){
            placeHolderCON.setVisibility(View.VISIBLE);
            requestRV.setVisibility(View.GONE);
        }else {
            placeHolderCON.setVisibility(View.GONE);
            requestRV.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onResponse(WishListGrantPermissionRequest.ServerResponse responseData) {
        WishListInfoViewerTransformer wishlistInfoTransformer = responseData.getData(WishListInfoViewerTransformer.class);
        if(wishlistInfoTransformer.status){
            notificationRecycleViewAdapter.removeAddressItem(wishlistInfoTransformer.wishListViewerItem.id);
            EventBus.getDefault().post(new RequestBadgeCount(notificationRecycleViewAdapter.getRequestCount(-1)));
            showHideView();
            Log.e("EEEEEEEEEEEEEEE", ">>>> " +wishlistInfoTransformer.wishListViewerItem.id);
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }


    @Subscribe
    public void onResponse(WishListRevokePermissionRequest.ServerResponse responseData) {
        WishListInfoViewerTransformer wishlistInfoTransformer = responseData.getData(WishListInfoViewerTransformer.class);
        if(wishlistInfoTransformer.status){
            notificationRecycleViewAdapter.removeAddressItem(wishlistInfoTransformer.wishListViewerItem.id);
            EventBus.getDefault().post(new RequestBadgeCount(notificationRecycleViewAdapter.getRequestCount(-1)));
            showHideView();
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(WishListCancelPermissionRequest.ServerResponse responseData) {
        WishListInfoViewerTransformer wishlistInfoTransformer = responseData.getData(WishListInfoViewerTransformer.class);
        if(wishlistInfoTransformer.status){
            notificationRecycleViewAdapter.removeOnGoingItem(wishlistInfoTransformer.wishListViewerItem.id);
            EventBus.getDefault().post(new RequestBadgeCount(notificationRecycleViewAdapter.getRequestCount(-1)));
            showHideView();
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(WishListReceivePermissionRequest.ServerResponse responseData) {
        WishListInfoTransactionTransformer wishlistInfoTransformer = responseData.getData(WishListInfoTransactionTransformer.class);
        if(wishlistInfoTransformer.status){
            notificationRecycleViewAdapter.removeGiftItem(wishlistInfoTransformer.data.id);
            EventBus.getDefault().post(new RequestBadgeCount(notificationRecycleViewAdapter.getRequestCount(-1)));
            showHideView();
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(WishListRejectGiftPermissionRequest.ServerResponse responseData) {
        WishListInfoTransactionTransformer wishlistInfoTransformer = responseData.getData(WishListInfoTransactionTransformer.class);
        if(wishlistInfoTransformer.status){
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onAccept(int wishListID, String dedication) {
        attemptReceivedRequest(wishListID, dedication);
        Log.e("EEEEEEEEEEEEEEE", ">>>> " +wishListID);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
//            case R.id.viewerBTN:
//                mainActivity.startRequestActivity("viewer");
//                break;
//            case R.id.transactionBTN:
//                mainActivity.startRequestActivity("transaction");
//                break;
//            case R.id.ownedBTN:
//                mainActivity.startRequestActivity("owned");
//                break;
        }
    }

    public class RequestBadgeCount{
        int count;
        public RequestBadgeCount(int count){
            this.count = count;
        }

        public int getCount() {
            return count;
        }
    }
}
