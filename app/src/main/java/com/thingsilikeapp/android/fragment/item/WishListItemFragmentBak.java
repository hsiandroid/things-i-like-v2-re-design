package com.thingsilikeapp.android.fragment.item;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ItemActivity;
import com.thingsilikeapp.android.adapter.RequestOnGoingAdapter;
import com.thingsilikeapp.android.adapter.TimelineAdapter;
import com.thingsilikeapp.android.dialog.SendGiftDialog;
import com.thingsilikeapp.android.dialog.UnfollowDialog;
import com.thingsilikeapp.android.dialog.ViewImageDialog;
import com.thingsilikeapp.android.dialog.WebViewDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.like.LikeRequest;
import com.thingsilikeapp.server.request.report.ReportRequest;
import com.thingsilikeapp.server.request.social.FollowRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.wishlist.BlockRequest;
import com.thingsilikeapp.server.request.wishlist.InfoWishListRequest;
import com.thingsilikeapp.server.request.wishlist.WishListDeleteRequest;
import com.thingsilikeapp.server.request.wishlist.WishListRequestPermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListRevokePermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListSendPermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishlistPermissionRequest;
import com.thingsilikeapp.server.transformer.report.ReportTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.server.transformer.wishlist.PermissionTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoViewerTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Formatter;
import com.thingsilikeapp.vendor.android.java.NumberFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.android.widget.ExpandableHeightListView;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class WishListItemFragmentBak extends BaseFragment implements
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        View.OnLongClickListener,
        SendGiftDialog.Callback,
        UnfollowDialog.Callback {

    public static final String TAG = WishListItemFragmentBak.class.getName();
    private ItemActivity itemActivity;
    private InfoWishListRequest infoWishListRequest;
    private TimelineAdapter timelineAdapter;


    private WishlistPermissionRequest wishlistPermissionRequest;
    private RequestOnGoingAdapter requestOnGoingAdapter;

    @State  int wishListID;
    @State  String include = "info,image,owner.info,owner.social,owner.statistics,delivery_info,transaction,logs,tracker";
    @State  WishListItem wishListItem;

    @BindView(R.id.iLikeIV)             ImageView iLikeIV;
    @BindView(R.id.iLikeTXT)            TextView iLikeTXT;
//    @BindView(R.id.itemNameTXT)         TextView itemNameTXT;
    @BindView(R.id.dateTXT)             TextView dateTXT;
    @BindView(R.id.descriptionTXT)      TextView descriptionTXT;
    @BindView(R.id.avatarCIV)           ImageView avatarCIV;
    @BindView(R.id.usernameTXT)         TextView usernameTXT;
    @BindView(R.id.commonNameTXT)       TextView commonNameTXT;
    @BindView(R.id.contentCON)          View contentCON;
    @BindView(R.id.placeholder404CON)   View placeholder404CON;
    @BindView(R.id.itemSRL)             MultiSwipeRefreshLayout itemSRL;

//    @BindView(R.id.buyItemCON)          View buyItemCON;
    @BindView(R.id.viewOnlineBTN)       View viewOnlineBTN;
    @BindView(R.id.urlTXT)              TextView urlTXT;

    @BindView(R.id.nameTXT)             TextView nameTXT;
    @BindView(R.id.addressTXT)          TextView addressTXT;
    @BindView(R.id.contactTXT)          TextView contactTXT;

    @BindView(R.id.userInfoCON)         View deliveryInfoCON;
    @BindView(R.id.editAddressBTN)      View editAddressBTN;
    @BindView(R.id.quickInfoTXT)        TextView quickInfoTXT;
    @BindView(R.id.lockAddressCON)      View lockAddressCON;
    @BindView(R.id.awaitingTXT)         TextView awaitingTXT;
    @BindView(R.id.viewGiftCardBTN)     View viewGiftCardBTN;

//    @BindView(R.id.sendGiftCON)         View sendGiftCON;
//    @BindView(R.id.requestAddressCON)   View requestAddressCON;
    @BindView(R.id.deliveryCON)         View deliveryCON;
    @BindView(R.id.deleteBTN)           View deleteBTN;
    @BindView(R.id.editBTN)             View editBTN;
    @BindView(R.id.commandCON)          View commandCON;
    @BindView(R.id.commandTXT)          TextView commandTXT;
    @BindView(R.id.loadingPB)           View loadingPB;
    @BindView(R.id.timelineCON)         View timelineCON;
    @BindView(R.id.avatarCON)           View avatarCON;
    @BindView(R.id.informationCON)      View informationCON;

    @BindView(R.id.viewCommentBTN)      TextView viewCommentBTN;
    @BindView(R.id.commentCountTXT)     TextView commentCountTXT;
    @BindView(R.id.showMoreBTN)      TextView viewRequestBTN;
    @BindView(R.id.rePostCountTXT)      TextView rePostCountTXT;
    @BindView(R.id.onGoingCON)          View requestCON;
    @BindView(R.id.commentCON)          View commentCON;
    @BindView(R.id.repostBTN)           View repostCON;
    @BindView(R.id.heartCountTXT)       TextView heartCountTXT;
    @BindView(R.id.commentTXT)          TextView commentTXT;
    @BindView(R.id.heartSB)             SparkButton heartSB;

    @BindView(R.id.requestedUserTXT)        TextView requestedUserTXT;
    @BindView(R.id.requestOnGoingEHLV)      ListView requestOnGoingEHLV;

    @BindView(R.id.timelineEHLV)        ExpandableHeightListView timelineEHLV;

    public static WishListItemFragmentBak newInstance(int id) {
        WishListItemFragmentBak wishListItemFragment = new WishListItemFragmentBak();
        wishListItemFragment.wishListID = id;
        return wishListItemFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_wishlist_item;
    }

    @Override
    public void onViewReady() {
        itemActivity = (ItemActivity) getContext();
        itemActivity.setTitle("");
        itemActivity.getRePostTXT().setOnClickListener(this);

        addressTXT.setOnLongClickListener(this);
        contactTXT.setOnLongClickListener(this);
        nameTXT.setOnLongClickListener(this);

        avatarCIV.setOnClickListener(this);
        avatarCON.setOnClickListener(this);
//        sendGiftCON.setOnClickListener(this);
//        requestAddressCON.setOnClickListener(this);
        viewOnlineBTN.setOnClickListener(this);
        deleteBTN.setOnClickListener(this);
        editBTN.setOnClickListener(this);
        commentCON.setOnClickListener(this);
        repostCON.setOnClickListener(this);
        editAddressBTN.setOnClickListener(this);
        viewRequestBTN.setOnClickListener(this);
        commandTXT.setOnClickListener(this);
        viewCommentBTN.setOnClickListener(this);
        heartCountTXT.setOnClickListener(this);
        viewGiftCardBTN.setOnClickListener(this);
        iLikeIV.setOnClickListener(this);

        setupTimelineListView();
        initSuggestionAPI();

        setupRequestListView();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void displayData() {
        basicInfo();
        ownerInfo();

//        sendGiftCON.setTag(wishListItem);
//        requestAddressCON.setTag(wishListItem.id);
        editBTN.setTag(wishListItem);
        viewGiftCardBTN.setTag(wishListItem);
        editAddressBTN.setTag(wishListItem);
        itemActivity.getRePostTXT().setTag(wishListItem);
        viewRequestBTN.setTag(wishListItem);
        iLikeIV.setTag(R.id.iLikeIV, wishListItem);

        if(wishListItem.comment_count == 1){
            commentCountTXT.setText(wishListItem.for_display_comment + " Comment");
        }else{
            commentCountTXT.setText(NumberFormatter.format(wishListItem.comment_count) + " Comments");
        }

        if(wishListItem.comment_count == 0){
            viewCommentBTN.setText("Be the first to comment");
        }else{
            viewCommentBTN.setText("Add Comment");
        }

        viewCommentBTN.setTag(wishListItem);
        commentCON.setTag(wishListItem);
        repostCON.setTag(wishListItem);

        if (wishListItem.info.data.url != null && !wishListItem.info.data.url.equals("null")) {
//            buyItemCON.setVisibility(View.VISIBLE);
            viewOnlineBTN.setTag(wishListItem.info.data.url);
        } else {
//            buyItemCON.setVisibility(View.GONE);
        }

        boolean isGranted = false;

        switch (wishListItem.delivery_info.data.permission_to_view) {
            case "granted":
//                sendGiftCON.setVisibility(wishListItem.is_sent ? View.GONE : View.VISIBLE);
//                requestAddressCON.setVisibility(View.GONE);

                deliveryInfoCON.setVisibility(View.VISIBLE);

                lockAddressCON.setVisibility(View.GONE);
                awaitingTXT.setVisibility(View.GONE);
                nameTXT.setText(wishListItem.delivery_info.data.recipient);
                addressTXT.setText(wishListItem.delivery_info.data.address);
                contactTXT.setText(wishListItem.delivery_info.data.contact_number);
                quickInfoTXT.setText("Address request granted. Start sending gift!");
                isGranted = true;
                break;
            case "denied":
            case "not_requested":
//                sendGiftCON.setVisibility(View.GONE);
//                requestAddressCON.setVisibility(View.VISIBLE);
                deliveryInfoCON.setVisibility(View.GONE);
                lockAddressCON.setVisibility(View.VISIBLE);
                awaitingTXT.setVisibility(View.GONE);

                quickInfoTXT.setText("Address hidden for privacy purposes.\nRequest user's address below.");
                break;
            case "pending":
//                sendGiftCON.setVisibility(View.GONE);
//                requestAddressCON.setVisibility(View.GONE);

                deliveryInfoCON.setVisibility(View.GONE);
                lockAddressCON.setVisibility(View.VISIBLE);
                awaitingTXT.setVisibility(View.VISIBLE);

                addressTXT.setText(wishListItem.delivery_info.data.address);
                contactTXT.setText(wishListItem.delivery_info.data.contact_number);
                quickInfoTXT.setText("Permission to view delivery address has been sent.");
                break;
        }

        if (wishListItem.transaction == null) {
//            sendGiftCON.setVisibility(View.GONE);
        } else {
            if (wishListItem.transaction.data.id != 0) {
//                sendGiftCON.setVisibility(View.GONE);
                quickInfoTXT.setText("Gift already sent.");
            }
        }

        timelineCON.setVisibility(View.GONE);
        displayUserData(wishListItem.owner.data);
        timelineAdapter.setNewData(wishListItem.tracker.data);
        viewRequestBTN.setText("View All (" + NumberFormatter.format(wishListItem.ongoing_viewer_count) + ")");
        int size = wishListItem.tracker.data.size();
        if(size > 0){
            if(wishListItem.tracker.data.get(size - 1).completed){
                quickInfoTXT.setVisibility(View.GONE);
                if(!isGranted){
                    quickInfoTXT.setVisibility(View.VISIBLE);
                    quickInfoTXT.setText("Address hidden for privacy purposes.");
                }
                awaitingTXT.setVisibility(View.VISIBLE);
                awaitingTXT.setText("Gift already sent and received.");
//                requestAddressCON.setVisibility(View.GONE);
                viewGiftCardBTN.setVisibility(View.VISIBLE);
            }else{
                timelineCON.setVisibility(View.VISIBLE);
            }
        }

        attemptGetOngoingRequest();
    }

    private void basicInfo(){
        Glide.with(getContext())
                .load(wishListItem.image.data.full_path)
                .apply(new RequestOptions()

                        .fitCenter()
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(iLikeIV);

        itemActivity.setTitle(wishListItem.title);
        iLikeTXT.setText(wishListItem.category.toUpperCase());

//        itemNameTXT.setVisibility(wishListItem.same_title ? View.GONE : View.VISIBLE);
        contentCON.setVisibility(View.VISIBLE);

//        itemNameTXT.setText(wishListItem.title);
        dateTXT.setText(wishListItem.time_passed);
        rePostCountTXT.setText(wishListItem.repost_count + " " + String.valueOf(wishListItem.repost_count == 1  ? "repost" : "reposts"));
        commentTXT.setText(wishListItem.comment_count + " " +  String.valueOf(wishListItem.comment_count == 1  ? "comment" : "comments")) ;
        heartCountTXT.setText(wishListItem.for_display_liker + " " + String.valueOf(wishListItem.liker_count == 1  ? "like" : "likes"));

        heartCountTXT.setTag(wishListItem.id);

        heartSB.pressOnTouch(false);
        heartSB.setChecked(wishListItem.is_liked);

        heartSB.setEventListener(new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean buttonState) {
                heartSB.playAnimation();
                attemptHeart(wishListItem);
            }

            @Override
            public void onEventAnimationEnd(ImageView button, boolean buttonState) {

            }

            @Override
            public void onEventAnimationStart(ImageView button, boolean buttonState) {

            }
        });

        if (wishListItem.info.data.url == null){
//            buyItemCON.setVisibility(View.GONE);
        }else {
//            buyItemCON.setVisibility(View.VISIBLE);
            urlTXT.setText(Formatter.getDomainFromURL(wishListItem.info.data.url));
        }

        String url = wishListItem.info.data.content;
        String search = "{link}";

        if (url.contains(search) && url != null && !url.equals("")) {
            url = url.replace(search, "");
            descriptionTXT.setText(url);
        } else {
            descriptionTXT.setText(wishListItem.info.data.content);
        }
    }

    private void ownerInfo(){
        Glide.with(getContext())
                .load(wishListItem.owner.data.getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .dontAnimate()
                .error(R.drawable.placeholder_avatar)
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(avatarCIV);

        avatarCIV.setTag(R.id.avatarCIV, wishListItem.owner.data.id);
        avatarCON.setTag(wishListItem.owner.data.id);

        usernameTXT.setText(VerifiedUserBadge.getFormattedName(getContext(), wishListItem.owner.data.name, wishListItem.owner.data.is_verified, 14));
        commonNameTXT.setText(wishListItem.owner.data.common_name);
    }


    public void displayUserData(UserItem userItem) {


        if (userItem.id == UserData.getUserItem().id) {
            awaitingTXT.setVisibility(View.GONE);
//            requestAddressCON.setVisibility(View.GONE);
//            sendGiftCON.setVisibility(View.GONE);
            quickInfoTXT.setVisibility(View.GONE);
            deleteBTN.setVisibility(View.VISIBLE);
            editBTN.setVisibility(View.VISIBLE);
            commandCON.setVisibility(View.GONE);
            requestCON.setVisibility(View.VISIBLE);
            editAddressBTN.setVisibility(View.VISIBLE);
            informationCON.setVisibility(View.GONE);
            itemActivity.getRePostTXT().setVisibility(View.GONE);
        } else {
            informationCON.setVisibility(View.VISIBLE);
            deleteBTN.setVisibility(View.GONE);
            editBTN.setVisibility(View.GONE);
            commandCON.setVisibility(View.VISIBLE);
            requestCON.setVisibility(View.GONE);
            editAddressBTN.setVisibility(View.GONE);
            deliveryCON.setVisibility(View.VISIBLE);
            itemActivity.getRePostTXT().setVisibility(View.VISIBLE);
        }

        rePostCountTXT.setTag(userItem.id);
        if (userItem.social.data.is_following.equals("yes")) {
            commandTXT.setBackground(ActivityCompat.getDrawable(getContext(), R.drawable.bg_follow));
            commandTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
            commandTXT.setText("Following");
        } else {
            commandTXT.setBackground(ActivityCompat.getDrawable(getContext(), R.drawable.bg_unfollow));
            commandTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
            commandTXT.setText("Follow");
        }

        commandTXT.setTag(userItem);
    }

    public void attemptHeart(WishListItem wishListItem){
        LikeRequest likeRequest = new LikeRequest(getContext());
        likeRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.info,owner.social,owner.statistics,delivery_info,transaction")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                .execute();
    }

    @Subscribe
    public void onResponse(LikeRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            wishListItem = wishListInfoTransformer.wishListItem;
            displayData();
        }
    }

    @Subscribe
    public void onResponse(WishListRevokePermissionRequest.ServerResponse responseData) {
        WishListInfoViewerTransformer wishlistInfoTransformer = responseData.getData(WishListInfoViewerTransformer.class);
        if(wishlistInfoTransformer.status){
            if(wishListItem != null){
                viewRequestBTN.setText("View All (" + wishListItem.ongoing_viewer_count + ")");
                requestOnGoingAdapter.removeData(wishlistInfoTransformer.wishListViewerItem);
                requestedUserTXT.setText("(" + NumberFormatter.format(requestOnGoingAdapter.getCount()) + ")");
            }
        }
    }

    private void setupTimelineListView(){
        timelineAdapter = new TimelineAdapter(getContext());
        timelineEHLV.setAdapter(timelineAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.avatarCON:
                if (v.getTag() != null) {
                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.AVATAR);
                    ((RouteActivity) getContext()).startProfileActivity((int) v.getTag());
                }
                break;
            case R.id.avatarCIV:
                if (v.getTag(R.id.avatarCIV) != null) {
                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.AVATAR);
                    ((RouteActivity) getContext()).startProfileActivity((int) v.getTag(R.id.avatarCIV));
                }
                break;
//            case R.id.sendGiftCON:
//                if (v.getTag() != null) {
//                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.SEND_GIFT);
//                    SendGiftDialog.newInstance("Dedication Message", ((WishListItem) v.getTag()).id, this).show(getChildFragmentManager(), SendGiftDialog.TAG);
//                }
//                break;
//            case R.id.requestAddressCON:
//                if (v.getTag() != null) {
//                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.REQUEST_ADDRESS);
//                    attemptRequestRequest((int) v.getTag());
//                }
//                break;
            case R.id.viewOnlineBTN:
                if (v.getTag() != null) {
                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.BUY_ITEM);
                    WebViewDialog.newInstance((String) v.getTag()).show(getChildFragmentManager(), WebViewDialog.TAG);
                }
                break;
            case R.id.deleteBTN:
                itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.DELETE_WISHLIST);
                deleteWishList();
                break;
            case R.id.editBTN:
                if (v.getTag() != null) {
                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.EDIT_WISHLIST);
                    itemActivity.startWishListActivity((WishListItem) v.getTag(), "edit");
                }
                break;
            case R.id.editAddressBTN:
                if (v.getTag() != null) {
                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.EDIT_ADRESSS);
                    itemActivity.startWishListActivity((WishListItem) v.getTag(), "address");
                }
                break;
            case R.id.rePostBTN:
                otherOptionClick(v);
                itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.GRAB);
                break;
            case R.id.commandTXT:
                if (v.getTag() != null) {
                    loadingPB.setVisibility(View.VISIBLE);
                    commandTXT.setVisibility(View.GONE);
                    commandButtonClick((UserItem) v.getTag());
                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.FOLLOW);
                }
                break;
            case R.id.viewCommentBTN:
                if (v.getTag() != null) {
                    WishListItem i = (WishListItem) v.getTag();
                    ((RouteActivity) getContext()).startCommentActivity(i.id, i.title);
                }
                break;
            case R.id.commentCON:
                if (v.getTag() != null) {
                    WishListItem i1 = (WishListItem) v.getTag();
                    ((RouteActivity) getContext()).startCommentActivity(i1.id, i1.title);
                }
                break;
            case R.id.showMoreBTN:
                if (v.getTag() != null) {
                    WishListItem wishListItem = (WishListItem) v.getTag();
                    ((RouteActivity) getContext()).startRequestActivity(wishListItem.id, wishListItem.ongoing_viewer_count, wishListItem.title, "request");
                }
                break;
            case R.id.rePostCountTXT:
                if (v.getTag() != null) {
                    itemActivity.startSearchActivity((int) v.getTag(), wishListItem.owner.data.common_name, "repost");
                }
                break;
            case R.id.heartCountTXT:
                if (v.getTag() != null) {
                    itemActivity.startSearchActivity((int) v.getTag(), wishListItem.owner.data.common_name, "likes");
                }
                break;
            case R.id.repostBTN:
                if (v.getTag() != null) {
                    WishListItem wishListItem1 = (WishListItem) v.getTag();
                    ((RouteActivity) getContext()).startWishListActivity(wishListItem1, "create_grab_other");
                }
                break;
            case R.id.viewGiftCardBTN:
                if (v.getTag() != null) {
                    WishListItem wish = (WishListItem) v.getTag();
                    itemActivity.openTransactionItem(wish.transaction.data.id, UserData.getUserId());
                }
                break;
            case R.id.iLikeIV:
                if (v.getTag(R.id.iLikeIV) != null) {
                    WishListItem w = (WishListItem) v.getTag(R.id.iLikeIV);
                    ViewImageDialog.newInstance(w.image.data.full_path, w.title).show(getChildFragmentManager(),ViewImageDialog.TAG);
                }
                break;
        }
    }

    private void otherOptionClick(View view) {
        if(wishListItem == null){
            return;
        }

        PopupMenu popup = new PopupMenu(getContext(), view);
        popup.getMenuInflater().inflate(R.menu.social_feed_menu, popup.getMenu());
        popup.getMenu().getItem(2).setVisible(false);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.repostBTN:
                        ((RouteActivity) getContext()).startWishListActivity(wishListItem, "create_grab_other");
                        break;
                    case R.id.hideBTN:
                        onHideClick(wishListItem);
                        break;
                    case R.id.unfollowBTN:
//                        clickListener.onUnfollowClick(wishListItem);
                        break;
                    case R.id.reportBTN:
                        onReportClick(wishListItem);
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    public void onHideClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to hide?")
                .setNote("You will no longer see this post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Hide")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BlockRequest blockRequest = new BlockRequest(getContext());
                        blockRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
    }

    public void onReportClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to report?")
                .setNote("You are about to report a post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Report")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportRequest reportWishlistRequest = new ReportRequest(getContext());
                        reportWishlistRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.TYPE, "wishlist")
                                .addParameters(Keys.server.key.REFERENCE_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
    }

    private void commandButtonClick(UserItem userItem) {
        if (userItem.social.data.is_following.equals("yes")) {
            unFollowUser(userItem);
        } else {
            followUser(userItem);
        }
    }

    public void followUser(UserItem userItem) {
        FollowRequest followRequest = new FollowRequest(getContext());
        followRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, userItem.id)
                .execute();
    }

    public void unFollowUser(UserItem userItem) {
        UnfollowDialog.newInstance(userItem.id, userItem.name, this).show(getChildFragmentManager(), UnfollowDialog.TAG);
    }

    public void deleteWishList() {
        ConfirmationDialog.Builder()
                .setDescription("Are you sure you want to delete this post?")
                .setNote("You cannot recover this post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Confirm")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new WishListDeleteRequest(getContext())
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Deleting Post...", false, false))
                                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .addParameters(Keys.server.key.INCLUDE, include)
                                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                                .execute();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
    }

    private void initSuggestionAPI() {

        itemSRL.setColorSchemeResources(R.color.colorPrimary);
        itemSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        itemSRL.setSwipeableChildren(R.id.contentCON,R.id.placeholder404CON);
        itemSRL.setOnRefreshListener(this);
//
        infoWishListRequest = new InfoWishListRequest(getContext());
        infoWishListRequest
                .setSwipeRefreshLayout(itemSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .showSwipeRefreshLayout(true);
    }

    private void attemptGetOngoingRequest(){
        requestCON.setVisibility(View.GONE);
        wishlistPermissionRequest = new WishlistPermissionRequest(getContext());
        wishlistPermissionRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "wishlist.image,wishlist.owner.info,viewer.info,info,image,sender.info")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .showSwipeRefreshLayout(true)
                .setPerPage(50)
                .first();
    }

    private void setupRequestListView() {
        requestOnGoingAdapter = new RequestOnGoingAdapter(getContext());
        requestOnGoingEHLV.setAdapter(requestOnGoingAdapter);
    }


    private void attemptRequestRequest(int wishListID) {
        new WishListRequestPermissionRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Sending Request...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .execute();
    }

    private void attemptSendRequest(int wishListID, String dedication) {
        new WishListSendPermissionRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Sending Gift...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .addParameters(Keys.server.key.DEDICATION_MESSAGE, dedication)
                .execute();
    }

    private void refreshList() {

        itemActivity.getRePostTXT().setVisibility(View.GONE);
        contentCON.setVisibility(View.GONE);
        placeholder404CON.setVisibility(View.GONE);
        infoWishListRequest
                .showSwipeRefreshLayout(true)
                .execute();

    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(WishlistPermissionRequest.ServerResponse responseData) {
        PermissionTransformer permissionTransformer = responseData.getData(PermissionTransformer.class);
        if(permissionTransformer.status){
            requestOnGoingAdapter.setNewData(permissionTransformer.data);
            requestCON.setVisibility(View.VISIBLE);
            requestedUserTXT.setText("(" + NumberFormatter.format(requestOnGoingAdapter.getCount()) + ")");
        }
    }

    @Subscribe
    public void onResponse(InfoWishListRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            wishListItem = wishListInfoTransformer.wishListItem;
            displayData();
        }

        if(responseData.getCode() == 404){
            contentCON.setVisibility(View.GONE);
            placeholder404CON.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onResponse(WishListRequestPermissionRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            wishListItem = wishListInfoTransformer.wishListItem;
            displayData();
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(WishListSendPermissionRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            wishListItem = wishListInfoTransformer.wishListItem;
            displayData();
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(WishListDeleteRequest.ServerResponse responseData) {
        WishListInfoTransformer baseTransformer = responseData.getData(WishListInfoTransformer.class);
        if (baseTransformer.status) {
            getActivity().finish();
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(BlockRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            itemActivity.finish();
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(ReportRequest.ServerResponse responseData) {
        ReportTransformer reportTransformer = responseData.getData(ReportTransformer.class);
        if (reportTransformer.status) {
            if(reportTransformer.data.is_removed){
                itemActivity.finish();
            }
            ToastMessage.show(getContext(), reportTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getContext(), reportTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onAccept(int wishListID, String dedication) {
        attemptSendRequest(wishListID, dedication);
    }


    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            displayUserData(userTransformer.userItem);
        }
        loadingPB.setVisibility(View.GONE);
        commandTXT.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onResponse(FollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            displayUserData(userTransformer.userItem);
        }
        loadingPB.setVisibility(View.GONE);
        commandTXT.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAccept(int userID) {
        UnFollowRequest unFollowRequest = new UnFollowRequest(getContext());
        unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, userID)
                .execute();
    }

    @Override
    public void onCancel(int userID) {
        if(loadingPB.isShown()){
            loadingPB.setVisibility(View.GONE);
            }
        commandTXT.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.contactTXT:
                copyToClipBoard("Contact", contactTXT.getText().toString());
                break;
            case R.id.addressTXT:
                copyToClipBoard("Address", addressTXT.getText().toString());
                break;
            case R.id.nameTXT:
                copyToClipBoard("Name", nameTXT.getText().toString());
                break;

        }
        return true;
    }

    public void copyToClipBoard(String label, String value){
        ClipboardManager clipboardManager = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText(label, value);
        clipboardManager.setPrimaryClip(clipData);
        ToastMessage.show(getActivity(), label + " Copied to clipboard.", ToastMessage.Status.SUCCESS);
    }
}
