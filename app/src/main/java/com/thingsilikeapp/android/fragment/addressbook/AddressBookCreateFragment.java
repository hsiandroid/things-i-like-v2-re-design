package com.thingsilikeapp.android.fragment.addressbook;

import android.app.ProgressDialog;
import android.support.v4.app.ActivityCompat;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.AddressBookActivity;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.android.dialog.CountryDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.AddressBookItem;
import com.thingsilikeapp.data.preference.CountryData;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.address.CreateAddressRequest;
import com.thingsilikeapp.server.transformer.SingleTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class
AddressBookCreateFragment extends BaseFragment implements
        View.OnClickListener,
        CountryDialog.CountryPickerListener {

    public static final String TAG = AddressBookCreateFragment.class.getName();
    private AddressBookActivity wishListActivity;
    private CountryData.Country countryData;
    private CountryData.Country phoneData;

    @BindView(R.id.nameET)              EditText nameET;
    @BindView(R.id.streetAddressET)     EditText streetAddressET;
    @BindView(R.id.cityET)              EditText cityET;
    @BindView(R.id.stateET)             EditText stateET;
    @BindView(R.id.zipET)               EditText zipET;
    @BindView(R.id.addressLabelET)      EditText addressLabelET;
    @BindView(R.id.countryTXT)          TextView countryTXT;
    @BindView(R.id.contactET)           EditText contactET;
    @BindView(R.id.contactCodeTXT)      TextView contactCodeTXT;
    @BindView(R.id.contactCON)          View contactCON;
    @BindView(R.id.saveBTN)             View saveBTN;
    @BindView(R.id.deleteBTN)           View deleteBTN;
    @BindView(R.id.noteTXT)             TextView noteTXT;
    @BindView(R.id.countryFlagIV)       ImageView countryFlagIV;

    private final int PHONE = 1;
    private final int COUNTRY = 2;

    public static AddressBookCreateFragment newInstance() {
        AddressBookCreateFragment addressBookCreateFragment = new AddressBookCreateFragment();
        return addressBookCreateFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_address_book_create;
    }

    @Override
    public void onViewReady() {
        wishListActivity = (AddressBookActivity) getActivity();
        wishListActivity.setTitle(getString(R.string.address_create_title));
        saveBTN.setOnClickListener(this);
        countryTXT.setOnClickListener(this);
        countryFlagIV.setOnClickListener(this);
        contactCodeTXT.setOnClickListener(this);
        contactCON.setOnClickListener(this);
        deleteBTN.setVisibility(View.GONE);

        displayData();
        setupCountryTextView();
        setUpNote();

        Analytics.trackEvent("addressbook_AddressBookCreateFragment_onViewReady");
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void setUpNote(){
        String text = getString(R.string.edit_profile_note);
        String privacy = "Privacy Policy";
        String terms = "Terms and Conditions";

        SpannableString spanString = new SpannableString(text + " " + privacy + " and " + terms);

        spanString.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(getContext(), R.color.colorPrimary)), text.length(), text.length() + privacy.length() + 1, 0);
        spanString.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(getContext(), R.color.colorPrimary)), text.length() + privacy.length() + 6, text.length() + privacy.length() + 1 + terms.length() + 5, 0);

        ClickableSpan privacyClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                wishListActivity.openUrl("https://thingsilikeapp.com/terms");
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        };

        ClickableSpan termsClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                wishListActivity.openUrl("https://thingsilikeapp.com/terms");
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        };

        spanString.setSpan(privacyClick, text.length(), text.length() + privacy.length() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanString.setSpan(termsClick, text.length() + privacy.length() + 6, text.length() + privacy.length() + 1 + terms.length() + 5, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        noteTXT.setText(spanString);
        noteTXT.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void setupCountryTextView(){
        countryTXT.setInputType(InputType.TYPE_NULL);
        countryTXT.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    showCountryPicker(COUNTRY);

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.saveBTN:
                if(contactET.getText().length() < 10){
                    ToastMessage.show(getContext(), "Invalid contact number", ToastMessage.Status.FAILED);
                }else{
                    attemptCreate();
                }
                Analytics.trackEvent("addressbook_AddressBookCreateFragment_saveBTN");
                break;
            case R.id.countryTXT:
                countryTXT.setError(null);
                showCountryPicker(COUNTRY);
                Analytics.trackEvent("addressbook_AddressBookCreateFragment_countryTXT");
                break;
            case R.id.countryFlagIV:
                Analytics.trackEvent("addressbook_AddressBookCreateFragment_countryFlagIV");
            case R.id.contactCON:
                Analytics.trackEvent("addressbook_AddressBookCreateFragment_contactCON");
            case R.id.contactCodeTXT:
                showCountryPicker(PHONE);
                Analytics.trackEvent("addressbook_AddressBookCreateFragment_contactCodeTXT");
                break;
        }
    }

    private void attemptCreate(){
        nameET.setError(null);
        streetAddressET.setError(null);
        cityET.setError(null);
        stateET.setError(null);
        zipET.setError(null);
        contactET.setError(null);
        countryTXT.setError(null);

        CreateAddressRequest createAddressRequest = new CreateAddressRequest(getContext());
        createAddressRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Creating address...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.ADDRESS_LABEL, addressLabelET.getText().toString())
                .addParameters(Keys.server.key.USER_ID, UserData.getUserId())
                .addParameters(Keys.server.key.RECEIVER_NAME, nameET.getText().toString())
                .addParameters(Keys.server.key.STREET_ADDRESS, streetAddressET.getText().toString())
                .addParameters(Keys.server.key.CITY, cityET.getText().toString())
                .addParameters(Keys.server.key.STATE, stateET.getText().toString())
                .addParameters(Keys.server.key.ZIP_CODE, zipET.getText().toString())
                .addParameters(Keys.server.key.COUNTRY, countryTXT.getText().toString())
                .addParameters(Keys.server.key.COUNTRY_CODE, countryData.code1)
                .addParameters(Keys.server.key.PHONE_NUMBER, contactCodeTXT.getText().toString() + contactET.getText().toString())
                .addParameters(Keys.server.key.PHONE_COUNTRY_DIAL_CODE, phoneData.code3)
                .addParameters(Keys.server.key.PHONE_COUNTRY_CODE, phoneData.code1)
                .execute();
    }

    private void displayData(){
//        countryData = CountryData.getCountryDataByCode1("default");
//        countryTXT.setText(countryData.country);
//
//        phoneData = CountryData.getCountryDataByCode1("default");
//        phoneCode(phoneData);

        countryData = CountryData.getCountryDataByCode1(UserData.getUserItem().info.data.country);
        countryTXT.setText(countryData.country);
        nameET.setText(UserData.getUserItem().common_name);
        contactET.setText(StringFormatter.replaceNull(UserData.getUserItem().info.data.contact_number).replace(countryData.code3, ""));
        contactCodeTXT.setText(countryData.code3);

        phoneData = CountryData.getCountryDataByCode1(UserData.getUserItem().info.data.country);
        phoneCode(phoneData);
    }

    private void phoneCode(CountryData.Country country){
        contactCodeTXT.setText(country.code3);
        Glide.with(getContext())
                .load(country.getFlag())
                .into(countryFlagIV);
    }

    @Override
    public void onSelectCountry(CountryData.Country country, int requestCode) {
        switch (requestCode){
            case PHONE:
                phoneData = country;
                phoneCode(country);
                break;
            case COUNTRY:
                countryData = country;
                countryTXT.setText(country.country);
                contactET.requestFocus();
                contactET.setSelection(contactET.getText().length());
                phoneData = country;
                phoneCode(country);
                break;
        }
    }

    private void showCountryPicker(int requestCode){
        CountryDialog.newInstance(this, requestCode).show(getChildFragmentManager(), CategoryDialog.TAG);
    }

    @Subscribe
    public void onResponse(CreateAddressRequest.ServerResponse responseData) {
        SingleTransformer<AddressBookItem, AddressBookItem.Errors> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            wishListActivity.finish();
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
            if(singleTransformer.hasRequirements()){
                addressLabelET.setError(ErrorResponseManger.first(singleTransformer.errors.address_label));
                nameET.setError(ErrorResponseManger.first(singleTransformer.errors.receiver_name));
                contactET.setError(ErrorResponseManger.first(singleTransformer.errors.phone_number));
                zipET.setError(ErrorResponseManger.first(singleTransformer.errors.zip_code));
                stateET.setError(ErrorResponseManger.first(singleTransformer.errors.state));
                cityET.setError(ErrorResponseManger.first(singleTransformer.errors.city));
                streetAddressET.setError(ErrorResponseManger.first(singleTransformer.errors.street_address));
            }
        }
    }
}
