package com.thingsilikeapp.android.fragment.rewards.exchange;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.adapter.LogsRecycleViewAdapter;
import com.thingsilikeapp.data.model.EncashHistoryItem;
import com.thingsilikeapp.data.model.LogsItem;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;

public class TransactionDetailFragment extends BaseFragment {

    public static final String TAG = TransactionDetailFragment.class.getName();

    private ExchangeActivity exchangeActivity;
    private EncashHistoryItem encashHistoryItem;
    private LogsRecycleViewAdapter bankAccountRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private List<LogsItem> logsItems;

    @BindView(R.id.refTXT) 				TextView refTXT;
    @BindView(R.id.statusTXT)           TextView statusTXT;
    @BindView(R.id.requestDateTXT)      TextView dateTXT;
    @BindView(R.id.releaseDateTXT)      TextView releaseTXT;
    @BindView(R.id.approvalDateTXT)     TextView approvalDate;
    @BindView(R.id.gemTXT)              TextView gemTXT;
    @BindView(R.id.finalAmountTXT)      TextView finalAmountTXT;
    @BindView(R.id.logsRV)              RecyclerView logsRV;
//    @BindView(R.id.serviceFeeTXT)       TextView serviceFeeTXT;
    @BindView(R.id.rateTXT)       		TextView rateTXT;

    public static TransactionDetailFragment newInstance(EncashHistoryItem encashHistoryItem) {
        TransactionDetailFragment fragment = new TransactionDetailFragment();
        fragment.encashHistoryItem = encashHistoryItem;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_transaction_detail;
    }

    @Override
    public void onViewReady() {
        DecimalFormat precision = new DecimalFormat("0.00");
        exchangeActivity = (ExchangeActivity) getContext();
        exchangeActivity.setTitle("Transaction Details");
        refTXT.setText(encashHistoryItem.refno);
        statusTXT.setText(encashHistoryItem.status);
        dateTXT.setText(encashHistoryItem.requestDate);
        releaseTXT.setText(encashHistoryItem.releaseDate);
        approvalDate.setText(encashHistoryItem.approvalDate);
        gemTXT.setText(encashHistoryItem.gemQty);
        finalAmountTXT.setText(encashHistoryItem.currency + " " +  precision.format(Double.parseDouble(encashHistoryItem.finalAmount)));
//        serviceFeeTXT.setText(encashHistoryItem.currency + " " +  precision.format(Double.parseDouble(encashHistoryItem.serviceFee)));
        rateTXT.setText(encashHistoryItem.currency + " " + precision.format(Double.parseDouble(encashHistoryItem.rate)));
        logsItems = encashHistoryItem.history.data;

        switch (encashHistoryItem.status){
            case "close":
                statusTXT.setBackgroundResource(R.color.gray);
                break;
            case "completed":
            case "approved":
                statusTXT.setBackgroundResource(R.color.completed);
                break;
            case "decline":
            case "cancelled":
                statusTXT.setBackgroundResource(R.color.failed);
                break;
            case "pending":
                statusTXT.setBackgroundResource(R.color.gold);
                break;
        }
        setUpRView();
        Analytics.trackEvent("rewards_exchange_TransactionDetailFragment_onViewReady");
    }

    private void setUpRView(){
        bankAccountRecycleViewAdapter = new LogsRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        logsRV.setLayoutManager(linearLayoutManager);
        logsRV.setAdapter(bankAccountRecycleViewAdapter);
        if (logsItems != null){
            bankAccountRecycleViewAdapter.setNewData(logsItems);
        }

    }

}
