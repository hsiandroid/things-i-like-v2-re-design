package com.thingsilikeapp.android.fragment.create_post;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.CreatePostActivity;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.BitmapItem;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.model.ConvertFileItem;
import com.thingsilikeapp.data.model.GalleryItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.wishlist.CreateWishListRequest;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class SendPostListFragment extends BaseFragment implements View.OnClickListener, CategoryDialog.Callback {

    public static final String TAG = SendPostListFragment.class.getName();

    private CreatePostActivity createPostActivity;
    private List<BitmapItem> galleryItems;
    private List<ConvertFileItem> fileItems;

    @BindView(R.id.imageIV)         ImageView imageView;
    @BindView(R.id.titleET)         EditText titleET;
    @BindView(R.id.contentET)       EditText contentET;
    @BindView(R.id.categoryIV)      ImageView categoryIV;
    @BindView(R.id.categoryBTN)     View categoryBTN;
    @BindView(R.id.categoryTXT)     TextView categoryTXT;
    @BindView(R.id.sizeTXT)         TextView sizeTXT;
    @BindView(R.id.nextBTN)         TextView nextBTN;
    @BindView(R.id.mainBackButtonIV)View mainBackButtonIV;
    @BindView(R.id.customTXT)      EditText customTXT;
    @BindView(R.id.customView)      View customView;

    @State Bitmap file;
    @State int size;
    @State String category = "I Just Like It";

    public static SendPostListFragment newInstance(Bitmap file, List<BitmapItem> galleryItems, int size) {
        SendPostListFragment fragment = new SendPostListFragment();
        fragment.file = file;
        fragment.galleryItems = galleryItems;
        fragment.size = size;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_send_post_list;
    }

    @Override
    public void onViewReady() {
        createPostActivity = (CreatePostActivity)getContext();
//        Glide.with(getContext()).load(file).centerCrop().into(imageView);
        imageView.setImageBitmap(file);
        categoryTXT.setOnClickListener(this);
        categoryIV.setOnClickListener(this);
        categoryBTN.setOnClickListener(this);
        categoryTXT.setText("I Just Like It");
        sizeTXT.setText(String.valueOf(size));
        nextBTN.setOnClickListener(this);
        mainBackButtonIV.setOnClickListener(this);
        new CheckTypesTask().execute();
    }

    private class CheckTypesTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog asyncDialog = new ProgressDialog(getContext());
        List<ConvertFileItem> convertedFile = new ArrayList<>();
        File files;
        @Override
        protected void onPreExecute() {
            //set message of the dialog
            asyncDialog.setMessage("Processing...");
            //show dialog
            asyncDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            ConvertFileItem defaultItem;
            for (BitmapItem bitmapItem : galleryItems) {
                files = getImageFile(bitmapItem.bitmap);
                defaultItem = new ConvertFileItem();
                defaultItem.file = files;
                convertedFile.add(defaultItem);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //hide the dialog
            asyncDialog.dismiss();
            fileItems = this.convertedFile;
            super.onPostExecute(result);
        }
    }


    private File getImageFile(Bitmap bitmap) {
        String imageFileName = "ThingsILikeCacheImage";
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    createImageDir()      /* directory */
            );

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
            byte[] bitmapData = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(image);
            fos.write(bitmapData);
            fos.flush();
            fos.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    private File createImageDir(){
        File storageDir = new File(Environment.getExternalStorageDirectory(), "Android/media/com.thingsilikeapp");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        return storageDir;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.categoryTXT:
            case R.id.categoryIV:
            case R.id.categoryBTN:
                categoryTXT.setError(null);
                CategoryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.CATEGORY);
                break;
            case R.id.nextBTN:
                attemptUpload();
                break;
            case R.id.mainBackButtonIV:
                createPostActivity.onBackPressed();
                break;
        }
    }

    public void attemptUpload(){

        CreateWishListRequest createWishListRequest = new CreateWishListRequest(getContext());

        createWishListRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Creating post...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addMultipartBody(Keys.server.key.TITLE, titleET.getText().toString())
//                .addMultipartBody(Keys.server.key.CATEGORY, categoryTXT.getText())
                .addMultipartBody(Keys.server.key.CONTENT, contentET.getText().toString());

        for (ConvertFileItem galleryItem : fileItems) {
            createWishListRequest.addMultipartBody(Keys.server.key.FILES, galleryItem.file);
        }

        if(category.equalsIgnoreCase("Others")){
            createWishListRequest
//                    .addMultipartBody(Keys.server.key.CATEGORY, categoryTXT.getText())
                    .addMultipartBody(Keys.server.key.CATEGORY, customTXT.getText());
        }
        else{
            createWishListRequest
                    .addMultipartBody(Keys.server.key.CATEGORY, categoryTXT.getText());
        }
        createWishListRequest.execute();
    }

    @Subscribe
    public void onResponse(CreateWishListRequest.ServerResponse responseData) {
        Log.e("ResponseCode", ">>>" + responseData.getCode());
        if(responseData.getCode() == 500){
            ToastMessage.show(getActivity(), "Something went wrong. Please try again!", ToastMessage.Status.FAILED);
        }
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if(wishListInfoTransformer.status){
            int count = UserData.getInt(UserData.TOTAL_POST) + 1;
            UserData.insert(UserData.TOTAL_POST, count);
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
            createPostActivity.startMainActivity("main");

        }else{
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
            if(wishListInfoTransformer.hasRequirements()){
                ErrorResponseManger.first(contentET, wishListInfoTransformer.requires.content);
                ErrorResponseManger.first(categoryTXT, wishListInfoTransformer.requires.category);

            }
        }

    }

    @Override
    public void callback(String categoryItem) {
//        categoryTXT.setText(categoryItem.title);
        this.category = categoryItem;
        categoryTXT.setText(categoryItem);
        if(categoryItem.equalsIgnoreCase("Others")){
            customView.setVisibility(View.VISIBLE);

        }
        else{
            categoryTXT.setText(categoryItem);
            customView.setVisibility(View.GONE);
        }
    }
}
