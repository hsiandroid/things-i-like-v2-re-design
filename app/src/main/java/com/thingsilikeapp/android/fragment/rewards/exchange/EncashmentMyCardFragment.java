package com.thingsilikeapp.android.fragment.rewards.exchange;


import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.adapter.CardRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.DeleteCardDialog;
import com.thingsilikeapp.data.model.BankCardItem;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;


public class EncashmentMyCardFragment extends BaseFragment implements View.OnClickListener, CardRecycleViewAdapter.ClickListener,  EndlessRecyclerViewScrollListener.Callback,
        SwipeRefreshLayout.OnRefreshListener, DeleteCardDialog.Callback {


    private RewardsActivity rewardsActivity;
    private CardRecycleViewAdapter adapter;
    private LinearLayoutManager manager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private APIRequest apiRequest;

    @BindView(R.id.addCardBTN)          TextView addCardBTN;
    @BindView(R.id.cardRV)              RecyclerView cardRV;
    @BindView(R.id.cardSRL)             SwipeRefreshLayout cardSRL;

    public static EncashmentMyCardFragment newInstance() {
        EncashmentMyCardFragment encashmentMyCardFragment = new EncashmentMyCardFragment();
        return encashmentMyCardFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_encashment_my_card;
    }

    @Override
    public void onViewReady(){
        rewardsActivity = (RewardsActivity) getContext();
        rewardsActivity.setTitle("My Card");
        rewardsActivity.showWallet(false);
        rewardsActivity.showOption(false);
        rewardsActivity.setTitleDark(false);
        rewardsActivity.showWallet(false);
        addCardBTN.setOnClickListener(this);
        cardSRL.setOnRefreshListener(this);
        cardSRL.setColorSchemeResources(R.color.colorPrimary);
        setUpCard();
        Analytics.trackEvent("rewards_exchange_EncashmentMyCardFragment_onViewReady");
    }

    private void setUpCard(){
        adapter = new CardRecycleViewAdapter(getContext());
        manager = new LinearLayoutManager(getContext());
        cardRV.setLayoutManager(manager);
        cardRV.setAdapter(adapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(manager, this);
        cardRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        adapter.setOnItemClickListener(this);
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void refreshList(){
        apiRequest = Wallet.getDefault().allCard(getContext(), cardSRL);
        apiRequest.first();
    }

    @Subscribe
    public void onResponse(Wallet.AllCardResponse colletionResponse) {
        CollectionTransformer<BankCardItem> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (colletionResponse.isNext()) {
                adapter.addNewData(collectionTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                adapter.setNewData(collectionTransformer.data);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addCardBTN:
                rewardsActivity.openEncashmentCardFragment();
                Analytics.trackEvent("rewards_exchange_EncashmentMyCardFragment_addCardBTN");
                break;

        }
    }

    @Override
    public void onItemClick(BankCardItem bankCardItem) {
        DeleteCardDialog.newInstance(bankCardItem, this).show(getFragmentManager(),DeleteCardDialog.TAG);
    }

    @Override
    public void onItemLongClick(BankCardItem bankCardItem) {

    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onSuccess() {
        refreshList();
    }
}