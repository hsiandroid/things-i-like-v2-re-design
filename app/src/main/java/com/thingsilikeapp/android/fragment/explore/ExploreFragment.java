package com.thingsilikeapp.android.fragment.explore;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MainActivity;
import com.thingsilikeapp.android.adapter.CategoryOwnedRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.HighestRatedRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.HotItemsRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.LikeeeRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.RecentlyPostRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.ThingYouMightRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.TopUserRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.TrendingCRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.TrendingRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.GroupDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.explore.HighestRatedRequest;
import com.thingsilikeapp.server.request.explore.HotRequest;
import com.thingsilikeapp.server.request.explore.LikeeRequest;
import com.thingsilikeapp.server.request.explore.RecentlyRequest;
import com.thingsilikeapp.server.request.explore.RecommendedRequest;
import com.thingsilikeapp.server.request.explore.TrendingRequest;
import com.thingsilikeapp.server.request.explore.TrendingUserRequest;
import com.thingsilikeapp.server.request.social.FollowRequest;
import com.thingsilikeapp.server.request.wishlist.TrendingCategoryRequest;
import com.thingsilikeapp.server.transformer.explore.ExploreTransformer;
import com.thingsilikeapp.server.transformer.explore.TrendingUserTransformer;
import com.thingsilikeapp.server.transformer.social.FeedTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.server.transformer.wishlist.CategoryTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.CustomGridLayoutManager;
import com.thingsilikeapp.vendor.android.java.CustomStaggeredGridLayoutManager;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.decoration.SpacesItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ExploreFragment extends BaseFragment  implements SwipeRefreshLayout.OnRefreshListener,
        LikeeeRecycleViewAdapter.ClickListener, HighestRatedRecycleViewAdapter.ClickListener,
        HotItemsRecycleViewAdapter.ClickListener, ThingYouMightRecycleViewAdapter.ClickListener,
        RecentlyPostRecycleViewAdapter.ClickListener, TopUserRecycleViewAdapter.ClickListener,
        GroupDialog.Callback, CategoryOwnedRecycleViewAdapter.ClickListener, TrendingRecycleViewAdapter.ClickListener, View.OnClickListener{

    public static final String TAG = ExploreFragment.class.getName();

    private MainActivity exploreActivity;

    public static ExploreFragment newInstance(List<UserItem> user, List<WishListItem> likeee,List<WishListItem> recently) {
        ExploreFragment fragment = new ExploreFragment();
        fragment.topuser = user;
        fragment.likeee = likeee;
        fragment.recently = recently;
        return fragment;
    }

    private TopUserRecycleViewAdapter topUserRvAdapter;
    private LikeeeRecycleViewAdapter likeeRVAdapter;
    private HighestRatedRecycleViewAdapter highestRatedRVAdapter;
    private HotItemsRecycleViewAdapter hotItemsRecycleViewAdapter;
    private ThingYouMightRecycleViewAdapter thingYouMightRecycleViewAdapter;
    private RecentlyPostRecycleViewAdapter recentlyPostRecycleViewAdapter;
    private CategoryOwnedRecycleViewAdapter trendingCRecycleViewAdapter;
    private TrendingRecycleViewAdapter trendingRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private SpacesItemDecoration spacesItemDecoration;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private GridLayoutManager gridLayoutManager;

    private TrendingUserRequest trendingUserRequest;
    private LikeeRequest likeeRequest;
    private RecentlyRequest recentlyRequest ;
    private RecommendedRequest recommendedRequest;
    private HotRequest hotRequest;
    private HighestRatedRequest highestRatedRequest;
    private TrendingCategoryRequest categoryRequest;
    private TrendingRequest trendingRequest;

    int ItemPosition;
    UserItem userItem;

    @BindView(R.id.topUserRV)                   RecyclerView topUserRv;
    @BindView(R.id.youMightLikeRV)              RecyclerView youMightLikeRv;
    @BindView(R.id.likeeeRV)                    RecyclerView likeeeRv;
    @BindView(R.id.highestRatedRV)              RecyclerView ratedRV;
    @BindView(R.id.hotItemsRV)                  RecyclerView hotItems;
    @BindView(R.id.postedRV)                    RecyclerView postedRV;
    @BindView(R.id.trendingRV)                  RecyclerView trendingRV;
    @BindView(R.id.trendingCategoryRV)          RecyclerView trendingCategoryRV;
    @BindView(R.id.exploreSRL)                  SwipeRefreshLayout exploreSRL;
    @BindView(R.id.topUserPB)                   ProgressBar topUserPB;
    @BindView(R.id.youMightLikePB)              ProgressBar youMightPB;
    @BindView(R.id.likeePB)                     ProgressBar likeePB;
    @BindView(R.id.highestRatedPB)              ProgressBar ratedPB;
    @BindView(R.id.hotItemsPB)                  ProgressBar hotItemsPB;
    @BindView(R.id.postedPB)                    ProgressBar postedPB;
    @BindView(R.id.trendingItemsPB)             ProgressBar trendingItemsPB;
    @BindView(R.id.searchCON)                   View searchCON;

    private List<UserItem> topuser;
    private List<WishListItem> likeee;
    private List<WishListItem> recently;


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_explore;
    }

    @Override
    public void onViewReady() {
        exploreActivity = (MainActivity)getContext();
        exploreActivity.setTitle("Explore");
        exploreActivity.peopleActive();
        setUpTopUser();
        setUpLikeee();
        setUpPosted();
        setUpRated();
        setUpYouMightLike();
        setUpHotItems();
        setUpTrendingC();
        setUpTrending();
        exploreSRL.setColorSchemeResources(R.color.colorPrimary);
        exploreSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        exploreSRL.setOnRefreshListener(this);
        initTopUser();
        initLikee();
        initRecently();
        initMostRated();
        initRecommended();
        initHot();
        initSuggestionAPI();
        searchCON.setOnClickListener(this);
        Analytics.trackEvent("explore_ExploreFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }


    private void refreshList(){
        trendingUserRequest
                .showSwipeRefreshLayout(true)
                .first();

        likeeRequest
                .showSwipeRefreshLayout(true)
                .first();

        hotRequest
                .showSwipeRefreshLayout(true)
                .first();

        recommendedRequest
                .showSwipeRefreshLayout(true)
                .first();

        recentlyRequest
                .showSwipeRefreshLayout(true)
                .first();

        highestRatedRequest
                .showSwipeRefreshLayout(true)
                .first();

//        trendingRequest
//                .showSwipeRefreshLayout(true)
//                .first();

        initTrending("");

        categoryRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    private void setUpTopUser(){
        topUserRvAdapter = new TopUserRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        topUserRv.setLayoutManager(linearLayoutManager);
        topUserRv.setAdapter(topUserRvAdapter);
        if (topuser!= null){
            topUserRvAdapter.setNewData(topuser);
        }else{
            topUserPB.setVisibility(View.VISIBLE);
        }
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, new EndlessRecyclerViewScrollListener.Callback() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                trendingUserRequest.nextPage();
            }
        });
        topUserRv.addOnScrollListener(endlessRecyclerViewScrollListener);
        topUserRvAdapter.setOnItemClickListener(this);
    }

    private void initTopUser(){
        trendingUserRequest = new TrendingUserRequest(getContext());
        trendingUserRequest
                .setSwipeRefreshLayout(exploreSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,statistic,social")
                .showSwipeRefreshLayout(true)
                .setPerPage(20).first();
    }

    @Subscribe
    public void onResponse(TrendingUserRequest.ServerResponse responseData) {
        TrendingUserTransformer suggestionTransformer = responseData.getData(TrendingUserTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                topUserRvAdapter.addNewData(suggestionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                topUserRvAdapter.setNewData(suggestionTransformer.data);
            }
            topUserPB.setVisibility(View.GONE);

        }
    }

    private void setUpLikeee(){
        likeeRVAdapter = new LikeeeRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        likeeeRv.setLayoutManager(linearLayoutManager);
        likeeeRv.setAdapter(likeeRVAdapter);
        if (likeee != null){
            likeeRVAdapter.setNewData(likeee);
        }else{
            likeePB.setVisibility(View.VISIBLE);
        }
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, new EndlessRecyclerViewScrollListener.Callback() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                likeeRequest.nextPage();
            }
        });
        likeeeRv.addOnScrollListener(endlessRecyclerViewScrollListener);
        likeeRVAdapter.setOnItemClickListener(this);
    }

    private void initLikee(){
        likeeRequest = new LikeeRequest(getContext());
        likeeRequest
                .setSwipeRefreshLayout(exploreSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner,delivery_info,transaction")
                .addParameters(Keys.server.key.CATEGORY, "I Just Like It")
                .showSwipeRefreshLayout(true)
                .setPerPage(20).first();
    }

    @Subscribe
    public void onResponse(LikeeRequest.ServerResponse responseData) {
        ExploreTransformer suggestionTransformer = responseData.getData(ExploreTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                likeeRVAdapter.addNewData(suggestionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                likeeRVAdapter.setNewData(suggestionTransformer.data);
            }

            likeePB.setVisibility(View.GONE);
        }
    }


    private void setUpPosted(){
        recentlyPostRecycleViewAdapter = new RecentlyPostRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        postedRV.setLayoutManager(linearLayoutManager);
        postedRV.setAdapter(recentlyPostRecycleViewAdapter);
        if (recently != null){
            recentlyPostRecycleViewAdapter.setNewData(recently);
        }else{
            postedPB.setVisibility(View.VISIBLE);
        }
        recentlyPostRecycleViewAdapter.setOnItemClickListener(this);
    }

    private void initRecently(){
        recentlyRequest = new RecentlyRequest(getContext());
        recentlyRequest
                .setSwipeRefreshLayout(exploreSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner,delivery_info,transaction")
                .showSwipeRefreshLayout(true)
                .setPerPage(20)
                .execute();
    }

    @Subscribe
    public void onResponse(RecentlyRequest.ServerResponse responseData) {
        ExploreTransformer suggestionTransformer = responseData.getData(ExploreTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                recentlyPostRecycleViewAdapter.addNewData(suggestionTransformer.data);
            }else{
                recentlyPostRecycleViewAdapter.setNewData(suggestionTransformer.data);
            }
            postedPB.setVisibility(View.GONE);
        }
    }

    private void setUpYouMightLike(){
        thingYouMightRecycleViewAdapter = new ThingYouMightRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        youMightLikeRv.setLayoutManager(linearLayoutManager);
        youMightLikeRv.setAdapter(thingYouMightRecycleViewAdapter);
        thingYouMightRecycleViewAdapter.setOnItemClickListener(this);
    }

    private void initRecommended(){
        recommendedRequest = new RecommendedRequest(getContext());
        recommendedRequest
                .setSwipeRefreshLayout(exploreSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner,delivery_info,transaction")
                .showSwipeRefreshLayout(true)
                .setPerPage(20)
                .execute();
    }

    @Subscribe
    public void onResponse(RecommendedRequest.ServerResponse responseData) {
        ExploreTransformer suggestionTransformer = responseData.getData(ExploreTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                thingYouMightRecycleViewAdapter.addNewData(suggestionTransformer.data);
            }else{
                thingYouMightRecycleViewAdapter.setNewData(suggestionTransformer.data);
            }
            youMightPB.setVisibility(View.GONE);
        }
    }

    private void setUpRated(){
        highestRatedRVAdapter = new HighestRatedRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        ratedRV.setLayoutManager(linearLayoutManager);
        ratedRV.setAdapter(highestRatedRVAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, new EndlessRecyclerViewScrollListener.Callback() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                highestRatedRequest.nextPage();
            }
        });
        likeeeRv.addOnScrollListener(endlessRecyclerViewScrollListener);
        highestRatedRVAdapter.setOnItemClickListener(this);
    }

    private void initMostRated(){
        highestRatedRequest = new HighestRatedRequest(getContext());
        highestRatedRequest
                .setSwipeRefreshLayout(exploreSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner,delivery_info,transaction")
                .showSwipeRefreshLayout(true)
                .setPerPage(20)
                .first();
    }

    @Subscribe
    public void onResponse(HighestRatedRequest.ServerResponse responseData) {
        ExploreTransformer suggestionTransformer = responseData.getData(ExploreTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                highestRatedRVAdapter.addNewData(suggestionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                highestRatedRVAdapter.setNewData(suggestionTransformer.data);
            }
            ratedPB.setVisibility(View.GONE);
        }
    }

    private void setUpHotItems(){
        hotItemsRecycleViewAdapter = new HotItemsRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        hotItems.setLayoutManager(linearLayoutManager);
        hotItems.setAdapter(hotItemsRecycleViewAdapter);
        hotItemsRecycleViewAdapter.setOnItemClickListener(this);
    }

    private void initHot(){
        hotRequest = new HotRequest(getContext());
        hotRequest
                .setSwipeRefreshLayout(exploreSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner,delivery_info,transaction")
                .showSwipeRefreshLayout(true)
                .setPerPage(20)
                .execute();
    }

    @Subscribe
    public void onResponse(HotRequest.ServerResponse responseData) {
        ExploreTransformer suggestionTransformer = responseData.getData(ExploreTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                hotItemsRecycleViewAdapter.addNewData(suggestionTransformer.data);
            }else {
                hotItemsRecycleViewAdapter.setNewData(suggestionTransformer.data);
            }
            hotItemsPB.setVisibility(View.GONE);
        }
    }

    private void setUpTrendingC(){
        trendingCRecycleViewAdapter = new CategoryOwnedRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        trendingCategoryRV.setLayoutManager(linearLayoutManager);
        trendingCategoryRV.setAdapter(trendingCRecycleViewAdapter);
        trendingCRecycleViewAdapter.setOnItemClickListener(this);
    }

    @Subscribe
    public void onResponse(TrendingCategoryRequest.ServerResponse responseData) {
        CategoryTransformer categoryTransformer = responseData.getData(CategoryTransformer.class);
        if (categoryTransformer.status) {
            if (responseData.isNext()) {
                trendingCRecycleViewAdapter.addNewData(categoryTransformer.data);
            }else{
                CategoryItem rewardCategoryItem = new CategoryItem();
                rewardCategoryItem.id = 0;
                rewardCategoryItem.title = "All";
                rewardCategoryItem.image = "https://www.catster.com/wp-content/uploads/2017/08/A-fluffy-cat-looking-funny-surprised-or-concerned.jpg";
//                rewardCategoryItem.selected = true;
                categoryTransformer.data.add(0, rewardCategoryItem);
                trendingCRecycleViewAdapter.setNewData(categoryTransformer.data);
            }
        }
    }

    private void initSuggestionAPI(){
        categoryRequest = new TrendingCategoryRequest(getContext());
        categoryRequest
//                .addParameters(Keys.server.key.USER_ID, UserData.getUserId())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(100).execute();
    }


    private void setUpTrending(){
        Log.e("TRY", "Hello");
        trendingRecycleViewAdapter = new TrendingRecycleViewAdapter(getContext());
//        gridLayoutManager = new GridLayoutManager(getContext(), 2);
        staggeredGridLayoutManager = new CustomStaggeredGridLayoutManager(2, OrientationHelper.VERTICAL);
        trendingRV.setLayoutManager(staggeredGridLayoutManager);
        spacesItemDecoration = new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.trending_spacing));
        trendingRV.addItemDecoration(spacesItemDecoration);
        trendingRV.setAdapter(trendingRecycleViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(staggeredGridLayoutManager, new EndlessRecyclerViewScrollListener.Callback() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                trendingRequest.nextPage();
            }
        });
        trendingRecycleViewAdapter.setOnItemClickListener(this);
    }

    private void initTrending(String category){
        trendingRequest = new TrendingRequest(getContext());
        trendingRequest
                .setSwipeRefreshLayout(exploreSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner,delivery_info,transaction")
                .addParameters(Keys.server.key.CATEGORY, category)
                .showSwipeRefreshLayout(true)
                .setPerPage(100)
                .first();
    }

    @Subscribe
    public void onResponse(TrendingRequest.ServerResponse responseData) {
        ExploreTransformer suggestionTransformer = responseData.getData(ExploreTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                trendingRecycleViewAdapter.addNewData(suggestionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                trendingRecycleViewAdapter.setNewData(suggestionTransformer.data);
            }
            trendingItemsPB.setVisibility(View.GONE);
        }
    }




    private List<CategoryItem> addAll(){
        List<CategoryItem> androidModels = new ArrayList<>();
        CategoryItem defaultItem;

        defaultItem = new CategoryItem();
        defaultItem.id = 0;
        defaultItem.title = "ALL";
        defaultItem.image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVMXy_6vlmbiZ0_vZwPf4cp-ZwYADclO5E86gz1zhm9XjvbP0_oA";
        androidModels.add(defaultItem);

        return androidModels;
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startItemActivity(wishListItem.id, wishListItem.owner.data.id, "i_like");
    }

    @Override
    public void onItemLongClick(WishListItem wishListItem) {

    }

    @Override
    public void onItemClick(UserItem userItem) {
        ((RouteActivity) getContext()).startProfileActivity(userItem.id);
    }

    @Override
    public void onFollowClick(UserItem userItem) {
        GroupDialog.newInstance(userItem, this).show(getFragmentManager(),GroupDialog.TAG);
    }

    @Override
    public void onItemLongClick(UserItem userItem) {
    }

    @Override
    public void onGroupSelect(String groupName, int id) {
        FollowRequest followRequest = new FollowRequest(getContext());
        followRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .addParameters(Keys.server.key.USER_ID, id)
                .addParameters(Keys.server.key.GROUP,groupName)
                .execute();
        ItemPosition = id;
    }

    @Subscribe
    public void onResponse(FollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            topUserRvAdapter.removeItem(ItemPosition);
            topUserRvAdapter.notifyItemRemoved(ItemPosition);
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onCancel(String groupName, int id) {

    }

    @Override
    public void onItemClick(CategoryItem categoryItem) {
        if (categoryItem.title.equalsIgnoreCase("ALL")){
            initTrending("");
        }else {
            initTrending(categoryItem.title);
        }

    }

    @Override
    public void onItemLongClick(CategoryItem categoryItem) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.searchCON:
                exploreActivity.openSearchExploreFragment();
                Analytics.trackEvent("comment_CommentFragment_sendBTN");
                break;
        }
    }


}
