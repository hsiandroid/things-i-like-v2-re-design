package com.thingsilikeapp.android.fragment.birthday;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.BirthdayActivity;
import com.thingsilikeapp.android.adapter.BirthdayCelebrantRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.BirthdayGroupRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.AdvanceGreetingDialog;
import com.thingsilikeapp.android.dialog.BirthdayRemindMeDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.BirthdayItem;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.GroupItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.user.GroupRequest;
import com.thingsilikeapp.server.request.user.celebrant.AllCelebrantRequest;
import com.thingsilikeapp.server.transformer.CollectionTransformer;
import com.thingsilikeapp.server.transformer.user.GroupCollectionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.decoration.HeaderItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class TodayCelebrantFragment extends BaseFragment  implements
        SwipeRefreshLayout.OnRefreshListener,
        BirthdayGroupRecycleViewAdapter.ClickListener,
        BirthdayCelebrantRecycleViewAdapter.ClickListener,
        AdvanceGreetingDialog.Callback,
        BirthdayRemindMeDialog.Callback{

    public static final String TAG = TodayCelebrantFragment.class.getName();

    private BirthdayActivity birthdayActivity;
    private BirthdayCelebrantRecycleViewAdapter birthdayCelebrantRVA;
    private LinearLayoutManager celebrantLayoutManager;
    private AllCelebrantRequest allCelebrantRequest;

    private BirthdayGroupRecycleViewAdapter birthdayGroupRecycleViewAdapter;
    private LinearLayoutManager groupLayoutManager;
    private GroupRequest groupRequest;

    @BindView(R.id.celebrantSRL)            SwipeRefreshLayout celebrantSRL;
    @BindView(R.id.celebrantRV)             RecyclerView celebrantRV;
    @BindView(R.id.groupRV)                 RecyclerView groupRV;
    @BindView(R.id.placeHolderCON)          View placeHolderCON;

    public static TodayCelebrantFragment newInstance() {
        TodayCelebrantFragment celebrantFragment = new TodayCelebrantFragment();
        return celebrantFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_celebrant_today;
    }

    @Override
    public void onViewReady() {
        birthdayActivity = (BirthdayActivity) getContext();
        birthdayActivity.setTitle("Today's celebrants");
        setupListView();
        initNotificationAPI();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
        searchGroup("");
    }

    public void setupListView(){
        birthdayCelebrantRVA = new BirthdayCelebrantRecycleViewAdapter(getContext());
        birthdayCelebrantRVA.setOnItemClickListener(this);
        celebrantLayoutManager = new LinearLayoutManager(getContext());
        celebrantRV.addItemDecoration(new HeaderItemDecoration(celebrantRV, birthdayCelebrantRVA));
        celebrantRV.setLayoutManager(celebrantLayoutManager);
        celebrantRV.setAdapter(birthdayCelebrantRVA);

        birthdayGroupRecycleViewAdapter = new BirthdayGroupRecycleViewAdapter(getContext());
        birthdayGroupRecycleViewAdapter.setOnItemClickListener(this);
        groupLayoutManager = new LinearLayoutManager(getContext());
        groupLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        groupRV.setLayoutManager(groupLayoutManager);
        groupRV.setAdapter(birthdayGroupRecycleViewAdapter);
    }

    private void initNotificationAPI(){

        celebrantSRL.setColorSchemeResources(R.color.colorPrimary);
        celebrantSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        celebrantSRL.setOnRefreshListener(this);

//        allCelebrantRequest = new AllCelebrantRequest(getContext());
//        allCelebrantRequest
//                .setSwipeRefreshLayout(celebrantSRL)
//                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .setPerPage(10);

        groupRequest = new GroupRequest(getContext());
        groupRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION));
    }

    private void refreshList(){
        groupRequest.execute();
    }

    private void searchGroup(String group){
        allCelebrantRequest = new AllCelebrantRequest(getContext());
        allCelebrantRequest
                .setSwipeRefreshLayout(celebrantSRL)
                .showSwipeRefreshLayout(true)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social")
                .addParameters(Keys.server.key.GROUP, group)
                .addParameters(Keys.server.key.TYPE, "today")
                .first();
    }

    @Override
    public void onRefresh() {
        refreshList();
        searchGroup("");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(AllCelebrantRequest.ServerResponse responseData) {
        CollectionTransformer<BirthdayItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            birthdayCelebrantRVA.build(collectionTransformer.data);
        }
        if (birthdayCelebrantRVA.getItemCount() == 0){
            placeHolderCON.setVisibility(View.VISIBLE);
            celebrantRV.setVisibility(View.GONE);
        }else {
            placeHolderCON.setVisibility(View.GONE);
            celebrantRV.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onResponse(GroupRequest.ServerResponse responseData) {
        GroupCollectionTransformer collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            collectionTransformer.data.add(0, new GroupItem("ALL"));
            birthdayGroupRecycleViewAdapter.setNewData(collectionTransformer.data);
            birthdayGroupRecycleViewAdapter.setSelected("All");
        }else{
            refreshList();
        }
    }

    @Override
    public void onItemClick(GroupItem groupItem) {
        searchGroup(groupItem.title.equalsIgnoreCase("all") ? "" : groupItem.title);
    }

    @Override
    public void onItemClick(BirthdayItem birthdayItem) {

    }

    @Override
    public void onGreetClick(UserItem userItem, boolean isToday) {
        if(isToday){
            birthdayActivity.startImageEditorActivity(new EventItem(), userItem, "create");
        }else{
            AdvanceGreetingDialog.newInstance(userItem, this).show(getChildFragmentManager(), AdvanceGreetingDialog.TAG);
        }
    }

    @Override
    public void advanceGreeting(UserItem userItem) {
        birthdayActivity.startImageEditorActivity(new EventItem(), userItem, "create");
    }

    @Override
    public void onProfileClick(UserItem userItem) {
        birthdayActivity.startProfileActivity(userItem.id);
    }

    @Override
    public void onSendGiftClick(UserItem userItem) {
        birthdayActivity.startProfileActivity(userItem.id);
    }

    @Override
    public void onRemindClick(UserItem userItem) {
        BirthdayRemindMeDialog.newInstance(userItem, this).show(getChildFragmentManager(), BirthdayRemindMeDialog.TAG);
    }

    @Override
    public void remindMe(UserItem userItem) {
        birthdayCelebrantRVA.updateUserItem(userItem);
    }
}
