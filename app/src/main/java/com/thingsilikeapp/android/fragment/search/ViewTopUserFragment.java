package com.thingsilikeapp.android.fragment.search;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SearchActivity;
import com.thingsilikeapp.android.adapter.SuggestionRecycleViewAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.social.SuggestionRequest;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;

public class ViewTopUserFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        SearchActivity.SearchListener,
        EndlessRecyclerViewScrollListener.Callback,
        SuggestionRecycleViewAdapter.ClickListener{

    public static final String TAG = ViewTopUserFragment.class.getName();
    private SearchActivity searchActivity;
    private SuggestionRequest suggestionRequest;

    private SuggestionRecycleViewAdapter suggestionRecycleViewAdapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.followerLV)      RecyclerView followerLV;
    @BindView(R.id.placeHolderCON)  View placeHolderCON;
    @BindView(R.id.followerSRL)     SwipeRefreshLayout followerSRL;
    @BindView(R.id.thingsPB)        View thingsPB;

    public static ViewTopUserFragment newInstance() {
        ViewTopUserFragment viewTopUserFragment = new ViewTopUserFragment();
        return viewTopUserFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_view_top_user;
    }

    @Override
    public void onViewReady() {
        searchActivity = (SearchActivity) getActivity();
        searchActivity.showSearchContainer(true);
        setupFollowersListView();
        initSuggestionAPI();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void setupFollowersListView() {

        suggestionRecycleViewAdapter = new SuggestionRecycleViewAdapter(getContext());
        suggestionRecycleViewAdapter.setOnItemClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);

        followerLV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        followerLV.addOnScrollListener(endlessRecyclerViewScrollListener);
        followerLV.setLayoutManager(linearLayoutManager);
        followerLV.setAdapter(suggestionRecycleViewAdapter);
    }

    private void initSuggestionAPI(){

        followerSRL.setColorSchemeResources(R.color.colorPrimary);
        followerSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        followerSRL.setOnRefreshListener(this);
        suggestionRecycleViewAdapter.reset();

        suggestionRequest = new SuggestionRequest(getContext());
        suggestionRequest
                .setSwipeRefreshLayout(followerSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showSwipeRefreshLayout(true)
                .setPerPage(20);
    }

    private void refreshList(){
        thingsPB.setVisibility(View.GONE);
        suggestionRecycleViewAdapter.setNewData(new ArrayList<UserItem>());
        suggestionRequest
                .showSwipeRefreshLayout(true)
                .clearParameters()
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.KEYWORD, searchActivity.getSearchEditText().getText().toString())
                .first();
    }

    @Override
    public void onCommandClick(UserItem userItem) {
        if(userItem.social.data.is_following.equals("yes")){
            suggestionRecycleViewAdapter.unFollowUser(userItem);
        }else{
            suggestionRecycleViewAdapter.followUser(userItem);
        }
    }

    @Override
    public void onUserClick(UserItem userItem) {

    }

    @Override
    public void onFollowSuccessClick() {

    }

    @Override
    public void onResume() {
        super.onResume();
        searchActivity.setSearchListener(this);
        refreshList();
    }

    @Override
    public void onPause() {
//        searchActivity.setSearchListener(null);
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(SuggestionRequest.ServerResponse responseData) {
        SuggestionTransformer suggestionTransformer = responseData.getData(SuggestionTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                suggestionRecycleViewAdapter.addNewData(suggestionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                suggestionRecycleViewAdapter.setNewData(suggestionTransformer.data);
            }

            thingsPB.setVisibility(View.GONE);
            if(suggestionRecycleViewAdapter.getItemCount() == 0){
                placeHolderCON.setVisibility(View.VISIBLE);
                followerLV.setVisibility(View.GONE);
            }else{
                placeHolderCON.setVisibility(View.GONE);
                followerLV.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void searchTextChange(final String keyword) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                refreshList();
            }
        });
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        thingsPB.setVisibility(suggestionRequest.hasMorePage() ? View.VISIBLE : View.GONE);
        suggestionRequest.nextPage();
    }
}
