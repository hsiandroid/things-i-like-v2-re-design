package com.thingsilikeapp.android.fragment.registration;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.NewRegistrationActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.server.ValidateNameRequest;
import com.thingsilikeapp.server.request.ValidateRequest;
import com.thingsilikeapp.server.transformer.ValidateTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class RegisterBFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = RegisterBFragment.class.getName();

    private NewRegistrationActivity registrationActivity;

    @BindView(R.id.nextBTN)             TextView nextBTN;
    @BindView(R.id.usernameET)          EditText usernameET;
    @BindView(R.id.nameET)              EditText nameET;
    @BindView(R.id.backBTN)             TextView backBTN;
    @BindView(R.id.merchantBTN)         TextView merchant;

    public static RegisterBFragment newInstance() {
        RegisterBFragment fragment = new RegisterBFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_registration_b;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (NewRegistrationActivity) getContext();
        nextBTN.setOnClickListener(this);
        backBTN.setOnClickListener(this);
        nameET.setFocusable(true);
        merchant.setOnClickListener(this);
//        nameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                validateName();
//            }
//        });
        Analytics.trackEvent("registration_RegisterBFragment_onViewReady");

        usernameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                s.toString().replaceAll(" ", "_").toLowerCase();
            }
        });
        PasswordEditTextManager.usernameFormat(usernameET);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.nextBTN:
                validate();
                validateName();
                Analytics.trackEvent("registration_RegisterBFragment_nextBTN");
                break;
            case R.id.backBTN:
                registrationActivity.startLandingActivity();
                Analytics.trackEvent("registration_RegisterBFragment_backBTN");
                break;

            case R.id.merchantBTN:
                registrationActivity.openBMerchantFragment();
                Analytics.trackEvent("registration_RegisterBFragment_merchantBTN");
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(ValidateNameRequest.ServerResponse responseData) {
        ValidateTransformer validateTransformer = responseData.getData(ValidateTransformer.class);
        if(validateTransformer.status){

        }else{
            if(validateTransformer.hasRequirements()){
                ErrorResponseManger.first(nameET, validateTransformer.requires.value);
            }
        }
    }


    @Subscribe
    public void onResponse(ValidateRequest.ServerResponse responseData) {
        ValidateTransformer validateTransformer = responseData.getData(ValidateTransformer.class);
        if(validateTransformer.status){
            registrationActivity.openCFragment(usernameET.getText().toString(), nameET.getText().toString());
        }else{

            if(validateTransformer.hasRequirements()){
                ErrorResponseManger.first(usernameET, validateTransformer.requires.value);
            }
        }
    }

    private void validate(){
        ValidateRequest createCommentRequest = new ValidateRequest(getContext());
        createCommentRequest
                .showNoInternetConnection(false)
                .addParameters(Keys.server.key.FIELD, "username")
                .addParameters(Keys.server.key.VALUE, usernameET.getText().toString())
                .execute();
    }

    private void validateName(){
        ValidateNameRequest createCommentRequest = new ValidateNameRequest(getContext());
        createCommentRequest
                .showNoInternetConnection(false)
                .addParameters(Keys.server.key.FIELD, "name")
                .addParameters(Keys.server.key.VALUE, nameET.getText().toString())
                .execute();
    }

}
