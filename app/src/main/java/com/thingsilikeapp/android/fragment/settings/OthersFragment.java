package com.thingsilikeapp.android.fragment.settings;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;


public class OthersFragment extends BaseFragment {

    public static final String TAG = OthersFragment.class.getName();

    private SettingsActivity settingsActivity;

    public static OthersFragment newInstance() {
        OthersFragment othersFragment = new OthersFragment();
        return othersFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_others;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle("Others");
    }
}
