package com.thingsilikeapp.android.fragment.messages;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MessageActivity;
import com.thingsilikeapp.android.adapter.GroupFollowersRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.NewCreateMessagesPagerAdapter;
import com.thingsilikeapp.android.fragment.messages.NewCreateMessageFragment;
import com.thingsilikeapp.data.model.ChatModel;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.request.Chat;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class NewCreateMessageFragment extends BaseFragment implements View.OnClickListener, GroupFollowersRecycleViewAdapter.ClickListener {
    public static final String TAG = NewCreateMessageFragment.class.getName();
    private MessageActivity messageActivity;
    private NewCreateMessagesPagerAdapter messagesPagerAdapter;
    private GroupFollowersRecycleViewAdapter groupFollowersRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.landingTL)                   TabLayout landingTL;
    @BindView(R.id.landingVP)                   ViewPager landingVP;
    @BindView(R.id.mainTitleTXT)                TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)            ImageView mainBackButtonIV;
    @BindView(R.id.infoBTN)                     ImageView infoBTN;
    @BindView(R.id.groupNameET)                 EditText groupNameET;
    @BindView(R.id.clearBTN)                    View clearBTN;
    @BindView(R.id.createBTN)                   View createBTN;
    @BindView(R.id.memberCON)                   View memberCON;
    @BindView(R.id.membersRV)                   RecyclerView membersRV;


    public static NewCreateMessageFragment newInstance(int item) {
        NewCreateMessageFragment fragment = new NewCreateMessageFragment();
        fragment.item = item;
        return fragment;
    }

    @State int item = 1;

    @Override
    public void onViewReady() {
        messageActivity = (MessageActivity)getContext();
        messagesPagerAdapter = new NewCreateMessagesPagerAdapter(getChildFragmentManager(), messageActivity);
        landingVP.setAdapter(messagesPagerAdapter);
        landingVP.setCurrentItem(item);
        landingTL.setupWithViewPager(landingVP);
        mainBackButtonIV.setOnClickListener(this);
        mainTitleTXT.setText("New Message");
        infoBTN.setVisibility(View.GONE);
        setupMembersListView();
        setup();
        createBTN.setOnClickListener(this);
        clearBTN.setOnClickListener(this);
        Analytics.trackEvent("messages_NewCreateMessageFragment_onViewReady");
    }

    private void setup(){
        messagesPagerAdapter.callback = new NewCreateMessagesPagerAdapter.Callback() {
            @Override
            public void onSuccess(UserItem userItem) {
                groupFollowersRecycleViewAdapter.getData().add(userItem);
                groupFollowersRecycleViewAdapter.notifyDataSetChanged();
                memberCON.setVisibility(View.GONE);
                membersRV.smoothScrollToPosition(0);
            }
        };
    }

    private void setupMembersListView() {
        groupFollowersRecycleViewAdapter = new GroupFollowersRecycleViewAdapter(getContext());
        groupFollowersRecycleViewAdapter.setOnItemClickListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        membersRV.setLayoutManager(linearLayoutManager);
        membersRV.setAdapter(groupFollowersRecycleViewAdapter);

        if(groupFollowersRecycleViewAdapter.getData().size() > 0){
            memberCON.setVisibility(View.GONE);
        }else {
            memberCON.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_new_create_message;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.mainBackButtonIV:
                messageActivity.onBackPressed();
                Analytics.trackEvent("messages_NewCreateMessageFragment_mainBackButtonIV");
                break;
            case R.id.createBTN:
                Chat.getDefault().createChat(getContext(), groupNameET.getText().toString(), groupFollowersRecycleViewAdapter.getSelectedItems());
                Log.e("SELECTED", groupFollowersRecycleViewAdapter.getSelectedItems());
                Analytics.trackEvent("messages_NewCreateMessageFragment_createBTN");
                break;
            case R.id.clearBTN:
                groupNameET.getText().clear();
                Analytics.trackEvent("messages_NewCreateMessageFragment_clearBTN");
                break;
        }
    }

    @Override
    public void onRemoveClick(int position) {
        groupFollowersRecycleViewAdapter.removeItem(position);
        groupFollowersRecycleViewAdapter.notifyItemRemoved(position);

        if(groupFollowersRecycleViewAdapter.getData().size() > 0){
            memberCON.setVisibility(View.GONE);
        }else {
            memberCON.setVisibility(View.VISIBLE);
        }
        Analytics.trackEvent("messages_NewCreateMessageFragment_onRemoveClick");
    }

    @Override
    public void onItemLongClick(UserItem userItem) {

    }

    @Subscribe
    public void onResponse(Chat.CreateChatResponse responseData) {
        SingleTransformer<ChatModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            groupNameET.setText("");
//            chatActivity.startMainActivity("messenger");
            messageActivity.openMessageFragment();
        }else{
            if(singleTransformer.hasRequirements()){
                ErrorResponseManger.first(groupNameET, singleTransformer.errors.title);
            }
        }
    }
}
