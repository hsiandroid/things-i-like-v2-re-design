package com.thingsilikeapp.android.fragment.rewards;

import android.annotation.SuppressLint;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.adapter.FragmentViewPagerAdapter;
import com.thingsilikeapp.android.dialog.GemsBuyDialog;
import com.thingsilikeapp.android.dialog.GemsReceiveDialog;
import com.thingsilikeapp.android.dialog.GemsSendDialog;
import com.thingsilikeapp.android.dialog.GemsSendsDialog;
import com.thingsilikeapp.android.dialog.WebViewDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.PasscodeVerificationDialog;
import com.thingsilikeapp.android.fragment.rewards.treasury.HistoryFragment;
import com.thingsilikeapp.android.fragment.rewards.treasury.MyRewardsFragment;
import com.thingsilikeapp.android.fragment.rewards.treasury.RewardsCatalogueFragment;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.EncashHistoryItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.server.request.reward.RewardHistoryRequest;
import com.thingsilikeapp.server.request.user.UserInfoRequest;
import com.thingsilikeapp.server.transformer.reward.RewardHistoryTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.GemHelper;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.BaseTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;
import ru.noties.scrollable.CanScrollVerticallyDelegate;
import ru.noties.scrollable.OnFlingOverListener;
import ru.noties.scrollable.OnScrollChangedListener;
import ru.noties.scrollable.OverScrollListener;
import ru.noties.scrollable.OverScrollListenerBase;
import ru.noties.scrollable.ScrollableLayout;

public class TreasuryFragment extends BaseFragment implements
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        GemsBuyDialog.Callback,
        GemsSendsDialog.Callback,
        GemsReceiveDialog.Callback, PasscodeVerificationDialog.Callback{

    public static final String TAG = TreasuryFragment.class.getName();

    private RewardsActivity rewardsActivity;

    private FragmentViewPagerAdapter fragmentViewPagerAdapter;
    private String type = "";
    private UserInfoRequest gemInfoRequest;
//    final ScrollableLayout scrollableLayout = null;


    @BindView(R.id.buyTXT)                      View buyTXT;
    @BindView(R.id.buyIV)                       View buyIV;
    @BindView(R.id.sendBTN)                     View sendBTN;
    @BindView(R.id.receiveBTN)                  View receiveBTN;
    @BindView(R.id.gemTXT)                      TextView gemTXT;
    @BindView(R.id.rewardsCatalogueBTN)         TextView rewardsCatalogueBTN;
    @BindView(R.id.rewardsBTN)                  TextView rewardsBTN;
    @BindView(R.id.historyBTN)                  TextView historyBTN;
    @BindView(R.id.rewardsVP)                   ViewPager rewardsVP;
    @BindView(R.id.rateTXT)                     TextView rateTXT;
    @BindView(R.id.gemSRL)                      SwipeRefreshLayout gemSRL;
    @BindView(R.id.scrollableLayout)            ScrollableLayout scrollableLayout;
    @BindView(R.id.mainCON)                     RelativeLayout mainCON;
    @BindView(R.id.headerCON)                   LinearLayout headerCON;


    @State String currency;
    @State double rate;
    @State boolean password_configured;
    @State boolean locksend;

    public static TreasuryFragment newInstance(String type) {
        TreasuryFragment treasuryFragment = new TreasuryFragment();
        treasuryFragment.type = type;
        return treasuryFragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_treasury_bak;
    }

    @Override
    public void onViewReady() {
        rewardsActivity = (RewardsActivity) getContext();
        refreshList();
        rewardsActivity.setTitle("My Wallet");
        rewardsActivity.showOption(true);
        rewardsActivity.getOptionBTN().setOnClickListener(this);
        rewardsActivity.setTitleDark(false);
        rewardsActivity.showWallet(true);
        rewardsActivity.getWalletBTN().setOnClickListener(this);
        buyTXT.setOnClickListener(this);
        buyIV.setOnClickListener(this);
        sendBTN.setOnClickListener(this);
        receiveBTN.setOnClickListener(this);
        rewardsCatalogueBTN.setOnClickListener(this);
        rewardsBTN.setOnClickListener(this);
        historyBTN.setOnClickListener(this);

        setupViewPager();
        gemSRL.setColorSchemeResources(R.color.colorPrimary);
        gemSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        gemSRL.setOnRefreshListener(this);
        rate = UserData.getUserItem().info.data.rate;
        currency = UserData.getUserItem().info.data.currency;
        rateTXT.setText(String.valueOf(GemHelper.formatGem(String.valueOf(rate)) + " " + currency).toUpperCase() );


        scrollableLayout.setCanScrollVerticallyDelegate(new CanScrollVerticallyDelegate() {
            @Override
            public boolean canScrollVertically(int direction) {

                final View view = rewardsVP;
                return view.canScrollVertically(direction);
            }
            });


        Analytics.trackEvent("rewards_TreasuryFragment_onViewReady");

    }

    public void refreshList(){
        gemInfoRequest = new UserInfoRequest(getContext());
        gemInfoRequest
                .setDeviceRegID(rewardsActivity.getDeviceRegID())
                .setSwipeRefreshLayout(gemSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,wallet")
                .addParameters(Keys.server.key.USER_ID, UserData.getUserId())
                .showSwipeRefreshLayout(true)
                .execute();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rewardsCatalogueBTN:
                rewardsCatalogueActive();
                Analytics.trackEvent("rewards_RedeemableFragment_rewardsCatalogueBTN");
                break;
            case R.id.rewardsBTN:
                rewardsActive();
                Analytics.trackEvent("rewards_RedeemableFragment_rewardsBTN");
                break;
            case R.id.historyBTN:
                historyActive();
                Analytics.trackEvent("rewards_RedeemableFragment_historyBTN");
                break;
            case R.id.buyTXT:
                Analytics.trackEvent("rewards_RedeemableFragment_buyTXT");
            case R.id.buyIV:
                openBuyNow();
                Analytics.trackEvent("rewards_RedeemableFragment_buyIV");
                break;
            case R.id.sendBTN:
                if (password_configured){
                    if (locksend){
                        PasscodeVerificationDialog.Builder(this).show(getFragmentManager(), TAG);
                    }
                    else{
                        openSend();
                    }
                } else{
                    rewardsActivity.startExchangeActivity("set");
                }
//                openSend();
                Analytics.trackEvent("rewards_RedeemableFragment_sendBTN");
                break;
            case R.id.receiveBTN:
                openReceive();
                Analytics.trackEvent("rewards_RedeemableFragment_receiveBTN");
                break;
            case R.id.optionBTN:
                treasuryMenu(this);
                Analytics.trackEvent("rewards_RedeemableFragment_optionBTN");
                break;
            case R.id.walletBTN:
                rewardsActivity.openEncashmentMyCardFragment();
                Analytics.trackEvent("rewards_RedeemableFragment_walletBTN");
                break;
            case R.id.cancelBTN:
                Wallet.getDefault().cancel(getContext());
                Analytics.trackEvent("rewards_RedeemableFragment_cancelBTN");
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        refreshList();
    }



    @SuppressLint("RestrictedApi")
    private void treasuryMenu(final PasscodeVerificationDialog.Callback callback){

        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getContext(), R.style.CustomPopupTheme);
        MenuBuilder menuBuilder = new MenuBuilder(getContext());

        MenuInflater inflater = new MenuInflater(getContext());
        inflater.inflate(R.menu.treasury_menu, menuBuilder);

        grayOut(menuBuilder.findItem(R.id.porfolioBTN));
//        grayOut(menuBuilder.findItem(R.id.exchangeLykaBTN));
//        grayOut(menuBuilder.findItem(R.id.exchangeCashBTN));
//        grayOut(menuBuilder.findItem(R.id.lockBTN));

        MenuPopupHelper optionsMenu = new MenuPopupHelper(contextThemeWrapper, menuBuilder, rewardsActivity.getOptionBTN());
        optionsMenu.setForceShowIcon(true);
        menuBuilder.setCallback(new MenuBuilder.Callback() {
            @Override
            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.exchangeCashBTN:
                        rewardsActivity.startExchangeActivity("exchange");
                        break;
                    case R.id.porfolioBTN:
//                    case R.id.exchangeLykaBTN:
                        break;
                    case R.id.historyBTN:
                        rewardsActivity.startExchangeActivity("history");
                        break;
                    case R.id.infoBTN:
                        WebViewDialog.newInstance("http://www.mylyka.com/general-info").show(getChildFragmentManager(), WebViewDialog.TAG);

                        break;
                    case R.id.verificationBTN:
                        rewardsActivity.startVerificationActivity("verification");
                        break;
                    case R.id.lockBTN:
                        if (password_configured){
                            if (locksend){
                                PasscodeVerificationDialog.Builder(callback).show(getFragmentManager(), TAG);
                            }
                            else{
                                rewardsActivity.startExchangeActivity("set");
                            }
                        }else{
                            rewardsActivity.startExchangeActivity("set");
                        }


//                if (password_configured){
//                    if (locksend){
//                        PasscodeVerificationDialog.Builder(this).show(getFragmentManager(), TAG);
//                    }
//                    else{
//                        openSend();
//                    }
//                }else{
//                    rewardsActivity.startExchangeActivity("set");
//                }
                        break;
                    case R.id.changePassBTN:
                        if (password_configured){
                            rewardsActivity.startExchangeActivity("change_pass");
                        }else{
                            ToastMessage.show(getContext(), "There is no passcode set.", ToastMessage.Status.FAILED);
                            rewardsActivity.startExchangeActivity("set");
                        }

                        break;
                    default:
                        WebViewDialog.newInstance("http://www.mylyka.com/general-info").show(getChildFragmentManager(), WebViewDialog.TAG);
//                    rewardsActivity.openGeneralInfoFragment();
                }
                return true;
            }

            @Override
            public void onMenuModeChange(MenuBuilder menu) {}
        });

        optionsMenu.show();

//        ContextThemeWrapper ctw = new ContextThemeWrapper(getContext(), R.style.CustomPopupTheme);
//        PopupOtherOption popupOtherOption =  PopupOtherOption.newInstance(getContext(), ctw, rewardsActivity.getOptionBTN());
//        popupOtherOption.setOnEditText(false);
//        popupOtherOption.setForceShowIcon();
//        popupOtherOption.addItem(2, "Lock Send Gems" , ActivityCompat.getDrawable(getContext(), R.drawable.ic_pass_lock));
//        popupOtherOption.addItem(1, "General Information", ActivityCompat.getDrawable(getContext(), R.drawable.ic_info));
//        popupOtherOption.setOnMenuItemClickListener(new PopupOtherOption.MenuItemClickListener() {
//            @Override
//            public void onMenuItemClick(MenuItem item) {
//                switch (item.getItemId()){
//                    case 1:
//                        rewardsActivity.openGeneralInfoFragment();
//                        break;
//                }
//            }
//        });
//        popupOtherOption.show();
    }

    private void grayOut(MenuItem menuItem){
        String report = menuItem.getTitle().toString();
        SpannableString spannableString = new SpannableString(report);
        spannableString.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(getContext(), R.color.white_dark_gray)), 0, spannableString.length(), 0);
        menuItem.setTitle(spannableString);
    }

    private void openBuyNow(){
        GemsBuyDialog.newInstance(this).show(getFragmentManager(), TAG);
    }

    private void openSend(){
        GemsSendsDialog.newInstance(this).show(getFragmentManager(), GemsSendDialog.TAG);
    }

    private void openReceive(){
        GemsReceiveDialog.newInstance(this).show(getFragmentManager(), GemsReceiveDialog.TAG);
    }

    private void setupViewPager(){
        fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getChildFragmentManager());
        fragmentViewPagerAdapter.addFragment(HistoryFragment.newInstance());
        fragmentViewPagerAdapter.addFragment(RewardsCatalogueFragment.newInstance());
        fragmentViewPagerAdapter.addFragment(MyRewardsFragment.newInstance());
        rewardsVP.setPageMargin(20);
        rewardsVP.setPageMarginDrawable(null);
        rewardsVP.setAdapter(fragmentViewPagerAdapter);
        rewardsVP.addOnPageChangeListener(new ViewPager.OnPageChangeListener(){


            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        historyActive();
                        break;
                    case 1:
                        rewardsCatalogueActive();
                        break;
                    case 2:
                        rewardsActive();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        rewardsVP.setOffscreenPageLimit(2);
        if (type.equalsIgnoreCase("treasury")){
            rewardsActive();
        } else {
            historyActive();
        }
    }

    public void historyActive(){
        historyBTN.setSelected(true);
        rewardsCatalogueBTN.setSelected(false);
        rewardsBTN.setSelected(false);
        rewardsVP.post(new Runnable() {
            @Override
            public void run() {
                if(rewardsVP != null){
                    rewardsVP.setCurrentItem(0);
                }
            }
        });
    }

    public void rewardsActive(){
        rewardsBTN.setSelected(true);
        rewardsCatalogueBTN.setSelected(false);
        historyBTN.setSelected(false);
        rewardsVP.post(new Runnable() {
            @Override
            public void run() {
                if(rewardsVP != null){
                    rewardsVP.setCurrentItem(2);
                }
            }
        });
    }

    public void rewardsCatalogueActive(){
        rewardsCatalogueBTN.setSelected(true);
        rewardsBTN.setSelected(false);
        historyBTN.setSelected(false);
        rewardsVP.post(new Runnable() {
            @Override
            public void run() {
                if(rewardsVP != null){
                    rewardsVP.setCurrentItem(1);
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(RewardHistoryRequest.ServerResponse responseData) {
        RewardHistoryTransformer rewardHistoryTransformer = responseData.getData(RewardHistoryTransformer.class);
        if(rewardHistoryTransformer.status){
            UserData.updateRewardCrystal(rewardHistoryTransformer.user.reward_crystal + "");
            UserData.updateRewardFragment(rewardHistoryTransformer.user.reward_fragment + "");
            UserData.updateDisplayRewardCrystal(rewardHistoryTransformer.user.lykagem_display + "");
            UserData.updateRewardRatio(rewardHistoryTransformer.ratio);
            gemTXT.setText(UserData.getDisplayCrystalFragment());
        }
    }

    @Subscribe
    public void onResponse(UserInfoRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            UserData.updateRewardCrystal(userTransformer.userItem.reward_crystal + "");
            UserData.updateDisplayRewardCrystal(userTransformer.userItem.lykagem_display + "");
            this.locksend = userTransformer.userItem.wallet.data.lockSend;
            this.password_configured = userTransformer.userItem.wallet.data.passcodeConfigured;
            Log.e("EEEEEEEEE", ">>>> " + userTransformer.userItem.wallet.data.passcodeConfigured + " >>>> " + userTransformer.userItem.wallet.data.lockSend);
            gemTXT.setText(UserData.getDisplayCrystalFragment());
        }else{
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onSuccess() {
        refreshList();
    }

    @Override
    public void onGemBuySuccess() {
        refreshList();
    }

    @Override
    public void onReceiveCallback() {
        refreshList();
    }

    @Override
    public void onUnlock() {
        refreshList();
        openSend();
    }

}