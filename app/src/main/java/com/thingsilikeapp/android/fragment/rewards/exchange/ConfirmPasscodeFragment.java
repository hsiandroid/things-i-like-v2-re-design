package com.thingsilikeapp.android.fragment.rewards.exchange;

import android.view.View;
import android.widget.TextView;

import com.goodiebag.pinview.Pinview;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.dialog.defaultdialog.PasscodeVerificationDialog;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.KYC;
import com.thingsilikeapp.server.Passcode;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class ConfirmPasscodeFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = ConfirmPasscodeFragment.class.getName();

    public static ConfirmPasscodeFragment newInstance(String passcode) {
        ConfirmPasscodeFragment fragment = new ConfirmPasscodeFragment();
        fragment.passcode = passcode;
        return fragment;
    }

    @BindView(R.id.addBTN)        TextView addBTN;
    @BindView(R.id.pinView)       Pinview pinview;

    @State String passcode;

    private ExchangeActivity exchangeActivity;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_confirm_passcode;
    }

    @Override
    public void onViewReady() {
        exchangeActivity = (ExchangeActivity)getContext();
        exchangeActivity.setTitle("Confirm Your Wallet Passcode");
        addBTN.setOnClickListener(this);
        Analytics.trackEvent("rewards_exchange_ConfirmPasscodeFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Subscribe
    public void onResponse(Passcode.PasscodeResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            Passcode.getDefault().lock_gem(getContext());
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Passcode.UnlockResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            exchangeActivity.startRewardsActivity("treasury");
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.addBTN:
                if (!passcode.equals(pinview.getValue())){
                    ToastMessage.show(getContext(), "Passcode mismatched", ToastMessage.Status.FAILED);
                }else{
                    Passcode.getDefault().passcode(getContext(), pinview.getValue());
                }
                Analytics.trackEvent("rewards_exchange_ConfirmPasscodeFragment_addBTN");
                break;
        }
    }
}
