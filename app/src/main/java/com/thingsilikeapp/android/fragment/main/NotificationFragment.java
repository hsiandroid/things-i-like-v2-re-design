package com.thingsilikeapp.android.fragment.main;

import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.DiscoverActivity;
import com.thingsilikeapp.android.adapter.FragmentViewPagerAdapter;
import com.thingsilikeapp.android.fragment.notification.ActivityFragment;
import com.thingsilikeapp.android.fragment.notification.RequestFragment;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.BadgeUtils;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import cn.nekocode.badge.BadgeDrawable;
import icepick.State;

public class NotificationFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = NotificationFragment.class.getName();
    private DiscoverActivity mainActivity;

    private FragmentViewPagerAdapter fragmentViewPagerAdapter;

    @BindView(R.id.notificationVP)      ViewPager notificationVP;
    @BindView(R.id.giftBTN)             View giftBTN;
    @BindView(R.id.giftTXT)             TextView giftTXT;
    @BindView(R.id.activityBTN)         TextView activityBTN;
    @BindView(R.id.badgeIV)             ImageView badgeIV;

    public static final int REQUEST_VIEW = 1;
    public static final int ACTIVITY_VIEW = 2;

    @State int selectedView;

    public static NotificationFragment newInstance(int selectedView) {
        NotificationFragment notificationFragment = new NotificationFragment();
        notificationFragment.selectedView = selectedView;
        return notificationFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_notification;
    }

    @Override
    public void onViewReady() {
        mainActivity = (DiscoverActivity) getContext();
//        mainActivity.setTitle(getString(R.string.title_main_notification));

//        mainActivity.showBackButton(true);
//        mainActivity.setSettingsVisibility(true);
//        mainActivity.setFeedsVisibility(false);
//        mainActivity.setSearchVisibility(false);
//        mainActivity.setEventVisibility(true);
//        mainActivity.setTitleVisibility(true);

//        mainActivity.createYourOwnPostLabelVisibility(false);
        mainActivity.setTitle("Notifications");
        giftBTN.setOnClickListener(this);
        activityBTN.setOnClickListener(this);

        setupViewPager();
        Analytics.trackEvent("main_NotificationFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
//        mainActivity.notificationActive();
        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)){
//            mainActivity.attemptGetCurrentWalkThrough();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.giftBTN:
                giftActive();
//                mainActivity.facebookLogger(FacebookCustomLoggerMessage.DiscoverActivity.EVENT,FacebookCustomLoggerMessage.DiscoverActivity.REQUEST);
                Analytics.trackEvent("main_NotificationFragment_giftBTN");
                break;
            case R.id.activityBTN:
                activityActive();
//                mainActivity.facebookLogger(FacebookCustomLoggerMessage.DiscoverActivity.EVENT,FacebookCustomLoggerMessage.DiscoverActivity.ACTIVITY);
                Analytics.trackEvent("main_NotificationFragment_activityBTN");
                break;
        }
    }

    private void setupViewPager(){
        fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getChildFragmentManager());
        fragmentViewPagerAdapter.addFragment(RequestFragment.newInstance());
        fragmentViewPagerAdapter.addFragment(ActivityFragment.newInstance(this));
        notificationVP.setPageMargin(20);
        notificationVP.setPageMarginDrawable(null);
        notificationVP.setAdapter(fragmentViewPagerAdapter);
        notificationVP.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        giftBTN.setSelected(true);
                        activityBTN.setSelected(false);
                        giftTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
                        activityBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        break;
                    case 1:
                        giftBTN.setSelected(false);
                        activityBTN.setSelected(true);
                        giftTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        activityBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        notificationVP.setOffscreenPageLimit(2);

        switch (selectedView){
            case REQUEST_VIEW:
                giftActive();
                break;
            case ACTIVITY_VIEW:
                activityActive();
                break;
        }
    }

    public void giftActive(){
        giftBTN.setSelected(true);
        activityBTN.setSelected(false);
        giftTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
        activityBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        notificationVP.post(new Runnable() {
            @Override
            public void run() {
                if(notificationVP != null){
                    notificationVP.setCurrentItem(0);
                }
            }
        });
    }

    public void activityActive(){
        giftBTN.setSelected(false);
        activityBTN.setSelected(true);
        giftTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        activityBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
        notificationVP.post(new Runnable() {
            @Override
            public void run() {
                if(notificationVP != null){
                    notificationVP.setCurrentItem(1);
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(RequestFragment.RequestBadgeCount requestBadgeCount) {
        badgeCount(requestBadgeCount.getCount());
    }

    private void badgeCount(final int count){
        BadgeUtils.setBadge(getContext(), count);
        if(count == 0){
            if(badgeIV != null){
                badgeIV.setVisibility(View.GONE);
            }
            return;
        }

        String newCount;

        if(count >= 100){
            newCount = "99+";
        }else{
            newCount = String.valueOf(count);
        }

        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getContext().getResources().getDisplayMetrics());

        if(badgeIV != null){
            badgeIV.setVisibility(View.VISIBLE);
            badgeIV.setImageDrawable(new BadgeDrawable.Builder()
                    .type(BadgeDrawable.TYPE_ONLY_ONE_TEXT)
                    .number(count)
                    .text1(" " + newCount + " ")
                    .textSize(px)
                    .build());
        }
    }
}
