package com.thingsilikeapp.android.fragment.settings;


import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;


import com.thingsilikeapp.android.adapter.ReportedPostRecyclerViewAdapter;
import com.thingsilikeapp.android.dialog.SettingDialog;
import com.thingsilikeapp.android.dialog.UnReportDialog;
import com.thingsilikeapp.data.model.HidePostItem;
import com.thingsilikeapp.data.model.ReportPostItem;
import com.thingsilikeapp.server.HidePost;
import com.thingsilikeapp.server.ReportPost;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ReportedPostsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        EndlessRecyclerViewScrollListener.Callback, ReportedPostRecyclerViewAdapter.ClickListener, UnReportDialog.Callback{

    public static final String TAG = ReportedPostsFragment.class.getName();

    private SettingsActivity settingsActivity;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;
    private ReportedPostRecyclerViewAdapter reportedPostRecyclerViewAdapter;

    private APIRequest apiRequest;

    @BindView(R.id.reportRV)               RecyclerView reportRV;
    @BindView(R.id.reportSRL)              SwipeRefreshLayout reportSRL;



    public static ReportedPostsFragment newInstance() {
        ReportedPostsFragment reportedPostsFragment = new ReportedPostsFragment();
        return reportedPostsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_reported_post;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle("Reported Posts");
        setupBlockListView();
        initSuggestionAPI();
        reportSRL.setOnRefreshListener(this);
//        onRefresh();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void setupBlockListView() {
        reportedPostRecyclerViewAdapter = new ReportedPostRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        reportRV.setLayoutManager(linearLayoutManager);
        reportRV.setAdapter(reportedPostRecyclerViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager,
                this);
        reportRV.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        reportRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        reportRV.setLayoutManager(linearLayoutManager);
        reportedPostRecyclerViewAdapter.setOnItemClickListener(this);
    }

    private void refreshList(){
//        endlessRecyclerViewScrollListener.reset();
       apiRequest =  ReportPost.getDefault().getReportPost(getContext(), reportSRL);
       apiRequest.first();
//        followersRequest
//                .showSwipeRefreshLayout(true)
//                .clearParameters()
//                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
//                .addParameters(Keys.server.key.USER_ID, userID)
//                .addParameters(Keys.server.key.KEYWORD, searchET.getText().toString())
//                .first();
    }

    private void initSuggestionAPI(){
        reportSRL.setColorSchemeResources(R.color.colorPrimary);
        reportSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        reportSRL.setOnRefreshListener(this);
    }

    @Subscribe
    public void onResponse(ReportPost.AllOrderResponse colletionResponse) {
        CollectionTransformer<ReportPostItem> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (colletionResponse.isNext()) {
                reportedPostRecyclerViewAdapter.addNewData(collectionTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                reportedPostRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
        }

    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onItemClick(ReportPostItem reportPostItem) {

    }

    @Override
    public void onUnreportPost(ReportPostItem reportPostItem) {
        UnReportDialog.newInstance(reportPostItem, this).show(getFragmentManager(),TAG);
        refreshList();
    }

    @Override
    public void onItemLongClick(ReportPostItem reportPostItem) {

    }

    @Override
    public void onSuccess() {
        refreshList();
    }
}
