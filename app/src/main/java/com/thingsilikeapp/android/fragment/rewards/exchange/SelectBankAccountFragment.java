package com.thingsilikeapp.android.fragment.rewards.exchange;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.adapter.BankAccountRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.CustomAdapter;
import com.thingsilikeapp.android.dialog.ExchangeToCashDialog;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SelectBankAccountFragment extends BaseFragment implements BankAccountRecycleViewAdapter.ClickListener,
        View.OnClickListener,
        EndlessRecyclerViewScrollListener.Callback,
        SwipeRefreshLayout.OnRefreshListener, AddBankAccountFragment.Callback{

    public static final String TAG = SelectBankAccountFragment.class.getName();

    public static SelectBankAccountFragment newInstance(String currency, String amount, String totalAmount, String serviceFee, String rate, String conversion) {
        SelectBankAccountFragment fragment = new SelectBankAccountFragment();
        fragment.currency = currency;
        fragment.amount = amount;
        fragment.totalAmount = totalAmount;
        fragment.serviceFee = serviceFee;
        fragment.conversion = rate;
        fragment.rate = conversion;
        return fragment;
    }

    private BankAccountRecycleViewAdapter bankAccountRecycleViewAdapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.addBankAccountBTN)        TextView addbankAccountTXT ;
    @BindView(R.id.accountsRV)               RecyclerView accountsRV;
    @BindView(R.id.bankSRL)                  SwipeRefreshLayout bankSRL;

    @State String currency;
    @State String amount;
    @State String totalAmount;
    @State String serviceFee;
    @State String conversion;
    @State String rate;

    private ExchangeActivity exchangeActivity;
    private APIRequest apiRequest;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_select_bank;
    }

    @Override
    public void onViewReady() {
        exchangeActivity = (ExchangeActivity)getContext();
        exchangeActivity.setTitle("Select Bank Account");
        setUpRView();
        bankSRL.setOnRefreshListener(this);
        addbankAccountTXT.setOnClickListener(this);
        Analytics.trackEvent("rewards_exchange_SelectBankAccountFragment_onViewReady");
    }

    private void setUpRView(){
        bankAccountRecycleViewAdapter = new BankAccountRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        accountsRV.setLayoutManager(linearLayoutManager);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        accountsRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        accountsRV.setAdapter(bankAccountRecycleViewAdapter);
        bankAccountRecycleViewAdapter.setOnItemClickListener(this);
    }

    @Subscribe
    public void onResponse(Wallet.AllBankResponse colletionResponse) {
        CollectionTransformer<BankItem> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (colletionResponse.isNext()) {
                bankAccountRecycleViewAdapter.addNewData(collectionTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                bankAccountRecycleViewAdapter.setNewData(collectionTransformer.data);
            }
        }
    }

    @Subscribe
    public void onResponse(Wallet.RemoveBankResponse responseData) {
        SingleTransformer<BankItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            refreshList();
        }else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    private void refreshList(){
        apiRequest = Wallet.getDefault().allBank(getContext(), bankSRL);
        apiRequest.first();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onItemClick(BankItem bankItem) {
        ExchangeToCashDialog.Builder(bankItem, currency, amount, totalAmount, serviceFee, rate, conversion).show(getFragmentManager(), TAG);
    }

    @Override
    public void onItemLongClick(BankItem bankItem) {

    }

    @Override
    public void onEditClick(BankItem bankItem) {
        exchangeActivity.openEditBankFragment(bankItem, currency, amount, totalAmount, serviceFee, rate, conversion);
   }

    @Override
    public void onRemoveClick(BankItem bankItem) {
        Wallet.getDefault().RemoveBank(getContext(), bankItem.id);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addBankAccountBTN:
                exchangeActivity.openAddBankFragment(currency, amount, totalAmount, serviceFee, rate, conversion, this);
                Analytics.trackEvent("rewards_exchange_SelectBankAccountFragment_addBankAccountBTN");
                break;
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onSuccess(String currency, String amount, String totalAmount, String serviceFee, String rate, String conversion) {

    }
}
