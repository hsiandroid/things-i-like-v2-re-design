package com.thingsilikeapp.android.fragment.rewards;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.adapter.RedeemableRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.RewardCategoryRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.RedeemRewardConfirmationDialog;
import com.thingsilikeapp.android.dialog.RedeemRewardFailedDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.CatalogueItem;
import com.thingsilikeapp.data.model.RewardCategoryItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.reward.RewardCatalogueCategoryRequest;
import com.thingsilikeapp.server.request.reward.RewardCatalogueRequest;
import com.thingsilikeapp.server.transformer.reward.RewardCatalogueCategoryTransformer;
import com.thingsilikeapp.server.transformer.reward.RewardCatalogueTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.CustomGridLayoutManager;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.GemHelper;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class RedeemableFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        RedeemableRecycleViewAdapter.ClickListener,
        EndlessRecyclerViewScrollListener.Callback,
        EndlessRecyclerViewScrollListener.HideShowListener,
        RewardCategoryRecycleViewAdapter.ClickListener{

    public static final String TAG = RedeemableFragment.class.getName();

    private RewardsActivity rewardsActivity;
    private RewardCatalogueCategoryRequest rewardCatalogueCategoryRequest;
    private RewardCategoryRecycleViewAdapter rewardCategoryRecycleViewAdapter;
    private LinearLayoutManager rewardCategoryLLM;

    private RedeemableRecycleViewAdapter redeemableRecycleViewAdapter;
    private CustomGridLayoutManager customStaggeredGridLayoutManager;
    private RewardCatalogueRequest rewardCatalogueRequest;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;

    long delay = 1000; // 1 seconds after user stops typing
    long last_text_edit = 0;
    Editable editables;


    @BindView(R.id.redeemableSRL)             MultiSwipeRefreshLayout redeemableSRL;
    @BindView(R.id.redeemableRV)              RecyclerView redeemableRV;
    @BindView(R.id.redeemableCategoryRV)      RecyclerView redeemableCategoryRV;
    @BindView(R.id.chipsCON)                  View chipsCON;
    @BindView(R.id.placeHolderCON)            View placeHolderCON;
    @BindView(R.id.loadingIV)                 View loadingIV;
    @BindView(R.id.loadMorePB)                View loadMorePB;
    @BindView(R.id.searchET)                  EditText searchET;

    public static RedeemableFragment newInstance() {
        RedeemableFragment redeemableFragment = new RedeemableFragment();
        return redeemableFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_redeemable;
    }

    @Override
    public void onViewReady() {
        rewardsActivity = (RewardsActivity) getContext();
        rewardsActivity.setTitle("Meowards");
        rewardsActivity.showOption(false);
        rewardsActivity.setTitleDark(true);
        rewardsActivity.showWallet(false);
        setUpRedeemableListView();
        initNotificationAPI();
        setUpRewardCategoryListView();
        refreshData();
        final Handler handler = new Handler();
        searchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    last_text_edit = System.currentTimeMillis();
                    editables = editable;
                    handler.postDelayed(input_finish_checker, delay);
                } else {
                    rewardCatalogueRequest = new RewardCatalogueRequest(getContext());
                    rewardCatalogueRequest
                            .setSwipeRefreshLayout(redeemableSRL)
                            .addParameters(Keys.server.key.CATEGORY, "all")
                            .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                            .addParameters(Keys.server.key.INCLUDE, "image")
                            .setPerPage(10)
                            .first();
                }

            }
        });
        Analytics.trackEvent("rewards_RedeemableFragment_onViewReady");
    }

    private Runnable input_finish_checker = new Runnable() {
        public void run() {
            if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {
                // TODO: do what you need here
                rewardCatalogueRequest = new RewardCatalogueRequest(getContext());
                rewardCatalogueRequest
                        .setSwipeRefreshLayout(redeemableSRL)
                        .addParameters(Keys.server.key.KEYWORD, editables)
                        .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                        .addParameters(Keys.server.key.INCLUDE, "image")
                        .setPerPage(10)
                        .first();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        rewardsActivity.updateReward();
    }

    private void refreshData(){
        if (rewardCategoryRecycleViewAdapter.getSelectedTitle().equalsIgnoreCase("")){
            refreshList("all");
        }else {
            refreshList(rewardCategoryRecycleViewAdapter.getSelectedTitle());
        }
        loadingIV.setVisibility(View.VISIBLE);
        redeemableRV.setVisibility(View.GONE);
    }

    private void initNotificationAPI(){

        redeemableSRL.setColorSchemeResources(R.color.colorPrimary);
        redeemableSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        redeemableSRL.setOnRefreshListener(this);

        rewardCatalogueRequest = new RewardCatalogueRequest(getContext());
        rewardCatalogueRequest
                .setSwipeRefreshLayout(redeemableSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(10);

        rewardCatalogueCategoryRequest = new RewardCatalogueCategoryRequest(getContext());
        rewardCatalogueCategoryRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(20);
    }

    private void refreshList(String category){
        endlessRecyclerViewScrollListener.reset();
        rewardCatalogueRequest
                .showSwipeRefreshLayout(true)
                .clearParameters()
                .addParameters(Keys.server.key.CATEGORY, category)
                .addParameters(Keys.server.key.INCLUDE, "image")
                .first();

        rewardCatalogueCategoryRequest
                .first();
    }

    private void setDataBasedOnCategoryClick(String category){
        rewardCatalogueRequest
                .showSwipeRefreshLayout(true)
                .clearParameters()
                .addParameters(Keys.server.key.CATEGORY, category)
                .addParameters(Keys.server.key.INCLUDE, "image")
                .first();
    }

    private void setUpRedeemableListView(){
        redeemableRecycleViewAdapter = new RedeemableRecycleViewAdapter(getContext());
        customStaggeredGridLayoutManager = new CustomGridLayoutManager(2,1);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(customStaggeredGridLayoutManager, this, this);
        customStaggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        redeemableRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        redeemableRV.setLayoutManager(customStaggeredGridLayoutManager);
        redeemableRV.setAdapter(redeemableRecycleViewAdapter);
        redeemableRecycleViewAdapter.setOnItemClickListener(this);
    }

    private void setUpRewardCategoryListView(){
        redeemableSRL.setSwipeableChildren(R.id.redeemableRV);
        rewardCategoryLLM = new LinearLayoutManager(getContext());
        rewardCategoryLLM.setOrientation(LinearLayoutManager.HORIZONTAL);
        rewardCategoryRecycleViewAdapter = new RewardCategoryRecycleViewAdapter(getContext());
        redeemableCategoryRV.setLayoutManager(rewardCategoryLLM);
        redeemableCategoryRV.setAdapter(rewardCategoryRecycleViewAdapter);
        rewardCategoryRecycleViewAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onRefresh() {
       refreshData();
    }

    @Override
    public void onItemClick(CatalogueItem catalogueItem) {
        if(catalogueItem.is_redeemable.equalsIgnoreCase("yes")){
//            RedeemRewardConfirmationDialog.newInstance(catalogueItem).show(getFragmentManager(), RedeemRewardConfirmationDialog.TAG);
        }else{
            RedeemRewardFailedDialog.newInstance(catalogueItem).show(getFragmentManager(), RedeemRewardFailedDialog.TAG);
        }
    }

    @Override
    public void onDetailClick(CatalogueItem catalogueItem) {
        if (catalogueItem.remaining == 0){
            ToastMessage.show(getContext(), "SOLD OUT", ToastMessage.Status.FAILED);
        }else {
            ((RouteActivity) getContext()).startRewardsActivity("details", catalogueItem.id);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(RewardCatalogueRequest.ServerResponse responseData) {
        RewardCatalogueTransformer collectionTransformer = responseData.getData(RewardCatalogueTransformer.class);
        if(collectionTransformer.status){
            if(responseData.isNext()){
                redeemableRecycleViewAdapter.addNewData(collectionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                redeemableRecycleViewAdapter.setNewData(collectionTransformer.data);
            }
        }
        Keyboard.hideKeyboard(getContext());
        loadingIV.setVisibility(View.GONE);
        redeemableRV.setVisibility(redeemableRecycleViewAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE);
        placeHolderCON.setVisibility(redeemableRecycleViewAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Subscribe
    public void onResponse(RewardCatalogueCategoryRequest.ServerResponse responseData) {
        RewardCatalogueCategoryTransformer rewardCatalogueCategoryTransformer = responseData.getData(RewardCatalogueCategoryTransformer.class);
        if(rewardCatalogueCategoryTransformer.status){
            if(responseData.isNext()){
                rewardCategoryRecycleViewAdapter.addNewData(rewardCatalogueCategoryTransformer.data);
            }else{
                RewardCategoryItem rewardCategoryItem = new RewardCategoryItem();
                rewardCategoryItem.id = 0;
                rewardCategoryItem.title = "All";
                rewardCategoryItem.image = "https://www.catster.com/wp-content/uploads/2017/08/A-fluffy-cat-looking-funny-surprised-or-concerned.jpg";
                rewardCategoryItem.selected = true;
                rewardCatalogueCategoryTransformer.data.add(0, rewardCategoryItem);
                rewardCategoryRecycleViewAdapter.setNewData(rewardCatalogueCategoryTransformer.data);
            }
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        Log.e("onLoadMore", "here");
        loadMorePB.setVisibility(rewardCatalogueRequest.hasMorePage() ? View.VISIBLE : View.GONE);
        rewardCatalogueRequest.nextPage();
    }

    @Override
    public void onHide() {
        chipsCON.setVisibility(View.GONE);
    }

    @Override
    public void onShow() {
        chipsCON.setVisibility(View.GONE);
    }

    @Override
    public void onScrolled(RecyclerView view, int dx, int dy, int totalScrolled) {

    }

    @Override
    public void onCategoryItemClick(RewardCategoryItem rewardCategoryItem) {
        rewardCategoryRecycleViewAdapter.setSelected(rewardCategoryItem.title);
        switch (rewardCategoryItem.title.toLowerCase()){
            case "all":
                setDataBasedOnCategoryClick("all");
                break;
                default:
                    setDataBasedOnCategoryClick(rewardCategoryItem.title);
        }
    }
}
