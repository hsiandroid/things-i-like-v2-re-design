package com.thingsilikeapp.android.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MainActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.DiscoverItem;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.KYC;
import com.thingsilikeapp.server.request.user.UserInfoRequest;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class DiscoverFragment extends BaseFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = DiscoverFragment.class.getName();

    private MainActivity mainActivity;
    private UserInfoRequest userInfoRequest;

    @BindView(R.id.notificationBTN)                     View notifBTN;
    @BindView(R.id.todaysBdayBTN)                       View todayBdayBTN;
    @BindView(R.id.upcomingBdayBTN)                     View upcomingBdayBTN;
    @BindView(R.id.myBdayBTN)                           View myBdayBTN;
    @BindView(R.id.searchBTN)                           View searchBTN;
    @BindView(R.id.settingsBTN)                         View settingBTN;
    @BindView(R.id.notifTXT)                            TextView notifTXT;
    @BindView(R.id.upcommingTXT)                        TextView upcommingTXT;
    @BindView(R.id.todayTXT)                            TextView todayTXT;
    @BindView(R.id.greetingsTXT)                        TextView greetingsTXT;
    @BindView(R.id.birthDayBTN)                         ImageView birthDayBTN;
    @BindView(R.id.discoverSRL)                         SwipeRefreshLayout discoverSRL;
//    @BindView(R.id.lykaSupportBTN)                      View lykaSupportBTN;

    public static DiscoverFragment newInstance() {
        DiscoverFragment fragment = new DiscoverFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_discover;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
        notifBTN.setOnClickListener(this);
        todayBdayBTN.setOnClickListener(this);
        upcomingBdayBTN.setOnClickListener(this);
        myBdayBTN.setOnClickListener(this);
        searchBTN.setOnClickListener(this);
        settingBTN.setOnClickListener(this);
        mainActivity.notificationActive();
        discoverSRL.setOnRefreshListener(this);
        birthDayBTN.setOnClickListener(this);
//        lykaSupportBTN.setOnClickListener(this);
        Analytics.trackEvent("main_DiscoverFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.notificationBTN:
                mainActivity.startDiscoverActivity("notif");
                Analytics.trackEvent("main_DiscoverFragment_notificationBTN");
                break;

            case R.id.todaysBdayBTN:
                mainActivity.startBirthdayActivity("today_celebrant");
                Analytics.trackEvent("main_DiscoverFragment_todaysBdayBTN");
                break;

            case R.id.upcomingBdayBTN:
                mainActivity.startBirthdayActivity("celebrant");
                Analytics.trackEvent("main_DiscoverFragment_upcomingBdayBTN");
                break;

            case R.id.myBdayBTN:
                mainActivity.startBirthdayActivity(new EventItem(), "greeting");
                Analytics.trackEvent("main_DiscoverFragment_myBdayBTN");
                break;

            case R.id.searchBTN:
                mainActivity.startDiscoverActivity("people");
                Analytics.trackEvent("main_DiscoverFragment_searchBTN");
                break;

            case R.id.settingsBTN:
                mainActivity.startDiscoverActivity("setting");
                Analytics.trackEvent("main_DiscoverFragment_settingsBTN");
                break;
            case R.id.birthDayBTN:
                openBirthdaySurprise();
                Analytics.trackEvent("main_DiscoverFragment_birthDayBTN");
                break;
//            case R.id.lykaSupportBTN:
//                mainActivity.startDiscoverActivity("kendra");
//                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void openBirthdaySurprise(){
        mainActivity.startBirthdaySurpriseActivity("");
    }


    public void refreshList(){
        KYC.getDefault().discover(getContext(), discoverSRL);
    }

    @Subscribe
    public void onResponse(KYC.DiscoverResponse responseData) {
        SingleTransformer<DiscoverItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            if (singleTransformer.data.isBday){
                birthDayBTN.setVisibility(View.VISIBLE);
            }else{
                birthDayBTN.setVisibility(View.GONE);
            }
            if (singleTransformer.data.bdayGreetingsNum.equalsIgnoreCase("0")){
                greetingsTXT.setVisibility(View.GONE);
            }else{
                greetingsTXT.setText(singleTransformer.data.bdayGreetingsNum);
                greetingsTXT.setVisibility(View.VISIBLE);
            }
            if (singleTransformer.data.notificationNum.equalsIgnoreCase("0")){
                notifTXT.setVisibility(View.GONE);

            }else{
                notifTXT.setText(singleTransformer.data.notificationNum);
                notifTXT.setVisibility(View.VISIBLE);
            }
            if (singleTransformer.data.upcomingBdayNum.equalsIgnoreCase("0")){
                upcommingTXT.setVisibility(View.GONE);
            }else{
                upcommingTXT.setText(singleTransformer.data.upcomingBdayNum);
                upcommingTXT.setVisibility(View.VISIBLE);
            }
            if (singleTransformer.data.todayBdayNum.equalsIgnoreCase("0")){
                todayTXT.setVisibility(View.GONE);
            }else{
                todayTXT.setText(singleTransformer.data.todayBdayNum);
                todayTXT.setVisibility(View.VISIBLE);
            }
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onRefresh() {
            refreshList();
    }
}