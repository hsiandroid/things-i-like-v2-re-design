package com.thingsilikeapp.android.fragment.wishlist;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.annotations.SerializedName;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.WishListActivity;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.BlockUserItem;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Settings;
import com.thingsilikeapp.server.request.wishlist.CreateWishListRequest;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.ImageQualityManager;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import butterknife.BindView;
import icepick.State;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class RePostFragment extends BaseFragment implements View.OnClickListener,
        CategoryDialog.Callback,
        WishListActivity.onBackPressedListener {

    public static final String TAG = RePostFragment.class.getName();
    private WishListActivity wishListActivity;
    private static final int PERMISSION_GALLERY = 102;

    private Handler handler;

    @BindView(R.id.descriptionET)           EditText descriptionET;
    @BindView(R.id.oldDescriptionET)        EditText oldDescriptionET;

    @BindView(R.id.metaDataCON)             View metaDataCON;
    @BindView(R.id.metaImageIV)             ImageView metaImageIV;
    @BindView(R.id.metaTitleTXT)            TextView metaTitleTXT;
    @BindView(R.id.metaDescriptionTXT)      TextView metaDescriptionTXT;

    @BindView(R.id.imageIV)                 ImageView imageIV;
    @BindView(R.id.metaVideoIV)             VideoView metaVideoIV;

    @BindView(R.id.categoryTXT)             TextView categoryTXT;
    @BindView(R.id.othersET)                EditText othersET;
    @BindView(R.id.titleET)                 EditText titleET;
    @BindView(R.id.othersTIL)               View othersTIL;

    @BindView(R.id.postBTN)                 View postBTN;
    @BindView(R.id.categoryBTN)             View categoryBTN;

    @State File file;

    @State int parent_id = 0;
    @State int wishlist_id = 0;
    @State String content = "";
    @State String type = "";
    @State String title = "";
    @State String url = "";
    @State String urlTitle = "";
    @State String urlDescription = "-";
    @State String urlImage = "/";
//    @State String urlVideo = "/";
    @State String category = "";

    public static RePostFragment newInstance(WishListItem wishListItem) {
        RePostFragment rePostFragment = new RePostFragment();
        rePostFragment.parent_id = wishListItem.parent_id == 0 ? wishListItem.id : wishListItem.parent_id;
        rePostFragment.wishlist_id = wishListItem.id;
        rePostFragment.url = wishListItem.info.data.url;
        rePostFragment.title = wishListItem.title;
        rePostFragment.content = wishListItem.info.data.content;
        rePostFragment.urlTitle = wishListItem.info.data.url_title;
        rePostFragment.urlDescription = wishListItem.info.data.url_description;
        rePostFragment.urlImage = wishListItem.image.data.full_path;
//        rePostFragment.urlVideo = wishListItem.images.data.get(0).fullPath;
        rePostFragment.parent_id = wishListItem.id;
        rePostFragment.category = wishListItem.category;
        rePostFragment.type = wishListItem.post_type;
        return rePostFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_re_post;
    }

    @Override
    public void onViewReady() {
        wishListActivity = (WishListActivity) getContext();
        wishListActivity.setTitle("RepopostBTNst");
        wishListActivity.setOnBackPressedListener(this);
        postBTN.setOnClickListener(this);
        categoryTXT.setOnClickListener(this);
        categoryBTN.setOnClickListener(this);
        categoryTXT.setText(category);
        titleET.setText(title);
        strictPolicy();
        openSharePost();
//        showDescriptionWalkThru();
        categoryTXT.setText("I Just Like It");
        Log.d("EEEEEEE", ">>>> " + wishlist_id);
    }

    private void onDescriptionTextChange(){
        descriptionET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                descriptionET.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN) {
                            switch (keyCode) {
                                case KeyEvent.KEYCODE_DPAD_CENTER:
                                case KeyEvent.KEYCODE_ENTER:
                                    Keyboard.hideKeyboard(getContext());
//                                    showRepostWalkThru();
                                    return true;
                                default:
                                    break;
                            }
                        }
                        return false;
                    }
                });
            }
        });
    }

    private void strictPolicy(){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    private void openSharePost(){

        if(!StringFormatter.isEmpty(url)){
            metaDataCON.setVisibility(View.VISIBLE);
            oldDescriptionET.setText(content);
            metaTitleTXT.setText(urlTitle);
            metaDescriptionTXT.setText(urlDescription);
            categoryTXT.setText(category);
                metaVideoIV.setVisibility(View.GONE);
                metaImageIV.setVisibility(View.VISIBLE);
                urlImage = urlImage.equals("") ? "/" : urlImage;
                Glide.with(getContext())
                        .load(urlImage)
                        .apply(new RequestOptions()
                        .placeholder(R.drawable.icon_image_placeholder)
                        .error(R.drawable.icon_image_corrupted))
                        .into(metaImageIV);

        }else{
            metaDataCON.setVisibility(View.GONE);
            Glide.with(getContext())
                    .load(urlImage)
                    .apply(new RequestOptions()
                    .placeholder(R.drawable.icon_image_placeholder)
                    .error(R.drawable.icon_image_corrupted))
                    .into(imageIV);
            oldDescriptionET.setText(content);
        }
        onDescriptionTextChange();
    }

//    private void downloadImage(String image){
//        final ProgressDialog progressDialog = ProgressDialog.show(getContext(), "", "Please wait", true, false);
//
//        Glide.with(getContext())
//                .asBitmap()
//                .load(image)
//                .apply(new RequestOptions())
//                .into(new SimpleTarget<Bitmap>() {
//                    @Override
//                    public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {
//                        ImageQualityManager.getFileFromBitmap(getContext(), bitmap, new ImageQualityManager.Callback() {
//                            @Override
//                            public void success(File file) {
//                                if(file != null){
//                                    RePostFragment.this.file = file;
//                                    imageIV.setVisibility(View.VISIBLE);
//                                    descriptionET.setText(content);
//                                    descriptionET.setSelection(descriptionET.length());
//                                    Glide.with(getContext())
//                                            .load(file)
//                                            .into(imageIV);
//
////                                    Glide.with(getContext())
////                                            .load(RePostFragment.this.file)
////                                            .into(imageIV);
//                                    progressDialog.cancel();
//                                }
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                        progressDialog.cancel();
//                        ToastMessage.show(getContext(), "Something went wrong! Please try again!", ToastMessage.Status.FAILED);
//                        wishListActivity.finish();
//                        super.onLoadFailed(e, errorDrawable);
//                    }
//                });
//    }

    @Override
    public void onResume() {
        super.onResume();

    }

//    private void downloadImageWithPermision(String image){
//        if(PermissionChecker.checkPermissions(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_GALLERY)){
//            downloadImage(image);
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == PERMISSION_GALLERY) {
//            if(wishListActivity.isAllPermissionResultGranted(grantResults)){
//                downloadImage(urlImage);
//            }
//        }
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.postBTN:
                attemptCreate();
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.CREATE_POST);
                ((RouteActivity)getContext()).startMainActivity("main");
                break;
            case R.id.categoryTXT:
            case R.id.categoryBTN:
                categoryTXT.setError(null);
                CategoryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.CATEGORY);
                break;
        }
    }

    @Override
    public void callback(String categoryItem) {
        categoryTXT.setText(categoryItem);

        if(categoryItem.equalsIgnoreCase("other") || categoryItem.equalsIgnoreCase("others")){
            othersTIL.setVisibility(View.VISIBLE);
        }else{
            othersTIL.setVisibility(View.GONE);
        }
    }

    private void attemptCreate(){
//        othersET.setError(null);
//        CreateWishListRequest createWishListRequest = new CreateWishListRequest(getContext());
//        if(file != null){
//            createWishListRequest.addMultipartBody(Keys.server.key.FILE, file);
//        }else {
////            createWishListRequest.addMultipartBody(Keys.server.key.URL, StringFormatter.replaceNull(url))
////                    .addMultipartBody(Keys.server.key.URL_TITLE, StringFormatter.replaceNull(urlTitle))
////                    .addMultipartBody(Keys.server.key.URL_DESCRIPTION, urlDescription)
////                    .addMultipartBody(Keys.server.key.URL_IMAGE, urlImage);
//        }
//
//        String category = categoryTXT.getText().toString();
//        if(category.equalsIgnoreCase("other") || category.equalsIgnoreCase("others")){
//           if(TextUtils.isEmpty(othersET.getText().toString())){
//               othersET.setError("Field is required.");
//               return;
//           }else{
//               category = othersET.getText().toString();
//           }
//        }
//
//        createWishListRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Reposting...", false, false))
//                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addMultipartBody(Keys.server.key.TITLE, titleET.getText().toString())
//                .addMultipartBody(Keys.server.key.PARENT_ID, parent_id)
//                .addMultipartBody(Keys.server.key.WISHLIST_OWNER_ID, wishlist_id)
//                .addMultipartBody(Keys.server.key.CATEGORY, category)
//                .addMultipartBody(Keys.server.key.CONTENT, descriptionET.getText().toString())
//                .addMultipartBody(Keys.server.key.URL, StringFormatter.replaceNull(url))
//                .addMultipartBody(Keys.server.key.URL_TITLE, StringFormatter.replaceNull(urlTitle))
//                .addMultipartBody(Keys.server.key.URL_DESCRIPTION, urlDescription)
//                .addMultipartBody(Keys.server.key.URL_IMAGE, urlImage)
//                .execute();
        Settings.getDefault().getRepost(getContext(), wishlist_id, descriptionET.getText().toString() + "\n " + oldDescriptionET.getText().toString());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    public void hideSoftKeyboard() {
        if(getActivity().getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

//    @Subscribe
//    public void onResponse(CreateWishListRequest.ServerResponse responseData) {
//        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
//        if(wishListInfoTransformer.status){
//            int count = UserData.getInt(UserData.TOTAL_POST) + 1;
//            UserData.insert(UserData.TOTAL_POST, count);
//            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
//            hideSoftKeyboard();
////            if (!UserData.getBoolean(UserData.WALKTHRU_DONE)){
////                UserData.insert(UserData.WALKTHRU_CURRENT, 7);
////                UserData.insert(UserData.SHARE_POST_SHOW, 2);
////            }
//            getActivity().finish();
//        }else{
//            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
//            if(wishListInfoTransformer.hasRequirements()){
//                ErrorResponseManger.first(descriptionET, wishListInfoTransformer.requires.content);
//                ErrorResponseManger.first(categoryTXT, wishListInfoTransformer.requires.category);
//            }
//        }
//        clearPost();
//    }

    @Subscribe
    public void onResponse(Settings.RepostResponse responseData) {
        SingleTransformer<WishListItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            int count = UserData.getInt(UserData.TOTAL_POST) + 1;
            UserData.insert(UserData.TOTAL_POST, count);
            hideSoftKeyboard();
            getActivity().finish();
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
            if(singleTransformer.hasRequirements()){
                ErrorResponseManger.first(descriptionET, singleTransformer.content);
                ErrorResponseManger.first(categoryTXT, singleTransformer.category);
            }
        }
        clearPost();
    }

    @Subscribe
    public void onResponse(BaseRequest.OnFailedResponse onFailedResponse){
        postBTN.setEnabled(true);
        metaDataCON.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        exitConfirmation();
    }

    public void exitConfirmation(){
        if(descriptionET.getText().toString().length() > 0){
            final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
            confirmationDialog
                    .setDescription("Save Post as Draft?")
                    .setNote("Click save so you can still edit this post later.")
                    .setIcon(R.drawable.icon_information)
                    .setPositiveButtonText("Save")
                    .setPositiveButtonClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            savePost();
                            wishListActivity.finish();
                            confirmationDialog.dismiss();
                        }
                    })
                    .setNegativeButtonClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            clearPost();
                            wishListActivity.finish();
                            confirmationDialog.dismiss();
                        }
                    })
                    .setNegativeButtonText("Discard")
                    .build(wishListActivity.getSupportFragmentManager());
        }else{
            wishListActivity.finish();
            clearPost();
        }
    }

    private void clearPost(){
        UserData.insert(UserData.POST_MESSAGE, "");
        UserData.insert(UserData.POST_IMAGE, "");
        UserData.insert(UserData.STREET_ADDRESS, "");
        UserData.insert(UserData.CITY, "");
        UserData.insert(UserData.STATE, "");
        UserData.insert(UserData.ZIP, "");
        UserData.insert(UserData.CONTACT_NUMBER, "");
        UserData.insert(UserData.CATEGORY, "");
        UserData.insert(UserData.COUNTRY, "");
    }

    private void savePost(){
        UserData.insert(UserData.POST_MESSAGE, descriptionET.getText().toString());
        UserData.insert(UserData.POST_IMAGE, "");
        UserData.insert(UserData.CATEGORY, categoryTXT.getText().toString());
    }


}
