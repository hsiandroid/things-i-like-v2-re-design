package com.thingsilikeapp.android.fragment.gem;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;

import com.google.zxing.Result;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.GemActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import butterknife.BindView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class GemScannerFragment extends BaseFragment  implements
        ZXingScannerView.ResultHandler{

    public static final String TAG = GemScannerFragment.class.getName();
    private int PERMISSION_CAMERA = 787;

    private GemActivity gemActivity;

    @BindView(R.id.scannerView)    ZXingScannerView scannerView;

    public static GemScannerFragment newInstance() {
        GemScannerFragment fragment = new GemScannerFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_gem_scanner;
    }

    @Override
    public void onViewReady() {
        gemActivity = (GemActivity) getContext();
        gemActivity.setTitle("Scan Code");
        Analytics.trackEvent("gem_GemScannerFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        if(PermissionChecker.checkPermissions(getActivity(), Manifest.permission.CAMERA, PERMISSION_CAMERA)){
            startCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CAMERA) {
            if(gemActivity.isAllPermissionResultGranted(grantResults)){
                startCamera();
            }else{
                gemActivity.onBackPressed();
            }
        }
    }

    private void startCamera(){
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        Log.e("EEEEEEE", ">>>" + result.getText());

        submitCallback(result.getText());
    }

    public void submitCallback(String result) {
        Intent data = new Intent();
        data.putExtra("result", result);
        gemActivity.setResult(Activity.RESULT_OK, data);
        gemActivity.finish();
    }

    private String decrypt(String raw){
        byte[] dataDec = Base64.decode(raw, Base64.DEFAULT);
        String decodedString = "";
        try {

            decodedString = new String(dataDec, StandardCharsets.UTF_8);
        } finally {

            return decodedString;
        }
    }


}
