package com.thingsilikeapp.android.fragment.profilebak;

import android.app.ProgressDialog;
import android.support.v7.widget.OrientationHelper;
import android.util.Log;
import android.view.View;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.SocialFeedRecyclerViewAdapter;
import com.thingsilikeapp.android.dialog.UnfollowDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.like.LikeRequest;
import com.thingsilikeapp.server.request.report.ReportRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.wishlist.BlockRequest;
import com.thingsilikeapp.server.request.wishlist.UserWishListRequest;
import com.thingsilikeapp.server.transformer.report.ReportTransformer;
import com.thingsilikeapp.server.transformer.social.FeedTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.CustomStaggeredGridLayoutManager;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.decoration.SpacesItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class ILikeFragmentBak extends BaseFragment implements
        SocialFeedRecyclerViewAdapter.ClickListener,
        EndlessRecyclerViewScrollListener.Callback{

    public static final String TAG = ILikeFragmentBak.class.getName();
    private SocialFeedRecyclerViewAdapter socialFeedRecyclerViewAdapter;
    private UserWishListRequest userWishListRequest;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private CustomStaggeredGridLayoutManager staggeredGridLayoutManager;

    @BindView(R.id.observableRecyclerView)  ObservableRecyclerView observableRecyclerView;
    @BindView(R.id.thingsPB)                View thingsPB;
    @BindView(R.id.placeHolderCON)          View placeHolderCON;

    @State int userID;
    @State boolean canScroll;

    public static ILikeFragmentBak newInstance(int userID) {
        ILikeFragmentBak iLikeFragment = new ILikeFragmentBak();
        iLikeFragment.userID = userID;
        return iLikeFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_i_like;
    }

    @Override
    public void onViewReady() {
        setupListView();
        initFeedAPI();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void setupListView(){
        socialFeedRecyclerViewAdapter = new SocialFeedRecyclerViewAdapter(getContext());
        socialFeedRecyclerViewAdapter.setEnableUserClick(false);

        staggeredGridLayoutManager = new CustomStaggeredGridLayoutManager(2, OrientationHelper.VERTICAL);
        staggeredGridLayoutManager.setScrollEnabled(false);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(staggeredGridLayoutManager, this);
        observableRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        observableRecyclerView.setAdapter(socialFeedRecyclerViewAdapter);
        observableRecyclerView.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.social_feed_spacing)));
        observableRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        socialFeedRecyclerViewAdapter.setOnItemClickListener(this);


        if(userID == UserData.getUserId()){
            FeedTransformer cache = new Gson().fromJson(UserData.getString(UserData.MY_FEED_CACHE), FeedTransformer.class);
            if(cache != null && socialFeedRecyclerViewAdapter.getItemCount() == 0){
                socialFeedRecyclerViewAdapter.setNewData(cache.data);
            }
        }
    }

    public void setScrollable(boolean scrollable){
        if(staggeredGridLayoutManager != null){
            staggeredGridLayoutManager.setScrollEnabled(scrollable);
        }
    }

    @Override
    public void onItemClick(WishListItem wishListItem) {
        if (wishListItem.owner.data != null) {
            ((RouteActivity) getContext()).startItemActivity(wishListItem.id, wishListItem.owner.data.id, "i_like");
        }
    }

    @Override
    public void onCommentClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startCommentActivity(wishListItem.id, wishListItem.title);
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        ((RouteActivity) getContext()).startProfileActivity(userItem.id);
    }

    @Override
    public void onGrabClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startWishListActivity(wishListItem, "create_grab_other");
    }

    @Override
    public void onHideClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to hide?")
                .setNote("You will no longer see this post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Hide")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BlockRequest blockRequest = new BlockRequest(getContext());
                        blockRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
    }

    @Override
    public void onUnfollowClick(final WishListItem wishListItem) {
        UnfollowDialog.newInstance(wishListItem.owner.data.id, wishListItem.owner.data.name, new UnfollowDialog.Callback() {
            @Override
            public void onAccept(int userID) {
                UnFollowRequest unFollowRequest = new UnFollowRequest(getContext());
                unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                        .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                        .addParameters(Keys.server.key.USER_ID, wishListItem.owner.data.id)
                        .execute();
            }

            @Override
            public void onCancel(int userID) {

            }
        }).show(((BaseActivity) getContext()).getSupportFragmentManager(), UnfollowDialog.TAG);
    }

    @Override
    public void onReportClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to report?")
                .setNote("You are about to report a post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Report")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportRequest reportRequest = new ReportRequest(getContext());
                        reportRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.TYPE, "wishlist")
                                .addParameters(Keys.server.key.REFERENCE_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
    }

    @Override
    public void onShareClick(WishListItem wishListItem) {

    }

    @Override
    public void onHeartClick(WishListItem wishListItem) {
        LikeRequest likeRequest = new LikeRequest(getContext());
        likeRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                .execute();
    }

    @Override
    public void onRepostClick(WishListItem wishListItem) {

    }

    @Override
    public void onRepostUsersClick(WishListItem wishListItem) {

    }

    @Override
    public void onHeaderClick(EventItem eventItem) {

    }

    @Override
    public void onHeaderShowClick(EventItem eventItem) {

    }

    private void initFeedAPI(){
        userWishListRequest = new UserWishListRequest(getContext());
        userWishListRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                .addParameters(Keys.server.key.USER_ID, userID)
                .setPerPage(10);
    }

    public void refreshList(){
        System.gc();
        if(thingsPB != null){
            thingsPB.setVisibility(View.VISIBLE);
        }
        if (userWishListRequest != null) {
            userWishListRequest
                    .first();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(UserWishListRequest.ServerResponse responseData) {
        Log.e("Hello", "here");
        FeedTransformer feedTransformer = responseData.getData(FeedTransformer.class);
        if(feedTransformer.status){
            if(responseData.isNext()){
                socialFeedRecyclerViewAdapter.addNewData(feedTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                socialFeedRecyclerViewAdapter.setNewData(feedTransformer.data);
            }

            if(userID == UserData.getUserId()){
                FeedTransformer cache = new FeedTransformer();
                cache.data = socialFeedRecyclerViewAdapter.getData();
                UserData.insert(UserData.MY_FEED_CACHE, new Gson().toJson(cache));
            }
        }

        if(socialFeedRecyclerViewAdapter.getItemCount() == 0){
            placeHolderCON.setVisibility(View.VISIBLE);
        }else {
            placeHolderCON.setVisibility(View.GONE);
        }
        thingsPB.setVisibility(View.GONE);
    }

    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            socialFeedRecyclerViewAdapter.unFollow(userTransformer.userItem.id);
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(BlockRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            socialFeedRecyclerViewAdapter.removeItem(wishListInfoTransformer.wishListItem);
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(LikeRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            socialFeedRecyclerViewAdapter.updateData(wishListInfoTransformer.wishListItem);
        }
    }

    @Subscribe
    public void onResponse(ReportRequest.ServerResponse responseData) {
        ReportTransformer reportTransformer = responseData.getData(ReportTransformer.class);
        if (reportTransformer.status) {
            if(reportTransformer.data.is_removed){
                socialFeedRecyclerViewAdapter.removeItemByID(reportTransformer.data.reference_id);
            }
            ToastMessage.show(getContext(), reportTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getContext(), reportTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    public void scrollUp(){
        if(observableRecyclerView != null){
            observableRecyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        thingsPB.setVisibility(userWishListRequest.hasMorePage() ? View.VISIBLE : View.GONE);
        userWishListRequest.nextPage();
    }
}
