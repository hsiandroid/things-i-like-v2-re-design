package com.thingsilikeapp.android.fragment.intro;

import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class SecondFragment extends BaseFragment {

    public static final String TAG = SecondFragment.class.getName();


    public static SecondFragment newInstance() {
        SecondFragment fragment = new SecondFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_intro_b;
    }

    @Override
    public void onViewReady() {

    }
}
