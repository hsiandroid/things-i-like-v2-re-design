package com.thingsilikeapp.android.fragment.request;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RequestActivity;
import com.thingsilikeapp.android.adapter.recyler.RequestOwnedRecyclerViewAdapter;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListViewerItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.wishlist.PermissionOwnedRequest;
import com.thingsilikeapp.server.request.wishlist.WishListCancelPermissionRequest;
import com.thingsilikeapp.server.transformer.wishlist.PermissionTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoViewerTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class RequestOwnedFragment extends BaseFragment implements
    SwipeRefreshLayout.OnRefreshListener,
    EndlessRecyclerViewScrollListener.Callback,
    RequestOwnedRecyclerViewAdapter.ClickListener{

    public static final String TAG = RequestOwnedFragment.class.getName();

    private RequestActivity requestActivity;
    private RequestOwnedRecyclerViewAdapter requestViewerRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private PermissionOwnedRequest permissionAllRequest;

    @BindView(R.id.requestRV)                   RecyclerView requestRV;
    @BindView(R.id.requestSRL)                  MultiSwipeRefreshLayout requestSRL;
    @BindView(R.id.requestPlaceHolderCON)       View requestPlaceHolderCON;

    @State String include = "wishlist.image,wishlist.owner.info,viewer.info,info,image,sender.info";

    public static RequestOwnedFragment newInstance() {
        RequestOwnedFragment requestOwnedFragment = new RequestOwnedFragment();
        return requestOwnedFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_request_owned;
    }

    @Override
    public void onViewReady() {
        requestActivity = (RequestActivity) getContext();
        requestActivity.setTitle(getString(R.string.title_request_owned));
        setupListView();
        initSuggestionAPI();
        Analytics.trackEvent("request_RequestOwnedFragment_onViewReady");
    }

    private void setupListView(){
        requestSRL.setColorSchemeResources(R.color.colorPrimary);
        requestSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        requestSRL.setSwipeableChildren(R.id.requestRV, R.id.requestPlaceHolderCON);
        requestSRL.setOnRefreshListener(this);

        requestViewerRecyclerViewAdapter = new RequestOwnedRecyclerViewAdapter(getContext());
        requestViewerRecyclerViewAdapter.setOnItemClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);

        requestRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        requestRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        requestRV.setLayoutManager(linearLayoutManager);
        requestRV.setAdapter(requestViewerRecyclerViewAdapter);
    }

    private void initSuggestionAPI(){
        permissionAllRequest = new PermissionOwnedRequest(getContext());
        permissionAllRequest
                .setSwipeRefreshLayout(requestSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .showSwipeRefreshLayout(true)
                .setPerPage(20);
    }

    private void refreshList(){
        permissionAllRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    public void showHideView(){
        if(requestViewerRecyclerViewAdapter.getItemCount() == 0){
            requestPlaceHolderCON.setVisibility(View.VISIBLE);
            requestRV.setVisibility(View.GONE);
        }else {
            requestPlaceHolderCON.setVisibility(View.GONE);
            requestRV.setVisibility(View.VISIBLE);
        }
    }

    private void attemptCancelRequest(final int wishlistID, final int userID){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Cancel Request")
                .setNote("You are cancelling your request.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Confirm")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new WishListCancelPermissionRequest(getContext())
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading...", false, false))
                                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .addParameters(Keys.server.key.INCLUDE, include)
                                .addParameters(Keys.server.key.WISHLIST_ID, wishlistID)
                                .addParameters(Keys.server.key.USER_ID, userID)
                                .execute();
                        confirmationDialog.dismiss();
                        ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.CANCEL_REQUEST);
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("request_RequestOwnedFragment_attemptCancelRequest");
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        permissionAllRequest.nextPage();
    }

    @Override
    public void onItemClick(WishListViewerItem wishListViewerItem) {
        requestActivity.startItemActivity(wishListViewerItem.wishlist.data.id, wishListViewerItem.viewer.data.id, "i_like");
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        requestActivity.startProfileActivity(userItem.id);
    }

    @Override
    public void onYourProfileClick(UserItem userItem) {
        requestActivity.startProfileActivity(userItem.id);
    }

    @Override
    public void onCancelClick(WishListViewerItem wishListViewerItem) {
        attemptCancelRequest(wishListViewerItem.wishlist.data.id, wishListViewerItem.viewer.data.id);
    }

    @Subscribe
    public void onResponse(PermissionOwnedRequest.ServerResponse responseData) {
        PermissionTransformer permissionTransformer = responseData.getData(PermissionTransformer.class);
        if(permissionTransformer.status){
            requestViewerRecyclerViewAdapter.setNewData(permissionTransformer.data);
            showHideView();
        }
    }

    @Subscribe
    public void onResponse(WishListCancelPermissionRequest.ServerResponse responseData) {
        WishListInfoViewerTransformer wishlistInfoTransformer = responseData.getData(WishListInfoViewerTransformer.class);
        if(wishlistInfoTransformer.status){
            requestViewerRecyclerViewAdapter.removeData(wishlistInfoTransformer.wishListViewerItem);
            showHideView();
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}
