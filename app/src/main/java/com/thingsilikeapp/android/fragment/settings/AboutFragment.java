package com.thingsilikeapp.android.fragment.settings;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;
import com.thingsilikeapp.android.dialog.WebViewDialog;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;

import butterknife.BindView;


public class AboutFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = AboutFragment.class.getName();

    private SettingsActivity settingsActivity;

    @BindView(R.id.versionTXT)  TextView versionTXT;
    @BindView(R.id.termsBTN)    View termsBTN;
    @BindView(R.id.privacyBTN)  View privacyBTN;

    public static AboutFragment newInstance() {
        AboutFragment aboutFragment = new AboutFragment();
        return aboutFragment;
    }

    // GGGGGGGGGGGG     AAAAAAAAAAAA     GGGGGGGGGGGG     GGGGGGGGGGGG
    // GGGGGGGGGGGG     AAAAAAAAAAAA     GGGGGGGGGGGG     GGGGGGGGGGGG
    // GGG              AAA      AAA     GGG              GGG      GGG
    // GGG              AAA      AAA     GGG              GGG      GGG
    // GGG    GGGGG     AAAAAAAAAAAA     GGG    GGGGG     GGG      GGG
    // GGG    GGGGG     AAAAAAAAAAAA     GGG    GGGGG     GGG      GGG
    // GGG      GGG     AAA      AAA     GGG      GGG     GGG      GGG
    // GGG      GGG     AAA      AAA     GGG      GGG     GGG      GGG
    // GGGGGGGGGGGG     AAA      AAA     GGGGGGGGGGGG     GGGGGGGGGGGG
    // GGGGGGGGGGGG     AAA      AAA     GGGGGGGGGGGG     GGGGGGGGGGGG

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_about;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle(getString(R.string.title_about));
        versionTXT.setText(getAppVersion());

        termsBTN.setOnClickListener(this);
        privacyBTN.setOnClickListener(this);

    }

    public void openUrl(String url){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    private String getAppVersion(){
        String version;
        try {
            version = "App Build version " + getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            version = "App Build version --";
        }

        return version;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.termsBTN:
                WebViewDialog.newInstance("http://www.mylyka.com/terms-of-use").show(getChildFragmentManager(), WebViewDialog.TAG);
//                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.SettingsActivity.EVENT,FacebookCustomLoggerMessage.SettingsActivity.TERMS);
                break;
            case R.id.privacyBTN:
                WebViewDialog.newInstance("http://www.mylyka.com/privacy-policy").show(getChildFragmentManager(), WebViewDialog.TAG);
//                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.SettingsActivity.EVENT,FacebookCustomLoggerMessage.SettingsActivity.POLICY);
                break;
        }
    }
}
