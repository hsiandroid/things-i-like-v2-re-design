package com.thingsilikeapp.android.fragment.create_post;

import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.CreatePostActivity;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.helper.LinkPreviewRequest;
import com.thingsilikeapp.server.request.wishlist.CreateWishListRequest;
import com.thingsilikeapp.server.transformer.helper.LinkTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import butterknife.BindView;
import icepick.State;

public class  SendPostLinkFragment extends BaseFragment implements View.OnClickListener, CategoryDialog.Callback {

    public static final String TAG = SendPostLinkFragment.class.getName();

    private CreatePostActivity createPostActivity;

    @BindView(R.id.imageIV)         ImageView imageView;
    @BindView(R.id.titleET)         EditText titleET;
    @BindView(R.id.contentET)       EditText contentET;
    @BindView(R.id.categoryIV)      ImageView categoryIV;
    @BindView(R.id.categoryBTN)     View categoryBTN;
    @BindView(R.id.categoryTXT)     TextView categoryTXT;
    @BindView(R.id.nextBTN)         TextView nextBTN;
    @BindView(R.id.mainBackButtonIV)    View mainBackButtonIV;
    @BindView(R.id.customTXT)      EditText customTXT;
    @BindView(R.id.customView)      View customView;

    @State String url;
    @State String urlTitle = "";
    @State String urlDescription = "-";
    @State String urlImage = "/";
    @State String category = "I Just Like It";

    public static SendPostLinkFragment newInstance(String url) {
        SendPostLinkFragment fragment = new SendPostLinkFragment();
        fragment.url = url;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_send_post;
    }

    @Override
    public void onViewReady() {
        createPostActivity = (CreatePostActivity)getContext();
        categoryTXT.setOnClickListener(this);
        categoryIV.setOnClickListener(this);
        categoryBTN.setOnClickListener(this);
        categoryTXT.setText("I Just Like It");
        nextBTN.setOnClickListener(this);
        mainBackButtonIV.setOnClickListener(this);
        attemptGetMeta(url);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.categoryTXT:
            case R.id.categoryIV:
            case R.id.categoryBTN:
                categoryTXT.setError(null);
                CategoryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.CATEGORY);
                break;
            case R.id.nextBTN:
                attemptUpload();
                break;
            case R.id.mainBackButtonIV:
                createPostActivity.onBackPressed();
                break;
        }
    }

    private void attemptGetMeta(String meta_url){

//        url = "";
//        urlTitle = "";
//        urlDescription = "-";
//        urlImage = "/";
//        thumbnailIndex = 0;

        LinkPreviewRequest linkPreviewRequest = new LinkPreviewRequest(getContext());
        linkPreviewRequest.addParameters(Keys.server.key.URL, meta_url)
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Processing...", false, false))
                .execute();

        Log.e("Meta", ">>>" + meta_url);
    }

    public void attemptUpload(){

        CreateWishListRequest createWishListRequest = new CreateWishListRequest(getContext());
        createWishListRequest.addMultipartBody(Keys.server.key.URL, StringFormatter.replaceNull(url))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Creating post...", false, false))
                .addMultipartBody(Keys.server.key.TITLE, StringFormatter.replaceNull(urlTitle))
//                .addMultipartBody(Keys.server.key.CATEGORY, categoryTXT.getText().toString())
                .addMultipartBody(Keys.server.key.CONTENT, contentET.getText().toString())
                .addMultipartBody(Keys.server.key.URL_TITLE, StringFormatter.replaceNull(titleET.getText().toString()))
                .addMultipartBody(Keys.server.key.URL_DESCRIPTION, contentET.getText().toString())
                .addMultipartBody(Keys.server.key.URL_IMAGE, urlImage);
//                .execute();
        if(category.equalsIgnoreCase("Others")){
            createWishListRequest
//                    .addMultipartBody(Keys.server.key.CATEGORY, categoryTXT.getText())
                    .addMultipartBody(Keys.server.key.CATEGORY, customTXT.getText())
                    .execute();
        }
        else{
            createWishListRequest
                    .addMultipartBody(Keys.server.key.CATEGORY, categoryTXT.getText())
                    .execute();
        }

    }

    @Subscribe
    public void onResponse(CreateWishListRequest.ServerResponse responseData) {
        Log.e("ResponseCode", ">>>" + responseData.getCode());
        if(responseData.getCode() == 500){
            ToastMessage.show(getActivity(), "Something went wrong. Please try again!", ToastMessage.Status.FAILED);
        }
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if(wishListInfoTransformer.status){
            int count = UserData.getInt(UserData.TOTAL_POST) + 1;
            UserData.insert(UserData.TOTAL_POST, count);
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
            createPostActivity.startMainActivity("main");

        }else{
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
            if(wishListInfoTransformer.hasRequirements()){
                ErrorResponseManger.first(contentET, wishListInfoTransformer.requires.content);
                ErrorResponseManger.first(categoryTXT, wishListInfoTransformer.requires.category);

            }
        }
    }

    @Subscribe
    public void onResponse(LinkPreviewRequest.ServerResponse responseData) {
        if(responseData.getCode() == 500){
//            closeMeta();
//            postBTN.setEnabled(true);
        }
        LinkTransformer linkTransformer = responseData.getData(LinkTransformer.class);
        if(linkTransformer.status){
//            metaPB.setVisibility(View.GONE);
//            postBTN.setEnabled(true);
//            metaDataCON.setVisibility(View.VISIBLE);

            urlTitle = linkTransformer.data.title;
            url = linkTransformer.data.url;
            urlDescription = linkTransformer.data.description;
            urlImage = linkTransformer.data.cover;
//            thumbnailIndex = 0;
//            thumbnailImages = linkTransformer.data.images;
//            if(thumbnailImages.size() > 0){
//                displayCounter();
//            }
            Picasso.with(getContext()).load(urlImage).into(imageView);
            titleET.setText(urlTitle);
            contentET.setText(urlDescription);
        }else{
//            metaCON.setVisibility(View.GONE);
//            metaPB.setVisibility(View.GONE);
//            postBTN.setEnabled(true);
//            metaDataCON.setVisibility(View.GONE);
        }
    }

    @Override
    public void callback(String categoryItem) {
//        categoryTXT.setText(categoryItem.title);
        this.category = categoryItem;
        categoryTXT.setText(categoryItem);
        if(categoryItem.equalsIgnoreCase("Others")){
            customView.setVisibility(View.VISIBLE);

        }
        else{
            categoryTXT.setText(categoryItem);
            customView.setVisibility(View.GONE);
        }
    }
}
