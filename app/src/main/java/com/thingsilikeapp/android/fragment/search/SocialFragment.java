package com.thingsilikeapp.android.fragment.search;

import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SearchActivity;
import com.thingsilikeapp.android.adapter.FragmentViewPagerAdapter;
import com.thingsilikeapp.android.fragment.search.social.FollowersFragment;
import com.thingsilikeapp.android.fragment.search.social.FollowingFragment;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;
import icepick.State;

public class SocialFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = SocialFragment.class.getName();

    private SearchActivity searchActivity;

    private FragmentViewPagerAdapter fragmentViewPagerAdapter;

    @BindView(R.id.followersBTN)                     TextView followersBTN;
    @BindView(R.id.followingBTN)                     TextView followingBTN;
    @BindView(R.id.socialVP)                         ViewPager socialVP;
    @BindView(R.id.headerCON)                        View headerCON;

    @State int userID;
    @State int viewType;
    @State String commonName;

    public static SocialFragment newInstance(int userID, String name, int viewType) {
        SocialFragment socialFragment = new SocialFragment();
        socialFragment.userID = userID;
        socialFragment.viewType = viewType;
        socialFragment.commonName = name;
        return socialFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_social;
    }

    @Override
    public void onViewReady() {
        searchActivity = (SearchActivity) getActivity();
        if (UserData.getUserItem().id == userID){
            searchActivity.setTitle(getString(R.string.my_profile_title));
        }else {
            searchActivity.setTitle(commonName);
        }
        Log.e("EEEEEEEEEEEE", ">>>>" + commonName);
        searchActivity.showSearchContainer(false);
        followersBTN.setOnClickListener(this);
        followingBTN.setOnClickListener(this);


//        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)){
//
//            followersBTN.setOnClickListener(null);
//            followingBTN.setOnClickListener(null);
//        }else {
//            followersBTN.setOnClickListener(this);
//            followingBTN.setOnClickListener(this);
//        }

        followersBTN.setOnClickListener(this);
        followingBTN.setOnClickListener(this);
        setupViewPager();
    }

    private void setupViewPager(){
        fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getChildFragmentManager());
        fragmentViewPagerAdapter.addFragment(FollowersFragment.newInstance(userID));
        fragmentViewPagerAdapter.addFragment(FollowingFragment.newInstance(userID));
        socialVP.setPageMargin(20);
        socialVP.setPageMarginDrawable(null);
        socialVP.setAdapter(fragmentViewPagerAdapter);
        socialVP.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        followersBTN.setSelected(true);
                        followingBTN.setSelected(false);
                        followersBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
                        followingBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        break;
                    case 1:
                        followersBTN.setSelected(false);
                        followingBTN.setSelected(true);
                        followersBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        followingBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        socialVP.setOffscreenPageLimit(2);
        switch (viewType){
            case 1:
                followersActive();
                break;
            case 2:
                followingActive();
                break;
        }

    }

    public void followersActive(){
        followersBTN.setSelected(true);
        followingBTN.setSelected(false);
        followersBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
        followingBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        socialVP.post(new Runnable() {
            @Override
            public void run() {
                if(socialVP != null){
                    socialVP.setCurrentItem(0);
                }
            }
        });
    }

    public void followingActive(){
        followersBTN.setSelected(false);
        followingBTN.setSelected(true);
        followersBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        followingBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
        socialVP.post(new Runnable() {
            @Override
            public void run() {
                if(socialVP != null){
                    socialVP.setCurrentItem(1);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.followersBTN:
                followersActive();
                break;
            case R.id.followingBTN:
                followingActive();
                break;
        }
    }
}
