package com.thingsilikeapp.android.fragment.messages;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MessageActivity;
import com.thingsilikeapp.android.adapter.MessagesPagerAdapter;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MessengerFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = MessengerFragment.class.getName();
    private MessageActivity messageActivity;
    private MessagesPagerAdapter messagesPagerAdapter;

    @BindView(R.id.landingTL)                   TabLayout landingTL;
    @BindView(R.id.landingVP)                   ViewPager landingVP;
    @BindView(R.id.mainTitleTXT)                TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)            ImageView mainBackButtonIV;
    @BindView(R.id.newChatBTN)                  ImageView newChatBTN;

    public static MessengerFragment newInstance(int item) {
        MessengerFragment fragment = new MessengerFragment();
        fragment.item = item;
        return fragment;
    }

    @State int item = 1;

    @Override
    public void onViewReady() {
        messageActivity = (MessageActivity)getContext();
        messagesPagerAdapter = new MessagesPagerAdapter(getChildFragmentManager(), messageActivity);
        landingVP.setAdapter(messagesPagerAdapter);
        landingVP.setCurrentItem(item);
        landingTL.setupWithViewPager(landingVP);
        mainBackButtonIV.setOnClickListener(this);
        newChatBTN.setOnClickListener(this);
        mainTitleTXT.setText("Messages");
        Analytics.trackEvent("messages_MessengerFragment_onViewReady");
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_messages;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.mainBackButtonIV:
                messageActivity.onBackPressed();
                Analytics.trackEvent("messages_MessengerFragment_mainBackButtonIV");
                break;
            case R.id.newChatBTN:
                messageActivity.openNewMessageFragment();
                Analytics.trackEvent("messages_MessengerFragmentnewChatBTN");
                break;
        }
    }
}
