package com.thingsilikeapp.android.fragment.tutorial;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

public class Tutorial4Fragment extends BaseFragment {

    public static final String TAG = Tutorial4Fragment.class.getName();


    public static Tutorial4Fragment newInstance() {
        Tutorial4Fragment tutorial4Fragment = new Tutorial4Fragment();
        return tutorial4Fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_tutorial_4;
    }

    @Override
    public void onViewReady() {
    }
}
