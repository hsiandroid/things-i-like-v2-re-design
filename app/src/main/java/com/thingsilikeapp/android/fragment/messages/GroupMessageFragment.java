package com.thingsilikeapp.android.fragment.messages;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MessageActivity;
import com.thingsilikeapp.android.adapter.GroupMessageRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.MessageRecycleViewAdapter;
import com.thingsilikeapp.data.model.ChatModel;
import com.thingsilikeapp.data.model.ChatModel;
import com.thingsilikeapp.server.request.Chat;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

public class GroupMessageFragment extends BaseFragment implements GroupMessageRecycleViewAdapter.ClickListener, EndlessRecyclerViewScrollListener.Callback, SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = GroupMessageFragment.class.getName();

    private MessageActivity messageActivity;
    private GroupMessageRecycleViewAdapter messageRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private APIRequest apiRequest;
    private Timer timer;

    @BindView(R.id.inboxRV)         RecyclerView inboxRV;
    @BindView(R.id.messageSRL)      SwipeRefreshLayout messageSRL;
    @BindView(R.id.searchET)        EditText searchET;

    public static GroupMessageFragment newInstance() {
        GroupMessageFragment fragment = new GroupMessageFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_group_message;
    }

    @Override
    public void onViewReady() {
        messageActivity = (MessageActivity) getContext();
        setUpRView();
        messageSRL.setOnRefreshListener(this);
        initSearch();
        Analytics.trackEvent("messages_GroupMessageFragment_onViewReady");
    }

    private void setUpRView(){
        messageRecycleViewAdapter = new GroupMessageRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        inboxRV.setLayoutManager(linearLayoutManager);
        inboxRV.setAdapter(messageRecycleViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        inboxRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        messageRecycleViewAdapter.setOnItemClickListener(this);
    }

    private List<ChatModel> getDefaultData(){
        List<ChatModel> androidModels = new ArrayList<>();
        ChatModel defaultItem;
        defaultItem = new ChatModel();
        defaultItem.is_add = true;
        defaultItem.id = 0;
        androidModels.add(0,defaultItem);
        return androidModels;
    }

    @Subscribe
    public void onResponse(Chat.MyChatResponse colletionResponse) {
        CollectionTransformer<ChatModel> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            if (colletionResponse.isNext()){
                messageRecycleViewAdapter.addNewData(collectionTransformer.data);
            }else {
                endlessRecyclerViewScrollListener.reset();
                ChatModel chatModel = new ChatModel();
                chatModel.id = 0;
                chatModel.title = "Add new group";
                chatModel.latestMessage = new ChatModel.LatestMessage();
                chatModel.latestMessage.data = new ChatModel.LatestMessage.Data();
                chatModel.latestMessage.data.info = new ChatModel.LatestMessage.Data.Info();
                chatModel.latestMessage.data.info.data = new ChatModel.LatestMessage.Data.Info.DataX();
                chatModel.latestMessage.data.info.data.dateCreated = new ChatModel.LatestMessage.Data.Info.DataX.DateCreated();
                chatModel.latestMessage.data.info.data.dateCreated.timePassed = "";
                chatModel.latestMessage.data.content = "";
                chatModel.info = new ChatModel.Info();
                chatModel.info.data = new ChatModel.Info.Data();
                chatModel.info.data.avatar = new ChatModel.Info.Data.Avatar();
                chatModel.info.data.avatar.fullPath = "https://cdn4.iconfinder.com/data/icons/evil-icons-user-interface/64/plus-512.png";
                collectionTransformer.data.add(0, chatModel);
                messageRecycleViewAdapter.setNewData(collectionTransformer.data);
            }
//            groupChatLL.setVisibility(messageRecycleViewAdapter.getData().size() > 0 ? View.GONE : View.VISIBLE);
        }
    }


    public void initSearch(){
        if(searchET != null){
            searchET.setHint("Search Messages...");
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (timer != null) {
                        timer.cancel();
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(final Editable s) {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Chat.getDefault().myChat(getContext(), "own", messageSRL, searchET.getText().toString()).execute();
                                }
                            });
                        }
                    }, 300);
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void refreshList(){

        apiRequest = Chat.getDefault().myChat(getContext(), "own", messageSRL,"");
        apiRequest.first();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onItemClick(ChatModel chatItem) {
        if (chatItem.id != 0){
            messageActivity.openThreadFragment(chatItem.id);
        }else{
            messageActivity.openNewCreateGroupChatFragment();
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
}
