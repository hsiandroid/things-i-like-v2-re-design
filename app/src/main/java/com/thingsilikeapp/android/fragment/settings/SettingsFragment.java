package com.thingsilikeapp.android.fragment.settings;

import android.content.pm.PackageManager;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;
import com.thingsilikeapp.android.adapter.SettingAdapter;
import com.thingsilikeapp.android.dialog.LogoutDialog;
import com.thingsilikeapp.data.model.SettingItem;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.android.widget.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class SettingsFragment extends BaseFragment implements SettingAdapter.ClickListener,View.OnClickListener {

    public static final String TAG = SettingsFragment.class.getName();
    private SettingsActivity settingsActivity;

    private SettingAdapter profileAdapter;
    private SettingAdapter notificationAdapter;
    private SettingAdapter aboutAdapter;

    private List<SettingItem> settingItem;

    @BindView(R.id.profileEHLV)         ExpandableHeightListView profileEHLV;
    @BindView(R.id.notificationEHLV)    ExpandableHeightListView notificationEHLV;
    @BindView(R.id.aboutEHLV)           ExpandableHeightListView aboutEHLV;

    public static SettingsFragment newInstance() {
        SettingsFragment settingsFragment = new SettingsFragment();
        return settingsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_settings;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getContext();
        settingsActivity.setTitle(getString(R.string.title_main_settings));

        setupProfileListView();
        sampleProfileData();

        setupNotificationListView();
        sampleNotificationData();

        setupAboutListView();
        sampleOthersData();

    }

    @Override
    public void onResume() {
        super.onResume();

        setupProfileListView();
        sampleProfileData();

        setupNotificationListView();
        sampleNotificationData();

        setupAboutListView();
        sampleOthersData();

    }

    private void setupProfileListView() {

        settingItem = new ArrayList<>();
        profileAdapter = new SettingAdapter(getContext(), settingItem);
        profileAdapter.setOnItemClickListener(this);
        profileEHLV.setAdapter(profileAdapter);
    }

    private void sampleProfileData() {

        settingItem.clear();

        SettingItem settingItems = new SettingItem();
        settingItems.id = 1;
        settingItems.name = "Edit Profile";
        settingItems.show_enabled = false;
        settingItems.description = "Update your profile information";
        settingItem.add(settingItems);

        settingItems = new SettingItem();
        settingItems.id = 2;
        settingItems.name = "Change Password";
        settingItems.show_enabled =false;
        settingItems.description = "Update your password regularly";
        settingItem.add(settingItems);
    }


    private void setupNotificationListView() {

        settingItem = new ArrayList<>();
        notificationAdapter = new SettingAdapter(getContext(), settingItem);
        notificationAdapter.setOnItemClickListener(this);
        notificationEHLV.setAdapter(notificationAdapter);
    }

    private void sampleNotificationData() {

        SettingItem settingItems = new SettingItem();
        settingItems.id = 3;
        settingItems.name = "Notification";
        settingItems.show_enabled =true;
        settingItems.description = "Lorem Ipsum is simply dummy text";
        settingItem.add(settingItems);
    }

    private void setupAboutListView() {

        settingItem = new ArrayList<>();
        aboutAdapter = new SettingAdapter(getContext(), settingItem);
        aboutAdapter.setOnItemClickListener(this);
        aboutEHLV.setAdapter(aboutAdapter);
    }

    private void sampleOthersData() {

        SettingItem settingItems = new SettingItem();
        settingItems.id = 4;
        settingItems.name = "About LYKA";
        settingItems.show_enabled =false;
        settingItems.description = getAppVersion();
        settingItem.add(settingItems);

        settingItems = new SettingItem();
        settingItems.id = 5;
        settingItems.name = "Log Out";
        settingItems.show_enabled =false;
        settingItems.description ="We hate to see you leave.";
        settingItem.add(settingItems);
    }

    @Override
    public void onSettingItemClickListener(int id) {
        switch (id){
            case 1:
                settingsActivity.openEditProfileFragment();
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.SettingsActivity.EVENT,FacebookCustomLoggerMessage.SettingsActivity.EDIT_PROFILE);
                break;
            case 2:
                settingsActivity.openChangePasswordFragment();
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.SettingsActivity.EVENT,FacebookCustomLoggerMessage.SettingsActivity.CHANGE_PASSWORD);
                break;
            case 3:
                break;
            case 4:
                settingsActivity.openAboutFragment();
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.SettingsActivity.EVENT,FacebookCustomLoggerMessage.SettingsActivity.ABOUT);
                break;
            case 5:
                LogoutDialog logoutDialog = LogoutDialog.newInstance();
                logoutDialog.show(getFragmentManager(), LogoutDialog.TAG);
                break;

        }
    }

    @Override
    public void onNotificationChanged(boolean active) {

    }


    private String getAppVersion(){
        String version;
        try {
            version = "App Build version " + getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName + " (Beta)";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            version = "App Build version -- (Beta)";
        }
        return version;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
        }
    }
}
