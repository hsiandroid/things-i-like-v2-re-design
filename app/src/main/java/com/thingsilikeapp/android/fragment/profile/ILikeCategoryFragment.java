package com.thingsilikeapp.android.fragment.profile;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.FeedCategoryRecyclerViewAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.FeedCategoryItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.social.FeedCategoryRequest;
import com.thingsilikeapp.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.decoration.SpacesItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class ILikeCategoryFragment extends BaseFragment implements
        EndlessRecyclerViewScrollListener.Callback,
        FeedCategoryRecyclerViewAdapter.ClickListener{

    public static final String TAG = ILikeCategoryFragment.class.getName();
    private FeedCategoryRecyclerViewAdapter feedCategoryRecyclerViewAdapter;
    private FeedCategoryRequest feedCategoryRequest;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.categoryRV)      RecyclerView categoryRV;
    @BindView(R.id.placeHolderCON)  View placeHolderCON;
    @BindView(R.id.loadingIV)       View loadingIV;

    @State int userID;
    @State boolean canScroll;

    public static ILikeCategoryFragment newInstance(int userID) {
        ILikeCategoryFragment iLikeFragment = new ILikeCategoryFragment();
        iLikeFragment.userID = userID;
        return iLikeFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_i_like_category;
    }

    @Override
    public void onViewReady() {
        setupListView();
        initFeedAPI();
        Analytics.trackEvent("profile_ILikeCategoryFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void setupListView(){
        feedCategoryRecyclerViewAdapter = new FeedCategoryRecyclerViewAdapter(getContext());
        feedCategoryRecyclerViewAdapter.setOnItemClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        categoryRV.setLayoutManager(linearLayoutManager);
        categoryRV.setAdapter(feedCategoryRecyclerViewAdapter);
        categoryRV.setNestedScrollingEnabled(false);
        categoryRV.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.social_feed_spacing)));
        categoryRV.addOnScrollListener(endlessRecyclerViewScrollListener);
    }

    public void setScrollable(boolean scrollable){
//        if(staggeredGridLayoutManager != null){
//            staggeredGridLayoutManager.setScrollEnabled(scrollable);
//        }
    }

    private void initFeedAPI(){
        feedCategoryRequest = new FeedCategoryRequest(getContext(), !UserData.isMe(userID));
        feedCategoryRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "image, owner.social")
//                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                .addParameters(Keys.server.key.USER_ID, userID)
                .setPerPage(20);
    }

    public void refreshList(){
        System.gc();
        if (feedCategoryRequest != null) {
            feedCategoryRequest
                    .first();
        }
        loadingIV.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(FeedCategoryRequest.ServerResponse responseData) {
        CollectionTransformer<FeedCategoryItem> feedTransformer = responseData.getData(CollectionTransformer.class);
        if(feedTransformer.status){
            feedCategoryRequest.setHasMorePage(feedTransformer.has_morepages);
            if(responseData.isNext()){
                feedCategoryRecyclerViewAdapter.addNewData(feedTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                feedCategoryRecyclerViewAdapter.setNewData(feedTransformer.data);
            }
        }
        loadingIV.setVisibility(View.GONE);
        categoryRV.setVisibility(feedCategoryRecyclerViewAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE);
        placeHolderCON.setVisibility(feedCategoryRecyclerViewAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        Log.e("NextPage", ">>>"  + page);
        if (feedCategoryRequest.hasMorePage()){
            feedCategoryRequest.nextPage();
        }
    }

    @Override
    public void onItemClick(FeedCategoryItem feedCategoryItem) {

    }

    @Override
    public void onWishListClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startItemActivity(wishListItem.id, wishListItem.owner.data.id, "i_like");
    }
}
