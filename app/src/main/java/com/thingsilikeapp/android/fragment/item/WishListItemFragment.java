package com.thingsilikeapp.android.fragment.item;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.slider.library.Animations.BaseAnimationInterface;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Transformers.BaseTransformer;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.danikula.videocache.HttpProxyCacheServer;
//import com.github.tcking.viewquery.ViewQuery;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ItemActivity;
import com.thingsilikeapp.android.adapter.CommentAdapter;
import com.thingsilikeapp.android.adapter.RelatedTempRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.RequestOnGoingAdapter;
import com.thingsilikeapp.android.adapter.TimelineAdapter;
import com.thingsilikeapp.android.dialog.GemsBuyDialog;
import com.thingsilikeapp.android.dialog.GroupDialog;
import com.thingsilikeapp.android.dialog.SendGiftDialog;
import com.thingsilikeapp.android.dialog.UnfollowDialog;
import com.thingsilikeapp.android.dialog.ViewImageDialog;
import com.thingsilikeapp.android.dialog.WebViewDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Share;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.ImageModel;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.comment.AllCommentRequest;
import com.thingsilikeapp.server.request.like.LikeRequest;
import com.thingsilikeapp.server.request.report.ReportRequest;
import com.thingsilikeapp.server.request.social.FollowRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.wishlist.BlockRequest;
import com.thingsilikeapp.server.request.wishlist.InfoWishListRequest;
import com.thingsilikeapp.server.request.wishlist.WishListDeleteRequest;
import com.thingsilikeapp.server.request.wishlist.WishListRelatedRequest;
import com.thingsilikeapp.server.request.wishlist.WishListRequestPermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListSendPermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishlistPermissionRequest;
import com.thingsilikeapp.server.transformer.comment.CommentCollectionTransformer;
import com.thingsilikeapp.server.transformer.report.ReportTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.server.transformer.wishlist.PermissionTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListRelatedTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Formatter;
import com.thingsilikeapp.vendor.android.java.NumberFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;
import com.thingsilikeapp.vendor.android.java.decoration.SpacesItemDecoration;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.android.widget.ExpandableHeightListView;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;
import com.willy.ratingbar.BaseRatingBar;
import com.willy.ratingbar.RotationRatingBar;
import com.yqritc.scalablevideoview.ScalableType;
import com.yqritc.scalablevideoview.ScalableVideoView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;

import butterknife.BindView;
import icepick.State;
import uk.co.deanwild.materialshowcaseview.IShowcaseListener;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class WishListItemFragment extends BaseFragment implements
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        UnfollowDialog.Callback,
        SendGiftDialog.Callback,
        GroupDialog.Callback,
        ItemActivity.SwipeListener,
        RelatedTempRecycleViewAdapter.ClickListener, BaseSliderView.OnSliderClickListener {

    public static final String TAG = WishListItemFragment.class.getName();

    private ItemActivity itemActivity;
    private InfoWishListRequest infoWishListRequest;
    private TimelineAdapter timelineAdapter;
    private WishlistPermissionRequest wishlistPermissionRequest;
    private RequestOnGoingAdapter requestOnGoingAdapter;
    private CommentAdapter commentAdapter;
    private AllCommentRequest allCommentRequest;
    private RelatedTempRecycleViewAdapter socialFeedRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private WishListRelatedRequest wishListRelatedRequest;
    private  SimpleExoPlayer player;
    private boolean playWhenReady = true;
    private int currentWindow = 0;
    private long playbackPosition = 0;
    private boolean enableUserClick = true;
    private Dialog mFullScreenDialog;
    private boolean mExoPlayerFullscreen = false;
    private FrameLayout mFullScreenButton;
    private ImageView mFullScreenIcon;

//    private ViewQuery $;
//    private int aspectRatio =  VideoInfo.AR_ASPECT_FILL_PARENT;

    @BindView(R.id.itemSRL)                         MultiSwipeRefreshLayout itemSRL;
    @BindView(R.id.contentCON)                      ScrollView contentCON;
    @BindView(R.id.placeholder404CON)               View placeholder404CON;
    @BindView(R.id.relatedCON)                      View relatedCON;

    //Basic Info
    @BindView(R.id.iLikeIV)                         ImageView iLikeIV;
    @BindView(R.id.titleTXT)                        TextView titleTXT;
    @BindView(R.id.categoryTXT)                     TextView categoryTXT;
    @BindView(R.id.dateTXT)                         TextView dateTXT;
    @BindView(R.id.descriptionTXT)                  TextView descriptionTXT;
    @BindView(R.id.heartCountTXT)                   TextView heartCountTXT;
    @BindView(R.id.commentCountTXT)                 TextView commentCountTXT;
    @BindView(R.id.rePostBTN)                       View rePostBTN;
    @BindView(R.id.rePostCountTXT)                  TextView rePostCountTXT;
    @BindView(R.id.heartSB)                         SparkButton heartSB;
    @BindView(R.id.urlTXT)                          TextView urlTXT;
    @BindView(R.id.urlCON)                          View urlCON;

    //Owner Info
    @BindView(R.id.avatarCIV)                       ImageView avatarCIV;
    @BindView(R.id.usernameTXT)                     TextView usernameTXT;
    @BindView(R.id.commonNameTXT)                   TextView commonNameTXT;
    @BindView(R.id.commandBTN)                      View commandBTN;
    @BindView(R.id.addBTN)                          View addBTN;
    @BindView(R.id.commandTXT)                      TextView commandTXT;
    @BindView(R.id.loadingPB)                       View loadingPB;

    //Delivery Info
    @BindView(R.id.informationCON)                  View informationCON;
    @BindView(R.id.lockAddressCON)                  View lockAddressCON;
    @BindView(R.id.userInfoCON)                     View userInfoCON;
    @BindView(R.id.nameTXT)                         TextView nameTXT;
    @BindView(R.id.addressTXT)                      TextView addressTXT;
    @BindView(R.id.contactTXT)                      TextView contactTXT;
    @BindView(R.id.quickInfoTXT)                    TextView quickInfoTXT;
    @BindView(R.id.awaitingTXT)                     TextView awaitingTXT;
    @BindView(R.id.requestAddressBTN)               View requestAddressBTN;
    @BindView(R.id.sendGiftBTN)                     View sendGiftBTN;
    @BindView(R.id.viewGiftCardBTN)                 View viewGiftCardBTN;
    @BindView(R.id.deleteBTN)                       View deleteBTN;
    @BindView(R.id.editBTN)                         View editBTN;
    @BindView(R.id.viewOnlineBTN)                   View viewOnlineBTN;

    //Comment Info
    @BindView(R.id.commentTXT)                      TextView commentTXT;
    @BindView(R.id.viewCommentBTN)                  TextView viewCommentBTN;

    //Comment Info
    @BindView(R.id.timelineEHLV)                    ExpandableHeightListView timelineEHLV;
    @BindView(R.id.timelineCON)                     View timelineCON;
    @BindView(R.id.shareBTN)                        View shareBTN;
    @BindView(R.id.heartCON)                        View heartCON;

    //Comment Info
    @BindView(R.id.requestOnGoingEHLV)              ExpandableHeightListView requestOnGoingEHLV;
    @BindView(R.id.commentEHLV)                     ExpandableHeightListView commentEHLV;
    @BindView(R.id.onGoingCON)                      View onGoingCON;
    @BindView(R.id.showMoreBTN)                     View showMoreBTN;
    @BindView(R.id.ongoingRequestPlaceHolderCON)    View ongoingRequestPlaceHolderCON;

    //related items
    @BindView(R.id.relatedRV)                       RecyclerView relatedRV;
    @BindView(R.id.loadingIV)                       View loadingIV;
    @BindView(R.id.commentBTN)                      View commentBTN;
    @BindView(R.id.commandCON)                      View commandCON;
    @BindView(R.id.wishListCON)                     View wishListCON;
    @BindView(R.id.backBTN)                         View backBTN;

    //diamond items
    @BindView(R.id.rateTXT)                         TextView rateTXT;
    @BindView(R.id.lykaRB)                          RotationRatingBar lykaRB;
    @BindView(R.id.diamondsTXT)                     TextView diamondsTXT;
    @BindView(R.id.sliderLayout)                    SliderLayout sliderLayout;
//    @BindView(R.id.sliderVP)                        ViewPager sliderVP;
    @BindView(R.id.videoView)                       PlayerView videoView;
    @BindView(R.id.loading)                         ProgressBar loading;
    @BindView(R.id.videoCON)                        RelativeLayout videoCON;

    @State String includeWishList = "info,image,owner.info,owner.social,owner.statistics,delivery_info,transaction,logs,tracker,images";
    @State String includeOwner = "social";

    @State int wishListID;
    @State String wishListName = "";
    @State int ownerID;
    @State String ownerName;
    @State String imageURL;
    @State int transactionID;
    @State boolean isFollowing;
    @State WishListItem wishListItem;

    public static WishListItemFragment newInstance(int id) {
        WishListItemFragment wishListItemFragment = new WishListItemFragment();
        wishListItemFragment.wishListID = id;
        return wishListItemFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_wishlist_item;
    }

    @Override
    public void onViewReady() {
        itemActivity = (ItemActivity) getContext();
        itemActivity.setTitle("");
        itemActivity.getContainer().setVisibility(View.GONE);
        itemActivity.setSwipeListener(this);
        rePostBTN.setOnClickListener(this);
        rePostCountTXT.setOnClickListener(this);
        commandBTN.setOnClickListener(this);
        avatarCIV.setOnClickListener(this);
        requestAddressBTN.setOnClickListener(this);
        deleteBTN.setOnClickListener(this);
        editBTN.setOnClickListener(this);
        viewOnlineBTN.setOnClickListener(this);
        sendGiftBTN.setOnClickListener(this);
        heartCountTXT.setOnClickListener(this);
        viewGiftCardBTN.setOnClickListener(this);
        viewCommentBTN.setOnClickListener(this);
        showMoreBTN.setOnClickListener(this);
        iLikeIV.setOnClickListener(this);
        shareBTN.setOnClickListener(this);
        commentBTN.setOnClickListener(this);
        rateTXT.setOnClickListener(this);
        backBTN.setOnClickListener(this);

        UserData.insert(UserData.REQUEST_ADDRESS_SHOW, 1);
        UserData.insert(UserData.REQUEST_LIKE_SHOW, 1);
        UserData.insert(UserData.POST_SHOW, 1);

        Log.e("WishList ID", ">>>" + wishListID);
        initWishListAPI();
        setupTimelineListView();
        setupCommentListView();
        setupRequestListView();
        setupRelatedItemView();
//        initFeedAPI();
        itemActivity.getRePostTXT().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otherOptionClick();
            }
        });

//        PlayerManager.getInstance().getDefaultVideoInfo().addOption(Option.create(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "multiple_requests", 1L));
        Analytics.trackEvent("item_WishListItemFragment_onViewReady");
    }

    private void initFeedAPI() {
        wishListRelatedRequest = new WishListRelatedRequest(getContext());
        wishListRelatedRequest
                .clearParameters()
                .setDeviceRegID(itemActivity.getDeviceRegID())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .showSwipeRefreshLayout(true)
                .setPerPage(15)
                .execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
//        if (!UserData.getBoolean(UserData.WALKTHRU_DONE) && UserData.getInt(UserData.WALKTHRU_CURRENT) == 7) {
//            itemActivity.finish();
//            wishListCON.setClickable(false);
//        }
        initFullscreenDialog();
        initFullscreenButton();

        if (mExoPlayerFullscreen) {
            ((ViewGroup) videoView.getParent()).removeView(videoView);
            mFullScreenDialog.addContentView(videoView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_fullscreen_skrink));
            mFullScreenDialog.show();
        }
    }

    private void initWishListAPI() {
        itemSRL.setColorSchemeResources(R.color.colorPrimary);
        itemSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        itemSRL.setSwipeableChildren(R.id.contentCON, R.id.placeholder404CON);
        itemSRL.setOnRefreshListener(this);

        infoWishListRequest = new InfoWishListRequest(getContext());
        infoWishListRequest
                .setSwipeRefreshLayout(itemSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, includeWishList)
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .showSwipeRefreshLayout(true);

        wishlistPermissionRequest = new WishlistPermissionRequest(getContext());
        wishlistPermissionRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "wishlist.image,wishlist.owner.info,viewer.info,info,image,sender.info")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .showSwipeRefreshLayout(true)
                .setPerPage(10);

        allCommentRequest = new AllCommentRequest(getContext());
        allCommentRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "author.info,tagged_user.info")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .showSwipeRefreshLayout(true)
                .setPerPage(2);
    }

    public void attemptGetRequestAddressWalkThrough() {
        MaterialShowcaseView.resetAll(getContext());
        if (!UserData.getBoolean(UserData.WALKTHRU_DONE) && UserData.getInt(UserData.REQUEST_ADDRESS_SHOW) == 1) {
            UserData.insert(UserData.REQUEST_ADDRESS_SHOW, 2);
            new MaterialShowcaseView.Builder(getActivity())
                    .setTarget(requestAddressBTN)
                    .setMaskColour(ActivityCompat.getColor(getContext(), R.color.walkthrough_bg))
                    .setTargetTouchable(false)
                    .setDismissOnTouch(false)
                    .setDismissText("Next")
                    .setDismissOnTargetTouch(false)
                    .setListener(new IShowcaseListener() {
                        @Override
                        public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {
                            wishListCON.setClickable(true);
                        }

                        @Override
                        public void onShowcaseDismissed(final MaterialShowcaseView materialShowcaseView) {
                            Handler handler;
                            handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    materialShowcaseView.removeFromWindow();
                                    contentCON.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            contentCON.scrollTo(0, heartSB.getBottom());
                                            attemptGetLikeWalkThrough();
                                        }
                                    });
                                }
                            }, 100);
                        }
                    })
                    .setShapePadding(0)
                    .withRectangleShape()
                    .setContentTextColor(ActivityCompat.getColor(getContext(), R.color.white_dim))
//                    .setContentText("Click \"Request Address\".")
                    .show();
        }
    }

    public void attemptGetLikeWalkThrough() {
        MaterialShowcaseView.resetAll(getContext());
        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)) {
            if (UserData.getInt(UserData.REQUEST_LIKE_SHOW) == 1) {
//                UserData.insert(UserData.REQUEST_LIKE_SHOW, 2);
                new MaterialShowcaseView.Builder(getActivity())
                        .setTarget(heartSB)
                        .setShapePadding(-3)
                        .setMaskColour(ActivityCompat.getColor(getContext(), R.color.walkthrough_bg))
                        .setTargetTouchable(true)
                        .setListener(new IShowcaseListener() {
                            @Override
                            public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {
                            }

                            @Override
                            public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
                            }
                        })
                        .setDismissOnTargetTouch(true)
                        .setContentTextColor(ActivityCompat.getColor(getContext(), R.color.white_dim))
                        .setContentText("Tap the \"Like\" button.")
                        .show();
            }
        }
    }

    public void attemptGetRepostWalkThrough() {

        MaterialShowcaseView.resetAll(getContext());

        contentCON.post(new Runnable() {
        @Override
        public void run() {
//            contentCON.fullScroll(View.FOCUS_DOWN);
            contentCON.scrollTo(0, rePostBTN.getBottom());
            if (!UserData.getBoolean(UserData.WALKTHRU_DONE) && UserData.getInt(UserData.SHARE_POST_SHOW) == 1) {

                UserData.insert(UserData.SHARE_POST_SHOW, 2);
                MaterialShowcaseView.resetAll(getContext());
                new MaterialShowcaseView.Builder(getActivity())
                        .setTarget(rePostBTN)
                        .setMaskColour(ActivityCompat.getColor(getContext(), R.color.walkthrough_bg))
                        .setTargetTouchable(false)
                        .setDismissText("Next")
                        .setDismissOnTargetTouch(false)
                        .setShapePadding(10)
                        .setListener(new IShowcaseListener() {
                            @Override
                            public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {
                                rePostBTN.setOnClickListener(null);
//                            rePostBTN.setTag(wishListItem);
                            }

                            @Override
                            public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
                                if (!UserData.getBoolean(UserData.WALKTHRU_DONE)) {
                                    UserData.insert(UserData.WALKTHRU_CURRENT, 7);
                                    UserData.insert(UserData.POST_SHOW, 2);
                                    UserData.insert(UserData.SHARE_POST_SHOW, 2);
                                    UserData.insert(UserData.SHARE_POST, 2);
                                }
                                Handler handler;
                                handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        itemActivity.finish();
                                    }
                                }, 200);
//                            rePostBTN.setOnClickListener(WishListItemFragment.this);
                            }
                        })
                        .setContentTextColor(ActivityCompat.getColor(getContext(), R.color.white_dim))
//                    .setContentText("Tap this button to share the post")
                        .setContentText("You can share this post by clicking this button")
                        .show();
            }
        }
    });

    }

    private void setupTimelineListView() {
        timelineAdapter = new TimelineAdapter(getContext());
        timelineEHLV.setAdapter(timelineAdapter);
    }

    private void setupCommentListView() {
        commentAdapter = new CommentAdapter(getContext());
        commentEHLV.setAdapter(commentAdapter);
    }

    private void setupRequestListView() {
        requestOnGoingAdapter = new RequestOnGoingAdapter(getContext());
        requestOnGoingEHLV.setAdapter(requestOnGoingAdapter);
    }

    private void setupRelatedItemView() {
        socialFeedRecyclerViewAdapter = new RelatedTempRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        relatedRV.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.social_feed_spacing)));
        relatedRV.setLayoutManager(linearLayoutManager);

        relatedRV.setAdapter(socialFeedRecyclerViewAdapter);
        socialFeedRecyclerViewAdapter.setOnItemClickListener(this);
//        ViewCompat.setNestedScrollingEnabled(relatedRV, true);
    }

    @Override
    public void onStart() {
        super.onStart();
        System.gc();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.viewCommentBTN:
            case R.id.commentBTN:
                itemActivity.startCommentActivity(wishListID, wishListName);
                Analytics.trackEvent("item_TransactionItemFragment_viewCommentBTN");
                break;
            case R.id.avatarCIV:
                if (ownerID != 0) {
                    itemActivity.startProfileActivity(ownerID);
                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.AVATAR);
                }
                Analytics.trackEvent("item_TransactionItemFragment_avatarCIV");
                break;
            case R.id.commandBTN:
                if (ownerID != 0) {
                    commandButtonClick();
                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.FOLLOW);
                }
                Analytics.trackEvent("item_TransactionItemFragment_commandBTN");
                break;
            case R.id.requestAddressBTN:
                attemptRequestRequest();
                itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.REQUEST_ADDRESS);
                Analytics.trackEvent("item_TransactionItemFragment_requestAddressBTN");
                break;
            case R.id.sendGiftBTN:
                SendGiftDialog.newInstance("Dedication Message", wishListID, this).show(getChildFragmentManager(), SendGiftDialog.TAG);
                itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.SEND_GIFT);
                Analytics.trackEvent("item_TransactionItemFragment_sendGiftBTN");
                break;
            case R.id.viewGiftCardBTN:
                itemActivity.openTransactionItem(transactionID, UserData.getUserId());
                Analytics.trackEvent("item_TransactionItemFragment_viewGiftCardBTN");
                break;
            case R.id.showMoreBTN:
                wishlistPermissionRequest.nextPage();
                Analytics.trackEvent("item_TransactionItemFragment_showMoreBTN");
                break;
            case R.id.editBTN:
                if (view.getTag() != null) {
                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.EDIT_WISHLIST);
//                    itemActivity.startWishListActivity((WishListItem) view.getTag(), "edit");

                    String data = new Gson().toJson(wishListItem);
                    itemActivity.startWishListActivity(data, "edit");
                }
                Analytics.trackEvent("item_TransactionItemFragment_editBTN");
                break;
            case R.id.deleteBTN:
                itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.DELETE_WISHLIST);
                deleteWishList();
                Analytics.trackEvent("item_TransactionItemFragment_deleteBTN");
                break;
            case R.id.viewOnlineBTN:
                if (view.getTag() != null) {
                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.BUY_ITEM);
                    WebViewDialog.newInstance((String) view.getTag()).show(getChildFragmentManager(), WebViewDialog.TAG);
                }
                Analytics.trackEvent("item_TransactionItemFragment_viewOnlineBTN");
                break;
            case R.id.heartCountTXT:
                if (view.getTag() != null) {
                    itemActivity.startSearchActivity((int) view.getTag(), wishListItem.owner.data.common_name, "likes");
                }
                Analytics.trackEvent("item_TransactionItemFragment_heartCountTXT");
                break;

            case R.id.rateTXT:
                    itemActivity.startSearchActivity(wishListItem.id, wishListItem.owner.data.common_name, "likes");
                Analytics.trackEvent("item_TransactionItemFragment_rateTXT");
                break;
            case R.id.rePostBTN:
                if (view.getTag() != null) {
                    String data = new Gson().toJson(wishListItem);
                    itemActivity.startWishListActivity(data, "create_grab_other");
                }
                Analytics.trackEvent("item_TransactionItemFragment_rePostBTN");
                break;
            case R.id.iLikeIV:
                ViewImageDialog.newInstance(imageURL, wishListName).show(getChildFragmentManager(), ViewImageDialog.TAG);
                Analytics.trackEvent("item_TransactionItemFragment_iLikeIV");
                break;
            case R.id.rePostCountTXT:
                itemActivity.startSearchActivity(wishListID, wishListItem.owner.data.common_name, "repost");
                Analytics.trackEvent("item_TransactionItemFragment_rePostCountTXT");
                break;
            case R.id.shareBTN:
                shareTheApp(wishListItem);
                Analytics.trackEvent("item_TransactionItemFragment_shareBTN");
                break;
            case R.id.backBTN:
                itemActivity.onBackPressed();
                break;
        }
    }

    public void shareTheApp(WishListItem wishListItem) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
//        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Things I Like");
//        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "@{username} posted @{wishlist_item} on things I like for @{category}. Check out what else he likes, and discover #thingsilike your modern wishlist app!");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, wishListItem.owner.data.username + " posted " + wishListItem.title + " on things I like for " + wishListItem.category + ". Check out what everyone likes and discover your modern wishlist app");
//        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "www.thingsilikeapp.com/share/i/" + id);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Share.URL + "/share/i/" + wishListItem.id);
        startActivity(Intent.createChooser(sharingIntent, "Share LYKA App"));
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void refreshList() {
        contentCON.setVisibility(View.GONE);
        placeholder404CON.setVisibility(View.GONE);
        infoWishListRequest
                .showSwipeRefreshLayout(true)
                .execute();


//        allCommentRequest
//                .execute();

//        wishListRelatedRequest
//                .execute();

        initFeedAPI();

        loadingIV.setVisibility(View.VISIBLE);
        relatedRV.setVisibility(View.GONE);
    }

    public void deleteWishList() {
        ConfirmationDialog.Builder()
                .setDescription("Are you sure you want to delete this post?")
                .setNote("You cannot recover this post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Confirm")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new WishListDeleteRequest(getContext())
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Deleting Post...", false, false))
                                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .addParameters(Keys.server.key.INCLUDE, includeWishList)
                                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                                .execute();
                        ((RouteActivity)getContext()).startMainActivity("main");
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("item_TransactionItemFragment_deleteWishlistDialog");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        PlayerManager.getInstance().onConfigurationChanged(newConfig);
    }

    private void displayData(WishListItem wishListItem) {

        if (wishListItem.post_type.equalsIgnoreCase("video")){
            videoCON.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.GONE);
//            sliderVP.setVisibility(View.GONE);
            iLikeIV.setVisibility(View.GONE);
            HttpProxyCacheServer httpProxyCacheServer = new HttpProxyCacheServer(getContext());
            String video_url = httpProxyCacheServer.getProxyUrl(wishListItem.images.data.get(0).fullPath);
            Uri uri = Uri.parse(video_url);
            displayVideo(wishListItem.images.data.get(0).fullPath);
//            displayVideo("https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8");

            Log.e("EEEEEEE", ">>>" + wishListItem.images.data.get(0).fullPath);



        }else{
            if (wishListItem.images.data.size() == 0 || wishListItem.images.data.size() == 1){
                videoCON.setVisibility(View.GONE);
                sliderLayout.setVisibility(View.GONE);
//                sliderVP.setVisibility(View.GONE);
                iLikeIV.setVisibility(View.VISIBLE);
            }else {
                videoCON.setVisibility(View.GONE);
                sliderLayout.setVisibility(View.VISIBLE);
//                sliderVP.setVisibility(View.VISIBLE);
                iLikeIV.setVisibility(View.GONE);
            }
        }

        basicInfo(wishListItem);
        ownerInfo(wishListItem.owner.data);
        deliveryInfo(wishListItem);
        commentInfo(wishListItem.comment_count);
        timelineInfo(wishListItem.tracker);
        gemInfo(wishListItem);
        attemptGetOngoingRequest(wishListItem.owner.data.id);
        showData();
        diamondsTXT.setText("This item has received " + wishListItem.for_display_liker + " diamonds");
        rateTXT.setText( " ( " + wishListItem.for_display_liker + " ) " );

        lykaRB.setRating(wishListItem.diamond_sent);
        if (wishListItem.diamond_sent ==  0) {
            lykaRB.setEnabled(true);
            lykaRB.setIsIndicator(false);
        }else{
            lykaRB.setEnabled(false);
            lykaRB.setIsIndicator(true);
        }

        Log.e("EEEEEEEE"," " +  wishListItem.diamond_sent);
        editBTN.setTag(wishListItem);
        deleteBTN.setTag(wishListItem);
        rePostBTN.setTag(wishListItem);
        if (wishListItem.info.data.url != null && !wishListItem.info.data.url.equals("null")) {
            viewOnlineBTN.setTag(wishListItem.info.data.url);
        }

        setUpSlider();
        for(ImageModel images : wishListItem.images.getImages()){
            addSlider(images);
        }

        sliderLayout.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                    enableDisableSwipeRefresh( state == ViewPager.SCROLL_STATE_IDLE );
            }
        });


        Log.e("Wishlist ID", ">>>" + wishListItem.id);
        Log.e("Wishlist USERNAME", ">>>" + wishListItem.owner.data.username);

    }

    private void enableDisableSwipeRefresh(boolean enable) {
        if (itemSRL != null) {
            itemSRL.setEnabled(enable);
        }
    }

    public void displayVideo(String url){

        String userAgent = Util.getUserAgent(getActivity(), "ExoVideoPlayer");

        TrackSelection.Factory adaptiveTrackSelection = new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter());
        RenderersFactory defaultRenderersFactory = new DefaultRenderersFactory(getContext());
        TrackSelector defaultTrackSelector = new DefaultTrackSelector(adaptiveTrackSelection);
        LoadControl defaultLoadControl = new DefaultLoadControl();
        player = ExoPlayerFactory.newSimpleInstance(defaultRenderersFactory, defaultTrackSelector, defaultLoadControl);

        videoView.setPlayer(player);

        //-------------------------------------------------
        DefaultBandwidthMeter defaultBandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getContext(),
                Util.getUserAgent(getContext(), "Exo2"), defaultBandwidthMeter);
        DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(userAgent, defaultBandwidthMeter, DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS, DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS, true);
        DefaultDataSourceFactory dataFactory = new DefaultDataSourceFactory(getContext(), defaultBandwidthMeter, httpDataSourceFactory);

        //-----------------------------------------------

        String hls_url = url;
        Uri uri = Uri.parse(hls_url);
        int length = url.length();
        String type = url.substring(length-3, length);
        Log.e("EEEEEE", ">>>> " + type);
        if (type.equalsIgnoreCase("mp4")){
            MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(uri);
//            player.prepare(videoSource);
            player.prepare(videoSource, true, false);
        }else{
//            Handler mainHandler = new Handler();
//            MediaSource mediaSource = new HlsMediaSource(uri,
//                    dataSourceFactory, mainHandler, null);
            MediaSource mediaSource = new HlsMediaSource.Factory(dataFactory).createMediaSource(uri);
//            player.prepare(mediaSource);
            player.prepare(mediaSource, true, false);
        }

        player.setPlayWhenReady(playWhenReady);
        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {
                    case ExoPlayer.STATE_READY:
                        loading.setVisibility(View.GONE);
                        break;
                    case ExoPlayer.STATE_BUFFERING:
                        loading.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
//        videoView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
//        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
        player.seekTo(currentWindow, playbackPosition);

    }

    private void releasePlayer() {
        if (player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            player.release();
            player = null;
        }
    }

    private void showData() {
        contentCON.setVisibility(View.VISIBLE);
    }

    public void setUpSlider() {
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Default);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
//        sliderLayout.setDuration(2000);
    }

//    private void setVideo(Uri uri) {
//        try {
//            videoView.setDataSource(getContext(),uri);
//            videoView.setLooping(true);
//            videoView.requestFocus();
//            videoView.prepare(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer mp) {
//                    videoView.start();
//                }
//            });
//
//        } catch (IOException ioe) {
//            //ignore
//        }
//    }

    private void addSlider(ImageModel promoModel){
        DefaultSliderView featuredEventTextView = new DefaultSliderView(getContext());
        featuredEventTextView
                .image(promoModel.getImage())
                .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                .setOnSliderClickListener(this);
        featuredEventTextView.bundle(new Bundle());
        featuredEventTextView.getBundle()
                .putString("product", promoModel.toString());

        sliderLayout.addSlider(featuredEventTextView);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void gemInfo(final WishListItem wishListItem){
//        lykaRB.setOnRatingChangeListener(new BaseRatingBar.OnRatingChangeListener() {
//            @Override
//            public void onRatingChange(BaseRatingBar baseRatingBar, final float v) {
//                switch ((int) v){
//                    case 0:
////                        rateTXT.setText("\n( " +(int)  v + " )");
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                attemptRateGem(wishListItem.id, (int) v);
//                            }
//                        }, 2000);
//
//
//                        Analytics.trackEvent("item_TransactionItemFragment_attemptRateGem0");
//                        break;
//                    case 1:
////                        rateTXT.setText("\n( " + (int) v + " )");
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                attemptRateGem(wishListItem.id, (int) v);
//                            }
//                        }, 2000);
//
//                        Analytics.trackEvent("item_TransactionItemFragment_attemptRateGem1");
//                        break;
//                    case 2:
////                        rateTXT.setText("\n( " + (int) v + " )");
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                attemptRateGem(wishListItem.id, (int) v);
//                            }
//                        }, 2000);
//
//                        Analytics.trackEvent("item_TransactionItemFragment_attemptRateGem2");
//                        break;
//                    case 3:
////                        rateTXT.setText("\n( " + (int) v + " )");
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                attemptRateGem(wishListItem.id, (int) v);
//                            }
//                        }, 2000);
//
//                        Analytics.trackEvent("item_TransactionItemFragment_attemptRateGem3");
//                        break;
//                    case 4:
////                        rateTXT.setText("\n( " + (int) v + " )");
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                attemptRateGem(wishListItem.id, (int) v);
//                            }
//                        }, 2000);
//
//                        Analytics.trackEvent("item_TransactionItemFragment_attemptRateGem4");
//                        break;
//                    case 5:
////                        rateTXT.setText("\n( " + (int) v + " )");
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                attemptRateGem(wishListItem.id, (int) v);
//                            }
//                        }, 2000);
//
//                        Analytics.trackEvent("item_TransactionItemFragment_attemptRateGem5");
//                        break;
//
//                }
//            }
//        });

        lykaRB.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()){
                    case MotionEvent.ACTION_UP:
                        Log.e("ACTION", "RELEASE");
                        attemptRateGem(wishListItem.id, (int) lykaRB.getRating());
                        break;
                }
                return false;
            }
        });
    }

    private void attemptRateGem(int id, int counter){
        LikeRequest likeRequest = new LikeRequest(getContext());
        likeRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "image,owner.social,images,info")
                .addParameters(Keys.server.key.WISHLIST_ID, id)
                .addParameters(Keys.server.key.COUNTER, counter)
                .execute();
    }

    private void basicInfo(WishListItem wishListItem) {
        imageURL = wishListItem.image.data.full_path;
        Glide.with(getContext())
                .load(imageURL)
                .apply(new RequestOptions()

                        .fitCenter()
                .dontAnimate()
                .skipMemoryCache(true)
                .dontTransform()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(iLikeIV);


        iLikeIV.measure(iLikeIV.getMeasuredWidth(), iLikeIV.getMeasuredHeight());
        Log.e("HHHHHHHH", ">>>> " + iLikeIV.getHeight());

        wishListName = wishListItem.title;
        if (wishListName.equalsIgnoreCase("") || wishListItem == null){
            titleTXT.setVisibility(View.GONE);
        }else{
            titleTXT.setVisibility(View.VISIBLE);
        }
        titleTXT.setText(wishListName);
        itemActivity.setTitle(wishListName);

        categoryTXT.setText(wishListItem.category.toUpperCase());


        dateTXT.setText(wishListItem.time_passed);
//        titleTXT.setVisibility(wishListItem.same_title ? View.GONE : View.VISIBLE);


//        heartCountTXT.setText(wishListItem.for_display_liker + " " + String.valueOf(wishListItem.liker_count == 1  ? "like" : "likes"));
//        commentCountTXT.setText(wishListItem.comment_count + " " +  String.valueOf(wishListItem.comment_count == 1  ? "comment" : "comments")) ;
//        rePostCountTXT.setText(wishListItem.repost_count + " " + String.valueOf(wishListItem.repost_count == 1  ? "repost" : "reposts"));

        heartCountTXT.setText(wishListItem.for_display_liker + "");
        commentCountTXT.setText(wishListItem.comment_count + " Comments");
        rePostCountTXT.setText(wishListItem.repost_count + " Reposts");

        heartCountTXT.setTag(wishListItem.id);

        heartSB.pressOnTouch(false);
        heartSB.setChecked(wishListItem.is_liked);

        heartSB.setEventListener(new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean buttonState) {
                heartSB.playAnimation();
                attemptLike();
            }

            @Override
            public void onEventAnimationEnd(ImageView button, boolean buttonState) {

            }

            @Override
            public void onEventAnimationStart(ImageView button, boolean buttonState) {

            }
        });

        String url = wishListItem.info.data.content;
        String search = "{link}";

        if (url.contains(search) && url != null && !url.equals("")) {
            url = url.replace(search, "");
            descriptionTXT.setText(url);
        } else {
            descriptionTXT.setText(wishListItem.info.data.content);
        }

        descriptionTXT.setVisibility(url.equalsIgnoreCase("") ? View.GONE : View.VISIBLE);

        if (wishListItem.info.data.url == null) {
            urlCON.setVisibility(View.GONE);
        } else {
            urlCON.setVisibility(View.VISIBLE);
            urlTXT.setText(Formatter.getDomainFromURL(wishListItem.info.data.url));
        }
    }

    private void ownerInfo(UserItem owner) {

        ownerID = owner.id;
        ownerName = owner.name;
        isFollowing = owner.social.data.is_following.equals("yes");

        Glide.with(getContext())
                .load(owner.getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .dontAnimate()
                .dontTransform()
                .skipMemoryCache(true)
                .error(R.drawable.placeholder_avatar)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(avatarCIV);

        usernameTXT.setText(VerifiedUserBadge.getFormattedName(getContext(), owner.name, owner.is_verified, 14));
        commonNameTXT.setText(owner.common_name);

        if (owner.id == UserData.getUserId()) {
            commandBTN.setVisibility(View.GONE);
        } else {
            commandBTN.setVisibility(View.VISIBLE);
            if (owner.social.data.is_following.equals("yes")) {
                commandBTN.setBackground(ActivityCompat.getDrawable(getContext(), R.drawable.bg_follow));
                commandTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
                commandTXT.setText("Following");
                addBTN.setVisibility(View.GONE);
            } else {
                commandBTN.setBackground(ActivityCompat.getDrawable(getContext(), R.drawable.bg_unfollow));
                commandTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                commandTXT.setText("Follow");
                addBTN.setVisibility(View.VISIBLE);
            }
        }
    }

    private void deliveryInfo(WishListItem wishListItem) {
        if (wishListItem.owner.data.id == UserData.getUserId()) {
            editBTN.setVisibility(View.VISIBLE);
            deleteBTN.setVisibility(View.VISIBLE);
            informationCON.setVisibility(View.GONE);
            onGoingCON.setVisibility(View.GONE);
        } else {
            editBTN.setVisibility(View.GONE);
            deleteBTN.setVisibility(View.GONE);
            if (wishListItem.category.equalsIgnoreCase("Christmas Gift") || wishListItem.category.equalsIgnoreCase("Birthday Gift") || wishListItem.category.equalsIgnoreCase("Items I Want")){
                informationCON.setVisibility(View.VISIBLE);
            }else{
                informationCON.setVisibility(View.GONE);
                onGoingCON.setVisibility(View.GONE);
            }


            switch (wishListItem.delivery_info.data.permission_to_view) {
                case "denied":
                case "not_requested":
                    lockAddressCON.setVisibility(View.VISIBLE);
                    userInfoCON.setVisibility(View.GONE);

                    sendGiftBTN.setVisibility(View.GONE);
                    requestAddressBTN.setVisibility(View.VISIBLE);
                    viewGiftCardBTN.setVisibility(View.GONE);

                    awaitingTXT.setVisibility(View.GONE);
                    quickInfoTXT.setText("Address hidden for privacy purposes.\nRequest user's address below.");
                    break;
                case "pending":
                    lockAddressCON.setVisibility(View.VISIBLE);
                    userInfoCON.setVisibility(View.GONE);

                    sendGiftBTN.setVisibility(View.GONE);
                    requestAddressBTN.setVisibility(View.GONE);
                    viewGiftCardBTN.setVisibility(View.GONE);

                    awaitingTXT.setVisibility(View.VISIBLE);
                    quickInfoTXT.setText("Permission to view delivery address has been sent.");
                    break;
                case "granted":
                    lockAddressCON.setVisibility(View.GONE);
                    userInfoCON.setVisibility(View.VISIBLE);

                    sendGiftBTN.setVisibility(View.VISIBLE);
                    requestAddressBTN.setVisibility(View.GONE);
                    viewGiftCardBTN.setVisibility(View.GONE);

                    awaitingTXT.setVisibility(View.GONE);
                    quickInfoTXT.setText("Address request granted. Start sending gift!");
                    nameTXT.setText(wishListItem.delivery_info.data.recipient);
                    addressTXT.setText(wishListItem.delivery_info.data.address);
                    contactTXT.setText(wishListItem.delivery_info.data.contact_number);

                    if (wishListItem.transaction != null && wishListItem.transaction.data.id != 0) {
                        transactionID = wishListItem.transaction.data.id;
                        quickInfoTXT.setText("Gift already sent.");
                        sendGiftBTN.setVisibility(View.GONE);
                    }
                    int size = wishListItem.tracker.data.size();
                    if (size > 0) {
                        if (wishListItem.tracker.data.get(size - 1).completed) {
                            quickInfoTXT.setVisibility(View.GONE);
                            awaitingTXT.setVisibility(View.VISIBLE);
                            awaitingTXT.setText("Gift already sent and received.");
                            viewGiftCardBTN.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }

        }
    }

    private void commentInfo(int commentCount) {
        commentTXT.setText(commentCount == 1 ? commentCount + " Comment" : NumberFormatter.format(commentCount) + " Comments");
        viewCommentBTN.setText(commentCount == 0 ? "Be the first to comment" : "Add Comment");
    }

    private void timelineInfo(WishListItem.Tracker tracker) {
        timelineCON.setVisibility(tracker.data.size() > 0 ? View.VISIBLE : View.GONE);
        timelineAdapter.setNewData(tracker.data);
    }

    private void attemptGetOngoingRequest(int id) {
        if (id == UserData.getUserId()) {
            wishlistPermissionRequest.first();
        } else {
            onGoingCON.setVisibility(View.GONE);
        }
    }

    private void commandButtonClick() {
        loadingPB.setVisibility(View.VISIBLE);
        commandBTN.setVisibility(View.GONE);
        if (isFollowing) {
            unFollowUser();
        } else {
            followUser();
        }
    }

    public void followUser() {
        GroupDialog.newInstance(wishListItem.owner.data, this).show(getFragmentManager(), GroupDialog.TAG);
    }

    @Override
    public void onGroupSelect(String groupName, int id) {
        Log.e("Group Name", ">>>" + groupName);
        FollowRequest followRequest = new FollowRequest(getContext());
        followRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .addParameters(Keys.server.key.USER_ID, id)
                .addParameters(Keys.server.key.GROUP, groupName)
                .execute();
    }

    @Override
    public void onCancel(String groupName, int id) {
        loadingPB.setVisibility(View.GONE);
        commandBTN.setVisibility(View.VISIBLE);
    }

    public void unFollowUser() {
        UnfollowDialog.newInstance(ownerID, ownerName, this).show(getChildFragmentManager(), UnfollowDialog.TAG);
    }

    private void attemptUnFollow() {
        UnFollowRequest unFollowRequest = new UnFollowRequest(getContext());
        unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, includeOwner)
                .addParameters(Keys.server.key.USER_ID, ownerID)
                .execute();
    }

    private void attemptLike() {
        LikeRequest likeRequest = new LikeRequest(getContext());
        likeRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .execute();
    }

    private void attemptRequestRequest() {
        new WishListRequestPermissionRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Sending Request...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, includeWishList)
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .execute();
    }

    private void attemptSendRequest(String dedication) {
        new WishListSendPermissionRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Sending Gift...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, includeWishList)
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .addParameters(Keys.server.key.DEDICATION_MESSAGE, dedication)
                .execute();
    }

    @Subscribe
    public void onResponse(LikeRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            basicInfo(wishListInfoTransformer.wishListItem);
            gemInfo(wishListInfoTransformer.wishListItem);
            UserData.updateRewardCrystal(wishListInfoTransformer.reward_crystal + "");
            UserData.updateRewardFragment(wishListInfoTransformer.reward_fragment + "");
            rateTXT.setText(" ( " + wishListItem.for_display_liker + " ) ");
//            MaterialShowcaseView.resetAll(getContext());
//            if (!UserData.getBoolean(UserData.WALKTHRU_DONE)) {
//                UserData.insert(UserData.WALKTHRU_CURRENT, 5);
//                itemActivity.startMainActivity("feed");
//            }
        }
    }

    @Subscribe
    public void onResponse(WishListDeleteRequest.ServerResponse responseData) {
        WishListInfoTransformer baseTransformer = responseData.getData(WishListInfoTransformer.class);
        if (baseTransformer.status) {
            getActivity().finish();
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(InfoWishListRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            sliderLayout.removeAllSliders();
            displayData(wishListInfoTransformer.wishListItem);
            wishListItem = wishListInfoTransformer.wishListItem;
            itemActivity.getRePostTXT().setVisibility(View.VISIBLE);
        }
        if (responseData.getCode() == 404) {
            contentCON.setVisibility(View.GONE);
            placeholder404CON.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onResponse(WishListSendPermissionRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            displayData(wishListInfoTransformer.wishListItem);
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);

        } else {
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(WishListRequestPermissionRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            displayData(wishListInfoTransformer.wishListItem);
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            ownerInfo(userTransformer.userItem);
        }
        loadingPB.setVisibility(View.GONE);
//        commandTXT.setVisibility(View.VISIBLE);
        commandBTN.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onResponse(FollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            ownerInfo(userTransformer.userItem);
        }
        loadingPB.setVisibility(View.GONE);
        commandBTN.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onResponse(WishlistPermissionRequest.ServerResponse responseData) {
        PermissionTransformer permissionTransformer = responseData.getData(PermissionTransformer.class);
        if (permissionTransformer.status) {
            if (responseData.isNext()) {
                requestOnGoingAdapter.addNewData(permissionTransformer.data);
            } else {
                requestOnGoingAdapter.setNewData(permissionTransformer.data);
            }

            allCommentRequest
                    .clearParameters()
                    .setDeviceRegID(itemActivity.getDeviceRegID())
                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                    .addParameters(Keys.server.key.INCLUDE, "author.info,tagged_user.info")
                    .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                    .showSwipeRefreshLayout(true)
                    .setPerPage(15)
                    .execute();

            Log.e("Data", ">>>" + new Gson().toJson(permissionTransformer.data));
            showMoreBTN.setVisibility(permissionTransformer.data.size() > 0 ? View.VISIBLE : View.GONE);
            if (permissionTransformer.data.size() > 0) {
                requestOnGoingEHLV.setVisibility(View.VISIBLE);
                ongoingRequestPlaceHolderCON.setVisibility(View.GONE);
                showMoreBTN.setVisibility(permissionTransformer.has_morepages ? View.VISIBLE : View.GONE);
            } else {
                requestOnGoingEHLV.setVisibility(View.GONE);
                ongoingRequestPlaceHolderCON.setVisibility(View.VISIBLE);
            }
        }
    }

    @Subscribe
    public void onResponse(AllCommentRequest.ServerResponse responseData) {
        CommentCollectionTransformer commentCollectionTransformer = responseData.getData(CommentCollectionTransformer.class);
        if (commentCollectionTransformer.status) {
            commentAdapter.setNewData(commentCollectionTransformer.data);
        }
        if (commentAdapter.getCount() == 0) {
            commentEHLV.setVisibility(View.GONE);
        } else {
            commentEHLV.setVisibility(View.VISIBLE);
        }

        if (responseData.getCode() == 404) {
            placeholder404CON.setVisibility(View.VISIBLE);
            getActivity().finish();
        } else {
            placeholder404CON.setVisibility(View.GONE);
        }

//        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)) {
//            switch (UserData.getInt(UserData.WALKTHRU_CURRENT)) {
//                case 4:
//                    if (requestAddressBTN.isShown()) {
//                        contentCON.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                contentCON.fullScroll(ScrollView.FOCUS_DOWN);
//                            }
//                        });
//                        Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                attemptGetRequestAddressWalkThrough();
//                            }
//                        },300);
//                    } else {
//                        heartSB.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                heartSB.scrollTo(0, heartSB.getBottom());
//                                attemptGetLikeWalkThrough();
//                            }
//                        });
//                    }
//                    break;
//                case 6:
//                    attemptGetRepostWalkThrough();
//                    break;
//                default:
//
//            }
//        }
    }

    @Subscribe
    public void onResponse(WishListRelatedRequest.ServerResponse responseData) {
        WishListRelatedTransformer wishListRelatedTransformer = responseData.getData(WishListRelatedTransformer.class);
        if (wishListRelatedTransformer.status) {
            if (responseData.isNext()) {
                socialFeedRecyclerViewAdapter.addNewData(wishListRelatedTransformer.data);
            } else {
                socialFeedRecyclerViewAdapter.setNewData(wishListRelatedTransformer.data);
            }
        }
        wishlistPermissionRequest
                .execute();

        loadingIV.setVisibility(View.GONE);
        relatedRV.setVisibility(View.VISIBLE);

        if (socialFeedRecyclerViewAdapter.getItemCount() == 0) {
            relatedCON.setVisibility(View.GONE);
        } else {
            relatedCON.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAccept(int userID) {
        attemptUnFollow();
    }

    @Override
    public void onCancel(int userID) {
        loadingPB.setVisibility(View.GONE);
        commandBTN.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAccept(int wishListID, String dedication) {
        attemptSendRequest(dedication);
    }

    @Override
    public void onSwipeRight() {
//        Log.e("Wishlist ID",">>>" + wishListItem.id);
    }

    @Override
    public void onSwipeLeft() {
//        Log.e("Wishlist ID",">>>" + wishListItem.id);
    }

    @Override
    public void onItemClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startItemActivity(wishListItem.id, wishListItem.owner.data.id, "i_like");
    }

    @Override
    public void onCommentClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startCommentActivity(wishListItem.id, wishListItem.title);
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        ((RouteActivity) getContext()).startProfileActivity(userItem.id);
    }

    @Override
    public void onGrabClick(WishListItem wishListItem) {
        String data = new Gson().toJson(wishListItem);
        itemActivity.startWishListActivity(data, "create_grab_other");
    }

    @Override
    public void onHideClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to hide?")
                .setNote("You will no longer see this post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Hide")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BlockRequest blockRequest = new BlockRequest(getContext());
                        blockRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("item_TransactionItemFragment_hideClickDialog");
    }

    @Override
    public void onUnfollowClick(final WishListItem wishListItem) {
        if (wishListItem.owner.data.social.data.is_following.equals("yes")) {
            UnfollowDialog.newInstance(wishListItem.owner.data.id, wishListItem.owner.data.name, new UnfollowDialog.Callback() {
                @Override
                public void onAccept(int userID) {
                    UnFollowRequest unFollowRequest = new UnFollowRequest(getContext());
                    unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                            .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                            .addParameters(Keys.server.key.USER_ID, wishListItem.owner.data.id)
                            .execute();
                }

                @Override
                public void onCancel(int userID) {

                }
            }).show(((BaseActivity) getContext()).getSupportFragmentManager(), UnfollowDialog.TAG);
        } else {

            GroupDialog.newInstance(wishListItem.owner.data, this).show(getFragmentManager(), GroupDialog.TAG);
        }
        Analytics.trackEvent("item_TransactionItemFragment_onUnfollowClickDialog");
    }

    @Override
    public void onReportClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to report?")
                .setNote("You are about to report a post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Report")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportRequest reportRequest = new ReportRequest(getContext());
                        reportRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.TYPE, "wishlist")
                                .addParameters(Keys.server.key.REFERENCE_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("item_TransactionItemFragment_onReportClickDialog");
    }

    @Override
    public void onHeartClick(WishListItem wishListItem) {

        LikeRequest likeRequest = new LikeRequest(getContext());
        likeRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social,images")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                .execute();

    }

    @Override
    public void onRepostClick(WishListItem wishListItem) {
        String data = new Gson().toJson(wishListItem);
        ((RouteActivity) getContext()).startWishListActivity(data, "create_grab_other");
//        itemActivity.startWishListActivity(wishListItem, "create_grab_other");
    }

    @Override
    public void onRepostUsersClick(WishListItem wishListItem) {
        itemActivity.startSearchActivity(wishListItem.id, wishListItem.owner.data.common_name, "repost");
    }

    @Override
    public void onHeaderClick(EventItem eventItem) {
        itemActivity.startBirthdayActivity(eventItem, "event");
    }

    @Override
    public void onHeaderShowClick(EventItem eventItem) {
        itemActivity.startBirthdayActivity(eventItem, "greeting");
    }

    @Subscribe
    public void onResponse(BlockRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            socialFeedRecyclerViewAdapter.removeItem(wishListInfoTransformer.wishListItem);
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Log.e("EEEEEEE" , ">>> " + slider.getUrl() + "");
        ViewImageDialog.newInstance(slider.getUrl(), wishListName).show(getChildFragmentManager(), ViewImageDialog.TAG);

    }

    private void otherOptionClick() {

        PopupMenu popup = new PopupMenu(getContext(), itemActivity.getRePostTXT());
        popup.getMenuInflater().inflate(R.menu.social_feed_menu_item, popup.getMenu());

        String report = popup.getMenu().findItem(R.id.reportBTN).getTitle().toString();
        SpannableString spannableString = new SpannableString(report);
        spannableString.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(getContext(), R.color.failed)), 0, spannableString.length(), 0);
        popup.getMenu().findItem(R.id.reportBTN).setTitle(spannableString);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.repostBTN:
                        String data = new Gson().toJson(wishListItem);
                        ((RouteActivity) getContext()).startWishListActivity(data, "create_grab_other");
                        Analytics.trackEvent("item_TransactionItemFragment_repostBTN");
                        break;
                    case R.id.hideBTN:
                        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
                        confirmationDialog
                                .setDescription("Are you sure you want to hide?")
                                .setNote("You will no longer see this post.")
                                .setIcon(R.drawable.icon_information)
                                .setPositiveButtonText("Hide")
                                .setPositiveButtonClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        BlockRequest blockRequest = new BlockRequest(getContext());
                                        blockRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                                                .execute();
                                        confirmationDialog.dismiss();
                                    }
                                })
                                .setNegativeButtonText("Cancel")
                                .build(getChildFragmentManager());
                        Analytics.trackEvent("item_TransactionItemFragment_hideBTN");
                        break;
                    case R.id.reportBTN:
                        final ConfirmationDialog confirmationDialogs = ConfirmationDialog.Builder();
                        confirmationDialogs
                                .setDescription("Are you sure you want to report?")
                                .setNote("You are about to report a post.")
                                .setIcon(R.drawable.icon_information)
                                .setPositiveButtonText("Report")
                                .setPositiveButtonClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        ReportRequest reportRequest = new ReportRequest(getContext());
                                        reportRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                                .addParameters(Keys.server.key.TYPE, "wishlist")
                                                .addParameters(Keys.server.key.REFERENCE_ID, wishListItem.id)
                                                .execute();
                                        confirmationDialogs.dismiss();
                                    }
                                })
                                .setNegativeButtonText("Cancel")
                                .build(getChildFragmentManager());
                        Analytics.trackEvent("item_TransactionItemFragment_reportBTN");
                        break;
                }

                return false;
            }
        });
        popup.show();
    }

    @Subscribe
    public void onResponse(ReportRequest.ServerResponse responseData) {
        ReportTransformer reportTransformer = responseData.getData(ReportTransformer.class);
        if (reportTransformer.status) {
            if (reportTransformer.data.is_removed) {

            }
            ToastMessage.show(getContext(), reportTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), reportTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(getContext(), android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }
    private void openFullscreenDialog() {

        ((ViewGroup) videoView.getParent()).removeView(videoView);
        mFullScreenDialog.addContentView(videoView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_fullscreen_skrink));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
    }

    private void closeFullscreenDialog() {
        ((ViewGroup) videoView.getParent()).removeView(videoView);
        videoCON.addView(videoView);
        mExoPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_fullscreen_expand));
    }

    private void initFullscreenButton() {

        PlaybackControlView controlView = videoView.findViewById(R.id.exo_controller);
        mFullScreenIcon = controlView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenButton = controlView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mExoPlayerFullscreen)
                    openFullscreenDialog();
                else
                    closeFullscreenDialog();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
//        MxVideoPlayer.releaseAllVideos();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }

        if (mFullScreenDialog != null){mFullScreenDialog.dismiss();}

    }
}
