package com.thingsilikeapp.android.fragment.birthday;

import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;

import com.daprlabs.cardstack.SwipeDeck;
import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.BirthdayActivity;
import com.thingsilikeapp.android.adapter.BirthdayGreetingsAdapter;
import com.thingsilikeapp.android.dialog.GiftCardPreviewDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.GreetingItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.gift.CelebrationRequest;
import com.thingsilikeapp.server.transformer.birthday.CelebrationTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.StringFormatter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class GreetingFragment extends BaseFragment
        implements BirthdayGreetingsAdapter.ClickListener{

    public static final String TAG = GreetingFragment.class.getName();

    private BirthdayActivity birthdayActivity;
    private BirthdayGreetingsAdapter birthdayGreetingsAdapter;

    @BindView(R.id.greetingSD)                  SwipeDeck greetingSD;
    @BindView(R.id.placeHolderCON)              View placeHolderCON;

    @State  String raw;

    private EventItem eventItem;

    public static GreetingFragment newInstance(String raw) {
        GreetingFragment greetingFragment = new GreetingFragment();
        greetingFragment.raw = raw;
        return greetingFragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_greeting;
    }

    @Override
    public void onViewReady() {
        birthdayActivity = (BirthdayActivity)getContext();
        eventItem = new Gson().fromJson(raw, EventItem.class);
        if(eventItem.id == 0){
            birthdayActivity.setTitle("Birthday Greetings");
        }else{
            birthdayActivity.setTitle(StringFormatter.titleCase(eventItem.title));
        }
        setBirthdayGreeting();
    }

    @Override
    public void onResume() {
        super.onResume();
        attemptAPICall();
    }

    private void setBirthdayGreeting(){
        birthdayGreetingsAdapter = new BirthdayGreetingsAdapter(getContext());
        birthdayGreetingsAdapter.setOnItemClickListener(this);
        greetingSD.setHardwareAccelerationEnabled(true);
        greetingSD.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
//                birthdayGreetingsAdapter.reInsert(position);
            }

            @Override
            public void cardSwipedRight(int position) {
//                birthdayGreetingsAdapter.reInsert(position);
            }

            @Override
            public void cardsDepleted() {
                greetingSD.setAdapter(birthdayGreetingsAdapter);
            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp() {

            }
        });
    }
    private void attemptAPICall(){
        Log.e("EventID", ">>>" + eventItem.id);
        CelebrationRequest sendGreetRequest = new CelebrationRequest(getContext());
        sendGreetRequest
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.EVENT_ID, eventItem.id)
                .execute();
    }

    @Subscribe
    public void onResponse(CelebrationRequest.ServerResponse responseData) {
        CelebrationTransformer baseTransformer = responseData.getData(CelebrationTransformer.class);
        if(baseTransformer.status){
            birthdayGreetingsAdapter.setNewData(baseTransformer.data);
            greetingSD.setAdapter(birthdayGreetingsAdapter);
        }

        greetingSD.setVisibility(birthdayGreetingsAdapter.getData().size() == 0 ? View.GONE : View.VISIBLE);
        placeHolderCON.setVisibility(birthdayGreetingsAdapter.getData().size() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void show(GreetingItem greetingItem) {
        GiftCardPreviewDialog.newInstance(greetingItem)
                .show(getChildFragmentManager(), GiftCardPreviewDialog.TAG);
    }
}
