package com.thingsilikeapp.android.fragment.rewards;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.dialog.RedeemRewardConfirmationDialog;
import com.thingsilikeapp.android.dialog.RedeemRewardFailedDialog;
import com.thingsilikeapp.android.dialog.RedeemRewardSuccessDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Share;
import com.thingsilikeapp.data.model.CatalogueItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.reward.RewardCatalogueRedeemRequest;
import com.thingsilikeapp.server.request.reward.RewardCatalogueShowRequest;
import com.thingsilikeapp.server.transformer.reward.RewardCatalogueShowTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;
import com.thingsilikeapp.vendor.android.widget.image.FixedWidthImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

import static com.thingsilikeapp.vendor.android.java.GemHelper.parseDouble;

public class RedeemableDetailFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener{

    public static final String TAG = RedeemableDetailFragment.class.getName();

    private RewardsActivity rewardsActivity;
    private CatalogueItem catalogueItem;

    public RequestListener requestListener;

    @BindView(R.id.shopSRL)                     MultiSwipeRefreshLayout shopSRL;
    @BindView(R.id.rewardCrystalTXT)            TextView rewardCrystalTXT;
    @BindView(R.id.rewardFragmentTXT)           TextView rewardFragmentTXT;
    @BindView(R.id.infoBTN)                     View infoBTN;
    @BindView(R.id.contentCON)                  View contentCON;
    @BindView(R.id.claimCON)                    View claimCON;
    @BindView(R.id.deliveryCON)                 View deliveryCON;
    @BindView(R.id.deliveryTXT)                 TextView deliveryTXT;

    @BindView(R.id.redeemableIV)                FixedWidthImageView redeemableIV;
    @BindView(R.id.nameTXT)                     TextView nameTXT;
    @BindView(R.id.dateTXT)                     TextView dateTXT;
    @BindView(R.id.typeIV)                      ImageView typeIV;
    @BindView(R.id.valueTXT)                    TextView valueTXT;
    @BindView(R.id.contentTXT)                  TextView contentTXT;
    @BindView(R.id.descriptionTXT)              TextView descriptionTXT;
    @BindView(R.id.noteCON)                     View noteCON;
    @BindView(R.id.noteTXT)                     TextView noteTXT;
    @BindView(R.id.remainingTXT)                TextView remainingTXT;
    @BindView(R.id.redeemBTN)                   View redeemBTN;
    @BindView(R.id.shareBTN)                    View shareBTN;
    @BindView(R.id.descriptionCON)              View descriptionCON;

    @State int id;
    @State String catalogueItemRaw;

    public static RedeemableDetailFragment newInstance(int id) {
        RedeemableDetailFragment redeemableDetailFragment = new RedeemableDetailFragment();
        redeemableDetailFragment.id = id;
        return redeemableDetailFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_shop_detail;
    }

    @Override
    public void onViewReady() {
        rewardsActivity = (RewardsActivity) getContext();
        rewardsActivity.setTitle("Product Details");
        rewardsActivity.showOption(false);
        rewardsActivity.setTitleDark(false);
        rewardsActivity.showWallet(false);
        shopSRL.setOnRefreshListener(this);
        infoBTN.setOnClickListener(this);
        redeemBTN.setOnClickListener(this);
        shareBTN.setOnClickListener(this);
        shareBTN.setEnabled(false);
        redeemableIV.autoResize(true);
        Analytics.trackEvent("rewards_RedeemableDetailFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public void onRefresh() {
        refresh();
    }

    private void refresh(){

        contentCON.setVisibility(View.INVISIBLE);

        shopSRL.setColorSchemeResources(R.color.colorPrimary);
        shopSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        shopSRL.setOnRefreshListener(this);
        shopSRL.setSwipeableChildren(R.id.shopDetailSV);

        new RewardCatalogueShowRequest(getContext())
                .setSwipeRefreshLayout(shopSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "image")
                .addParameters(Keys.server.key.CATALOGUE_ID, id)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.infoBTN:
                rewardsActivity.openMyTreasuryFragment();
                Analytics.trackEvent("rewards_RedeemableDetailFragment_infoBTN");
                break;
            case R.id.redeemBTN:
//                redeemNow();
                if(parseDouble(UserData.getRewardCrystal()) >= catalogueItem.value){
                    rewardsActivity.openConfirmPurchaseFragment(catalogueItem);
                }else{
                    RedeemRewardFailedDialog.newInstance(catalogueItem).show(getFragmentManager(), RedeemRewardFailedDialog.TAG);
                }
                Analytics.trackEvent("rewards_RedeemableDetailFragment_redeemBTN");
                break;
            case R.id.shareBTN:
//                String data = new Gson().toJson(wishListItem());
//                rewardsActivity.startWishListActivity(data, "create_grab_other");
                shareTheApp(catalogueItem);
                Analytics.trackEvent("rewards_RedeemableDetailFragment_shareBTN");
                break;
        }
    }

    public void shareTheApp(CatalogueItem wishListItem) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                UserData.getUserItem().username + " posted " +
                        wishListItem.title + " on LYKA " +" . Check out what everyone likes and discover your modern wishlist app");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Share.URL + "/share/i/" + wishListItem.id);
        startActivity(Intent.createChooser(sharingIntent, "Share LYKA"));
        Analytics.trackEvent("main_ProfileFragment_shareTheApp");
    }

//    private void redeemNow(){
//        double value = 0;
//        if(valueTXT.getTag() != null){
//            value = (double) valueTXT.getTag();
//        }
//        if(parseDouble(UserData.getRewardCrystal()) >= value){
//            RedeemRewardConfirmationDialog.newInstance(catalogueItem).show(getFragmentManager(), RedeemRewardConfirmationDialog.TAG);
//        }else{
//            RedeemRewardFailedDialog.newInstance(catalogueItem).show(getFragmentManager(), RedeemRewardFailedDialog.TAG);
//        }
//    }
//
//    public double parseDouble(String amount){
//        if(StringFormatter.isEmpty(amount)){
//            return 0;
//        }
//        return Double.parseDouble(amount);
//    }

    private WishListItem wishListItem(){
        WishListItem wishListItem = new WishListItem();

        wishListItem.id = catalogueItem.id;
        wishListItem.info = new WishListItem.Info();
        wishListItem.info.data = new WishListItem.Info.InfoData();
        wishListItem.info.data.content = catalogueItem.title;
//        wishListItem.info.data.url = catalogueItem.image.data.full_path;
        wishListItem.owner = new WishListItem.Owner();
        wishListItem.owner.data = new UserItem();
        wishListItem.owner.data.id = 0;

        wishListItem.image = new WishListItem.Image();
        wishListItem.image.data = new WishListItem.Image.ImageData();
        wishListItem.image.data.full_path = catalogueItem.image.data.full_path;

        return wishListItem;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(RewardCatalogueShowRequest.ServerResponse responseData) {
        RewardCatalogueShowTransformer rewardCatalogueShowTransformer = responseData.getData(RewardCatalogueShowTransformer.class);
        if(rewardCatalogueShowTransformer.status){
            contentCON.setVisibility(View.VISIBLE);
            UserData.updateRewardCrystal(rewardCatalogueShowTransformer.user.reward_crystal + "");
            UserData.updateDisplayRewardCrystal(rewardCatalogueShowTransformer.user.lykagem_display + "");
            UserData.updateRewardFragment(rewardCatalogueShowTransformer.user.reward_fragment + "");
            rewardCrystalTXT.setText(UserData.getRewardCrystal());
            rewardFragmentTXT.setText(UserData.getRewardFragment());
            shareBTN.setEnabled(true);
            Log.e("EEEEEEEE", ">>>>>>" + rewardCatalogueShowTransformer.data.id);
            displayData(rewardCatalogueShowTransformer.data);
        }
    }

    private void attemptRedeem(){
        RewardCatalogueRedeemRequest rewardCatalogueRedeemRequest = new RewardCatalogueRedeemRequest(getContext());
        rewardCatalogueRedeemRequest
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.CATALOGUE_ID, id)
                .addParameters(Keys.server.key.INCLUDE, "image")
                .execute();
    }

    @Subscribe
    public void onResponse(RewardCatalogueRedeemRequest.ServerResponse responseData) {
        RewardCatalogueShowTransformer rewardCatalogueShowTransformer = responseData.getData(RewardCatalogueShowTransformer.class);
        if(rewardCatalogueShowTransformer.status){
            UserData.updateRewardCrystal(rewardCatalogueShowTransformer.user.reward_crystal + "");
            UserData.updateRewardFragment(rewardCatalogueShowTransformer.user.reward_fragment + "");
            rewardCrystalTXT.setText(UserData.getRewardCrystal());
            rewardFragmentTXT.setText(UserData.getRewardFragment());
            displayData(rewardCatalogueShowTransformer.data);
            RedeemRewardSuccessDialog.newInstance(rewardCatalogueShowTransformer.data).show(((BaseActivity) getContext()).getSupportFragmentManager(), RedeemRewardSuccessDialog.TAG);
        }else if(rewardCatalogueShowTransformer.statusCode.equalsIgnoreCase("INSUFFICIENT_FUND")){
            RedeemRewardFailedDialog.newInstance(rewardCatalogueShowTransformer.data).show(getFragmentManager(), RedeemRewardFailedDialog.TAG);
        }
    }

    private CatalogueItem getCatalogue(){
        return new Gson().fromJson(catalogueItemRaw, CatalogueItem.class);
    }



    private void displayData(CatalogueItem catalogueItem){
        this.catalogueItem = catalogueItem;
        catalogueItemRaw = catalogueItem.toString();
        nameTXT.setText(catalogueItem.title);

//        requestListener = new RequestListener<String, GlideDrawable>() {
//            @Override
//            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                return false;
//            }
//
//            @Override
//            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
////               shareBTN.setClickable(true);
//                return false;
//            }
//        };

        Glide.with(getContext())
                .load(catalogueItem.image.data.full_path)
                .listener(requestListener)
                .apply(new RequestOptions()
                .placeholder(R.color.light_gray)
                .error(R.color.light_gray))
                .into(redeemableIV);

        typeIV.setImageDrawable(
                ActivityCompat.getDrawable(getContext(),
                        catalogueItem.reward_type.equalsIgnoreCase("crystal") ?
                                R.drawable.lyka_gem_big :
                                R.drawable.lyka_shards_big));
        double doubles = catalogueItem.value;
        int ints = (int) doubles;
        valueTXT.setText(String.valueOf(ints));
        valueTXT.setTag(catalogueItem.value);
        descriptionTXT.setText(Html.fromHtml(catalogueItem.description, null, new UlTagHandler()));

        if(StringFormatter.isEmpty(catalogueItem.content)){
            claimCON.setVisibility(View.GONE);
        }else{
            claimCON.setVisibility(View.VISIBLE);
            contentTXT.setText(Html.fromHtml(catalogueItem.content, null, new UlTagHandler()));
        }

        if(StringFormatter.isEmpty(catalogueItem.delivery_details)){
            deliveryCON.setVisibility(View.GONE);
        }else{
            deliveryCON.setVisibility(View.VISIBLE);
            deliveryTXT.setText(Html.fromHtml(catalogueItem.delivery_details, null, new UlTagHandler()));
        }

        if(StringFormatter.isEmpty(catalogueItem.note)){
            noteCON.setVisibility(View.GONE);
        }else{
            noteCON.setVisibility(View.VISIBLE);
            noteTXT.setText(Html.fromHtml(catalogueItem.note, null, new UlTagHandler()));
        }
        descriptionCON.setVisibility(StringFormatter.isEmpty(catalogueItem.description) ? View.GONE : View.VISIBLE);
        remainingTXT.setText(catalogueItem.remaining + " left");
//        redeemBTN.setVisibility(catalogueItem.is_redeemable.equalsIgnoreCase("yes") ? View.VISIBLE : View.GONE);
    }

}


