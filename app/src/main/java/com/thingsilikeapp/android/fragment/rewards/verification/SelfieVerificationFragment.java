package com.thingsilikeapp.android.fragment.rewards.verification;

import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.VerificationActivity;
import com.thingsilikeapp.android.dialog.ImagePickerV2Dialog;
import com.thingsilikeapp.data.model.KYCItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.KYC;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.widget.image.ResizableImageView;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import butterknife.BindView;
import icepick.State;

public class SelfieVerificationFragment extends BaseFragment implements ImagePickerV2Dialog.ImageCallback, View.OnClickListener {

    public static final String TAG = SelfieVerificationFragment.class.getName();

    @BindView(R.id.attachBTN)   TextView attachBTN;
    @BindView(R.id.submitBTN)   TextView submitBTN;
    @BindView(R.id.imageIV)     ResizableImageView imageView;

    @State
    File file;

    private VerificationActivity verificationActivity;

    public static SelfieVerificationFragment newInstance() {
        SelfieVerificationFragment fragment = new SelfieVerificationFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_verification_selfie;
    }

    @Override
    public void onViewReady() {
        verificationActivity = (VerificationActivity) getContext();
        verificationActivity.setTitle("Selfie Verification");
        attachBTN.setOnClickListener(this);
        submitBTN.setOnClickListener(this);
        verificationActivity.setBackBTN(true);
        Analytics.trackEvent("rewards_verification_SelfieVerificationFragment_onViewReady");
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.attachBTN:
                ImagePickerV2Dialog.newInstance("Upload Photo", false, this).show(getFragmentManager(), TAG);
                Analytics.trackEvent("rewards_verification_SelfieVerificationFragment_attachBTN");
                break;
            case R.id.submitBTN:
                KYC.getDefault().selfie(getContext(), file);
                Analytics.trackEvent("rewards_verification_SelfieVerificationFragment_submitBTN");
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(KYC.SelfieResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            verificationActivity.openVerificationFragment();
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }


    @Override
    public void result(File file) {
        this.file = file;
        Picasso.with(getContext()).load(file).into(imageView);
    }
}
