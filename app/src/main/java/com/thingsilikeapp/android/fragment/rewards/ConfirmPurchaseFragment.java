package com.thingsilikeapp.android.fragment.rewards;

import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.adapter.AddressPickerRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.RedeemRewardConfirmationDialog;
import com.thingsilikeapp.android.dialog.RedeemRewardFailedDialog;
import com.thingsilikeapp.data.model.AddressBookItem;
import com.thingsilikeapp.data.model.CatalogueItem;
import com.thingsilikeapp.data.model.OrderItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Order;
import com.thingsilikeapp.server.Passcode;
import com.thingsilikeapp.server.request.address.AllAddressRequest;
import com.thingsilikeapp.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ConfirmPurchaseFragment extends BaseFragment implements AddressPickerRecycleViewAdapter.ClickListener, AddressBookCreateFragment.Callback, View.OnClickListener, RedeemRewardConfirmationDialog.Callback {

    public static final String TAG = ConfirmPurchaseFragment.class.getName();

    private RewardsActivity rewardsActivity;
    private CatalogueItem catalogueItem;
    public RequestListener requestListener;
    private AddressPickerRecycleViewAdapter addressPickerRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private AllAddressRequest allAddressRequest;

    @BindView(R.id.itemNameTXT)         TextView itemNameTXT;
    @BindView(R.id.itemName2TXT)        TextView itemName2TXT;
    @BindView(R.id.addressBTN)          TextView addressBTN;
    @BindView(R.id.confirmBuyBTN)       TextView confirmBuyBTN;
    @BindView(R.id.valueTXT)            TextView valueTXT;
    @BindView(R.id.ordernoteTXT)        EditText ordernoteTXT;
    @BindView(R.id.addressRV)           RecyclerView addressRV;
    @BindView(R.id.imageIV)             ImageView imageIV;

    public static ConfirmPurchaseFragment newInstance(CatalogueItem catalogueItem) {
        ConfirmPurchaseFragment fragment = new ConfirmPurchaseFragment();
        fragment.catalogueItem = catalogueItem;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_confirm_purchase;
    }

    @Override
    public void onViewReady() {
        rewardsActivity = (RewardsActivity) getContext();
        rewardsActivity.setTitle("Confirm Purchase");
        itemNameTXT.setText(catalogueItem.title);
        itemName2TXT.setText("(1) " + catalogueItem.title);
        valueTXT.setText(String.valueOf(catalogueItem.value));
        confirmBuyBTN.setOnClickListener(this);
//        requestListener = new RequestListener<String, GlideDrawable>() {
//            @Override
//            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                return false;
//            }
//
//            @Override
//            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
////               shareBTN.setClickable(true);
//                return false;
//            }
//        };

        requestListener = new RequestListener() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        };


        Glide.with(getContext())
                .load(catalogueItem.image.data.full_path)
                .listener(requestListener)
                .apply(new RequestOptions()
                .fitCenter()
                .placeholder(R.color.light_gray)
                .error(R.color.light_gray))
                .into(imageIV);

        initAPICall();
        setUpAddressBookListView();
            addressBTN.setOnClickListener(this);
        Analytics.trackEvent("rewards_ConfirmPurchaseFragment_onViewReady");
    }

    private void setUpAddressBookListView(){
        addressPickerRecycleViewAdapter = new AddressPickerRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        addressRV.setLayoutManager(linearLayoutManager);
        addressRV.setAdapter(addressPickerRecycleViewAdapter);
        addressPickerRecycleViewAdapter.setOnItemClickListener(this);
    }


    private void initAPICall(){
        allAddressRequest = new AllAddressRequest(getContext());
        allAddressRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(10)
                .execute();
    }

    private void redeemNow(){
        Log.e("MYGEM", ">>>" + UserData.getRewardCrystal());
//        if(parseDouble(UserData.getRewardCrystal()) >= catalogueItem.value){
            RedeemRewardConfirmationDialog.newInstance(catalogueItem, this).show(getFragmentManager(), RedeemRewardConfirmationDialog.TAG);
//        }else{
//            RedeemRewardFailedDialog.newInstance(catalogueItem).show(getFragmentManager(), RedeemRewardFailedDialog.TAG);
//        }
    }

    public double parseDouble(String amount){
        if(StringFormatter.isEmpty(amount)){
            return 0;
        }
        return Double.parseDouble(amount);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
    @Override
    public void onResume() {
        super.onResume();
        allAddressRequest.first();
    }

    @Override
    public void onItemClick(AddressBookItem addressBookItem) {
        addressPickerRecycleViewAdapter.setItemSelected(addressBookItem.id);
        Log.e("FFFFFFF", ">>>>>" + addressPickerRecycleViewAdapter.getSelectedID());
    }

    @Subscribe
    public void onResponse(AllAddressRequest.ServerResponse responseData) {
        CollectionTransformer<AddressBookItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if (UserData.getUserItem().is_verified){
                AddressBookItem addressBookItem = new AddressBookItem();
                addressBookItem.id = -1;
                addressBookItem.address_label = "Pick up center";
                addressBookItem.street_address = "C/O Highly Succeed Inc., 28F Tower 2 Enterprise Center, Ayala-Paseo de Roxas";
                addressBookItem.phone_number = "09062708663";
                addressBookItem.state = "Metro Manila";
                addressBookItem.city = "Makati";
                addressBookItem.country = "Philippines";
                addressBookItem.zip_code = "1226";
                addressBookItem.is_default = "no";
                addressBookItem.selected = false;
                collectionTransformer.data.add(0, addressBookItem);
            }
            if(responseData.isNext()){
                addressPickerRecycleViewAdapter.addNewData(collectionTransformer.data);
            }else{
                addressPickerRecycleViewAdapter.setNewData(collectionTransformer.data);
            }
            addressPickerRecycleViewAdapter.setDefaultSelected();
        }

        if (addressPickerRecycleViewAdapter.getItemCount() == 0){
//			((RouteActivity) getContext()).startAddressBookActivity("", "create");
        }
    }

    @Subscribe
    public void onResponse(Order.OrderResponse responseData) {
        SingleTransformer<OrderItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            rewardsActivity.startRewardsActivity("treasury");
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onSuccess() {
        allAddressRequest.first();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.addressBTN:
                rewardsActivity.openAddAddressFragment(this);
                Analytics.trackEvent("rewards_ConfirmPurchaseFragment_addressBTN");
                break;
            case R.id.confirmBuyBTN:
                redeemNow();
                Analytics.trackEvent("rewards_ConfirmPurchaseFragment_confirmBuyBTN");
                break;
        }
    }

    @Override
    public void onRedeemSuccess() {
        Order.getDefault().create(getContext(), catalogueItem.id, addressPickerRecycleViewAdapter.getSelectedID(), ordernoteTXT.getText().toString());
    }
}
