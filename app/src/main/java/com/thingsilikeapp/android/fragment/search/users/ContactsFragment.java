package com.thingsilikeapp.android.fragment.search.users;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.ContactsRecycleViewAdapter;
import com.thingsilikeapp.config.App;
import com.thingsilikeapp.data.model.ContactsItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.social.InviteContactsRequest;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class ContactsFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        ContactsRecycleViewAdapter.ClickListener,
        View.OnClickListener{

    public static final String TAG = ContactsFragment.class.getName();

    private static final int PERMISSION_READ_CONTACTS = 102;

    private ContactsRecycleViewAdapter contactsRecycleViewAdapter;

    @State boolean selectAll = false;

    @BindView(R.id.topUserEHLV)                 RecyclerView topUserEHLV;
    @BindView(R.id.suggestionSRL)               MultiSwipeRefreshLayout suggestionSRL;
    @BindView(R.id.suggestionCON)               View suggestionCON;
    @BindView(R.id.loadMorePB)                  View loadMorePB;
    @BindView(R.id.searchET)                    EditText searchET;
    @BindView(R.id.searchBTN)                   View searchBTN;
    @BindView(R.id.placeHolderCON)              View placeHolderCON;
    @BindView(R.id.multiSelectCON)              View multiSelectCON;
    @BindView(R.id.selectAllBTN)                RadioButton selectAllBTN;
    @BindView(R.id.selectAllTXT)                View selectAllTXT;
    @BindView(R.id.cancelBTN)                   View cancelBTN;
    @BindView(R.id.shareBTN)                    View shareBTN;
    @BindView(R.id.commandTXT)                  View commandTXT;
    @BindView(R.id.orBTN)                       View orBTN;
    @BindView(R.id.SMSCountTXT)                 TextView SMSCountTXT;
    @BindView(R.id.placeHolderLoadingCON)       View placeHolderLoadingCON;

    public static ContactsFragment newInstance() {
        ContactsFragment contactsFragment = new ContactsFragment();
        return contactsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_contacts;
    }

    @Override
    public void onViewReady() {
        setupContactListView();
        setSwipeRefresh();
        displayData();
        searchBTN.setOnClickListener(this);
        searchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    contactsRecycleViewAdapter.getFilter().filter(searchET.getText().toString());
                    if (contactsRecycleViewAdapter.getItemCount() == 0){
                        placeHolderCON.setVisibility(View.VISIBLE);
                        topUserEHLV.setVisibility(View.GONE);
                    }else {
                        placeHolderCON.setVisibility(View.GONE);
                        topUserEHLV.setVisibility(View.VISIBLE);
                    }
                    Keyboard.hideKeyboard(getActivity());
                    return true;
                }
                return false;
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            topUserEHLV.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View view, int i, int i1, int i2, int i3) {
//                    Keyboard.hideKeyboard(getActivity());
                }
            });
        }
        initSearch();
        Analytics.trackEvent("search_users_ContactsFragment_onViewReady");
    }

    private void setSwipeRefresh(){
        cancelBTN.setOnClickListener(this);
        commandTXT.setOnClickListener(this);
        selectAllTXT.setOnClickListener(this);
        shareBTN.setOnClickListener(this);

        selectAllBTN.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    contactsRecycleViewAdapter.selectAll();
                }else{
                    contactsRecycleViewAdapter.unSelectAll();
                }
            }
        });

        suggestionSRL.setColorSchemeResources(R.color.colorPrimary);
        suggestionSRL.setSwipeableChildren(R.id.topUserEHLV, R.id.placeHolderCON);
        suggestionSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        suggestionSRL.setOnRefreshListener(ContactsFragment.this);
        commandTXT.setVisibility(View.INVISIBLE);
        orBTN.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void displayData() {
        suggestionSRL.setRefreshing(false);
        if(PermissionChecker.checkPermissions(getContext(), Manifest.permission.READ_CONTACTS, PERMISSION_READ_CONTACTS)){
            new ContactTask().execute();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupContactListView() {
        contactsRecycleViewAdapter = new ContactsRecycleViewAdapter(getContext());
        topUserEHLV.setLayoutManager(new LinearLayoutManager(getContext()));
        topUserEHLV.setAdapter(contactsRecycleViewAdapter);
        contactsRecycleViewAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_READ_CONTACTS) {
            if(((RouteActivity)getContext()).isAllPermissionResultGranted(grantResults)){
               displayData();
            }
        }
    }

    public void initSearch(){
        if(searchET != null){
            searchET.setHint("Search People...");
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    contactsRecycleViewAdapter.getFilter().filter(s);
                    if (contactsRecycleViewAdapter.getItemCount() == 0){
                        placeHolderCON.setVisibility(View.VISIBLE);
                        topUserEHLV.setVisibility(View.GONE);
                    }else {
                        placeHolderCON.setVisibility(View.GONE);
                        topUserEHLV.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void afterTextChanged(final Editable s) {
//                    contactsRecycleViewAdapter.getFilter().filter(s);
//                    if (contactsRecycleViewAdapter.getItemCount() == 0){
//                        placeHolderCON.setVisibility(View.VISIBLE);
//                        topUserEHLV.setVisibility(View.GONE);
//                    }else {
//                        placeHolderCON.setVisibility(View.GONE);
//                        topUserEHLV.setVisibility(View.VISIBLE);
//                    }
                }
            });
        }
    }

    private void refreshList(){
        displayData();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onInviteClick(ContactsItem contactsItem) {
        attemptInvite();
        sendSMS(contactsItem.number);
    }

    @Override
    public void onItemLongClick(int position) {
        contactsRecycleViewAdapter.setMultipleSelect(true);
        contactsRecycleViewAdapter.setItemSelected(position, true);
        showMultiSelectOption(true);
    }

    @Override
    public void onItemSelect(int position) {
        boolean show = contactsRecycleViewAdapter.hasCount();
        int count = contactsRecycleViewAdapter.getSelectedCount();
        commandTXT.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
        orBTN.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 14, getContext().getResources().getDisplayMetrics());
        SMSCountTXT.setText("("  + count + ")");

//        if(SMSCount != null){
//            SMSCount.setVisibility(View.VISIBLE);
//            SMSCount.setImageDrawable(new BadgeDrawable.Builder()
//                    .type(BadgeDrawable.TYPE_ONLY_ONE_TEXT)
//                    .number(count)
//                    .text1("(" + count + ")")
//                    .textSize(px)
//                    .build());
//        }

    }

    private void showMultiSelectOption(boolean show){
        multiSelectCON.setVisibility(show ? View.VISIBLE : View.GONE);
        selectAll = false;
        selectAllBTN.setChecked(selectAll);
    }

    private void attemptInvite(){
        InviteContactsRequest inviteContactsRequest = new InviteContactsRequest(getContext());
        inviteContactsRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }

    private void sendSMS(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + phoneNumber));
        intent.putExtra("sms_body", App.SHARE_PROFILE.replace("{username}", UserData.getUserItem().username));
        intent.putExtra("compose_mode", true);
        startActivity(intent);
    }

    @Subscribe
    public void onResponse(InviteContactsRequest.ServerResponse responseData) {
        BaseTransformer wishListInfoTransformer = responseData.getData(BaseTransformer.class);
        if(wishListInfoTransformer.status){

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cancelBTN:
                contactsRecycleViewAdapter.setMultipleSelect(false);
                multiSelectCON.setVisibility(View.GONE);
                Analytics.trackEvent("search_users_ContactsFragment_cancelBTN");
                break;
            case R.id.commandTXT:
                attemptInvite();
                sendSMS(contactsRecycleViewAdapter.getPhoneNumber());
//                contactsRecycleViewAdapter.setMultipleSelect(false);
//                multiSelectCON.setVisibility(View.GONE);
                Analytics.trackEvent("search_users_ContactsFragment_commandTXT");
                break;
            case R.id.selectAllTXT:
                selectAll = !selectAll;
                selectAllBTN.setChecked(selectAll);
                Analytics.trackEvent("search_users_ContactsFragment_selectAllTXT");
                break;
            case R.id.shareBTN:
                shareTheApp();
                Analytics.trackEvent("search_users_ContactsFragment_shareBTN");
                break;
            case R.id.searchBTN:
                contactsRecycleViewAdapter.getFilter().filter(searchET.getText().toString());
                if (contactsRecycleViewAdapter.getItemCount() == 0){
                    placeHolderCON.setVisibility(View.VISIBLE);
                    topUserEHLV.setVisibility(View.GONE);
                }else {
                    placeHolderCON.setVisibility(View.GONE);
                    topUserEHLV.setVisibility(View.VISIBLE);
                }
                Keyboard.hideKeyboard(getActivity());
                Analytics.trackEvent("search_users_ContactsFragment_searchBTN");
                break;
        }
    }

    public void shareTheApp(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "LYKA");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, App.SHARE_PROFILE.replace("{username}", UserData.getUserItem().username));
        startActivity(Intent.createChooser(sharingIntent, "Share LYKA"));
    }

    private class ContactTask extends AsyncTask<Object, Object, List<ContactsItem>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            suggestionSRL.setRefreshing(true);
            placeHolderLoadingCON.setVisibility(View.VISIBLE);
            placeHolderCON.setVisibility(View.GONE);
            topUserEHLV.setVisibility(View.GONE);
        }

        @Override
        protected List<ContactsItem> doInBackground(Object... objects) {
            List<ContactsItem> contactsItems = new ArrayList();
            ContactsItem contactsItem;
            ContentResolver contentResolver = getContext().getContentResolver();
            Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
            if (cursor != null){
                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext()) {
                        int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                        if (hasPhoneNumber > 0) {
                            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                            Cursor phoneCursor = contentResolver.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                    new String[]{id},
                                    null);

                            contactsItem = new ContactsItem();
                            contactsItem.name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                            contactsItem.image = cursor .getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

                            if (phoneCursor.moveToNext()) {
                                contactsItem.number = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            }

                            phoneCursor.close();
                            contactsItems.add(contactsItem);
                        }
                    }
                }
            }
            return contactsItems;
        }

        @Override
        protected void onPostExecute(List<ContactsItem> contactsItems) {
            super.onPostExecute(contactsItems);

            if(suggestionSRL != null){
                suggestionSRL.setRefreshing(false);
            }

            showView(multiSelectCON, true);
            showView(placeHolderLoadingCON, false);
            showView(placeHolderCON, false);
            showView(topUserEHLV, true);

            if(contactsRecycleViewAdapter != null){
                contactsRecycleViewAdapter.setNewData(contactsItems);
                contactsRecycleViewAdapter.setMultipleSelect(true);
            }
        }

        private void showView(View view, boolean show){
            if(view != null){
                view.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        }
    }
}
