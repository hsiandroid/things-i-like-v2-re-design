package com.thingsilikeapp.android.fragment.rewards.treasury;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.adapter.OrderRecycleViewAdapter;
import com.thingsilikeapp.data.model.OrderItem;
import com.thingsilikeapp.server.Order;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class MyRewardsFragment extends BaseFragment implements
        OrderRecycleViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener, EndlessRecyclerViewScrollListener.Callback {

    public static final String TAG = MyRewardsFragment.class.getName();

    private OrderRecycleViewAdapter myRewardsRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private RewardsActivity rewardsActivity;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;

    private APIRequest apiRequest;

    @BindView(R.id.myRewardsSRL)            MultiSwipeRefreshLayout myRewardsSRL;
    @BindView(R.id.myRewardsRV)             RecyclerView myRewardsRV;
    @BindView(R.id.placeHolderCON)          View placeHolderCON;

    public static MyRewardsFragment newInstance() {
        MyRewardsFragment myRewardsFragment = new MyRewardsFragment();
        return myRewardsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_my_rewards;
    }

    @Override
    public void onViewReady() {
        rewardsActivity = (RewardsActivity) getContext();
        setUpMyOrders();
        initMyRewardAPI();
        refreshList();
        Analytics.trackEvent("rewards_treasury_MyRewardsFragment_onViewReady");
    }

    private void initMyRewardAPI(){
        myRewardsSRL.setColorSchemeResources(R.color.colorPrimary);
        myRewardsSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        myRewardsSRL.setOnRefreshListener(this);
        myRewardsSRL.setSwipeableChildren(R.id.myRewardsRV);

        apiRequest = Order.getDefault().getAll(getContext(), myRewardsSRL);
        apiRequest.first();
    }

    private void setUpMyOrders(){
        myRewardsRecycleViewAdapter = new OrderRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        myRewardsRV.setLayoutManager(linearLayoutManager);
        myRewardsRV.setAdapter(myRewardsRecycleViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        myRewardsRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        myRewardsRecycleViewAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void refreshList(){
        apiRequest = Order.getDefault().getAll(getContext(), myRewardsSRL);
        apiRequest.first();
    }


    @Subscribe
    public void onResponse(Order.AllOrderResponse colletionResponse) {
        CollectionTransformer<OrderItem> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
            if (collectionTransformer.status) {
            if (colletionResponse.isNext()) {
                myRewardsRecycleViewAdapter.addNewData(collectionTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                myRewardsRecycleViewAdapter.setNewData(collectionTransformer.data);
            }
        }
        placeHolderCON.setVisibility(myRewardsRecycleViewAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        myRewardsRV.setVisibility(myRewardsRecycleViewAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onItemClick(OrderItem orderItem) {
        rewardsActivity.openOrderdetailFragment(orderItem.id);
    }

    @Override
    public void onItemLongClick(OrderItem orderItem) {

    }
}
