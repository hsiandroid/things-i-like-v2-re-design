package com.thingsilikeapp.android.fragment.messages;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MessageActivity;
import com.thingsilikeapp.data.model.ChatModel;
import com.thingsilikeapp.server.request.Chat;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ChatChangeNameFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = ChatChangeNameFragment.class.getName();

    public static ChatChangeNameFragment newInstance(int chatID) {
        ChatChangeNameFragment fragment = new ChatChangeNameFragment();
        fragment.chat_id = chatID;
        return fragment;
    }

    private MessageActivity chatActivity;

    @BindView(R.id.mainTitleTXT)                TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)            ImageView mainBackButtonIV;
    @BindView(R.id.infoBTN)                     ImageView infoBTN;
    @BindView(R.id.groupNameET)                 EditText groupNameET;
    @BindView(R.id.clearBTN)                    ImageView cleaeBTN;
    @BindView(R.id.saveBTN)                     TextView saveBTN;

    @State int chat_id;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_chat_name;
    }

    @Override
    public void onViewReady() {
        chatActivity = (MessageActivity) getContext();
        cleaeBTN.setOnClickListener(this);
        saveBTN.setOnClickListener(this);
        mainTitleTXT.setText("Group name");
//        ("chatID: " + String.valueOf(chat_id));
        Chat.getDefault().detail(getContext(), chat_id);
        infoBTN.setVisibility(View.GONE);
        mainBackButtonIV.setOnClickListener(this);
        Analytics.trackEvent("messages_ChatChangeNameFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Subscribe
    public void onResponse(Chat.UpdateChatResponse responseData) {
        SingleTransformer<ChatModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            groupNameET.setText("");
            chatActivity.openEditFragment(chat_id);
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Chat.ChatDetailResponse responseData) {
        SingleTransformer<ChatModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            groupNameET.setText(singleTransformer.data.title);
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.clearBTN:
                groupNameET.getText().clear();
                Analytics.trackEvent("messages_ChatChangeNameFragment_clearBTN");
                break;
            case R.id.saveBTN:
                Chat.getDefault().editChatName(getContext(), groupNameET.getText().toString(), chat_id);
                Analytics.trackEvent("messages_ChatChangeNameFragment_saveBTN");
                break;
            case R.id.mainBackButtonIV:
                chatActivity.onBackPressed();
                Analytics.trackEvent("messages_ChatChangeNameFragment_mainBackButtonIV");
                break;
        }
    }
}
