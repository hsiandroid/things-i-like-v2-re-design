package com.thingsilikeapp.android.fragment.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.CategoryOwnedRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.SocialFeedRecyclerViewAdapter;
import com.thingsilikeapp.android.dialog.UnfollowDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Share;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.like.LikeRequest;
import com.thingsilikeapp.server.request.report.ReportRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.wishlist.BlockRequest;
import com.thingsilikeapp.server.request.wishlist.CategoryOwnedRequest;
import com.thingsilikeapp.server.request.wishlist.UserWishListRequest;
import com.thingsilikeapp.server.transformer.report.ReportTransformer;
import com.thingsilikeapp.server.transformer.social.FeedTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.server.transformer.wishlist.CategoryTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.CustomStaggeredGridLayoutManager;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.decoration.SpacesItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import icepick.State;

public class ILikeFragment extends BaseFragment implements
        SocialFeedRecyclerViewAdapter.ClickListener,
        EndlessRecyclerViewScrollListener.Callback,CategoryOwnedRecycleViewAdapter.ClickListener {

    public static final String TAG = ILikeFragment.class.getName();
    private SocialFeedRecyclerViewAdapter socialFeedRecyclerViewAdapter;
    private CategoryOwnedRecycleViewAdapter categoryOwnedRecycleViewAdapter;
    private UserWishListRequest userWishListRequest;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private CustomStaggeredGridLayoutManager staggeredGridLayoutManager;
    private LinearLayoutManager linearLayoutManager;

    private Timer timer;
    private TimerTask delayedThreadStartTask;
    private CategoryOwnedRequest categoryRequest;

    @BindView(R.id.observableRecyclerView) RecyclerView observableRecyclerView;
    @BindView(R.id.categoryRV)             RecyclerView categoryRV;
    @BindView(R.id.placeHolderCON)         View placeHolderCON;
    @BindView(R.id.loadingIV)              View loadingIV;

    @State  int userID;
    @State  boolean canScroll;

    public static ILikeFragment newInstance(int userID) {
        ILikeFragment iLikeFragment = new ILikeFragment();
        iLikeFragment.userID = userID;
        return iLikeFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_i_like;
    }

    @Override
    public void onViewReady() {
        setupListView();
        initFeedAPI();
        setUpRView();
        initSuggestionAPI();
        Analytics.trackEvent("profile_ILikeFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void setupListView() {
        socialFeedRecyclerViewAdapter = new SocialFeedRecyclerViewAdapter(getContext());
        socialFeedRecyclerViewAdapter.setEnableUserClick(false);
        socialFeedRecyclerViewAdapter.setFeedView(SocialFeedRecyclerViewAdapter.GRID);
        staggeredGridLayoutManager = new CustomStaggeredGridLayoutManager(2, OrientationHelper.VERTICAL);
        staggeredGridLayoutManager.setScrollEnabled(true);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(staggeredGridLayoutManager, this);
        observableRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        observableRecyclerView.setAdapter(socialFeedRecyclerViewAdapter);
        observableRecyclerView.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.social_feed_spacing)));
        socialFeedRecyclerViewAdapter.setOnItemClickListener(this);
        observableRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

    }

    public void setScrollable(boolean scrollable) {
        if (staggeredGridLayoutManager != null) {
            staggeredGridLayoutManager.setScrollEnabled(scrollable);
        }
    }

    @Override
    public void onItemClick(WishListItem wishListItem) {
        if (wishListItem.owner.data != null) {
            ((RouteActivity) getContext()).startItemActivity(wishListItem.id, wishListItem.owner.data.id, "i_like");
        }
    }

    @Override
    public void onCommentClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startCommentActivity(wishListItem.id, wishListItem.title);
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        ((RouteActivity) getContext()).startProfileActivity(userItem.id);
    }

    @Override
    public void onGrabClick(WishListItem wishListItem) {
        String data = new Gson().toJson(wishListItem);
        ((RouteActivity) getContext()).startWishListActivity(data, "create_grab_other");
    }

    @Override
    public void onHideClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to hide?")
                .setNote("You will no longer see this post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Hide")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BlockRequest blockRequest = new BlockRequest(getContext());
                        blockRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("profile_ILikeFragment_onHideClick");
    }

    @Override
    public void onUnfollowClick(final WishListItem wishListItem) {
        UnfollowDialog.newInstance(wishListItem.owner.data.id, wishListItem.owner.data.name, new UnfollowDialog.Callback() {
            @Override
            public void onAccept(int userID) {
                UnFollowRequest unFollowRequest = new UnFollowRequest(getContext());
                unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                        .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                        .addParameters(Keys.server.key.USER_ID, wishListItem.owner.data.id)
                        .execute();
            }

            @Override
            public void onCancel(int userID) {

            }
        }).show(((BaseActivity) getContext()).getSupportFragmentManager(), UnfollowDialog.TAG);
        Analytics.trackEvent("profile_ILikeFragment_onUnfollowClick");
    }

    @Override
    public void onReportClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to report?")
                .setNote("You are about to report a post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Report")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportRequest reportRequest = new ReportRequest(getContext());
                        reportRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.TYPE, "wishlist")
                                .addParameters(Keys.server.key.REFERENCE_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("profile_ILikeFragment_onReportClick");
    }

    @Override
    public void onShareClick(WishListItem wishListItem) {
        shareTheApp(wishListItem);
    }

    public void shareTheApp(WishListItem wishListItem) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, wishListItem.owner.data.username + " posted " + wishListItem.title + " on things I like for " + wishListItem.category + ". Check out what everyone likes and discover your modern wishlist app");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Share.URL + "/share/i/" + wishListItem.id);
        startActivity(Intent.createChooser(sharingIntent, "Share Things I like"));
        Analytics.trackEvent("profile_ILikeFragment_shareTheApp");
    }

    @Override
    public void onHeartClick(WishListItem wishListItem) {
        LikeRequest likeRequest = new LikeRequest(getContext());
        likeRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")`
                .addParameters(Keys.server.key.INCLUDE, "image,owner.social")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                .execute();
    }

    @Override
    public void onRepostClick(WishListItem wishListItem) {

    }

    @Override
    public void onRepostUsersClick(WishListItem wishListItem) {

    }

    @Override
    public void onHeaderClick(EventItem eventItem) {

    }

    @Override
    public void onHeaderShowClick(EventItem eventItem) {

    }

    private void initFeedAPI() {
        userWishListRequest = new UserWishListRequest(getContext());
        userWishListRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                .addParameters(Keys.server.key.INCLUDE, "image,owner.social")
                .addParameters(Keys.server.key.USER_ID, userID)
                .setPerPage(20);
    }

    public void refreshList() {
        System.gc();
        if (userWishListRequest != null) {
            userWishListRequest
                    .first();
            categoryRequest.first();
        }

        loadingIV.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        if (timer != null) {
            timer.cancel();
        }
        super.onStop();
    }

    @Subscribe
    public void onResponse(UserWishListRequest.ServerResponse responseData) {
        FeedTransformer feedTransformer = responseData.getData(FeedTransformer.class);
        if (feedTransformer.status) {

            userWishListRequest.setHasMorePage(feedTransformer.has_morepages);
            if (responseData.isNext()) {
                socialFeedRecyclerViewAdapter.addNewData(feedTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                socialFeedRecyclerViewAdapter.setNewData(feedTransformer.data);
            }

            if (userID == UserData.getUserId()) {
                FeedTransformer cache = new FeedTransformer();
                cache.data = socialFeedRecyclerViewAdapter.getData();
                UserData.insert(UserData.MY_FEED_CACHE, new Gson().toJson(cache));
            }
        }
        loadingIV.setVisibility(View.GONE);
        observableRecyclerView.setVisibility(socialFeedRecyclerViewAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE);
        placeHolderCON.setVisibility(socialFeedRecyclerViewAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            socialFeedRecyclerViewAdapter.unFollow(userTransformer.userItem.id);
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(BlockRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            socialFeedRecyclerViewAdapter.removeItem(wishListInfoTransformer.wishListItem);
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(LikeRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            socialFeedRecyclerViewAdapter.updateData(wishListInfoTransformer.wishListItem);
        }
    }

    @Subscribe
    public void onResponse(ReportRequest.ServerResponse responseData) {
        ReportTransformer reportTransformer = responseData.getData(ReportTransformer.class);
        if (reportTransformer.status) {
            if (reportTransformer.data.is_removed) {
                socialFeedRecyclerViewAdapter.removeItemByID(reportTransformer.data.reference_id);
            }
            ToastMessage.show(getContext(), reportTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), reportTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        timer = new Timer();
        delayedThreadStartTask = new TimerTask() {
            @Override
            public void run() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (userWishListRequest.hasMorePage()) {
                                    userWishListRequest.nextPage();
                                }
                            }
                        });
                    }
                }).start();
            }
        };

        timer.schedule(delayedThreadStartTask, 8000);
    }

    private void setUpRView(){
        categoryOwnedRecycleViewAdapter = new CategoryOwnedRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        categoryRV.setLayoutManager(linearLayoutManager);
//        categoryOwnedRecycleViewAdapter.setNewData(getDefaultData());
        categoryRV.setAdapter(categoryOwnedRecycleViewAdapter);
        categoryOwnedRecycleViewAdapter.setOnItemClickListener(this);
    }

    private void initSuggestionAPI(){
        categoryRequest = new CategoryOwnedRequest(getContext());
        categoryRequest
                .addParameters(Keys.server.key.USER_ID, userID)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(10);
    }


    @Subscribe
    public void onResponse(CategoryOwnedRequest.ServerResponse responseData) {
        CategoryTransformer categoryTransformer = responseData.getData(CategoryTransformer.class);
        if (categoryTransformer.status) {
            if (responseData.isNext()) {
                categoryOwnedRecycleViewAdapter.addNewData(categoryTransformer.data);
            }else{
                CategoryItem rewardCategoryItem = new CategoryItem();
                rewardCategoryItem.id = 0;
                rewardCategoryItem.title = "All";
                rewardCategoryItem.image = "https://www.catster.com/wp-content/uploads/2017/08/A-fluffy-cat-looking-funny-surprised-or-concerned.jpg";
//                rewardCategoryItem.selected = true;
                categoryTransformer.data.add(0, rewardCategoryItem);
                categoryOwnedRecycleViewAdapter.setNewData(categoryTransformer.data);
            }
        }
    }

    private List<CategoryItem> getDefaultData(){
        List<CategoryItem> androidModels = new ArrayList<>();
        CategoryItem defaultItem;
        for(int i = 0; i < 9; i++){
            defaultItem = new CategoryItem();
            defaultItem.id = i;
            defaultItem.title = "Liza Soberano " + i;
            defaultItem.image = "http://starmagic.blob.core.windows.net/albums/2016/120416-Stunning-Photos-Liza/Stunning-Liza-1.jpg";
            androidModels.add(defaultItem);
        }
        return androidModels;
    }

    @Override
    public void onItemClick(CategoryItem categoryItem) {
        if (categoryItem.title.equalsIgnoreCase("all")){
            observableRecyclerView.setVisibility(observableRecyclerView.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
           initFeedAPI();
        }else{
            observableRecyclerView.setVisibility(observableRecyclerView.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            loadingIV.setVisibility(View.VISIBLE);
            userWishListRequest = new UserWishListRequest(getContext());
            userWishListRequest
                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                    .addParameters(Keys.server.key.INCLUDE, "image,owner.social")
                    .addParameters(Keys.server.key.CATEGORY, categoryItem.title)
                    .addParameters(Keys.server.key.USER_ID, userID)
                    .setPerPage(50);
            userWishListRequest.first();
        }

    }

    @Override
    public void onItemLongClick(CategoryItem categoryItem) {

    }
}
