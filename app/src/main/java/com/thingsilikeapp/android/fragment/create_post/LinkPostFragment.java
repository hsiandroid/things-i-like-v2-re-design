package com.thingsilikeapp.android.fragment.create_post;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.CreatePostActivity;
import com.thingsilikeapp.android.dialog.SearchWebViewDialog;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;


import java.net.MalformedURLException;
import java.net.URL;

import butterknife.BindView;

public class LinkPostFragment extends BaseFragment implements View.OnClickListener, SearchWebViewDialog.Callback {

    public static final String TAG = LinkPostFragment.class.getName();

    private CreatePostActivity createPostActivity;

    @BindView(R.id.linkET)          EditText linkET;
    @BindView(R.id.searchBTN)       TextView searchBTN;
    @BindView(R.id.nextBTN)         TextView nextBTN;
    @BindView(R.id.mainBackButtonIV)	View mainBackButtonIV;

    public static LinkPostFragment newInstance() {
        LinkPostFragment fragment = new LinkPostFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_link;
    }

    @Override    public void onViewReady() {
        createPostActivity = (CreatePostActivity)getContext();
        searchBTN.setOnClickListener(this);
        nextBTN.setOnClickListener(this);
        mainBackButtonIV.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.mainBackButtonIV:
                createPostActivity.onBackPressed();
                break;
            case R.id.searchBTN:
                SearchWebViewDialog.newInstance("https://google.com", this).show(getFragmentManager(), TAG);
                break;
            case R.id.nextBTN:
                if (!linkET.getText().toString().equalsIgnoreCase("")){
                    String u = linkET.getText().toString();
                    URL url = null;
                    try {
                        url = new URL(u);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    String host = url.getHost();
                    Log.e("EEEEEEEE", ">>> " + host);

                    if (host.equalsIgnoreCase("www.instagram.com") ||
                            host.equalsIgnoreCase("www.google.com") ||
                            host.equalsIgnoreCase("www.facebook.com") ||
                            host.equalsIgnoreCase("twitter.com") ||
                            host.equalsIgnoreCase("mobile.twitter.com") ||
                            host.equalsIgnoreCase("m.facebook.com")){

                        ToastMessage.show(getContext(), "Invalid link", ToastMessage.Status.FAILED);

                    }else{
                        createPostActivity.openSendPostLink(linkET.getText().toString());
                    }


                }else{
                    ToastMessage.show(getContext(), "Please paste a link first.", ToastMessage.Status.FAILED);
                }
                break;
        }
    }

    @Override
    public void onSuccess(String url) {
        linkET.setText(url);
    }
}
