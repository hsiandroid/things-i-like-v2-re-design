package com.thingsilikeapp.android.fragment.create_post;

import android.Manifest;
import android.content.ContentResolver;
import android.content.CursorLoader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.system.ErrnoException;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.VideoView;


import com.google.android.exoplayer2.ui.PlayerView;
import com.iceteck.silicompressorr.SiliCompressor;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.CreatePostActivity;
import com.thingsilikeapp.android.adapter.AlbumAdapter;
import com.thingsilikeapp.android.adapter.GalleryAdapter;
import com.thingsilikeapp.android.adapter.GallerySIngleAdapter;
import com.thingsilikeapp.data.model.BitmapItem;
import com.thingsilikeapp.data.model.GalleryItem;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.InfiniteScrollListener;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.image.AlbumItem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;


public class GalleryPostFragment extends BaseFragment  implements View.OnClickListener,
        GalleryAdapter.ClickListener, GallerySIngleAdapter.ClickListener,CropImageView.OnSetImageUriCompleteListener,CropImageView.OnCropImageCompleteListener{


    public static final String TAG = GalleryPostFragment.class.getName();

    private CreatePostActivity createPostActivity;
    private static final int REQUEST_CAMERA = 10001;
    private static final int REQUEST_GALLERY = 10002;

    private static final int PERMISSION_CAMERA = 201;
    private static final int PERMISSION_GALLERY = 202;
    private File photoFile;

    private GalleryAdapter galleryAdapter;
    private GallerySIngleAdapter gallerySIngleAdapter;
    private AlbumAdapter albumAdapter;
    public ImageCallback imageCallback;
    public Callback callback;
    private GalleryItem galleryItem;
    private LinearLayoutManager linearLayoutManager;
    private Handler handler;

    @BindView(R.id.cropImageView)       CropImageView cropImageView;
    @BindView(R.id.cameraBTN)           View cameraBTN;
    @BindView(R.id.galleryBTN) 			View galleryBTN;
    @BindView(R.id.rotateLeftBTN) 		View rotateLeftBTN;
//    @BindView(R.id.rotateRightBTN) 		View rotateRightBTN;
    @BindView(R.id.singleBTN) 		    View singleBTN;
    @BindView(R.id.multipleBTN) 		View multipleBTN;
    @BindView(R.id.pickerBTN)			View pickerBTN;
    @BindView(R.id.imageCON)			View imageCON;
    @BindView(R.id.mainBackButtonIV)	View mainBackButtonIV;
    @BindView(R.id.placeHolderIV)		View placeHolderIV;
    @BindView(R.id.titleTXT)			TextView titleTXT;
    @BindView(R.id.galleryGV)           GridView galleryGV;
    @BindView(R.id.gallerySingleGV)     GridView gallerySingleGV;
    @BindView(R.id.albumSPR)            Spinner albumSPR;
//    @BindView(R.id.videoView)           PlayerView videoView;
    @BindView(R.id.videoView)           VideoView videoView;
    @BindView(R.id.multipleImageView)   ImageView multipleImageView;


    @State          String title;
    @State          String type = "single";
    @State          Bitmap imageBitmap;
    @State          boolean isAspectRatio = false;
    @State          String file;
    @State          String fileType = "image";

    private         String path = "";
    private         Uri newURI ;

    public Uri uri;

    public static GalleryPostFragment newInstance() {
        GalleryPostFragment fragment = new GalleryPostFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_gallery_post;
    }

    @Override
    public void onViewReady() {
        createPostActivity = (CreatePostActivity) getContext();
        pickerBTN.setOnClickListener(this);
        cameraBTN.setOnClickListener(this);
        galleryBTN.setOnClickListener(this);
        singleBTN.setOnClickListener(this);
        multipleBTN.setOnClickListener(this);
//        rotateLeftBTN.setOnClickListener(this);
        rotateLeftBTN.setOnClickListener(this);
        mainBackButtonIV.setOnClickListener(this);


        if(title == null){
            titleTXT.setText(getString(R.string.app_name));
        }else{
            titleTXT.setText(title);
        }

        if(uri != null){
            showImage(uri);
        }

        if(PermissionChecker.checkPermissions(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_GALLERY)){
            initGallery();
            initGallerySingle();
            setAlbum();
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    private void setAlbum(){
        albumAdapter = new AlbumAdapter(getContext());
        albumAdapter.setNewData(getAlbumList());
        albumSPR.setAdapter(albumAdapter);
        albumSPR.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                galleryAdapter.setNewData(getAllImagesByBucket(albumAdapter.getData(position).id));
                    if (galleryAdapter.getCount() >= 0 && albumAdapter.getCount() >= 0){
                        path = galleryAdapter.getGalleryItemByPosition(0).absolutePath;
                        newURI = Uri.fromFile(new File(galleryAdapter.getGalleryItemByPosition(0).absolutePath));
                    }
                    gallerySIngleAdapter.setNewData(getAllImagesByBuckett(albumAdapter.getData(position).id));
                    if (gallerySIngleAdapter.getCount() >= 0 && albumAdapter.getCount() >= 0){
                        path = gallerySIngleAdapter.getGalleryItemByPosition(0).absolutePath;
                        newURI = Uri.fromFile(new File(gallerySIngleAdapter.getGalleryItemByPosition(0).absolutePath));
                    }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }



    @Override
    public void onStart() {
        super.onStart();
        cropImageView.setOnSetImageUriCompleteListener(this);
        cropImageView.setOnCropImageCompleteListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        type = "single";
    }

    @Override
    public void onStop() {
        super.onStop();
        cropImageView.setOnSetImageUriCompleteListener(this);
        cropImageView.setOnCropImageCompleteListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.pickerBTN:
                    if(type.equalsIgnoreCase("single")){
                        if (path == null){
                            ToastMessage.show(getContext(),"Please select an image/video", ToastMessage.Status.FAILED);
                        }else {
                            if (fileType.equalsIgnoreCase("video")) {
                                createPostActivity.openVideoPost(path);
                            } else {
                                Bitmap bitmap = cropImageView.getCroppedImage();
                                createPostActivity.openEditPost(bitmap);
                            }
                        }
                    }else {
                        try {
                            if (convertBitmap().size() == 0) {
                                ToastMessage.show(getContext(), "Please select aleast 1 image", ToastMessage.Status.FAILED);
                            } else {
                                    createPostActivity.openEditGalleryPost(convertBitmap());
                                    Log.e("EEEEEEEEE", "List: " + convertBitmap());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                break;
            case R.id.cameraBTN:
                break;
            case R.id.imageCON:
                break;
            case R.id.galleryBTN:
                break;
            case R.id.multipleBTN:
                gallerySingleGV.setVisibility(View.GONE);
                galleryGV.setVisibility(View.VISIBLE);
                multipleBTN.setVisibility(View.GONE);
                singleBTN.setVisibility(View.VISIBLE);
                type = "multiple";
                break;
            case R.id.singleBTN:
                gallerySingleGV.setVisibility(View.VISIBLE);
                galleryGV.setVisibility(View.GONE);
                multipleBTN.setVisibility(View.VISIBLE);
                singleBTN.setVisibility(View.GONE);
                type = "single";
                break;
            case R.id.rotateLeftBTN:
                cropImageView.rotateImage(90);
                break;
            case R.id.mainBackButtonIV:
                createPostActivity.onBackPressed();
                break;
        }
    }

    private void initGallery(){
        galleryAdapter = new GalleryAdapter(getContext());
        galleryAdapter.setOnItemClickListener(this);
        galleryAdapter.notifyDataSetChanged();
        galleryGV.setAdapter(galleryAdapter);
        galleryGV.setOnScrollListener(new InfiniteScrollListener(5) {
            @Override
            public void loadMore(int page, int totalItemsCount) {
                galleryAdapter.addPage(page);
            }
        });

    }

    private void initGallerySingle(){
        gallerySIngleAdapter = new GallerySIngleAdapter(getContext());
        gallerySIngleAdapter.setOnItemClickListener(this);
        gallerySIngleAdapter.notifyDataSetChanged();
        gallerySingleGV.setAdapter(gallerySIngleAdapter);
        gallerySingleGV.setOnScrollListener(new InfiniteScrollListener(5) {
            @Override
            public void loadMore(int page, int totalItemsCount) {
                gallerySIngleAdapter.addPage(page);
            }
        });
    }



    public List<GalleryItem> getAllImagesByBucket(String id) {

        int indexData;
        int indexName;

        List<GalleryItem> listOfAllImages = new ArrayList<>();

        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {
                MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME
                    };

        String search =
                MediaStore.Images.ImageColumns.BUCKET_ID
                + "=\"" + id + "\"";

        Cursor cursor = getContext().getContentResolver().query(uri, projection, search,
                null, MediaStore.Images.Media.DATE_ADDED + " DESC");


        indexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        indexName = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);


        GalleryItem galleryItem;


        int galleryID = 0;
        while (cursor.moveToNext()) {
            galleryID ++;
            galleryItem = new GalleryItem();
            galleryItem.id = galleryID;
            galleryItem.absolutePath = cursor.getString(indexData);
            galleryItem.name = cursor.getString(indexName);
            galleryItem.position = galleryID - 1;
            galleryItem.file = new File(cursor.getString(indexData));
            listOfAllImages.add(galleryItem);
        }

        if(listOfAllImages.size() > 0 && this.uri == null){
            showImage(Uri.fromFile(new File(listOfAllImages.get(0).absolutePath)));
        }

        return listOfAllImages;
    }



    public List<GalleryItem> getAllImagesByBuckett(String id) {

        int indexData;
        int indexName;

        List<GalleryItem> listOfAllImages = new ArrayList<>();
//        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//        Cursor cursor = getContext().getContentResolver().query(uri, projection, search,
//                null, MediaStore.Images.Media.DATE_ADDED + " DESC");
//        indexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//        indexName = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);

        String[] projection = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DATA,
                MediaStore.Files.FileColumns.DATE_ADDED,
                MediaStore.Files.FileColumns.MEDIA_TYPE,
                MediaStore.Files.FileColumns.MIME_TYPE,
                MediaStore.Files.FileColumns.TITLE
        };

//        String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
//                + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
//                + " OR "
//                + MediaStore.Files.FileColumns.MEDIA_TYPE + "="
//                + MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

        String selection =
                MediaStore.Images.ImageColumns.BUCKET_ID
                + "=\"" + id + "\"" + " OR "
                + MediaStore.Video.VideoColumns.BUCKET_ID +"=\"" + id + "\"";


        Uri queryUri = MediaStore.Files.getContentUri("external");
        CursorLoader cursorLoader = new CursorLoader(
                getActivity(),
                queryUri,
                projection,
                selection,
                null, // Selection args (none).
                MediaStore.Files.FileColumns.DATE_ADDED + " DESC");

        Cursor cursor = cursorLoader.loadInBackground();
        indexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        indexName = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.TITLE);

        GalleryItem galleryItem;


        int galleryID = 0;
        while (cursor.moveToNext()) {
            galleryID ++;
            galleryItem = new GalleryItem();
            galleryItem.id = galleryID;
            galleryItem.absolutePath = cursor.getString(indexData);
            galleryItem.name = cursor.getString(indexName);
            galleryItem.position = galleryID - 1;
            galleryItem.file = new File(cursor.getString(indexData));
            listOfAllImages.add(galleryItem);
        }

        if(listOfAllImages.size() > 0 && this.uri == null){
            showImage(Uri.fromFile(new File(listOfAllImages.get(0).absolutePath)));
        }

        return listOfAllImages;
    }


    private List<AlbumItem> getAlbumList(){

        List<AlbumItem> albumItems = new ArrayList<>();
        AlbumItem albumItem;

        String[] PROJECTION_BUCKET = {
                MediaStore.Images.ImageColumns.BUCKET_ID,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.DATA};


        String BUCKET_GROUP_BY = "1) GROUP BY 1,(2";
        String BUCKET_ORDER_BY = "MAX(datetaken) DESC";

        Uri images = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Cursor cursor = getContext().getContentResolver().query(images, PROJECTION_BUCKET, BUCKET_GROUP_BY, null, BUCKET_ORDER_BY) ;
        if (cursor != null || !cursor.equals("") || !cursor.equals(null)) {
            if (cursor.moveToFirst()) {
                do {
                    albumItem = new AlbumItem();

                    albumItem.id = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_ID));
                    albumItem.album = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME));
                    albumItem.date = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN));
                    albumItem.thumbnail = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

                    albumItems.add(albumItem);
                } while (cursor.moveToNext());
            }
        }

        return albumItems;
    }


    @Override
    public void onImageClickListener(GalleryItem galleryItem) {
        showImage(Uri.fromFile(new File(galleryItem.absolutePath)));
        Log.e("OriginalPath", ">>>" + galleryItem.absolutePath);
        Log.e("NewURI", ">>>" + newURI);
        path = galleryItem.absolutePath;
        newURI = Uri.fromFile(new File(galleryItem.absolutePath));
        uri = null;
        videoView.setVisibility(View.GONE);
        multipleImageView.setVisibility(View.GONE);
        cropImageView.setVisibility(View.VISIBLE);

    }


    public void onVideoClickListener(GalleryItem galleryItem) {

    }

    @Override
    public void onSelectedClickListener(int position) {
//        GalleryItem albumsModel = galleryAdapter.getAlbumByID(position);
//        albumsModel.is_selected = !albumsModel.is_selected;
//        galleryAdapter.updateItem(albumsModel);
        videoView.setVisibility(View.GONE);
        cropImageView.setVisibility(View.GONE);
        multipleImageView.setVisibility(View.VISIBLE);
        GalleryItem albumsModel = galleryAdapter.getAlbumByID(position);
        if(galleryAdapter.getSelectedItemCount()  <= 10){
            albumsModel.is_selected = !albumsModel.is_selected;
            galleryAdapter.updateItem(albumsModel);
        } else {
            albumsModel.is_selected = false;
            galleryAdapter.updateItem(albumsModel);
            ToastMessage.show(getContext(), "You've already reached the maximum number of photo selection", ToastMessage.Status.FAILED);
        }

    }

    @Override
    public void onSingleImageClickListener(GalleryItem galleryItem) {
        showImage(Uri.fromFile(new File(galleryItem.absolutePath)));
        Log.e("OriginalPath", ">>>" + galleryItem.absolutePath);
        Log.e("NewURI", ">>>" + newURI);
        Log.e("SINGLE LANG", ">>>" + galleryItem.absolutePath);

        path = galleryItem.absolutePath;
        newURI = Uri.fromFile(new File(galleryItem.absolutePath));

        if(galleryItem.absolutePath.contains(".mp4")) {
            videoView.setVisibility(View.VISIBLE);
            cropImageView.setVisibility(View.GONE);
            multipleImageView.setVisibility(View.GONE);
            displayVid(Uri.fromFile(new File(galleryItem.absolutePath)));
            fileType = "video";
        }
        else{
            cropImageView.setVisibility(View.VISIBLE);
            multipleImageView.setVisibility(View.GONE);
            videoView.setVisibility(View.GONE);
            fileType = "image";
        }
        uri = null;
    }


    private List<BitmapItem> convertBitmap() throws IOException {
        List<BitmapItem> bitmapItems = new ArrayList<>();
        BitmapItem defaultItem;
        for (String imagePath : galleryAdapter.getSelectedPicture()) {
            Bitmap imageBitmap = SiliCompressor.with(getContext()).getCompressBitmap(imagePath);
            defaultItem = new BitmapItem();
            defaultItem.bitmap = imageBitmap;
            defaultItem.origBitmap = imageBitmap;
            bitmapItems.add(defaultItem);
        }
        return bitmapItems;
    }



    public void displayVid(Uri galleryItem){
        final MediaController controller = MediaController.getInstance();
        controller.setMediaPlayer(videoView);
        controller.setVisibility(View.VISIBLE);
        videoView.setMediaController(controller.setMediaPlayer());
        videoView.setVideoURI(galleryItem);
        videoView.start();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);

            }

        });
    }

    /**
     * Called when a crop image view has completed loading image for cropping.<br>
     * If loading failed error parameter will contain the error.
     *
     * @param view  The crop image view that loading of image was complete.
     * @param uri   the URI of the image that was loading
     * @param error if error occurred during loading will contain the error, otherwise null.
     */
    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {

    }

    /**
     * Called when a crop image view has completed cropping image.<br>
     * Result object contains the cropped bitmap, saved cropped image uri, crop points data or
     * the error occured during cropping.
     *
     * @param view   The crop image view that cropping of image was complete.
     * @param result the crop image result data (with cropped image or error)
     */
    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {

    }


    public interface ImageCallback{
        void result(File file);
    }

    public interface Callback{
//        void onCropView(boolean crop);
    }

    public void showImage(Uri imageUri){
        boolean requirePermissions = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                isUriRequiresPermissions(imageUri)) {

            requirePermissions = true;
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
        }

        if (!requirePermissions) {
            cropImageView.setImageUriAsync(imageUri);
            multipleImageView.setImageURI(imageUri);

        }

        cropImageView.setVisibility(View.VISIBLE);
        multipleImageView.setVisibility(View.GONE);
        videoView.setVisibility(View.GONE);
        placeHolderIV.setVisibility(View.GONE);
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CAMERA) {
        }
        if (requestCode == PERMISSION_GALLERY) {
            if(createPostActivity.isAllPermissionResultGranted(grantResults)){
                initGallery();
                setAlbum();
            }else{
                createPostActivity.onBackPressed();
            }
        }
    }


}
