package com.thingsilikeapp.android.fragment.rewards.exchange;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.adapter.BankHistoryRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.EncashmentDetailDialog;
import com.thingsilikeapp.android.dialog.ExchangeToCashDialog;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.data.model.EncashHistoryItem;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class HistoryAccountFragment extends BaseFragment implements BankHistoryRecycleViewAdapter.ClickListener, EndlessRecyclerViewScrollListener.Callback, SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = HistoryAccountFragment.class.getName();

    public static HistoryAccountFragment newInstance() {
        HistoryAccountFragment fragment = new HistoryAccountFragment();
        return fragment;
    }

    private BankHistoryRecycleViewAdapter bankAccountRecycleViewAdapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.accountsRV)               RecyclerView accountsRV;
    @BindView(R.id.placeHolderCON)           TextView placeHolderCON;
    @BindView(R.id.bankSRL)                  SwipeRefreshLayout bankSRL;

    private ExchangeActivity exchangeActivity;
    private APIRequest apiRequest;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_history_bank;
    }

    @Override
    public void onViewReady() {
        exchangeActivity = (ExchangeActivity)getContext();
        exchangeActivity.setTitle("Encashment History");
        setUpRView();
        bankSRL.setOnRefreshListener(this);
        Analytics.trackEvent("rewards_exchange_HistoryAccountFragment_onViewReady");
    }

    private void setUpRView(){
        bankAccountRecycleViewAdapter = new BankHistoryRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        accountsRV.setLayoutManager(linearLayoutManager);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        accountsRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        accountsRV.setAdapter(bankAccountRecycleViewAdapter);
        bankAccountRecycleViewAdapter.setOnItemClickListener(this);
    }

    @Subscribe
    public void onResponse(Wallet.HistoryResponse colletionResponse) {
        CollectionTransformer<EncashHistoryItem> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (colletionResponse.isNext()) {
                bankAccountRecycleViewAdapter.addNewData(collectionTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                bankAccountRecycleViewAdapter.setNewData(collectionTransformer.data);
            }
            placeHolderCON.setVisibility(bankAccountRecycleViewAdapter.getData().size() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    private void refreshList(){
        apiRequest = Wallet.getDefault().history(getContext(), bankSRL);
        apiRequest.first();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(EncashHistoryItem encashHistoryItem) {
//        EncashmentDetailDialog.newInstance(encashHistoryItem).show(getFragmentManager(), TAG);
        exchangeActivity.openTransactionFragment(encashHistoryItem);
    }

    @Override
    public void onItemLongClick(EncashHistoryItem encashHistoryItem) {

    }

    @Override
    public void onEditClick(EncashHistoryItem encashHistoryItem) {

    }

    @Override
    public void onRemoveClick(EncashHistoryItem encashHistoryItem) {

    }
}
