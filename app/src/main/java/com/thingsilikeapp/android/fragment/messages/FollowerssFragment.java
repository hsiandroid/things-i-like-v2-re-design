package com.thingsilikeapp.android.fragment.messages;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SearchActivity;
import com.thingsilikeapp.android.adapter.PeopleRecycleViewAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.social.FollowersRequest;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import icepick.State;

public class FollowerssFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        SearchActivity.SearchListener,
        EndlessRecyclerViewScrollListener.Callback,
        PeopleRecycleViewAdapter.ClickListener{

    public static final String TAG = FollowerssFragment.class.getName();
    private FollowersRequest followersRequest;

    private BaseActivity baseActivity;
    private PeopleRecycleViewAdapter suggestionRecycleViewAdapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;

    private Timer timer;

    @State int userID;
    @State boolean isRequest;

    @BindView(R.id.followerRV)                  RecyclerView followerRV;
    @BindView(R.id.followerSRL)                 SwipeRefreshLayout followerSRL;
    @BindView(R.id.followersPB)                 View followersPB;
    @BindView(R.id.loadMorePB)                  View loadMorePB;
    @BindView(R.id.searchET)                    EditText searchET;
    @BindView(R.id.searchBTN)                   View searchBTN;
    @BindView(R.id.placeHolderCON)              View placeHolderCON;
    @BindView(R.id.placeholderTXT)              TextView placeholderTXT;

    public static FollowerssFragment newInstance(int userID) {
        FollowerssFragment followersFragment = new FollowerssFragment();
        followersFragment.userID = userID;
        return followersFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_followers;
    }

    @Override
    public void onViewReady() {
        baseActivity = ((BaseActivity) getContext());
        Bundle bundle =  baseActivity.getFragmentBundle();
        if(bundle != null){
            isRequest = bundle.getBoolean("is_request", false);
        }

        setupFollowersListView();
        initSuggestionAPI();
        initSearch();
        Analytics.trackEvent("messages_FolowerssFragment_onViewReady");
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void setupFollowersListView() {

        suggestionRecycleViewAdapter = new PeopleRecycleViewAdapter(getContext());
        suggestionRecycleViewAdapter.setRequest(isRequest);
        suggestionRecycleViewAdapter.setOnItemClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);

        followerRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        followerRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        followerRV.setLayoutManager(linearLayoutManager);
        followerRV.setAdapter(suggestionRecycleViewAdapter);
    }

    private void initSuggestionAPI(){

        followerSRL.setColorSchemeResources(R.color.colorPrimary);
        followerSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        followerSRL.setOnRefreshListener(this);
        suggestionRecycleViewAdapter.reset();

        followersRequest = new FollowersRequest(getContext());
        followersRequest
                .setSwipeRefreshLayout(followerSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showSwipeRefreshLayout(true)
                .setPerPage(20);
    }

    private void refreshList(){
        followersPB.setVisibility(View.GONE);
        endlessRecyclerViewScrollListener.reset();
        suggestionRecycleViewAdapter.reset();
        followersRequest
                .showSwipeRefreshLayout(true)
                .clearParameters()
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, userID)
//                .addParameters(Keys.server.key.KEYWORD, searchET.getText().toString())
                .first();
    }

    public void initSearch(){
        if(searchET != null){
            searchET.setHint("Search People...");
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (timer != null) {
                        timer.cancel();
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                   refreshList();
                                }
                            });
                        }
                    }, 300);
                }
            });
        }
    }

    @Override
    public void onCommandClick(UserItem userItem) {
        if(userItem.social.data.is_following.equals("yes")){
            suggestionRecycleViewAdapter.unFollowUser(userItem);
        }else{
            suggestionRecycleViewAdapter.followUser(userItem);
        }
    }

    @Override
    public void onUserClick(UserItem userItem) {
//        if(isRequest){
//            Intent data = new Intent();
//            data.putExtra("result", new Gson().toJson(userItem));
//            baseActivity.setResult(Activity.RESULT_OK, data);
//            baseActivity.finish();
//        }
        ((RouteActivity)getContext()).startVisitMessageActivity("my_thread", Integer.parseInt(userItem.chat_id), userItem.id, userItem.common_name);
    }

    @Override
    public void onFollowSuccessClick() {

    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(FollowersRequest.ServerResponse responseData) {
        SuggestionTransformer suggestionTransformer = responseData.getData(SuggestionTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                suggestionRecycleViewAdapter.addNewData(suggestionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                suggestionRecycleViewAdapter.setNewData(suggestionTransformer.data);
            }
        }
        followersPB.setVisibility(View.GONE);
        loadMorePB.setVisibility(View.GONE);

        if(suggestionRecycleViewAdapter.getItemCount() == 0){
            placeHolderCON.setVisibility(View.VISIBLE);
            followerRV.setVisibility(View.GONE);
        }else {
            placeHolderCON.setVisibility(View.GONE);
            followerRV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void searchTextChange(final String keyword) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                refreshList();
            }
        });
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        loadMorePB.setVisibility(followersRequest.hasMorePage() ? View.VISIBLE : View.GONE);
        followersRequest.nextPage();
    }
}
