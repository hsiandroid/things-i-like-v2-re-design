package com.thingsilikeapp.android.fragment.wishlist;

import android.app.ProgressDialog;
import android.support.v4.app.ActivityCompat;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.WishListActivity;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.android.dialog.CountryDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.CountryData;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.wishlist.WishListAddressRequest;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class EditAddressFragment extends BaseFragment implements
        View.OnClickListener,
        CountryDialog.CountryPickerListener {

    public static final String TAG = EditAddressFragment.class.getName();
    private WishListActivity wishListActivity;
    private CountryData.Country countryData;

    @BindView(R.id.nameET)              EditText nameET;
    @BindView(R.id.streetAddressET)     EditText streetAddressET;
    @BindView(R.id.cityET)              EditText cityET;
    @BindView(R.id.stateET)             EditText stateET;
    @BindView(R.id.zipET)               EditText zipET;
    @BindView(R.id.countryTXT)          TextView countryTXT;
    @BindView(R.id.contactET)           EditText contactET;
    @BindView(R.id.contactCON)          View contactCON;
    @BindView(R.id.contactCodeTXT)      TextView contactCodeTXT;
    @BindView(R.id.updateBTN)           View updateBTN;
    @BindView(R.id.noteTXT)             TextView noteTXT;
    @BindView(R.id.pickupAddressCHBX)   CheckBox pickupAddressCHBX;
    @BindView(R.id.pickUpCON)           View pickUpCON;

    @State WishListItem wishListItem;

    public static EditAddressFragment newInstance(WishListItem wishListItem) {
        EditAddressFragment editAddressFragment = new EditAddressFragment();
        editAddressFragment.wishListItem = wishListItem;
        return editAddressFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_address;
    }

    @Override
    public void onViewReady() {
        wishListActivity = (WishListActivity) getActivity();
        wishListActivity.setTitle(getString(R.string.title_edit_address));
        wishListActivity.setOnBackPressedListener(null);
        updateBTN.setOnClickListener(this);
        countryTXT.setOnClickListener(this);
        contactCodeTXT.setOnClickListener(this);

        setUpAddressInfo();
        displayData();
        setupCountryTextView();
        setUpNote();
    }

    private void setUpNote(){
        String text = getString(R.string.edit_profile_note);
        String privacy = "Privacy Policy";
        String terms = "Terms and Conditions";

        SpannableString spanString = new SpannableString(text + " " + privacy + " and " + terms);

        spanString.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(getContext(), R.color.colorPrimary)), text.length(), text.length() + privacy.length() + 1, 0);
        spanString.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(getContext(), R.color.colorPrimary)), text.length() + privacy.length() + 6, text.length() + privacy.length() + 1 + terms.length() + 5, 0);

        ClickableSpan privacyClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                wishListActivity.openUrl("https://thingsilikeapp.com/privacy-policy");
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        };

        ClickableSpan termsClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                wishListActivity.openUrl("https://thingsilikeapp.com/terms");
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        };

        spanString.setSpan(privacyClick, text.length(), text.length() + privacy.length() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanString.setSpan(termsClick, text.length() + privacy.length() + 6, text.length() + privacy.length() + 1 + terms.length() + 5, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        noteTXT.setText(spanString);
        noteTXT.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void setUpAddressInfo(){
        pickUpCON.setVisibility(UserData.getUserItem().is_verified ? View.VISIBLE : View.GONE);
        displayAddressInfo(pickupAddressCHBX.isChecked());
        pickupAddressCHBX.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                displayAddressInfo(b);
            }
        });
    }

    private void displayAddressInfo(boolean b){
        if (b){
            streetAddressET.setText("C/O Highly Succeed Inc., 28F Tower 2 Enterprise Center, Ayala-Paseo de Roxas");
            cityET.setText("Makati City");
            stateET.setText("Metro Manila");
            zipET.setText("1226");
            contactET.setText("9062708663");
            countryData = CountryData.getCountryDataByCode1("PH");
            countryTXT.setText(countryData.country);
            contactCodeTXT.setText(countryData.code1);

            streetAddressET.setEnabled(false);
            cityET.setEnabled(false);
            stateET.setEnabled(false);
            countryTXT.setEnabled(false);
            zipET.setEnabled(false);
            contactCON.setEnabled(false);
            enableDisableView(contactCON, false);
        }else {
            streetAddressET.setText(wishListItem.delivery_info.data.street_address);
            cityET.setText(wishListItem.delivery_info.data.city);
            stateET.setText(wishListItem.delivery_info.data.state);
            zipET.setText(wishListItem.delivery_info.data.zip_code);
            countryData = CountryData.getCountryDataByCode1(wishListItem.delivery_info.data.country);
            countryTXT.setText(countryData.country);
            contactET.setText(StringFormatter.replaceNull(wishListItem.delivery_info.data.contact_number).replace(countryData.code3, ""));
            contactCodeTXT.setText(countryData.code3);

            streetAddressET.setEnabled(true);
            cityET.setEnabled(true);
            stateET.setEnabled(true);
            countryTXT.setEnabled(true);
            zipET.setEnabled(true);
            contactCON.setEnabled(true);
            enableDisableView(contactCON, true);
        }
    }

    private void enableDisableView(View view, boolean enabled) {
        view.setEnabled(enabled);

        if ( view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup)view;

            for ( int idx = 0 ; idx < group.getChildCount() ; idx++ ) {
                enableDisableView(group.getChildAt(idx), enabled);
            }
        }
    }

    private void setupCountryTextView(){
        countryTXT.setInputType(InputType.TYPE_NULL);
        countryTXT.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    showCountryPicker();

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.updateBTN:
                attemptUpdate();
                break;
            case R.id.countryTXT:
                countryTXT.setError(null);
                showCountryPicker();
                break;
            case R.id.contactCodeTXT:
                showCountryPicker();
                break;
        }
    }

    public void displayData(){
        nameET.setText(wishListItem.delivery_info.data.recipient);
        contactET.setText(wishListItem.delivery_info.data.contact_number);
    }

    private void attemptUpdate(){
        Log.e("Wishlist", ">>>" + wishListItem.id);
        WishListAddressRequest wishListAddressRequest = new WishListAddressRequest(getContext());
        wishListAddressRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating post address...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner,delivery_info")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                .addParameters(Keys.server.key.RECIPIENT, nameET.getText().toString())
                .addParameters(Keys.server.key.STREET_ADDRESS, streetAddressET.getText().toString())
                .addParameters(Keys.server.key.CITY, cityET.getText().toString())
                .addParameters(Keys.server.key.STATE, stateET.getText().toString())
                .addParameters(Keys.server.key.ZIP_CODE, zipET.getText().toString())
                .addParameters(Keys.server.key.COUNTRY, countryData.code1)
                .addParameters(Keys.server.key.COUNTRY_NAME, countryData.country)
                .addParameters(Keys.server.key.CONTACT_NUMBER, contactET.getText().toString())
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onSelectCountry(CountryData.Country country, int requestCode) {
        countryData = country;
        countryTXT.setText(country.country);
        contactCodeTXT.setText(country.code3);
        contactET.requestFocus();
        contactET.setSelection(contactET.getText().length());
    }

    private void showCountryPicker(){
        CountryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
    }

    @Subscribe
    public void onResponse(WishListAddressRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if(wishListInfoTransformer.status){
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
            wishListActivity.finish();
        }else{
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
            if(wishListInfoTransformer.hasRequirements()){
                ErrorResponseManger.first(nameET, wishListInfoTransformer.requires.recipient);
                ErrorResponseManger.first(streetAddressET, wishListInfoTransformer.requires.street_address);
                ErrorResponseManger.first(cityET, wishListInfoTransformer.requires.city);
                ErrorResponseManger.first(stateET, wishListInfoTransformer.requires.state);
                ErrorResponseManger.first(zipET, wishListInfoTransformer.requires.zip_code);
                ErrorResponseManger.first(countryTXT, wishListInfoTransformer.requires.country);
                ErrorResponseManger.first(contactET, wishListInfoTransformer.requires.contact_number);
            }
        }
    }
}
