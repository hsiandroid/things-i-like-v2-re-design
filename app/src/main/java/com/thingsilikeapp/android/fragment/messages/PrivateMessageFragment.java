package com.thingsilikeapp.android.fragment.messages;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MessageActivity;
import com.thingsilikeapp.android.adapter.BankAccountRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.GroupMessageRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.MessageRecycleViewAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.ChatItem;
import com.thingsilikeapp.data.model.ChatItem;
import com.thingsilikeapp.data.model.ChatModel;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.Chat;
import com.thingsilikeapp.server.request.social.SuggestionRequest;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

public class    PrivateMessageFragment extends BaseFragment implements GroupMessageRecycleViewAdapter.ClickListener, EndlessRecyclerViewScrollListener.Callback, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener{

    public static final String TAG = PrivateMessageFragment.class.getName();

    private MessageActivity messageActivity;
    private GroupMessageRecycleViewAdapter messageRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private APIRequest apiRequest;

    private Timer timer;

    @BindView(R.id.inboxRV)         RecyclerView inboxRV;
    @BindView(R.id.messageSRL)      SwipeRefreshLayout messageSRL;
    @BindView(R.id.searchET)        EditText searchET;

    public static PrivateMessageFragment newInstance() {
        PrivateMessageFragment fragment = new PrivateMessageFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_private_message;
    }

    @Override
    public void onViewReady() {
        messageActivity = (MessageActivity) getContext();
        setUpRView();
        messageSRL.setOnRefreshListener(this);
        initSearch();
        Analytics.trackEvent("messages_PrivateMessageFragment_onViewReady");
    }

    private void setUpRView(){
        messageRecycleViewAdapter = new GroupMessageRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        inboxRV.setLayoutManager(linearLayoutManager);
        inboxRV.setAdapter(messageRecycleViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        inboxRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        messageRecycleViewAdapter.setOnItemClickListener(this);
    }

    private List<ChatItem> getDefaultData(){
        List<ChatItem> androidModels = new ArrayList<>();
        ChatItem defaultItem;
        for(int i = 0; i < 9; i++){
            defaultItem = new ChatItem();
            defaultItem.id = i;
            defaultItem.title = "name " + i;
            androidModels.add(defaultItem);
        }
        return androidModels;
    }

    @Subscribe
    public void onResponse(Chat.MyMessageResponse colletionResponse) {
        CollectionTransformer<ChatModel> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            if (colletionResponse.isNext()){
                messageRecycleViewAdapter.addNewData(collectionTransformer.data);
            }else {
                endlessRecyclerViewScrollListener.reset();
                messageRecycleViewAdapter.setNewData(collectionTransformer.data);
            }
//            groupChatLL.setVisibility(messageRecycleViewAdapter.getData().size() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    public void initSearch(){
        if(searchET != null){
            searchET.setHint("Search Messages...");
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (timer != null) {
                        timer.cancel();
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(final Editable s) {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Chat.getDefault().myMessage(getContext(), "own", messageSRL, searchET.getText().toString()).execute();
                                }
                            });
                        }
                    }, 300);
                }
            });
        }
    }



    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void refreshList(){
        apiRequest = Chat.getDefault().myMessage(getContext(), "own", messageSRL, "");
        apiRequest.first();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

       @Override
    public void onRefresh() {

    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {

    }

    @Override
    public void onItemClick(ChatModel chatItem) {
        messageActivity.openMyThreadFragment(chatItem.id, chatItem.latestMessage.data.senderUserId, chatItem.title);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.searchBTN:
//                attemptSearch(searchET.getText().toString());
                Keyboard.hideKeyboard(getActivity());
                break;
            case R.id.searchET:
                Keyboard.showKeyboard(getContext(), searchET);
                break;
        }
    }


}
