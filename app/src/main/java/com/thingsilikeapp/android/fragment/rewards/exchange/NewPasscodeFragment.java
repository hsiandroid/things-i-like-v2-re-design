package com.thingsilikeapp.android.fragment.rewards.exchange;

import android.view.View;
import android.widget.TextView;

import com.goodiebag.pinview.Pinview;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.dialog.defaultdialog.PasscodeVerificationDialog;
import com.thingsilikeapp.server.Passcode;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import butterknife.BindView;
import icepick.State;

public class NewPasscodeFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = NewPasscodeFragment.class.getName();

    public static NewPasscodeFragment newInstance(String passcode) {
        NewPasscodeFragment fragment = new NewPasscodeFragment();
        fragment.passcode = passcode;
        return fragment;
    }

    @BindView(R.id.addBTN)        TextView addBTN;
    @BindView(R.id.pinView)       Pinview pinView;

    @State String passcode;

    private ExchangeActivity exchangeActivity;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_new_passcode;
    }

    @Override
    public void onViewReady() {
        exchangeActivity = (ExchangeActivity)getContext();
        exchangeActivity.setTitle("New Wallet Passcode");
        addBTN.setOnClickListener(this);
        Analytics.trackEvent("rewards_exchange_NewPasscodeFragment_onViewReady");
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.addBTN:
                if (pinView.getValue().equalsIgnoreCase("")){
                    ToastMessage.show(getContext(), "Please input your new 4 digit passcode.", ToastMessage.Status.FAILED);
                }else{
                    exchangeActivity.openUpdatePasscodeFragment(passcode, pinView.getValue());
                }
                Analytics.trackEvent("rewards_exchange_NewPasscodeFragment_addBTN");
                break;
        }
    }
}
