package com.thingsilikeapp.android.fragment.account;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.AccountActivity;
import com.thingsilikeapp.config.App;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class ShareAppFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = ShareAppFragment.class.getName();

    private AccountActivity accountActivity;

    @BindView(R.id.descriptionTXT)          TextView descriptionTXT;
    @BindView(R.id.noteTXT)          		TextView noteTXT;
    @BindView(R.id.positiveBTN)             TextView positiveBTN;
    @BindView(R.id.negativeBTN)             TextView negativeBTN;

    public static ShareAppFragment newInstance() {
        ShareAppFragment shareAppFragment = new ShareAppFragment();
        return shareAppFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_share_app;
    }

    @Override
    public void onViewReady() {
        accountActivity = (AccountActivity) getContext();
        accountActivity.showSearchContainer(false);
        accountActivity.setTitle(getString(R.string.title_share_app));
        descriptionTXT.setText(getContext().getString(R.string.app_name));
        noteTXT.setText("Would you like to share "+ getContext().getString(R.string.app_name) +"?");

        positiveBTN.setOnClickListener(this);
        negativeBTN.setOnClickListener(this);
        Analytics.trackEvent("account_ShareAppFragment_onViewReady");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.positiveBTN:
                accountActivity.setResult(Activity.RESULT_OK);
                shareTheApp();
                Analytics.trackEvent("account_ShareAppFragment_positiveBTN");
                break;
            case R.id.negativeBTN:
//                accountActivity.setResult(Activity.RESULT_OK);

                accountActivity.finish();
                Analytics.trackEvent("account_ShareAppFragment_negativeBTN");
                break;
        }
    }

    public void shareTheApp(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "LYKA");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, App.SHARE_MESSAGE.replace("{username}", UserData.getUserItem().username));
        startActivityForResult(Intent.createChooser(sharingIntent, "Share LYKA"), 212);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 212){
            accountActivity.setResult(Activity.RESULT_OK);
            accountActivity.finish();
        }
    }
}
