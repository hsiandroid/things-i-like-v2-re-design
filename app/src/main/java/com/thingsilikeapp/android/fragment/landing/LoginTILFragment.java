package com.thingsilikeapp.android.fragment.landing;

import android.Manifest;
import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.LandingActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.auth.FacebookLoginRequest;
import com.thingsilikeapp.server.request.auth.LoginRequest;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;

import org.andengine.entity.text.Text;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class LoginTILFragment extends BaseFragment implements View.OnClickListener, LandingActivity.FBCallback{

    public static final String TAG = LoginTILFragment.class.getName();
    public LandingActivity landingActivity;
    private static final int PERMISSION_GALLERY = 103;

    @BindView(R.id.emailACTV)               EditText emailACTV;
    @BindView(R.id.passwordET)              EditText passwordET;
    @BindView(R.id.showPasswordBTN)         ImageView showPasswordBTN;
    @BindView(R.id.loginTILBTN)             TextView loginTILBTN;
    @BindView(R.id.mainBackButtonIV)        ImageView mainBackButtonIV;
    @BindView(R.id.mainTitleTXT)            TextView mainTitleTXT;

    public static LoginTILFragment newInstance(){
        LoginTILFragment loginFragment = new LoginTILFragment();
        return loginFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_login_til;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
//        forgotPasswordTV.setOnClickListener(this);
//        signUpBTN.setOnClickListener(this);
//        loginBTN.setOnClickListener(this);
//        facebookLoginBTN.setOnClickListener(this);
        loginTILBTN.setOnClickListener(this);
        mainBackButtonIV.setOnClickListener(this);
        mainTitleTXT.setText("");


        PasswordEditTextManager.addShowPassword(getContext(), passwordET, showPasswordBTN);
//        emailACTV.setText("jomar.olaybal@gmail.com");
//        passwordET.setText("asdfghjkl");

        if(PermissionChecker.checkPermissions(getContext(), Manifest.permission.READ_PHONE_STATE, PERMISSION_GALLERY)){

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.loginTILBTN:
                if(PermissionChecker.checkPermissions(getContext(), Manifest.permission.READ_PHONE_STATE, PERMISSION_GALLERY)){
                    attemptLogin();
                }
                break;
            case R.id.mainBackButtonIV:
                landingActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void attemptLogin(){
        LoginRequest loginRequest = new LoginRequest(getContext());
        loginRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Logging in...", false, false))
                .setDeviceRegID(landingActivity.getDeviceRegID())
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics,kyc")
                .addParameters(Keys.server.key.USERNAME, emailACTV.getText().toString())
                .addParameters(Keys.server.key.PASSWORD, passwordET.getText().toString())
                .execute();
//        Log.e("IMEI", ">>>" + loginRequest.getDeviceIMEI());
    }

    private void attemptFBLogin(UserItem userItem){
        FacebookLoginRequest facebookLoginRequest = new FacebookLoginRequest(getContext());
        if(userItem.email != null && !userItem.email.equals("null")){
            facebookLoginRequest.addParameters(Keys.server.key.EMAIL, userItem.email);
        }

        Log.e("Access Token", ">>>" + landingActivity.getAccessToken());
        facebookLoginRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Logging in...", false, false))
                .setDeviceRegID(landingActivity.getDeviceRegID())
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics,kyc")
                .addParameters(Keys.server.key.FB_ID, userItem.fb_id)
                .addParameters(Keys.server.key.ACCESS_TOKEN, landingActivity.getAccessToken())
                .addParameters(Keys.server.key.NAME, userItem.name)
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(FacebookLoginRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
//            generateDeepLink(userTransformer.userItem.username);
            UserData.insert(userTransformer.userItem);
            UserData.insert(UserData.AUTHORIZATION, userTransformer.token);
            UserData.insert(UserData.FIRST_LOGIN, userTransformer.first_login);
            UserData.insert(UserData.SHOW_SHARE, userTransformer.first_login);
            UserData.insert(UserData.TOKEN_EXPIRED, false);
            if (!UserData.getBoolean(UserData.WALKTHRU_DONE,false)){
                UserData.insert(UserData.WALKTHRU_DONE, false);
                UserData.insert(UserData.WALKTHRU_CURRENT, 0);
            }else{
                UserData.insert(UserData.WALKTHRU_DONE, true);
                UserData.insert(UserData.WALKTHRU_CURRENT, 10);
            }

//                UserData.insert(UserData.WALKTHRU_DONE, true);
//                UserData.insert(UserData.WALKTHRU_CURRENT, 10);

            UserData.insert(UserData.TOTAL_POST, userTransformer.userItem.statistics.data.total_wishlist);

            if(landingActivity.getImageExtra() != null){
                landingActivity.startMainActivity("home", landingActivity.getImageExtra());
            }else{
                landingActivity.startMainActivity("home");
            }

            landingActivity.facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT,FacebookCustomLoggerMessage.FACEBOOK_LOGIN);

            Log.e("user_id" , ">>>" + userTransformer.userItem.id);
            Log.e("token" , ">>>" + userTransformer.token);
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.FAILED);
            if(userTransformer.hasRequirements()){

            }
        }
    }

    @Subscribe
    public void onResponse(LoginRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
            UserData.insert(userTransformer.userItem);
            UserData.insert(UserData.AUTHORIZATION, userTransformer.token);
            UserData.insert(UserData.FIRST_LOGIN, userTransformer.first_login);
            UserData.insert(UserData.SHOW_SHARE, userTransformer.first_login);
            Log.e("Done", ">>>" + UserData.getBoolean(UserData.WALKTHRU_DONE,true));
            if (!UserData.getBoolean(UserData.WALKTHRU_DONE,false)){
                UserData.insert(UserData.WALKTHRU_DONE, false);
                UserData.insert(UserData.WALKTHRU_CURRENT, 0);
            }else{
                UserData.insert(UserData.WALKTHRU_DONE, true);
                UserData.insert(UserData.WALKTHRU_CURRENT, 10);
            }

//            UserData.insert(UserData.WALKTHRU_DONE, true);
//            UserData.insert(UserData.WALKTHRU_CURRENT, 10);

            UserData.insert(UserData.REWARD_FRAGMENT, userTransformer.userItem.reward_fragment  + "");
            UserData.insert(UserData.REWARD_CRYSTAL, userTransformer.userItem.reward_crystal + "");
            UserData.insert(UserData.LYKAGEM_DISPLAY, userTransformer.userItem.lykagem_display);
            UserData.insert(UserData.TOTAL_POST, userTransformer.userItem.statistics.data.total_wishlist);

            if(landingActivity.getImageExtra() != null){
                landingActivity.startMainActivity("home", landingActivity.getImageExtra());
            }else{
                landingActivity.startMainActivity("home");
            }

            Log.e("Crystals" , ">>>" + userTransformer.reward_crystal);
            Log.e("Fragments" , ">>>" + userTransformer.reward_fragment);
            Log.e("Display Crystal" , ">>>" + userTransformer.userItem.lykagem_display);
            landingActivity.facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT,FacebookCustomLoggerMessage.EMAIL_LOGIN);
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.FAILED);
            if(userTransformer.hasRequirements()){

            }
        }
    }


    @Override
    public void success(UserItem userItem) {
        attemptFBLogin(userItem);
    }

    @Override
    public void failed() {
        ToastMessage.show(getActivity(), "Facebook login failed.", ToastMessage.Status.FAILED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
