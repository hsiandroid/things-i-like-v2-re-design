package com.thingsilikeapp.android.fragment.tutorial;

import android.widget.ImageView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class Tutorial1Fragment extends BaseFragment {

    public static final String TAG = Tutorial1Fragment.class.getName();

    @BindView(R.id.tutorial1IV)           ImageView tutorial1IV;

    public static Tutorial1Fragment newInstance() {
        Tutorial1Fragment tutorial1Fragment = new Tutorial1Fragment();
        return tutorial1Fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_tutorial_1;
    }

    @Override
    public void onViewReady() {
    }
}
