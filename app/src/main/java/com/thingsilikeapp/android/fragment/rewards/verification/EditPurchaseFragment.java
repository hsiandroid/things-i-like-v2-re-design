package com.thingsilikeapp.android.fragment.rewards.verification;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.adapter.AddressPickerRecycleViewAdapter;
import com.thingsilikeapp.android.fragment.rewards.AddressBookCreateFragment;
import com.thingsilikeapp.data.model.AddressBookItem;
import com.thingsilikeapp.data.model.OrderItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Order;
import com.thingsilikeapp.server.request.address.AllAddressRequest;
import com.thingsilikeapp.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class EditPurchaseFragment extends BaseFragment implements AddressPickerRecycleViewAdapter.ClickListener, AddressBookCreateFragment.Callback, View.OnClickListener {

    public static final String TAG = EditPurchaseFragment.class.getName();

    private RewardsActivity rewardsActivity;
    private OrderItem orderItem;
    public RequestListener requestListener;
    private AddressPickerRecycleViewAdapter addressPickerRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private AllAddressRequest allAddressRequest;

    @BindView(R.id.itemNameTXT)         TextView itemNameTXT;
    @BindView(R.id.itemName2TXT)        TextView itemName2TXT;
    @BindView(R.id.addressBTN)          TextView addressBTN;
    @BindView(R.id.confirmBuyBTN)       TextView confirmBuyBTN;
    @BindView(R.id.valueTXT)            TextView valueTXT;
    @BindView(R.id.addressRV)           RecyclerView addressRV;
    @BindView(R.id.imageIV)             ImageView imageIV;
    @BindView(R.id.ordernoteTXT)        EditText ordernoteTXT;

    public static EditPurchaseFragment newInstance(OrderItem orderItem) {
        EditPurchaseFragment fragment = new EditPurchaseFragment();
        fragment.orderItem = orderItem;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_purchase;
    }

    @Override
    public void onViewReady() {
        rewardsActivity = (RewardsActivity) getContext();
        rewardsActivity.setTitle("Confirm Purchase");
        rewardsActivity.showWallet(false);
        rewardsActivity.showOption(false);
        rewardsActivity.setTitleDark(false);
        rewardsActivity.showWallet(false);
        itemNameTXT.setText(orderItem.detail.data.get(0).title);
        itemName2TXT.setText("(1) " + orderItem.detail.data.get(0).title);
        valueTXT.setText(String.valueOf(orderItem.detail.data.get(0).price));
        ordernoteTXT.setText(orderItem.note);
        confirmBuyBTN.setOnClickListener(this);
//        requestListener = new RequestListener<String, GlideDrawable>() {
//            @Override
//            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                return false;
//            }
//
//            @Override
//            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
////               shareBTN.setClickable(true);
//                return false;
//            }
//        };

        Glide.with(getContext())
                .load(orderItem.detail.data.get(0).fullPath)
                .listener(requestListener)
                .apply(new RequestOptions()
                .fitCenter()
                .placeholder(R.color.light_gray)
                .error(R.color.light_gray))
                .into(imageIV);

        initAPICall();
        setUpAddressBookListView();
            addressBTN.setOnClickListener(this);
        Analytics.trackEvent("rewards_verification_EditPurchaseFragment_onViewReady");
    }

    private void setUpAddressBookListView(){
        addressPickerRecycleViewAdapter = new AddressPickerRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        addressRV.setLayoutManager(linearLayoutManager);
        addressRV.setAdapter(addressPickerRecycleViewAdapter);
        addressPickerRecycleViewAdapter.setOnItemClickListener(this);
    }


    private void initAPICall(){
        allAddressRequest = new AllAddressRequest(getContext());
        allAddressRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(10)
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
    @Override
    public void onResume() {
        super.onResume();
        allAddressRequest.first();
    }

    @Override
    public void onItemClick(AddressBookItem addressBookItem) {
        addressPickerRecycleViewAdapter.setItemSelected(addressBookItem.id);
        Log.e("FFFFFFF", ">>>>>" + addressPickerRecycleViewAdapter.getSelectedID());
    }

    @Subscribe
    public void onResponse(AllAddressRequest.ServerResponse responseData) {
        CollectionTransformer<AddressBookItem> collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            if (UserData.getUserItem().is_verified){
                AddressBookItem addressBookItem = new AddressBookItem();
                addressBookItem.id = -1;
                addressBookItem.address_label = "Pick up center";
                addressBookItem.street_address = "C/O Highly Succeed Inc., 28F Tower 2 Enterprise Center, Ayala-Paseo de Roxas";
                addressBookItem.phone_number = "09062708663";
                addressBookItem.state = "Metro Manila";
                addressBookItem.city = "Makati";
                addressBookItem.country = "Philippines";
                addressBookItem.zip_code = "1226";
                addressBookItem.is_default = "no";
                addressBookItem.selected = false;
                collectionTransformer.data.add(0, addressBookItem);
            }
            if(responseData.isNext()){
                addressPickerRecycleViewAdapter.addNewData(collectionTransformer.data);
            }else{
                addressPickerRecycleViewAdapter.setNewData(collectionTransformer.data);
            }
            addressPickerRecycleViewAdapter.setDefaultSelected();
        }

        if (addressPickerRecycleViewAdapter.getItemCount() == 0){
//			((RouteActivity) getContext()).startAddressBookActivity("", "create");
        }
    }

    @Subscribe
    public void onResponse(Order.OrderResponse responseData) {
        SingleTransformer<OrderItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            rewardsActivity.openOrderdetailFragment(singleTransformer.data.id);
        } else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onSuccess() {
        allAddressRequest.first();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.addressBTN:
                rewardsActivity.openAddAddressFragment(this);
                Analytics.trackEvent("rewards_verification_EditPurchaseFragment_addressBTN");
                break;
            case R.id.confirmBuyBTN:
                Order.getDefault().edit(getContext(), addressPickerRecycleViewAdapter.getSelectedID(), ordernoteTXT.getText().toString(), String.valueOf(orderItem.id));
                Analytics.trackEvent("rewards_verification_EditPurchaseFragment_confirmBuyBTN");
                break;
        }
    }
}
