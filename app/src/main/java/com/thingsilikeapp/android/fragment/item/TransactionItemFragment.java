package com.thingsilikeapp.android.fragment.item;

import android.app.ProgressDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListTransactionItem;
import com.thingsilikeapp.server.request.social.FollowRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.wishlist.TransactionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListReceivePermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListRejectGiftPermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListSendPermissionRequest;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.server.transformer.wishlist.TransactionTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransactionTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListTransactionTransformer;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.widget.ExpandableHeightListView;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;
import com.thingsilikeapp.android.activity.ItemActivity;
import com.thingsilikeapp.android.adapter.TimelineAdapter;
import com.thingsilikeapp.android.dialog.AppreciateGiftDialog;
import com.thingsilikeapp.android.dialog.GroupDialog;
import com.thingsilikeapp.android.dialog.SendGiftDialog;
import com.thingsilikeapp.android.dialog.UnfollowDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class TransactionItemFragment extends BaseFragment implements View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        UnfollowDialog.Callback, GroupDialog.Callback{

    public static final String TAG = TransactionItemFragment.class.getName();
    private ItemActivity itemActivity;
    private TransactionRequest transactionRequest;

    private TimelineAdapter timelineAdapter;

    @State int wishListTransactionID;
    @State WishListTransactionItem wishListTransactionItem;
    @State int userId;

    @State String include = "info,image,owner.info,owner.social,owner.statistics,sender.info,sender.social,sender.statistics,delivery_info,logs,tracker";

    @BindView(R.id.timelineEHLV)            ExpandableHeightListView timelineEHLV;
    @BindView(R.id.logoIV)                  ImageView logoIV;
    @BindView(R.id.iLikeIV)                 ImageView iLikeIV;
    @BindView(R.id.addBTN)                 ImageView addBTN;
    @BindView(R.id.titleTXT)                TextView titleTXT;
    @BindView(R.id.statusTXT)               TextView statusTXT;
    @BindView(R.id.dateTXT)                 TextView dateTXT;
    @BindView(R.id.senderLBL)               TextView senderLBL;
    @BindView(R.id.senderMessageLBL)        TextView senderMessageLBL;
    @BindView(R.id.contentCON)              View contentCON;
    @BindView(R.id.placeholder404CON)       View placeholder404CON;
    @BindView(R.id.itemSRL)                 MultiSwipeRefreshLayout itemSRL;

    @BindView(R.id.responseNameTXT)         TextView responseNameTXT;
    @BindView(R.id.ownerMessageLabelTXT)    TextView ownerMessageLabelTXT;
    @BindView(R.id.senderMessageTXT)        TextView senderMessageTXT;
    @BindView(R.id.ownerMessageCON)         View ownerMessageCON;

    @BindView(R.id.avatarCIV)               ImageView avatarCIV;
    @BindView(R.id.usernameTXT)             TextView usernameTXT;
    @BindView(R.id.commonNameTXT)           TextView commonNameTXT;
    @BindView(R.id.commandTXT)              TextView commandTXT;
    @BindView(R.id.commandBTN)              View commandBTN;
    @BindView(R.id.loadingPB)               View loadingPB;

    @BindView(R.id.senderMessageLabelTXT)   TextView senderMessageLabelTXT;
    @BindView(R.id.receiverMessageTXT)      TextView receiverMessageTXT;
    @BindView(R.id.giftCommandBTN)          TextView giftCommandBTN;
    @BindView(R.id.senderMessageCON)        View senderMessageCON;
    @BindView(R.id.timelineCON)             View timelineCON;
    @BindView(R.id.giftCardCON)             View giftCardCON;
    @BindView(R.id.deliveryCON)             View deliveryCON;
    @BindView(R.id.userInfoCON)             View userInfoCON;
    @BindView(R.id.hiddenAddressCON)        View hiddenAddressCON;
    @BindView(R.id.nameTXT)                 TextView nameTXT;
    @BindView(R.id.addressTXT)              TextView addressTXT;
    @BindView(R.id.contactTXT)              TextView contactTXT;
    @BindView(R.id.quickInfoTXT)            TextView quickInfoTXT;

    public static TransactionItemFragment newInstance(int id, int userId) {
        TransactionItemFragment transactionItemFragment = new TransactionItemFragment();
        transactionItemFragment.wishListTransactionID = id;
        transactionItemFragment.userId = userId;
        return transactionItemFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_transaction_item1;
    }

    @Override
    public void onViewReady() {
        itemActivity = (ItemActivity) getContext();
        itemActivity.setTitle("");
        itemActivity.getRePostTXT().setVisibility(View.GONE);

        avatarCIV.setOnClickListener(this);
        commandTXT.setOnClickListener(this);
        commandBTN.setOnClickListener(this);
        titleTXT.setOnClickListener(this);
        iLikeIV.setOnClickListener(this);
        giftCommandBTN.setOnClickListener(this);

        initSuggestionAPI();

        setupTimelineListView();

        Analytics.trackEvent("item_TransactionItemFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void setupTimelineListView(){
        timelineAdapter = new TimelineAdapter(getContext());
        timelineEHLV.setAdapter(timelineAdapter);
    }

    private void displayData(final WishListTransactionItem wishListItem){
        contentCON.setVisibility(View.VISIBLE);
        itemActivity.setTitle(wishListItem.title);
        titleTXT.setText(wishListItem.title);
        statusTXT.setText(StringFormatter.first(wishListItem.status));
        dateTXT.setText(wishListItem.time_passed);

        Glide.with(getContext()).load(R.raw.moving_cat)
                .apply(new RequestOptions()

                        .dontAnimate())
                .into(logoIV);

        Glide.with(getContext())
                .load(wishListItem.image.data.full_path)
                .apply(new RequestOptions()

                        .dontAnimate())
                .into(iLikeIV);

        titleTXT.setTag(wishListItem);
        iLikeIV.setTag(R.id.iLikeIV, wishListItem);

        if(wishListItem.role.equals("receiver")){
            quickInfoTXT.setVisibility(View.GONE);
            senderLBL.setText("Sent By");
            senderMessageLBL.setText( StringFormatter.first(StringFormatter.firstWord(wishListItem.sender.data.common_name)) + " just made your wish come true.");
            displayDataSender(wishListItem.sender.data);
        }else{
            quickInfoTXT.setVisibility(View.VISIBLE);
            senderLBL.setText("Sent To");
            senderMessageLBL.setText("You just made this wish come true for " + StringFormatter.first(StringFormatter.firstWord(wishListItem.owner.data.common_name)) +".");
            displayDataSender(wishListItem.owner.data);
        }

        if (wishListItem.role.equals("receiver")){
            giftCommandBTN.setText("Receive Gift Now");
            switch (wishListItem.status.toLowerCase()){
                case "pending":
                    giftCommandBTN.setVisibility(View.VISIBLE);
                    giftCardCON.setVisibility(View.VISIBLE);
                    break;
                case "completed":
                    hiddenAddressCON.setVisibility(View.GONE);
                    giftCommandBTN.setVisibility(View.GONE);
                    giftCardCON.setVisibility(View.VISIBLE);
                    break;
                default:
                    giftCommandBTN.setVisibility(View.GONE);
                    giftCardCON.setVisibility(View.GONE);
            }
        }else {
            giftCommandBTN.setText("Send Gift Now");
            switch (wishListItem.status.toLowerCase()){
                case "ongoing":
                    deliveryCON.setVisibility(View.VISIBLE);
                    giftCommandBTN.setVisibility(View.VISIBLE);
                    giftCardCON.setVisibility(View.GONE);
                    break;
                case "pending":
                    deliveryCON.setVisibility(View.GONE);
                    giftCommandBTN.setVisibility(View.GONE);
                    giftCardCON.setVisibility(View.VISIBLE);
                    break;
                case "completed":
                    hiddenAddressCON.setVisibility(View.GONE);
                    deliveryCON.setVisibility(View.GONE);
                    giftCommandBTN.setVisibility(View.GONE);
                    giftCardCON.setVisibility(View.VISIBLE);
                    break;
                default:
                    deliveryCON.setVisibility(View.GONE);
                    giftCommandBTN.setVisibility(View.GONE);
                    giftCardCON.setVisibility(View.GONE);
            }
        }

        giftCommandBTN.setTag(wishListItem);
        senderMessageLabelTXT.setText(wishListItem.owner.data.username + ",");
        ownerMessageLabelTXT.setText(wishListItem.sender.data.username);
        nameTXT.setText(wishListItem.owner.data.common_name);
        Log.e("Address",">>>" + wishListItem.delivery_info.data.address);
        Log.e("Status",">>>" + wishListItem.status);
        addressTXT.setText(wishListItem.delivery_info.data.address);
        contactTXT.setText(wishListItem.delivery_info.data.contact_number);

        if(wishListItem.info.data.appreciation_message != null && !wishListItem.info.data.appreciation_message.equals("null")){
            receiverMessageTXT.setText("\"" + wishListItem.info.data.appreciation_message + "." + "\"");
        }else{
            receiverMessageTXT.setText("");
        }

        if(wishListItem.info.data.dedication_message != null && !wishListItem.info.data.dedication_message.equals("null")){
            senderMessageTXT.setText("\"" + wishListItem.info.data.dedication_message + "\"");
        }else{
            senderMessageTXT.setText("");
        }

        responseNameTXT.setText(wishListItem.owner.data.name + " response");
        timelineCON.setVisibility(wishListItem.tracker.data.size() == 0 ? View.GONE : View.VISIBLE);
        timelineAdapter.setNewData(wishListItem.tracker.data);
    }

    public void displayDataSender(UserItem userItem){
        Glide.with(getContext())
                .load(userItem.getAvatar())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate())
                .into(avatarCIV);

        loadingPB.setVisibility(View.GONE);
        avatarCIV.setTag(R.id.avatarCIV,userItem.id);
        usernameTXT.setText(userItem.name);
        commonNameTXT.setText(userItem.common_name);

        if(userItem.id == UserData.getUserItem().id){
            commandBTN.setVisibility(View.GONE);
        }else{
            commandBTN.setVisibility(View.VISIBLE);
        }

        if (userItem.social.data.is_following.equals("yes")) {
            commandBTN.setBackground(ActivityCompat.getDrawable(getContext(), R.drawable.bg_follow));
            commandTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
            addBTN.setVisibility(View.GONE);
            commandTXT.setText("FOLLOWING");
        } else {
            commandBTN.setBackground(ActivityCompat.getDrawable(getContext(), R.drawable.bg_unfollow));
            commandTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
            addBTN.setVisibility(View.VISIBLE);
            commandTXT.setText("FOLLOW");
        }

        commandTXT.setTag(R.id.commandTXT, userItem);
        commandBTN.setTag(R.id.commandBTN, userItem);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.titleTXT:
                if (v.getTag() != null) {
                    WishListTransactionItem wishListTransactionItem = (WishListTransactionItem) v.getTag();
                    itemActivity.startItemActivity(wishListTransactionItem.wishlist_id, wishListTransactionItem.owner.data.id, "i_like");
                }
                Analytics.trackEvent("item_TransactionItemFragment_titleTXT");
                 break;
            case R.id.iLikeIV:
                if (v.getTag(R.id.iLikeIV) != null){
                    WishListTransactionItem wishListTransactionItem1 = (WishListTransactionItem) v.getTag(R.id.iLikeIV);
                    itemActivity.startItemActivity(wishListTransactionItem1.wishlist_id, wishListTransactionItem1.owner.data.id, "i_like");
                }
                Analytics.trackEvent("item_TransactionItemFragment_iLikeIV");
               break;
            case R.id.avatarCIV:
                if (v.getTag(R.id.avatarCIV) != null){
                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.SENDER);
                    ((RouteActivity) getContext()).startProfileActivity((int) v.getTag(R.id.avatarCIV));
                }
                Analytics.trackEvent("item_TransactionItemFragment_avatarCIV");
                break;
            case R.id.commandTXT:
                if (v.getTag(R.id.commandTXT) != null){
                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.FOLLOW);
                    loadingPB.setVisibility(View.VISIBLE);
                    commandBTN.setVisibility(View.GONE);
                    commandButtonClick((UserItem) v.getTag(R.id.commandTXT));
                }
                Analytics.trackEvent("item_TransactionItemFragment_commandTXT");
            case R.id.commandBTN:
            if (v.getTag(R.id.commandBTN) != null){
                itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.FOLLOW);
                loadingPB.setVisibility(View.VISIBLE);
                commandBTN.setVisibility(View.GONE);
                commandButtonClick((UserItem) v.getTag(R.id.commandBTN));
            }
                Analytics.trackEvent("item_TransactionItemFragment_commandBTN");
            break;
//            case R.id.buyItemCON:
//                if (v.getTag() != null) {
//                    itemActivity.facebookLogger(FacebookCustomLoggerMessage.ItemActivity.EVENT, FacebookCustomLoggerMessage.ItemActivity.BUY_ITEM);
//                    WebViewDialog.newInstance((String) v.getTag()).show(getChildFragmentManager(), WebViewDialog.TAG);
//                }
//                break;
            case R.id.giftCommandBTN:
                if (v.getTag() != null) {
                    WishListTransactionItem wishListTransactionItem2 = ((WishListTransactionItem) v.getTag());
                    if (wishListTransactionItem2.role.toLowerCase().equals("receiver")) {
                        receivedConfirmation(wishListTransactionItem2.id, wishListTransactionItem2.sender.data.id);

                    } else {
                        SendGiftDialog.newInstance("Dedication Message",
                                wishListTransactionItem2.wishlist_id,
                                new SendGiftDialog.Callback() {
                                    @Override
                                    public void onAccept(int wishlistID, String dedication) {
                                        attemptSendRequest(wishlistID, dedication);
                                    }
                                }).show(getChildFragmentManager(), SendGiftDialog.TAG);
                    }
                }
                Analytics.trackEvent("item_TransactionItemFragment_giftCommandBTN");
                break;
        }
    }

    private void attemptSendRequest(int wishListID, String dedication) {
        new WishListSendPermissionRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Sending Gift...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .addParameters(Keys.server.key.DEDICATION_MESSAGE, dedication)
                .execute();
    }

    private void receivedConfirmation(final int wishlistID, final int userID){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Have you received the actual gift?")
                .setNote("If not yet notify this person that you have not received the gift.")
                .setIcon(R.drawable.icon_information)
                .setNegativeButtonText("Not Yet")
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new WishListRejectGiftPermissionRequest(getContext())
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading...", false, false))
                                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .addParameters(Keys.server.key.INCLUDE, include)
                                .addParameters(Keys.server.key.WISHLIST_TRANSACTION_ID, wishlistID)
                                .addParameters(Keys.server.key.USER_ID, userID)
                                .execute();
                        confirmationDialog.dismiss();
                        ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.UNRECEIVED_GIFT);
                    }
                })
                .setPositiveButtonText("Yes")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppreciateGiftDialog.newInstance("Thank You Message", wishlistID, new AppreciateGiftDialog.Callback() {
                            @Override
                            public void onAccept(int wishListID, String dedication) {
                                attemptReceivedRequest(wishListID, dedication);
                            }
                        }).show(getChildFragmentManager(), AppreciateGiftDialog.TAG);
                        confirmationDialog.dismiss();
                    }
                })
                .build(getChildFragmentManager());
        Analytics.trackEvent("item_TransactionItemFragment_recieveItemDialog");
    }

    private void attemptReceivedRequest(int wishListID, String dedication){

        new WishListReceivePermissionRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Accepting Gift...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .addParameters(Keys.server.key.WISHLIST_TRANSACTION_ID, wishListID)
                .addParameters(Keys.server.key.APPRECIATION_MESSAGE, dedication)
                .execute();
    }

    private void initSuggestionAPI(){
        itemSRL.setColorSchemeResources(R.color.colorPrimary);
        itemSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        itemSRL.setSwipeableChildren(R.id.contentCON,R.id.placeholder404CON);
        itemSRL.setOnRefreshListener(this);

        transactionRequest = new TransactionRequest(getContext());
        transactionRequest
                .setSwipeRefreshLayout(itemSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .addParameters(Keys.server.key.WISHLIST_TRANSACTION_ID, wishListTransactionID)
                .addParameters(Keys.server.key.USER_ID, userId)
                .showSwipeRefreshLayout(true);
    }

    private void refreshList(){
        contentCON.setVisibility(View.GONE);
        placeholder404CON.setVisibility(View.GONE);
        transactionRequest
                .showSwipeRefreshLayout(true)
                .execute();
    }

    private void commandButtonClick(UserItem userItem) {
        if (userItem.social.data.is_following.equals("yes")) {
            unFollowUser(userItem);
        } else {
            followUser(userItem);
        }
    }

    public void followUser(UserItem userItem) {
        GroupDialog.newInstance(userItem, this).show(getFragmentManager(),GroupDialog.TAG);
    }

    @Override
    public void onGroupSelect(String groupName, int id) {
        Log.e("Group Name",">>>" + groupName);
        FollowRequest followRequest = new FollowRequest(getContext());
        followRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .addParameters(Keys.server.key.USER_ID, id)
                .addParameters(Keys.server.key.GROUP,groupName)
                .execute();
    }

    @Override
    public void onCancel(String groupName, int id) {
        loadingPB.setVisibility(View.GONE);
        commandBTN.setVisibility(View.VISIBLE);
    }

    public void unFollowUser(UserItem userItem) {
        UnfollowDialog.newInstance(userItem.id, userItem.name, this).show(getChildFragmentManager(), UnfollowDialog.TAG);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(WishListSendPermissionRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            refreshList();
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(WishListReceivePermissionRequest.ServerResponse responseData) {
        WishListInfoTransactionTransformer wishlistInfoTransformer = responseData.getData(WishListInfoTransactionTransformer.class);
        if(wishlistInfoTransformer.status){
            refreshList();
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(TransactionRequest.ServerResponse responseData) {
        TransactionTransformer transactionTransformer = responseData.getData(WishListTransactionTransformer.class);
        if(transactionTransformer.status){
            displayData(transactionTransformer.data);
            wishListTransactionItem = transactionTransformer.data;
        }

        if(responseData.getCode() == 404){
            contentCON.setVisibility(View.GONE);
            placeholder404CON.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            displayDataSender(userTransformer.userItem);
        }
    }

    @Subscribe
    public void onResponse(FollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            displayDataSender(userTransformer.userItem);
        }
    }

    @Override
    public void onAccept(int userID) {
        UnFollowRequest unFollowRequest = new UnFollowRequest(getContext());
        unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, userID)
                .execute();
    }

    @Override
    public void onCancel(int userID) {
        loadingPB.setVisibility(View.GONE);
        commandBTN.setVisibility(View.VISIBLE);
    }

}
