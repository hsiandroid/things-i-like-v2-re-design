package com.thingsilikeapp.android.fragment.notification;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.ActivityRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.NotificationOtherOptionDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.android.fragment.main.NotificationFragment;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.NotificationItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.notification.AllNotificationRequest;
import com.thingsilikeapp.server.request.notification.DeleteAllRequest;
import com.thingsilikeapp.server.request.notification.DeleteNotificationRequest;
import com.thingsilikeapp.server.request.notification.ReadAllRequest;
import com.thingsilikeapp.server.request.notification.ReadOneItemCountRequest;
import com.thingsilikeapp.server.transformer.notification.AllNotificationTransformer;
import com.thingsilikeapp.server.transformer.notification.SingleNotificationTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ActivityFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        ActivityRecycleViewAdapter.ClickListener,
        View.OnClickListener,
        EndlessRecyclerViewScrollListener.Callback,
        NotificationOtherOptionDialog.NotificationCallback{

    public static final String TAG = ActivityFragment.class.getName();
    private ActivityRecycleViewAdapter activityAdapter;
    private AllNotificationRequest allNotificationRequest;
    private NotificationFragment notificationFragment;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.activityRV)      RecyclerView activityRV;
    @BindView(R.id.activitySRL)     MultiSwipeRefreshLayout activitySRL;
    @BindView(R.id.placeHolderCON)  View placeHolderCON;
    @BindView(R.id.dataCon)         View dataCon;
    @BindView(R.id.markReadBTN)     View markReadBTN;
    @BindView(R.id.otherOptionBTN)  View otherOptionBTN;
    @BindView(R.id.cancelBTN)       View cancelBTN;
    @BindView(R.id.deleteBTN)       View deleteBTN;
    @BindView(R.id.loadingIV)       View loadingIV;
    @BindView(R.id.selectAllBTN)    TextView selectAllBTN;

    public static ActivityFragment newInstance(NotificationFragment notificationFragment) {
        ActivityFragment activityFragment = new ActivityFragment();
        activityFragment.notificationFragment = notificationFragment;
        return activityFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_activity;
    }

    @Override
    public void onViewReady() {
        setupListView();
        initNotificationAPI();
        refreshList();
        Analytics.trackEvent("notification_ActivityFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setupListView(){
        activityAdapter = new ActivityRecycleViewAdapter(getContext());
        activityAdapter.setOnItemClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);

        activityRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(getResources().getDimensionPixelSize(R.dimen.default_dimen)));
        activityRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        activityRV.setLayoutManager(linearLayoutManager);
        activityRV.setAdapter(activityAdapter);

        markReadBTN.setOnClickListener(this);
        otherOptionBTN.setOnClickListener(this);
        cancelBTN.setOnClickListener(this);
        deleteBTN.setOnClickListener(this);
        selectAllBTN.setOnClickListener(this);
    }

    private void initNotificationAPI(){

        activitySRL.setColorSchemeResources(R.color.colorPrimary);
        activitySRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        activitySRL.setSwipeableChildren(R.id.activityRV, R.id.placeHolderCON);
        activitySRL.setOnRefreshListener(this);
        activityAdapter.reset();

        allNotificationRequest = new AllNotificationRequest(getContext());
        allNotificationRequest
                .setSwipeRefreshLayout(activitySRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "user")
                .setPerPage(10);
    }

    private void refreshList(){
        endlessRecyclerViewScrollListener.reset();
        activityAdapter.reset();
        activityAdapter.setMultipleSelect(false);
        loadingIV.setVisibility(View.VISIBLE);
        showMultiSelectOption(false);

        allNotificationRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(AllNotificationRequest.ServerResponse responseData) {
        AllNotificationTransformer allNotificationTransformer = responseData.getData(AllNotificationTransformer.class);
        if(allNotificationTransformer.status){
            if(responseData.isNext()){
                activityAdapter.addNewData(allNotificationTransformer.data);
            }else{
                activityAdapter.setNewData(allNotificationTransformer.data);
            }
            showHidePlaceholder();
        }
    }

    private void showHidePlaceholder(){
        loadingIV.setVisibility(View.GONE);
        activityRV.setVisibility(View.VISIBLE);

        if (activityAdapter.getData().size() == 0){
            placeHolderCON.setVisibility(View.VISIBLE);
            dataCon.setVisibility(View.GONE);
        }else {
            placeHolderCON.setVisibility(View.GONE);
            dataCon.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onResponse(ReadOneItemCountRequest.ServerResponse responseData) {
        BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);
        if(baseTransformer.status){

        }
    }

    @Override
    public void onItemClick(int position, View v) {
        NotificationItem notificationItem =  activityAdapter.getItem(position);
        showNotification(notificationItem);
    }

    @Override
    public void onItemLongClick(int position, View v) {
//        NotificationItem notificationItem =  activityAdapter.getItem(position);
//        NotificationOtherOptionDialog.newInstance(notificationItem, this).show(getChildFragmentManager(), NotificationOtherOptionDialog.TAG);
        activityAdapter.setMultipleSelect(true);
        activityAdapter.setItemSelected(position, true);
        showMultiSelectOption(true);
    }

    private void showMultiSelectOption(boolean b){
        if(b){
            selectAllBTN.setText("Select All");
        }
        cancelBTN.setVisibility(b ? View.VISIBLE : View.GONE);
        deleteBTN.setVisibility(b ? View.VISIBLE : View.GONE);
        selectAllBTN.setVisibility(b ? View.VISIBLE : View.GONE);
        markReadBTN.setVisibility(!b ? View.VISIBLE : View.GONE);
        otherOptionBTN.setVisibility(!b ? View.VISIBLE : View.GONE);
    }

    private void readNotification(String notificationID){
        ReadOneItemCountRequest unreadCountRequest = new ReadOneItemCountRequest(getContext());
        unreadCountRequest
                .showNoInternetConnection(false)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.NOTIFICATION_ID, notificationID)
                .execute();
    }

    private void deleteNotification(String notificationID){
        DeleteNotificationRequest unreadCountRequest = new DeleteNotificationRequest(getContext());
        unreadCountRequest
                .showNoInternetConnection(false)
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Removing...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "user")
                .addParameters(Keys.server.key.SELECTED_ID, notificationID)
                .execute();
    }

    private void deleteAll(String notificationID){
        DeleteAllRequest unreadCountRequest = new DeleteAllRequest(getContext());
        unreadCountRequest
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "user")
                .addParameters(Keys.server.key.SELECTED_ID, notificationID)
                .execute();
    }

    private void showNotification(NotificationItem notificationItem){
        activityAdapter.setRead(notificationItem.id);
        readNotification(notificationItem.id);
        switch (notificationItem.type.toUpperCase()){
            case "WISHLIST_LIKE":
            case "WISHLIST":
                ((RouteActivity) getContext()).startItemActivity(notificationItem.reference_id, notificationItem.user.data.id, "i_like");
                break;
            case "WISHLIST_VIEWER":
                notificationFragment.giftActive();
                refreshList();
                break;
            case "WISHLIST_TRANSACTION":
                ((RouteActivity) getContext()).startItemActivity(notificationItem.reference_id, notificationItem.user.data.id, "i_received");
                break;
            case "USER":
                ((RouteActivity) getContext()).startProfileActivity(notificationItem.reference_id);
                break;
            case "COMMENT":
                ((RouteActivity) getContext()).startItemActivity(notificationItem.reference_id, notificationItem.user.data.id, "i_like");
//                ((RouteActivity)  getContext()).startCommentActivity(notificationItem.reference_id, "");
                break;
            case "BIRTHDAY":
                ((RouteActivity) getContext()).startBirthdaySurpriseActivity("");
                break;
            case "CHAT":
                ((RouteActivity) getContext()).startMessageActivity("message");
                break;
            case "WALLET":
                ((RouteActivity) getContext()).startRewardsActivity("treasury");
                break;
            case "PROFILE":
                ((RouteActivity) getContext()).startProfileActivity(notificationItem.reference_id);
                break;
            case "ORDER_STATUS":
//                ((RouteActivity) getContext()).startProfileActivity(notificationItem.reference_id);
                break;
        }
    }

    private void otherOption(final View v){
        PopupMenu popup = new PopupMenu(getActivity(), v);
        popup.getMenuInflater().inflate(R.menu.notification_activity_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.deleteAllBTN:
                        attemptDeleteAllRequest();
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    private void markAllUnread(){
        new ReadAllRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "user")
                .setPerPage(100)
                .execute();
    }

    private void attemptDeleteAllRequest(){
        attemptDeleteAllRequest("");
    }

    private void attemptDeleteAllRequest(final String id){

        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Notifications")
                .setNote("Are you sure you want to remove all notifications?")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Remove All")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteAll(id);
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("notification_ActivityFragment_attemptDeleteAllRequest");
    }

    private void attemptDeleteByIdRequest(final String id){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Notifications")
                .setNote("Are you sure you want to remove selected notifications?")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Remove")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteAll(id);
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("notification_ActivityFragment_attemptDeleteByIdRequest");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.markReadBTN:
                markAllUnread();
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.MARK_ALL);
                Analytics.trackEvent("notification_ActivityFragment_markReadBTN");
                break;
            case R.id.otherOptionBTN:
                otherOption(v);
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.OTHER_OPTION);
                Analytics.trackEvent("notification_ActivityFragment_otherOptionBTN");
                break;
            case R.id.cancelBTN:
                activityAdapter.setMultipleSelect(false);
                showMultiSelectOption(false);
                Analytics.trackEvent("notification_ActivityFragment_cancelBTN");
                break;
            case R.id.deleteBTN:
                removeSelected();
                Analytics.trackEvent("notification_ActivityFragment_deleteBTN");
                break;
            case R.id.selectAllBTN:
                selectAll();
                Analytics.trackEvent("notification_ActivityFragment_selectAllBTN");
                break;
        }
    }

    private void removeSelected(){
        if(activityAdapter.isSelectAll()){
            attemptDeleteAllRequest();
        }else{
            Log.e("Selected" , ">>>" + StringFormatter.implode(activityAdapter.getSelectedIds(), ","));
            attemptDeleteByIdRequest(StringFormatter.implode(activityAdapter.getSelectedIds(), ","));
        }
    }

    private void selectAll(){
        if(selectAllBTN.getText().toString().equals("Select All")){
            selectAllBTN.setText("Diselect All");
            activityAdapter.selectAll();
        }else{
            selectAllBTN.setText("Select All");
            activityAdapter.unSelectAll();
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        allNotificationRequest.nextPage();
    }

    @Override
    public void openNotification(NotificationItem notificationItem) {
        showNotification(notificationItem);
    }

    @Override
    public void hideNotification(NotificationItem notificationItem) {
        deleteNotification(notificationItem.id);
    }

    @Override
    public void turnOffNotification(NotificationItem notificationItem) {

    }

    @Subscribe
    public void onResponse(ReadAllRequest.ServerResponse responseData) {
        AllNotificationTransformer allNotificationTransformer = responseData.getData(AllNotificationTransformer.class);
        if(allNotificationTransformer.status){
            if(responseData.isNext()){
                activityAdapter.addNewData(allNotificationTransformer.data);
            }else{
                activityAdapter.setNewData(allNotificationTransformer.data);
            }
            showHidePlaceholder();
        }
    }

    @Subscribe
    public void onResponse(DeleteAllRequest.ServerResponse responseData) {
        AllNotificationTransformer allNotificationTransformer = responseData.getData(AllNotificationTransformer.class);
        if(allNotificationTransformer.status){
            activityAdapter.setMultipleSelect(false);
            showMultiSelectOption(false);
            allNotificationRequest.setPage(1);
            activityAdapter.setNewData(allNotificationTransformer.data);
            showHidePlaceholder();
        }
    }

    @Subscribe
    public void onResponse(DeleteNotificationRequest.ServerResponse responseData) {
        SingleNotificationTransformer allNotificationTransformer = responseData.getData(SingleNotificationTransformer.class);
        if(allNotificationTransformer.status){
            activityAdapter.removeData(allNotificationTransformer.data);
            showHidePlaceholder();
        }
    }
}
