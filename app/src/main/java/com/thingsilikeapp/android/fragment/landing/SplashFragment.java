package com.thingsilikeapp.android.fragment.landing;

import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.LandingActivity;
import com.thingsilikeapp.android.dialog.MaintenanceDialog;
import com.thingsilikeapp.android.dialog.VersionControlDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.auth.AppSettingsRequest;
import com.thingsilikeapp.server.request.auth.RefreshTokenRequest;
import com.thingsilikeapp.server.transformer.AppSettingsTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.firebase.MyFirebaseMessagingService;
import com.thingsilikeapp.vendor.server.base.BaseRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

public class SplashFragment extends BaseFragment implements
        VersionControlDialog.ClickListener,
        MaintenanceDialog.TimerCallback{

    public static final String TAG = SplashFragment.class.getName();

    private LandingActivity landingActivity;
    private Timer dotTimer;
    private TimerTask timerTask;

    @BindView(R.id.logoIV)          ImageView logoIV;
    @BindView(R.id.loadingTXT)      TextView loadingTXT;
    @BindView(R.id.noInternetTXT)   TextView noInternetTXT;

    public static SplashFragment newInstance() {
        SplashFragment splashFragment = new SplashFragment();
        return splashFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
        Glide.with(getContext()).load(R.raw.moving_cat).into(logoIV);
    }

    private String currentMessage;
    private int splashTime = 0;
    private int repeat = 4;
    private int repeatTime = 0;

    private void loadingText() {
        currentMessage = randomMessage();
        dotTimer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                if (splashTime < 10) {
                    setText(currentMessage + "    ");
                }else if (splashTime >= 10 && splashTime < 20) {
                    setText(currentMessage + " .  ");
                }else if (splashTime >= 20 && splashTime < 30) {
                    setText(currentMessage + " .. ");
                }else if (splashTime >= 30 && splashTime < 40) {
                    setText(currentMessage + " ...");
                    splashTime = 0;
                    repeatTime++;
                }

                if(repeatTime == repeat){
                    currentMessage = randomMessage();
                    repeatTime = 0;
                }

                splashTime++;
            }
        };
        dotTimer.schedule(timerTask, 0, 50);
    }

    private String [] messages = {
            "Knitting kitten's mat",
            "Picking blueberries",
            "Preparing picnic basket",
            "Viewing the sun set",
            "Dribbling basketball",
            "Striding the hurdles",
            "Smelling the roses",
            "Tasting the savory delicacies",
            "Sniffing the coffee aroma",
    };

    private String randomMessage() {
        return messages[randomInt(0, messages.length)];
    }

    private int randomInt(int min, int max){
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    private void setText(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(loadingTXT != null){
                    loadingTXT.setText(message);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        loadingText();
        attemptGetSettings();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (dotTimer != null) {
            dotTimer.cancel();
        }
        if (timerTask != null) {
            timerTask.cancel();
        }
    }

    private void attemptRefreshToken() {
//        if(UserData.getBoolean(UserData.TOKEN_EXPIRED)){
//            RefreshTokenRequest refreshTokenRequest = new RefreshTokenRequest(getContext());
//            refreshTokenRequest
//                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                    .addParameters(Keys.server.key.INCLUDE, "info,statistics,social")
//                    .showNoInternetConnection(false)
//                    .execute();
//        }else if(!UserData.getString(UserData.AUTHORIZATION).equals("") && UserData.getString(UserData.AUTHORIZATION) != null){
//            dynamicLink();
//        }else{
//            showLogin();
//        }

            RefreshTokenRequest refreshTokenRequest = new RefreshTokenRequest(getContext());
            refreshTokenRequest
                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                    .addParameters(Keys.server.key.INCLUDE, "info,statistics,social,kyc")
                    .showNoInternetConnection(false)
                    .execute();
    }

    private void attemptGetSettings() {
        new AppSettingsRequest(getContext())
                .showNoInternetConnection(false)
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(RefreshTokenRequest.ServerResponse responseData) {
        noInternetTXT.setVisibility(View.GONE);
        UserTransformer baseTransformer = responseData.getData(UserTransformer.class);

        if (baseTransformer.status) {
            Log.e("onResponse", ">>>" + baseTransformer.new_token);
            UserData.insert(UserData.AUTHORIZATION, baseTransformer.new_token);
            UserData.insert(baseTransformer.userItem);
            dynamicLink();
        } else {
            showLogin();
        }
    }

    private void authenticated(Uri uri){
        if (landingActivity.getTextExtra() != null) {
            landingActivity.startMainActivity("home", landingActivity.getTextExtra());
        }else if (landingActivity.getImageExtra() != null) {
            landingActivity.startMainActivity("home", landingActivity.getImageExtra());
        } else if (landingActivity.hasFCMData()) {
            landingActivity.startMainActivity("home", landingActivity.getFCMData());
        } else if (getUrl(uri) != null) {
            landingActivity.startMainActivity("home", urlToFCMData(getUrl(uri)));
        } else {
            landingActivity.startMainActivity("home");
//            UserData.insert(UserData.WALKTHRU_CURRENT, 9);
//            UserData.insert(UserData.WALKTHRU_DONE, false);
        }
        UserData.insert(UserData.TOKEN_EXPIRED, false);
        UserData.insert(UserData.FIRST_LOGIN, false);
        ToastMessage.show(getActivity(), "Welcome Back " + UserData.getUserItem().name, ToastMessage.Status.SUCCESS);
    }

    private void dynamicLink(){
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(landingActivity.getIntent())
                .addOnSuccessListener(landingActivity, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                        }
                        Log.e(TAG, "getDynamicLink:onSuccess");
                        authenticated(deepLink);
                    }
                })
                .addOnFailureListener(landingActivity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "getDynamicLink:onFailure", e);
                        authenticated(null);
                    }
                });
    }

    private URL getUrl(Uri uri){
        if(uri != null){
            try {
                URL url = new URL(uri.getScheme(), uri.getHost(), uri.getPath());
                Log.e(TAG, "getDynamicLink:" + url.getHost() + " --- "+ url.getPath());
                return url;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private MyFirebaseMessagingService.FCMData urlToFCMData(URL url){

        String path = url.getPath();
        String[] strings = path.split("/");
        int id = 0;
        String target = "";
        if(strings.length > 0){
            id = Integer.parseInt(strings[strings.length - 1]);
            target = strings[strings.length - 2];

        }
        MyFirebaseMessagingService.FCMData data = new MyFirebaseMessagingService.FCMData();
        data.referenceID = id;
        switch (target){
            case "u":
                data.type = "USER";
                break;
            case "p":
                data.type = "WISHLIST";
                break;
            default:
                data.type = "HOME";
                break;
        }
        return data;
    }

    private void showLogin(){
        landingActivity.openLoginFragment();
        UserData.insert(UserData.FIRST_OPEN, true);
    }

    @Subscribe
    public void onResponse(AppSettingsRequest.ServerResponse responseData) {
        noInternetTXT.setVisibility(View.GONE);
        AppSettingsTransformer appSettingsTransformer = responseData.getData(AppSettingsTransformer.class);
        if (appSettingsTransformer.status) {
            int appVersion = 0;
            if(appSettingsTransformer.data.latest_version.is_maintenance){
                MaintenanceDialog.newInstance(appSettingsTransformer.data.latest_version.next_update.date, appSettingsTransformer.data.latest_version.next_update.timezone, this).show(getChildFragmentManager(), MaintenanceDialog.TAG);
            }else{
                try {
                    appVersion = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionCode;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                if (appVersion < appSettingsTransformer.data.latest_version.major_version) {
                    VersionControlDialog.newInstance(true, this).show(getChildFragmentManager(), VersionControlDialog.TAG);
                } else if (appVersion < appSettingsTransformer.data.latest_version.minor_version) {
                    VersionControlDialog.newInstance(false, this).show(getChildFragmentManager(), VersionControlDialog.TAG);
                } else {
                    attemptRefreshToken();
                    Log.e("Refresh", ">>>true");
                }
            }
        }
    }

    @Subscribe
    public void onResponse(BaseRequest.OnFailedResponse onFailedResponse){
        noInternetTXT.setVisibility(View.VISIBLE);
        onFailedResponse.getRequest().execute();
    }

    @Override
    public void later(VersionControlDialog dialogFragment) {
        attemptRefreshToken();
        dialogFragment.dismiss();
    }

    @Override
    public void update(VersionControlDialog dialogFragment) {
        dialogFragment.openGooglePlayStore();
        dialogFragment.dismiss();
    }

    @Override
    public void maintenanceTimer() {
        attemptGetSettings();
    }
}
