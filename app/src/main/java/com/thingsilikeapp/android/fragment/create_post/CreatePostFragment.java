package com.thingsilikeapp.android.fragment.create_post;


import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.support.design.widget.TabLayout;
import android.util.Log;

import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraUtils;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.Facing;
import com.otaliastudios.cameraview.Flash;
import com.otaliastudios.cameraview.Gesture;
import com.otaliastudios.cameraview.GestureAction;
import com.otaliastudios.cameraview.SessionType;
import com.otaliastudios.cameraview.VideoCodec;
import com.otaliastudios.cameraview.VideoQuality;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.CreatePostActivity;
import com.thingsilikeapp.android.adapter.CreatePostPagerAdapter;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.widget.CustomV2ViewPager;


import java.io.File;

import butterknife.BindView;
import icepick.State;



/**
 * Created by Labyalo on 8/12/2017.
 */

public class CreatePostFragment extends BaseFragment{
    public static final String TAG = CreatePostFragment.class.getName();
    private CreatePostActivity createPostActivity;
    private CreatePostPagerAdapter createPostAdapter;



    @BindView(R.id.landingTL)       TabLayout landingTL;
    @BindView(R.id.landingVP)       CustomV2ViewPager landingVP;
    @BindView(R.id.cameraView)      CameraView cameraView;


    @State File file;
    Matrix matrix = new Matrix();



    public static CreatePostFragment newInstance(int item) {
        CreatePostFragment fragment = new CreatePostFragment();
        fragment.item = item;
        return fragment;
    }

    @State int item = 1;
    @State int PAGE = 2;

    @Override
    public void onViewReady() {
        createPostActivity = (CreatePostActivity)getContext();
        createPostAdapter = new CreatePostPagerAdapter(getChildFragmentManager(), createPostActivity);
                landingVP.setAdapter(createPostAdapter);
        landingVP.setCurrentItem(item);
        landingTL.setupWithViewPager(landingVP);
        setUpCamera();
        captureVideo();

    }


    public void setUpCamera(){
        createPostAdapter.callback = new CreatePostPagerAdapter.Callback() {
            @Override
            public void onCaptureSuccess(boolean capture) {
                if (capture){
                    cameraView.setSessionType(SessionType.PICTURE);
//                    cameraView.setMode(Mode.PICTURE);
                     capturePicture();
                }
            }


            @Override
            public void onFlash(boolean flash) {
                if (flash){
                    cameraView.setFlash(Flash.OFF);

                }else{
                    cameraView.setFlash(Flash.ON);
                }
            }

            @Override
            public void onCameraView(boolean facing) {
                if (facing){
                    cameraView.setFacing(Facing.BACK);
                    matrix.preScale(1.0f,-1.0f);
//                    matrix.postScale(1.0f,-1.0f);
                }else{
                    cameraView.setFacing(Facing.FRONT);
                    matrix.preScale(-1.0f, 1.0f);

                }


            }

            @Override
            public void onVideoSuccess(boolean capture) {
                if (capture){
                    cameraView.setSessionType(SessionType.VIDEO);
//                    cameraView.setMode(Mode.VIDEO);
                    cameraView.setVideoMaxDuration(29000);
                    cameraView.setVideoQuality(VideoQuality.MAX_720P);
//                    cameraView.takeVideo(null);
                    cameraView.startCapturingVideo(null);
                    cameraView.setPlaySounds(false);
                    landingVP.disableScroll(true);
                    landingTL.setEnabled(false);
                }
                else
                    {
                    cameraView.stopCapturingVideo();
//                    cameraView.stopVideo();
                    landingVP.disableScroll(false);
                    landingTL.setEnabled(true);
                }
            }

        };

    }


    @Override
    public void onResume() {
        super.onResume();
        cameraView.start();
//        cameraView.open();
    }

    public void capturePicture(){
        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] jpeg) {
                super.onPictureTaken(jpeg);
                CameraUtils.decodeBitmap(jpeg, 1000, 1000, new CameraUtils.BitmapCallback() {
                    @Override
                    public void onBitmapReady(Bitmap bitmap) {
                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                        createPostActivity.openEditPost(bitmap);
                    }
                });

            }
        });
        cameraView.capturePicture();
    }

//    public void capturePicture(){
//        cameraView.addCameraListener(new CameraListener() {
//            @Override
//            public void onPictureTaken(PictureResult result) {
//                super.onPictureTaken(result);
//                result.toBitmap(1000,1000,callback);
//
//                result.toFile(file, callback);
//
//                // Access the raw data if needed.
//                byte[] data = result.getData();
//
//            }
//        });
//        cameraView.takePicture();
//    }

    public void captureVideo(){
        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onVideoTaken(File video) {
                super.onVideoTaken(video);
                createPostActivity.openVideoPost(video.getAbsolutePath());
//                Log.e("EEEEEEEE", video.getAbsolutePath());
            }
        });
    }

//    public void captureVideo(){
//        cameraView.addCameraListener(new CameraListener() {
//            @Override
//            public void onVideoTaken(VideoResult result) {
//                super.onVideoTaken(result);
//            }
//        });
//    }

    @Override
    public void onPause() {
        super.onPause();
        if (cameraView !=null){
            cameraView.stop();
//            cameraView.close();
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (cameraView !=null){
            cameraView.destroy();

        }
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_createpost;
    }

    @Override
    public void onStart() {
        super.onStart();
    }



    @Override
    public void onStop() {
        super.onStop();
    }
}
