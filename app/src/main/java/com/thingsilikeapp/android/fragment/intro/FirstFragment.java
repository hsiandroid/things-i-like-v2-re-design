package com.thingsilikeapp.android.fragment.intro;

import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class FirstFragment extends BaseFragment {

    public static final String TAG = FirstFragment.class.getName();


    public static FirstFragment newInstance() {
        FirstFragment fragment = new FirstFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_intro_a;
    }

    @Override
    public void onViewReady() {

    }
}
