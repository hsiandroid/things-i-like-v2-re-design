package com.thingsilikeapp.android.fragment.create_post;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.iceteck.silicompressorr.SiliCompressor;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.CreatePostActivity;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.wishlist.CreateWishListRequest;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import butterknife.BindView;
import icepick.State;

public class SendPostFragmentFragment extends BaseFragment implements View.OnClickListener, CategoryDialog.Callback {

    public static final String TAG = SendPostFragmentFragment.class.getName();

    private CreatePostActivity createPostActivity;
    private CategoryItem categoryItem;

    @BindView(R.id.imageIV)         ImageView imageView;
    @BindView(R.id.titleET)         EditText titleET;
    @BindView(R.id.contentET)       EditText contentET;
    @BindView(R.id.categoryIV)      ImageView categoryIV;
    @BindView(R.id.categoryBTN)     View categoryBTN;
    @BindView(R.id.categoryTXT)     TextView categoryTXT;
    @BindView(R.id.nextBTN)         TextView nextBTN;
    @BindView(R.id.mainBackButtonIV)   View mainBackButtonIV;

    @BindView(R.id.customTXT)      EditText customTXT;
    @BindView(R.id.customView)      View customView;

    @State File file;
    @State String category = "I Just Like It";

    public static SendPostFragmentFragment newInstance(File file) {
        SendPostFragmentFragment fragment = new SendPostFragmentFragment();
        fragment.file = file;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_send_post;
    }

    @Override
    public void onViewReady() {
        createPostActivity = (CreatePostActivity)getContext();
        Picasso.with(getContext()).load(file).centerCrop().fit().into(imageView);
        categoryTXT.setOnClickListener(this);
        categoryIV.setOnClickListener(this);
        categoryBTN.setOnClickListener(this);
        categoryTXT.setText("I Just Like It");
        nextBTN.setOnClickListener(this);
        mainBackButtonIV.setOnClickListener(this);


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.categoryTXT:
            case R.id.categoryIV:
            case R.id.categoryBTN:
                categoryTXT.setError(null);
                CategoryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.CATEGORY);
                break;
            case R.id.nextBTN:
                attemptUpload();
                break;
            case R.id.mainBackButtonIV:
                createPostActivity.onBackPressed();
                break;
        }
    }

    public void attemptUpload(){

        CreateWishListRequest createWishListRequest = new CreateWishListRequest(getContext());

        createWishListRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Creating post...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addMultipartBody(Keys.server.key.TITLE, titleET.getText().toString())
//                .addMultipartBody(Keys.server.key.CATEGORY, categoryTXT.getText())
//                .addMultipartBody(Keys.server.key.CATEGORY, customTXT.getText())
                .addMultipartBody(Keys.server.key.CONTENT, contentET.getText().toString())
                .addMultipartBody(Keys.server.key.FILE, file);
//                .execute();

        if(category.equalsIgnoreCase("Others")){
            createWishListRequest
//                    .addMultipartBody(Keys.server.key.CATEGORY, categoryTXT.getText())
                    .addMultipartBody(Keys.server.key.CATEGORY, customTXT.getText())
                    .execute();
        }
        else{
            createWishListRequest
                    .addMultipartBody(Keys.server.key.CATEGORY, categoryTXT.getText())
                    .execute();
        }



    }

    @Subscribe
    public void onResponse(CreateWishListRequest.ServerResponse responseData) {
        Log.e("ResponseCode", ">>>" + responseData.getCode());
        if(responseData.getCode() == 500){
            ToastMessage.show(getActivity(), "Something went wrong. Please try again!", ToastMessage.Status.FAILED);
        }
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if(wishListInfoTransformer.status){
            int count = UserData.getInt(UserData.TOTAL_POST) + 1;
            UserData.insert(UserData.TOTAL_POST, count);
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
            createPostActivity.startMainActivity("main");

        }else{
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
            if(wishListInfoTransformer.hasRequirements()){
                ErrorResponseManger.first(titleET, wishListInfoTransformer.requires.content);
                ErrorResponseManger.first(contentET, wishListInfoTransformer.requires.content);
                ErrorResponseManger.first(categoryTXT, wishListInfoTransformer.requires.category);

            }
        }

    }

    @Override
    public void callback(String categoryItem) {
        this.category = categoryItem;
        categoryTXT.setText(categoryItem);
        if(categoryItem.equalsIgnoreCase("Others")){
            customView.setVisibility(View.VISIBLE);

        }
        else{
            categoryTXT.setText(categoryItem);
            customView.setVisibility(View.GONE);
        }
    }
}
