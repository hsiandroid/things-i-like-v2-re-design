package com.thingsilikeapp.android.fragment.tutorial;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

public class Tutorial3Fragment extends BaseFragment {

    public static final String TAG = Tutorial3Fragment.class.getName();

    public static Tutorial3Fragment newInstance() {
        Tutorial3Fragment tutorial3Fragment = new Tutorial3Fragment();
        return tutorial3Fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_tutorial_3;
    }

    @Override
    public void onViewReady() {
    }
}
