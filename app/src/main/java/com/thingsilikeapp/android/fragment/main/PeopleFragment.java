package com.thingsilikeapp.android.fragment.main;

import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.DiscoverActivity;
import com.thingsilikeapp.android.adapter.FragmentViewPagerAdapter;
import com.thingsilikeapp.android.fragment.search.users.ContactsFragment;
import com.thingsilikeapp.android.fragment.search.users.SuggestionFragment;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.SnackBar;

import butterknife.BindView;
import uk.co.deanwild.materialshowcaseview.IShowcaseListener;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class PeopleFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = PeopleFragment.class.getName();

    private DiscoverActivity mainActivity;

    private FragmentViewPagerAdapter fragmentViewPagerAdapter;
    private MaterialShowcaseView.Builder materialShowCaseView;

    private Handler handler;


    @BindView(R.id.suggestedUserBTN)                TextView suggestedUserBTN;
    @BindView(R.id.fbFriendsBTN)                    TextView fbFriendsBTN;
    @BindView(R.id.contactsBTN)                     TextView contactsBTN;
    @BindView(R.id.searchVP)                        ViewPager searchVP;
    @BindView(R.id.headerCON)                        View headerCON;

    public static PeopleFragment newInstance() {
        PeopleFragment searchUserFragment = new PeopleFragment();
        return searchUserFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_search_user;
    }

    @Override
    public void onViewReady() {
        mainActivity = (DiscoverActivity) getActivity();
//        mainActivity.setTitle(getString(R.string.people_title));
//        headerCON.setOnClickListener(this);

//        mainActivity.showBackButton(true);
//        mainActivity.setSettingsVisibility(true);
//        mainActivity.setFeedsVisibility(false);
//        mainActivity.setSearchVisibility(false);
//        mainActivity.setEventVisibility(true);
//        mainActivity.setTitleVisibility(true);

        headerCON.setOnClickListener(this);
        contactsBTN.setOnClickListener(this);
        suggestedUserBTN.setOnClickListener(this);
        fbFriendsBTN.setOnClickListener(this);
        mainActivity.setTitle("People");
        setupViewPager();

//        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)){
//            headerCON.setOnClickListener(null);
//            contactsBTN.setOnClickListener(null);
//            suggestedUserBTN.setOnClickListener(null);
//            fbFriendsBTN.setOnClickListener(null);
//            if (UserData.getInt(UserData.WALKTHRU_CURRENT) == 2) {
//                attemptGetCurrentWalkThrough();
//            }else {
//                mainActivity.attemptGetCurrentWalkThrough();
//            }
//        }else {
//            headerCON.setOnClickListener(this);
//            contactsBTN.setOnClickListener(this);
//            suggestedUserBTN.setOnClickListener(this);
//            fbFriendsBTN.setOnClickListener(this);
//        }

        headerCON.setOnClickListener(this);
        contactsBTN.setOnClickListener(this);
        suggestedUserBTN.setOnClickListener(this);
        fbFriendsBTN.setOnClickListener(this);
        Log.e("Current",">>>" + UserData.getInt(UserData.WALKTHRU_CURRENT));
        Analytics.trackEvent("main_PeopleFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
//        mainActivity.peopleActive();
//        mainActivity.attemptGetCurrentWalkThrough();
    }

    public void attemptGetCurrentWalkThrough(){
        materialShowCaseView = new MaterialShowcaseView.Builder(getActivity());
        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)){
            materialShowCaseView
                    .setTarget(headerCON)
                    .setMaskColour(ActivityCompat.getColor(getContext(), R.color.walkthrough_bg))
                    .setTargetTouchable(true)
                    .setDismissOnTargetTouch(true)
                    .setShapePadding(0)
                    .setListener(new IShowcaseListener() {
                        @Override
                        public void onShowcaseDisplayed(final MaterialShowcaseView materialShowcaseView) {
                            handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                   materialShowcaseView.removeFromWindow();
                                }
                            }, 3500);
                        }

                        @Override
                        public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {

                        }
                    })
                    .withRectangleShape()
                    .setContentTextColor(ActivityCompat.getColor(getContext(), R.color.white_dim))
                    .setContentText("Follow suggested friends or invite your friends to join from your \"Contact List.\"")
                    .show();
        }
    }

    private void setupViewPager(){
        fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getChildFragmentManager());
        fragmentViewPagerAdapter.addFragment(SuggestionFragment.newInstance());
        fragmentViewPagerAdapter.addFragment(ContactsFragment.newInstance());
        searchVP.setPageMargin(20);
        searchVP.setPageMarginDrawable(null);
        searchVP.setAdapter(fragmentViewPagerAdapter);
        searchVP.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        suggestedUserBTN.setSelected(true);
                        fbFriendsBTN.setSelected(false);
                        contactsBTN.setSelected(false);
                        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
                        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        Keyboard.hideKeyboard(getActivity());

                        break;
                    case 1:
                        suggestedUserBTN.setSelected(false);
                        fbFriendsBTN.setSelected(false);
                        contactsBTN.setSelected(true);
                        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
                        Keyboard.hideKeyboard(getActivity());
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        searchVP.setOffscreenPageLimit(2);
        suggestedUserActive();

    }

    public void suggestedUserActive(){
        suggestedUserBTN.setSelected(true);
        fbFriendsBTN.setSelected(false);
        contactsBTN.setSelected(false);
        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        searchVP.post(new Runnable() {
            @Override
            public void run() {
                if(searchVP != null){
                    searchVP.setCurrentItem(0);
                }
            }
        });
    }

    public void contactsActive(){
        suggestedUserBTN.setSelected(false);
        fbFriendsBTN.setSelected(false);
        contactsBTN.setSelected(true);
        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
        searchVP.post(new Runnable() {
            @Override
            public void run() {
                if(searchVP != null){
                    searchVP.setCurrentItem(1);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.suggestedUserBTN:
                Analytics.trackEvent("main_PeopleFragment_suggestedUserBTN");
            case R.id.headerCON:
                suggestedUserActive();
//                sampleSnackBar(suggestedUserBTN);
                Analytics.trackEvent("main_PeopleFragment_headerCON");
                break;
            case R.id.fbFriendsBTN:
                Analytics.trackEvent("main_PeopleFragment_fbFriendsBTN");
                break;
            case R.id.contactsBTN:
                contactsActive();
                Analytics.trackEvent("main_PeopleFragment_contactsBTN");
                break;
        }
    }

    public void sampleSnackBar(View view){
        SnackBar.make(view, "Success", SnackBar.SHORT);
        SnackBar.setActionTextColor(ActivityCompat.getColor(getContext(),R.color.white));
//        SnackBar.setAction("Next", new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                contactsActive();
//            }
//        });
        SnackBar.setDuration(500);
        SnackBar.addCallback(new Snackbar.Callback(){
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                contactsActive();
            }

            @Override
            public void onShown(Snackbar sb) {
                super.onShown(sb);
            }
        });
        SnackBar.show();
    }
}
