package com.thingsilikeapp.android.fragment.create_post;


import android.Manifest;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.Log;

import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Chronometer;

import com.microsoft.appcenter.analytics.Analytics;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.CreatePostActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;
import com.thingsilikeapp.vendor.android.java.ToastMessage;


import org.andengine.entity.text.Text;
import org.andengine.util.adt.color.Color;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import io.github.krtkush.lineartimer.LinearTimer;
import io.github.krtkush.lineartimer.LinearTimerView;

public class VideoPostFragment extends BaseFragment  implements View.OnClickListener,  LinearTimer.TimerListener
{
    //, View.OnTouchListener,View.OnLongClickListener

    public static final String TAG = VideoPostFragment.class.getName();

    private CreatePostActivity createPostActivity;
    public  Callback callback;
    private Timer timer;
    private TimerTask timerTask;
    private LinearTimer linearTimer;
    private Animation anim;

    private static final int PERMISSION_RECORD_AUDIO = 101;



    @BindView(R.id.titleTXT)            TextView titleTXT;
    @BindView(R.id.videoBTN)            ImageView videoBTN;
    @BindView(R.id.videoStopBTN)        ImageView videoStopBTN;
    @BindView(R.id.selfieBTN)           ImageView selfieBTN;
    @BindView(R.id.rearBTN)             ImageView rearBTN;
    @BindView(R.id.flashOnBTN)          ImageView flashOnBTN;
    @BindView(R.id.flashOffBTN)         ImageView flashOffBTN;
    @BindView(R.id.mainBackButtonIV)	View mainBackButtonIV;
    @BindView(R.id.linearTimer)         LinearTimerView linearTimerView;
    @BindView(R.id.chronometer)         Chronometer chronometer;
    @BindView(R.id.hintTXT)             TextView hintTXT;
    @BindView(R.id.nextBTN)             TextView nextBTN;


    public VideoPostFragment() {
    }

    public static VideoPostFragment newInstance() {
        VideoPostFragment fragment = new VideoPostFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_videopost;
    }

    @Override
    public void onViewReady() {
        createPostActivity = (CreatePostActivity) getContext();
        mainBackButtonIV.setOnClickListener(this);
        titleTXT.setText("Video");
        nextBTN.setText("");
        selfieBTN.setOnClickListener(this);
        rearBTN.setOnClickListener(this);
        flashOnBTN.setOnClickListener(this);
        flashOffBTN.setOnClickListener(this);
        videoBTN.setOnClickListener(this);
        videoStopBTN.setOnClickListener(this);
//        videoBTN.setOnTouchListener(this);
//        videoBTN.setOnLongClickListener(this);

        long duration = 31000;
        linearTimer = new LinearTimer.Builder()
                .linearTimerView(linearTimerView)
                .duration(duration)
                .timerListener(this)
                .getCountUpdate(LinearTimer.COUNT_DOWN_TIMER, 1000)
                .build();

        if(PermissionChecker.checkPermissions(getContext(), Manifest.permission.RECORD_AUDIO, PERMISSION_RECORD_AUDIO)){
        }
        Analytics.trackEvent("create_post_VideoPostFragment_onViewReady");

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.mainBackButtonIV:
                createPostActivity.onBackPressed();
                Analytics.trackEvent("create_post_VideoPostFragment_mainBackButton");
                break;
            case R.id.selfieBTN:
                selfieBTN.setVisibility(View.GONE);
                rearBTN.setVisibility(View.VISIBLE);
                if(callback != null){
                    callback.onCameraView(false);
                }
                Analytics.trackEvent("create_post_VideoPostFragment_selfieBTN");
                break;
            case R.id.rearBTN:
                selfieBTN.setVisibility(View.VISIBLE);
                rearBTN.setVisibility(View.GONE);
                if(callback != null){
                    callback.onCameraView(true);
                }
                Analytics.trackEvent("create_post_VideoPostFragment_rearBTN");
                break;
            case R.id.flashOnBTN:
                flashOffBTN.setVisibility(View.VISIBLE);
                flashOnBTN.setVisibility(View.GONE);
                if(callback != null){
                    callback.onFlash(true);
                }
                Analytics.trackEvent("create_post_VideoPostFragment_flashOnBTN");
                break;
            case R.id.flashOffBTN:
                flashOffBTN.setVisibility(View.GONE);
                flashOnBTN.setVisibility(View.VISIBLE);
                if(callback != null){
                    callback.onFlash(false);
                }
                Analytics.trackEvent("create_post_VideoPostFragment_flashOffBTN");
                break;
            case R.id.videoBTN:
//                Log.e("ACTION", "CLICKED");
                if(callback != null){
                    videoBTN.setVisibility(View.GONE);
                    videoStopBTN.setVisibility(View.VISIBLE);
                    callback.onVideoSuccess(true);
                    hintTXT.setText("Tap to Stop");
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    linearTimer.startTimer();
                }
                break;
            case R.id.videoStopBTN:
                videoBTN.setVisibility(View.VISIBLE);
                videoStopBTN.setVisibility(View.GONE);
                if(callback != null){
                    callback.onVideoSuccess(false);
                    startBlinkText();
                    linearTimer.pauseTimer();
                    linearTimer.resetTimer();
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.stop();
                    Analytics.trackEvent("create_post_VideoPostFragment_actionUp");
                }
                break;
        }
    }


//    @Override
//    public boolean onTouch(View view, MotionEvent motionEvent) {
//                switch(motionEvent.getAction()){
//                    case MotionEvent.ACTION_DOWN:
//                        Log.e("ACTION", "PRESSED");
//                        if(callback != null){
//                            callback.onVideoSuccess(true);
//                            stopBlinkText();
//                            chronometer.setBase(SystemClock.elapsedRealtime());
//                            chronometer.start();
//                            linearTimer.startTimer();
//                            Analytics.trackEvent("create_post_VideoPostFragment_actionDown");
//                        }
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        Log.e("ACTION", "RELEASE");
//                        if(callback != null){
//                            callback.onVideoSuccess(false);
//                            startBlinkText();
//                            linearTimer.pauseTimer();
//                            linearTimer.resetTimer();
//                            chronometer.setBase(SystemClock.elapsedRealtime());
//                            chronometer.stop();
//                            Analytics.trackEvent("create_post_VideoPostFragment_actionUp");
//                        }
//                        break;
//                }
//        return true;
//    }
//
//    @Override
//    public boolean onLongClick(View view) {
//        switch (view.getId()){
//            case R.id.videoBTN:
//                Log.e("ACTION", "LONGCLICK");
//                    videoBTN.setOnTouchListener(this);
//                    linearTimer.startTimer();
//                    Analytics.trackEvent("create_post_VideoPostFragment_videoBTNLongClick");
//                break;
//        }
//        return false;
//    }

    @Override
    public void animationComplete() {
        linearTimer.resetTimer();
    }

    @Override
    public void timerTick(long tickUpdateInMillis) {

    }

    @Override
    public void onTimerReset() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_RECORD_AUDIO) {

        }

    }
    public void startBlinkText() {
        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1000); //You can manage the time of the blink with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        hintTXT.startAnimation(anim);
    }
    public void stopBlinkText(){
        try {
            anim.cancel();
            anim.reset();
//             myText.startAnimation(anim);
        } catch (Exception e) {

        }
    }

    public interface Callback {
        void onVideoSuccess(boolean capture);
        void onFlash(boolean flash);
        void onCameraView(boolean facing);
//        void onCropView(boolean crop);

    }
}
