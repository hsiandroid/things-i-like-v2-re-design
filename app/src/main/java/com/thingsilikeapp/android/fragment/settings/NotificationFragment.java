package com.thingsilikeapp.android.fragment.settings;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;
import com.thingsilikeapp.android.adapter.NotificationSettingsRecycleViewAdapter;
import com.thingsilikeapp.data.model.NotificationSettingsItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.pusher.Sound;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class NotificationFragment extends BaseFragment implements NotificationSettingsRecycleViewAdapter.ClickListener {

    public static final String TAG = NotificationFragment.class.getName();

    private SettingsActivity settingsActivity;

    private NotificationSettingsRecycleViewAdapter notificationSettingsRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.notificationSRL)         SwipeRefreshLayout notificationSRL;
    @BindView(R.id.notificationRV)          RecyclerView notificationRV;

    public static NotificationFragment newInstance() {
        NotificationFragment fragment = new NotificationFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_notification_settings;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getContext();
        settingsActivity.setTitle("Notification Settings");
        setUpNotificationListView();
    }

    private void setUpNotificationListView(){
        notificationSettingsRecycleViewAdapter = new NotificationSettingsRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        notificationRV.setLayoutManager(linearLayoutManager);
        notificationRV.setAdapter(notificationSettingsRecycleViewAdapter);
        notificationSettingsRecycleViewAdapter.setNewData(getNotificationData());
        notificationSettingsRecycleViewAdapter.setOnItemClickListener(this);
    }


    private List<NotificationSettingsItem> getNotificationData(){
        List<NotificationSettingsItem> notificationItems = new ArrayList<>();

            NotificationSettingsItem notificationItem = new NotificationSettingsItem();
            notificationItem.id = "1";
            notificationItem.name = "Default Ringtone";
            notificationItem.code = "default";
            notificationItem.selected = notificationItem.code == UserData.getString(UserData.RINGTONE);
            notificationItems.add(notificationItem);

            notificationItem = new NotificationSettingsItem();
            notificationItem.id = "2";
            notificationItem.name = "Ringtone 1";
            notificationItem.code = "cat1";
            notificationItem.selected = notificationItem.code == UserData.getString(UserData.RINGTONE);
            notificationItems.add(notificationItem);

            notificationItem = new NotificationSettingsItem();
            notificationItem.id = "3";
            notificationItem.name = "Ringtone 2";
            notificationItem.code = "cat2";
            notificationItem.selected = notificationItem.code == UserData.getString(UserData.RINGTONE);
            notificationItems.add(notificationItem);

        return notificationItems;
    }

    @Override
    public void onItemClick(int position, View v) {
        notificationSettingsRecycleViewAdapter.setItemSelected(getNotificationData(), position, true);
        UserData.insert(UserData.RINGTONE,getNotificationData().get(position).code);
        UserData.insert(UserData.RINGTONE_NAME,getNotificationData().get(position).name);
        Sound.playNotification(getContext());
    }

    @Override
    public void onItemLongClick(int position, View v) {

    }
}
