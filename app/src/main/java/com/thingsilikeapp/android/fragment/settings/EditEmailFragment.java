package com.thingsilikeapp.android.fragment.settings;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.user.ChangeEmailRequest;
import com.thingsilikeapp.server.request.user.ChangePasswordRequest;
import com.thingsilikeapp.server.transformer.user.EditProfileTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class EditEmailFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = EditEmailFragment.class.getName();


    private SettingsActivity settingsActivity;
    private APIRequest apiRequest;

    @BindView(R.id.emailET)                     EditText emailET;
    @BindView(R.id.confirmBTN)                  View confirmBTN;

    public static EditEmailFragment newInstance() {
        EditEmailFragment changePasswordFragment = new EditEmailFragment();
        return changePasswordFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_change_email;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle("Change Email");
        confirmBTN.setOnClickListener(this);
    }

    private void attemptChangeEmail(){
//        ChangeEmailRequest changeEmailRequest = new ChangeEmailRequest(getContext());
//        changeEmailRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating Email...", false, false))
//                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.EMAIL, emailET.getText().toString())
//                .execute();
        ChangeEmailRequest.getDefault().changeEmail(getContext(),emailET.getText().toString());

    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmBTN:
                attemptChangeEmail();
                break;
        }
    }

    @Subscribe
    public void onResponse(ChangeEmailRequest.ChangeEmailResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            emailET.setText("");
            UserData.insert(singleTransformer.data);
            settingsActivity.startDiscoverActivity("setting");
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);

        }
        else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);

        }

    }
}
