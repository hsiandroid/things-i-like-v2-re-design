package com.thingsilikeapp.android.fragment.rewards.exchange;

import android.view.View;
import android.widget.TextView;

import com.goodiebag.pinview.Pinview;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.dialog.defaultdialog.PasscodeVerificationDialog;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

public class SetPasscodeFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = SetPasscodeFragment.class.getName();

    public static SetPasscodeFragment newInstance() {
        SetPasscodeFragment fragment = new SetPasscodeFragment();
        return fragment;
    }

    @BindView(R.id.addBTN)        TextView addBTN;
    @BindView(R.id.pinView)       Pinview pinView;

    private ExchangeActivity exchangeActivity;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_set_passcode;
    }

    @Override
    public void onViewReady() {
        exchangeActivity = (ExchangeActivity)getContext();
        exchangeActivity.setTitle("Set Your Wallet Passcode");
        addBTN.setOnClickListener(this);
        Analytics.trackEvent("rewards_exchange_SetPasscodeFragment_onViewReady");
    }


    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.addBTN:
                if (pinView.getValue().equalsIgnoreCase("")){
                    ToastMessage.show(getContext(), "Please input 4 digit passcode.", ToastMessage.Status.FAILED);
                }else{
                    exchangeActivity.openConfirmPasscodeFragment(pinView.getValue());
                }
                Analytics.trackEvent("rewards_exchange_SetPasscodeFragment_addBTN");
                break;
        }
    }
}
