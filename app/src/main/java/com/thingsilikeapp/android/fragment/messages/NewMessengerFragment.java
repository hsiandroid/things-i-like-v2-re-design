package com.thingsilikeapp.android.fragment.messages;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MessageActivity;
import com.thingsilikeapp.android.adapter.NewMessagesPagerAdapter;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class NewMessengerFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = NewMessengerFragment.class.getName();
    private MessageActivity messageActivity;
    private NewMessagesPagerAdapter messagesPagerAdapter;

    @BindView(R.id.landingTL)                   TabLayout landingTL;
    @BindView(R.id.landingVP)                   ViewPager landingVP;
    @BindView(R.id.mainTitleTXT)                TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)            ImageView mainBackButtonIV;
    @BindView(R.id.infoBTN)                  ImageView infoBTN;

    public static NewMessengerFragment newInstance(int item) {
        NewMessengerFragment fragment = new NewMessengerFragment();
        fragment.item = item;
        return fragment;
    }

    @State int item = 1;

    @Override
    public void onViewReady() {
        messageActivity = (MessageActivity)getContext();
        messagesPagerAdapter = new NewMessagesPagerAdapter(getChildFragmentManager(), messageActivity);
        landingVP.setAdapter(messagesPagerAdapter);
        landingVP.setCurrentItem(item);
        landingTL.setupWithViewPager(landingVP);
        mainBackButtonIV.setOnClickListener(this);
        mainTitleTXT.setText("New Message");
        infoBTN.setVisibility(View.GONE);
        Analytics.trackEvent("messages_NewMessengerFragment_onViewReady");
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_new_messages;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.mainBackButtonIV:
                messageActivity.onBackPressed();
                Analytics.trackEvent("messages_NewMessengerFragment_mainBackButtonIV");
                break;
        }
    }
}
