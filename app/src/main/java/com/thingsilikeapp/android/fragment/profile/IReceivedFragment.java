package com.thingsilikeapp.android.fragment.profile;

import android.app.ProgressDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.iGaveReceivedActivity;
import com.thingsilikeapp.android.adapter.GiftRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.AppreciateGiftDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListTransactionItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.wishlist.UserReceivedWishListRequest;
import com.thingsilikeapp.server.request.wishlist.WishListReceivePermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListRejectGiftPermissionRequest;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransactionTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListTransactionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.CustomLinearLayoutManager;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class IReceivedFragment extends BaseFragment implements
        GiftRecycleViewAdapter.ClickListener,
        EndlessRecyclerViewScrollListener.Callback,
        AppreciateGiftDialog.Callback{

    public static final String TAG = IReceivedFragment.class.getName();
    private GiftRecycleViewAdapter giftRecycleViewAdapter;
    private UserReceivedWishListRequest userReceivedWishListRequest;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private CustomLinearLayoutManager linearLayoutManager;
    private iGaveReceivedActivity iGaveReceivedActivity;

    @BindView(R.id.observableRecyclerView)      RecyclerView observableRecyclerView;
    @BindView(R.id.placeHolderCON)              View placeHolderCON;
    @BindView(R.id.loadingIV)                   View loadingIV;

    @State int userID;
    @State String include = "image,owner.info,owner.social,sender.info,sender.statistics";
//    @State String include = "info,image,owner.info,owner.social,owner.statistics,sender.info,sender.social,sender.statistics,delivery_info";

    public static IReceivedFragment newInstance(int userID) {
        IReceivedFragment iReceivedFragment = new IReceivedFragment();
        iReceivedFragment.userID = userID;
        return iReceivedFragment;
    }



    @Override
    public int onLayoutSet() {
        return R.layout.fragment_i_received;
    }

    @Override
    public void onViewReady() {
        setupListView();
        initFeedAPI();
        iGaveReceivedActivity=(iGaveReceivedActivity)getContext();
        iGaveReceivedActivity.setTitle("Received");
        Analytics.trackEvent("profile_IReceivedFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void setupListView(){
        giftRecycleViewAdapter = new GiftRecycleViewAdapter(getContext(), GiftRecycleViewAdapter.DisplayType.RECEIVED);
        linearLayoutManager = new CustomLinearLayoutManager(getContext());
        linearLayoutManager.setScrollEnabled(false);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);

        observableRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        observableRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(20));
        observableRecyclerView.setLayoutManager(linearLayoutManager);
        observableRecyclerView.setHasFixedSize(false);
        observableRecyclerView.setAdapter(giftRecycleViewAdapter);
        giftRecycleViewAdapter.setOnItemClickListener(this);
    }

    public void setScrollable(boolean scrollable){
        if(linearLayoutManager != null){
            linearLayoutManager.setScrollEnabled(scrollable);
        }
    }

    @Override
    public void onItemClick(int position, View v) {
        WishListTransactionItem wishListTransactionItem = (WishListTransactionItem) giftRecycleViewAdapter.getItemData(position);
        ((RouteActivity) getContext()).startItemActivity(wishListTransactionItem.id, wishListTransactionItem.owner.data.id, "i_received");
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        ((RouteActivity)getActivity()).startProfileActivity(userItem.id);
    }

    @Override
    public void onStatusClick(String status, int wishlistID, int userID) {
        receivedConfirmation(wishlistID, userID);
    }

    private void receivedConfirmation(final int wishlistID, final int userID){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Have you received the actual gift?")
                .setNote("If not yet notify this person that you have not received the gift.")
                .setIcon(R.drawable.icon_information)
                .setNegativeButtonText("Not Yet")
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new WishListRejectGiftPermissionRequest(getContext())
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading...", false, false))
                                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .addParameters(Keys.server.key.INCLUDE, include)
                                .addParameters(Keys.server.key.WISHLIST_TRANSACTION_ID, wishlistID)
                                .addParameters(Keys.server.key.USER_ID, userID)
                                .execute();
                        confirmationDialog.dismiss();
                        ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.UNRECEIVED_GIFT);
                    }
                })
                .setPositiveButtonText("Yes")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppreciateGiftDialog.newInstance("Thank You Message", wishlistID, IReceivedFragment.this).show(getChildFragmentManager(), AppreciateGiftDialog.TAG);
                        confirmationDialog.dismiss();
                    }
                })
                .build(getChildFragmentManager());
        Analytics.trackEvent("profile_IReceivedFragment_receivedConfirmation");
    }

    private void initFeedAPI(){
        userReceivedWishListRequest = new UserReceivedWishListRequest(getContext());
        userReceivedWishListRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .addParameters(Keys.server.key.USER_ID, userID)
                .addParameters(Keys.server.key.VIEW_ALL, "yes")
                .setPerPage(10);
    }

    public void refreshList(){
        System.gc();
        if (userReceivedWishListRequest != null) {
            userReceivedWishListRequest.first();
        }
        loadingIV.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(UserReceivedWishListRequest.ServerResponse responseData) {
        WishListTransactionTransformer wishListTransactionTransformer = responseData.getData(WishListTransactionTransformer.class);
        if(wishListTransactionTransformer.status){
            if(responseData.isNext()){
                giftRecycleViewAdapter.addNewData(wishListTransactionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                giftRecycleViewAdapter.setNewData(wishListTransactionTransformer.data);
            }
        }
        loadingIV.setVisibility(View.GONE);
        observableRecyclerView.setVisibility(giftRecycleViewAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE);
        placeHolderCON.setVisibility(giftRecycleViewAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }


    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        userReceivedWishListRequest.nextPage();
    }

    @Override
    public void onAccept(int wishListID, String dedication) {
        attemptReceivedRequest(wishListID, dedication);
    }

    private void attemptReceivedRequest(int wishListID, String dedication){
        new WishListReceivePermissionRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Accepting Gift...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .addParameters(Keys.server.key.WISHLIST_TRANSACTION_ID, wishListID)
                .addParameters(Keys.server.key.APPRECIATION_MESSAGE, dedication)
                .execute();
        Analytics.trackEvent("profile_IReceivedFragment_attemptReceivedRequest");
    }

    @Subscribe
    public void onResponse(WishListReceivePermissionRequest.ServerResponse responseData) {
        WishListInfoTransactionTransformer wishlistInfoTransformer = responseData.getData(WishListInfoTransactionTransformer.class);
        if(wishlistInfoTransformer.status){
            giftRecycleViewAdapter.updateStatus(wishlistInfoTransformer.data.id, "completed");
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}