package com.thingsilikeapp.android.fragment.rewards.historyDetails;

import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.data.model.RewardHistoryItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Settings;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class HistoryDetailsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    private RewardsActivity rewardsActivity;
    private RewardHistoryItem rewardHistoryItem;
    private RewardHistoryItem rewardHistoryItems;

    @BindView(R.id.valueTXT)                        TextView valueTXT;
    @BindView(R.id.titleTXT)                        TextView titleTXT;
    @BindView(R.id.itemTitleTXT)                    TextView itemTitleTXT;
    @BindView(R.id.descriptionTXT)                  TextView descriptionTXT;
    @BindView(R.id.historySRL)                      SwipeRefreshLayout historySRL;
    @BindView(R.id.dateTXT)                         TextView dateTXT;
    @BindView(R.id.timeTXT)                         TextView timeTXT;

    @BindView(R.id.nameTXT)                         TextView nameTXT;
    @BindView(R.id.commonNameTXT)                   TextView commonNameTXT;
    @BindView(R.id.imageCIV)                        ImageView imageCIV;

    @BindView(R.id.iLikeIV)                         ImageView iLikeIV;
    @BindView(R.id.userCON)                         View userCON;
    @BindView(R.id.itemCON)                         View itemCON;

    @BindView(R.id.refTXT)                          TextView refTXT;
    @BindView(R.id.statusTXT)                       TextView statusTXT;
    @BindView(R.id.remarksTXT)                      TextView reamarksTXT;
    @BindView(R.id.gemTXT)                          TextView gemTXT;
    @BindView(R.id.finalAmountTXT)                  TextView finalAmountTXT;
    @BindView(R.id.encashmentCON)                   View encashmentCON;

    @BindView(R.id.photosIV)                        ImageView photosIV;
    @BindView(R.id.orderTXT)                        TextView orderTXT;
    @BindView(R.id.itemNameTXT)                     TextView itemNameTXT;
    @BindView(R.id.qtyTXT)                          TextView qtyTXT;
    @BindView(R.id.statussTXT)                      TextView statussTXT;
    @BindView(R.id.orderCON)                        View orderCON;
    @BindView(R.id.itemView)                        View itemView;


    @State String itemType;

    public static HistoryDetailsFragment newInstance(RewardHistoryItem rewardHistoryItem) {
        HistoryDetailsFragment historyDetailsFragment = new HistoryDetailsFragment();
        historyDetailsFragment.rewardHistoryItem = rewardHistoryItem;
        return historyDetailsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_history_details;
    }

    @Override
    public void onViewReady() {
        rewardsActivity = (RewardsActivity) getContext();
        rewardsActivity.setTitle("TRANSACTION DETAILS");
        rewardsActivity.showWallet(false);
        rewardsActivity.showOption(false);
        rewardsActivity.setTitleDark(false);
        rewardsActivity.showWallet(false);

        switch (rewardHistoryItem.itemType){
            case "user":
                itemType = "user";
                break;
            case "item":
                itemType = "item.image,item.owner";
                break;
            case "transaction":
                itemType = "transaction";
                break;
            case "order":
                itemType = "order.detail";
                break;
            case "encashment":
                itemType = "encashment";
                break;
        }

        userCON.setOnClickListener(this);
        itemCON.setOnClickListener(this);
        orderCON.setOnClickListener(this);
        encashmentCON.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Settings.RewardHistoryResponse response) {
        SingleTransformer<RewardHistoryItem> singleTransformer = response.getData(SingleTransformer.class);
        if (singleTransformer.status) {
            displayData(singleTransformer.data);

        }else{
            ToastMessage.show(getContext(), "Something went wrong", ToastMessage.Status.FAILED);
        }

    }

    public void displayData(RewardHistoryItem rewardHistoryItem){
        valueTXT.setText(rewardHistoryItem.value);
        rewardHistoryItems = rewardHistoryItem;
        boolean isCredit = rewardHistoryItem.type.equalsIgnoreCase("credit");
        valueTXT.setTextColor(ActivityCompat.getColor(getContext(), isCredit ? R.color.earnAdd : R.color.earnLess));
        titleTXT.setText(rewardHistoryItem.title);
        descriptionTXT.setText(rewardHistoryItem.remarks);
        dateTXT.setText(rewardHistoryItem.createdAt.dateDb);
        timeTXT.setText(rewardHistoryItem.createdAt.time);
        switch (rewardHistoryItem.itemType) {
            case "user":
                userCON.setVisibility(View.VISIBLE);
                itemCON.setVisibility(View.GONE);
                encashmentCON.setVisibility(View.GONE);
                orderCON.setVisibility(View.GONE);
                nameTXT.setText(rewardHistoryItem.user.data.name);
                commonNameTXT.setText(rewardHistoryItem.user.data.commonName);
                Picasso.with(getContext()).load(rewardHistoryItem.user.data.image).placeholder(R.drawable.placeholder_avatar).into(imageCIV);
                break;
            case "item":
                userCON.setVisibility(View.GONE);
                itemCON.setVisibility(View.VISIBLE);
                orderCON.setVisibility(View.GONE);
                encashmentCON.setVisibility(View.GONE);
                itemTitleTXT.setText(rewardHistoryItem.item.data.title);
                Picasso.with(getContext()).load(rewardHistoryItem.item.data.image.data.full_path).placeholder(R.drawable.placeholder_avatar).into(iLikeIV);
                break;
            case "transaction":
                userCON.setVisibility(View.GONE);
                itemCON.setVisibility(View.GONE);
                orderCON.setVisibility(View.GONE);
                encashmentCON.setVisibility(View.GONE);
                itemView.setVisibility(View.GONE);
                break;
            case "order":
                userCON.setVisibility(View.GONE);
                itemCON.setVisibility(View.GONE);
                encashmentCON.setVisibility(View.GONE);
                orderCON.setVisibility(View.VISIBLE);
                orderTXT.setText(rewardHistoryItem.order.data.displayId);
                String upperString = rewardHistoryItem.order.data.statusDisplay.substring(0,1).toUpperCase() + rewardHistoryItem.order.data.statusDisplay.substring(1);
                String format = upperString.replace("_", " ");
                statusTXT.setText(format);
                dateTXT.setText(rewardHistoryItem.order.data.purchasedDate.date);

                if (rewardHistoryItem.order.data.detail.data != null){
                    itemNameTXT.setText(rewardHistoryItem.order.data.detail.data.get(0).title);
                    qtyTXT.setText("QTY: " + rewardHistoryItem.order.data.detail.data.get(0).itemQty);
                    Picasso.with(getContext()).load(rewardHistoryItem.order.data.detail.data.get(0).thumbPath).centerCrop().fit().into(photosIV);
                }else{
                    itemNameTXT.setText("null");
                    qtyTXT.setText("null");
                    Picasso.with(getContext()).load(R.drawable.placeholder_request).centerCrop().fit().into(photosIV);
                }

                switch (rewardHistoryItem.order.data.statusDisplay){
                    case "cancelled":
                    case "dispute":
                    case "refund":
                        statusTXT.setBackgroundResource(R.color.red);

                        break;
                    case "pending":
                    case "for_approval":
                        statusTXT.setBackgroundResource(R.color.gray);

                        break;
                    case "closed":
                        statusTXT.setBackgroundResource(R.color.gray);
                        break;
                    case "delivered":
                    case "completed":
                        statusTXT.setBackgroundResource(R.color.completed);
                        break;
                    case "in_transit":
                        statusTXT.setBackgroundResource(R.color.orange);
                        break;
                    case "for_delivery":
                        statusTXT.setBackgroundResource(R.color.colorPrimary);
                        break;
                }
                break;
            case "encashment":
                userCON.setVisibility(View.GONE);
                itemCON.setVisibility(View.GONE);
                orderCON.setVisibility(View.GONE);
                encashmentCON.setVisibility(View.VISIBLE);
                refTXT.setText("Ref.no. " +rewardHistoryItem.encashment.data.refno);
                statusTXT.setText(rewardHistoryItem.encashment.data.status);
                reamarksTXT.setText(rewardHistoryItem.encashment.data.remarks);
                dateTXT.setText(rewardHistoryItem.encashment.data.requestDate);
                gemTXT.setText(rewardHistoryItem.encashment.data.gemQty);
                finalAmountTXT.setText(rewardHistoryItem.encashment.data.currency + " " + rewardHistoryItem.encashment.data.finalAmount);

                switch (rewardHistoryItem.encashment.data.status){
                    case "close":
                        statusTXT.setBackgroundResource(R.color.gray);
                        break;
                    case "completed":
                    case "approved":
                        statusTXT.setBackgroundResource(R.color.completed);
                        break;
                    case "decline":
                    case "cancelled":
                        statusTXT.setBackgroundResource(R.color.failed);
                        break;
                    case "pending":
                        statusTXT.setBackgroundResource(R.color.gold);
                        break;
                }
                break;
        }
    }

    public void refreshList(){
        Settings.getDefault().getRewardHistory(getContext(), itemType, rewardHistoryItem.id, historySRL);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.userCON:
                if (UserData.getUserItem().id == rewardHistoryItems.user.data.id) {
                    ((RouteActivity) getContext()).startProfileActivity(rewardHistoryItems.user.data.id);
                    return;
                }
                ((RouteActivity) getContext()).startProfileActivity(rewardHistoryItems.user.data.id);
                break;
            case R.id.itemCON:
                rewardsActivity.startItemActivity(rewardHistoryItems.item.data.id, rewardHistoryItems.item.data.owner.data.id, "i_like");
                break;
            case R.id.orderCON:
                rewardsActivity.openOrderdetailFragment(rewardHistoryItems.order.data.id);
                break;
            case R.id.encashmentCON:
                rewardsActivity.startExchangeActivity("history");
                break;

        }
    }
}
