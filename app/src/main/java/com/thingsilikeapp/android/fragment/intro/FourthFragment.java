package com.thingsilikeapp.android.fragment.intro;

import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class FourthFragment extends BaseFragment {

    public static final String TAG = FourthFragment.class.getName();



    public static FourthFragment newInstance() {
        FourthFragment fragment = new FourthFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_intro_d;
    }

    @Override
    public void onViewReady() {
    }
}
