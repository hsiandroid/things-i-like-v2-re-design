package com.thingsilikeapp.android.fragment.rewards;

import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.microsoft.appcenter.analytics.Analytics;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.MyRewardsItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.reward.MyRewardShowRequest;
import com.thingsilikeapp.server.transformer.reward.MyRewardShowTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.widget.image.FixedWidthImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class MyRewardDetailFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener, View.OnClickListener{

    public static final String TAG = MyRewardDetailFragment.class.getName();

    private RewardsActivity rewardsActivity;

    @BindView(R.id.shopSRL)                     SwipeRefreshLayout shopSRL;
    @BindView(R.id.rewardCrystalTXT)            TextView rewardCrystalTXT;
    @BindView(R.id.rewardFragmentTXT)           TextView rewardFragmentTXT;
    @BindView(R.id.infoBTN)                     View infoBTN;
    @BindView(R.id.contentCON)                  View contentCON;

    @BindView(R.id.redeemableIV)                FixedWidthImageView redeemableIV;
    @BindView(R.id.nameTXT)                     TextView nameTXT;
    @BindView(R.id.dateTXT)                     TextView dateTXT;
    @BindView(R.id.typeIV)                      ImageView typeIV;
    @BindView(R.id.valueTXT)                    TextView valueTXT;
    @BindView(R.id.contentTXT)                  TextView contentTXT;
    @BindView(R.id.noteCON)                     View noteCON;
    @BindView(R.id.noteTXT)                     TextView noteTXT;

    @State int id;

    public static MyRewardDetailFragment newInstance(int id) {
        MyRewardDetailFragment myRewardDetailFragment = new MyRewardDetailFragment();
        myRewardDetailFragment.id = id;
        return myRewardDetailFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_my_reward_detail;
    }

    @Override
    public void onViewReady() {
        rewardsActivity = (RewardsActivity) getContext();
        rewardsActivity.setTitle("Item Details");
        rewardsActivity.showOption(false);
        rewardsActivity.setTitleDark(false);
        rewardsActivity.showWallet(false);
        shopSRL.setOnRefreshListener(this);
        infoBTN.setOnClickListener(this);
        redeemableIV.autoResize(true);
        Analytics.trackEvent("rewards_MyRewardDetailFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public void onRefresh() {
        refresh();
    }


    private void refresh(){

        contentCON.setVisibility(View.INVISIBLE);

        shopSRL.setColorSchemeResources(R.color.colorPrimary);
        shopSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        shopSRL.setOnRefreshListener(this);

        new MyRewardShowRequest(getContext())
                .setSwipeRefreshLayout(shopSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "image")
                .addParameters(Keys.server.key.REWARD_ID, id)
                .showSwipeRefreshLayout(true)
                .execute();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.infoBTN:
                rewardsActivity.openMyTreasuryFragment();
                Analytics.trackEvent("rewards_MyRewardDetailFragment_attachBTN");
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(MyRewardShowRequest.ServerResponse responseData) {
        MyRewardShowTransformer myRewardShowTransformer = responseData.getData(MyRewardShowTransformer.class);
        if(myRewardShowTransformer.status){
            contentCON.setVisibility(View.VISIBLE);
            rewardCrystalTXT.setText(UserData.getRewardCrystal());
            rewardFragmentTXT.setText(UserData.getRewardFragment());
            displayData(myRewardShowTransformer.data);
        }
    }

    private void displayData(MyRewardsItem myRewardsItem){

        nameTXT.setText(myRewardsItem.title);

        Glide.with(getContext())
                .load(myRewardsItem.image.data.full_path)
                .apply(new RequestOptions()

                        .placeholder(R.color.light_gray)
                .error(R.color.light_gray))
                .into(redeemableIV);

        typeIV.setImageDrawable(
                ActivityCompat.getDrawable(getContext(),
                        myRewardsItem.reward_type.equalsIgnoreCase("crystal") ?
                                R.drawable.icon_lyka_gems :
                                R.drawable.icon_lyka_chips));

        valueTXT.setText(myRewardsItem.value);
        contentTXT.setText(myRewardsItem.content);

        if(StringFormatter.isEmpty(myRewardsItem.note)){
            noteCON.setVisibility(View.GONE);
        }else{
            noteCON.setVisibility(View.VISIBLE);
            noteTXT.setText(myRewardsItem.note);
        }
    }
}
