package com.thingsilikeapp.android.fragment.rewards.verification;

import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.VerificationActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.widget.image.ResizableImageView;

import butterknife.BindView;
import icepick.State;

public class VerificationImageFragment extends BaseFragment {

    public static final String TAG = VerificationImageFragment.class.getName();

    private VerificationActivity verificationActivity;

    @BindView(R.id.imageIV)         ResizableImageView imageView;
    @BindView(R.id.typeCON)         View typeCON;
    @BindView(R.id.imageCON)        View imageCON;
    @BindView(R.id.textCON)         View textCON;
    @BindView(R.id.typeTXT)         TextView typeTXT;
    @BindView(R.id.statusTXT)       TextView statusTXT;
    @BindView(R.id.emailET)
    EditText emailET;

    @State String url;
    @State String format;
    @State String type;
    @State String status;

    public static VerificationImageFragment newInstance(String url, String format, String type, String status) {
        VerificationImageFragment fragment = new VerificationImageFragment();
        fragment.url = url;
        fragment.format = format;
        fragment.type = type;
        fragment.status = status;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_verification_image;
    }

    @Override
    public void onViewReady() {
        verificationActivity = (VerificationActivity) getContext();
        verificationActivity.setTitle(format + " Verification");
        verificationActivity.setBackBTN(true);
        statusTXT.setText("Status: " + status);
        if (format.equalsIgnoreCase("Email") || format.equalsIgnoreCase("Phone")){
            textCON.setVisibility(View.VISIBLE);
            imageCON.setVisibility(View.GONE);
            emailET.setText(url);
        }else {
            Picasso.with(getContext()).load(url).into(imageView);
            textCON.setVisibility(View.GONE);
            imageCON.setVisibility(View.VISIBLE);
            if (format.equalsIgnoreCase("Identity")){
                typeCON.setVisibility(View.VISIBLE);
                String upperString = type.substring(0,1).toUpperCase() + type.substring(1);
                typeTXT.setText("Type: " + upperString);
            }else{
                typeCON.setVisibility(View.GONE);
            }
        }


        Analytics.trackEvent("rewards_verification_VerificationImageFragment_onViewReady");

    }
}
