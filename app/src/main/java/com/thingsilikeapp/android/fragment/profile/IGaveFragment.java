package com.thingsilikeapp.android.fragment.profile;

import android.app.ProgressDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.iGaveReceivedActivity;
import com.thingsilikeapp.android.adapter.GiftRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.SendGiftDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListTransactionItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.wishlist.UserSentWishListRequest;
import com.thingsilikeapp.server.request.wishlist.WishListSendPermissionRequest;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListTransactionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class IGaveFragment extends BaseFragment implements
        GiftRecycleViewAdapter.ClickListener,
        EndlessRecyclerViewScrollListener.Callback,
        SendGiftDialog.Callback{

    public static final String TAG = IGaveFragment.class.getName();

    private GiftRecycleViewAdapter giftRecycleViewAdapter;
    private UserSentWishListRequest userSentWishListRequest;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;
    private iGaveReceivedActivity iGaveReceivedActivity;

    @BindView(R.id.observableRecyclerView)      RecyclerView observableRecyclerView;
    @BindView(R.id.placeHolderCON)              View placeHolderCON;
    @BindView(R.id.loadingIV)                   View loadingIV;

    @State int userID;


    public static IGaveFragment newInstance(int userID) {
        IGaveFragment iGaveFragment = new IGaveFragment();
        iGaveFragment.userID = userID;
        return iGaveFragment;
    }



    @Override
    public int onLayoutSet() {
        return R.layout.fragment_i_gave;
    }

    @Override
    public void onViewReady() {
        setupListView();
        initFeedAPI();
        iGaveReceivedActivity=(iGaveReceivedActivity)getContext();
        iGaveReceivedActivity.setTitle("Gave");
        Analytics.trackEvent("profile_IGraveFragmentFragment_onViewReady");
    }


    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void setupListView(){
        giftRecycleViewAdapter = new GiftRecycleViewAdapter(getContext(), GiftRecycleViewAdapter.DisplayType.GAVE);
        linearLayoutManager = new LinearLayoutManager(getContext());
//        linearLayoutManager.setScrollEnabled(false);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        observableRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        observableRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(20));
        observableRecyclerView.setLayoutManager(linearLayoutManager);
        observableRecyclerView.setHasFixedSize(false);
        observableRecyclerView.setAdapter(giftRecycleViewAdapter);
        giftRecycleViewAdapter.setOnItemClickListener(this);
    }

    public void setScrollable(boolean scrollable){
        if(linearLayoutManager != null){
//            linearLayoutManager.setScrollEnabled(scrollable);
        }
    }

    @Override
    public void onItemClick(int position, View v) {
        WishListTransactionItem wishListTransactionItem = (WishListTransactionItem) giftRecycleViewAdapter.getItemData(position);
        ((RouteActivity) getContext()).startItemActivity(wishListTransactionItem.id, wishListTransactionItem.owner.data.id, "i_gave");
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        ((RouteActivity)getActivity()).startProfileActivity(userItem.id);
    }

    @Override
    public void onStatusClick(String status, int wishlistID, int userID) {
        SendGiftDialog.newInstance("Dedication Message", wishlistID, this).show(getChildFragmentManager(), SendGiftDialog.TAG);
    }

    private void initFeedAPI(){
        userSentWishListRequest = new UserSentWishListRequest(getContext());
        userSentWishListRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.info,owner.social,owner.statistics,sender.info,sender.social,sender.statistics,delivery_info")
                .addParameters(Keys.server.key.INCLUDE, "image,owner.info,owner.social,sender.info,sender.statistics")
                .addParameters(Keys.server.key.USER_ID, userID)
                .addParameters(Keys.server.key.VIEW_ALL, "yes")
                .setPerPage(10);
    }

    public void refreshList(){
        System.gc();
        if (userSentWishListRequest != null) {
            userSentWishListRequest.first();
        }
        loadingIV.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(UserSentWishListRequest.ServerResponse responseData) {
        WishListTransactionTransformer wishListTransactionTransformer = responseData.getData(WishListTransactionTransformer.class);
        if(wishListTransactionTransformer.status){
            if(responseData.isNext()){
                giftRecycleViewAdapter.addNewData(wishListTransactionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                giftRecycleViewAdapter.setNewData(wishListTransactionTransformer.data);
            }
        }
        loadingIV.setVisibility(View.GONE);
        observableRecyclerView.setVisibility(giftRecycleViewAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE);
        placeHolderCON.setVisibility(giftRecycleViewAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        observableRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        userSentWishListRequest.nextPage();
    }

    private void attemptSendRequest(int wishListID, String dedication) {
        new WishListSendPermissionRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Sending Gift...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .addParameters(Keys.server.key.DEDICATION_MESSAGE, dedication)
                .execute();
    }

    @Subscribe
    public void onResponse(WishListSendPermissionRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            giftRecycleViewAdapter.updateStatus(wishListInfoTransformer.wishListItem.id, "pending");
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onAccept(int wishlistID, String dedication) {
        attemptSendRequest(wishlistID, dedication);
    }
}