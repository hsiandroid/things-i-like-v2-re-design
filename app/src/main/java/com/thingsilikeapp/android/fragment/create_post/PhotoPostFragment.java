package com.thingsilikeapp.android.fragment.create_post;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.CreatePostActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import org.andengine.util.debug.Debug;

import butterknife.BindView;

public class PhotoPostFragment extends BaseFragment  implements View.OnClickListener{

    public static final String TAG = PhotoPostFragment.class.getName();

    private CreatePostActivity createPostActivity;

    public Callback callback;

    @BindView(R.id.titleTXT)            TextView titleTXT;
    @BindView(R.id.captureBTN)          ImageView captureBTN;
    @BindView(R.id.selfieBTN)           ImageView selfieBTN;
    @BindView(R.id.rearBTN)             ImageView rearBTN;
    @BindView(R.id.flashOnBTN)          ImageView flashOnBTN;
    @BindView(R.id.flashOffBTN)         ImageView flashOffBTN;
    @BindView(R.id.mainBackButtonIV)	View mainBackButtonIV;
    @BindView(R.id.nextBTN)             TextView nextBTN;

    public static PhotoPostFragment newInstance() {
        PhotoPostFragment fragment = new PhotoPostFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_photopost;
    }

    @Override
    public void onViewReady() {
        createPostActivity = (CreatePostActivity) getContext();
        mainBackButtonIV.setOnClickListener(this);
        titleTXT.setText("Photo");
        nextBTN.setText("");
        captureBTN.setOnClickListener(this);
        selfieBTN.setOnClickListener(this);
        rearBTN.setOnClickListener(this);
        flashOnBTN.setOnClickListener(this);
        flashOffBTN.setOnClickListener(this);
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i == KeyEvent.KEYCODE_VOLUME_DOWN)
        {
            Log.e("EEEEEEEEEE", "CLICKED");
            return true;
        }

                return false;
            }
        });

        Analytics.trackEvent("create_post_PhotoPostFragment_onViewReady");

    }

    @Override
    public void onResume() {
        super.onResume();
//        cameraView.start();
    }

    @Override
    public void onPause() {
        super.onPause();
//        cameraView.stop();
//        Log.e("PHOTOS", "PAUSED");
//        if (cameraView !=null){
//            cameraView.stop();
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if (cameraView != null){
//            cameraView.destroy();
//        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
//            case R.id.pickerBTN:
//                break;
            case R.id.mainBackButtonIV:
                createPostActivity.onBackPressed();
                Analytics.trackEvent("create_post_PhotoPostFragment_mainBackButton");
                break;
            case R.id.captureBTN:
                if(callback != null){
                    callback.onCaptureSuccess(true);
                    Analytics.trackEvent("create_post_PhotoPostFragment_captureBTN");
                }
                break;
            case R.id.selfieBTN:
                selfieBTN.setVisibility(View.GONE);
                rearBTN.setVisibility(View.VISIBLE);
                if(callback != null){
                    callback.onCameraView(false);
                }
                Analytics.trackEvent("create_post_PhotoPostFragment_selfieBTN");
                break;
            case R.id.rearBTN:
                selfieBTN.setVisibility(View.VISIBLE);
                rearBTN.setVisibility(View.GONE);
                if(callback != null){
                    callback.onCameraView(true);
                }
                Analytics.trackEvent("create_post_PhotoPostFragment_rearBTN");
                break;
            case R.id.flashOnBTN:
                flashOffBTN.setVisibility(View.VISIBLE);
                flashOnBTN.setVisibility(View.GONE);
                if(callback != null){
                    callback.onFlash(true);
                }
                Analytics.trackEvent("create_post_PhotoPostFragment_flashOnBTN");
                break;
            case R.id.flashOffBTN:
                flashOffBTN.setVisibility(View.GONE);
                flashOnBTN.setVisibility(View.VISIBLE);
                if(callback != null){
                    callback.onFlash(false);
                }
                Analytics.trackEvent("create_post_PhotoPostFragment_flashOffBTN");
                break;
        }
    }

//    @Override
//    public boolean onKeyUp(int keyCode, KeyEvent event) {
//        super.onKeyUp(keyCode, event);
//        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
//        {
//            Toast.makeText(MainActivity.this,"Up working",Toast.LENGTH_SHORT).show();
//            return true;
//        }
//        return false;
//    }
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        super.onKeyDown(keyCode, event);
//        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
//        {
//            Toast.makeText(MainActivity.this,"Down working",Toast.LENGTH_SHORT).show();
//            return true;
//        }
//        return false;
//    }

    public interface Callback {
        void onCaptureSuccess(boolean capture);
        void onFlash(boolean flash);
        void onCameraView(boolean facing);
    }
}
