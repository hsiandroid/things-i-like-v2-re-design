package com.thingsilikeapp.android.fragment.registration;

import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.NewRegistrationActivity;
import com.thingsilikeapp.android.dialog.CalendarDialog;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;

import butterknife.BindView;
import icepick.State;

public class RegisterDFragment extends BaseFragment implements View.OnClickListener, CalendarDialog.DateTimePickerListener {

    public static final String TAG = RegisterDFragment.class.getName();

    private NewRegistrationActivity registrationActivity;

    @BindView(R.id.nextBTN)         TextView nextBTN;
    @BindView(R.id.backBTN)         TextView backBTN;
    @BindView(R.id.birthDateBTN)    TextView birthdateBTN;

    @State String username;
    @State String gender;
    @State String name;

    public static RegisterDFragment newInstance(String username, String gender, String name) {
        RegisterDFragment fragment = new RegisterDFragment();
        fragment.username = username;
        fragment.gender = gender;
        fragment.name = name;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_registration_d;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (NewRegistrationActivity) getContext();
        nextBTN.setOnClickListener(this);
        birthdateBTN.setOnClickListener(this);
        backBTN.setOnClickListener(this);
        Analytics.trackEvent("registration_RegisterDFragment_onViewReady");
    }

    private void showCalendarPicker(){
        CalendarDialog.newInstance(this, birthdateBTN.getText().toString()).show(getChildFragmentManager(), CalendarDialog.TAG);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.nextBTN:
                if (birthdateBTN.getText().toString().equalsIgnoreCase("Set birthday")){
                    ToastMessage.show(getContext(), "Please set your birthday", ToastMessage.Status.FAILED);
                }else{
                    registrationActivity.openEFragment(username, gender, birthdateBTN.getText().toString(), name);
                }
                Analytics.trackEvent("registration_RegisterDFragment_nextBTN");
                break;
            case R.id.birthDateBTN:
                showCalendarPicker();
                Analytics.trackEvent("registration_RegisterDFragment_birthDateBTN");
                break;
            case R.id.backBTN:
                registrationActivity.startLandingActivity();
                Analytics.trackEvent("registration_RegisterDFragment_backBTN");
                break;
        }
    }

    @Override
    public void forDisplay(String date) {
        birthdateBTN.setText(date);
    }
}
