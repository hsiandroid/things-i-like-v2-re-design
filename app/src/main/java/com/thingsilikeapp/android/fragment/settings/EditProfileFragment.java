package com.thingsilikeapp.android.fragment.settings;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;
import com.thingsilikeapp.android.adapter.SpinnerAdapter;
import com.thingsilikeapp.android.dialog.CalendarDialog;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.android.dialog.CountryDialog;
import com.thingsilikeapp.android.dialog.ImagePickerV2Dialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.ChangeEmailItem;
import com.thingsilikeapp.data.model.PrivacyItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.CountryData;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.user.AvatarRequest;
import com.thingsilikeapp.server.request.user.ChangeContactNumberRequest;
import com.thingsilikeapp.server.request.user.EditProfileRequest;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class EditProfileFragment extends BaseFragment implements View.OnClickListener,
        ImagePickerV2Dialog.ImageCallback, CountryDialog.CountryPickerListener,
        CalendarDialog.DateTimePickerListener,
        View.OnFocusChangeListener{

    public static final String TAG = EditProfileFragment.class.getName();

    private SettingsActivity settingsActivity;

    @BindView(R.id.nameET)              EditText nameET;
    @BindView(R.id.birthdayTXT)         TextView birthdayTXT;
    @BindView(R.id.editProfileNoteTXT)  TextView editProfileNoteTXT;
    @BindView(R.id.usernameET)          EditText usernameET;
    @BindView(R.id.emailET)             TextView emailET;
    @BindView(R.id.contactET)           TextView contactET;
    @BindView(R.id.professionET)        EditText professionET;
    @BindView(R.id.contactCodeTXT)      TextView contactCodeTXT;
    @BindView(R.id.contactCON)          View contactCON;
    @BindView(R.id.countryFlagIV)       ImageView countryFlagIV;
    @BindView(R.id.maleRBTN)            RadioButton maleRBTN;
    @BindView(R.id.femaleRBTN)          RadioButton femaleRBTN;
    @BindView(R.id.genderRG)            RadioGroup genderRG;
    @BindView(R.id.updateBTN)           View updateBTN;
    @BindView(R.id.changeAvatarIV)      ImageView changeAvatarIV;
    @BindView(R.id.avatarCIV)           ImageView avatarCIV;
    @BindView(R.id.changeAvatarTXT)     View changeAvatarTXT;
    @BindView(R.id.scrollView)          ScrollView scrollView;
    @BindView(R.id.privacySPR)          Spinner privacySPR;
    @BindView(R.id.privacyCON)          View privacyCON;
    @BindView(R.id.privacyBTN)          View privacyBTN;
    @BindView(R.id.editEmailBTN)        TextView editEmailBTN;
    @BindView(R.id.editContactBTN)      TextView editContactBTN;

    private CountryData.Country countryData;

    @State String country;
    @State String contact;
    @State String contry_code;

    public static EditProfileFragment newInstance() {
        EditProfileFragment editProfileFragment = new EditProfileFragment();

        return editProfileFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_profile;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle(getString(R.string.title_edit_profile));
        updateBTN.setOnClickListener(this);
        changeAvatarTXT.setOnClickListener(this);
        avatarCIV.setOnClickListener(this);
        contactCodeTXT.setOnClickListener(this);
        contactCON.setOnClickListener(this);
        birthdayTXT.setOnClickListener(this);
        privacyCON.setOnClickListener(this);
        privacyBTN.setOnClickListener(this);
        emailET.setOnClickListener(this);
        contactET.setOnClickListener(this);
        editEmailBTN.setOnClickListener(this);
        editContactBTN.setOnClickListener(this);
        setUpPrivacySPR();
        setNotClickableTextView(birthdayTXT, this);
        PasswordEditTextManager.usernameFormat(usernameET);
        editProfileNoteTXT.setText(spannableString());
        editProfileNoteTXT.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!UserData.getUserItem().equals("") || !UserData.getUserItem().equals(null) || UserData.getUserItem() != null){
            displayData(UserData.getUserItem());
        }
    }

    private SpannableString spannableString(){
        String text=getString(R.string.edit_profile_note);
        String privacy = "Privacy Policy";
        String terms = "Terms and Condition";

        SpannableString spanString = new SpannableString(text + " " + privacy + " and " + terms);

        spanString.setSpan(new ForegroundColorSpan(Color.parseColor("#0000FF")), text.length(), text.length() + privacy.length() + 1, 0);
        spanString.setSpan(new ForegroundColorSpan(Color.parseColor("#0000FF")), text.length() + privacy.length() + 6, text.length() + privacy.length() + 1 + terms.length() + 5, 0);

        ClickableSpan privacyClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                settingsActivity.openUrl("https://thingsilikeapp.com/privacy-policy");
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
            }
        };

        ClickableSpan termsClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                settingsActivity.openUrl("https://thingsilikeapp.com/terms");
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
            }
        };

        spanString.setSpan(privacyClick, text.length(), text.length() + privacy.length() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanString.setSpan(termsClick, text.length() + privacy.length() + 6, text.length() + privacy.length() + 1 + terms.length() + 5, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spanString;
    }

    private void setUpPrivacySPR() {
        final List<PrivacyItem> privacyItems = new ArrayList<>();
        PrivacyItem privacyItem = new PrivacyItem();
        privacyItem.id = 1;
        privacyItem.name = "Only Me";
        privacyItem.code = "only_me";
        privacyItems.add(privacyItem);

        privacyItem = new PrivacyItem();
        privacyItem.id = 2;
        privacyItem.name = "Public (MM/DD)";
        privacyItem.code = "month_day";
        privacyItems.add(privacyItem);

        privacyItem = new PrivacyItem();
        privacyItem.id = 3;
        privacyItem.name = "Public (MM/DD/YY)";
        privacyItem.code = "month_day_year";
        privacyItems.add(privacyItem);

        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(getContext());
        spinnerAdapter.setNewData(privacyItems);
        privacySPR.setAdapter(spinnerAdapter);
    }

    private void setNotClickableTextView(TextView textView, View.OnFocusChangeListener focusChangeListener){
        textView.setInputType(InputType.TYPE_NULL);
        textView.setOnFocusChangeListener(focusChangeListener);
    }

    private void showCountryPicker(){
        CountryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
    }

    private void showCalendarPicker(){
        CalendarDialog.newInstance(this, birthdayTXT.getText().toString()).show(getChildFragmentManager(), CalendarDialog.TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.updateBTN:
                attemptUpdateInfo();
                settingsActivity.facebookLogger(FacebookCustomLoggerMessage.SettingsActivity.EVENT,FacebookCustomLoggerMessage.SettingsActivity.UPDATE_PROFILE);
                break;
            case R.id.changeAvatarTXT:
            case R.id.avatarCIV:
                ImagePickerV2Dialog.newInstance("Change Avatar", true, this).show(getChildFragmentManager(), ImagePickerV2Dialog.TAG);
                settingsActivity.facebookLogger(FacebookCustomLoggerMessage.SettingsActivity.EVENT,FacebookCustomLoggerMessage.SettingsActivity.UPDATE_AVATAR);
                break;
            case R.id.contactCON:
            case R.id.contactCodeTXT:
            case R.id.countryFlagIV:
//                showCountryPicker();
                break;
            case R.id.birthdayTXT:
                birthdayTXT.setError(null);
                showCalendarPicker();
                break;
            case R.id.privacyBTN:
            case R.id.privacyCON:
                privacySPR.performClick();
                break;
            case R.id.editEmailBTN:
                settingsActivity.openEditEmailFragment();
                break;
            case R.id.editContactBTN:
                settingsActivity.openEditContactNumberFragment(country,contact,contry_code);
//                settingsActivity.openOTPChangeNumber(contact, contry_code, country);
                break;
        }
    }

    private String birthdayFormat(String birthday){
        if(birthday == null){
            return "";
        }
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat newFormat = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date = originalFormat.parse(birthday);
            return newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String birthdayReFormat(String birthday){
        SimpleDateFormat originalFormat = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = originalFormat.parse(birthday);
            return newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void displayData(UserItem userItem){
        nameET.setText(userItem.common_name);
        emailET.setText(userItem.email);
        usernameET.setText(userItem.username);
        if (userItem.info.data != null) {
            birthdayTXT.setText(birthdayFormat(userItem.info.data.birthdate));
            professionET.setText(userItem.info.data.profession);

            countryData = CountryData.getCountryDataByCode1(userItem.info.data.country);
            contactET.setText(StringFormatter.replaceNull(userItem.info.data.contact_number).replace(countryData.code3, ""));
            contactCodeTXT.setText(countryData.code3);
            countryCode(countryData);

            country = userItem.info.data.country;
            contact = userItem.info.data.contact_number;
            contry_code = countryData.code3;

            if (userItem.info.data.gender == null) {
                genderRG.clearCheck();
            } else {
                maleRBTN.setChecked(userItem.info.data.gender.equals("male"));
                femaleRBTN.setChecked(userItem.info.data.gender.equals("female"));
            }
        }

        Glide.with(getContext())
                .load(userItem.getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .dontTransform()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(avatarCIV);

//        if(userItem.fb_id == null){
//            verificationCON.setVisibility(View.VISIBLE);
//        }else{
//            verificationCON.setVisibility(View.GONE);
//        }

        switch (userItem.info.data.my_privacy) {
            case "only_me":
                privacySPR.setSelection(0);
                break;
            case "month_day":
                privacySPR.setSelection(1);
                break;
            case "month_day_year":
                privacySPR.setSelection(2);
                break;
        }
    }

    private void countryCode(CountryData.Country country){
        contactCodeTXT.setText(country.code3);
        Glide.with(getContext())
                .load(country.getFlag())
                .into(countryFlagIV);
    }

    private void attemptUpdateInfo(){
        usernameET.setError(null);
        nameET.setError(null);
        contactET.setError(null);
        emailET.setError(null);
//        passwordET.setError(null);

        EditProfileRequest editProfileRequest = new EditProfileRequest(getContext());
        editProfileRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating Profile...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addMultipartBody(Keys.server.key.INCLUDE, "info,social,statistics,kyc")
                .addMultipartBody(Keys.server.key.NAME, nameET.getText().toString())
                .addMultipartBody(Keys.server.key.PR0FESSION, professionET.getText().toString())
                .addMultipartBody(Keys.server.key.CONTACT_NUMBER, contactCodeTXT.getText().toString() + contactET.getText().toString())
                .addMultipartBody(Keys.server.key.USERNAME, usernameET.getText().toString())
                .addMultipartBody(Keys.server.key.EMAIL, emailET.getText().toString())
                .addMultipartBody(Keys.server.key.GENDER, femaleRBTN.isChecked() ? "female" :  "male")
                .addMultipartBody(Keys.server.key.CONTACT_COUNTRY_CODE, countryData.code1)
                .addMultipartBody(Keys.server.key.CONTACT_COUNTRY_DIAL_CODE, contactCodeTXT.getText().toString())
                .addMultipartBody(Keys.server.key.MY_PRIVACY, ((PrivacyItem) privacySPR.getSelectedItem()).code)
                .addMultipartBody(Keys.server.key.BIRTHDATE, birthdayReFormat(birthdayTXT.getText().toString()))
//                .addMultipartBody(Keys.server.key.PASSWORD, passwordET.getText().toString())
                .execute();

        Log.e("Country Code",">>>" + countryData.code1);
        Log.e("Country Dial Code",">>>" + contactCodeTXT.getText().toString());
    }

    private void attemptUpdateAvatar(File file){
        AvatarRequest avatarRequest = new AvatarRequest(getContext());
        if(file != null){
            avatarRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating Avatar...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addMultipartBody(Keys.server.key.INCLUDE, "info,social,statistics,kyc")
                .addMultipartBody(Keys.server.key.FILE, file)
                .execute();
        }
    }

    @Override
    public void result(File file) {
        attemptUpdateAvatar(file);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private int incorrect = 0;

    @Subscribe
    public void onResponse(EditProfileRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
//            passwordET.setText("");
            displayData(userTransformer.userItem);
            UserData.insert(userTransformer.userItem);
            settingsActivity.startMainActivity("profile");
            incorrect = 0;
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.FAILED);
            if(userTransformer.hasRequirements()){
                ErrorResponseManger.first(usernameET, userTransformer.requires.username);
                ErrorResponseManger.first(nameET, userTransformer.requires.name);
                ErrorResponseManger.first(birthdayTXT, userTransformer.requires.birthdate);
                ErrorResponseManger.first(contactET, userTransformer.requires.contact_number);
                ErrorResponseManger.first(emailET, userTransformer.requires.email);
//                ErrorResponseManger.first(passwordET, userTransformer.requires.password);
                ErrorResponseManger.first(professionET, userTransformer.requires.profession);
                String password = ErrorResponseManger.first(userTransformer.requires.password);
                if(!StringFormatter.isEmpty(password)){
                    incorrect++;
                }
            }
        }

        if(incorrect >= 3){
            UserData.clearData();
            settingsActivity.startLandingActivity();
            ToastMessage.show(getActivity(), "You are automatically logged out to the app. Due to multiple times of invalid attempts", ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(AvatarRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if(userTransformer.status){
//            passwordET.setText("");
            UserData.insert(userTransformer.userItem);
            Glide.with(getContext())
                    .load(userTransformer.userItem.getAvatar())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.placeholder_avatar)
                    .error(R.drawable.placeholder_avatar)
                    .dontAnimate()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                    .into(avatarCIV);
            scrollView.smoothScrollTo(0, 0);
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.SUCCESS);
        }
    }

    @Override
    public void onSelectCountry(CountryData.Country country, int requestCode) {
        countryData = country;
        countryCode(countryData);
    }

    @Override
    public void forDisplay(String date) {
        birthdayTXT.setText(date);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()){
            case R.id.birthdayTXT:
                if (hasFocus){
                    showCalendarPicker();
                }
                break;
        }
    }
}
