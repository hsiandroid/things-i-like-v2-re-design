package com.thingsilikeapp.android.fragment.registration;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RegistrationActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.server.request.auth.ResetPasswordRequest;
import com.thingsilikeapp.server.request.auth.VerificationCodeRequest;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class ResetPasswordFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = ResetPasswordFragment.class.getName();

    private RegistrationActivity registrationActivity;

    @BindView(R.id.confirmNewPasswordTV)        TextView confirmNewPasswordTV;
    @BindView(R.id.resendVerificationCodeTV)    TextView resendVerificationCodeTV;
    @BindView(R.id.emailET)                     EditText emailET;
    @BindView(R.id.verificationCodeET)          EditText verificationCodeET;
    @BindView(R.id.passwordET)                  EditText passwordET;
    @BindView(R.id.showPasswordBTN)             ImageView showPasswordBTN;
    @BindView(R.id.confirmPasswordET)           EditText confirmPasswordET;
    @BindView(R.id.showConfirmPasswordBTN)      ImageView showConfirmPasswordBTN;
    @BindView(R.id.showPasswordCHBX)            CheckBox showPasswordCHBX;
    @BindView(R.id.showPassword1CHBX)            CheckBox showPassword1CHBX;

    @State String email;

    public static ResetPasswordFragment newInstance(String email) {
        ResetPasswordFragment resetPasswordFragment = new ResetPasswordFragment();
        resetPasswordFragment.email = email;
        return resetPasswordFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_reset_password;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();
        registrationActivity.setTitle(getString(R.string.title_reset_password));

        emailET.setText(email);
        confirmNewPasswordTV.setOnClickListener(this);
        resendVerificationCodeTV.setOnClickListener(this);

        PasswordEditTextManager.addShowPassword(getContext(), passwordET, showPassword1CHBX);
        PasswordEditTextManager.addShowPassword(getContext(), confirmPasswordET, showPasswordCHBX);
        Analytics.trackEvent("registration_ResetPasswordFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.confirmNewPasswordTV:
                attemptRequestVerification();
                Analytics.trackEvent("registration_ResetPasswordFragment_confirmNewPasswordTV");
                break;
            case R.id.resendVerificationCodeTV:
                attemptResendRequestVerification();
                Analytics.trackEvent("registration_ResetPasswordFragment_resendVerificationCodeTV");
                break;
        }
    }

    private void attemptRequestVerification(){
        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest(getContext());
        resetPasswordRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Resetting password...", false, false))
                .addParameters(Keys.server.key.EMAIL, emailET.getText().toString())
                .addParameters(Keys.server.key.VALIDATION_TOKEN, verificationCodeET.getText().toString())
                .addParameters(Keys.server.key.PASSWORD, passwordET.getText().toString())
                .addParameters(Keys.server.key.PASSWORD_CONFIRMATION, confirmPasswordET.getText().toString())
                .execute();
    }

    private void attemptResendRequestVerification(){
        VerificationCodeRequest verificationCodeRequest = new VerificationCodeRequest(getContext());
        verificationCodeRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Resending verification code...", false, false))
                .addParameters(Keys.server.key.EMAIL, emailET.getText().toString())
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(VerificationCodeRequest.ServerResponse responseData) {
        BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(getActivity(), baseTransformer.msg,ToastMessage.Status.SUCCESS);
        }
    }

    @Subscribe
    public void onResponse(ResetPasswordRequest.ServerResponse responseData) {
        BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            getActivity().finish();
        }else {
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}
