package com.thingsilikeapp.android.fragment.settings;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.user.ChangePasswordRequest;
import com.thingsilikeapp.server.transformer.user.EditProfileTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ChangePasswordFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = ChangePasswordFragment.class.getName();

    private SettingsActivity settingsActivity;

    @BindView(R.id.oldPasswordET)               EditText oldPasswordET;
    @BindView(R.id.showOldPasswordBTN)          ImageView showOldPasswordBTN;
    @BindView(R.id.newPasswordET)               EditText newPasswordET;
    @BindView(R.id.showNewPasswordBTN)          ImageView showNewPasswordBTN;
    @BindView(R.id.confirmNewPasswordET)        EditText confirmNewPasswordET;
    @BindView(R.id.showConfirmNewPasswordBTN)   ImageView showConfirmNewPasswordBTN;
    @BindView(R.id.confirmBTN)                  View confirmBTN;
//    @BindView(R.id.avatarCIV)                   ImageView avatarCIV;
//    @BindView(R.id.userNameTXT)                 TextView userNameTXT;
    @BindView(R.id.showPasswordCHBX)            CheckBox showPasswordCHBX;
    @BindView(R.id.showPassword1CHBX)            CheckBox showPassword1CHBX;
    @BindView(R.id.showPassword2CHBX)            CheckBox showPassword2CHBX;

    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();
        return changePasswordFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_change_password;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle("Change Password");
        confirmBTN.setOnClickListener(this);

//        userNameTXT.setText(UserData.getUserItem().name);

//        Glide.with(getContext())
//                .load(UserData.getUserItem().getAvatar())
//                .apply(new RequestOptions()
//                .placeholder(R.drawable.placeholder_avatar)
//                .error(R.drawable.placeholder_avatar))
//                .into(avatarCIV);

        PasswordEditTextManager.addShowPassword(getContext(), oldPasswordET, showPassword1CHBX);
        PasswordEditTextManager.addShowPassword(getContext(), newPasswordET, showPassword2CHBX);
        PasswordEditTextManager.addShowPassword(getContext(), confirmNewPasswordET, showPasswordCHBX);

    }

    private void attemptChangePassword(){
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest(getContext());
        changePasswordRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating Password...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.CURRENT_PASSWORD, oldPasswordET.getText().toString())
                .addParameters(Keys.server.key.NEW_PASSWORD, newPasswordET.getText().toString())
                .addParameters(Keys.server.key.NEW_PASSWORD_CONFIRMATION, confirmNewPasswordET.getText().toString())
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmBTN:
                attemptChangePassword();
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.SettingsActivity.EVENT,FacebookCustomLoggerMessage.SettingsActivity.CHANGE_PASSWORD);
                break;
        }
    }

    @Subscribe
    public void onResponse(ChangePasswordRequest.ServerResponse responseData) {
        EditProfileTransformer editProfileTransformer = responseData.getData(EditProfileTransformer.class);
        if(editProfileTransformer.status){
            oldPasswordET.setText("");
            newPasswordET.setText("");
            confirmNewPasswordET.setText("");
            ToastMessage.show(getActivity(), editProfileTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), editProfileTransformer.msg, ToastMessage.Status.FAILED);
            if(editProfileTransformer.hasRequirements()){
                ErrorResponseManger.first(oldPasswordET, editProfileTransformer.requires.current_password);
                ErrorResponseManger.first(newPasswordET, editProfileTransformer.requires.new_password);
                ErrorResponseManger.first(confirmNewPasswordET, editProfileTransformer.requires.new_password_confirmation);
            }
        }
    }
}
