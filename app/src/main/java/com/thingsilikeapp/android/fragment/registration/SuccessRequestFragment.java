package com.thingsilikeapp.android.fragment.registration;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RegistrationActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SuccessRequestFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = SuccessRequestFragment.class.getName();

    public static SuccessRequestFragment newInstance(String email) {
        SuccessRequestFragment fragment = new SuccessRequestFragment();
        fragment.email = email;
        return fragment;
    }
    private RegistrationActivity registrationActivity;

    @BindView(R.id.okayBTN) TextView okayBTN;
    @BindView(R.id.textTXT) TextView textTXT;

    @State String email;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_success;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();
        okayBTN.setOnClickListener(this);
        registrationActivity.setTitle("Success!");
        textTXT.setText("An e-mail will be sent to your stored e-mail address in the next few minutes: " + email);
        Analytics.trackEvent("registration_SuccessRequestFragment_onViewReady");
    }


    @Override
    public void onStart() {
        super.onStart();
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
//        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.okayBTN:
                registrationActivity.startLandingActivity();
                Analytics.trackEvent("registration_SuccessRequestFragment_okayBTN");
            break;
        }
    }
}
