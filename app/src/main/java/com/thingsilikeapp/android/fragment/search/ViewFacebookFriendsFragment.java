package com.thingsilikeapp.android.fragment.search;

import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.ListView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MainActivity;
import com.thingsilikeapp.android.adapter.SuggestionAdapter;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.request.social.SuggestionRequest;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ViewFacebookFriendsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, SuggestionAdapter.ClickListener{

    public static final String TAG = ViewFacebookFriendsFragment.class.getName();

    private MainActivity mainActivity;
    private SuggestionAdapter suggestionAdapter;
    private List<UserItem> userItems;
    private SuggestionRequest suggestionRequest;

    @BindView(R.id.viewFacebookFriendsLV) ListView viewFacebookFriendsLV;

    public static ViewFacebookFriendsFragment newInstance() {
        ViewFacebookFriendsFragment viewFacebookFriendsFragment = new ViewFacebookFriendsFragment();
        return viewFacebookFriendsFragment;
    }

    @Override
    public int onLayoutSet() {

        return R.layout.fragment_view_facebook_friends;
    }

    @Override
    public void onViewReady() {
        setupFacebookFriendsListView();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void setupFacebookFriendsListView() {

        userItems = new ArrayList<>();
        suggestionAdapter = new SuggestionAdapter(getContext());
        viewFacebookFriendsLV.setAdapter(suggestionAdapter);
    }

    private void refreshList(){
        userItems.clear();

        suggestionAdapter.setNewData(userItems);

        suggestionRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onCommandClick(UserItem userItem) {

    }

    @Override
    public void onUserClick(UserItem userItem) {

    }
}
