package com.thingsilikeapp.android.fragment.search;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SearchActivity;
import com.thingsilikeapp.android.adapter.FriendsRecyclerViewAdapter;
import com.thingsilikeapp.data.model.FriendsItem;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class FriendsFragment extends BaseFragment {

    public static final String TAG = FriendsFragment.class.getName();

    private SearchActivity searchActivity;
    private FriendsRecyclerViewAdapter friendsRecyclerViewAdapter;
    private List<FriendsItem> friendsItem;

    @BindView(R.id.friendsRV) RecyclerView friendsRV;

    public static FriendsFragment newInstance() {
        FriendsFragment friendsFragment = new FriendsFragment();
        return friendsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_friends;
    }

    @Override
    public void onViewReady() {
        searchActivity = (SearchActivity) getActivity();
        searchActivity.setTitle("Friends List");
        setUpListView();
        sampleData();
    }

    private void setUpListView(){
        friendsItem = new ArrayList<>();
        friendsRecyclerViewAdapter = new FriendsRecyclerViewAdapter(getContext(),new ArrayList<FriendsItem>(), 0);
        friendsRV.setLayoutManager(new LinearLayoutManager(getContext()));
        friendsRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(20));
    }

    private void sampleData(){
        friendsItem.clear();

        FriendsItem friendsItems = new  FriendsItem();
        friendsItems.name = "John Doe";
        friendsItems.full_path="";
        friendsItem.add(friendsItems);
    }
}
