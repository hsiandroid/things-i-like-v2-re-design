package com.thingsilikeapp.android.fragment.wishlist;

import android.app.ProgressDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.WishListActivity;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.android.dialog.ImagePickerV2Dialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.helper.LinkPreviewRequest;
import com.thingsilikeapp.server.request.wishlist.EditWishListRequest;
import com.thingsilikeapp.server.transformer.helper.LinkTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import icepick.State;

public class EditWishListFragment extends BaseFragment implements
        View.OnClickListener,
        CategoryDialog.Callback,
        ImagePickerV2Dialog.ImageCallback{

    public static final String TAG = EditWishListFragment.class.getName();
    private WishListActivity wishListActivity;

    private Timer timer;

    @BindView(R.id.postBTN)                 View postBTN;
    @BindView(R.id.descriptionET)           EditText descriptionET;
    @BindView(R.id.othersET)                EditText othersET;
    @BindView(R.id.titleET)                 EditText titleET;
    @BindView(R.id.othersTIL)               View othersTIL;
    @BindView(R.id.categoryTXT)             TextView categoryTXT;

    @BindView(R.id.metaImageCON)            View metaImageCON;
    @BindView(R.id.metaImageImgIV)          ImageView metaImageImgIV;
    @BindView(R.id.metaImageCloseBTN)       View metaImageCloseBTN;

    @BindView(R.id.metaCON)                 View metaCON;
    @BindView(R.id.metaPB)                  View metaPB;
    @BindView(R.id.metaCloseBTN)            View metaCloseBTN;
    @BindView(R.id.metaDataCON)             View metaDataCON;

    @BindView(R.id.metaTitleTXT)            TextView metaTitleTXT;
    @BindView(R.id.metaDescriptionTXT)      TextView metaDescriptionTXT;
    @BindView(R.id.counterTXT)              TextView counterTXT;
    @BindView(R.id.metaImageIV)             ImageView metaImageIV;
    @BindView(R.id.cameraBTN)               View cameraBTN;
    @BindView(R.id.counterCON)              View counterCON;
    @BindView(R.id.prevBTN)                 View prevBTN;
    @BindView(R.id.nextBTN)                 View nextBTN;
    @BindView(R.id.cameraLBL)               View cameraLBL;
    @BindView(R.id.imageCloseBTN)           View imageCloseBTN;
    @BindView(R.id.cameraCon)               View cameraCon;
    @BindView(R.id.imageLBL)                View imageLBL;
    @BindView(R.id.categoryBTN)             View categoryBTN;
    @BindView(R.id.imageIV)                 ImageView imageIV;

    @State String currentSelectedURL;
    @State boolean isSearching;
    @State File file = null;

    @State String title = "";
    @State String url = "";
    @State String urlTitle = "";
    @State String urlDescription = "-";
    @State String urlImage = "/";
    @State int thumbnailIndex = 0;

    @State WishListItem wishListItem;
    private List<String> thumbnailImages;

    public static EditWishListFragment newInstance(WishListItem wishListItem) {
        EditWishListFragment editWishListFragment = new EditWishListFragment();
        editWishListFragment.wishListItem = wishListItem;
        return editWishListFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_wishlist;
    }

    @Override
    public void onViewReady() {
        wishListActivity = (WishListActivity) getContext();
        wishListActivity.setTitle(getString(R.string.title_main_edit));
        wishListActivity.setOnBackPressedListener(null);
        postBTN.setOnClickListener(this);
        categoryTXT.setOnClickListener(this);
        metaCloseBTN.setOnClickListener(this);
        prevBTN.setOnClickListener(this);
        nextBTN.setOnClickListener(this);
        metaImageCloseBTN.setOnClickListener(this);
        cameraBTN.setOnClickListener(this);
        cameraLBL.setOnClickListener(this);
        imageCloseBTN.setOnClickListener(this);
        categoryBTN.setOnClickListener(this);


        setUpUrlSearch();
        displayData();
    }

    public void displayData(){
        title = wishListItem.title;
        url = wishListItem.info.data.url;
        urlTitle = wishListItem.info.data.url_title;
        urlDescription = wishListItem.info.data.url_description;
        urlImage = wishListItem.info.data.url_image;
        String search = "{link}";
        String content = wishListItem.info.data.content;
        if(content.contains(search)){
            if(url != null && !url.equals("")){
                content = content.replace(search, url);
            }else{
                content = content.replace(search, "");
            }
        }

        Log.e("Image", ">>>" + wishListItem.image.data.full_path);
        Log.e("Image", ">>>" + urlImage);

        if(!wishListItem.image.data.full_path.equals(urlImage)){
            cameraCon.setVisibility(View.VISIBLE);
            Glide.with(getContext())
                    .load(wishListItem.image.data.full_path)
                    .into(imageIV);
        }else{
            cameraCon.setVisibility(View.GONE);
        }
        titleET.setText(wishListItem.title);
        descriptionET.setText(content);
        descriptionET.setSelection(descriptionET.length());
        categoryTXT.setText(wishListItem.category);
        openSharePost();
    }



    private void openSharePost(){
        if(!StringFormatter.isEmpty(url)){
            currentSelectedURL = url;
            descriptionET.setText("\n\n- " + url);
            metaPB.setVisibility(View.GONE);
            postBTN.setEnabled(true);
            metaCON.setVisibility(View.VISIBLE);
            metaDataCON.setVisibility(View.VISIBLE);
            thumbnailIndex = 0;
            thumbnailImages = new ArrayList<>();
            thumbnailImages.add(urlImage);
            if(thumbnailImages.size() > 0){
                displayCounter();
            }
            metaTitleTXT.setText(urlTitle);
            metaDescriptionTXT.setText(urlDescription);
        }
    }

    public void setUpUrlSearch(){

        descriptionET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if(descriptionET != null){
                                        if(currentSelectedURL !=null && !descriptionET.getText().toString().contains(currentSelectedURL)){
                                        closeMeta();
                                    }
                                    searchURL(descriptionET.getText().toString());
                                }
                            }
                        });
                    }
                }, 600);
            }
        });
    }

    public void searchURL(String s){
        String [] parts = s.split("\\s+");
        for( String item : parts ){
            if(Patterns.WEB_URL.matcher(item).matches()){
                if(!item.equals(currentSelectedURL)){
                    currentSelectedURL = item;
                    attemptGetMeta(item);
                }
                break;
            }
        }
    }

    public void closeMeta(){
        metaCON.setVisibility(View.GONE);
        metaPB.setVisibility(View.GONE);
        currentSelectedURL = "";
        url = "";
        urlTitle = "";
        urlDescription = "-";
        urlImage = "/";
        thumbnailIndex = 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.postBTN:
                attemptUpdate();
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.UPDATE_POST);
                break;
            case R.id.categoryTXT:
            case R.id.categoryBTN:
                categoryTXT.setError(null);
                CategoryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.CATEGORY);
                break;
            case R.id.metaCloseBTN:
                closeMeta();
                break;
            case R.id.cameraLBL:
            case R.id.cameraBTN:
                imageLBL.setVisibility(View.GONE);
                ImagePickerV2Dialog.newInstance(getString(R.string.post_hint_add_photo), false, this).show(getChildFragmentManager(), ImagePickerV2Dialog.TAG);
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.PICK_IMAGE);
                break;
            case R.id.imageCloseBTN:
                closeImage();
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.CLOSE_IMAGE);
                break;
            case R.id.prevBTN:
                prevThumbnail();
                break;
            case R.id.nextBTN:
                nextThumbnail();
                break;
        }
    }

    private void nextThumbnail(){
        if(thumbnailIndex < thumbnailImages.size() -1 ){
            thumbnailIndex++;
            displayCounter();
        }
    }

    private void prevThumbnail(){
        if(thumbnailIndex > 0){
            thumbnailIndex--;
            displayCounter();
        }
    }

    @Override
    public void callback(String categoryItem) {
        categoryTXT.setText(categoryItem);

        if(categoryItem.equalsIgnoreCase("other") || categoryItem.equalsIgnoreCase("others")){
            othersTIL.setVisibility(View.VISIBLE);
        }else{
            othersTIL.setVisibility(View.GONE);
        }
    }

    private void attemptUpdate(){
        imageLBL.setVisibility(View.GONE);

        EditWishListRequest editWishListRequest = new EditWishListRequest(getContext());

        if(file != null){
            editWishListRequest.addMultipartBody(Keys.server.key.FILE, file);
        }else {
            editWishListRequest
                    .addMultipartBody(Keys.server.key.TITLE, titleET.getText().toString())
                    .addMultipartBody(Keys.server.key.URL, StringFormatter.replaceNull(url))
//                    .addMultipartBody(Keys.server.key.URL_TITLE, StringFormatter.replaceNull(urlTitle))
                    .addMultipartBody(Keys.server.key.URL_DESCRIPTION, urlDescription)
                    .addMultipartBody(Keys.server.key.URL_IMAGE, urlImage);
        }

        if(urlTitle == null){
            urlTitle = "";
        }


        String category = categoryTXT.getText().toString();
        if(category.equalsIgnoreCase("other") || category.equalsIgnoreCase("others")){
            if(TextUtils.isEmpty(othersET.getText().toString())){
                othersET.setError("Field is required.");
                return;
            }else{
                category = othersET.getText().toString();
            }
        }

        editWishListRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating post...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addMultipartBody(Keys.server.key.TITLE,titleET.getText().toString())
                .addMultipartBody(Keys.server.key.WISHLIST_ID, wishListItem.id)
//                .addMultipartBody(Keys.server.key.TITLE, StringFormatter.replaceNull(urlTitle, category))
                .addMultipartBody(Keys.server.key.CATEGORY, category)
                .addMultipartBody(Keys.server.key.CONTENT, descriptionET.getText().toString())
                .execute();
    }

    private void attemptGetMeta(String meta_url){

        metaCON.setVisibility(View.VISIBLE);
        metaPB.setVisibility(View.VISIBLE);
        metaDataCON.setVisibility(View.GONE);

        url = "";
        urlTitle = "";
        urlDescription = "-";
        urlImage = "/";
        thumbnailIndex = 0;

        LinkPreviewRequest linkPreviewRequest = new LinkPreviewRequest(getContext());
        linkPreviewRequest.addParameters(Keys.server.key.URL, meta_url)
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(EditWishListRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if(wishListInfoTransformer.status){
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
            wishListActivity.finish();
        }else{
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
            if(wishListInfoTransformer.hasRequirements()){
                ErrorResponseManger.first(descriptionET, wishListInfoTransformer.requires.content);
                ErrorResponseManger.first(categoryTXT, wishListInfoTransformer.requires.category);

                if(ErrorResponseManger.first(wishListInfoTransformer.requires.file).equals("")){
                    imageLBL.setVisibility(View.GONE);
                }else{
                    imageLBL.setVisibility(View.VISIBLE);
                }
            }
        }
    }


    @Subscribe
    public void onResponse(LinkPreviewRequest.ServerResponse responseData) {
        if(responseData.getCode() == 500){
            closeMeta();
            postBTN.setEnabled(true);
        }

        LinkTransformer linkTransformer = responseData.getData(LinkTransformer.class);
        if(linkTransformer.status){
            metaPB.setVisibility(View.GONE);
            postBTN.setEnabled(true);
            metaDataCON.setVisibility(View.VISIBLE);


            urlTitle = linkTransformer.data.title;
            url = linkTransformer.data.url;
            urlDescription = linkTransformer.data.description;
            urlImage = "/";
            thumbnailIndex = 0;
            thumbnailImages = linkTransformer.data.images;
            if(thumbnailImages.size() > 0){
                displayCounter();
            }

            metaTitleTXT.setText(urlTitle);
            metaDescriptionTXT.setText(urlDescription);
        }else{
            metaCON.setVisibility(View.GONE);
            metaPB.setVisibility(View.GONE);
            postBTN.setEnabled(true);
            metaDataCON.setVisibility(View.GONE);
        }

    }

    @Subscribe
    public void onResponse(BaseRequest.OnFailedResponse onFailedResponse){
        metaCON.setVisibility(View.GONE);
        metaPB.setVisibility(View.GONE);
        postBTN.setEnabled(true);
        metaDataCON.setVisibility(View.GONE);
    }

    private void displayCounter(){
        urlImage = thumbnailImages.get(thumbnailIndex);
        Glide.with(getContext())
                .load(urlImage)
                .apply(new RequestOptions()

                        .placeholder(R.drawable.icon_image_placeholder)
                .error(R.drawable.icon_image_corrupted))
                .into(metaImageIV);
        counterTXT.setText(String.valueOf(thumbnailIndex + 1) + " of " + thumbnailImages.size());
        counterCON.setVisibility(thumbnailImages.size() < 2 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void result(File file) {
        this.file = file;
        cameraCon.setVisibility(View.VISIBLE);

        Glide.with(getContext())
                .load(file)
                .into(imageIV);
    }

    public void closeImage(){
        file = null;
        cameraCon.setVisibility(View.GONE);
    }
}
