package com.thingsilikeapp.android.fragment.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.activity.ProfileActivity;
import com.thingsilikeapp.android.adapter.CategoryOwnedRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.FragmentViewPagerAdapter;
import com.thingsilikeapp.android.adapter.SocialFeedRecyclerViewAdapter;
import com.thingsilikeapp.android.dialog.GemsSendDialog;
import com.thingsilikeapp.android.dialog.GroupDialog;
import com.thingsilikeapp.android.dialog.UnfollowDialog;
import com.thingsilikeapp.android.dialog.ViewImageDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.PasscodeVerificationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Share;
import com.thingsilikeapp.data.model.BlockUserItem;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Settings;
import com.thingsilikeapp.server.request.like.LikeRequest;
import com.thingsilikeapp.server.request.report.ReportRequest;
import com.thingsilikeapp.server.request.social.FollowRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.user.UserInfoRequest;
import com.thingsilikeapp.server.request.wishlist.BlockRequest;
import com.thingsilikeapp.server.request.wishlist.CategoryOwnedRequest;
import com.thingsilikeapp.server.request.wishlist.UserWishListRequest;
import com.thingsilikeapp.server.transformer.report.ReportTransformer;
import com.thingsilikeapp.server.transformer.social.FeedTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.server.transformer.wishlist.CategoryTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.CustomStaggeredGridLayoutManager;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.NumberFormatter;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;
import com.thingsilikeapp.vendor.android.java.decoration.SpacesItemDecoration;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.android.widget.CustomViewPager;
import com.thingsilikeapp.vendor.server.request.APIRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import icepick.State;

public class VisitingProfileFragment extends BaseFragment implements View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        View.OnTouchListener,
        UnfollowDialog.Callback,
        GroupDialog.Callback, GemsSendDialog.Callback, SocialFeedRecyclerViewAdapter.ClickListener,
        EndlessRecyclerViewScrollListener.Callback,CategoryOwnedRecycleViewAdapter.ClickListener  {

    public static final String TAG = VisitingProfileFragment.class.getName();

    private ProfileActivity profileActivity;
    private FragmentViewPagerAdapter fragmentViewPagerAdapter;
    private UserInfoRequest userInfoRequest;
    private SocialFeedRecyclerViewAdapter socialFeedRecyclerViewAdapter;
    private CategoryOwnedRecycleViewAdapter categoryOwnedRecycleViewAdapter;
    private UserWishListRequest userWishListRequest;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private CustomStaggeredGridLayoutManager staggeredGridLayoutManager;
    private LinearLayoutManager linearLayoutManager;
    private Timer timer;
    private TimerTask delayedThreadStartTask;
    private CategoryOwnedRequest categoryRequest;
    private ExchangeActivity exchangeActivity;

    @BindView(R.id.backButtonIV)                    View backButtonIV;
    @BindView(R.id.commandCON)                      View commandCON;
    @BindView(R.id.commandTXT)                      TextView commandTXT;
    @BindView(R.id.loadingPB)                       View loadingPB;

    @BindView(R.id.profileVP)                       CustomViewPager profileVP;
    @BindView(R.id.listViewBTN)                     View listViewBTN;
    @BindView(R.id.messageBTN)                      View messageBTN;
    @BindView(R.id.gridViewBTN)                     View gridViewBTN;

    @BindView(R.id.followersBTN)                    View followersBTN;
    @BindView(R.id.followingBTN)                    View followingBTN;
    @BindView(R.id.followersTXT)                    TextView followersTXT;
    @BindView(R.id.followingTXT)                    TextView followingTXT;
    @BindView(R.id.followersLBL)                    TextView followersLBL;
    @BindView(R.id.followingLBL)                    TextView followingLBL;
//    @BindView(R.id.birthDateTXT)                    TextView birthDateTXT;

    @BindView(R.id.imageCIV)                        ImageView imageCIV;
    @BindView(R.id.commonNameTXT)                   TextView commonNameTXT;
    @BindView(R.id.fullNameTXT)                     TextView fullNameTXT;
    @BindView(R.id.userNameTXT)                     TextView userNameTXT;
    @BindView(R.id.groupTXT)                        TextView groupTXT;
    @BindView(R.id.profileSRL)                      SwipeRefreshLayout profileSRL;

    @BindView(R.id.observableRecyclerView)          RecyclerView observableRecyclerView;
    @BindView(R.id.categoryRV)                      RecyclerView categoryRV;
    @BindView(R.id.placeHolderCON)                  View placeHolderCON;
    @BindView(R.id.loadingIV)                       View loadingIV;

    @State int userId;
    @State int visited_userId;
    @State int chat_id;
    @State String commonName;
    @State String visitedProfUrl;
    @State String visitedName;
    @State String username;


    public static VisitingProfileFragment newInstance(int userId) {
        VisitingProfileFragment profileFragment = new VisitingProfileFragment();
        profileFragment.userId = userId;
        return profileFragment;

    }

    public int flexibleSpaceHeight;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_visiting_profile;
    }

    @Override
    public void onViewReady() {
        profileActivity = (ProfileActivity) getContext();

        backButtonIV.setOnClickListener(this);
        imageCIV.setOnClickListener(this);
        commandTXT.setOnClickListener(this);

        commandTXT.setVisibility(View.GONE);

        messageBTN.setOnClickListener(this);

        setupPage();
        initSuggestionAPI();
        initFeedAPI();
        setupListView();
        setUpRView();
        initCateforyAPI();
        displayViewPager();
        gridViewBTN.setOnClickListener(this);
        Analytics.trackEvent("profile_VisitingPRofileFragment_onViewReady");
    }

    public void setupPage(){

        flexibleSpaceHeight = getResources().getDimensionPixelSize(R.dimen.profile_header);

        followersBTN.setOnTouchListener(this);
        followingBTN.setOnTouchListener(this);

//        listViewBTN.setOnTouchListener(this);
        gridViewBTN.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            switch (v.getId()){
                case R.id.followersBTN:
                    profileActivity.startSearchActivity(userId,commonName, "followers");
                    Analytics.trackEvent("profile_VisitingPRofileFragment_followersBTN");
                    break;
                case R.id.followingBTN:
                    profileActivity.startSearchActivity(userId, commonName, "following");
                    Analytics.trackEvent("profile_VisitingPRofileFragment_followingBTN");
                    break;
                case R.id.listViewBTN:
//                    listViewActive();
                    Analytics.trackEvent("profile_VisitingPRofileFragment_listViewBTN");
                    break;
                case R.id.gridViewBTN:
//                    gridViewActive();
                    Analytics.trackEvent("profile_VisitingPRofileFragment_gridViewBTN");
                    break;
            }
        }
        return false;
    }

    private void otherOptionClick(View view) {
        final UserItem userItem = (UserItem) view.getTag();

        PopupMenu popup = new PopupMenu(getContext(), view);
        popup.getMenuInflater().inflate(R.menu.user_menu, popup.getMenu());
        popup.getMenu().getItem(0).setVisible(userItem.social.data.is_following.equals("no"));
        popup.getMenu().getItem(1).setVisible(userItem.social.data.is_following.equals("yes"));

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.followBTN:
                    case R.id.unfollowBTN:
                        commandTXT.setVisibility(View.GONE);
                        userItem.is_loading_follow = true;
                        displayData(userItem);
                        commandButtonClick(userItem);
                        break;
                    case R.id.reportBTN:
                        reportUser(userItem);
                        break;
                    case R.id.blockBTN:
                        blockUser(userItem);
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    private void reportUser(final UserItem userItem){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to report " + StringFormatter.first(StringFormatter.firstWord(userItem.common_name)) + "?")
                .setNote("You are about to report this user.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Report")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportRequest reportRequest = new ReportRequest(getContext());
                        reportRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.TYPE, "user")
                                .addParameters(Keys.server.key.REFERENCE_ID, userItem.id)
                                .execute();


                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("profile_VisitingPRofileFragment_reportUser");
    }

    private void blockUser(final UserItem userItem){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to block " + StringFormatter.first(StringFormatter.firstWord(userItem.common_name)) + "?")
                .setNote("You are about to block this user.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Block")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                       Settings.getDefault().blockUser(getContext(),userItem.id)
                                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                               .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameter(Keys.server.key.TYPE, "user")
                                .addParameter(Keys.server.key.REFERENCE_ID, userItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("profile_VisitingPRofileFragment_blockUser");
    }

    private void commandButtonClick(UserItem userItem) {
        if (userItem.social.data.is_following.equals("yes")) {
            unFollowUser(userItem);
        } else {
            followUser(userItem);
        }
    }

    @Override
    public void onGroupSelect(String groupName, int id) {
        FollowRequest followRequest = new FollowRequest(getContext());
        followRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .addParameters(Keys.server.key.USER_ID, id)
                .addParameters(Keys.server.key.GROUP,groupName)
                .execute();
    }

    @Override
    public void onCancel(String groupName, int id) {
        loadingPB.setVisibility(View.GONE);
        commandCON.setVisibility(View.VISIBLE);
    }

    public void unFollowUser(UserItem userItem) {
        UnfollowDialog.newInstance(userItem.id, userItem.name, this).show(getChildFragmentManager(), UnfollowDialog.TAG);
    }

    public void followUser(UserItem userItem) {
        GroupDialog.newInstance(userItem, this).show(getFragmentManager(),GroupDialog.TAG);
    }

    @Override
    public void onAccept(int userID) {
        UnFollowRequest unFollowRequest = new UnFollowRequest(getContext());
        unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setDeviceRegID(profileActivity.getDeviceRegID())
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, userID)
                .execute();
    }

    @Override
    public void onCancel(int userID) {
        loadingPB.setVisibility(View.GONE);
        commandCON.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void displayData(UserItem userItem) {

        chat_id = Integer.parseInt(userItem.chat_id);
        visitedName = userItem.name;
        visitedProfUrl = userItem.getAvatar();
        username = userItem.username;
        visited_userId = userItem.id;

        Log.e("CHATID", String.valueOf(chat_id));
        Log.e("USECHATID", userItem.chat_id);
        Log.e("USERID", String.valueOf(userItem.id));

        commandTXT.setVisibility(View.VISIBLE);
        if(userItem.is_loading_follow){
            loadingPB.setVisibility(View.VISIBLE);
            commandCON.setVisibility(View.GONE);
        }
        else
            {
            loadingPB.setVisibility(View.GONE);
            commandCON.setVisibility(View.VISIBLE);
        }

        commonNameTXT.setText(userItem.common_name);
        fullNameTXT.setText(userItem.username);
        userNameTXT.setText(VerifiedUserBadge.getFormattedName(getContext(), userItem.name, userItem.is_verified, 14, true));

        Glide.with(getContext())
                .load(userItem.getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(imageCIV);

        if (userItem.social.data.is_following.equals("yes")) {
            commandTXT.setBackground(ActivityCompat.getDrawable(getContext(), R.drawable.bg_follow));
            commandTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
            commandTXT.setText("FOLLOWING");
            commandTXT.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        }
        else
            {
            commandTXT.setBackground(ActivityCompat.getDrawable(getContext(), R.drawable.bg_unfollow));
            commandTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
            commandTXT.setText("FOLLOW");
            commandTXT.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_add_blue,0,0,0);
        }

        commandTXT.setTag(userItem);

        if (userItem.social.data.is_following.equalsIgnoreCase("yes")){
            groupTXT.setVisibility(View.VISIBLE);
            groupTXT.setText(userItem.social.data.group);
        }else {
            groupTXT.setVisibility(View.GONE);
            groupTXT.setText("");
        }

//        if (userItem.info.data.birthdate.equals("null") || userItem.info.data.birthdate.equals("")){
//            birthDateTXT.setText("");
//            birthDateTXT.setVisibility(View.GONE);
//        }else {
//            switch (userItem.info.data.my_privacy) {
//                case "only_me":
//                    birthDateTXT.setText(birthdayFormat(userItem.info.data.birthdate));
//                    break;
//                case "month_day":
//                    birthDateTXT.setText(monthDayFormat(userItem.info.data.birthdate));
//                    break;
//                case "month_day_year":
//                    birthDateTXT.setText(birthdayFormat(userItem.info.data.birthdate));
//                    break;
//                }
//        }
//
//        if(userItem.is_birthday){
//            birthDateTXT.setOnClickListener(this);
//        }else{
//            birthDateTXT.setOnClickListener(null);
//        }

        followersTXT.setText(NumberFormatter.formatWithComma(userItem.statistics.data.followers));
        followingTXT.setText(NumberFormatter.formatWithComma(userItem.statistics.data.following));
        followersLBL.setText(userItem.statistics.data.followers == 1 ? "follower" : "followers");
    }

    private String birthdayFormat(String birthday){
        if(birthday == null){
            return "";
        }
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat newFormat = new SimpleDateFormat("MMM dd, yyyy");
        try {
            Date date = originalFormat.parse(birthday);
            return newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String monthDayFormat(String birthday){
        if(birthday == null){
            return "";
        }
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat newFormat = new SimpleDateFormat("MMM dd");
        try {
            Date date = originalFormat.parse(birthday);
            return newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void displayViewPager(){

        fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getChildFragmentManager());
        fragmentViewPagerAdapter.clearFragment();

//        fragmentViewPagerAdapter.addFragment(ILikeCategoryFragment.newInstance(userId));
        fragmentViewPagerAdapter.addFragment(ILikeFragment.newInstance(userId));

        profileVP.setPageMargin(10);
        profileVP.setPageMarginDrawable(null);
        profileVP.setAdapter(fragmentViewPagerAdapter);
        profileVP.setOffscreenPageLimit(2);
        profileVP.canSwipe(false);

        gridViewActive();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backButtonIV:
                profileActivity.onBackPressed();
                Analytics.trackEvent("profile_VisitingPRofileFragment_backButtonIV");
                break;
            case R.id.commandTXT:
                otherOptionClick(v);
                Analytics.trackEvent("profile_VisitingPRofileFragment_commandTXT");
                break;
            case R.id.followersBTN:
                if (v.getTag(R.id.followersBTN) != null){
                    profileActivity.startSearchActivity((int) v.getTag(R.id.followersBTN), commonName, "followers");
                    ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.ProfileActivity.EVENT,FacebookCustomLoggerMessage.ProfileActivity.FOLLOWERS);
                }
                Analytics.trackEvent("profile_VisitingPRofileFragment_followersBTN");
                break;
            case R.id.followingBTN:
                if (v.getTag(R.id.followingBTN) != null){
                    ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.ProfileActivity.EVENT,FacebookCustomLoggerMessage.ProfileActivity.FOLLOWING);
                    profileActivity.startSearchActivity((int) v.getTag(R.id.followingBTN), commonName, "following");

                }
                Analytics.trackEvent("profile_VisitingPRofileFragment_followingBTN");
                break;
            case R.id.imageCIV:
                ViewImageDialog.newInstance(visitedProfUrl,visitedName).show(getChildFragmentManager(), ViewImageDialog.TAG);
                Analytics.trackEvent("profile_VisitingPRofileFragment_imageCIV");
                break;
            case R.id.listViewBTN:
                Analytics.trackEvent("profile_VisitingPRofileFragment_listViewBTN");
            case R.id.messageBTN:
                profileActivity.startVisitMessageActivity("my_thread", chat_id, visited_userId, commonName);
                Analytics.trackEvent("profile_VisitingPRofileFragment_messageBTN");
                break;
            case R.id.gridViewBTN:
                GemsSendDialog.newInstance(this, username).show(getFragmentManager(), GemsSendDialog.TAG);
                Analytics.trackEvent("profile_VisitingPRofileFragment_gridViewBTN");
                break;
        }
    }

//    public void listViewActive(){
//        profileVP.setCurrentItem(0, false);
//        listViewBTN.setSelected(true);
//        gridViewBTN.setSelected(false);
//    }

    public void gridViewActive(){
        profileVP.setCurrentItem(0, false);
        profileVP.requestLayout();

//        listViewBTN.setSelected(false);
        gridViewBTN.setSelected(true);
    }

    private void initSuggestionAPI(){

        profileSRL.setColorSchemeResources(R.color.colorPrimary);
        profileSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        profileSRL.setOnRefreshListener(this);

        userInfoRequest = new UserInfoRequest(getContext());
        userInfoRequest
                .setDeviceRegID(profileActivity.getDeviceRegID())
                .setSwipeRefreshLayout(profileSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,statistics,social")
                .addParameters(Keys.server.key.USER_ID, userId)
                .showSwipeRefreshLayout(true);
    }

    private void refreshList(){
        clearUserData();
        userInfoRequest
                .showSwipeRefreshLayout(true)
                .execute();
        if (userWishListRequest != null) {
            userWishListRequest
                    .first();
            categoryRequest.first();
        }
        loadingIV.setVisibility(View.VISIBLE);
    }

    private void clearUserData(){
        commonNameTXT.setText("");
        followersTXT.setText("0");
        followingTXT.setText("0");
    }

    private void refreshData(){
        Fragment fragment = getCurrentFragment();
        if (fragment == null) {
            return;
        }

        if(fragment instanceof ILikeCategoryFragment){
            ((ILikeCategoryFragment) fragment).refreshList();
        }else if(fragment instanceof ILikeFragment){
            ((ILikeFragment) fragment).refreshList();
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        Glide.get(getContext()).clearMemory();
        if (timer != null) {
            timer.cancel();
        }
        super.onStop();
    }

    @Subscribe
    public void onResponse(UserInfoRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            displayData(userTransformer.userItem);
            commonName = userTransformer.userItem.common_name;
            Log.e("FFFFFFF", ">>>>> " + userTransformer.userItem.common_name);
//            refreshData();
        }
    }

    @Subscribe
    public void onResponse(ReportRequest.ServerResponse responseData) {
        ReportTransformer reportTransformer = responseData.getData(ReportTransformer.class);
        if (reportTransformer.status) {
            profileActivity.finish();
            ToastMessage.show(getContext(), reportTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getContext(), reportTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(FollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            displayData(userTransformer.userItem);
            UserData.updateStatistic(1);
        }
    }

    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            displayData(userTransformer.userItem);
            UserData.updateStatistic(-1);
        }
    }

    private Fragment getCurrentFragment() {
        if (profileVP != null){
            return fragmentViewPagerAdapter.getItem(profileVP.getCurrentItem());
        }
        return new Fragment();
    }

    @Override
    public void onSuccess() {
//        refreshData();
        refreshList();
    }

    public void setupListView() {
        socialFeedRecyclerViewAdapter = new SocialFeedRecyclerViewAdapter(getContext());
        socialFeedRecyclerViewAdapter.setEnableUserClick(false);
        socialFeedRecyclerViewAdapter.setFeedView(SocialFeedRecyclerViewAdapter.GRID);
        staggeredGridLayoutManager = new CustomStaggeredGridLayoutManager(2, OrientationHelper.VERTICAL);
        staggeredGridLayoutManager.setScrollEnabled(true);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(staggeredGridLayoutManager, this);
        observableRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        observableRecyclerView.setAdapter(socialFeedRecyclerViewAdapter);
        observableRecyclerView.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.social_feed_spacing)));
        socialFeedRecyclerViewAdapter.setOnItemClickListener(this);
        observableRecyclerView.setNestedScrollingEnabled(false);
        observableRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

    }

    private void setUpRView(){
        categoryOwnedRecycleViewAdapter = new CategoryOwnedRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        categoryRV.setLayoutManager(linearLayoutManager);
//        categoryOwnedRecycleViewAdapter.setNewData(getDefaultData());
        categoryRV.setAdapter(categoryOwnedRecycleViewAdapter);
        categoryOwnedRecycleViewAdapter.setOnItemClickListener(this);
    }

    private void initFeedAPI() {
        userWishListRequest = new UserWishListRequest(getContext());
        userWishListRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                .addParameters(Keys.server.key.INCLUDE, "image,owner.social")
                .addParameters(Keys.server.key.USER_ID, userId)
                .setPerPage(20);
        userWishListRequest.first();
    }


    private void initCateforyAPI(){
        categoryRequest = new CategoryOwnedRequest(getContext());
        categoryRequest
                .addParameters(Keys.server.key.USER_ID, userId)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(100);
        categoryRequest.first();
    }

    @Subscribe
    public void onResponse(UserWishListRequest.ServerResponse responseData) {
        FeedTransformer feedTransformer = responseData.getData(FeedTransformer.class);
        if (feedTransformer.status) {

            userWishListRequest.setHasMorePage(feedTransformer.has_morepages);
            if (responseData.isNext()) {
                socialFeedRecyclerViewAdapter.addNewData(feedTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                socialFeedRecyclerViewAdapter.setNewData(feedTransformer.data);
            }

//            if (userId == UserData.getUserId()) {
//                FeedTransformer cache = new FeedTransformer();
//                cache.data = socialFeedRecyclerViewAdapter.getData();
//                UserData.insert(UserData.MY_FEED_CACHE, new Gson().toJson(cache));
//            }
        }
        loadingIV.setVisibility(View.GONE);
        observableRecyclerView.setVisibility(socialFeedRecyclerViewAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE);
        placeHolderCON.setVisibility(socialFeedRecyclerViewAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Subscribe
    public void onResponse(CategoryOwnedRequest.ServerResponse responseData) {
        CategoryTransformer categoryTransformer = responseData.getData(CategoryTransformer.class);
        if (categoryTransformer.status) {
            if (responseData.isNext()) {
                categoryOwnedRecycleViewAdapter.addNewData(categoryTransformer.data);
            }else{
                CategoryItem rewardCategoryItem = new CategoryItem();
                rewardCategoryItem.id = 0;
                rewardCategoryItem.title = "All";
                rewardCategoryItem.image = "https://www.catster.com/wp-content/uploads/2017/08/A-fluffy-cat-looking-funny-surprised-or-concerned.jpg";
//                rewardCategoryItem.selected = true;
                categoryTransformer.data.add(0, rewardCategoryItem);
                categoryOwnedRecycleViewAdapter.setNewData(categoryTransformer.data);
            }
        }
    }

    @Override
    public void onItemClick(CategoryItem categoryItem) {
        if (categoryItem.title.equalsIgnoreCase("all")){
            observableRecyclerView.setVisibility(observableRecyclerView.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            initFeedAPI();
        }else{
            observableRecyclerView.setVisibility(observableRecyclerView.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            loadingIV.setVisibility(View.VISIBLE);
            userWishListRequest = new UserWishListRequest(getContext());
            userWishListRequest
                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                    .addParameters(Keys.server.key.INCLUDE, "image,owner.social")
                    .addParameters(Keys.server.key.CATEGORY, categoryItem.title)
                    .addParameters(Keys.server.key.USER_ID, userId)
                    .setPerPage(10);
            userWishListRequest.first();
        }
    }

    @Override
    public void onItemLongClick(CategoryItem categoryItem) {

    }

    @Override
    public void onItemClick(WishListItem wishListItem) {
        if (wishListItem.owner.data != null) {
            ((RouteActivity) getContext()).startItemActivity(wishListItem.id, wishListItem.owner.data.id, "i_like");
        }
    }

    @Override
    public void onCommentClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startCommentActivity(wishListItem.id, wishListItem.title);
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        ((RouteActivity) getContext()).startProfileActivity(userItem.id);
    }

    @Override
    public void onGrabClick(WishListItem wishListItem) {
        String data = new Gson().toJson(wishListItem);
        ((RouteActivity) getContext()).startWishListActivity(data, "create_grab_other");
    }

    @Override
    public void onHideClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to hide?")
                .setNote("You will no longer see this post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Hide")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BlockRequest blockRequest = new BlockRequest(getContext());
                        blockRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("profile_VisitingPRofileFragment_onHideClick");
    }

    @Override
    public void onUnfollowClick(final WishListItem wishListItem) {
        UnfollowDialog.newInstance(wishListItem.owner.data.id, wishListItem.owner.data.name, new UnfollowDialog.Callback() {
            @Override
            public void onAccept(int userID) {
                UnFollowRequest unFollowRequest = new UnFollowRequest(getContext());
                unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                        .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                        .addParameters(Keys.server.key.USER_ID, wishListItem.owner.data.id)
                        .execute();
            }

            @Override
            public void onCancel(int userID) {

            }
        }).show(((BaseActivity) getContext()).getSupportFragmentManager(), UnfollowDialog.TAG);
        Analytics.trackEvent("profile_VisitingPRofileFragment_onUnfollowClick");
    }

    @Override
    public void onReportClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to report?")
                .setNote("You are about to report a post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Report")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportRequest reportRequest = new ReportRequest(getContext());
                        reportRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.TYPE, "wishlist")
                                .addParameters(Keys.server.key.REFERENCE_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("profile_VisitingPRofileFragment_onReportClick");
    }

    public void shareTheApp(WishListItem wishListItem) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, wishListItem.owner.data.username + " posted " + wishListItem.title + " on things I like for " + wishListItem.category + ". Check out what everyone likes and discover your modern wishlist app");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Share.URL + "/share/i/" + wishListItem.id);
        startActivity(Intent.createChooser(sharingIntent, "Share Things I like"));
        Analytics.trackEvent("profile_VisitingPRofileFragment_shareTheApp");
    }


    @Override
    public void onShareClick(WishListItem wishListItem) {
        shareTheApp(wishListItem);
    }

    @Override
    public void onHeartClick(WishListItem wishListItem) {
        LikeRequest likeRequest = new LikeRequest(getContext());
        likeRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")`
                .addParameters(Keys.server.key.INCLUDE, "image,owner.social")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                .execute();
    }

    @Override
    public void onRepostClick(WishListItem wishListItem) {

    }

    @Override
    public void onRepostUsersClick(WishListItem wishListItem) {

    }

    @Override
    public void onHeaderClick(EventItem eventItem) {

    }

    @Override
    public void onHeaderShowClick(EventItem eventItem) {

    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        timer = new Timer();
        delayedThreadStartTask = new TimerTask() {
            @Override
            public void run() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (userWishListRequest.hasMorePage()) {
                                    userWishListRequest.nextPage();
                                }
                            }
                        });
                    }
                }).start();
            }
        };
        timer.schedule(delayedThreadStartTask, 5000);
        loadingIV.setVisibility(View.VISIBLE);
    }
}
