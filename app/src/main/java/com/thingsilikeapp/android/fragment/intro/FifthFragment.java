package com.thingsilikeapp.android.fragment.intro;

import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class FifthFragment extends BaseFragment {

    public static final String TAG = FifthFragment.class.getName();

    public static FifthFragment newInstance() {
        FifthFragment fragment = new FifthFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_intro_e;
    }

    @Override
    public void onViewReady() {

    }
}
