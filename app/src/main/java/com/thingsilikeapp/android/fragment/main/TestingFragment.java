package com.thingsilikeapp.android.fragment.main;

import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class TestingFragment extends BaseFragment {

    public static final String TAG = TestingFragment.class.getName();

    @BindView(R.id.textviewTxt) TextView textViewTxt;

    public static TestingFragment newInstance() {
        TestingFragment fragment = new TestingFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_template;
    }

    @Override
    public void onViewReady() {
        textViewTxt.setText("Hello fragment");
    }
}
