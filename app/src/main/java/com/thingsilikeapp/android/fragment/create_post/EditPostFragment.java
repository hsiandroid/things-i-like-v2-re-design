package com.thingsilikeapp.android.fragment.create_post;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.CreatePostActivity;
import com.thingsilikeapp.android.adapter.FilterRecycleViewAdapter;
import com.thingsilikeapp.android.filter.ThumbnailsAdapter;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.decoration.SpacesItemDecoration;
import com.zomato.photofilters.FilterPack;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.utils.ThumbnailCallback;
import com.zomato.photofilters.utils.ThumbnailItem;
import com.zomato.photofilters.utils.ThumbnailsManager;

import android.graphics.Bitmap;

import com.thingsilikeapp.vendor.android.java.BitmapUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

import static com.facebook.FacebookSdk.getApplicationContext;

public class EditPostFragment extends BaseFragment  implements ThumbnailsAdapter.ThumbnailsAdapterListener, View.OnClickListener {

    static
    {
        System.loadLibrary("NativeImageProcessor");
    }

    public static final String TAG = EditPostFragment.class.getName();

    private CreatePostActivity createPostActivity;
    private LinearLayoutManager linearLayoutManager;
    private ThumbnailsAdapter thumbnailsAdapter;
    List<ThumbnailItem> thumbnailItemList;

    @BindView(R.id.titleTXT)    TextView textViewTxt;
    @BindView(R.id.imageIV)     ImageView imageIV;
    @BindView(R.id.filterRV)    RecyclerView filterRV;
    @BindView(R.id.nextBTN)     TextView nextBTN;
    @BindView(R.id.mainBackButtonIV)	View mainBackButtonIV;

    @State Bitmap file;
    @State Bitmap imageBitMap;
    @State Bitmap filteredImage;
    @State File filteredFile;

    public static EditPostFragment newInstance(Bitmap file) {
        EditPostFragment fragment = new EditPostFragment();
        fragment.file = file;
        fragment.imageBitMap = file;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_post;
    }

    @Override
    public void onViewReady() {
        createPostActivity = (CreatePostActivity) getContext();
        textViewTxt.setText("Edit");
        imageIV.setImageBitmap(file);
        filteredImage = file;
        setUpRV();
        nextBTN.setOnClickListener(this);
        mainBackButtonIV.setOnClickListener(this);
    }

    public void setUpRV(){

        thumbnailItemList = new ArrayList<>();
        thumbnailsAdapter = new ThumbnailsAdapter(getActivity(), thumbnailItemList, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        filterRV.setLayoutManager(mLayoutManager);
        filterRV.setItemAnimator(new DefaultItemAnimator());
        int space = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1,
                getResources().getDisplayMetrics());
        filterRV.addItemDecoration(new SpacesItemDecoration(space));
        filterRV.setAdapter(thumbnailsAdapter);

        prepareThumbnail(imageBitMap);

    }

    public void prepareThumbnail(final Bitmap bitmap) {
        Runnable r = new Runnable() {
            public void run() {
                Bitmap thumbImage;

                    thumbImage = Bitmap.createScaledBitmap(bitmap, 100, 100, false);

                if (thumbImage == null)
                    return;

                ThumbnailsManager.clearThumbs();
                thumbnailItemList.clear();

                // add normal bitmap first
                ThumbnailItem thumbnailItem = new ThumbnailItem();
                thumbnailItem.image = thumbImage;
                thumbnailItem.filterName = "Normal";
                ThumbnailsManager.addThumb(thumbnailItem);

                List<Filter> filters = FilterPack.getFilterPack(getActivity());

                for (Filter filter : filters) {
                    ThumbnailItem tI = new ThumbnailItem();
                    tI.image = thumbImage;
                    tI.filter = filter;
                    tI.filterName = filter.getName();
                    ThumbnailsManager.addThumb(tI);
                }

                thumbnailItemList.addAll(ThumbnailsManager.processThumbs(getActivity()));

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        thumbnailsAdapter.notifyDataSetChanged();
                    }
                });
            }
        };

        new Thread(r).start();
    }


    @Override
    public void onFilterSelected(Filter filter) {
        // preview filtered image
        if (!imageBitMap.isRecycled()){
            filteredImage = imageBitMap.copy(Bitmap.Config.ARGB_8888, true);

        }
        imageIV.setImageBitmap(filter.processFilter(filteredImage));

    }

    private File getImageFile(Bitmap bitmap) {
        String imageFileName = "LykaCacheImage";
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    createImageDir()      /* directory */
            );

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
            byte[] bitmapData = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(image);
            fos.write(bitmapData);
            fos.flush();
            fos.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    private File createImageDir(){
        File storageDir = new File(Environment.getExternalStorageDirectory(), "Android/media/com.thingsilikeapp");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        return storageDir;
    }

    private class CheckTypesTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog asyncDialog = new ProgressDialog(getContext());
        String typeStatus;


        @Override
        protected void onPreExecute() {
            //set message of the dialog
            asyncDialog.setMessage("Processing...");
            //show dialog
            asyncDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {

           filteredFile = getImageFile(filteredImage);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //hide the dialog
            asyncDialog.dismiss();
            createPostActivity.openSendPost(filteredFile);
            super.onPostExecute(result);
        }

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.mainBackButtonIV:
                createPostActivity.onBackPressed();
                break;
            case R.id.nextBTN:
                new CheckTypesTask().execute();
                break;
        }
    }
}
