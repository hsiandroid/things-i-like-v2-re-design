package com.thingsilikeapp.android.fragment.registration;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.NewRegistrationActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.server.request.ValidateRequest;
import com.thingsilikeapp.server.transformer.ValidateTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManagerRegister;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class RegisterGFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = RegisterGFragment.class.getName();

    private NewRegistrationActivity registrationActivity;

    @BindView(R.id.nextBTN)             TextView nextBTN;
    @BindView(R.id.backBTN)             TextView backBTN;
    @BindView(R.id.passwordET)          EditText passwordET;
    @BindView(R.id.showPasswordBTN)     ImageView showPasswordBTN;

    @State String username;
    @State String gender;
    @State String birthdate;
    @State String name;
    @State String type;
    @State String email;
    @State String country;
    @State String contry_code;

    public static RegisterGFragment newInstance(String username, String gender, String birthdate, String name, String email, String type, String country, String code) {
        RegisterGFragment fragment = new RegisterGFragment();
        fragment.username = username;
        fragment.gender = gender;
        fragment.birthdate = birthdate;
        fragment.name = name;
        fragment.type = type;
        fragment.email = email;
        fragment.country  = country;
        fragment.contry_code = code;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_registration_g;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (NewRegistrationActivity) getContext();
        nextBTN.setOnClickListener(this);
        backBTN.setOnClickListener(this);
//        PasswordEditTextManager.addShowPassword(getContext(), passwordET, showPasswordBTN);
        PasswordEditTextManagerRegister.addShowPassword(getContext(), passwordET, showPasswordBTN);

        Analytics.trackEvent("registration_RegisterGFragment_onViewReady");
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.nextBTN:
                if (passwordET.length() < 6){
                    ToastMessage.show(getContext(), "Password must be atleast 6 characters", ToastMessage.Status.FAILED);
                }else{

                    validate();
                }
                Analytics.trackEvent("registration_RegsterGFragment_nextBTN");
                break;
            case R.id.backBTN:
                registrationActivity.startLandingActivity();
                Analytics.trackEvent("registration_RegsterGFragment_backBTN");
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(ValidateRequest.ServerResponse responseData) {
        ValidateTransformer validateTransformer = responseData.getData(ValidateTransformer.class);
        if(validateTransformer.status){
            registrationActivity.openHFragment(username,gender,birthdate,name,email,passwordET.getText().toString(), type, country, contry_code);
        }else{
            ToastMessage.show(getContext(), validateTransformer.msg, ToastMessage.Status.FAILED);
            if(validateTransformer.hasRequirements()){
                ErrorResponseManger.first(passwordET, validateTransformer.requires.password);
            }
        }
    }

    private void validate(){
        ValidateRequest createCommentRequest = new ValidateRequest(getContext());
        createCommentRequest
                .showNoInternetConnection(false)
                .addParameters(Keys.server.key.FIELD, "password")
                .addParameters(Keys.server.key.VALUE, passwordET.getText().toString())
                .execute();
    }
}
