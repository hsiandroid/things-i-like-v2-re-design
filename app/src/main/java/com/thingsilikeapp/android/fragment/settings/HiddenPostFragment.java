package com.thingsilikeapp.android.fragment.settings;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;

import com.thingsilikeapp.android.adapter.HiddenPostRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.UnhideDialog;
import com.thingsilikeapp.data.model.HidePostItem;
import com.thingsilikeapp.server.HidePost;
import com.thingsilikeapp.server.Settings;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class HiddenPostFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        EndlessRecyclerViewScrollListener.Callback, HiddenPostRecycleViewAdapter.ClickListener, UnhideDialog.Callback{

    public static final String TAG = HiddenPostFragment.class.getName();

    private SettingsActivity settingsActivity;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;
    private HiddenPostRecycleViewAdapter hiddenPostRecycleViewAdapter;

    private APIRequest apiRequest;

    @BindView(R.id.hiddenRV)               RecyclerView hiddenRV;
    @BindView(R.id.hiddenSRL)              SwipeRefreshLayout hiddenSRL;



    public static HiddenPostFragment newInstance() {
        HiddenPostFragment hiddenPostFragment = new HiddenPostFragment();
        return hiddenPostFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_hidden_post;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle("Hidden Posts");
        setupBlockListView();
        initSuggestionAPI();
        hiddenSRL.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void setupBlockListView() {
        hiddenPostRecycleViewAdapter = new HiddenPostRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        hiddenRV.setLayoutManager(linearLayoutManager);
        hiddenRV.setAdapter(hiddenPostRecycleViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager,
                this);
        hiddenRV.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        hiddenRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        hiddenRV.setLayoutManager(linearLayoutManager);
        hiddenPostRecycleViewAdapter.setOnItemClickListener(this);
    }

    private void refreshList(){
//        endlessRecyclerViewScrollListener.reset();
       apiRequest =  HidePost.getDefault().getHidePost(getContext(), hiddenSRL);
       apiRequest.first();
//        followersRequest
//                .showSwipeRefreshLayout(true)
//                .clearParameters()
//                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
//                .addParameters(Keys.server.key.USER_ID, userID)
//                .addParameters(Keys.server.key.KEYWORD, searchET.getText().toString())
//                .first();
    }

    private void initSuggestionAPI(){
        hiddenSRL.setColorSchemeResources(R.color.colorPrimary);
        hiddenSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        hiddenSRL.setOnRefreshListener(this);
    }

    @Subscribe
    public void onResponse(HidePost.AllOrderResponse colletionResponse) {
        CollectionTransformer<HidePostItem> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (colletionResponse.isNext()) {
                hiddenPostRecycleViewAdapter.addNewData(collectionTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                hiddenPostRecycleViewAdapter.setNewData(collectionTransformer.data);
            }
        }
        Log.e("EEEEEEEE", ">>>>>" + collectionTransformer.data);

    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onItemClick(HidePostItem hidePostItem) {

    }

    @Override
    public void onUnhideClick(HidePostItem hidePostItem) {
        UnhideDialog.newInstance(hidePostItem, this).show(getFragmentManager(), TAG);
        refreshList();
    }

    @Override
    public void onItemLongClick(HidePostItem hidePostItem) {

    }

    @Override
    public void onSuccess() {
        refreshList();
    }
}
