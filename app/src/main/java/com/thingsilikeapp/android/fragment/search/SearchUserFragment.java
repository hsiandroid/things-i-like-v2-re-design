package com.thingsilikeapp.android.fragment.search;

import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SearchActivity;
import com.thingsilikeapp.android.adapter.FragmentViewPagerAdapter;
import com.thingsilikeapp.android.fragment.search.users.ContactsFragment;
import com.thingsilikeapp.android.fragment.search.users.SuggestionFragment;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class SearchUserFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = SearchUserFragment.class.getName();

    private SearchActivity searchActivity;

    private FragmentViewPagerAdapter fragmentViewPagerAdapter;

    @BindView(R.id.suggestedUserBTN)                TextView suggestedUserBTN;
    @BindView(R.id.fbFriendsBTN)                    TextView fbFriendsBTN;
    @BindView(R.id.contactsBTN)                     TextView contactsBTN;
    @BindView(R.id.searchVP)                        ViewPager searchVP;

    public static SearchUserFragment newInstance() {
        SearchUserFragment searchUserFragment = new SearchUserFragment();
        return searchUserFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_search_user;
    }

    @Override
    public void onViewReady() {
        searchActivity = (SearchActivity) getActivity();
        searchActivity.setTitle(getString(R.string.people_title));
        searchActivity.showSearchContainer(false);

        suggestedUserBTN.setOnClickListener(this);
        fbFriendsBTN.setOnClickListener(this);
        contactsBTN.setOnClickListener(this);

        setupViewPager();

    }

    private void setupViewPager(){
        fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getChildFragmentManager());
        fragmentViewPagerAdapter.addFragment(SuggestionFragment.newInstance());
        fragmentViewPagerAdapter.addFragment(ContactsFragment.newInstance());
//        fragmentViewPagerAdapter.addFragment(FacebookFriendsFragment.newInstance());
        searchVP.setPageMargin(20);
        searchVP.setPageMarginDrawable(null);
        searchVP.setAdapter(fragmentViewPagerAdapter);
        searchVP.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        suggestedUserBTN.setSelected(true);
                        fbFriendsBTN.setSelected(false);
                        contactsBTN.setSelected(false);
                        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
                        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        break;
                    case 1:
                        suggestedUserBTN.setSelected(false);
                        fbFriendsBTN.setSelected(false);
                        contactsBTN.setSelected(true);
                        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        searchVP.setOffscreenPageLimit(2);
        suggestedUserActive();

    }

    public void suggestedUserActive(){
        suggestedUserBTN.setSelected(true);
        fbFriendsBTN.setSelected(false);
        contactsBTN.setSelected(false);
        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        searchVP.post(new Runnable() {
            @Override
            public void run() {
                if(searchVP != null){
                    searchVP.setCurrentItem(0);
                }
            }
        });
    }

    public void contactsActive(){
        suggestedUserBTN.setSelected(false);
        fbFriendsBTN.setSelected(false);
        contactsBTN.setSelected(true);
        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
        searchVP.post(new Runnable() {
            @Override
            public void run() {
                if(searchVP != null){
                    searchVP.setCurrentItem(1);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.suggestedUserBTN:
                suggestedUserActive();
                break;
            case R.id.fbFriendsBTN:
                break;
            case R.id.contactsBTN:
                contactsActive();
                break;
        }
    }
}
