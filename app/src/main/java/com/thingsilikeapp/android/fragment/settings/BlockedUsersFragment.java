package com.thingsilikeapp.android.fragment.settings;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;
import com.thingsilikeapp.android.adapter.BlockUserRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.SettingDialog;
import com.thingsilikeapp.data.model.BlockUserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Settings;
import com.thingsilikeapp.server.transformer.SingleTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class BlockedUsersFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        EndlessRecyclerViewScrollListener.Callback,BlockUserRecycleViewAdapter.ClickListener, SettingDialog.Callback{

    public static final String TAG = BlockedUsersFragment.class.getName();

    private SettingsActivity settingsActivity;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;
    private BlockUserRecycleViewAdapter blockUserRecycleViewAdapter;

    private APIRequest apiRequest;

    @BindView(R.id.blockedRV)               RecyclerView blockedRV;
    @BindView(R.id.blockedSRL)              SwipeRefreshLayout blockedSRL;



    public static BlockedUsersFragment newInstance() {
        BlockedUsersFragment blockedUsersFragment = new BlockedUsersFragment();
        return blockedUsersFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_block_users;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle("Blocked Users");
        setupBlockListView();
        initSuggestionAPI();
        blockedSRL.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void setupBlockListView() {
        blockUserRecycleViewAdapter = new BlockUserRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        blockedRV.setLayoutManager(linearLayoutManager);
        blockedRV.setAdapter(blockUserRecycleViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager,
                this);
        blockedRV.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        blockedRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        blockedRV.setLayoutManager(linearLayoutManager);
        blockUserRecycleViewAdapter.setOnItemClickListener(this);

    }

    private void refreshList(){
//        endlessRecyclerViewScrollListener.reset();
        apiRequest =  Settings.getDefault().getBlockUser(getContext(), blockedSRL);
        apiRequest.first();


//        followersRequest
//                .showSwipeRefreshLayout(true)
//                .clearParameters()
//                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
//                .addParameters(Keys.server.key.USER_ID, userID)
//                .addParameters(Keys.server.key.KEYWORD, searchET.getText().toString())
//                .first();
    }

    private void initSuggestionAPI(){
        blockedSRL.setColorSchemeResources(R.color.colorPrimary);
        blockedSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        blockedSRL.setOnRefreshListener(this);
    }

    @Subscribe
    public void onResponse(Settings.AllOrderResponse colletionResponse) {
        CollectionTransformer<BlockUserItem> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (colletionResponse.isNext()) {
                blockUserRecycleViewAdapter.addNewData(collectionTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                blockUserRecycleViewAdapter.setNewData(collectionTransformer.data);
            }
        }

    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onItemClick(BlockUserItem blockUserItem) {

    }

    @Override
    public void onUnblockClick(BlockUserItem blockUserItem) {
        SettingDialog.newInstance(blockUserItem, this).show(getFragmentManager(), TAG);
        refreshList();
    }

    @Override
    public void onItemLongClick(BlockUserItem blockUserItem) {

    }

    @Override
    public void onSuccess() {
        refreshList();
    }
}