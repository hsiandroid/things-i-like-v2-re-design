package com.thingsilikeapp.android.fragment.search.users;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.adapter.ContactsRecycleViewAdapter;
import com.thingsilikeapp.config.App;
import com.thingsilikeapp.data.model.ContactsItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;

import org.json.JSONObject;

import butterknife.BindView;
import icepick.State;

public class FacebookFriendsFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        ContactsRecycleViewAdapter.ClickListener,
        View.OnClickListener{

    public static final String TAG = FacebookFriendsFragment.class.getName();

    private static final int PERMISSION_READ_CONTACTS = 102;

    private ContactsRecycleViewAdapter contactsRecycleViewAdapter;

    @State boolean selectAll = false;

    @BindView(R.id.topUserEHLV)                 RecyclerView topUserEHLV;
    @BindView(R.id.suggestionSRL)               MultiSwipeRefreshLayout suggestionSRL;
    @BindView(R.id.suggestionCON)               View suggestionCON;
    @BindView(R.id.loadMorePB)                  View loadMorePB;
    @BindView(R.id.searchET)                    EditText searchET;
    @BindView(R.id.searchBTN)                   View searchBTN;
    @BindView(R.id.placeHolderCON)              View placeHolderCON;
    @BindView(R.id.multiSelectCON)              View multiSelectCON;
    @BindView(R.id.selectAllBTN)                RadioButton selectAllBTN;
    @BindView(R.id.selectAllTXT)                View selectAllTXT;
    @BindView(R.id.cancelBTN)                   View cancelBTN;
    @BindView(R.id.commandTXT)                  View commandTXT;

    public static FacebookFriendsFragment newInstance() {
        FacebookFriendsFragment contactsFragment = new FacebookFriendsFragment();
        return contactsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_contacts;
    }

    @Override
    public void onViewReady() {
        setupContactListView();
        initSearch();
        setSwipeRefresh();
        initFacebook();
    }

    private void setSwipeRefresh(){
        cancelBTN.setOnClickListener(this);
        commandTXT.setOnClickListener(this);
        selectAllTXT.setOnClickListener(this);

        selectAllBTN.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    contactsRecycleViewAdapter.selectAll();
                }else{
                    contactsRecycleViewAdapter.unSelectAll();
                }
//                compoundButton.setChecked(!b);
            }
        });

        suggestionSRL.setColorSchemeResources(R.color.colorPrimary);
        suggestionSRL.setSwipeableChildren(R.id.topUserEHLV, R.id.placeHolderCON);
        suggestionSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        suggestionSRL.setOnRefreshListener(FacebookFriendsFragment.this);
    }

    private void initFacebook(){
        AccessToken token = AccessToken.getCurrentAccessToken();
        GraphRequest graphRequest = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {

                Log.e("response", ">>>here" + jsonObject);
//                try {
//                    JSONArray jsonArrayFriends = jsonObject.getJSONObject("friendlist").getJSONArray("data");
//                    JSONObject friendlistObject = jsonArrayFriends.getJSONObject(0);
//                    String friendListID = friendlistObject.getString("id");
//                    requestList(friendListID);
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
            }
        });
        Bundle param = new Bundle();
        param.putString("fields", "friendlist,members");
        graphRequest.setParameters(param);
        graphRequest.executeAsync();
    }

    private void requestList(String friendListId) {
        final String graphPath = "/"+friendListId+"/members/";
        AccessToken token = AccessToken.getCurrentAccessToken();
        GraphRequest request = new GraphRequest(token, graphPath, null, HttpMethod.GET, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                Log.e("response", ">>>" + graphResponse.getJSONObject());

//                JSONObject object = graphResponse.getJSONObject();
//                try {
//                    JSONArray arrayOfUsersInFriendList= object.getJSONArray("data");
//                /* Do something with the user list */
//                /* ex: get first user in list, "name" */
//                    JSONObject user = arrayOfUsersInFriendList.getJSONObject(0);
//                    String usersName = user.getString("name");
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
            }
        });
        Bundle param = new Bundle();
        param.putString("fields", "name");
        request.setParameters(param);
        request.executeAsync();
    }

    private void displayData() {
        suggestionSRL.setRefreshing(false);

        final String graphPath = "/"+ UserData.getUserItem().fb_id+"/members/";
        AccessToken token = AccessToken.getCurrentAccessToken();
        GraphRequest request = new GraphRequest(token, graphPath, null, HttpMethod.GET, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                Log.e("response", ">>>" + graphResponse.getJSONObject().toString());
//                try {
//                    JSONObject object = graphResponse.getJSONObject();
//                    JSONArray arrayOfUsersInFriendList = object.getJSONArray("data");
//                /* Do something with the user list */
//                /* ex: get first user in list, "name" */
//                    JSONObject user = arrayOfUsersInFriendList.getJSONObject(0);
//                    String usersName = user.getString("name");
//                    Log.e("name", ">>>" + usersName);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
            }
        });
        Bundle param = new Bundle();
        param.putString("fields", "name");
        request.setParameters(param);
        request.executeAsync();

//        if(PermissionChecker.checkPermissions(getContext(), Manifest.permission.READ_CONTACTS, PERMISSION_READ_CONTACTS)){
//            new AsyncTask<Object, Object, List<ContactsItem>>(){
//                @Override
//                protected void onPreExecute() {
//                    super.onPreExecute();
//                    suggestionSRL.setRefreshing(true);
//                }
//
//                @Override
//                protected List<ContactsItem> doInBackground(Object... objects) {
//                    List<ContactsItem> contactsItems = new ArrayList();
//                    ContactsItem contactsItem;
//                    ContentResolver contentResolver = getContext().getContentResolver();
//                    Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
//                    if (cursor.getCount() > 0) {
//                        while (cursor.moveToNext()) {
//                            int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
//                            if (hasPhoneNumber > 0) {
//                                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
//                                Cursor phoneCursor = contentResolver.query(
//                                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                                        null,
//                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
//                                        new String[]{id},
//                                        null);
//
//                                contactsItem = new ContactsItem();
//                                contactsItem.name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//                                contactsItem.image = cursor .getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
//
//                                if (phoneCursor.moveToNext()) {
//                                    contactsItem.number = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                                }
//
//                                phoneCursor.close();
//                                contactsItems.add(contactsItem);
//                            }
//                        }
//                    }
//                    return contactsItems;
//                }
//
//                @Override
//                protected void onPostExecute(List<ContactsItem> contactsItems) {
//                    super.onPostExecute(contactsItems);
//                    if(suggestionSRL != null){
//                        suggestionSRL.setRefreshing(false);
//                        multiSelectCON.setVisibility(View.GONE);
//                    }
//                    contactsRecycleViewAdapter.setNewData(contactsItems);
//                    contactsRecycleViewAdapter.setMultipleSelect(false);
//                }
//            }.execute();
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupContactListView() {
        contactsRecycleViewAdapter = new ContactsRecycleViewAdapter(getContext());
        topUserEHLV.setLayoutManager(new LinearLayoutManager(getContext()));
        topUserEHLV.setAdapter(contactsRecycleViewAdapter);
        contactsRecycleViewAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_READ_CONTACTS) {
            if(((RouteActivity)getContext()).isAllPermissionResultGranted(grantResults)){
               displayData();
            }
        }
    }

    public void initSearch(){
        if(searchET != null){
            searchET.setHint("Search People...");
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    contactsRecycleViewAdapter.getFilter().filter(s);
                    if (contactsRecycleViewAdapter.getItemCount() == 0){
                        placeHolderCON.setVisibility(View.VISIBLE);
                        topUserEHLV.setVisibility(View.GONE);
                    }else {
                        placeHolderCON.setVisibility(View.GONE);
                        topUserEHLV.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void afterTextChanged(final Editable s) {
                    contactsRecycleViewAdapter.getFilter().filter(s);
                    if (contactsRecycleViewAdapter.getItemCount() == 0){
                        placeHolderCON.setVisibility(View.VISIBLE);
                        topUserEHLV.setVisibility(View.GONE);
                    }else {
                        placeHolderCON.setVisibility(View.GONE);
                        topUserEHLV.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }

    private void refreshList(){
        displayData();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onInviteClick(ContactsItem contactsItem) {
        sendSMS(contactsItem.number);
    }

    @Override
    public void onItemLongClick(int position) {
        contactsRecycleViewAdapter.setMultipleSelect(true);
        contactsRecycleViewAdapter.setItemSelected(position, true);
        showMultiSelectOption(true);
    }

    @Override
    public void onItemSelect(int position) {

    }

    private void showMultiSelectOption(boolean show){
        multiSelectCON.setVisibility(show ? View.VISIBLE : View.GONE);
        selectAll = false;
        selectAllBTN.setChecked(selectAll);
    }

    private void sendSMS(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + phoneNumber));
        intent.putExtra("sms_body", App.SHARE_MESSAGE.replace("{username}", UserData.getUserItem().username));
        intent.putExtra("compose_mode", true);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cancelBTN:
                contactsRecycleViewAdapter.setMultipleSelect(false);
                multiSelectCON.setVisibility(View.GONE);
                break;
            case R.id.commandTXT:
                sendSMS(contactsRecycleViewAdapter.getPhoneNumber());
                contactsRecycleViewAdapter.setMultipleSelect(false);
                multiSelectCON.setVisibility(View.GONE);
                break;
            case R.id.selectAllTXT:
                selectAll = !selectAll;
                Log.e("select", ">>>" + selectAll);
                selectAllBTN.setChecked(selectAll);
                break;
        }
    }
}
