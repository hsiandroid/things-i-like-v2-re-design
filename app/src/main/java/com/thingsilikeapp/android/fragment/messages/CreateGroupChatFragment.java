package com.thingsilikeapp.android.fragment.messages;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MessageActivity;
import com.thingsilikeapp.android.activity.SearchActivity;
import com.thingsilikeapp.android.adapter.FollowersRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.GroupFollowersRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.SuggestionRecycleViewAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.ChatModel;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.Chat;
import com.thingsilikeapp.server.request.social.FollowersRequest;
import com.thingsilikeapp.server.request.social.SuggestionRequest;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import icepick.State;

public class CreateGroupChatFragment extends BaseFragment implements  SwipeRefreshLayout.OnRefreshListener,
        SearchActivity.SearchListener,
        EndlessRecyclerViewScrollListener.Callback,
        FollowersRecycleViewAdapter.ClickListener, GroupFollowersRecycleViewAdapter.ClickListener, View.OnClickListener{

    public static final String TAG = CreateGroupChatFragment.class.getName();


    private MessageActivity messageActivity;

    public static CreateGroupChatFragment newInstance() {
        CreateGroupChatFragment fragment = new CreateGroupChatFragment();
        return fragment;
    }

    private FollowersRequest followersRequest;

    private FollowersRecycleViewAdapter followersRecyclerView;
    private GroupFollowersRecycleViewAdapter groupFollowersRecycleViewAdapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayoutManager memberLinearLayoutManager;
    private SuggestionRecycleViewAdapter suggestionRecycleViewAdapter;

    private Timer timer;
    private SuggestionRequest suggestionRequest;

    @State int userID = UserData.getUserId();
    @State boolean isRequest;

    @BindView(R.id.followerRV)                  RecyclerView followerRV;
    @BindView(R.id.membersRV)                   RecyclerView membersRV;
    @BindView(R.id.followerSRL)                 SwipeRefreshLayout followerSRL;
    @BindView(R.id.followersPB)                 View followersPB;
    @BindView(R.id.loadMorePB)                  View loadMorePB;
    @BindView(R.id.searchET)                    EditText searchET;
    @BindView(R.id.groupNameET)                 EditText groupNameET;
    @BindView(R.id.searchBTN)                   View searchBTN;
    @BindView(R.id.clearBTN)                    View clearBTN;
    @BindView(R.id.createBTN)                   View createBTN;
    @BindView(R.id.memberCON)                   View memberCON;
    @BindView(R.id.placeHolderCON)              View placeHolderCON;
    @BindView(R.id.mainTitleTXT)                TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)            ImageView mainBackButtonIV;
    @BindView(R.id.infoBTN)                     ImageView infoBTN;
    @BindView(R.id.searchPB)                    View searchPB;


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_create_chat;
    }

    @Override
    public void onViewReady() {
        messageActivity = (MessageActivity) getContext();
        mainTitleTXT.setText("Create Groupchat");
        Bundle bundle =  messageActivity.getFragmentBundle();
        if(bundle != null){
            isRequest = bundle.getBoolean("is_request", false);
        }

        setupFollowersListView();
        initSuggestionAPI();
        initSearch();
        setupMembersListView();

        createBTN.setOnClickListener(this);
        clearBTN.setOnClickListener(this);
        infoBTN.setVisibility(View.GONE);
        mainBackButtonIV.setOnClickListener(this);
        Analytics.trackEvent("messages_CreateGroupChatFragment_onViewReady");
    }

    private void setupFollowersListView() {
        followersRecyclerView = new FollowersRecycleViewAdapter(getContext());
        followersRecyclerView.setOnItemClickListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        followerRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        followerRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        followerRV.setLayoutManager(linearLayoutManager);
        followerRV.setAdapter(followersRecyclerView);
    }

    private void setupMembersListView() {
        groupFollowersRecycleViewAdapter = new GroupFollowersRecycleViewAdapter(getContext());
        groupFollowersRecycleViewAdapter.setOnItemClickListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        membersRV.setLayoutManager(linearLayoutManager);
        membersRV.setAdapter(groupFollowersRecycleViewAdapter);

        if(groupFollowersRecycleViewAdapter.getData().size() > 0){
            memberCON.setVisibility(View.GONE);
        }else {
            memberCON.setVisibility(View.VISIBLE);
        }
    }

    private void initSuggestionAPI(){

        followerSRL.setColorSchemeResources(R.color.colorPrimary);
        followerSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        followerSRL.setOnRefreshListener(this);
        followersRecyclerView.reset();

        followersRequest = new FollowersRequest(getContext());
        followersRequest
                .setSwipeRefreshLayout(followerSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showSwipeRefreshLayout(true)
                .setPerPage(20);
    }

    private void refreshList(){
//        followersPB.setVisibility(View.GONE);
//        endlessRecyclerViewScrollListener.reset();
//        followersRecyclerView.reset();
//        followersRequest
//                .showSwipeRefreshLayout(true)
//                .clearParameters()
//                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
//                .addParameters(Keys.server.key.USER_ID, userID)
//                .addParameters(Keys.server.key.KEYWORD, searchET.getText().toString())
//                .first();
        followersRecyclerView.reset();

        searchET.setEnabled(false);
        followersRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    public void initSearch(){
        if(searchET != null){
            searchET.setHint("Search People...");
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (timer != null) {
                        timer.cancel();
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    attemptSearch(searchET.getText().toString());
//                                    refreshList();
                                }
                            });
                        }
                    }, 300);
                }
            });
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void searchTextChange(String keyword) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                refreshList();
            }
        });

    }


    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        loadMorePB.setVisibility(followersRequest.hasMorePage() ? View.VISIBLE : View.GONE);
        followersRequest.nextPage();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    private void attemptSearch(String text){
        searchPB.setVisibility(View.VISIBLE);
        placeHolderCON.setVisibility(View.GONE);
        followerRV.setVisibility(View.VISIBLE);
        suggestionRecycleViewAdapter.reset();
        followersRequest = new FollowersRequest(getContext());
        followersRequest
                .clearParameters()
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, userID)
                .addParameters(Keys.server.key.KEYWORD, text)
                .setPerPage(15)
                .execute();
    }

    @Subscribe
    public void onResponse(FollowersRequest.ServerResponse responseData) {
        SuggestionTransformer suggestionTransformer = responseData.getData(SuggestionTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                followersRecyclerView.addNewData(suggestionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                followersRecyclerView.setNewData(suggestionTransformer.data);
            }
        }
        followersPB.setVisibility(View.GONE);
        loadMorePB.setVisibility(View.GONE);

        if(followersRecyclerView.getItemCount() == 0){
            placeHolderCON.setVisibility(View.VISIBLE);
            followerRV.setVisibility(View.GONE);
        }else {
            placeHolderCON.setVisibility(View.GONE);
            followerRV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemClick(UserItem userItem) {
        UserItem userModel = new UserItem();
        userModel.id = userItem.id;
        userModel.name = userItem.name;
        userModel.image = userItem.image;
        groupFollowersRecycleViewAdapter.getData().add(userModel);
        groupFollowersRecycleViewAdapter.notifyDataSetChanged();
        memberCON.setVisibility(View.GONE);
    }


    @Override
    public void onRemoveClick(int position) {
        groupFollowersRecycleViewAdapter.removeItem(position);
        groupFollowersRecycleViewAdapter.notifyItemRemoved(position);

        if(groupFollowersRecycleViewAdapter.getData().size() > 0){
            memberCON.setVisibility(View.GONE);
        }else {
            memberCON.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemLongClick(UserItem userItem) {

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.createBTN:
                Chat.getDefault().createChat(getContext(), groupNameET.getText().toString(), groupFollowersRecycleViewAdapter.getSelectedItems());
                Log.e("SELECTED", groupFollowersRecycleViewAdapter.getSelectedItems());
                Analytics.trackEvent("messages_ChatChangeNameFragment_createBTN");
                break;
            case R.id.clearBTN:
                groupNameET.getText().clear();
                Analytics.trackEvent("messages_ChatChangeNameFragment_clearBTN");
                break;
            case R.id.mainBackButtonIV:
                messageActivity.onBackPressed();
                Analytics.trackEvent("messages_ChatChangeNameFragment_mainBackButtonIV");
                break;
            case R.id.searchET:
                Keyboard.showKeyboard(getContext(), searchET);
                break;
            case R.id.searchBTN:
                attemptSearch(searchET.getText().toString());
                Keyboard.hideKeyboard(getActivity());
                break;
        }
    }

    @Subscribe
    public void onResponse(Chat.CreateChatResponse responseData) {
        SingleTransformer<ChatModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            groupNameET.setText("");
//            chatActivity.startMainActivity("messenger");
            messageActivity.openMessageFragment();
        }else{
            if(singleTransformer.hasRequirements()){
                ErrorResponseManger.first(groupNameET, singleTransformer.errors.title);
            }
        }
    }
}
