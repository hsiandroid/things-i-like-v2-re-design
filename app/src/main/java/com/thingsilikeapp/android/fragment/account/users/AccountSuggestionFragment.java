package com.thingsilikeapp.android.fragment.account.users;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.AccountActivity;
import com.thingsilikeapp.android.adapter.SuggestionRecycleViewAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.social.SuggestionRequest;
import com.thingsilikeapp.server.request.user.SearchUserRequest;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

public class AccountSuggestionFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener,
        SuggestionRecycleViewAdapter.ClickListener{
    public static final String TAG = AccountSuggestionFragment.class.getName();


    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private EndlessRecyclerViewScrollListener searchEndlessRecyclerViewScrollListener;

    private LinearLayoutManager linearLayoutManager;
    private LinearLayoutManager searchLinearLayoutManager;

    private SuggestionRecycleViewAdapter searchResultAdapter;
    private SuggestionRecycleViewAdapter suggestionRecycleViewAdapter;

    private SuggestionRequest suggestionRequest;
    private SearchUserRequest searchUserRequest;

    private Timer timer;

    @BindView(R.id.suggestedRV)                 RecyclerView suggestedRV;
    @BindView(R.id.suggestionSRL)               MultiSwipeRefreshLayout suggestionSRL;
    @BindView(R.id.searchPB)                    View searchPB;
    @BindView(R.id.suggestionCON)               View suggestionCON;
    @BindView(R.id.loadMorePB)                  View loadMorePB;
    @BindView(R.id.searchET)                    EditText searchET;
    @BindView(R.id.searchBTN)                   View searchBTN;
    @BindView(R.id.nextBTN)                     View nextBTN;
    @BindView(R.id.placeHolderCON)              View placeHolderCON;

    public static AccountSuggestionFragment newInstance() {
        AccountSuggestionFragment suggestionFragment = new AccountSuggestionFragment();
        return suggestionFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_account_suggestion;
    }

    @Override
    public void onViewReady() {

        searchBTN.setOnClickListener(this);
        nextBTN.setOnClickListener(this);

        setupTopUserListView();

        initSuggestionAPI();
        initSearch();
        Keyboard.hideKeyboard(getActivity());
        Analytics.trackEvent("account_users_AccountSuggestionFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        Keyboard.hideKeyboard(getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
        if (timer != null) {
            timer.cancel();
        }
    }


    private void setupTopUserListView() {
        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, new EndlessRecyclerViewScrollListener.Callback() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePB.setVisibility(suggestionRequest.hasMorePage() ? View.VISIBLE : View.GONE);
                suggestionRequest.nextPage();
            }
        });

        suggestionRecycleViewAdapter = new SuggestionRecycleViewAdapter(getContext());
        suggestedRV.setLayoutManager(linearLayoutManager);
        suggestedRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        suggestionRecycleViewAdapter.setOnItemClickListener(this);
        suggestedRV.setAdapter(suggestionRecycleViewAdapter);
    }

    private void initSuggestionAPI(){
        searchET.setEnabled(false);
        suggestionSRL.setColorSchemeResources(R.color.colorPrimary);
        suggestionSRL.setSwipeableChildren(R.id.topUserEHLV);
        suggestionSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        suggestionSRL.setOnRefreshListener(this);
        suggestionRequest = new SuggestionRequest(getContext());
        suggestionRequest
                .setSwipeRefreshLayout(suggestionSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .showSwipeRefreshLayout(true)
                .setPerPage(25)
                .execute();
    }

    private void attemptSearch(String text){
        searchPB.setVisibility(View.VISIBLE);
        placeHolderCON.setVisibility(View.GONE);
        suggestedRV.setVisibility(View.VISIBLE);
        suggestionRecycleViewAdapter.reset();
        suggestionRequest = new SuggestionRequest(getContext());
        suggestionRequest
                .clearParameters()
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.KEYWORD, text)
                .setPerPage(25)
                .execute();
    }

    public void initSearch(){
        if(searchET != null){
            searchET.setHint("Search People...");
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (timer != null) {
                        timer.cancel();
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                        attemptSearch(searchET.getText().toString());
            }
        });
                        }
                    }, 300);
                }
            });
        }
    }

    private void refreshList(){
        suggestionRecycleViewAdapter.reset();

        searchET.setEnabled(false);
        suggestionRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(SuggestionRequest.ServerResponse responseData) {
        SuggestionTransformer suggestionTransformer = responseData.getData(SuggestionTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                suggestionRecycleViewAdapter.addNewData(suggestionTransformer.data);
            }else{
                suggestionRecycleViewAdapter.setNewData(suggestionTransformer.data);
            }
        }

        if (!searchET.getText().toString().equals("") || !searchET.getText().toString().equals(null)){
            if (suggestionRecycleViewAdapter.getItemCount() == 0){
                placeHolderCON.setVisibility(View.VISIBLE);
                suggestedRV.setVisibility(View.GONE);
            }
        }

        searchET.setEnabled(true);
        loadMorePB.setVisibility(View.GONE);
        searchPB.setVisibility(View.GONE);
    }



    @Override
    public void onCommandClick(UserItem userItem) {
        if(userItem.social.data.is_following.equals("yes")){
            suggestionRecycleViewAdapter.unFollowUser(userItem);
        }else{
            suggestionRecycleViewAdapter.followUser(userItem);
        }
    }

    @Override
    public void onUserClick(UserItem userItem) {

    }

    @Override
    public void onFollowSuccessClick() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.nextBTN:
//                if(UserData.getUserItem().statistics.data.following >= 1){
                    ((AccountActivity)getContext()).openShareAppFragment();
//                }else{
//                    ToastMessage.show(getContext(), "You must follow at least 1 user", ToastMessage.Status.FAILED);
//                }
                Analytics.trackEvent("account_AccountContactsFragment_nextBTN");
                break;
        }
    }

//    @Override
//    public void searchTextChange(final String keyword) {
//        getActivity().runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if(searchResultCON != null && suggestionCON != null){
//                    if (keyword.equals("")){
//                        searchResultCON.setVisibility(View.GONE);
//                        suggestionCON.setVisibility(View.VISIBLE);
//                    }else{
//                        searchResultCON.setVisibility(View.VISIBLE);
//                        suggestionCON.setVisibility(View.GONE);
//                        attemptSearch(keyword);
//                    }
//                }
//            }
//        });
//    }
}
