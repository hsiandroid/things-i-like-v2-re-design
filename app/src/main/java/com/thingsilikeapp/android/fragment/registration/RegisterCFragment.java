package com.thingsilikeapp.android.fragment.registration;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.NewRegistrationActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;
import icepick.State;

public class RegisterCFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = RegisterCFragment.class.getName();

    private NewRegistrationActivity registrationActivity;

    @BindView(R.id.nextBTN)                 TextView nextBTN;
    @BindView(R.id.skipBTN)                 TextView skipBTN;
    @BindView(R.id.backBTN)                 TextView backBTN;
    @BindView(R.id.maleBTN)                 ImageView maleBTN;
    @BindView(R.id.femaleBTN)               ImageView femaleBTN;

    @State String username;
    @State String gender;
    @State String name;


    public static RegisterCFragment newInstance(String username, String name) {
        RegisterCFragment fragment = new RegisterCFragment();
        fragment.username = username;
        fragment.name = name;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_registration_c;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (NewRegistrationActivity) getContext();
        nextBTN.setOnClickListener(this);
        maleBTN.setOnClickListener(this);
        femaleBTN.setOnClickListener(this);
        skipBTN.setOnClickListener(this);
        backBTN.setOnClickListener(this);
        Analytics.trackEvent("registration_RegisterCFragment_onViewReady");
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.nextBTN:
                registrationActivity.openDFragment(username, gender, name);
                Analytics.trackEvent("registration_RegisterCFragment_nextBTN");
                break;
            case R.id.maleBTN:
                maleBTN.setImageResource(R.drawable.male_selected);
                femaleBTN.setImageResource(R.drawable.female_unselected);
                gender = "male";
                Analytics.trackEvent("registration_RegisterCFragment_maleBTN");
                break;
            case R.id.femaleBTN:
                maleBTN.setImageResource(R.drawable.male_unselected);
                femaleBTN.setImageResource(R.drawable.female_selected);
                gender = "female";
                Analytics.trackEvent("registration_RegisterCFragment_femaleBTN");
                break;
            case R.id.skipBTN:
                registrationActivity.openDFragment(username, "", name);
                Analytics.trackEvent("registration_RegisterCFragment_skipBTN");
                break;
            case R.id.backBTN:
                registrationActivity.startLandingActivity();
                Analytics.trackEvent("registration_RegisterCFragment_backBTN");
                break;
        }
    }
}
