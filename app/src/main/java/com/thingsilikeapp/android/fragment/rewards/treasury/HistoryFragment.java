package com.thingsilikeapp.android.fragment.rewards.treasury;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.adapter.RewardHistoryRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.PasscodeVerificationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.EncashHistoryItem;
import com.thingsilikeapp.data.model.RewardHistoryItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.server.request.report.ReportRequest;
import com.thingsilikeapp.server.request.reward.RewardHistoryRequest;
import com.thingsilikeapp.server.request.user.UserInfoRequest;
import com.thingsilikeapp.server.transformer.reward.RewardHistoryTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;
import com.thingsilikeapp.vendor.server.transformer.BaseTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public  class HistoryFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        RewardHistoryRecycleViewAdapter.ClickListener,
        EndlessRecyclerViewScrollListener.Callback, View.OnClickListener{

    public static final String TAG = HistoryFragment.class.getName();

    private RewardHistoryRecycleViewAdapter rewardHistoryRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private  RewardHistoryRequest rewardHistoryRequest;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private RewardsActivity rewardsActivity;

    @BindView(R.id.historyRV)                   RecyclerView historyRV;
    @BindView(R.id.treasurySRL)                 MultiSwipeRefreshLayout treasurySRL;
    @BindView(R.id.refTXT)                      TextView refTXT;
    @BindView(R.id.statusTXT)                   TextView statusTXT;
    @BindView(R.id.cancelBTN)                   TextView cancelBTN;
    @BindView(R.id.remarksTXT)                  TextView reamarksTXT;
    @BindView(R.id.gemEncashTXT)                TextView gemEncashTXT;
    @BindView(R.id.finalAmountTXT)              TextView finalAmountTXT;
    @BindView(R.id.pendingCON)                  View pendingCON;

    public static HistoryFragment newInstance() {
        HistoryFragment historyFragment = new HistoryFragment();
        return historyFragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_my_reward_history;
    }

    @Override
    public void onViewReady() {
        rewardsActivity = (RewardsActivity) getContext();
        setUpTreasuryListView();
        initNotificationAPI();
        Analytics.trackEvent("rewards_treasury_HistoryFragment_onViewReady");
        cancelBTN.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void initNotificationAPI(){

        treasurySRL.setColorSchemeResources(R.color.colorPrimary);
        treasurySRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        treasurySRL.setOnRefreshListener(this);
        treasurySRL.setSwipeableChildren(R.id.historyRV);

        rewardHistoryRequest = new RewardHistoryRequest(getContext());
        rewardHistoryRequest
                .setSwipeRefreshLayout(treasurySRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "transaction,encashment,user,order,item")
                .setPerPage(10);
    }

    private void refreshList(){
        Wallet.getDefault().pending(getContext());
        endlessRecyclerViewScrollListener.reset();
        rewardHistoryRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    private void setUpTreasuryListView(){
        rewardHistoryRecycleViewAdapter = new RewardHistoryRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        historyRV.setLayoutManager(linearLayoutManager);
        historyRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        historyRV.setAdapter(rewardHistoryRecycleViewAdapter);
        rewardHistoryRecycleViewAdapter.setOnItemClickListener(this);
    }


    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(RewardHistoryRequest.ServerResponse responseData) {
        RewardHistoryTransformer rewardHistoryTransformer = responseData.getData(RewardHistoryTransformer.class);
        if(rewardHistoryTransformer.status){
            if(responseData.isNext()){
                rewardHistoryRecycleViewAdapter.addNewData(rewardHistoryTransformer.data);
            }else{
                rewardHistoryRecycleViewAdapter.setNewData(rewardHistoryTransformer.data);
            }

            UserData.updateRewardCrystal(rewardHistoryTransformer.user.reward_crystal + "");
            UserData.updateRewardFragment(rewardHistoryTransformer.user.reward_fragment + "");
            UserData.updateRewardRatio(rewardHistoryTransformer.ratio);
        }
    }

    @Subscribe
    public void onResponse(Wallet.PendingResponse tranformerResponse) {
        SingleTransformer<EncashHistoryItem> transformer = tranformerResponse.getData(SingleTransformer.class);
        if (transformer.status) {
            if (!transformer.status_code.equalsIgnoreCase("NO_PENDING_WITHDRAWAL")){
                switch (transformer.status_code){
                    case "cancelled":
                    case "rejected":
                    case "release":
                    case "completed":
                        pendingCON.setVisibility(View.GONE);
                        break;
                        default:
                            pendingCON.setVisibility(View.VISIBLE);
                            break;
                }

                refTXT.setText(transformer.data.refno);
                statusTXT.setText(transformer.data.status);
                reamarksTXT.setText(transformer.data.remarks);
                gemEncashTXT.setText(transformer.data.gemQty);
                finalAmountTXT.setText(transformer.data.currency + " " + transformer.data.finalAmount);

                switch (transformer.data.status){
                    case "close":
                        statusTXT.setBackgroundResource(R.color.gray);
                        break;
                    case "completed":
                    case "approved":
                        statusTXT.setBackgroundResource(R.color.completed);
                        break;
                    case "decline":
                    case "cancelled":
                        statusTXT.setBackgroundResource(R.color.failed);
                        break;
                    case "pending":
                        statusTXT.setBackgroundResource(R.color.gold);
                        break;
                }
            }else{
                pendingCON.setVisibility(View.GONE);
            }

        }
    }

    @Subscribe
    public void onResponse(Wallet.CancelResponse tranformerResponse) {
        BaseTransformer transformer = tranformerResponse.getData(BaseTransformer.class);
        if (transformer.status) {
            pendingCON.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onResponse(UserInfoRequest.ServerResponse responseData) {
        refreshList();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        historyRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        rewardHistoryRequest.nextPage();
    }

    @Override
    public void onItemClick(RewardHistoryItem rewardHistoryItem) {
        rewardsActivity.openHistoryDetailsFragment(rewardHistoryItem);
//        ((RouteActivity) getContext()).startRewardsActivity("history_Details", rewardHistoryItem.id);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cancelBTN:
                final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
                confirmationDialog
                        .setDescription("Are you sure you want to cancel this encashment?")
                        .setNote("You are about to cancel an encashment.")
                        .setIcon(R.drawable.icon_information)
                        .setPositiveButtonText("Yes")
                        .setPositiveButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Wallet.getDefault().cancel(getContext());
                                confirmationDialog.dismiss();
                            }
                        })
                        .setNegativeButtonText("Cancel")
                        .build(getChildFragmentManager());

                break;
        }
    }
}
