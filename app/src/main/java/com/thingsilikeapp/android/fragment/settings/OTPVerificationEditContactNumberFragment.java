package com.thingsilikeapp.android.fragment.settings;

import android.view.View;

import com.dpizarro.pinview.library.PinView;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;
import com.thingsilikeapp.android.activity.VerificationActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.KYC;
import com.thingsilikeapp.server.request.user.ChangeContactNumberRequest;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

import static com.thingsilikeapp.vendor.android.java.SnackBar.dismiss;

public class OTPVerificationEditContactNumberFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = OTPVerificationEditContactNumberFragment.class.getName();


    @BindView(R.id.submitBTN)          View submitBTN;
    @BindView(R.id.pinView)            PinView pinView;

    @State String country;
    @State String contact;
    @State String contry_code;
    @State String otp;

    private SettingsActivity settingsActivity;

    public static OTPVerificationEditContactNumberFragment newInstance(String country, String contact, String contry_code) {
        OTPVerificationEditContactNumberFragment fragment = new OTPVerificationEditContactNumberFragment();
        fragment.country = country;
        fragment.contact = contact;
        fragment.contry_code = contry_code;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_verification_otp;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getContext();
        settingsActivity.setTitle("OTP Verification");
        submitBTN.setOnClickListener(this);

    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(ChangeContactNumberRequest.ChangeContactResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            UserData.insert(singleTransformer.data);
            settingsActivity.startDiscoverActivity("setting");
//            settingsActivity.openEditContactNumberFragment();
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.submitBTN:
//                ChangeContactNumberRequest.getDefault().phoneOTP(getContext(), pinView.getPinResults());
                ChangeContactNumberRequest.getDefault().changeContact(getContext(), country,contact, contry_code, pinView.getPinResults());
                break;
        }
    }
}
