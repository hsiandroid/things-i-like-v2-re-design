package com.thingsilikeapp.android.fragment.tutorial;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

public class Tutorial2Fragment extends BaseFragment {

    public static final String TAG = Tutorial2Fragment.class.getName();

    public static Tutorial2Fragment newInstance() {
        Tutorial2Fragment tutorial2Fragment = new Tutorial2Fragment();
        return tutorial2Fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_tutorial_2;
    }

    @Override
    public void onViewReady() {
    }
}
