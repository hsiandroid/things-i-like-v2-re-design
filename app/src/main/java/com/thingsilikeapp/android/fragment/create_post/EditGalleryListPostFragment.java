package com.thingsilikeapp.android.fragment.create_post;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.iceteck.silicompressorr.SiliCompressor;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.CreatePostActivity;
import com.thingsilikeapp.android.adapter.EditPhotoListRecycleViewAdapters;
import com.thingsilikeapp.android.filter.ThumbnailsAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.BitmapItem;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.decoration.SpacesItemDecoration;
import com.zomato.photofilters.FilterPack;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.utils.ThumbnailItem;
import com.zomato.photofilters.utils.ThumbnailsManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;
import id.zelory.compressor.Compressor;

public class EditGalleryListPostFragment extends BaseFragment  implements ThumbnailsAdapter.ThumbnailsAdapterListener,
        View.OnClickListener,
        EditPhotoListRecycleViewAdapters.ClickListener, EditGalleryPostSingleFragment.FilteredCalback {

    static
    {
        System.loadLibrary("NativeImageProcessor");
    }

    public static final String TAG = EditGalleryListPostFragment.class.getName();

    private CreatePostActivity createPostActivity;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayoutManager LLManager;
    private ThumbnailsAdapter thumbnailsAdapter;
    private EditPhotoListRecycleViewAdapters editPhotoListRecycleViewAdapter;
    List<ThumbnailItem> thumbnailItemList;
    private List<BitmapItem> bitmapItem;


    @BindView(R.id.titleTXT)    TextView textViewTxt;
    @BindView(R.id.filterRV)    RecyclerView filterRV;
    @BindView(R.id.photoRV)    RecyclerView photoRV;
    @BindView(R.id.nextBTN)     TextView nextBTN;
    @BindView(R.id.mainBackButtonIV)	View mainBackButtonIV;

    @State Bitmap file;
    @State Bitmap imageBitMap;
    @State Bitmap filteredImage;
    @State File filteredFile;

    public static EditGalleryListPostFragment newInstance(List<BitmapItem> bitmapItem) {
        EditGalleryListPostFragment fragment = new EditGalleryListPostFragment();
        fragment.bitmapItem = bitmapItem;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_post_list;
    }

    @Override
    public void onViewReady() {
        createPostActivity = (CreatePostActivity) getContext();
        textViewTxt.setText("Edit");
        setUpRV();
        filteredImage = file;
        nextBTN.setOnClickListener(this);
        mainBackButtonIV.setOnClickListener(this);
        imageBitMap = BitmapFactory.decodeResource(getContext().getResources(),
                R.drawable.cat);

        setUpPhotoRV();

    }

    private void setUpPhotoRV(){
        editPhotoListRecycleViewAdapter = new EditPhotoListRecycleViewAdapters(getContext());
        LLManager = new LinearLayoutManager(getContext());
        LLManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        photoRV.setLayoutManager(LLManager);
        editPhotoListRecycleViewAdapter.setNewData(bitmapItem);
        photoRV.setAdapter(editPhotoListRecycleViewAdapter);
        editPhotoListRecycleViewAdapter.setOnItemClickListener(this);
    }

    public void setUpRV(){
        thumbnailItemList = new ArrayList<>();
        thumbnailsAdapter = new ThumbnailsAdapter(getActivity(), thumbnailItemList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        filterRV.setLayoutManager(mLayoutManager);
        filterRV.setItemAnimator(new DefaultItemAnimator());
        int space = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1,
                getResources().getDisplayMetrics());
        filterRV.addItemDecoration(new SpacesItemDecoration(space));
        filterRV.setAdapter(thumbnailsAdapter);

        Bitmap bitmap= BitmapFactory.decodeResource(getContext().getResources(),
                R.drawable.cat);

        prepareThumbnail(bitmap);

    }

    public void prepareThumbnail(final Bitmap bitmap) {
        Runnable r = new Runnable() {
            public void run() {
                Bitmap thumbImage;

                    thumbImage = Bitmap.createScaledBitmap(bitmap, 100, 100, false);

                if (thumbImage == null)
                    return;

                ThumbnailsManager.clearThumbs();
                thumbnailItemList.clear();

                // add normal bitmap first
                ThumbnailItem thumbnailItem = new ThumbnailItem();
                thumbnailItem.image = thumbImage;
                thumbnailItem.filterName = "Normal";
                ThumbnailsManager.addThumb(thumbnailItem);

                List<Filter> filters = FilterPack.getFilterPack(getActivity());

                for (Filter filter : filters) {
                    ThumbnailItem tI = new ThumbnailItem();
                    tI.image = thumbImage;
                    tI.filter = filter;
                    tI.filterName = filter.getName();

                    ThumbnailsManager.addThumb(tI);
                }

                thumbnailItemList.addAll(ThumbnailsManager.processThumbs(getActivity()));

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        thumbnailsAdapter.notifyDataSetChanged();
                    }
                });
            }
        };

        new Thread(r).start();
    }


    @Override
    public void onFilterSelected(Filter filter) {
        FilterTask task = new FilterTask();
        task.selectedFilter  = filter;
        task.execute();

    }

    private File getImageFile(Bitmap bitmap) {
        String imageFileName = "LykaCacheImage";
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    createImageDir()      /* directory */
            );

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
            byte[] bitmapData = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(image);
            fos.write(bitmapData);
            fos.flush();
            fos.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    private File createImageDir(){
        File storageDir = new File(Environment.getExternalStorageDirectory(), "Android/media/com.thingsilikeapp");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        return storageDir;
    }

    @Override
    public void onItemClick(BitmapItem galleryItem) {
        createPostActivity.openEditGalleryPostSingle(galleryItem, this);
    }

    @Override
    public void onSuccess(BitmapItem bitmapItem) {
        editPhotoListRecycleViewAdapter.updateBitmap(bitmapItem);
    }

    private class FilterTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog asyncDialog = new ProgressDialog(getContext());
        Filter selectedFilter;
        List<BitmapItem> bitmapItems = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            //set message of the dialog
            asyncDialog.setMessage("Processing...");
            //show dialog
            asyncDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            BitmapItem defaultItem;
            for (BitmapItem imagePath : bitmapItem) {
                filteredImage = imagePath.origBitmap.copy(Bitmap.Config.ARGB_8888, true);
                defaultItem = new BitmapItem();
                if (selectedFilter.getName() != null){
                    defaultItem.bitmap = selectedFilter.processFilter(filteredImage);
                }else{
                    defaultItem.bitmap = imagePath.origBitmap;
                }
                defaultItem.origBitmap = imagePath.origBitmap;
                bitmapItems.add(defaultItem);

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //hide the dialog
            editPhotoListRecycleViewAdapter.setNewData(bitmapItems);
            Log.e("EEEEEEEEE", "SUCCESS!");
            asyncDialog.dismiss();
            super.onPostExecute(result);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.mainBackButtonIV:
                createPostActivity.onBackPressed();
                break;
            case R.id.nextBTN:
//                new CheckTypesTask().execute();
                createPostActivity.openSendPostList(editPhotoListRecycleViewAdapter.getImage(), editPhotoListRecycleViewAdapter.getData(), editPhotoListRecycleViewAdapter.getItemCount());
                break;
        }
    }
}
