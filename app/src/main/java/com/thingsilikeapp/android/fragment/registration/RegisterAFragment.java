package com.thingsilikeapp.android.fragment.registration;

import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.NewRegistrationActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class RegisterAFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = RegisterAFragment.class.getName();

    private NewRegistrationActivity registrationActivity;

    @BindView(R.id.nextBTN) TextView nextBTN;
    @BindView(R.id.backBTN) TextView backBTN;

    public static RegisterAFragment newInstance() {
        RegisterAFragment fragment = new RegisterAFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_registration_a;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (NewRegistrationActivity) getContext();
        nextBTN.setOnClickListener(this);
        backBTN.setOnClickListener(this);
        Analytics.trackEvent("registration_RegisterAFragment_onViewReady");
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.nextBTN:
                registrationActivity.openBFragment();
                Analytics.trackEvent("registration_ForgotPasswordFragment_nextBTN");
                break;
            case R.id.backBTN:
                registrationActivity.startLandingActivity();
                Analytics.trackEvent("registration_ForgotPasswordFragment_backBTN");
                break;
        }
    }
}
