package com.thingsilikeapp.android.fragment.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.danikula.videocache.HttpProxyCacheServer;
import com.google.gson.Gson;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MainActivity;
import com.thingsilikeapp.android.adapter.SocialFeedRecyclerViewAdapter;
import com.thingsilikeapp.android.adapter.SocialFeedThreeRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.FeedCategoryDialog;
import com.thingsilikeapp.android.dialog.GroupDialog;
import com.thingsilikeapp.android.dialog.SearchDialog;
import com.thingsilikeapp.android.dialog.UnfollowDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Share;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.like.LikeRequest;
import com.thingsilikeapp.server.request.report.ReportRequest;
import com.thingsilikeapp.server.request.social.FeedRequest;
import com.thingsilikeapp.server.request.social.FollowRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.user.UserInfoRequest;
import com.thingsilikeapp.server.request.wishlist.BlockRequest;
import com.thingsilikeapp.server.transformer.report.ReportTransformer;
import com.thingsilikeapp.server.transformer.social.FeedTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.CustomGridLayoutManager;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.WalkThrough;
import com.thingsilikeapp.vendor.android.java.decoration.SpacesItemDecoration;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.android.widget.MaterialShowcaseView;
import com.thingsilikeapp.vendor.android.widget.OneItemListener;
import com.thingsilikeapp.vendor.android.widget.OneItemManager;
import com.thingsilikeapp.vendor.android.widget.swiperefresh.CustomSwipeHeadView;
import com.thingsilikeapp.vendor.android.widget.swiperefresh.CustomSwipeRefreshLayout;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.yqritc.scalablevideoview.ScalableType;
import com.thingsilikeapp.vendor.android.widget.CustomVideoView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;

import butterknife.BindView;

import static android.view.View.GONE;

public class SocialFeedFragment extends BaseFragment implements
        SocialFeedRecyclerViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        CustomSwipeRefreshLayout.OnRefreshListener,
        EndlessRecyclerViewScrollListener.Callback,
        EndlessRecyclerViewScrollListener.HideShowListener, SocialFeedThreeRecycleViewAdapter.ClickListener,
        View.OnClickListener,
        GroupDialog.Callback,
        SearchDialog.Callback,
        FeedCategoryDialog.Callback,
        MainActivity.GemListener {

    public static final String TAG = SocialFeedFragment.class.getName();

    private MainActivity mainActivity;
    private String lastPage= "0";
    private String currentPage = "1";

    private SocialFeedRecyclerViewAdapter socialFeedRecyclerViewAdapter;
    private SocialFeedThreeRecycleViewAdapter socialFeedThreeRecycleViewAdapter;
    private FeedRequest feedRequest;
//    private UserInfoRequest userInfoRequest;

    private CustomGridLayoutManager staggeredGridLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private SpacesItemDecoration spacesItemDecoration;
    private LinearLayoutManager linearLayoutManager;
    private View currentFocusedLayout, oldFocusedLayout;

    private OneItemListener oneItemListener;
    private int targetPosition = 0;

    private String keyword = "RECENT POSTS";
    private String random = "RANDOM";

    @BindView(R.id.socialFeedRV)                    RecyclerView socialFeedRV;
    @BindView(R.id.customSwipeRefreshLayout)        CustomSwipeRefreshLayout customSwipeRefreshLayout;
    @BindView(R.id.placeHolderCON)                  View placeHolderCON;
    @BindView(R.id.loadMorePB)                      View loadMorePB;
    @BindView(R.id.shareBTN)                        View shareBTN;
    @BindView(R.id.birthDateBTN)                    View birthDateBTN;
    @BindView(R.id.viewTypeBTN)                     ImageView viewTypeBTN;
    @BindView(R.id.feedTXT)                         TextView feedTXT;
    @BindView(R.id.eraseBTN)                        View eraseBTN;
    @BindView(R.id.chipsPB)                         ProgressBar chipsPB;
    @BindView(R.id.chipsCON)                        View chipsCON;
    @BindView(R.id.rewardsBTN)                      View rewardsBTN;
    @BindView(R.id.rewardsCON)                      View rewardsCON;
    @BindView(R.id.container)                       FrameLayout container;

    public static SocialFeedFragment newInstance() {
        SocialFeedFragment socialFeedFragment = new SocialFeedFragment();
        return socialFeedFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_social_feed;
    }

    @Override
    public void onViewReady(Bundle bundle) {
        mainActivity = (MainActivity) getContext();
        shareBTN.setOnClickListener(this);
        viewTypeBTN.setOnClickListener(this);
        feedTXT.setOnClickListener(this);
        eraseBTN.setOnClickListener(this);
        rewardsBTN.setOnClickListener(this);
        birthDateBTN.setOnClickListener(this);
        viewTypeBTN.setTag("grid");
//        setupListView();
//        onRefresh();
        setUpRV();
        attemptSearch();
        setBackToDefault();
        Analytics.trackEvent("main_SocialFeedFragment_onViewReady");

        Log.e("TOKEN", ">>>" + UserData.getString(UserData.AUTHORIZATION));
    }

    @Override
    public void onResume() {
        super.onResume();
        Glide.with(getContext()).clear(getView());
        Glide.get(getContext()).clearMemory();
        mainActivity.createYourOwnPostLabelVisibility(UserData.getInt(UserData.TOTAL_POST) == 0);
        mainActivity.feedActive();

//        mainActivity.updateReward();
//        attemptSearch();
//        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)) {
//            switch (UserData.getInt(UserData.WALKTHRU_CURRENT)) {
//                case 6:
//                    attemptSearch();
//                    break;
//                case 7:
//                    mainActivity.attemptGetCurrentWalkThrough();
//                    mainActivity.setGemsListener(this);
//                    break;
//                case 4:
//                case 8:
//                case 5:
//                case 9:
//                case 0:
//                case 1:
//                case 3:
//                    mainActivity.attemptGetCurrentWalkThrough();
//                    break;
//                case 2:
//                    MaterialShowcaseView.resetSingleUse(getContext(), WalkThrough.FRIENDS);
//                    mainActivity.attemptGetCurrentWalkThrough();
//                    break;
//            }
//        }
    }



    private void setUpRV(){
        customSwipeRefreshLayout.setCustomHeadview(new CustomSwipeHeadView(getContext()));
        customSwipeRefreshLayout.setOnRefreshListener(this);

        socialFeedThreeRecycleViewAdapter = new SocialFeedThreeRecycleViewAdapter(getContext());

        linearLayoutManager = new LinearLayoutManager(getContext());
        socialFeedRV.setLayoutManager(linearLayoutManager);
        socialFeedRV.setAdapter(socialFeedThreeRecycleViewAdapter);
        socialFeedRV.setHasFixedSize(true);
        socialFeedRV.setSaveEnabled(true);
        socialFeedThreeRecycleViewAdapter.setOnItemClickListener(this);
//        OneItemManager.setup(getContext(), socialFeedThreeRecycleViewAdapter, socialFeedRV);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this, this);
        socialFeedRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        oneItem();
    }


    private void oneItem(){
        socialFeedRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(linearLayoutManager == null){
                    Log.e(TAG, "LayoutManager is not initialized, please call and use OneItemManager#getLayoutManager()");
                    return;
                }

                int firstCompletelyVisibleItem = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                int prevItemPosition = targetPosition;

                if(firstCompletelyVisibleItem == RecyclerView.NO_POSITION)
                    return;

                targetPosition = firstCompletelyVisibleItem;

                if(prevItemPosition != targetPosition){
                    socialFeedThreeRecycleViewAdapter.unSelectItemAt(prevItemPosition);
                    socialFeedThreeRecycleViewAdapter.selectItemAt(targetPosition);
                }
            }
        });
    }

    private void scrollListener(){
        socialFeedRV.addOnScrollListener(new RecyclerView.OnScrollListener() {

            int activeAdapter = 0;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                // Get the index of the first Completely visible item
                int firstCompletelyVisibleItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                Log.d(TAG, "onScrolled: firstCompletelyVisibleItemPosition : " + firstCompletelyVisibleItemPosition);

                // Even if we scroll by a few millimeters the video will start playing from the beginning
                // So we need to check if the new Active row layout position is equal to the current active row layout position

                if (activeAdapter != firstCompletelyVisibleItemPosition) {

                    try {
                        WishListItem videoModel = socialFeedThreeRecycleViewAdapter.getItem(firstCompletelyVisibleItemPosition);
                        String video_url = videoModel.images.data.get(0).fullPath;
                        final CustomVideoView videoView = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition).findViewById(R.id.videoView);
                        final ImageView iv_cover = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition).findViewById(R.id.coverIV);
                        final ImageView playBTN = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition).findViewById(R.id.playBTN);

                        Log.e("EEEEEEEE", ">>>>>>" + videoModel.images.data.get(0).fullPath);

                        // Start playing the video in Active row layout
                        if (videoModel.post_type.equalsIgnoreCase("video")){
                            try {
                                videoView.prepare(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        videoView.setVisibility(View.VISIBLE);
                                        videoView.start();
                                    }
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.e("ZZZZZZZZ", ">>>>>>" + e);

                            }
                            Log.e("ZZZZZZZZ", ">>>>>>" + videoModel.post_type);
                        }


                        // assign this row layout position as active row Layout
                        activeAdapter = firstCompletelyVisibleItemPosition;
                        Log.d(TAG, "onScrolled: activeAdapter : " + activeAdapter);


                        // Hide the thumbnail ImageView with a delay of 300 millisecond or else there will be black
                        // screen before a video plays
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                iv_cover.setVisibility(View.GONE);
                                playBTN.setVisibility(View.GONE);
                            }
                        }, 800);


                    } catch (NullPointerException e) {
                        // Sometimes you scroll so fast that the views are not attached so it gives a NullPointerException
                    } catch (ArrayIndexOutOfBoundsException e) {

                    }

                /* Get the Video Surface directly above the fully visible Row Layout so that you may stop the playback
                 when a new row Layout is fully visible
                 */
                    if (firstCompletelyVisibleItemPosition >= 1) {

                        try {
                            CustomVideoView video_view_above = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition - 1).findViewById(R.id.videoView);
//                            video_view_above.stop();

                            WishListItem videoModel = socialFeedThreeRecycleViewAdapter.getItem(firstCompletelyVisibleItemPosition - 1);

                            if (videoModel.post_type.equalsIgnoreCase("video")){
                                if (video_view_above.isPlaying()){
                                    video_view_above.stop();
                                }
                            }
//                            String thumbnail_string = videoModel.getThumbnail();
                            //  video_view_above.start(Uri.parse(thumbnail_string));

                            ImageView iv_cover_above = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition - 1).findViewById(R.id.coverIV);
                            ImageView playBTN = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition - 1).findViewById(R.id.playBTN);

                            // Show the cover Thumbnail ImageView
                            iv_cover_above.setVisibility(View.VISIBLE);
                            playBTN.setVisibility(View.GONE);
//
//                            Picasso.with(VideoListActivity.this)
//                                    .load((Uri.parse(thumbnail_string)))
//                                    .into(iv_cover_above);

                            // video_view_above.setBackground(Uri.parse(thumbnail_string));

                        } catch (NullPointerException e) {
                        } catch (ArrayIndexOutOfBoundsException e) {
                        }
                    }

               /* Get the Video Surface directly Below the fully visible Row Layout so that you may stop the playback
                 when a new row Layout is fully visible
                 */
                    if (firstCompletelyVisibleItemPosition + 1 < socialFeedThreeRecycleViewAdapter.getItemCount()) {

                        try {
                            CustomVideoView video_view_below = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition + 1).findViewById(R.id.videoView);
//                            video_view_below.stop();

                            WishListItem videoModel = socialFeedThreeRecycleViewAdapter.getItem(firstCompletelyVisibleItemPosition + 1);
                            if (videoModel.post_type.equalsIgnoreCase("video")){
//                                video_view_below.release();
                                if (video_view_below.isPlaying()){
                                    video_view_below.stop();
                                }
                            }

                            //  video_view_below.start(Uri.parse(thumbnail_string));

                            ImageView iv_cover_below = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition + 1).findViewById(R.id.coverIV);
                            ImageView playBTN = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition + 1).findViewById(R.id.playBTN);

                            iv_cover_below.setVisibility(View.VISIBLE);
                            playBTN.setVisibility(View.GONE);

//                            Picasso.with(VideoListActivity.this)
//                                    .load((Uri.parse(thumbnail_string)))
//                                    .into(iv_cover_below);

                        } catch (NullPointerException e) {
                        } catch (ArrayIndexOutOfBoundsException e) {
                        }

                    }
                }


            }
        });
    }

    private void setColumnCount(int count) {
        staggeredGridLayoutManager = new CustomGridLayoutManager(count, 1);
        staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(staggeredGridLayoutManager, this, this);

        socialFeedRV.setLayoutManager(staggeredGridLayoutManager);
        socialFeedRV.addOnScrollListener(endlessRecyclerViewScrollListener);
    }

    public void shortcutClicked() {
        if (socialFeedRV != null) {
            socialFeedRV.stopScroll();
            if (socialFeedRV.getVerticalScrollbarPosition() > 10) {
                socialFeedRV.scrollToPosition(0);
            } else {
                socialFeedRV.smoothScrollToPosition(0);
            }
        }

        if (feedTXT != null) {
            setBackToDefault();
        }
    }

    @Override
    public void onItemClick(WishListItem wishListItem) {
//        ((RouteActivity) getContext()).startItemActivity(wishListItem.id, wishListItem.owner.data.id, "i_like");
        mainActivity.startItemActivity(wishListItem.id, wishListItem.owner.data.id, "i_like");
        try {
            int firstCompletelyVisibleItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
            CustomVideoView video_view = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition).findViewById(R.id.videoView);
            ImageView iv_cover_below = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition + 1).findViewById(R.id.coverIV);

            iv_cover_below.setVisibility(View.VISIBLE);

            if (video_view.isPlaying()){
                video_view.stop();
            }


        } catch (NullPointerException e) {
            // Sometimes you scroll so fast that the views are not attached so it gives a NullPointerException
        } catch (ArrayIndexOutOfBoundsException e) {

        }
    }

    @Override
    public void onCommentClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startCommentActivity(wishListItem.id, wishListItem.title);
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        if (UserData.getUserItem().id == userItem.id) {
            ((RouteActivity) getContext()).startProfileActivity(userItem.id);
            return;
        }
        ((RouteActivity) getContext()).startProfileActivity(userItem.id);
    }

    @Override
    public void onGrabClick(WishListItem wishListItem) {
        String data = new Gson().toJson(wishListItem);
        mainActivity.startWishListActivity(data, "create_grab_other");
    }

    @Override
    public void onHideClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to hide?")
                .setNote("You will no longer see this post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Hide")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BlockRequest blockRequest = new BlockRequest(getContext());
                        blockRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("main_ProfileFragment_onHideClickDialog");
    }

    @Override
    public void onUnfollowClick(final WishListItem wishListItem) {
        if (wishListItem.owner.data.social.data.is_following.equals("yes")) {
            UnfollowDialog.newInstance(wishListItem.owner.data.id, wishListItem.owner.data.name, new UnfollowDialog.Callback() {
                @Override
                public void onAccept(int userID) {
                    UnFollowRequest unFollowRequest = new UnFollowRequest(getContext());
                    unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                            .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                            .addParameters(Keys.server.key.USER_ID, wishListItem.owner.data.id)
                            .execute();
                }

                @Override
                public void onCancel(int userID) {

                }
            }).show(((BaseActivity) getContext()).getSupportFragmentManager(), UnfollowDialog.TAG);
        } else {
            GroupDialog.newInstance(wishListItem.owner.data, this).show(getFragmentManager(), GroupDialog.TAG);
        }
        Analytics.trackEvent("main_ProfileFragment_onUnfollowClickDialog");
    }

    @Override
    public void onGroupSelect(String groupName, int id) {
        FollowRequest followRequest = new FollowRequest(getContext());
        followRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .addParameters(Keys.server.key.USER_ID, id)
                .addParameters(Keys.server.key.GROUP, groupName)
                .execute();
    }

    @Override
    public void onCancel(String groupName, int id) {

    }

    @Override
    public void onReportClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to report?")
                .setNote("You are about to report a post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Report")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportRequest reportRequest = new ReportRequest(getContext());
                        reportRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.TYPE, "wishlist")
                                .addParameters(Keys.server.key.REFERENCE_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("main_ProfileFragment_onReportClickDialog");
    }

    @Override
    public void onShareClick(WishListItem wishListItem) {
        shareTheApp(wishListItem);
    }

    public void shareTheApp(WishListItem wishListItem) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                wishListItem.owner.data.username + " posted " +
                        wishListItem.title + " on LYKA for " +
                        wishListItem.category + ". Check out what everyone likes and discover your modern wishlist app");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Share.URL + "/share/i/" + wishListItem.id);
        startActivity(Intent.createChooser(sharingIntent, "Share LYKA"));
        Analytics.trackEvent("main_ProfileFragment_shareTheApp");
    }

    @Override
    public void onHeartClick(WishListItem wishListItem) {
//        if (!wishListItem.wishListMin.is_liked) {
//            mainActivity.doAnimation();
//        }
        LikeRequest likeRequest = new LikeRequest(getContext());
        likeRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "image,owner.social")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                .execute();
        Analytics.trackEvent("main_ProfileFragment_onHeartClick");
    }

    @Override
    public void onRatingClick(final WishListItem wishListItem, final int rate) {
            if (UserData.getUserItem().kyc.data.contactNumberStatus.equalsIgnoreCase("pending")) {
                Log.e("EEEEEEEEEEE", ">>>>" + UserData.getUserItem().kyc.data.contactNumberStatus);
                final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
                confirmationDialog
                        .setDescription("Activate Lyka Wallet")
                        .setNote("Input your mobile number to start earning LYKA Gems.")
                        .setIcon(R.drawable.icon_information)
                        .setPositiveButtonText("Proceed")
                        .setPositiveButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mainActivity.startVerificationActivity("verificationS");
                                confirmationDialog.dismiss();
                            }
                        })
                        .setNegativeButtonText("Ignore")
                        .build(getFragmentManager());
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        LikeRequest likeRequest = new LikeRequest(getContext());
                        likeRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .addParameters(Keys.server.key.INCLUDE, "image,owner.social,images,info")
                                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                                .addParameters(Keys.server.key.COUNTER, rate)
                                .execute();
                    }
                }, 2000);

            }
        Log.e("EEEEEEEEEEEE", "rate: " + rate);
        Analytics.trackEvent("main_ProfileFragment_onRatingClick");
    }

    @Override
    public void onRateClick(WishListItem wishListItem) {
        mainActivity.startSearchActivity(wishListItem.id, wishListItem.owner.data.common_name, "likes");
    }

    @Override
    public void onPlayClick(WishListItem wishListItem) {

    }

    @Override
    public void onRepostClick(WishListItem wishListItem) {
        String data = new Gson().toJson(wishListItem);
        ((RouteActivity) getContext()).startWishListActivity(data, "create_grab_other");
        Analytics.trackEvent("main_ProfileFragment_onRepostClick");
    }

    @Override
    public void onRepostUsersClick(WishListItem wishListItem) {
        mainActivity.startSearchActivity(wishListItem.id, wishListItem.owner.data.common_name, "repost");
    }

    @Override
    public void onHeaderClick(EventItem eventItem) {
        mainActivity.startBirthdayActivity(eventItem, "event");
    }

    @Override
    public void onHeaderShowClick(EventItem eventItem) {
        mainActivity.startBirthdayActivity(eventItem, "greeting");
    }

    @Override
    public void onRefresh() {
        attemptSearch();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        // We need to pause any playback when the application is minimised

        try {
            int firstCompletelyVisibleItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
            CustomVideoView video_view = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition).findViewById(R.id.videoView);
            ImageView iv_cover_below = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition + 1).findViewById(R.id.coverIV);

            iv_cover_below.setVisibility(View.VISIBLE);
            if (video_view.isPlaying()){
                video_view.stop();
            }else{
                video_view.start();
            }

        } catch (NullPointerException e) {
            // Sometimes you scroll so fast that the views are not attached so it gives a NullPointerException
        } catch (ArrayIndexOutOfBoundsException e) {

        }

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        Glide.get(getContext()).clearMemory();
        try {
            int firstCompletelyVisibleItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
            CustomVideoView video_view = linearLayoutManager.findViewByPosition(firstCompletelyVisibleItemPosition).findViewById(R.id.videoView);

            if (video_view.isPlaying()){
                video_view.stop();
            }


        } catch (NullPointerException e) {
            // Sometimes you scroll so fast that the views are not attached so it gives a NullPointerException
        } catch (ArrayIndexOutOfBoundsException e) {

        }
        super.onStop();
    }

    @Subscribe
    public void onResponse(FeedRequest.ServerResponse responseData) {
        customSwipeRefreshLayout.refreshComplete();
        loadMorePB.setVisibility(GONE);

        FeedTransformer feedTransformer = responseData.getData(FeedTransformer.class);
        if (feedTransformer.status) {
            lastPage = feedTransformer.last_page;
            currentPage = feedTransformer.current_page;
            if (keyword.equalsIgnoreCase("random")){
                if (responseData.isNext()) {
                    socialFeedThreeRecycleViewAdapter.addNewData(feedTransformer.data);
                } else {
//                    WishListItem wishListItem = new WishListItem();
//                    wishListItem.id = Integer.parseInt(lastPage) * 5 + 1;
//                    wishListItem.pattern = 6;
//                    feedTransformer.data.add(Integer.parseInt(lastPage) * 5 + 1, wishListItem);
                    socialFeedThreeRecycleViewAdapter.addNewData(feedTransformer.data);
                }
            }else{
                if (responseData.isNext()) {
                    socialFeedThreeRecycleViewAdapter.addNewData(feedTransformer.data);
                } else {
                    endlessRecyclerViewScrollListener.reset();
                    socialFeedThreeRecycleViewAdapter.setNewData(feedTransformer.data);
                }
            }

            Log.e("EEEEEEEE", ">>>>>> KEYWORD >>>>>>" + feedTransformer.keyword);
            Log.e("EEEEEEEE", ">>>>>> Last Page >>>>>>" + feedTransformer.last_page);
            Log.e("EEEEEEEE", ">>>>>> Current Page >>>>>>" + feedTransformer.current_page);
            Log.e("EEEEEEEE", ">>>>>> Get Page >>>>>>" + feedRequest.getPage());

            if(feedTransformer.current_page.equals(feedTransformer.last_page)){
                ToastMessage.show(getContext(), "You've seen everything from your circle. Scroll for some post you might like.", ToastMessage.Status.SUCCESS);
                getRandomFeed();
            }
        }

        if (socialFeedRecyclerViewAdapter.getItemCount() == 0) {
            placeHolderCON.setVisibility(View.VISIBLE);
            socialFeedRV.setVisibility(GONE);
        } else {
            placeHolderCON.setVisibility(GONE);
            socialFeedRV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
            if(Integer.parseInt(currentPage) < Integer.parseInt(lastPage)){
                feedRequest.nextPage();
                Log.e("EEEEEEEE", ">>>>>>" + "NEXT");
            }


    }

    public void getRandomFeed(){
        feedRequest = new FeedRequest(getContext());
        feedRequest
                .setDeviceRegID(mainActivity.getDeviceRegID())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social,images")
                .addParameters(Keys.server.key.KEYWORD, random)
                .setPerPage(10)
                .nextPage();
    }

    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            socialFeedRecyclerViewAdapter.unFollow(userTransformer.userItem.id);
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(FollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            socialFeedRecyclerViewAdapter.follow(userTransformer.userItem.id);
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(BlockRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            socialFeedRecyclerViewAdapter.removeItem(wishListInfoTransformer.wishListItem);
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(LikeRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            socialFeedThreeRecycleViewAdapter.updateLikeCount(wishListInfoTransformer.wishListItem.id, wishListInfoTransformer.wishListItem.for_display_liker,wishListInfoTransformer.wishListItem.diamond_sent);
            UserData.updateRewardCrystal(wishListInfoTransformer.reward_crystal + "");
            UserData.updateRewardFragment(wishListInfoTransformer.reward_fragment + "");
//            mainActivity.updateReward(wishListInfoTransformer.lykagem_display + "", wishListInfoTransformer..reward_fragment + "");
        }
        if (!UserData.getBoolean(UserData.WALKTHRU_DONE) && UserData.getInt(UserData.WALKTHRU_CURRENT) == 7) {
            mainActivity.showChipsWalkThrough();
        }
    }

    @Subscribe
    public void onResponse(ReportRequest.ServerResponse responseData) {
        ReportTransformer reportTransformer = responseData.getData(ReportTransformer.class);
        if (reportTransformer.status) {
            if (reportTransformer.data.is_removed) {
                socialFeedRecyclerViewAdapter.removeItemByID(reportTransformer.data.reference_id);
            }
            ToastMessage.show(getContext(), reportTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), reportTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(BaseRequest.OnFailedResponse onFailedResponse) {
        customSwipeRefreshLayout.refreshComplete();
        loadMorePB.setVisibility(GONE);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shareBTN:
                mainActivity.shareTheApp();
                mainActivity.facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT, FacebookCustomLoggerMessage.MainActivity.SHARE);
                Analytics.trackEvent("main_ProfileFragment_shareBTN");
                break;
            case R.id.listViewBTN:
                listViewActive();
                Analytics.trackEvent("main_ProfileFragment_listViewBTN");
                break;
            case R.id.gridViewBTN:
                gridViewActive();
                Analytics.trackEvent("main_ProfileFragment_gridViewBTN");
                break;
            case R.id.viewTypeBTN:
                switchView();
                Analytics.trackEvent("main_ProfileFragment_viewTypeBTN");
                break;
            case R.id.feedTXT:
                FeedCategoryDialog.newInstance(this).show(getFragmentManager(), FeedCategoryDialog.TAG);
                Analytics.trackEvent("main_ProfileFragment_feedTXT");
                break;
            case R.id.eraseBTN:
                setBackToDefault();
                Analytics.trackEvent("main_ProfileFragment_eraseBTN");
                break;
            case R.id.rewardsBTN:
                mainActivity.startRewardsActivity("shop");
                Analytics.trackEvent("main_ProfileFragment_rewardsBTN");
                break;
            case R.id.birthDateBTN:
                mainActivity.startBirthdayActivity("celebrant");
                Analytics.trackEvent("main_ProfileFragment_birthDateBTN");
                break;
        }
    }

    private void switchView(){
        if (viewTypeBTN.getTag().equals("list")){
            gridViewActive();
            viewTypeBTN.setTag("grid");
        }else{
            listViewActive();
            viewTypeBTN.setTag("list");
        }
    }

    private void setBackToDefault() {
        keyword = "RECENT POSTS";
        eraseBTN.setVisibility(GONE);
        attemptSearch();
    }

    private void listViewActive() {
        viewTypeBTN.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_grid_big_selected));
        socialFeedRV.removeItemDecoration(spacesItemDecoration);
        spacesItemDecoration = new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.social_feed_spacing), getResources().getDimensionPixelSize(R.dimen.social_feed_no_spacing));
        socialFeedRV.addItemDecoration(spacesItemDecoration);
        setColumnCount(1);
        socialFeedRecyclerViewAdapter.setFeedView(SocialFeedRecyclerViewAdapter.LIST);
    }

    private void gridViewActive() {
        viewTypeBTN.setImageDrawable(ActivityCompat.getDrawable(getContext(), R.drawable.icon_list_view_selected));
        socialFeedRV.removeItemDecoration(spacesItemDecoration);
        spacesItemDecoration = new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.social_feed_spacing));
        socialFeedRV.addItemDecoration(spacesItemDecoration);
        setColumnCount(2);

        socialFeedRecyclerViewAdapter.setFeedView(SocialFeedRecyclerViewAdapter.GRID);
    }

    @Override
    public void onHide() {
        shareBTN.setVisibility(GONE);
        rewardsCON.setVisibility(GONE);
    }

    @Override
    public void onShow() {
        rewardsCON.setVisibility(GONE);
    }

    @Override
    public void onScrolled(RecyclerView view, int dx, int dy, int totalScrolled) {
        if (totalScrolled < 10) {
            if (staggeredGridLayoutManager != null) {
                staggeredGridLayoutManager.invalidateSpanAssignments();
            }
        }
    }

    public void attemptSearch() {
        if (keyword.equalsIgnoreCase("hot items")){
            keyword = "MOST LIKES";
        }

        if (keyword.equalsIgnoreCase("trending")){
            keyword = "MOST REPOSTS";
        }

        feedTXT.setText(keyword);

        if (customSwipeRefreshLayout != null) {
            customSwipeRefreshLayout.startRefresh();
        }

        if (endlessRecyclerViewScrollListener != null) {
            endlessRecyclerViewScrollListener.reset();
        }

        if (socialFeedRV != null) {
            socialFeedRV.scrollToPosition(0);
        }

        feedRequest = new FeedRequest(getContext());
        feedRequest
                .setDeviceRegID(mainActivity.getDeviceRegID())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social,images")
                .addParameters(Keys.server.key.KEYWORD, keyword)
                .setPerPage(10)
                .first();

//        userInfoRequest = new UserInfoRequest(getContext());
//        userInfoRequest
//                .setDeviceRegID(mainActivity.getDeviceRegID())
//                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "info,statistics,social,wallet,kyc")
//                .addParameters(Keys.server.key.USER_ID, UserData.getUserId())
//                .execute();
    }

    public void attemptSearchOthers(String keyword) {
        this.keyword = keyword;
        eraseBTN.setVisibility(View.VISIBLE);
        attemptSearch();
    }

    @Override
    public void searchTextChange(String keyword) {
        this.keyword = keyword;
        attemptSearchOthers(keyword);
    }

    @Override
    public void onItemSelect(String keyword, boolean isOthers) {
        this.keyword = keyword;
        if (isOthers) {
            attemptSearchOthers(keyword);
        } else {
            attemptSearch();
        }
    }

    @Override
    public void onGemsClick() {
//        if (socialFeedRecyclerViewAdapter != null) {
//            if (socialFeedRecyclerViewAdapter.getData().size() > 0 && (UserData.getInt(UserData.WALKTHRU_CURRENT) == 7)) {
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
////                    socialFeedRV.scrollBy(0, socialFeedRecyclerViewAdapter.getData().get(1));
//                        socialFeedRV.scrollToPosition(2);
//                    }
//                }, 300);
//                if (socialFeedRV != null) {
////                   socialFeedRV.smoothScrollToPosition(1);
//                    WishListItem wishListItem = socialFeedRecyclerViewAdapter.getData().get(0);
//                    wishListItem.walkthrough_on_process_like = !UserData.getBoolean(UserData.WALKTHRU_DONE);
//                    wishListItem.wishListMin.is_liked = false;
//                    socialFeedRecyclerViewAdapter.updateData(wishListItem);
//                    socialFeedRecyclerViewAdapter.notifyDataSetChanged();
//                }
//            }

//        }
    }

    @Override
    public void onFinalClick() {
        mainActivity.startRewardsActivity("shop");
    }

//    @Subscribe
//    public void onResponse(UserInfoRequest.ServerResponse responseData) {
//        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
//        if (userTransformer.status) {
//           UserData.insert(userTransformer.userItem);
//            Log.e("EEEEEEEEEEEE", "rate: " + userTransformer.userItem.id);
//            Log.e("EEEEEEEEEEEE", "rate: " + userTransformer.userItem.kyc.data.contactNumberStatus);
//        }else{
//            ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.FAILED);
//        }
//    }

}
