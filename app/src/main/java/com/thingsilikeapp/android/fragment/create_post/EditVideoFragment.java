package com.thingsilikeapp.android.fragment.create_post;

import android.app.ProgressDialog;

import android.content.ContentUris;
import android.content.Context;


import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;


import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
//import android.widget.MediaController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.danikula.videocache.HttpProxyCacheServer;
//import com.github.tcking.viewquery.ViewQuery;

import com.thingsilikeapp.android.fragment.create_post.MediaController;

import com.iceteck.silicompressorr.SiliCompressor;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.CreatePostActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;



import java.io.ByteArrayOutputStream;
import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;


import butterknife.BindView;
import icepick.State;
//import tcking.github.com.giraffeplayer2.VideoInfo;

public class
EditVideoFragment extends BaseFragment  implements View.OnClickListener{
//, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener
    public static final String TAG = EditVideoFragment.class.getName();

    private CreatePostActivity createPostActivity;

    @BindView(R.id.titleTXT)            TextView textViewTxt;
    @BindView(R.id.videoView)           VideoView videoView;
    @BindView(R.id.nextBTN)             TextView nextBTN;
    @BindView(R.id.mainBackButtonIV)	View mainBackButtonIV;


    @State File file;

    public static EditVideoFragment newInstance(File file) {
        EditVideoFragment fragment = new EditVideoFragment();
        fragment.file = file;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_video;
    }

    @Override
    public void onViewReady() {
        createPostActivity = (CreatePostActivity) getContext();
        textViewTxt.setText("Preview");
        nextBTN.setOnClickListener(this);
        mainBackButtonIV.setOnClickListener(this);
        setupVideo();

    }
    public void setupVideo() {
        Uri uri = Uri.fromFile(file);
//        Uri uri = Uri.parse("https://teststreaming-aase.streaming.media.azure.net/9423\afaa-e456-4ed6-b3b3-5fa12cb4a70e/SampleVideo_1280x720_1mb_960x540_AACAudio_760.mp4");
        final MediaController controller = MediaController.getInstance();
        controller.setMediaPlayer(videoView);
        controller.setVisibility(View.VISIBLE);
        videoView.setMediaController(controller.setMediaPlayer());
        videoView.setVideoURI(uri);
        videoView.start();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }

        });
    }

    private File createImageDir(){
        File storageDir = new File(Environment.getExternalStorageDirectory(), "Android/media/com.thingsilikeapp");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        return storageDir;
    }

 private class VideoCompressAsyncTask extends AsyncTask<String, String, String> {
        ProgressDialog asyncDialog = new ProgressDialog(getContext());
        Context mContext;
        File thumbnail;

        public VideoCompressAsyncTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            asyncDialog.setMessage("Processing...");
            asyncDialog.show();
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... paths) {
                File storageDir = new File(Environment.getExternalStorageDirectory(),
                        "Android/media/com.thingsilikeapp");
                if (!storageDir.exists()) {
                    storageDir.mkdirs();
                }
          boolean isconverted = MediaController.getInstance().convertVideo(file.toString(),
                  storageDir, 0, 0, 0);

            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(MediaController.cachedFile.getPath(), MediaStore.Video.Thumbnails.MINI_KIND);
                thumbnail = getImageFile(thumb);
            return MediaController.cachedFile.getPath();

        }

        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
            //hide the dialog
            asyncDialog.dismiss();
            File videoFile = new File(compressedFilePath);
            createPostActivity.openSendVideoPost(videoFile, thumbnail);
        }
    }


    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.mainBackButtonIV:
                createPostActivity.onBackPressed();
                break;
            case R.id.nextBTN:
                new VideoCompressAsyncTask(getContext()).execute();
                break;

        }
    }

    private File getImageFile(Bitmap bitmap) {
        String imageFileName = "ThingsILikeCacheImage";
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    createImageDir()      /* directory */
            );
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
            byte[] bitmapData = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(image);
            fos.write(bitmapData);
            fos.flush();
            fos.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

}
