package com.thingsilikeapp.android.fragment.rewards.exchange;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.adapter.BankAccountRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.android.dialog.CountryDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.BankItem;
import com.thingsilikeapp.data.model.ChatThreadModel;
import com.thingsilikeapp.data.preference.CountryData;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.server.request.Chat;
import com.thingsilikeapp.server.request.GetCountryRequest;
import com.thingsilikeapp.server.transformer.GetCountryTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.request.APIResponse;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class AddBankAccountFragment extends BaseFragment implements View.OnClickListener,  CountryDialog.CountryPickerListener {

    public static final String TAG = AddBankAccountFragment.class.getName();

    public static AddBankAccountFragment newInstance(String currency, String amount, String totalAmount, String serviceFee, String rate, String conversion, Callback callback) {
        AddBankAccountFragment fragment = new AddBankAccountFragment();
        fragment.currency = currency;
        fragment.amount = amount;
        fragment.totalAmount = totalAmount;
        fragment.serviceFee = serviceFee;
        fragment.conversion = rate;
        fragment.rate = conversion;
        fragment.callback = callback;
        return fragment;
    }

    @BindView(R.id.addBTN)          TextView addBTN;
    @BindView(R.id.countryFlagIV)   ImageView countryFlagIV;
    @BindView(R.id.contactCodeTXT)  TextView contactCodeTXT;
    @BindView(R.id.contactET)       EditText contactET;
    @BindView(R.id.bankNameET)      EditText bankNameET;
    @BindView(R.id.bankAddressET)   EditText bankAddressET;
    @BindView(R.id.bankSwiftCodeET) EditText bankSwiftCodeET;
    @BindView(R.id.accountAddressET)EditText accountAddressET;
    @BindView(R.id.nameET)          EditText nameET;
    @BindView(R.id.emailET)         EditText emailET;
    @BindView(R.id.numberET)        EditText numberET;
    @BindView(R.id.primaryBTN)      Switch primaryBTN;
    @BindView(R.id.contactCON)      View contactCON;


    @State String country;
    @State String contry_code;
    @State String is_primary;

    @State String currency;
    @State String amount;
    @State String totalAmount;
    @State String serviceFee;
    @State String conversion;
    @State String rate;

    private ExchangeActivity exchangeActivity;
    private CountryData.Country countryData;
    private APIRequest apiRequest;
    private Callback callback;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_add_bank;
    }

    @Override
    public void onViewReady() {
        exchangeActivity = (ExchangeActivity)getContext();
        exchangeActivity.setTitle("Add Bank Account");
        addBTN.setOnClickListener(this);
        getCountry();
        contactCON.setOnClickListener(this);
        Analytics.trackEvent("rewards_exchange_AddBankAccountFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void addBank(){
        if (primaryBTN.isChecked()){
            is_primary = "yes";
        }else{
            is_primary = "no";
        }

        apiRequest = Wallet.getDefault().addBank(getContext())
                .addParameter(Keys.server.key.BANK_NAME, bankNameET.getText().toString())
                .addParameter(Keys.server.key.BANK_ADDRESS, bankAddressET.getText().toString())
                .addParameter(Keys.server.key.BANK_SWIFT_CODE, bankSwiftCodeET.getText().toString())
                .addParameter(Keys.server.key.ACCOUNT_NAME, nameET.getText().toString())
                .addParameter(Keys.server.key.ACCOUNT_NUMBER, numberET.getText().toString())
                .addParameter(Keys.server.key.ACCOUNT_ADDRESS, accountAddressET.getText().toString())
                .addParameter(Keys.server.key.COUNTRY_ISO, countryData.code1)
                .addParameter(Keys.server.key.COUNTRY_CODE, countryData.code3)
                .addParameter(Keys.server.key.CONTACT_NUMBER, countryData.code3 + contactET.getText().toString())
                .addParameter(Keys.server.key.EMAIL, emailET.getText().toString())
                .addParameter(Keys.server.key.IS_PRIMARY, is_primary);
        apiRequest.execute();
    }

    @Subscribe
    public void onResponse(GetCountryRequest.ServerResponse responseData) {
        GetCountryTransformer getCountryTransformer = responseData.getData(GetCountryTransformer.class);
        if(getCountryTransformer.status){
            countryData = CountryData.getCountryDataByCode1(getCountryTransformer.data.country);
            contactCodeTXT.setText(countryData.code3);
            countryCode(countryData);
        }else{
            ToastMessage.show(getContext(), getCountryTransformer.msg, ToastMessage.Status.FAILED);

        }
    }

    private void getCountry(){
        GetCountryRequest createCommentRequest = new GetCountryRequest(getContext());
        createCommentRequest
                .showNoInternetConnection(false)
                .execute();
    }

    private void countryCode(CountryData.Country country){
        contactCodeTXT.setText(country.code3);
        this.country = countryData.code1;
        this.contry_code = countryData.code3;
        Glide.with(getContext())
                .load(country.getFlag())
                .into(countryFlagIV);
    }

    private void showCountryPicker(){
        CountryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
    }


    @Subscribe
    public void onResponse(Wallet.AddBankResponse responseData) {
        SingleTransformer<BankItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            exchangeActivity.openSelectBankFragment(currency, amount, totalAmount, serviceFee, rate, conversion);
        }else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.addBTN:
                addBank();
                Analytics.trackEvent("rewards_exchange_AddBankAccountFragment_addBTN");
                break;
            case R.id.contactCON:
                showCountryPicker();
                Analytics.trackEvent("rewards_exchange_AddBankAccountFragment_contactCON");
                break;
        }
    }

    @Override
    public void onSelectCountry(CountryData.Country country, int requestCode) {
        countryData = country;
        countryCode(countryData);
    }

    public interface Callback{
        void onSuccess(String currency, String amount, String totalAmount, String serviceFee, String rate, String conversion);
    }
}
