package com.thingsilikeapp.android.fragment.settings;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;
import com.thingsilikeapp.android.adapter.BlockUserRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.ReportUserRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.UnReportUserDialog;
import com.thingsilikeapp.data.model.BlockUserItem;
import com.thingsilikeapp.data.model.ReportUserItem;
import com.thingsilikeapp.server.ReportUser;
import com.thingsilikeapp.server.Settings;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ReportedUsersFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        EndlessRecyclerViewScrollListener.Callback, ReportUserRecycleViewAdapter.ClickListener, UnReportUserDialog.Callback{

    public static final String TAG = ReportedUsersFragment.class.getName();

    private SettingsActivity settingsActivity;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;
    private ReportUserRecycleViewAdapter reportUserRecycleViewAdapter;

    private APIRequest apiRequest;

    @BindView(R.id.reportRV)               RecyclerView reportRV;
    @BindView(R.id.reportSRL)              SwipeRefreshLayout reportSRL;



    public static ReportedUsersFragment newInstance() {
        ReportedUsersFragment reportedUsersFragment = new ReportedUsersFragment();
        return reportedUsersFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_reported_users;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle("Reported Users");
        setupBlockListView();
        initSuggestionAPI();
        reportSRL.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void setupBlockListView() {
        reportUserRecycleViewAdapter = new ReportUserRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        reportRV.setLayoutManager(linearLayoutManager);
        reportRV.setAdapter(reportUserRecycleViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager,
                this);
        reportRV.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        reportRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        reportRV.setLayoutManager(linearLayoutManager);
        reportUserRecycleViewAdapter.setOnItemClickListener(this);
    }

    private void refreshList(){
//        endlessRecyclerViewScrollListener.reset();
       apiRequest =  ReportUser.getDefault().getReportUser(getContext(), reportSRL);
       apiRequest.first();
//        followersRequest
//                .showSwipeRefreshLayout(true)
//                .clearParameters()
//                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
//                .addParameters(Keys.server.key.USER_ID, userID)
//                .addParameters(Keys.server.key.KEYWORD, searchET.getText().toString())
//                .first();
    }

    private void initSuggestionAPI(){
        reportSRL.setColorSchemeResources(R.color.colorPrimary);
        reportSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        reportSRL.setOnRefreshListener(this);
    }

    @Subscribe
    public void onResponse(ReportUser.AllOrderResponse colletionResponse) {
        CollectionTransformer<ReportUserItem> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (colletionResponse.isNext()) {
                reportUserRecycleViewAdapter.addNewData(collectionTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                reportUserRecycleViewAdapter.setNewData(collectionTransformer.data);
            }
        }

    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onItemClick(ReportUserItem reportUserItem) {

    }

    @Override
    public void onUnreportUserClick(ReportUserItem reportUserItem) {
        UnReportUserDialog.newInstance(reportUserItem, this).show(getFragmentManager(),TAG);
        refreshList();
    }

    @Override
    public void onItemLongClick(ReportUserItem reportUserItem) {

    }

    @Override
    public void onSuccess() {
        refreshList();
    }
}
