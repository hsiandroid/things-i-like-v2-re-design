package com.thingsilikeapp.android.fragment.rewards;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.dialog.WebViewDialog;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class GeneralInfoFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = GeneralInfoFragment.class.getName();

    private RewardsActivity rewardsActivity;

    @BindView(R.id.privacyTXT)          TextView privacyTXT;
    @BindView(R.id.supportTXT)          TextView supportTXT;

    public static GeneralInfoFragment newInstance() {
        GeneralInfoFragment generalInfoFragment = new GeneralInfoFragment();
        return generalInfoFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_general_info;
    }

    @Override
    public void onViewReady() {
        rewardsActivity = (RewardsActivity) getContext();
        rewardsActivity.setTitle("General Information");
        rewardsActivity.showOption(false);
        rewardsActivity.showWallet(false);
        setClickableGmailString();
        setClickablePrivacyString();
//        supportTXT.setText(formatGmailLink());
        Analytics.trackEvent("rewards_GeneralInfoFragment_onViewReady");
    }

    private void setClickableGmailString(){
        final SpannableString ss = new SpannableString("You may email us at support@thingsilikeapp.com for your concerns and suggestions. ");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                redirectToGmail("support@thingsilikeapp.com");
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        ss.setSpan(clickableSpan, 20, 46, Spanned.SPAN_INTERMEDIATE);

        supportTXT.setMovementMethod(LinkMovementMethod.getInstance());
        supportTXT.setText(ss);
    }
    private void setClickablePrivacyString(){
        String s="terms and conditions";
        final SpannableString ss = new SpannableString("Our platform is guided by the terms and conditions of our Privacy Policy");
        String first ="terms and conditions";
        int firstIndex = s.indexOf(first);

        ClickableSpan firstwordClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                WebViewDialog.newInstance("http://privacypolicy.technology/kpp.php").show(getChildFragmentManager(), WebViewDialog.TAG);
            }
        };

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                WebViewDialog.newInstance("http://privacypolicy.technology/kpp.php").show(getChildFragmentManager(), WebViewDialog.TAG);

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };

        ss.setSpan(firstwordClick,30, 50, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan, ss.length() -14, ss.length(), Spanned.SPAN_INTERMEDIATE);
        privacyTXT.setMovementMethod(LinkMovementMethod.getInstance());
        privacyTXT.setText(ss);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
        }
    }

    public void openUrl(String url){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    private void redirectToGmail(String email){
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { email });
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Things I Like App");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Send your message.");
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://mail.google.com/mail/u/0/#sent?compose=new"));
        try {
            startActivity(emailIntent);
        }catch (ActivityNotFoundException ex){
            startActivity(webIntent);
        }
    }
}
