package com.thingsilikeapp.android.fragment.messages;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.microsoft.appcenter.analytics.Analytics;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MessageActivity;
import com.thingsilikeapp.android.adapter.GroupChatMemberRecyclerViewAdapter;
import com.thingsilikeapp.android.adapter.GroupChatModeratorRecyclerViewAdapter;
import com.thingsilikeapp.android.dialog.GroupChatAddMemberDialog;
import com.thingsilikeapp.android.dialog.ImagePickerDialog;
import com.thingsilikeapp.android.dialog.ImagePickerV2Dialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.data.model.ChatModel;
import com.thingsilikeapp.data.model.ChatUserModel;
import com.thingsilikeapp.server.request.Chat;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import butterknife.BindView;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ChatEditFragment extends BaseFragment implements View.OnClickListener,
        GroupChatMemberRecyclerViewAdapter.ClickListener, ImagePickerDialog.ImageCallback,
        GroupChatModeratorRecyclerViewAdapter.ClickListener, SwipeRefreshLayout.OnRefreshListener, ImagePickerV2Dialog.ImageCallback, GroupChatAddMemberDialog.Callback {

    public static final String TAG = ChatEditFragment.class.getName();

    public static ChatEditFragment newInstance(int chatID) {
        ChatEditFragment fragment = new ChatEditFragment();
        fragment.chat_id = chatID;
        return fragment;
    }

    private GroupChatMemberRecyclerViewAdapter groupChatMemberRecyclerViewAdapter;
    private GroupChatModeratorRecyclerViewAdapter groupChatModeratorRecyclerViewAdapter;

    private LinearLayoutManager menteesLLM;
    private LinearLayoutManager layoutManager;
    private MessageActivity chatActivity;

    @BindView(R.id.editMenteesRV)               RecyclerView editMenteesRV;
    @BindView(R.id.editChatModeratorRV)         RecyclerView editChatModeratorRV;
    @BindView(R.id.addMemberBTN)                LinearLayout addMemberBTN;
    @BindView(R.id.groupNameBTN)                LinearLayout groupNameBTN;
    @BindView(R.id.addBannerBTN)                ImageView addBannerBTN;
    @BindView(R.id.groupNameTXT)                TextView groupNameTXT;
    @BindView(R.id.leaveBTN)                    TextView leaveBTN;
    @BindView(R.id.editChatSRL)                 SwipeRefreshLayout editChatSRL;
    @BindView(R.id.mainTitleTXT)                TextView mainTitleTXT;
    @BindView(R.id.mainBackButtonIV)            ImageView mainBackButtonIV;
    @BindView(R.id.infoBTN)                     ImageView infoBTN;


    @State
    File file;
    @State int chat_id;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_chat;
    }

    @Override
    public void onViewReady() {
        chatActivity = (MessageActivity) getContext();
        setModerator();
        setMembers();
        groupNameBTN.setOnClickListener(this);
        addMemberBTN.setOnClickListener(this);
        addBannerBTN.setOnClickListener(this);
        leaveBTN.setOnClickListener(this);
        editChatSRL.setOnRefreshListener(this);
        editChatSRL.setColorSchemeResources(R.color.colorPrimary);
        Chat.getDefault().detail(getContext(), chat_id);
        mainTitleTXT.setText("Chat Settings");
        infoBTN.setVisibility(View.GONE);
        mainBackButtonIV.setOnClickListener(this);
        Analytics.trackEvent("messages_ChatEditFragment_onViewReady");
    }

    private void refreshList(){
        Chat.getDefault().allMember(getContext(), chat_id, editChatSRL);
        Chat.getDefault().allModerator(getContext(), chat_id, editChatSRL);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void setMembers(){
        menteesLLM = new LinearLayoutManager(getContext());
        groupChatMemberRecyclerViewAdapter = new GroupChatMemberRecyclerViewAdapter(getContext());
        editMenteesRV.setLayoutManager(menteesLLM);
        editMenteesRV.setAdapter(groupChatMemberRecyclerViewAdapter);
        editMenteesRV.setNestedScrollingEnabled(false);
        groupChatMemberRecyclerViewAdapter.setOnItemClickListener(this);
    }


    private void setModerator(){
        layoutManager = new LinearLayoutManager(getContext());
        groupChatModeratorRecyclerViewAdapter = new GroupChatModeratorRecyclerViewAdapter(getContext());
        editChatModeratorRV.setLayoutManager(layoutManager);
        editChatModeratorRV.setAdapter(groupChatModeratorRecyclerViewAdapter);
        editChatModeratorRV.setNestedScrollingEnabled(false);
        groupChatModeratorRecyclerViewAdapter.setOnItemClickListener(this);
    }


    @Subscribe
    public void onResponse(Chat.CollectionModeratorResponse colletionResponse) {
        CollectionTransformer<ChatUserModel> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            if (colletionResponse.isNext()){
                groupChatModeratorRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else {
                groupChatModeratorRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
            if (groupChatModeratorRecyclerViewAdapter.getData().size() == 0){
            }else {
            }
        }
    }

    @Subscribe
    public void onResponse(Chat.CollectionMemberResponse colletionResponse) {
        CollectionTransformer<ChatUserModel> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            if (colletionResponse.isNext()){
                groupChatMemberRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else {
                groupChatMemberRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
            if (groupChatMemberRecyclerViewAdapter.getData().size() == 0){
            }else {
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addMemberBTN:
                GroupChatAddMemberDialog.newInstance(chat_id, this).show(getFragmentManager(), GroupChatAddMemberDialog.TAG);
                Analytics.trackEvent("messages_ChatChangeNameFragment_addMemberBTN");
                break;
            case R.id.groupNameBTN:
                chatActivity.openChangeNameFragment(chat_id);
                Analytics.trackEvent("messages_ChatChangeNameFragment_groupNameBTN");
                break;
            case R.id.mainBackButtonIV:
                chatActivity.onBackPressed();
                Analytics.trackEvent("messages_ChatChangeNameFragment_mainBackButtonIV");
                break;
            case R.id.addBannerBTN:
                ImagePickerV2Dialog.newInstance("Add Banner", true, this).show(getChildFragmentManager(), ImagePickerV2Dialog.TAG);
                Analytics.trackEvent("messages_ChatChangeNameFragment_addBannerBTN");
                break;
            case R.id.leaveBTN:
                final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
                confirmationDialog
                        .setDescription("Leave Group?")
                        .setNote("Are you sure you want to Leave/Close this group?")
                        .setIcon(R.drawable.icon_information)
                        .setPositiveButtonText("Leave")
                        .setPositiveButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Chat.getDefault().leave(getContext(), chat_id);
                                confirmationDialog.dismiss();
                            }
                        })
                        .setNegativeButtonText("Cancel")
                        .setNegativeButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                confirmationDialog.dismiss();
                            }
                        })
                        .build(getChildFragmentManager());
                Analytics.trackEvent("messages_ChatChangeNameFragment_leaveBTN");
                break;

        }
    }


    @Subscribe
    public void onResponse(Chat.ChatDetailResponse responseData) {
        SingleTransformer<ChatModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            groupNameTXT.setText(singleTransformer.data.title);
            Picasso.with(getContext())
                    .load(singleTransformer.data.info.data.avatar.fullPath)
                    .centerCrop()
                    .fit()
                    .into(addBannerBTN);
            Log.e("EEEEEEE", ">>>>>" + singleTransformer.data.info.data.avatar.fullPath);
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Chat.ChatIconResponse responseData) {
        SingleTransformer<ChatModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Chat.DemoteUserResponse responseData) {
        SingleTransformer<ChatUserModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            refreshList();
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);

        }
    }

    @Subscribe
    public void onResponse(Chat.RemoveUserResponse responseData) {
        SingleTransformer<ChatUserModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            refreshList();
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Chat.LeaveResponse responseData) {
        SingleTransformer<ChatUserModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            chatActivity.openMessageFragment();
        } else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Chat.PromoteUserResponse responseData) {
        SingleTransformer<ChatUserModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            refreshList();
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void result(File file) {
        this.file = file;
        Picasso.with(getContext())
                .load(file)
                .centerCrop()
                .fit()
                .into(addBannerBTN);
        Chat.getDefault().icon(getContext(), file, chat_id);
    }

    @Override
    public void onMenteeClick(ChatUserModel chatUserModel) {

    }

    @Override
    public void onPromote(ChatUserModel chatUserModel) {
        Chat.getDefault().promoteParticipant(getContext(), chat_id, chatUserModel.userId);
        Log.e("DETAILS","detail" + chatUserModel.userId);
        refreshList();
    }

    @Override
    public void onRemoveClick(ChatUserModel chatUserModel) {
        Chat.getDefault().removeParticipant(getContext(), chat_id, chatUserModel.userId);
        refreshList();
    }

    @Override
    public void onAvatarClick(ChatUserModel mentorshipUserModel) {
        if (mentorshipUserModel.author.data.idX != 1){
//            ProfileDialog.newInstance(mentorshipUserModel.userId).show(getFragmentManager(), TAG);
        }
    }

    @Override
    public void onDemote(ChatUserModel chatUserModel) {
        Chat.getDefault().demoteParticipant(getContext(), chat_id, chatUserModel.userId);
        refreshList();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onSuccess() {
        refreshList();
    }
}
