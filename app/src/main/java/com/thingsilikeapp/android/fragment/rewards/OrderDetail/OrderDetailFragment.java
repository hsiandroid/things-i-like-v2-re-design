package com.thingsilikeapp.android.fragment.rewards.OrderDetail;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.adapter.LogsRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.OrderTimelineAdapter;
import com.thingsilikeapp.android.adapter.TimelineAdapter;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.data.model.EncashHistoryItem;
import com.thingsilikeapp.data.model.LogsItem;
import com.thingsilikeapp.data.model.OrderHistoryItem;
import com.thingsilikeapp.data.model.OrderItem;
import com.thingsilikeapp.server.Order;
import com.thingsilikeapp.server.request.Chat;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.widget.ExpandableHeightListView;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class OrderDetailFragment extends BaseFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = OrderDetailFragment.class.getName();

    private RewardsActivity rewardsActivity;
    private OrderTimelineAdapter orderTimelineAdapter;

    @State int id;
    private OrderItem orderItem;

    @BindView(R.id.refTXT) 				TextView refTXT;
    @BindView(R.id.titleTXT) 			TextView titleTXT;
    @BindView(R.id.statusTXT) 			TextView statusTXT;
    @BindView(R.id.dateTXT) 			TextView dateTXT;
    @BindView(R.id.priceTXT) 			TextView priceTXT;
    @BindView(R.id.qtyTXT) 			    TextView qtyTXT;
    @BindView(R.id.subtotalTXT) 	    TextView subtotalTXT;
    @BindView(R.id.deliveryStreetTXT) 	TextView deliveryStreetTXT;
    @BindView(R.id.deliveryCityTXT) 	TextView deliveryCityTXT;
    @BindView(R.id.deliveryStateTXT) 	TextView deliveryStateTXT;
    @BindView(R.id.deliveryCountryTXT) 	TextView deliveryCountryTXT;
    @BindView(R.id.cancelBTN) 	        TextView cancelBTN;
    @BindView(R.id.ordernoteTXT) 	    TextView ordernoteTXT;
    @BindView(R.id.editBTN) 	        TextView editBTN;
    @BindView(R.id.iLikeIV)             ImageView iLikeIV;
    @BindView(R.id.timelineEHLV)        ExpandableHeightListView timelineEHLV;
    @BindView(R.id.optionLayout)        LinearLayout optionLayout;
    @BindView(R.id.itemSRL)             SwipeRefreshLayout itemSRL;


    public static OrderDetailFragment newInstance(int id) {
        OrderDetailFragment fragment = new OrderDetailFragment();
        fragment.id = id;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_order_detail;
    }

    @Override
    public void onViewReady() {
        rewardsActivity = (RewardsActivity) getContext();
        rewardsActivity.setTitle("Order Details");
        rewardsActivity.showWallet(false);
        rewardsActivity.showOption(false);
        rewardsActivity.setTitleDark(false);
        rewardsActivity.showWallet(false);
        cancelBTN.setOnClickListener(this);
        editBTN.setOnClickListener(this);
        itemSRL.setOnRefreshListener(this);
        Analytics.trackEvent("rewards_OrderDetail_OrderDetailsFragment_onViewReady");

    }

    private void setupTimelineListView(List<OrderHistoryItem> orderHistoryItem){
        orderTimelineAdapter= new OrderTimelineAdapter(getContext());
        timelineEHLV.setAdapter(orderTimelineAdapter);
        orderTimelineAdapter.setNewData(orderHistoryItem);
    }

    private void display(OrderItem orderItem){
        this.orderItem = orderItem;
        refTXT.setText(orderItem.displayId);
        titleTXT.setText(orderItem.detail.data.get(0).title);
        subtotalTXT.setText(orderItem.detail.data.get(0).subTotal);
        priceTXT.setText(orderItem.detail.data.get(0).price);
        qtyTXT.setText(orderItem.detail.data.get(0).itemQty);
        dateTXT.setText(orderItem.purchasedDate.date);
        deliveryStreetTXT.setText(orderItem.deliveryStreet);
        deliveryCityTXT.setText(orderItem.deliveryCity);
        deliveryStateTXT.setText(orderItem.deliveryState);
        deliveryCountryTXT.setText(orderItem.deliveryCountry);
        ordernoteTXT.setText(orderItem.note);
        setupTimelineListView(orderItem.history.data);
        Picasso.with(getContext()).load(orderItem.detail.data.get(0).fullPath).centerCrop().fit().into(iLikeIV);

        switch (orderItem.statusDisplay){
            case "cancelled":
            case "dispute":
            case "refund":
                statusTXT.setBackgroundResource(R.color.red);
                optionLayout.setVisibility(View.GONE);
                break;
            case "pending":
            case "for_approval":
                statusTXT.setBackgroundResource(R.color.gray);
                optionLayout.setVisibility(View.VISIBLE);
                break;
            case "closed":
                statusTXT.setBackgroundResource(R.color.gray);
                optionLayout.setVisibility(View.GONE);
                break;
            case "delivered":
            case "completed":
                statusTXT.setBackgroundResource(R.color.completed);
                optionLayout.setVisibility(View.GONE);
                break;
            case "in_transit":
                statusTXT.setBackgroundResource(R.color.orange);
                optionLayout.setVisibility(View.GONE);
                break;
            case "for_delivery":
                statusTXT.setBackgroundResource(R.color.colorPrimary);
                optionLayout.setVisibility(View.GONE);
                break;
        }

        String upperString = orderItem.statusDisplay.substring(0,1).toUpperCase() + orderItem.statusDisplay.substring(1);
        String format = upperString.replace("_", " ");
        statusTXT.setText(format);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Subscribe
    public void onResponse(Order.OrderResponse responseData) {
        SingleTransformer<OrderItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            rewardsActivity.startRewardsActivity("treasury");
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Order.ShowOrderResponse responseData) {
        SingleTransformer<OrderItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            display(singleTransformer.data);
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.cancelBTN:
                final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
                confirmationDialog
                        .setDescription("Cancel order")
                        .setNote("Are you sure you want to cancel this order?")
                        .setIcon(R.drawable.icon_information)
                        .setPositiveButtonText("Cancel")
                        .setPositiveButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Order.getDefault().cancel(getContext(), id);
                                confirmationDialog.dismiss();
                            }
                        })
                        .setNegativeButtonText("Go back")
                        .setNegativeButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                confirmationDialog.dismiss();
                            }
                        })
                        .build(getChildFragmentManager());
                Analytics.trackEvent("rewards_OrderDetail_OrderDetailFragment_addBTN");
                break;
            case R.id.editBTN:
                rewardsActivity.openEditPurchaseFragment(orderItem);
                Analytics.trackEvent("rewards_OrderDetail_OrderDetailFragment_editBTN");
                break;
        }
    }

    public void refreshList(){
        Order.getDefault().myorder(getContext(), id, itemSRL);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
}
