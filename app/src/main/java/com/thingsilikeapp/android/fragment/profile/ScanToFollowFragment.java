package com.thingsilikeapp.android.fragment.profile;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.google.zxing.Result;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.iGaveReceivedActivity;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;
import com.thingsilikeapp.vendor.android.java.QRCode;
import com.thingsilikeapp.vendor.android.java.StringFormatter;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import butterknife.BindView;
import icepick.State;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanToFollowFragment extends BaseFragment  implements
        ZXingScannerView.ResultHandler{

    public static final String TAG = ScanToFollowFragment.class.getName();
    private int PERMISSION_CAMERA = 787;

    private iGaveReceivedActivity gemActivity;

    @BindView(R.id.scannerView)    ZXingScannerView scannerView;
    @BindView(R.id.barcodeIV)      ImageView barcodeIV;

    @State String code = "";

    public static ScanToFollowFragment newInstance() {
        ScanToFollowFragment fragment = new ScanToFollowFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_scan_follow;
    }

    @Override
    public void onViewReady() {
        gemActivity = (iGaveReceivedActivity) getContext();
        gemActivity.setTitle("Scan to follow");
        code = encrypt(UserData.getUserId() + "");
        barcodeIV.setImageBitmap(QRCode.encodeToQrCode(300, StringFormatter.isEmpty(code) ? "-" : code.trim()));
        Analytics.trackEvent("profile_ScanToFollowFragment_onViewReady");
    }

    private String encrypt(String s) {
        byte[] data = new byte[0];
        try {
            data = s.getBytes(StandardCharsets.UTF_8);
        } finally {
            return Base64.encodeToString(data, Base64.DEFAULT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(PermissionChecker.checkPermissions(getActivity(), Manifest.permission.CAMERA, PERMISSION_CAMERA)){
            startCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CAMERA) {
            if(gemActivity.isAllPermissionResultGranted(grantResults)){
                startCamera();
            }else{
                gemActivity.onBackPressed();
            }
        }
    }

    private void startCamera(){
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        Log.e("EEEEEEEEEEEEEE", ">>>>" + decrypt(result.getText()));
        submitCallback(decrypt(result.getText()));
    }

    public void submitCallback(String result) {
        Log.e("EEEEEEEEEEEEEE", ">>>>" + result);
        gemActivity.startProfileActivity(Integer.parseInt(result));
        gemActivity.finish();
    }

    private String decrypt(String raw){
        byte[] dataDec = Base64.decode(raw, Base64.DEFAULT);
        String decodedString = "";
        try {

            decodedString = new String(dataDec, StandardCharsets.UTF_8);
        } finally {

            return decodedString;
        }
    }

}
