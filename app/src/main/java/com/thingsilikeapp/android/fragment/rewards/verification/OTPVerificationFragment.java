package com.thingsilikeapp.android.fragment.rewards.verification;

import android.view.View;

import com.dpizarro.pinview.library.PinView;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.VerificationActivity;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.KYC;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class OTPVerificationFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = OTPVerificationFragment.class.getName();


    @BindView(R.id.submitBTN)          View submitBTN;
    @BindView(R.id.pinView)            PinView pinView;

    @State String country;
    @State String contact;
    @State String contry_code;

    private VerificationActivity verificationActivity;

    public static OTPVerificationFragment newInstance(String contact, String countrycode, String country) {
        OTPVerificationFragment fragment = new OTPVerificationFragment();
        fragment.contact = contact;
        fragment.contry_code = countrycode;
        fragment.country = country;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_verification_otp;
    }

    @Override
    public void onViewReady() {
        verificationActivity = (VerificationActivity) getContext();
        verificationActivity.setTitle("OTP Verification");
        submitBTN.setOnClickListener(this);
        verificationActivity.setBackBTN(false);
        Analytics.trackEvent("rewards_verification_OTPVerificationFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(KYC.PhoneResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            verificationActivity.openVerificationFragment();
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.submitBTN:
                KYC.getDefault().phoneOTP(getContext(), contact, contry_code, country, pinView.getPinResults());
                Analytics.trackEvent("rewards_verification_OTPVerificationFragment_submitBTN");
                break;
        }
    }
}
