package com.thingsilikeapp.android.fragment.main;

import android.content.pm.PackageManager;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.DiscoverActivity;
import com.thingsilikeapp.android.adapter.SettingAdapter;
import com.thingsilikeapp.android.dialog.AppTutorialDialog;
import com.thingsilikeapp.android.dialog.LogoutDialog;
import com.thingsilikeapp.data.model.SettingItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.android.widget.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainSettingsFragment extends BaseFragment implements SettingAdapter.ClickListener{

    public static final String TAG = MainSettingsFragment.class.getName();
    private DiscoverActivity mainActivity;

    private SettingAdapter profileAdapter;
    private SettingAdapter notificationAdapter;
    private SettingAdapter aboutAdapter;

    private List<SettingItem> settingItem;

    @BindView(R.id.profileEHLV)         ExpandableHeightListView profileEHLV;
    @BindView(R.id.profileSettingEHLV)  ExpandableHeightListView profileSettingEHLV;
    @BindView(R.id.notificationEHLV)    ExpandableHeightListView notificationEHLV;
    @BindView(R.id.aboutEHLV)           ExpandableHeightListView aboutEHLV;

    public static MainSettingsFragment newInstance() {
        MainSettingsFragment mainSettingsFragment = new MainSettingsFragment();
        return mainSettingsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_settings;
    }

    @Override
    public void onViewReady() {
        mainActivity = (DiscoverActivity) getContext();
//        mainActivity.setTitle(getString(R.string.title_main_settings));
//        mainActivity.setTitleVisibility(true);
//        mainActivity.showBackButton(true);
//        mainActivity.setSettingsVisibility(false);
//        mainActivity.setFeedsVisibility(false);
//        mainActivity.setEventVisibility(false);
//        mainActivity.createYourOwnPostLabelVisibility(false);
        mainActivity.setTitle("Settings");
        setupProfileListView();
        sampleProfileData();

//        setupNotificationListView();
//        sampleNotificationData();

        setupAboutListView();
        sampleOthersData();

        setupProfileSettingListView();
        sampleProfileSettingData();

        Analytics.trackEvent("main_MainSettingsFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        setupAboutListView();
        sampleOthersData();

//        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)){
//            mainActivity.attemptGetCurrentWalkThrough();
//        }

    }

    private void setupProfileListView() {
        settingItem = new ArrayList<>();
        profileAdapter = new SettingAdapter(getContext(), settingItem);
        profileAdapter.setOnItemClickListener(this);
        profileEHLV.setAdapter(profileAdapter);
    }

    private void sampleProfileData() {

        settingItem.clear();

        SettingItem settingItems = new SettingItem();
        settingItems.id = 1;
        settingItems.name = "Edit Profile";
        settingItems.show_enabled = false;
        settingItems.description = "Update your profile information";
        settingItem.add(settingItems);

        if(UserData.getUserItem().fb_id == null){
            settingItems = new SettingItem();
            settingItems.id = 2;
            settingItems.name = "Change Password";
            settingItems.show_enabled =false;
            settingItems.description = "Update your password regularly";
            settingItem.add(settingItems);
        }


        settingItems = new SettingItem();
        settingItems.id = 11;
        settingItems.name = "Address Book";
        settingItems.show_enabled = false;
        settingItems.description = "List of delivery address";
        settingItem.add(settingItems);


    }

//    private void setupNotificationListView() {
//
//        settingItem = new ArrayList<>();
//        notificationAdapter = new SettingAdapter(getContext(), settingItem);
//        notificationAdapter.setOnItemClickListener(this);
//        notificationEHLV.setAdapter(notificationAdapter);
//    }

//    private void sampleNotificationData() {
//
//        SettingItem settingItems = new SettingItem();
//        settingItems.id = 3;
//        settingItems.name = "Notification";
//        settingItems.show_enabled =true;
//        settingItems.description = "Lorem Ipsum is simply dummy text";
//        settingItem.add(settingItems);
//    }

    private void setupProfileSettingListView(){
        settingItem = new ArrayList<>();
        aboutAdapter = new SettingAdapter(getContext(), settingItem);
        aboutAdapter.setOnItemClickListener(this);
        profileSettingEHLV.setAdapter(aboutAdapter);
    }

    private void sampleProfileSettingData(){

        SettingItem settingItems = new SettingItem();
        settingItems.id =3;
        settingItems.name = "Blocked Users";
        settingItems.show_enabled=false;
        settingItems.description = "List of Blocked Users";
        settingItem.add(settingItems);


        settingItems = new SettingItem();
        settingItems.id = 4;
        settingItems.name = "Hidden Posts";
        settingItems.show_enabled=false;
        settingItems.description = "List of Hidden Posts";
        settingItem.add(settingItems);

        settingItems = new SettingItem();
        settingItems.id = 5;
        settingItems.name = "Reported Users";
        settingItems.show_enabled=false;
        settingItems.description = "List of Reported Users";
        settingItem.add(settingItems);

        settingItems = new SettingItem();
        settingItems.id = 6;
        settingItems.name = "Reported Posts";
        settingItems.show_enabled=false;
        settingItems.description = "List of Reported Posts";
        settingItem.add(settingItems);
    }


    private void setupAboutListView() {

        settingItem = new ArrayList<>();
        aboutAdapter = new SettingAdapter(getContext(), settingItem);
        aboutAdapter.setOnItemClickListener(this);
        aboutEHLV.setAdapter(aboutAdapter);
    }

    private void sampleOthersData() {

        SettingItem settingItems = new SettingItem();
        settingItems.id = 7;
        settingItems.name = "Share App";
        settingItems.show_enabled =false;
        settingItems.description = "Share LYKA with your friends and loved ones.";
        settingItem.add(settingItems);

//        settingItems = new SettingItem();
//        settingItems.id = 10;
//        settingItems.name = "Features";
//        settingItems.show_enabled = false;
//        settingItems.description = "App features";
//        settingItem.add(settingItems);

        settingItems = new SettingItem();
        settingItems.id = 8;
        settingItems.name = "About LYKA";
        settingItems.show_enabled =false;
        settingItems.description = getAppVersion();
        settingItem.add(settingItems);

        settingItems = new SettingItem();
        settingItems.id = 9;
        settingItems.name = "Log Out";
        settingItems.show_enabled =false;
        settingItems.description ="We hate to see you leave.";
        settingItem.add(settingItems);

//        settingItems = new SettingItem();
//        settingItems.id = 7;
//        settingItems.name = "How to Use";
//        settingItems.show_enabled =false;
//        settingItems.description = "Open tutorial";
//        settingItem.add(settingItems);


//        settingItems = new SettingItem();
//        settingItems.id = 9;
//        settingItems.name = "Notification";
//        settingItems.show_enabled =true;
//        settingItems.enable = getIsNotificationEnabled();
//        settingItems.description = getNotification();
//        settingItem.add(settingItems);



    }

    private String getNotification(){
        String notification;
        if (UserData.getString(UserData.RINGTONE_NAME).equals("") || UserData.getString(UserData.RINGTONE_NAME).equals(null)) {
            notification = "Default";
        }else {
            notification = UserData.getString(UserData.RINGTONE_NAME);
        }

        return notification;
    }

    private boolean getIsNotificationEnabled(){
        boolean b = UserData.getBoolean(UserData.NOTIFICATION_ENABLE,true);

        return b;
    }

    private String getAppVersion(){
        String version;
        try {
            version = "App Build version " + getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            version = "App Build version --";
        }

        return version;
    }

    @Override
    public void onSettingItemClickListener(int id) {
        switch (id){
            case 1:
                mainActivity.startSettingsActivity("edit");
//                mainActivity.facebookLogger(FacebookCustomLoggerMessage.DiscoverActivity.EVENT,FacebookCustomLoggerMessage.DiscoverActivity.EDIT);
                Analytics.trackEvent("main_MainSettingsFragment_edit");
                break;
            case 2:
                mainActivity.startSettingsActivity("password");
//                mainActivity.facebookLogger(FacebookCustomLoggerMessage.DiscoverActivity.EVENT,FacebookCustomLoggerMessage.DiscoverActivity.CHANGE_PASSWORD);
                Analytics.trackEvent("main_MainSettingsFragment_password");
                break;
            case 3:
                mainActivity.startSettingsActivity("block");
                Analytics.trackEvent("main_MainSettingsFragment_block");
                break;
            case 4:
                mainActivity.startSettingsActivity("hide");
                Analytics.trackEvent("main_MainSettingsFragment_hide");
                break;
            case 5:
                mainActivity.startSettingsActivity("reportUser");
                Analytics.trackEvent("main_MainSettingsFragment_reportUser");
                break;
            case 6:
                mainActivity.startSettingsActivity("reportPost");
                Analytics.trackEvent("main_MainSettingsFragment_reportPost");
                break;
            case 7:
                mainActivity.shareTheApp();
//                mainActivity.facebookLogger(FacebookCustomLoggerMessage.DiscoverActivity.EVENT,FacebookCustomLoggerMessage.DiscoverActivity.SHARE);
                Analytics.trackEvent("main_MainSettingsFragment_shareTheApp");
                break;
            case 8:
                mainActivity.startSettingsActivity("about");
//                mainActivity.facebookLogger(FacebookCustomLoggerMessage.DiscoverActivity.EVENT,FacebookCustomLoggerMessage.DiscoverActivity.ABOUT);
                Analytics.trackEvent("main_MainSettingsFragment_about");
                break;
            case 9:
                LogoutDialog logoutDialog = LogoutDialog.newInstance();
                logoutDialog.show(getFragmentManager(), LogoutDialog.TAG);
//                mainActivity.facebookLogger(FacebookCustomLoggerMessage.DiscoverActivity.EVENT,FacebookCustomLoggerMessage.DiscoverActivity.OPEN_LOGOUT);
                Analytics.trackEvent("main_MainSettingsFragment_logoutDialog");
                break;

            case 10:
//                AppTutorialDialog.newInstance().show(getChildFragmentManager(), AppTutorialDialog.TAG);
                Analytics.trackEvent("main_MainSettingsFragment_AppTutorialDialog");
                break;
            case 11:
                mainActivity.startAddressBookActivity("", "all");
                Analytics.trackEvent("main_MainSettingsFragment_all");
                break;
            case 12:
                UserData.insert(UserData.NOTIFICATION_ENABLE , true);
                aboutAdapter.setSwitchClicked(9, true);
                mainActivity.startSettingsActivity("notification");
                Analytics.trackEvent("main_MainSettingsFragment_notification");
                break;
        }
    }

    @Override
    public void onNotificationChanged(boolean active) {
        UserData.insert(UserData.NOTIFICATION_ENABLE , active);
    }
}
