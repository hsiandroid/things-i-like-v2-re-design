package com.thingsilikeapp.android.fragment.intro;

import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class ThirdFragment extends BaseFragment {

    public static final String TAG = ThirdFragment.class.getName();


    public static ThirdFragment newInstance() {
        ThirdFragment fragment = new ThirdFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_intro_c;
    }

    @Override
    public void onViewReady() {

    }
}
