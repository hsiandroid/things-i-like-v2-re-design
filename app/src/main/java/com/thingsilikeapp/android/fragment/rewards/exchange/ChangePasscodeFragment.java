package com.thingsilikeapp.android.fragment.rewards.exchange;

import android.view.View;
import android.widget.TextView;

import com.goodiebag.pinview.Pinview;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.dialog.ResetPasscodeDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.PasscodeVerificationDialog;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.Passcode;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ChangePasscodeFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = ChangePasscodeFragment.class.getName();

    public static ChangePasscodeFragment newInstance() {
        ChangePasscodeFragment fragment = new ChangePasscodeFragment();
        return fragment;
    }

    @BindView(R.id.forgotBTN)     TextView forgotBTN;
    @BindView(R.id.addBTN)        TextView addBTN;
    @BindView(R.id.pinView)       Pinview pinView;

    private ExchangeActivity exchangeActivity;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_change_passcode;
    }

    @Override
    public void onViewReady() {
        exchangeActivity = (ExchangeActivity)getContext();
        exchangeActivity.setTitle("Update Your Wallet Passcode");
        addBTN.setOnClickListener(this);
        forgotBTN.setOnClickListener(this);
        Analytics.trackEvent("rewards_exchange_ChangePasscodeFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Passcode.PasscodeResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            exchangeActivity.openNewPasscodeFragment(pinView.getValue());
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.addBTN:
                if (pinView.getValue().equalsIgnoreCase("")){
                    ToastMessage.show(getContext(), "Please input your current 4 digit passcode.", ToastMessage.Status.FAILED);
                }else{
                    Passcode.getDefault().validate_passcode(getContext(), pinView.getValue());
                }
                Analytics.trackEvent("rewards_exchange_ChangePasscodeFragment_addBTN");
                break;

            case R.id.forgotBTN:
//                ResetPasscodeDialog.newInstance(reportUserItem).show(getFragmentManager(),TAG);
                exchangeActivity.openForgotPasscodeFragment();
                break;
        }
    }
}
