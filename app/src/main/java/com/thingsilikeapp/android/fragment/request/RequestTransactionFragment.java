package com.thingsilikeapp.android.fragment.request;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RequestActivity;
import com.thingsilikeapp.android.adapter.recyler.RequestTransactionRecyclerViewAdapter;
import com.thingsilikeapp.android.dialog.AppreciateGiftDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListTransactionItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.wishlist.PermissionPendingRequest;
import com.thingsilikeapp.server.request.wishlist.WishListReceivePermissionRequest;
import com.thingsilikeapp.server.request.wishlist.WishListRejectGiftPermissionRequest;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransactionTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListTransactionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.android.widget.MultiSwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class RequestTransactionFragment extends BaseFragment implements
    SwipeRefreshLayout.OnRefreshListener,
    EndlessRecyclerViewScrollListener.Callback,
    RequestTransactionRecyclerViewAdapter.ClickListener,
    AppreciateGiftDialog.Callback{

    public static final String TAG = RequestTransactionFragment.class.getName();

    private RequestActivity requestActivity;
    private RequestTransactionRecyclerViewAdapter requestTransactionRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private PermissionPendingRequest permissionPendingRequest;

    @BindView(R.id.requestRV)                   RecyclerView requestRV;
    @BindView(R.id.requestSRL)                  MultiSwipeRefreshLayout requestSRL;
    @BindView(R.id.requestPlaceHolderCON)       View requestPlaceHolderCON;

    @State String include = "wishlist.image,wishlist.owner.info,viewer.info,info,image,sender.info";

    public static RequestTransactionFragment newInstance() {
        RequestTransactionFragment requestTransactionFragment = new RequestTransactionFragment();
        return requestTransactionFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_request_transaction;
    }

    @Override
    public void onViewReady() {
        requestActivity = (RequestActivity) getContext();
        requestActivity.setTitle(getString(R.string.title_request_transaction));
        setupListView();
        initSuggestionAPI();
        Analytics.trackEvent("request_RequestTransactionFragment_onViewReady");
    }



    private void setupListView(){
        requestSRL.setColorSchemeResources(R.color.colorPrimary);
        requestSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        requestSRL.setSwipeableChildren(R.id.requestRV, R.id.requestPlaceHolderCON);
        requestSRL.setOnRefreshListener(this);

        requestTransactionRecyclerViewAdapter = new RequestTransactionRecyclerViewAdapter(getContext());
        requestTransactionRecyclerViewAdapter.setOnItemClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);

        requestRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        requestRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        requestRV.setLayoutManager(linearLayoutManager);
        requestRV.setAdapter(requestTransactionRecyclerViewAdapter);
    }

    private void initSuggestionAPI(){
        permissionPendingRequest = new PermissionPendingRequest(getContext());
        permissionPendingRequest
                .setSwipeRefreshLayout(requestSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .showSwipeRefreshLayout(true)
                .setPerPage(20);
    }

    private void refreshList(){
        permissionPendingRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    public void showHideView(){
        if(requestTransactionRecyclerViewAdapter.getItemCount() == 0){
            requestPlaceHolderCON.setVisibility(View.VISIBLE);
            requestRV.setVisibility(View.GONE);
        }else {
            requestPlaceHolderCON.setVisibility(View.GONE);
            requestRV.setVisibility(View.VISIBLE);
        }
    }

    private void attemptReceivedRequest(int wishListID, String dedication){
        new WishListReceivePermissionRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Accepting Gift...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, include)
                .addParameters(Keys.server.key.WISHLIST_TRANSACTION_ID, wishListID)
                .addParameters(Keys.server.key.APPRECIATION_MESSAGE, dedication)
                .execute();
    }

    private void attemptRejectGiftRequest(final int wishlistID, final int userID){

        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Have you received the actual gift?")
                .setNote("You will notify this person that you have not received the gift.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Confirm")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new WishListRejectGiftPermissionRequest(getContext())
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading...", false, false))
                                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .addParameters(Keys.server.key.INCLUDE, include)
                                .addParameters(Keys.server.key.WISHLIST_TRANSACTION_ID, wishlistID)
                                .addParameters(Keys.server.key.USER_ID, userID)
                                .execute();
                        confirmationDialog.dismiss();
                        ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT,FacebookCustomLoggerMessage.ItemActivity.UNRECEIVED_GIFT);
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("request_RequestTransactionFragment_attemptRejectGiftRequest");

    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        permissionPendingRequest.nextPage();
    }

    @Override
    public void onItemClick(WishListTransactionItem wishListTransactionItem) {
        requestActivity.startItemActivity(wishListTransactionItem.id, wishListTransactionItem.sender.data.id, "i_received");
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        requestActivity.startProfileActivity(userItem.id);
    }

    @Override
    public void onYesClick(WishListTransactionItem wishListTransactionItem) {
        AppreciateGiftDialog.newInstance("Thank You Message", wishListTransactionItem.id, this).show(getChildFragmentManager(), AppreciateGiftDialog.TAG);
    }

    @Override
    public void onNoClick(WishListTransactionItem wishListTransactionItem) {
        attemptRejectGiftRequest(wishListTransactionItem.id, wishListTransactionItem.sender.data.id);
    }

    @Override
    public void onAccept(int wishlistID, String dedication) {
        attemptReceivedRequest(wishlistID, dedication);
    }

    @Subscribe
    public void onResponse(PermissionPendingRequest.ServerResponse responseData) {
        WishListTransactionTransformer permissionTransformer = responseData.getData(WishListTransactionTransformer.class);
        if(permissionTransformer.status){
            requestTransactionRecyclerViewAdapter.setNewData(permissionTransformer.data);
            showHideView();
        }
    }

    @Subscribe
    public void onResponse(WishListReceivePermissionRequest.ServerResponse responseData) {
        WishListInfoTransactionTransformer wishlistInfoTransformer = responseData.getData(WishListInfoTransactionTransformer.class);
        if(wishlistInfoTransformer.status){
            requestTransactionRecyclerViewAdapter.removeData(wishlistInfoTransformer.data);
            showHideView();
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(WishListRejectGiftPermissionRequest.ServerResponse responseData) {
        WishListInfoTransactionTransformer wishlistInfoTransformer = responseData.getData(WishListInfoTransactionTransformer.class);
        if(wishlistInfoTransformer.status){
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), wishlistInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}
