package com.thingsilikeapp.android.fragment.rewards.verification;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.VerificationActivity;
import com.thingsilikeapp.data.model.KYCItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.KYC;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class EmailVerificationFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = EmailVerificationFragment.class.getName();

    @BindView(R.id.emailET)            EditText emailET;
    @BindView(R.id.submitBTN)          View submitBTN;
//    @BindView(R.id.phoneBTN)           View phoneBTN;

    private VerificationActivity verificationActivity;

    public static EmailVerificationFragment newInstance() {
        EmailVerificationFragment fragment = new EmailVerificationFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_verification_email;
    }

    @Override
    public void onViewReady() {
        verificationActivity = (VerificationActivity) getContext();
        verificationActivity.setTitle("Email Verification");
        submitBTN.setOnClickListener(this);
//        phoneBTN.setOnClickListener(this);
        verificationActivity.setBackBTN(true);
        Analytics.trackEvent("rewards_verification_EmailVerificationFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(KYC.EmailResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            verificationActivity.openVerificationFragment();
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.submitBTN:
                KYC.getDefault().email(getContext(), emailET.getText().toString());
                Analytics.trackEvent("rewards_verification_EmailVerificationFragment_submitBTN");
                break;
//            case R.id.phoneBTN:
//                verificationActivity.openPhoneFragment();
//                Analytics.trackEvent("rewards_verification_EmailVerificationFragment_phoneBTN");
//                break;
        }
    }
}
