package com.thingsilikeapp.android.fragment.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ProfileActivity;
import com.thingsilikeapp.android.adapter.CategoryOwnedRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.FragmentViewPagerAdapter;
import com.thingsilikeapp.android.adapter.SocialFeedRecyclerViewAdapter;
import com.thingsilikeapp.android.dialog.GemsReceiveDialog;
import com.thingsilikeapp.android.dialog.UnfollowDialog;
import com.thingsilikeapp.android.dialog.ViewImageDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.config.Share;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.like.LikeRequest;
import com.thingsilikeapp.server.request.report.ReportRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.user.ProfileInfoRequest;
import com.thingsilikeapp.server.request.wishlist.BlockRequest;
import com.thingsilikeapp.server.request.wishlist.CategoryOwnedRequest;
import com.thingsilikeapp.server.request.wishlist.UserWishListRequest;
import com.thingsilikeapp.server.transformer.social.FeedTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.server.transformer.wishlist.CategoryTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.CustomStaggeredGridLayoutManager;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.NumberFormatter;
import com.thingsilikeapp.vendor.android.java.VerifiedUserBadge;
import com.thingsilikeapp.vendor.android.java.decoration.SpacesItemDecoration;
import com.thingsilikeapp.vendor.android.widget.CustomViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class ProfileeFragment extends BaseFragment implements View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        PopupMenu.OnMenuItemClickListener,
        View.OnTouchListener, AppBarLayout.OnOffsetChangedListener, GemsReceiveDialog.Callback, SocialFeedRecyclerViewAdapter.ClickListener,
        EndlessRecyclerViewScrollListener.Callback,CategoryOwnedRecycleViewAdapter.ClickListener{

    public static final String TAG = ProfileeFragment.class.getName();
    private FragmentViewPagerAdapter fragmentViewPagerAdapter;
    private ProfileInfoRequest userInfoRequest;
    private SocialFeedRecyclerViewAdapter socialFeedRecyclerViewAdapter;
    private CategoryOwnedRecycleViewAdapter categoryOwnedRecycleViewAdapter;
    private UserWishListRequest userWishListRequest;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private CustomStaggeredGridLayoutManager staggeredGridLayoutManager;
    private LinearLayoutManager linearLayoutManager;

    private Timer timer;
    private TimerTask delayedThreadStartTask;
    private CategoryOwnedRequest categoryRequest;

    private ProfileActivity profileActivity;

    @BindView(R.id.profileVP)                       CustomViewPager profileVP;
//    @BindView(R.id.listViewBTN)                     View listViewBTN;
 //   @BindView(R.id.gridViewBTN)                     View gridViewBTN;
    @BindView(R.id.requestBTN)                      View requestBTN;
    @BindView(R.id.iGaveBTN)                        View iGaveBTN;
    @BindView(R.id.iReceivedBTN)                    View iReceivedBTN;
    @BindView(R.id.settingsBTN)                     View settingsBTN;

    @BindView(R.id.followersBTN)                    View followersBTN;
    @BindView(R.id.followingBTN)                    View followingBTN;
    @BindView(R.id.followersTXT)                    TextView followersTXT;
    @BindView(R.id.followingTXT)                    TextView followingTXT;
    @BindView(R.id.followersLBL)                    TextView followersLBL;
    @BindView(R.id.followingLBL)                    TextView followingLBL;

    @BindView(R.id.iReceivedBdg)                    View iReceivedBdg;
    @BindView(R.id.iGaveBdg)                        View iGaveBdg;

    @BindView(R.id.imageCIV)                        ImageView imageCIV;
    @BindView(R.id.editBTN)                         ImageView editBTN;
    @BindView(R.id.scanToFollow)                    ImageView scanToFollowBTN;
    @BindView(R.id.commonNameTXT)                   TextView commonNameTXT;
    @BindView(R.id.userNameTXT)                     TextView userNameTXT;
    @BindView(R.id.profileSRL)                      SwipeRefreshLayout profileSRL;
   // @BindView(R.id.app_bar)                         AppBarLayout appBarLayout;

    @BindView(R.id.observableRecyclerView)          RecyclerView observableRecyclerView;
    @BindView(R.id.categoryRV)                      RecyclerView categoryRV;
    @BindView(R.id.placeHolderCON)                  View placeHolderCON;
    @BindView(R.id.loadingIV)                       View loadingIV;
    @BindView(R.id.profileHeaderCon)                View profileHeaderCon;

    @BindView(R.id.backButtonIV)                    View backButtonIV;

    public static ProfileeFragment newInstance() {
        ProfileeFragment profileFragment = new ProfileeFragment();
        return profileFragment;
    }

    public int flexibleSpaceHeight;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_profilee;
    }

    @Override
    public void onViewReady() {
        profileActivity = (ProfileActivity) getContext();
        imageCIV.setOnClickListener(this);
        editBTN.setOnClickListener(this);
        settingsBTN.setOnClickListener(this);
        iGaveBTN.setOnClickListener(this);
        scanToFollowBTN.setOnClickListener(this);
        backButtonIV.setOnClickListener(this);
        setupPage();
        initSuggestionAPI();
        initFeedAPI();
        setupListView();
        setUpRView();
        initCategoryAPI();
        displayViewPager();
        displayData(UserData.getUserItem());
        Analytics.trackEvent("main_ProfileFragment_onViewReady");
    }

    public void setupPage(){
        flexibleSpaceHeight = getResources().getDimensionPixelSize(R.dimen.profile_header);
        followersBTN.setOnTouchListener(this);
        followingBTN.setOnTouchListener(this);
        iReceivedBTN.setOnTouchListener(this);
        iGaveBTN.setOnTouchListener(this);
        //gridViewBTN.setOnTouchListener(this);
        requestBTN.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            switch (v.getId()){
                case R.id.followersBTN:
                    profileActivity.startSearchActivity(UserData.getUserItem().id, UserData.getUserItem().common_name, "followers");
                    Analytics.trackEvent("main_ProfileFragment_followersBTN");
                    break;
                case R.id.followingBTN:
                    profileActivity.startSearchActivity(UserData.getUserItem().id, UserData.getUserItem().common_name, "following");
                    Analytics.trackEvent("main_ProfileFragment_followingBTN");
                    break;
                case R.id.iReceivedBTN:
                   // iReceivedActive();
                    profileActivity.startiGaveReceivedActivity("Received", UserData.getUserItem().id);
                    Analytics.trackEvent("main_ProfileFragment_iReceivedBTN");
                    break;
                case R.id.iGaveBTN:
                    profileActivity.startiGaveReceivedActivity("Gave", UserData.getUserItem().id);
                    Analytics.trackEvent("main_ProfileFragment_iGaveBTN");
                    break;
//                case R.id.listViewBTN:
//                    listViewActive();
//                    break;
                case R.id.requestBTN:
                    GemsReceiveDialog.newInstance(this).show(getFragmentManager(),TAG);
                   // gridViewActive();
                    Analytics.trackEvent("main_ProfileFragment_requestBTN");
                    break;
                case R.id.profileVP:
                    gridViewActive();
                    Analytics.trackEvent("main_ProfileFragment_profileVP");
                    break;
                case R.id.scanToFollow:
                    profileActivity.startiGaveReceivedActivity("scan", 0);
                    Analytics.trackEvent("main_ProfileFragment_scanToFollow");
                    break;
            }
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
      //  appBarLayout.addOnOffsetChangedListener(this);
//        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)) {
//            if (UserData.getBoolean(UserData.PROFILE_CURRENT)) {
//                switch (UserData.getInt(UserData.WALKTHRU_CURRENT)) {
//                    case 3:
//                        attemptGetSocialWalkThrough();
//                        break;
//                        default:
//                            profileActivity.showFriendsWalkThrough();
//
//                }
//            }else {
//                if (UserData.getBoolean(UserData.WALKTHRU_CREATE_POST_SHOW)){
////                    MaterialShowcaseView.resetAll(getContext());
//                    profileActivity.attemptGetCurrentWalkThrough();
//                }
//            }
//        }else {
//
//        }

        refreshList();
//        profileActivity.profileActive();
//        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)) {
//            if (UserData.getBoolean(UserData.PROFILE_CURRENT)) {
//                switch (UserData.getInt(UserData.WALKTHRU_CURRENT)) {
//                    case 3:
//                        attemptGetSocialWalkThrough();
//                        break;
//                    default:
//                        profileActivity.attemptGetCurrentWalkThrough();
//
//                }
//            }
//        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //appBarLayout.removeOnOffsetChangedListener(this);
    }

    public void attemptGetSocialWalkThrough(){
        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)){
            new MaterialShowcaseView.Builder(getActivity())
                    .setTarget(followingBTN)
//                    .setTarget(followersBTN)
                    .setMaskColour(ActivityCompat.getColor(getContext(), R.color.walkthrough_bg))
                    .setTargetTouchable(true)
                    .setDismissOnTargetTouch(true)
                    .setShapePadding(0)
                    .setContentTextColor(ActivityCompat.getColor(getContext(), R.color.white_dim))
//                    .setContentText("Here you can see your followers. You can also follow others.\n" +
//                            "Now, try to click “+Follow” button.\n")
                    .setContentText("You can see your followers and who are you following when you click these\n" +
                            "Now, let\'s click \"Following\"")
                    .show();
        }
    }

    private void displayData(UserItem userItem) {
//        profileActivity.setTitle(getString(R.string.title_main_profile));

        commonNameTXT.setText(userItem.common_name);
        userNameTXT.setText(VerifiedUserBadge.getFormattedName(getContext(), userItem.name, userItem.is_verified, 14, true));

        Glide.with(getContext())
                .load(userItem.getAvatar())
                .apply(new RequestOptions()

                        .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(imageCIV);

        Log.e("Avatar", ">>>" + userItem.getAvatar());

        iGaveBdg.setVisibility(userItem.statistics.data.ongoing_igave ? View.VISIBLE : View.INVISIBLE);
        iReceivedBdg.setVisibility(userItem.statistics.data.ongoing_ireceived ? View.VISIBLE : View.INVISIBLE);
//        iLikeCountTXT.setText(NumberFormatter.format(userItem.statistics.data.total_wishlist));
//        iReceivedCountTXT.setText(NumberFormatter.format(userItem.statistics.data.received_gifts));
//        iGaveCountTXT.setText(NumberFormatter.format(userItem.statistics.data.sent_gifts));
        followersTXT.setText(NumberFormatter.formatWithComma(userItem.statistics.data.followers));
        followingTXT.setText(NumberFormatter.formatWithComma(userItem.statistics.data.following));
        followersLBL.setText(userItem.statistics.data.followers == 1 ? "follower" : "followers");
    }

    private String birthdayFormat(String birthday){
        if(birthday == null){
            return "";
        }
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat newFormat = new SimpleDateFormat("MMM dd, yyyy");
        try {
            Date date = originalFormat.parse(birthday);
            return newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String monthDayFormat(String birthday){
        if(birthday == null){
            return "";
        }
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat newFormat = new SimpleDateFormat("MMM dd");
        try {
            Date date = originalFormat.parse(birthday);
            return newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void displayViewPager(){

        int id = UserData.getUserItem().id;

        fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getChildFragmentManager());
        fragmentViewPagerAdapter.clearFragment();
//        fragmentViewPagerAdapter.addFragment(ILikeCategoryFragment.newInstance(id));
        fragmentViewPagerAdapter.addFragment(ILikeFragment.newInstance(id));
//        fragmentViewPagerAdapter.addFragment(IReceivedFragment.newInstance(id));
//        fragmentViewPagerAdapter.addFragment(IGaveFragment.newInstance(id));
        profileVP.setPageMargin(10);
        profileVP.setPageMarginDrawable(null);
        profileVP.setAdapter(fragmentViewPagerAdapter);
        profileVP.setOffscreenPageLimit(3);
        profileVP.canSwipe(false);
        gridViewActive();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backButtonIV:
                profileActivity.onBackPressed();
                Analytics.trackEvent("main_ProfileFragment_backButtonIV");
                break;
            case R.id.editBTN:
                profileActivity.startSettingsActivity("edit");
                Analytics.trackEvent("main_ProfileFragment_editBTN");
                break;
            case R.id.imageCIV:
                ViewImageDialog.newInstance(UserData.getUserItem().getAvatar(), UserData.getUserItem().name).show(getChildFragmentManager(), ViewImageDialog.TAG);
                Analytics.trackEvent("main_ProfileFragment_imageCIV");
                break;
            case R.id.scanToFollow:
                profileActivity.startiGaveReceivedActivity("scan", 0);
                Analytics.trackEvent("main_ProfileFragment_scanToFollowOnClick");
                break;
            case R.id.settingsBTN:
//                profileActivity.openSettingsFragment();
                Analytics.trackEvent("main_ProfileFragment_settingsBTN");
                break;


        }
    }

//    public void listViewActive(){
//        profileVP.setCurrentItem(0, false);
////        listViewBTN.setSelected(true);
//        gridViewBTN.setSelected(false);
//        iGaveBTN.setSelected(false);
//        iReceivedBTN.setSelected(false);
//    }

    public void gridViewActive(){
        profileVP.setCurrentItem(0, false);
        profileVP.requestLayout();
//        listViewBTN.setSelected(false);
  //      gridViewBTN.setSelected(false);
        requestBTN.setSelected(false);
        iGaveBTN.setSelected(false);
        iReceivedBTN.setSelected(false);
    }

//    public void iReceivedActive() {
//        profileVP.setCurrentItem(1, false);
//        profileVP.requestLayout();
//
////        listViewBTN.setSelected(false);
//        gridViewBTN.setSelected(false);
//        iGaveBTN.setSelected(false);
//        iReceivedBTN.setSelected(true);
//    }
//
//    public void iGaveActive() {
//        profileVP.setCurrentItem(2, false);
//        profileVP.requestLayout();
//
////        listViewBTN.setSelected(false);
//        gridViewBTN.setSelected(false);
//        iGaveBTN.setSelected(true);
//        iReceivedBTN.setSelected(false);
//    }

    private void initSuggestionAPI(){
        profileSRL.setNestedScrollingEnabled(false);
        profileSRL.setColorSchemeResources(R.color.colorPrimary);
        profileSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        profileSRL.setOnRefreshListener(this);

        userInfoRequest = new ProfileInfoRequest(getContext());
        userInfoRequest
                .setDeviceRegID(profileActivity.getDeviceRegID())
                .setSwipeRefreshLayout(profileSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,statistics,social");
    }

    private void refreshList(){
        userInfoRequest
                .showSwipeRefreshLayout(true)
                .execute();
        if (userWishListRequest != null) {
            userWishListRequest
                    .first();
            categoryRequest.first();
        }
        loadingIV.setVisibility(View.VISIBLE);
    }


    private void refreshData(){

        Fragment fragment = getCurrentFragment();

        if (fragment == null) {
            return;
        }

        /*if(fragment instanceof ILikeCategoryFragment){
            ((ILikeCategoryFragment) fragment).refreshList();
        }else*/ if(fragment instanceof ILikeFragment){
            ((ILikeFragment) fragment).refreshList();
        }else if(fragment instanceof IGaveFragment){
            ((IGaveFragment) fragment).refreshList();
        }else if(fragment instanceof IReceivedFragment){
            ((IReceivedFragment) fragment).refreshList();
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
//        refreshData();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        Glide.get(getContext()).clearMemory();
        if (timer != null) {
            timer.cancel();
        }
        super.onStop();
    }

    @Subscribe
    public void onResponse(ProfileInfoRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            UserData.insert(userTransformer.userItem);
            UserData.insert(UserData.TOTAL_POST, userTransformer.userItem.statistics.data.total_wishlist);
            UserData.updateRewardCrystal(userTransformer.userItem.reward_crystal + "");
            UserData.updateDisplayRewardCrystal(userTransformer.userItem.lykagem_display + "");
            UserData.updateRewardFragment(userTransformer.userItem.reward_fragment + "");
            displayData(userTransformer.userItem);
//            refreshData();
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
//            case R.id.editProfileBTN:
//                profileActivity.startSettingsActivity("edit");
//                break;
//            case R.id.viewPhotoBTN:
//                ViewImageDialog.newInstance(UserData.getUserItem().getAvatar(), UserData.getUserItem().name).show(getChildFragmentManager(), ViewImageDialog.TAG);
//                break;
        }
        return false;
    }

    private Fragment getCurrentFragment() {
        if (profileVP != null){
            return fragmentViewPagerAdapter.getItem(profileVP.getCurrentItem());
        }
        return new Fragment();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        profileSRL.setEnabled(i == 0);
    }

    @Override
    public void onReceiveCallback() {

    }

    private void initFeedAPI() {
        userWishListRequest = new UserWishListRequest(getContext());
        userWishListRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                .addParameters(Keys.server.key.INCLUDE, "image,owner.social")
                .addParameters(Keys.server.key.USER_ID, UserData.getUserId())
                .setPerPage(20);
    }


    public void setupListView() {
        socialFeedRecyclerViewAdapter = new SocialFeedRecyclerViewAdapter(getContext());
        socialFeedRecyclerViewAdapter.setEnableUserClick(false);
        socialFeedRecyclerViewAdapter.setFeedView(SocialFeedRecyclerViewAdapter.GRID);
        staggeredGridLayoutManager = new CustomStaggeredGridLayoutManager(2, OrientationHelper.VERTICAL);
//        staggeredGridLayoutManager.setScrollEnabled(true);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(staggeredGridLayoutManager, this);
        observableRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        observableRecyclerView.setAdapter(socialFeedRecyclerViewAdapter);
        observableRecyclerView.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.social_feed_spacing)));
        socialFeedRecyclerViewAdapter.setOnItemClickListener(this);
        observableRecyclerView.setNestedScrollingEnabled(false);
        observableRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

    }

    @Subscribe
    public void onResponse(UserWishListRequest.ServerResponse responseData) {
        FeedTransformer feedTransformer = responseData.getData(FeedTransformer.class);
        if (feedTransformer.status) {

//            userWishListRequest.setHasMorePage(feedTransformer.has_morepages);
            if (responseData.isNext()) {
                socialFeedRecyclerViewAdapter.addNewData(feedTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                socialFeedRecyclerViewAdapter.setNewData(feedTransformer.data);
            }

//            if (userID == UserData.getUserId()) {
//                FeedTransformer cache = new FeedTransformer();
//                cache.data = socialFeedRecyclerViewAdapter.getData();
//                UserData.insert(UserData.MY_FEED_CACHE, new Gson().toJson(cache));
//            }
        }
        loadingIV.setVisibility(View.GONE);
        observableRecyclerView.setVisibility(socialFeedRecyclerViewAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE);
        placeHolderCON.setVisibility(socialFeedRecyclerViewAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    private void setUpRView(){
        categoryOwnedRecycleViewAdapter = new CategoryOwnedRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        categoryRV.setLayoutManager(linearLayoutManager);
//        categoryOwnedRecycleViewAdapter.setNewData(getDefaultData());
        categoryRV.setAdapter(categoryOwnedRecycleViewAdapter);
        categoryOwnedRecycleViewAdapter.setOnItemClickListener(this);
    }

    private void initCategoryAPI(){
        categoryRequest = new CategoryOwnedRequest(getContext());
        categoryRequest
                .addParameters(Keys.server.key.USER_ID, UserData.getUserId())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(100);
    }

    @Subscribe
    public void onResponse(CategoryOwnedRequest.ServerResponse responseData) {
        CategoryTransformer categoryTransformer = responseData.getData(CategoryTransformer.class);
        if (categoryTransformer.status) {
            if (responseData.isNext()) {
                categoryOwnedRecycleViewAdapter.addNewData(categoryTransformer.data);
            }else{
                CategoryItem rewardCategoryItem = new CategoryItem();
                rewardCategoryItem.id = 0;
                rewardCategoryItem.title = "All";
                rewardCategoryItem.image = "https://www.catster.com/wp-content/uploads/2017/08/A-fluffy-cat-looking-funny-surprised-or-concerned.jpg";
//                rewardCategoryItem.selected = true;
                categoryTransformer.data.add(0, rewardCategoryItem);
                categoryOwnedRecycleViewAdapter.setNewData(categoryTransformer.data);
            }
        }
    }

    @Override
    public void onItemClick(CategoryItem categoryItem) {
        if (categoryItem.title.equalsIgnoreCase("all")){
            observableRecyclerView.setVisibility(observableRecyclerView.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            initFeedAPI();
        }else{
            observableRecyclerView.setVisibility(observableRecyclerView.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            loadingIV.setVisibility(View.VISIBLE);
            userWishListRequest = new UserWishListRequest(getContext());
            userWishListRequest
                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                    .addParameters(Keys.server.key.INCLUDE, "image,owner.social")
                    .addParameters(Keys.server.key.CATEGORY, categoryItem.title)
                    .addParameters(Keys.server.key.USER_ID, UserData.getUserId())
                    .setPerPage(50);
            userWishListRequest.first();
        }
    }

    @Override
    public void onItemLongClick(CategoryItem categoryItem) {

    }

    @Override
    public void onItemClick(WishListItem wishListItem) {
        if (wishListItem.owner.data != null) {
            ((RouteActivity) getContext()).startItemActivity(wishListItem.id, wishListItem.owner.data.id, "i_like");
        }
    }

    @Override
    public void onCommentClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startCommentActivity(wishListItem.id, wishListItem.title);
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        ((RouteActivity) getContext()).startProfileActivity(userItem.id);
    }

    @Override
    public void onGrabClick(WishListItem wishListItem) {
        String data = new Gson().toJson(wishListItem);
        ((RouteActivity) getContext()).startWishListActivity(data, "create_grab_other");
    }

    @Override
    public void onHideClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to hide?")
                .setNote("You will no longer see this post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Hide")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BlockRequest blockRequest = new BlockRequest(getContext());
                        blockRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("main_ProfileFragment_onHideClickDialog");
    }

    @Override
    public void onUnfollowClick(final WishListItem wishListItem) {
        UnfollowDialog.newInstance(wishListItem.owner.data.id, wishListItem.owner.data.name, new UnfollowDialog.Callback() {
            @Override
            public void onAccept(int userID) {
                UnFollowRequest unFollowRequest = new UnFollowRequest(getContext());
                unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                        .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                        .addParameters(Keys.server.key.USER_ID, wishListItem.owner.data.id)
                        .execute();
            }

            @Override
            public void onCancel(int userID) {

            }
        }).show(((BaseActivity) getContext()).getSupportFragmentManager(), UnfollowDialog.TAG);
        Analytics.trackEvent("main_ProfileFragment_onUnfollowClickDialog");
    }

    @Override
    public void onReportClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to report?")
                .setNote("You are about to report a post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Report")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportRequest reportRequest = new ReportRequest(getContext());
                        reportRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.TYPE, "wishlist")
                                .addParameters(Keys.server.key.REFERENCE_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
        Analytics.trackEvent("main_ProfileFragment_onReportClickDialog");
    }

    @Override
    public void onShareClick(WishListItem wishListItem) {
        shareTheApp(wishListItem);
    }

    public void shareTheApp(WishListItem wishListItem) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, wishListItem.owner.data.username + " posted " + wishListItem.title + " on things I like for " + wishListItem.category + ". Check out what everyone likes and discover your modern wishlist app");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, Share.URL + "/share/i/" + wishListItem.id);
        startActivity(Intent.createChooser(sharingIntent, "Share Things I like"));
        Analytics.trackEvent("main_ProfileFragment_shareTheApp");
    }

    @Override
    public void onHeartClick(WishListItem wishListItem) {
        LikeRequest likeRequest = new LikeRequest(getContext());
        likeRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")`
                .addParameters(Keys.server.key.INCLUDE, "image,owner.social")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                .execute();
    }


    @Override
    public void onRepostClick(WishListItem wishListItem) {

    }

    @Override
    public void onRepostUsersClick(WishListItem wishListItem) {

    }

    @Override
    public void onHeaderClick(EventItem eventItem) {

    }

    @Override
    public void onHeaderShowClick(EventItem eventItem) {

    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        timer = new Timer();
        delayedThreadStartTask = new TimerTask() {
            @Override
            public void run() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (userWishListRequest.hasMorePage()) {
                                    userWishListRequest.nextPage();
                                }
                            }
                        });
                    }
                }).start();
            }
        };
        loadingIV.setVisibility(View.VISIBLE);
        timer.schedule(delayedThreadStartTask, 5000);
    }
}
