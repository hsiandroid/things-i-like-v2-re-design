package com.thingsilikeapp.android.fragment.rewards.exchange;

import android.view.View;
import android.widget.TextView;

import com.goodiebag.pinview.Pinview;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.android.dialog.defaultdialog.PasscodeVerificationDialog;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.Passcode;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class UpdatePasscodeFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = UpdatePasscodeFragment.class.getName();

    public static UpdatePasscodeFragment newInstance(String old_passcode, String passcode) {
        UpdatePasscodeFragment fragment = new UpdatePasscodeFragment();
        fragment.passcode = passcode;
        fragment.old_passcode = old_passcode;
        return fragment;
    }

    @BindView(R.id.addBTN)        TextView addBTN;
    @BindView(R.id.pinView)       Pinview pinView;
    @State String passcode;
    @State String old_passcode;

    private ExchangeActivity exchangeActivity;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_update_passcode;
    }

    @Override
    public void onViewReady() {
        exchangeActivity = (ExchangeActivity)getContext();
        exchangeActivity.setTitle("Update Your Wallet Passcode");
        addBTN.setOnClickListener(this);
        Analytics.trackEvent("rewards_exchange_UpdatePasscodeFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Passcode.PasscodeResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            exchangeActivity.startRewardsActivity("treasury");
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.addBTN:
                if (!pinView.getValue().equalsIgnoreCase(passcode)){
                    ToastMessage.show(getContext(), "Please input your new 4 digit passcode to confirm.", ToastMessage.Status.FAILED);
                }else{
                    Passcode.getDefault().update_passcode(getContext(), old_passcode, pinView.getValue());
                }
                Analytics.trackEvent("rewards_exchange_UpdatePasscodeFragment_addBTN");
                break;
        }
    }
}
