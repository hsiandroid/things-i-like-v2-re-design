package com.thingsilikeapp.android.fragment.registration;

import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.NewRegistrationActivity;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.android.dialog.CountryDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.OTPDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.preference.CountryData;
import com.thingsilikeapp.server.request.GetCountryRequest;
import com.thingsilikeapp.server.request.ValidateRequest;
import com.thingsilikeapp.server.transformer.GetCountryTransformer;
import com.thingsilikeapp.server.transformer.ValidateTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class RegisterFFragment extends BaseFragment implements View.OnClickListener, CountryDialog.CountryPickerListener {

    public static final String TAG = RegisterFFragment.class.getName();

    private NewRegistrationActivity registrationActivity;

    @BindView(R.id.nextBTN)         TextView nextBTN;
    @BindView(R.id.emailBTN)        View emailBTN;
    @BindView(R.id.countryFlagIV)   ImageView countryFlagIV;
    @BindView(R.id.contactCodeTXT)  TextView contactCodeTXT;
    @BindView(R.id.backBTN)         TextView backBTN;
    @BindView(R.id.contactET)       EditText contactET;
    @BindView(R.id.contactCON)      View contactCON;

    @State String username;
    @State String gender;
    @State String birthdate;
    @State String name;
    @State String country;
    @State String contry_code;

    private CountryData.Country countryData;

    public static RegisterFFragment newInstance(String username, String gender, String birthdate, String name) {
        RegisterFFragment fragment = new RegisterFFragment();
        fragment.username = username;
        fragment.gender = gender;
        fragment.birthdate = birthdate;
        fragment.name = name;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_registration_f;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (NewRegistrationActivity) getContext();
        nextBTN.setOnClickListener(this);
        emailBTN.setOnClickListener(this);
        contactCON.setOnClickListener(this);
        getCountry();
        backBTN.setOnClickListener(this);
        Analytics.trackEvent("registration_RegisterFFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(GetCountryRequest.ServerResponse responseData) {
        GetCountryTransformer getCountryTransformer = responseData.getData(GetCountryTransformer.class);
        if(getCountryTransformer.status){
            countryData = CountryData.getCountryDataByCode1(getCountryTransformer.data.country);
            contactCodeTXT.setText(countryData.code3);
            countryCode(countryData);
        }else{
            ToastMessage.show(getContext(), getCountryTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(ValidateRequest.ServerResponse responseData) {
        nextBTN.setEnabled(true);
        ValidateTransformer validateTransformer = responseData.getData(ValidateTransformer.class);
        if(validateTransformer.status){
            OTPDialog.Builder(username,gender,birthdate,name,contactCodeTXT.getText().toString() + contactET.getText().toString(), "contact", country, contry_code, validateTransformer.otp_value).show(getFragmentManager(), TAG);
//            registrationActivity.openGFragment(username,gender,birthdate,name,contactCodeTXT.getText().toString() + contactET.getText().toString(), "contact", country, contry_code);
            Log.e("EEEEEEEEE", contactCodeTXT.getText().toString() + contactET.getText().toString());
            Log.e("FFFFFFF", " " + validateTransformer.otp_value);
        } else{
            ToastMessage.show(getContext(), validateTransformer.msg, ToastMessage.Status.FAILED);
            if(validateTransformer.hasRequirements()){
                ErrorResponseManger.first(contactET, validateTransformer.requires.contact_number);
            }
        }
    }

    private void validate(){
        nextBTN.setEnabled(false);
        ValidateRequest createCommentRequest = new ValidateRequest(getContext());
        createCommentRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Verifying...", false, false))
                .showNoInternetConnection(false)
                .addParameters(Keys.server.key.FIELD, "contact_number")
                .addParameters(Keys.server.key.COUNTRY, country)
                .addParameters(Keys.server.key.COUNTRY_CODE, contactCodeTXT.getText().toString())
                .addParameters(Keys.server.key.CONTACT_NUMBER, contactCodeTXT.getText().toString() + contactET.getText().toString())
                .execute();
    }

    private void getCountry(){
        GetCountryRequest createCommentRequest = new GetCountryRequest(getContext());
        createCommentRequest
                .showNoInternetConnection(false)
                .execute();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.nextBTN:
                validate();
                Analytics.trackEvent("registration_RegisterFFragment_nextBTN");
                break;
            case R.id.backBTN:
                registrationActivity.startLandingActivity();
                Analytics.trackEvent("registration_RegisterFFragment_backBTN");
                break;
            case R.id.emailBTN:
                registrationActivity.openEFragment(username, gender, birthdate, name);
                Analytics.trackEvent("registration_RegisterFFragment_emailBTN");
                break;
            case R.id.contactCON:
                showCountryPicker();
                Analytics.trackEvent("registration_RegisterFFragment_contactCON");
                break;
        }
    }

    private void countryCode(CountryData.Country country){
        contactCodeTXT.setText(country.code3);
        this.country = countryData.code1;
        this.contry_code = countryData.code3;
        Glide.with(getContext())
                .load(country.getFlag())
                .into(countryFlagIV);
    }

    private void showCountryPicker(){
        CountryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
    }

    @Override
    public void onSelectCountry(CountryData.Country country, int requestCode) {
        countryData = country;
        countryCode(countryData);
    }
}
