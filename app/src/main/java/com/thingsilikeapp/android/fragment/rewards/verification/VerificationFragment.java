package com.thingsilikeapp.android.fragment.rewards.verification;

import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.VerificationActivity;
import com.thingsilikeapp.data.model.KYCItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.KYC;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class VerificationFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = VerificationFragment.class.getName();


    private VerificationActivity verificationActivity;

    private UserItem userItem;

    @BindView(R.id.emailBTN)            View emailBTN;
    @BindView(R.id.phoneBTN)            View phoneBTN;
    @BindView(R.id.identityBTN)         View identityBTN;
    @BindView(R.id.selfieBTN)           View selfieBTN;
    @BindView(R.id.addressBTN)          View addressBTN;

    @BindView(R.id.emailTXT)            TextView emailTXT;
    @BindView(R.id.phoneTXT)            TextView phoneTXT;
    @BindView(R.id.identityTXT)         TextView identityTXT;
    @BindView(R.id.selfieTXT)           TextView selfieTXT;
    @BindView(R.id.addressTXT)          TextView addressTXT;


    @State String identityURL;
    @State String selfieURL;
    @State String addressURL;

    public static VerificationFragment newInstance() {
        VerificationFragment fragment = new VerificationFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_verification;
    }

    @Override
    public void onViewReady() {
        verificationActivity = (VerificationActivity) getContext();
        verificationActivity.setTitle("Account Verification");
        emailBTN.setOnClickListener(this);
        phoneBTN.setOnClickListener(this);
        identityBTN.setOnClickListener(this);
        selfieBTN.setOnClickListener(this);
        addressBTN.setOnClickListener(this);
        verificationActivity.setBackBTN(true);
        Analytics.trackEvent("rewards_verification_VerificationFragment_onViewReady");
    }

    @Override
    public void onResume() {
        super.onResume();
        KYC.getDefault().kyc(getContext());
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.emailBTN:
                if (userItem.kyc.data.emailStatusDisplay.equalsIgnoreCase("verify")){
                    verificationActivity.openEmailFragment();
                }else {
                    verificationActivity.openVerificationImageFragment(userItem.kyc.data.email, "Email", "", userItem.kyc.data.emailStatusDisplay);
                }
                Analytics.trackEvent("rewards_verification_VerificationFragment_emailBTN");
                break;
            case R.id.phoneBTN:
                if (userItem.kyc.data.contactNumberStatusDisplay.equalsIgnoreCase("verify")){
                    verificationActivity.openPhoneFragment();
                }else {
                    verificationActivity.openVerificationImageFragment(userItem.kyc.data.contactNumber, "Phone", "", userItem.kyc.data.contactNumberStatusDisplay);
                }
                Analytics.trackEvent("rewards_verification_VerificationFragment_phoneBTN");
                break;
            case R.id.identityBTN:
                if (userItem.kyc.data.idStatusDisplay.equalsIgnoreCase("verify")){
                    verificationActivity.openIdentityFragment();
                }else {
                    verificationActivity.openVerificationImageFragment(identityURL, "Identity", userItem.kyc.data.idType, userItem.kyc.data.idStatusDisplay);
                }
                Analytics.trackEvent("rewards_verification_VerificationFragment_identityBTN");
                break;
            case R.id.selfieBTN:
                if (userItem.kyc.data.selfieStatusDisplay.equalsIgnoreCase("verify")){
                    verificationActivity.openSelfieFragment();
                }else {
                    verificationActivity.openVerificationImageFragment(selfieURL, "Selfie", "",userItem.kyc.data.selfieStatusDisplay);
                }
                Analytics.trackEvent("rewards_verification_VerificationFragment_selfieBTN");
                break;
            case R.id.addressBTN:
                if (userItem.kyc.data.addressStatusDisplay.equalsIgnoreCase("verify")){
                    verificationActivity.openAddressFragment();
                }else {
                    verificationActivity.openVerificationImageFragment(addressURL, "Address", "", userItem.kyc.data.addressStatusDisplay);
                }
                Analytics.trackEvent("rewards_verification_VerificationFragment_addressBTN");
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(KYC.KYCResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
//            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            emailTXT.setText(singleTransformer.data.kyc.data.emailStatusDisplay);
            phoneTXT.setText(singleTransformer.data.kyc.data.contactNumberStatusDisplay);
            identityTXT.setText(singleTransformer.data.kyc.data.idStatusDisplay);
            selfieTXT.setText(singleTransformer.data.kyc.data.selfieStatusDisplay);
            addressTXT.setText(singleTransformer.data.kyc.data.addressStatusDisplay);

            addressURL = singleTransformer.data.kyc.data.address.fullPath;
            selfieURL = singleTransformer.data.kyc.data.selfie.fullPath;
            identityURL = singleTransformer.data.kyc.data.id.fullPath;

            userItem = singleTransformer.data;

        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}
