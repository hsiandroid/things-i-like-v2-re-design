package com.thingsilikeapp.android.fragment.registration;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.NewRegistrationActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.server.request.ValidateRequest;
import com.thingsilikeapp.server.transformer.ValidateTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class RegisterEFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = RegisterEFragment.class.getName();

    private NewRegistrationActivity registrationActivity;

    @BindView(R.id.nextBTN)         TextView nextBTN;
    @BindView(R.id.backBTN)         TextView backBTN;
    @BindView(R.id.phoneBTN)        View phoneBTN;
    @BindView(R.id.emailET)         EditText emailET;

    @State String username;
    @State String gender;
    @State String birthdate;
    @State String name;

    public static RegisterEFragment newInstance(String username, String gender, String birthdate, String name) {
        RegisterEFragment fragment = new RegisterEFragment();
        fragment.username = username;
        fragment.gender = gender;
        fragment.birthdate = birthdate;
        fragment.name = name;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_registration_e;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (NewRegistrationActivity) getContext();
        nextBTN.setOnClickListener(this);
        phoneBTN.setOnClickListener(this);
        backBTN.setOnClickListener(this);
        Analytics.trackEvent("registration_RegisterEFragment_onViewReady");
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.nextBTN:
                validate();
                Analytics.trackEvent("registration__RegisterEFragment_nextBTN");
                break;
            case R.id.backBTN:
                registrationActivity.startLandingActivity();
                Analytics.trackEvent("registration__RegisterEFragment_backBTN");
                break;
            case R.id.phoneBTN:
                registrationActivity.openFFragment(username, gender, birthdate, name);
                Analytics.trackEvent("registration__RegisterEFragment_phoneBTN");
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(ValidateRequest.ServerResponse responseData) {
        ValidateTransformer validateTransformer = responseData.getData(ValidateTransformer.class);
        if(validateTransformer.status){
            registrationActivity.openGFragment(username,gender,birthdate,name,emailET.getText().toString(), "email", "", "");
        }else{
            if(validateTransformer.hasRequirements()){
                ErrorResponseManger.first(emailET, validateTransformer.requires.value);
            } else {
                ToastMessage.show(getContext(), validateTransformer.msg, ToastMessage.Status.FAILED);
            }
        }
    }

    private void validate(){
        ValidateRequest createCommentRequest = new ValidateRequest(getContext());
        createCommentRequest
                .showNoInternetConnection(false)
                .addParameters(Keys.server.key.FIELD, "email")
                .addParameters(Keys.server.key.VALUE, emailET.getText().toString())
                .execute();
    }
}
