package com.thingsilikeapp.android.fragment.search;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.request.social.LikesRequest;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.android.activity.SearchActivity;
import com.thingsilikeapp.android.adapter.SuggestionRecycleViewAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class LikesFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        SearchActivity.SearchListener,
        EndlessRecyclerViewScrollListener.Callback,
        SuggestionRecycleViewAdapter.ClickListener{

    public static final String TAG = LikesFragment.class.getName();
    private SearchActivity searchActivity;
    private LikesRequest likesRequest;
    private int search = 0;

    private SuggestionRecycleViewAdapter likesRecycleViewAdapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;

    @State int wishListID;

    @BindView(R.id.likesRV)         RecyclerView likesRV;
    @BindView(R.id.likesSRL)        SwipeRefreshLayout likesSRL;
    @BindView(R.id.placeHolderCON)  View placeHolderCON;
    @BindView(R.id.searchCON)       View searchCON;
    @BindView(R.id.thingsPB)        View thingsPB;

    public static LikesFragment newInstance(int wishListID) {
        LikesFragment likesFragment = new LikesFragment();
        likesFragment.wishListID = wishListID;
        return likesFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_likes;
    }

    @Override
    public void onViewReady() {
        searchActivity = (SearchActivity) getActivity();
        searchActivity.showSearchContainer(true);
        setupFollowersListView();
        initSuggestionAPI();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void setupFollowersListView() {

        likesRecycleViewAdapter = new SuggestionRecycleViewAdapter(getContext());
        likesRecycleViewAdapter.setOnItemClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);

        likesRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        likesRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        likesRV.setLayoutManager(linearLayoutManager);
        likesRV.setAdapter(likesRecycleViewAdapter);
    }

    private void initSuggestionAPI(){

        likesSRL.setColorSchemeResources(R.color.colorPrimary);
        likesSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        likesSRL.setOnRefreshListener(this);
        likesRecycleViewAdapter.reset();

        likesRequest = new LikesRequest(getContext());
        likesRequest
                .setSwipeRefreshLayout(likesSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showSwipeRefreshLayout(true)
                .setPerPage(20);
    }

    private void refreshList(){
        thingsPB.setVisibility(View.GONE);
        endlessRecyclerViewScrollListener.reset();
        likesRecycleViewAdapter.reset();
        likesRequest
                .showSwipeRefreshLayout(true)
                .clearParameters()
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .addParameters(Keys.server.key.KEYWORD, searchActivity.getSearchEditText().getText().toString())
                .first();
    }

    @Override
    public void onCommandClick(UserItem userItem) {
        if(userItem.social.data.is_following.equals("yes")){
            likesRecycleViewAdapter.unFollowUser(userItem);
        }else{
            likesRecycleViewAdapter.followUser(userItem);
        }
    }

    @Override
    public void onUserClick(UserItem userItem) {

    }

    @Override
    public void onFollowSuccessClick() {

    }

    @Override
    public void onResume() {
        super.onResume();
        searchActivity.setSearchListener(this);
        refreshList();
    }

    @Override
    public void onPause() {
        searchActivity.setSearchListener(null);
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(LikesRequest.ServerResponse responseData) {
        SuggestionTransformer suggestionTransformer = responseData.getData(SuggestionTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                likesRecycleViewAdapter.addNewData(suggestionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                likesRecycleViewAdapter.setNewData(suggestionTransformer.data);
            }

            thingsPB.setVisibility(View.GONE);

            if (search == 0){
                searchCON.setVisibility(View.GONE);
                if(likesRecycleViewAdapter.getItemCount() == 0){
                    placeHolderCON.setVisibility(View.VISIBLE);
                    likesRV.setVisibility(View.GONE);
                }else {
                    placeHolderCON.setVisibility(View.GONE);
                    likesRV.setVisibility(View.VISIBLE);
                }
            }else {
                placeHolderCON.setVisibility(View.GONE);
                if(likesRecycleViewAdapter.getItemCount() == 0){
                    searchCON.setVisibility(View.VISIBLE);
                    likesRV.setVisibility(View.GONE);
                }else {
                    searchCON.setVisibility(View.GONE);
                    likesRV.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void searchTextChange(final String keyword) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                refreshList();
                if (searchActivity.getSearchEditText().getText().toString().equals("") || searchActivity.getSearchEditText().getText().toString().equals(null)){
                    search = 0;
                }else {
                    search = 1;
                }
            }
        });
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        thingsPB.setVisibility(likesRequest.hasMorePage() ? View.VISIBLE : View.GONE);
        likesRequest.nextPage();
    }
}
