package com.thingsilikeapp.android.fragment.main;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.asksira.bsimagepicker.Utils;
import com.google.gson.Gson;
import com.microsoft.appcenter.analytics.Analytics;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.MessageActivity;
import com.thingsilikeapp.android.adapter.InboxRecyclerViewAdapter;
import com.thingsilikeapp.android.dialog.ViewImageDialog;
import com.thingsilikeapp.data.model.ChatModel;
import com.thingsilikeapp.data.model.ChatThreadModel;
import com.thingsilikeapp.data.model.GemItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.Chat;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.DownloadFileManager;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.pusher.Cred;
import com.thingsilikeapp.vendor.android.widget.ImageManager;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.CollectionTransformer;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import dk.nodes.filepicker.FilePickerActivity;
import dk.nodes.filepicker.FilePickerConstants;
import dk.nodes.filepicker.uriHelper.FilePickerUriHelper;
import icepick.State;

public class KendraFragment extends BaseFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener,
        InboxRecyclerViewAdapter.ClickListener, DownloadFileManager.DownloadCallback, EndlessRecyclerViewScrollListener.Callback,
        BSImagePicker.OnSingleImageSelectedListener, ImageManager.Callback {

    public static final String TAG = KendraFragment.class.getName();

    private MessageActivity messageActivity;

    public static KendraFragment newInstance(int chat_id) {
        KendraFragment fragment = new KendraFragment();
        fragment.chat_id = chat_id;
        return fragment;
    }

    private InboxRecyclerViewAdapter inboxRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private DownloadFileManager downloadFileManager;
    private ChatThreadModel chatThreadModel;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private APIRequest apiRequest;
    private Pusher pusher;

    private static final int PICK_FILE_RESULT_CODE = 1;

    @BindView(R.id.defaultRV)                   RecyclerView defaultRV;
    @BindView(R.id.messageET)                   EditText messageET;
    @BindView(R.id.sendBTN)                     View sendBTN;
    @BindView(R.id.fileBTN)                     View fileBTN;
    @BindView(R.id.imageBTN)                    View imageBTN;
    @BindView(R.id.joinBTN)                     TextView joinBTN;
    @BindView(R.id.titleTXT)                   TextView titleTXT;
    @BindView(R.id.messageCON)                  View messageCON;
    @BindView(R.id.mainBackButtonIV)            ImageView mainBackButtonIV;
    @BindView(R.id.infoBTN)                     ImageView infoBTN;
    @BindView(R.id.chatSRL)                     SwipeRefreshLayout chatSRL;
    @BindView(R.id.rewardCON)                   View rewardCON;
    @BindView(R.id.gemTXT)                      TextView gemTXT;
    @BindView(R.id.percentPB)                   ProgressBar percentPB;

    @State int chat_id;
    @State String title;
    @State int tempID = 0;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_thread;
    }

    @Override
    public void onViewReady() {
        messageActivity = (MessageActivity) getContext();
        messageActivity.openInfo();
        sendBTN.setOnClickListener(this);
        fileBTN.setOnClickListener(this);
        imageBTN.setOnClickListener(this);
        setUpListView();
        chatSRL.setOnRefreshListener(this);
        chatSRL.setColorSchemeResources(R.color.colorPrimary);
        downloadFileManager = DownloadFileManager.newInstance(getContext(), null, this);
        joinBTN.setOnClickListener(this);
        setupPusher("groupchat."+chat_id);
        setupPusherGem("user.wallet."+UserData.getUserId());
        mainBackButtonIV.setOnClickListener(this);
        infoBTN.setOnClickListener(this);
        gemTXT.setText(UserData.getDisplayCrystalFragment());
        setRewardFragments();
        refreshList();
        titleTXT.setSelected(true);
        percentPB.setMax(100);
        Chat.getDefault().detail(getContext(), chat_id);
        Analytics.trackEvent("messages_GroupThreadFragment_onViewReady");
    }

    private void setUpListView(){
        inboxRecyclerViewAdapter = new InboxRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        defaultRV.setLayoutManager(linearLayoutManager);
        defaultRV.setAdapter(inboxRecyclerViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        defaultRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        inboxRecyclerViewAdapter.setOnItemClickListener(this);
    }

    private void refreshList(){
        apiRequest = Chat.getDefault().chatThread(getContext(), chat_id, chatSRL);
        apiRequest.first();
    }

    @Subscribe
    public void onResponse(Chat.ChatThreadResponse colletionResponse) {
        CollectionTransformer<ChatThreadModel> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            if (colletionResponse.isNext()){
                inboxRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else {
                endlessRecyclerViewScrollListener.reset();
                inboxRecyclerViewAdapter.setNewData(collectionTransformer.data);
                Log.e("DATALIST", "List: " + collectionTransformer.data);
            }

            if(collectionTransformer.chat_status.equalsIgnoreCase("inactive")){
                sendBTN.setEnabled(false);
                fileBTN.setEnabled(false);
                messageActivity.hideInfo();
            }else{
                sendBTN.setEnabled(true);
                fileBTN.setEnabled(true);
                messageActivity.openInfo();
            }

            if (!collectionTransformer.is_participant){
                joinBTN.setVisibility(View.VISIBLE);
                messageCON.setVisibility(View.GONE);
                messageActivity.hideInfo();
            }else{
                joinBTN.setVisibility(View.GONE);
                messageCON.setVisibility(View.VISIBLE);
                messageActivity.openInfo();
            }
            messageActivity.setTitle(collectionTransformer.chat_title);
            titleTXT.setText(collectionTransformer.chat_title);
        }
    }

    @Subscribe
    public void onResponse(Chat.ChatDetailResponse responseData) {
        SingleTransformer<ChatModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            Picasso.with(getContext())
                    .load(singleTransformer.data.info.data.avatar.fullPath)
                    .placeholder(R.drawable.placeholder_avatar)
                    .centerCrop()
                    .fit()
                    .into(infoBTN);
        }else{
            infoBTN.setEnabled(false);
        }
    }

    @Subscribe
    public void onResponse(Chat.SendMessageResponse responseData) {
        SingleTransformer<ChatThreadModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            inboxRecyclerViewAdapter.updateMessage(singleTransformer.data);
            defaultRV.smoothScrollToPosition(0);
            singleTransformer.data.status = "Sent.";
            Log.e("data", ">>" + singleTransformer.data.toString());
        }else{

        }
    }

    @Subscribe
    public void onResponse(Chat.ChatJoinResponse responseData) {
        SingleTransformer<ChatThreadModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            refreshList();
            joinBTN.setVisibility(View.GONE);
            messageCON.setVisibility(View.VISIBLE);
        }else{

        }
    }

    @Subscribe
    public void onResponse(Chat.SendFileResponse responseData) {
        SingleTransformer<ChatThreadModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            inboxRecyclerViewAdapter.updateMessage(singleTransformer.data);
            defaultRV.smoothScrollToPosition(0);
            singleTransformer.data.status = "Sent.";
            Log.e("data", ">>" + singleTransformer.data.toString());
        }else{

        }
    }

    @Subscribe
    public void onResponse(Chat.RemoveMessageResponse responseData) {
        SingleTransformer<ChatThreadModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            inboxRecyclerViewAdapter.updateMessage(chatThreadModel);
            refreshList();
        }else{

        }
    }

    public void attemptSend(){
        String message = messageET.getText().toString();
        if(message.length() != 0){
            tempID--;
            Chat.getDefault().sendMessage(getContext(), chat_id, message, tempID);
            inboxRecyclerViewAdapter.newMessage(ChatThreadModel.generateMessage(getContext(),tempID, message, null, "message"));
            defaultRV.smoothScrollToPosition(0);
            messageET.setText("");
        }
    }



    public double intFormatter(String value){
        if (!value.equalsIgnoreCase("") && value != null){
            return Double.parseDouble(value);
        }
        return 0;
    }

    public void updateReward(String lykagem, String progress, int max){
        if(gemTXT != null){
            gemTXT.setText(lykagem);
        }
        setRewardFragment(progress, max);
    }

    public void setRewardFragment(String progress, int max){
            try {
                percentPB.setMax(max);
                double finalProgress = intFormatter(progress);
                if (finalProgress == 0){
                    percentPB.setProgress(1);
                }else if (finalProgress > 1 && finalProgress < 15){
                    percentPB.setProgress(10);
                }else if (finalProgress > 16 && finalProgress < 25){
                    percentPB.setProgress(20);
                }else if (finalProgress > 26 && finalProgress < 45){
                    percentPB.setProgress(40);
                }else if (finalProgress > 46 && finalProgress < 65){
                    percentPB.setProgress(60);
                }else if (finalProgress > 66 && finalProgress < 85){
                    percentPB.setProgress(80);
                }else{
                    percentPB.setProgress((int)finalProgress);
                }
            } catch (Exception e) {
                System.out.println("Error " + e.getMessage());
            }
    }

    public void setRewardFragments(){
        percentPB.setMax(100);
        double finalProgress = intFormatter(UserData.getRewardFragment());
        if (finalProgress == 0){
            percentPB.setProgress(1);
        }else if (finalProgress > 1 && finalProgress < 15){
            percentPB.setProgress(10);
        }else if (finalProgress > 16 && finalProgress < 25){
            percentPB.setProgress(20);
        }else if (finalProgress > 26 && finalProgress < 45){
            percentPB.setProgress(40);
        }else if (finalProgress > 46 && finalProgress < 65){
            percentPB.setProgress(60);
        }else if (finalProgress > 66 && finalProgress < 85){
            percentPB.setProgress(80);
        }else{
            percentPB.setProgress((int)finalProgress);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        messageActivity.openInfo();
        messageActivity.hideInfoMessage();
        setupPusherGem("user.wallet."+UserData.getUserId());
        setRewardFragments();
    }

    private void openFilePicker(){
        Intent intent = new Intent(getContext(), FilePickerActivity.class);
        intent.putExtra(FilePickerConstants.FILE, true);
        intent.putExtra(FilePickerConstants.MULTIPLE_TYPES, new String[]{
                FilePickerConstants.MIME_IMAGE,
                FilePickerConstants.MIME_VIDEO,
                FilePickerConstants.MIME_PDF,
                FilePickerConstants.MIME_PDF,
                FilePickerConstants.MIME_TEXT_PLAIN,
        });

        startActivityForResult(intent, PICK_FILE_RESULT_CODE);
    }

    @Override
    public  void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case PICK_FILE_RESULT_CODE:
                if(resultCode == Activity.RESULT_OK){
                    defaultRV.smoothScrollToPosition(0);
                    File file = FilePickerUriHelper.getFile(getContext(), data);
                    String type = getMimeType(getContext(), file);
                    String types;
                    if (type.contains("image")){
                        types = "image";
                    }else {
                        types = "file";
                        }
                    tempID--;
                    inboxRecyclerViewAdapter.newMessage(ChatThreadModel.generateMessage(getContext(),tempID, file.getName(), file, types));
                    Chat.getDefault().sendFile(getContext(), chat_id, file, tempID);
                }
                break;
        }
    }

    public static String getMimeType(Context context, File file) {
        Uri uri = Uri.fromFile(file);
        String mimeType;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        downloadFileManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.sendBTN:
                attemptSend();
                Analytics.trackEvent("messages_ChatChangeNameFragment_sendBTN");
                break;
            case R.id.fileBTN:
                openFilePicker();
                Analytics.trackEvent("messages_ChatChangeNameFragment_fileBTN");
                break;
            case R.id.imageBTN:
                BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.thingsilikeapp.fileprovider")
                        .setMaximumDisplayingImages(24) //Default: Integer.MAX_VALUE. Don't worry about performance :)
                        .setSpanCount(3) //Default: 3. This is the number of columns
                        .setGridSpacing(Utils.dp2px(2)) //Default: 2dp. Remember to pass in a value in pixel.
                        .setPeekHeight(Utils.dp2px(360)) //Default: 360dp. This is the initial height of the dialog.
                        .setTag("A request ID") //Default: null. Set this if you need to identify which picker is calling back your fragment / activity.
                        .build();
                singleSelectionPicker.show(getChildFragmentManager(), "picker");
                Analytics.trackEvent("messages_ChatChangeNameFragment_imageBTN");
                break;
            case R.id.joinBTN:
                Chat.getDefault().join(getContext(), chat_id);
                Analytics.trackEvent("messages_ChatChangeNameFragment_joinBTN");
                break;
            case R.id.mainBackButtonIV:
                messageActivity.onBackPressed();
                Analytics.trackEvent("messages_ChatChangeNameFragment_mainBackButtonIV");
                break;
            case R.id.infoBTN:
                messageActivity.openEditFragment(chat_id);
                Analytics.trackEvent("messages_ChatChangeNameFragment_infoBTN");
                break;
        }
    }

    @Override
    public void onItemClick(int sampleModel) {

    }

    @Override
    public void onDeleteClick(ChatThreadModel sampleModel) {
        Chat.getDefault().removeMessage(getContext(), chat_id, sampleModel.id);
    }

    @Override
    public void onFileClick(ChatThreadModel sampleModel) {
//        downloadFileManager.setFileName(sampleModel.content);
//        downloadFileManager.downloadWithPermissionGranted(sampleModel.info.data.attachment.fullPath);
//        WebViewDialog.newInstance(sampleModel.info.data.attachment.fullPath).show(getFragmentManager(), TAG);
//        String url = sampleModel.info.data.attachment.fullPath;
//        int length = url.length();
//        String type = url.substring(length-3, length);
//        Log.e("EEEEEE", ">>>> " + type);
//        if (type.equalsIgnoreCase("mp4")){
//            WebViewDialog.newInstance(sampleModel.info.data.attachment.fullPath).show(getFragmentManager(), TAG);
//        }else{
            Intent intent = new Intent();
            intent.setDataAndType(Uri.parse(sampleModel.info.data.attachment.fullPath), "application/pdf");
            startActivity(intent);
//        }

        Log.e("EEEEE", ">>>>" + sampleModel.info.data.attachment.fullPath);
        Analytics.trackEvent("messages_ChatChangeNameFragment_onFileClick");
    }

    @Override
    public void onImageClick(ChatThreadModel sampleModel) {
        ViewImageDialog.newInstance(sampleModel.info.data.attachment.fullPath, sampleModel.content).show(getFragmentManager(), ViewImageDialog.TAG);
        Analytics.trackEvent("messages_ChatChangeNameFragment_onImageClick");
    }

    @Override
    public void onAvatarClick(ChatThreadModel sampleModel) {
        messageActivity.startProfileActivity(sampleModel.senderUserId);
        Analytics.trackEvent("messages_ChatChangeNameFragment_onAvatarClick");
    }

    @Override
    public void onFileDownloadCompleted() {
        ToastMessage.show(getContext(), "Downloaded Successfully", ToastMessage.Status.SUCCESS);
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    private void setupPusher(String channelName){
        PusherOptions options = new PusherOptions();
        options.setCluster(Cred.CLUSTER);
        pusher = new Pusher(Cred.KEY, options);
        Channel channel = pusher.subscribe(channelName);

        channel.bind("new_message", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                processPusher(data,eventName);
            }
        });
        channel.bind("deleted_message", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                processPusher(data,eventName);
            }
        });
        pusher.connect();
    }

    private void processPusher(final String data, final String eventName){
        final ChatThreadModel chatThreadModel = new Gson().fromJson(data, ChatThreadModel.class);

        messageActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch(eventName){
                    case "deleted_message":
                    case "updated_message":
                        inboxRecyclerViewAdapter.updateMessage(chatThreadModel);
                        break;
                    default:
                        if(chatThreadModel.senderUserId != UserData.getUserId()){
                            inboxRecyclerViewAdapter.newMessage(chatThreadModel);
                        }
                }

                if (defaultRV != null){
                    defaultRV.smoothScrollToPosition(0);
                }
            }
        });
        Log.e(TAG, "PUSHER TO" + eventName);
    }

    private void setupPusherGem(String channelName){
        PusherOptions options = new PusherOptions();
        options.setCluster(Cred.CLUSTER);
        pusher = new Pusher(Cred.KEY, options);
        Channel channel = pusher.subscribe(channelName);

        channel.bind("UpdatedLykaGems", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                processGemPusher(data,eventName);
            }
        });

        pusher.connect();
    }

    private void processGemPusher(final String data, final String eventName){
        final GemItem gemItem = new Gson().fromJson(data, GemItem.class);

        messageActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch(eventName){
                    case "UpdatedLykaGems":
                        updateReward(gemItem.lykagemDisplay, gemItem.progress + "", gemItem.max);
                        UserData.updateDisplayRewardCrystal(gemItem.lykagemDisplay);
                        UserData.updateRewardCrystal(gemItem.lykagem + "");
                        break;

                }

            }
        });
        Log.e(TAG, "PUSHER TO" + eventName);
    }

    @Override
    public void onSingleImageSelected(Uri uri, String s) {
        try{
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(messageActivity.getContentResolver(),uri);
            int nh = (int) ( bitmap.getHeight() * (512.0 / bitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
            ImageManager.getFileFromBitmap(messageActivity,scaled,"image1",this);
        } catch (IOException e){
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void success(File file) {
        tempID--;
        inboxRecyclerViewAdapter.newMessage(ChatThreadModel.generateMessage(getContext(),tempID, file.getName(), file, "image"));
        Chat.getDefault().sendFile(getContext(), chat_id, file, tempID);
    }
}
