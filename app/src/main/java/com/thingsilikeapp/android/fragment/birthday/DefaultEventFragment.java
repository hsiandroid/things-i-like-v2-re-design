package com.thingsilikeapp.android.fragment.birthday;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.BirthdayActivity;
import com.thingsilikeapp.android.adapter.BirthdayGroupRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.EventRecycleViewAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.GroupItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.social.FollowingRequest;
import com.thingsilikeapp.server.request.user.GroupRequest;
import com.thingsilikeapp.server.transformer.CollectionTransformer;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.server.transformer.user.GroupCollectionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class DefaultEventFragment extends BaseFragment  implements
        SwipeRefreshLayout.OnRefreshListener,
        BirthdayGroupRecycleViewAdapter.ClickListener,
        EndlessRecyclerViewScrollListener.Callback,
        EventRecycleViewAdapter.ClickListener{

    public static final String TAG = DefaultEventFragment.class.getName();

    private BirthdayActivity birthdayActivity;
    private EventRecycleViewAdapter eventRecycleViewAdapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;
    private FollowingRequest followingRequest;

    private BirthdayGroupRecycleViewAdapter birthdayGroupRecycleViewAdapter;
    private LinearLayoutManager groupLayoutManager;
    private GroupRequest groupRequest;

    @BindView(R.id.celebrantSRL)            SwipeRefreshLayout celebrantSRL;
    @BindView(R.id.celebrantRV)             RecyclerView celebrantRV;
    @BindView(R.id.groupRV)                 RecyclerView groupRV;
    @BindView(R.id.placeHolderCON)          View placeHolderCON;

    @State String raw;

    private EventItem eventItem;

    public static DefaultEventFragment newInstance(String raw) {
        DefaultEventFragment celebrantFragment = new DefaultEventFragment();
        celebrantFragment.raw = raw;
        return celebrantFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_default_event;
    }

    @Override
    public void onViewReady() {
        birthdayActivity = (BirthdayActivity) getContext();

        eventItem = new Gson().fromJson(raw, EventItem.class);
        birthdayActivity.setTitle(StringFormatter.titleCase(eventItem.title));
        setupListView();
        initNotificationAPI();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
        searchGroup("");
    }

    public void setupListView(){
        eventRecycleViewAdapter = new EventRecycleViewAdapter(getContext());
        eventRecycleViewAdapter.setOnItemClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);

        celebrantRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        celebrantRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        celebrantRV.setLayoutManager(linearLayoutManager);
        celebrantRV.setAdapter(eventRecycleViewAdapter);

        birthdayGroupRecycleViewAdapter = new BirthdayGroupRecycleViewAdapter(getContext());
        birthdayGroupRecycleViewAdapter.setOnItemClickListener(this);
        groupLayoutManager = new LinearLayoutManager(getContext());
        groupLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        groupRV.setLayoutManager(groupLayoutManager);
        groupRV.setAdapter(birthdayGroupRecycleViewAdapter);
    }

    private void initNotificationAPI(){

        celebrantSRL.setColorSchemeResources(R.color.colorPrimary);
        celebrantSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        celebrantSRL.setOnRefreshListener(this);

        groupRequest = new GroupRequest(getContext());
        groupRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION));
    }

    private void refreshList(){
        groupRequest.execute();
    }

    private void searchGroup(String group){
        followingRequest = new FollowingRequest(getContext());
        followingRequest
                .setSwipeRefreshLayout(celebrantSRL)
                .showSwipeRefreshLayout(true)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, UserData.getUserId())
                .addParameters(Keys.server.key.EVENT_ID, eventItem.id)
                .addParameters(Keys.server.key.GROUP, group)
                .first();
    }

    @Override
    public void onRefresh() {
        refreshList();
        searchGroup("");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(FollowingRequest.ServerResponse responseData) {
        SuggestionTransformer suggestionTransformer = responseData.getData(SuggestionTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                eventRecycleViewAdapter.addNewData(suggestionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                eventRecycleViewAdapter.setNewData(suggestionTransformer.data);
            }
        }

        if (eventRecycleViewAdapter.getItemCount() == 0){
            placeHolderCON.setVisibility(View.VISIBLE);
            celebrantRV.setVisibility(View.GONE);
        }else {
            placeHolderCON.setVisibility(View.GONE);
            celebrantRV.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onResponse(GroupRequest.ServerResponse responseData) {
        GroupCollectionTransformer collectionTransformer = responseData.getData(CollectionTransformer.class);
        if(collectionTransformer.status){
            collectionTransformer.data.add(0, new GroupItem("ALL"));
            birthdayGroupRecycleViewAdapter.setNewData(collectionTransformer.data);
            birthdayGroupRecycleViewAdapter.setSelected("All");
        }else{
            refreshList();
        }
    }

    @Override
    public void onItemClick(GroupItem groupItem) {
        searchGroup(groupItem.title.equalsIgnoreCase("all") ? "" : groupItem.title);
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        followingRequest.nextPage();
    }

    @Override
    public void onCommandClick(UserItem userItem) {
        if(!userItem.is_event_greeted){
            birthdayActivity.startImageEditorActivity(eventItem, userItem, "create");
        }
    }

    @Override
    public void onUserClick(UserItem userItem) {

    }
}
