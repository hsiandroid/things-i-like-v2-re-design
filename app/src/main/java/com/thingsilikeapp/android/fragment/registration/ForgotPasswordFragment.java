package com.thingsilikeapp.android.fragment.registration;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RegistrationActivity;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.server.request.auth.VerificationCodeRequest;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.base.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = ForgotPasswordFragment.class.getName();

    private RegistrationActivity registrationActivity;

    @BindView(R.id.requestEmailTV)      TextView requestEmailTV;
    @BindView(R.id.cancelTV)            TextView cancelTV;
    @BindView(R.id.emailET)             EditText emailET;

    public static ForgotPasswordFragment newInstance() {
        ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment();
        return forgotPasswordFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getActivity();
        registrationActivity.setTitle(getString(R.string.title_forgot_password));
        requestEmailTV.setOnClickListener(this);
        cancelTV.setOnClickListener(this);
        Analytics.trackEvent("registration_ForgotPasswordFragment_onViewReady");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.requestEmailTV:
                attemptRequestVerification();
                Analytics.trackEvent("registration_ForgotPasswordFragment_requestEmailTV");
                break;
            case R.id.cancelTV:
                registrationActivity.onBackPressed();
                Analytics.trackEvent("registration_ForgotPasswordFragment_cancelTV");
                break;
        }
    }

    public void openUrl(String url){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    private void attemptRequestVerification(){
        VerificationCodeRequest verificationCodeRequest = new VerificationCodeRequest(getContext());
        verificationCodeRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Checking your email...", false, false))
                .addParameters(Keys.server.key.EMAIL, emailET.getText().toString())
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(VerificationCodeRequest.ServerResponse responseData) {
        BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            registrationActivity.openSuccessFragment(emailET.getText().toString());
        }else {
            ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

}
