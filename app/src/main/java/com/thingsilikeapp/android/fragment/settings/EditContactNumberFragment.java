package com.thingsilikeapp.android.fragment.settings;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SettingsActivity;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.android.dialog.CountryDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.CountryData;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.user.ChangeContactNumberRequest;
import com.thingsilikeapp.server.request.user.ChangeEmailRequest;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.request.APIRequest;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class EditContactNumberFragment extends BaseFragment implements View.OnClickListener, CountryDialog.CountryPickerListener{

    public static final String TAG = EditContactNumberFragment.class.getName();


    private SettingsActivity settingsActivity;


    @BindView(R.id.contactET)           EditText contactET;
//    @BindView(R.id.professionET)        EditText professionET;
    @BindView(R.id.contactCodeTXT)      TextView contactCodeTXT;
    @BindView(R.id.contactCON)          View contactCON;
    @BindView(R.id.countryFlagIV)       ImageView countryFlagIV;
    @BindView(R.id.confirmBTN)          View confirmBTN;

    @State String country;
    @State String contact;
    @State String contry_code;

    private CountryData.Country countryData;

    public static EditContactNumberFragment newInstance(String country, String contact, String contry_code) {
        EditContactNumberFragment editContactNumberFragment = new EditContactNumberFragment();
        editContactNumberFragment.country = country;
        editContactNumberFragment.contact = contact;
        editContactNumberFragment.contry_code = contry_code;
        return editContactNumberFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_change_contact_number;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle("Change Contact Number");
        confirmBTN.setOnClickListener(this);
        countryData = CountryData.getCountryDataByCode1(country);
        contactET.setText(StringFormatter.replaceNull(contact).replace(contry_code, ""));
        contactCodeTXT.setText(contry_code);
        countryCode(countryData);
        confirmBTN.setEnabled(false);
        contactET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (contact.equalsIgnoreCase(s.toString()) || contactET.length() < 10){
                    confirmBTN.setEnabled(false);
                }else{
                    confirmBTN.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void countryCode(CountryData.Country country){
        contactCodeTXT.setText(country.code3);
        Glide.with(getContext())
                .load(country.getFlag())
                .into(countryFlagIV);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmBTN:
//                ChangeContactNumberRequest.getDefault().changeContact(getContext(), countryData.code1,contactCodeTXT.getText().toString() + contactET.getText().toString(), contactCodeTXT.getText().toString(), "otp");
                ChangeContactNumberRequest.getDefault().phoneOTP(getContext(), countryData.code1, contactCodeTXT.getText().toString() + contactET.getText().toString(), contactCodeTXT.getText().toString());
                break;
            case R.id.contactCON:
            case R.id.contactCodeTXT:
            case R.id.countryFlagIV:
                showCountryPicker();
                break;
        }
    }

    private void showCountryPicker(){
        CountryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
    }

    @Subscribe
    public void onResponse(ChangeContactNumberRequest.PhoneResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
//            contactET.setText("");
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            settingsActivity.openOTPChangeNumber(countryData.code1, contactCodeTXT.getText().toString() + contactET.getText().toString(),  contactCodeTXT.getText().toString());
        }
        else{
//            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
            if(singleTransformer.hasRequirements()){
                ErrorResponseManger.first(contactET, singleTransformer.errors.contact_number);
            }
        }

    }

    @Override
    public void onSelectCountry(CountryData.Country country, int requestCode) {
        countryData = country;
        countryCode(countryData);
    }
}
