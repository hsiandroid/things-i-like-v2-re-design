package com.thingsilikeapp.android.fragment.rewards.exchange;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.ExchangeActivity;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.ForgotPasscode;
import com.thingsilikeapp.server.request.user.ChangeContactNumberRequest;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ForgotPasscodeFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = ForgotPasscodeFragment.class.getName();


    public static ForgotPasscodeFragment newInstance(){
        ForgotPasscodeFragment forgotPasscodeFragment = new ForgotPasscodeFragment();
        return forgotPasscodeFragment;
    }


    @BindView(R.id.contactET)           EditText contactET;
    //    @BindView(R.id.professionET)        EditText professionET;
    @BindView(R.id.contactCodeTXT)      TextView contactCodeTXT;
    @BindView(R.id.contactCON)          View contactCON;
    @BindView(R.id.countryFlagIV)       ImageView countryFlagIV;
    @BindView(R.id.confirmBTN)          View confirmBTN;

    private ExchangeActivity exchangeActivity;


    @Override
    public int onLayoutSet(){
        return R.layout.fragment_forgot_passcode;
    }


    @Override
    public void onViewReady(){
        exchangeActivity = (ExchangeActivity) getContext();
        exchangeActivity.setTitle("Reset Passcode");
        confirmBTN.setOnClickListener(this);



        contactET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (contactET.length() < 10){
                    confirmBTN.setEnabled(false);
                }else{
                    confirmBTN.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }



    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Subscribe
    public void onResponse(ChangeContactNumberRequest.PhoneResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
//            exchangeActivity.openOTPChangeNumber(countryData.code1, contactCodeTXT.getText().toString() + contactET.getText().toString(),  contactCodeTXT.getText().toString());
        }
        else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);

        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmBTN:
                ForgotPasscode.getDefault().forgotPasscode(getContext());
                break;
        }

    }
}
