package com.thingsilikeapp.android.fragment.rewards.exchange;

import android.view.View;
import android.view.WindowManager;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.cardform.OnCardFormSubmitListener;
import com.braintreepayments.cardform.utils.CardType;
import com.braintreepayments.cardform.view.CardEditText;
import com.braintreepayments.cardform.view.CardForm;
import com.braintreepayments.cardform.view.SupportedCardTypesView;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RewardsActivity;
import com.thingsilikeapp.android.dialog.WebViewDialog;
import com.thingsilikeapp.data.model.BankCardItem;
import com.thingsilikeapp.data.model.EncashHistoryItem;
import com.thingsilikeapp.server.Wallet;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;


public class EncashmentCardFragment extends BaseFragment implements OnCardFormSubmitListener,
        CardEditText.OnCardTypeChangedListener,View.OnClickListener   {

    private static final CardType[] SUPPORTED_CARD_TYPES = { CardType.VISA, CardType.MASTERCARD,
            CardType.JCB, CardType.UNIONPAY };


    @BindView(R.id.supported_card_types)    SupportedCardTypesView supported_card_types;
    @BindView(R.id.card_form)               CardForm card_form;
    @BindView(R.id.saveBTN)                 TextView saveBTN;
    @BindView(R.id.termsBTN)                TextView termsBTN;

    private RewardsActivity rewardsActivity;



    public static EncashmentCardFragment newInstance() {
        EncashmentCardFragment encashmentcardFragment = new EncashmentCardFragment();
        return encashmentcardFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_encashment_card;
    }

    @Override
    public void onViewReady() {
        rewardsActivity = (RewardsActivity) getContext();
        rewardsActivity.setTitle("Add Card");
        rewardsActivity.showWallet(false);
        rewardsActivity.showOption(false);
        rewardsActivity.setTitleDark(false);
        rewardsActivity.showWallet(false);
        card_form.cardRequired(true)
                .maskCardNumber(true)
                .maskCvv(true)
                .expirationRequired(true)
                .cvvRequired(true)
//                .mobileNumberExplanation("Make sure SMS is enabled for this mobile number")
                .actionLabel(getString(R.string.purchase))
                .setup(getActivity());
        card_form.setOnCardFormSubmitListener(this);
        card_form.setOnCardTypeChangedListener(this);
        saveBTN.setOnClickListener(this);
        termsBTN.setOnClickListener(this);


        // Failure to set FLAG_SECURE exposes your app to screenshots allowing other apps to steal card information.
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
        Analytics.trackEvent("rewards_exchange_EncashmentCardFragment_onViewReady");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.saveBTN:
                setSaveBTN();
                break;
                case R.id.termsBTN:
                    WebViewDialog.newInstance("http://www.mylyka.com/terms-of-use").show(getChildFragmentManager(), WebViewDialog.TAG);
                break;
        }
    }


    @Subscribe
    public void onResponse(Wallet.AddCardResponse tranformerResponse) {
        SingleTransformer<BankCardItem> transformer = tranformerResponse.getData(SingleTransformer.class);
        if (transformer.status) {
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.SUCCESS);
            rewardsActivity.openEncashmentMyCardFragment();
            }else{
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.FAILED);
            }
    }

    public void setSaveBTN(){
        int month = Integer.parseInt(card_form.getExpirationMonth());
        Wallet.getDefault().addCard(getContext(), card_form.getCardNumber(), month, card_form.getExpirationYear());
    }

    @Override
    public void onCardTypeChanged(CardType cardType) {
        if (cardType == CardType.EMPTY) {
            supported_card_types.setSupportedCardTypes(SUPPORTED_CARD_TYPES);
        }
        else
            {
//                supported_card_types.setSelected(cardType);
                supported_card_types.setSupportedCardTypes(cardType);

            }
    }
    @Override
    public void onCardFormSubmit() {
        if (card_form.isValid())
        {
            Toast.makeText(getActivity(), R.string.valid, Toast.LENGTH_SHORT).show();
        }
        else
            {
                card_form.validate();
            Toast.makeText(getActivity(), R.string.invalid, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
