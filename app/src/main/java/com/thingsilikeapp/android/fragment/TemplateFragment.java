package com.thingsilikeapp.android.fragment;

import android.widget.TextView;

import com.thingsilikeapp.R;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class TemplateFragment extends BaseFragment {

    public static final String TAG = TemplateFragment.class.getName();

    @BindView(R.id.textviewTxt) TextView textViewTxt;

    public static TemplateFragment newInstance() {
        TemplateFragment fragment = new TemplateFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_template;
    }

    @Override
    public void onViewReady() {
        textViewTxt.setText("Hello fragment");
    }
}
