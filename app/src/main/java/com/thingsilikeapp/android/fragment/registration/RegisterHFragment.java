package com.thingsilikeapp.android.fragment.registration;

import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.NewRegistrationActivity;
import com.thingsilikeapp.android.dialog.WebViewDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.auth.SignUpRequest;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class RegisterHFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = RegisterHFragment.class.getName();

    private NewRegistrationActivity registrationActivity;

    @BindView(R.id.nextBTN)  TextView nextBTN;
    @BindView(R.id.backBTN)  TextView backBTN;
    @BindView(R.id.termsBTN) TextView termsBTN;

    @State String username;
    @State String gender;
    @State String birthdate;
    @State String name;
    @State String email;
    @State String type;
    @State String password;
    @State String country;
    @State String contry_code;

    public static RegisterHFragment newInstance(String username, String gender, String birthdate, String name, String email, String password, String type, String country, String code) {
        RegisterHFragment fragment = new RegisterHFragment();
        fragment.username = username;
        fragment.gender = gender;
        fragment.birthdate = birthdate;
        fragment.name = name;
        fragment.email = email;
        fragment.type = type;
        fragment.password = password;
        fragment.country = country;
        fragment.contry_code = code;
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_registration_h;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (NewRegistrationActivity) getContext();
        nextBTN.setOnClickListener(this);
        termsBTN.setOnClickListener(this);
        backBTN.setOnClickListener(this);
        Analytics.trackEvent("registration_RegisterHFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void attemptEmail() {
        SignUpRequest signUpRequest = new SignUpRequest(getContext());
        signUpRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Signing up...", false, false))
                .setDeviceRegID(registrationActivity.getDeviceRegID())
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.NAME, name)
                .addParameters(Keys.server.key.USERNAME, username)
                .addParameters(Keys.server.key.EMAIL, email)
                .addParameters(Keys.server.key.PASSWORD,password)
                .addParameters(Keys.server.key.GENDER, gender)
                .addParameters(Keys.server.key.BIRTHDATE, birthdate)
                .execute();
    }

    private void attemptContact() {
        SignUpRequest signUpRequest = new SignUpRequest(getContext());
        signUpRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Signing up...", false, false))
                .setDeviceRegID(registrationActivity.getDeviceRegID())
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.NAME, name)
                .addParameters(Keys.server.key.USERNAME, username)
                .addParameters(Keys.server.key.CONTACT_NUMBER,  email)
                .addParameters(Keys.server.key.COUNTRY_CODE, contry_code)
                .addParameters(Keys.server.key.COUNTRY, country)
                .addParameters(Keys.server.key.PASSWORD,password)
                .addParameters(Keys.server.key.GENDER, gender)
                .addParameters(Keys.server.key.BIRTHDATE, birthdate)
                .execute();
    }

    private void attempSignUp(){

//        if(type.equalsIgnoreCase("email")){
//            attemptEmail();
//        }else{
//            attemptContact();
//        }

        switch (type){
            case "email":
                attemptEmail();
                Analytics.trackEvent("registration_RegisterHFragment_email");
                break;
            case "contact":
                attemptContact();
                Analytics.trackEvent("registration_RegisterHFragment_contact");
                break;
        }

    }


    @Subscribe
    public void onResponse(SignUpRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            UserData.insert(userTransformer.userItem);
            UserData.insert(UserData.AUTHORIZATION, userTransformer.token);
            UserData.insert(UserData.FIRST_LOGIN, true);
            UserData.insert(UserData.SHOW_SHARE, true);
            UserData.insert(UserData.TOKEN_EXPIRED, false);

//            if (!UserData.getBoolean(UserData.WALKTHRU_DONE, false)) {
//                UserData.insert(UserData.WALKTHRU_DONE, false);
//                UserData.insert(UserData.WALKTHRU_CURRENT, 0);
//            } else {
//                UserData.insert(UserData.WALKTHRU_DONE, true);
//                UserData.insert(UserData.WALKTHRU_CURRENT, 10);
//            }
            UserData.insert(UserData.TOTAL_POST, userTransformer.userItem.statistics.data.total_wishlist);
            registrationActivity.startMainActivity("home");
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.SUCCESS);
            registrationActivity.facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT, FacebookCustomLoggerMessage.SIGN_UP);
        } else {
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.FAILED);
        }
    }


    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.nextBTN:
                Log.e("REGISTER", username + gender + birthdate + name + email + password);
                attempSignUp();
                Analytics.trackEvent("registration_RegisterHFragment_nextBTN");
                break;
            case R.id.backBTN:
                registrationActivity.startLandingActivity();
                Analytics.trackEvent("registration_RegisterHFragment_backBTN");
                break;
            case R.id.termsBTN:
                WebViewDialog.newInstance("http://mylyka.com/terms-of-use").show(getChildFragmentManager(), WebViewDialog.TAG);
                Analytics.trackEvent("registration_RegisterHFragment_termsBTN");
                break;
        }
    }
}
