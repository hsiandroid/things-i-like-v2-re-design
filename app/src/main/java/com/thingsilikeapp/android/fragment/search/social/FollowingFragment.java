package com.thingsilikeapp.android.fragment.search.social;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SearchActivity;
import com.thingsilikeapp.android.adapter.GroupFollowingRecycleViewAdapter;
import com.thingsilikeapp.android.adapter.SuggestionRecycleViewAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.GroupFollowingItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.social.FollowingRequest;
import com.thingsilikeapp.server.request.social.GroupFollowingRequest;
import com.thingsilikeapp.server.transformer.social.GroupFollowingCollectionTransformer;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import icepick.State;

public class FollowingFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        SearchActivity.SearchListener,
        EndlessRecyclerViewScrollListener.Callback,
        SuggestionRecycleViewAdapter.ClickListener,GroupFollowingRecycleViewAdapter.ClickListener{

    public static final String TAG = FollowingFragment.class.getName();

    private BaseActivity baseActivity;
    private FollowingRequest followingRequest;
    private GroupFollowingRequest groupFollowingRequest;

    private SuggestionRecycleViewAdapter suggestionRecycleViewAdapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;

    private GroupFollowingRecycleViewAdapter groupFollowingRecycleViewAdapter;
    private LinearLayoutManager grouplLinearLayoutManager;

    private Timer timer;
    private boolean isGroup;

    @State int userID;
    @State boolean isRequest;

    @BindView(R.id.followingRV)                 RecyclerView followingRV;
    @BindView(R.id.groupFollowingRV)            RecyclerView groupFollowingRV;
    @BindView(R.id.followingSRL)                SwipeRefreshLayout followingSRL;
    @BindView(R.id.followingPB)                 View followingPB;
    @BindView(R.id.loadMorePB)                  View loadMorePB;
    @BindView(R.id.searchET)                    EditText searchET;
    @BindView(R.id.searchBTN)                   View searchBTN;
    @BindView(R.id.placeHolderCON)              View placeHolderCON;
    @BindView(R.id.placeHolderGroupCON)         View placeHolderGroupCON;

    public static FollowingFragment newInstance(int userID) {
        FollowingFragment followingFragment = new FollowingFragment();
        followingFragment.userID = userID;
        return followingFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_following;
    }

    @Override
    public void onViewReady() {
        baseActivity = ((BaseActivity) getContext());
        Bundle bundle =  baseActivity.getFragmentBundle();
        if(bundle != null){
            isRequest = bundle.getBoolean("is_request", false);
        }

        setupFollowersListView();
        setUpGroupFollowingListView();
        initSuggestionAPI();
        initGroupAPI();
        initSearch();
        Analytics.trackEvent("search_social_FollowingFragment_onViewReady");
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void setupFollowersListView() {

        suggestionRecycleViewAdapter = new SuggestionRecycleViewAdapter(getContext());
        suggestionRecycleViewAdapter.setRequest(isRequest);
        suggestionRecycleViewAdapter.setOnItemClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);

        followingRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        followingRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        followingRV.setLayoutManager(linearLayoutManager);
        followingRV.setAdapter(suggestionRecycleViewAdapter);
        Log.e("ID",">>>"  + userID);

    }

    private void setUpGroupFollowingListView(){
        if(UserData.getUserId() == userID){
            groupFollowingRecycleViewAdapter = new GroupFollowingRecycleViewAdapter(getContext());
            grouplLinearLayoutManager = new LinearLayoutManager(getContext());
            grouplLinearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            groupFollowingRV.setLayoutManager(grouplLinearLayoutManager);
            groupFollowingRV.setAdapter(groupFollowingRecycleViewAdapter);
            groupFollowingRecycleViewAdapter.setOnItemClickListener(this);
        }else {
            groupFollowingRV.setVisibility(View.GONE);
        }
    }

    private void initSuggestionAPI(){

        followingSRL.setColorSchemeResources(R.color.colorPrimary);
        followingSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        followingSRL.setOnRefreshListener(this);
        suggestionRecycleViewAdapter.reset();

        followingRequest = new FollowingRequest(getContext());
        followingRequest
                .setSwipeRefreshLayout(followingSRL)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showSwipeRefreshLayout(true)
                .setPerPage(20);
    }

    private void initGroupAPI(){
        groupFollowingRequest = new GroupFollowingRequest(getContext());
        groupFollowingRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, userID)
                .setPerPage(1000);
    }

    private void refreshList(){
        groupFollowingRV.setVisibility(View.GONE);
        loadMorePB.setVisibility(View.GONE);
        endlessRecyclerViewScrollListener.reset();
        suggestionRecycleViewAdapter.reset();

        if(UserData.getUserId() == userID){
            groupFollowingRequest.first();
        }

        followingRequest
                .showSwipeRefreshLayout(true)
                .clearParameters()
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, userID)
                .addParameters(Keys.server.key.KEYWORD, searchET.getText().toString())
                .first();

    }

    public void initSearch(){
        if(searchET != null){
            searchET.setHint("Search People...");
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (timer != null) {
                        timer.cancel();
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    refreshList();
                                }
                            });
                        }
                    }, 300);
                }
            });
        }
    }

    @Override
    public void onCommandClick(UserItem userItem) {
        if(userItem.social.data.is_following.equals("yes")){
            suggestionRecycleViewAdapter.unFollowUser(userItem);
        }else{
            suggestionRecycleViewAdapter.followUser(userItem);
        }
    }

    @Override
    public void onUserClick(UserItem userItem) {
        if(isRequest){
            Intent data = new Intent();
            data.putExtra("result", new Gson().toJson(userItem));
            baseActivity.setResult(Activity.RESULT_OK, data);
            baseActivity.finish();
        }
    }

    @Override
    public void onFollowSuccessClick() {

    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(FollowingRequest.ServerResponse responseData) {
        SuggestionTransformer suggestionTransformer = responseData.getData(SuggestionTransformer.class);
        if(suggestionTransformer.status){
            if(responseData.isNext()){
                suggestionRecycleViewAdapter.addNewData(suggestionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                suggestionRecycleViewAdapter.setNewData(suggestionTransformer.data);
            }
        }

        if (suggestionRecycleViewAdapter.getData().size()>0){
            UserItem userItem = suggestionRecycleViewAdapter.getData().get(0);
            userItem.walkthrough_on_process_avatar = !UserData.getBoolean(UserData.WALKTHRU_DONE);
            suggestionRecycleViewAdapter.updateData(userItem);
        }

        isGroup = false;
        followingPB.setVisibility(View.GONE);
        loadMorePB.setVisibility(View.GONE);

        if(suggestionRecycleViewAdapter.getItemCount() == 0){
            placeHolderCON.setVisibility(View.VISIBLE);
            followingRV.setVisibility(View.GONE);
        }else {
            placeHolderCON.setVisibility(View.GONE);
            followingRV.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onResponse(GroupFollowingRequest.ServerResponse responseData) {
        GroupFollowingCollectionTransformer groupFollowingCollectionTransformer = responseData.getData(GroupFollowingCollectionTransformer.class);
        if(groupFollowingCollectionTransformer.status){
            groupFollowingRV.setVisibility(View.VISIBLE);
            GroupFollowingItem groupFollowingItem = new GroupFollowingItem();
            groupFollowingItem.title = "ALL";
            groupFollowingItem.selected = true;
            groupFollowingCollectionTransformer.data.add(0, groupFollowingItem);
            groupFollowingRecycleViewAdapter.setNewData(groupFollowingCollectionTransformer.data);
        }

    }

    @Override
    public void searchTextChange(final String keyword) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                refreshList();
            }
        });
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        if (!isGroup){
            loadMorePB.setVisibility(followingRequest.hasMorePage() ? View.VISIBLE : View.GONE);
            followingRequest.nextPage();
        }
    }

    @Override
    public void onGroupItemClick(GroupFollowingItem groupFollowingItem) {
        if (groupFollowingItem.title.equalsIgnoreCase("all")){
            followingRequest
                .showSwipeRefreshLayout(true)
                .clearParameters()
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.USER_ID, userID)
                .addParameters(Keys.server.key.KEYWORD, searchET.getText().toString())
                .first();
            isGroup = false;
        }else {
            isGroup = true;
            suggestionRecycleViewAdapter.setNewData(groupFollowingItem.followers.data);
            if (suggestionRecycleViewAdapter.getItemCount() == 0 ){
                placeHolderGroupCON.setVisibility(View.VISIBLE);
                followingRV.setVisibility(View.GONE);
            }else {
                placeHolderGroupCON.setVisibility(View.GONE);
                followingRV.setVisibility(View.VISIBLE);
            }
        }
        groupFollowingRecycleViewAdapter.setSelected(groupFollowingItem.title);
    }
}
