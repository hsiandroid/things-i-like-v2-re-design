package com.thingsilikeapp.android.fragment.addressbook;

import android.app.ProgressDialog;
import android.support.v4.app.ActivityCompat;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.AddressBookActivity;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.android.dialog.CountryDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.AddressBookItem;
import com.thingsilikeapp.data.preference.CountryData;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.address.DeleteAddressRequest;
import com.thingsilikeapp.server.request.address.EditAddressRequest;
import com.thingsilikeapp.server.transformer.SingleTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class AddressBookEditFragment extends BaseFragment implements
        View.OnClickListener,
        CountryDialog.CountryPickerListener {

    public static final String TAG = AddressBookEditFragment.class.getName();
    private AddressBookActivity wishListActivity;
    private CountryData.Country countryData;
    private CountryData.Country phoneData;

    @BindView(R.id.nameET)              EditText nameET;
    @BindView(R.id.streetAddressET)     EditText streetAddressET;
    @BindView(R.id.cityET)              EditText cityET;
    @BindView(R.id.stateET)             EditText stateET;
    @BindView(R.id.zipET)               EditText zipET;
    @BindView(R.id.addressLabelET)      EditText addressLabelET;
    @BindView(R.id.countryTXT)          TextView countryTXT;
    @BindView(R.id.contactET)           EditText contactET;
    @BindView(R.id.contactCodeTXT)      TextView contactCodeTXT;
    @BindView(R.id.contactCON)          View contactCON;
    @BindView(R.id.saveBTN)             TextView saveBTN;
    @BindView(R.id.deleteBTN)           View deleteBTN;
    @BindView(R.id.noteTXT)             TextView noteTXT;
    @BindView(R.id.countryFlagIV)       ImageView countryFlagIV;

    private final int PHONE = 1;
    private final int COUNTRY = 2;

    @State String addressRaw = "";
    @State int  id;
    public static AddressBookEditFragment newInstance(String addressRaw) {
        AddressBookEditFragment addressBookEditFragment = new AddressBookEditFragment();
        addressBookEditFragment.addressRaw = addressRaw;
        return addressBookEditFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_address_book_create;
    }

    @Override
    public void onViewReady() {
        wishListActivity = (AddressBookActivity) getActivity();
        wishListActivity.setTitle(getString(R.string.address_edit_title));
        saveBTN.setOnClickListener(this);
        deleteBTN.setOnClickListener(this);
        countryTXT.setOnClickListener(this);
        countryFlagIV.setOnClickListener(this);
        contactCodeTXT.setOnClickListener(this);
        contactCON.setOnClickListener(this);
        saveBTN.setText("Update");

        displayData();
        setupCountryTextView();
        setUpNote();

        Analytics.trackEvent("addressbook_AddressBookEditFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void setUpNote(){
        String text = getString(R.string.edit_profile_note);
        String privacy = "Privacy Policy";
        String terms = "Terms and Conditions";

        SpannableString spanString = new SpannableString(text + " " + privacy + " and " + terms);

        spanString.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(getContext(), R.color.colorPrimary)), text.length(), text.length() + privacy.length() + 1, 0);
        spanString.setSpan(new ForegroundColorSpan(ActivityCompat.getColor(getContext(), R.color.colorPrimary)), text.length() + privacy.length() + 6, text.length() + privacy.length() + 1 + terms.length() + 5, 0);

        ClickableSpan privacyClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
//                wishListActivity.openUrl("https://thingsilikeapp.com/privacy-policy");
                wishListActivity.openUrl("http://demo.thingsilikeapp.com/privacy");
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        };

        ClickableSpan termsClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
//                wishListActivity.openUrl("https://thingsilikeapp.com/terms");
                wishListActivity.openUrl("http://demo.thingsilikeapp.com/terms");
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        };

        spanString.setSpan(privacyClick, text.length(), text.length() + privacy.length() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanString.setSpan(termsClick, text.length() + privacy.length() + 6, text.length() + privacy.length() + 1 + terms.length() + 5, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        noteTXT.setText(spanString);
        noteTXT.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void setupCountryTextView(){
        countryTXT.setInputType(InputType.TYPE_NULL);
        countryTXT.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    showCountryPicker(COUNTRY);

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.saveBTN:
                attemptCreate();
                Analytics.trackEvent("addressbook_AddressBookEditFragment_saveBTN");
                break;
            case R.id.deleteBTN:
                deleteCommentDialog();
                Analytics.trackEvent("addressbook_AddressBookEditFragment_deleteBTN");
                break;
            case R.id.countryTXT:
                countryTXT.setError(null);
                showCountryPicker(COUNTRY);
                Analytics.trackEvent("addressbook_AddressBookEditFragment_countryTXT");
                break;
            case R.id.countryFlagIV:
                Analytics.trackEvent("addressbook_AddressBookEditFragment_countryFlagIV");
            case R.id.contactCON:
                Analytics.trackEvent("addressbook_AddressBookEditFragment_contactCON");
            case R.id.contactCodeTXT:
                showCountryPicker(PHONE);
                Analytics.trackEvent("addressbook_AddressBookEditFragment_contactCodeTXT");
                break;
        }
    }

    private void attemptCreate(){
        nameET.setError(null);

        streetAddressET.setError(null);
        cityET.setError(null);
        stateET.setError(null);
        zipET.setError(null);
        contactET.setError(null);
        countryTXT.setError(null);

        EditAddressRequest editAddressRequest = new EditAddressRequest(getContext());
        editAddressRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating address...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.ADDRESS_ID, id)
                .addParameters(Keys.server.key.USER_ID, UserData.getUserId())
                .addParameters(Keys.server.key.ADDRESS_LABEL, addressLabelET.getText().toString())
                .addParameters(Keys.server.key.RECEIVER_NAME, nameET.getText().toString())
                .addParameters(Keys.server.key.STREET_ADDRESS, streetAddressET.getText().toString())
                .addParameters(Keys.server.key.CITY, cityET.getText().toString())
                .addParameters(Keys.server.key.STATE, stateET.getText().toString())
                .addParameters(Keys.server.key.ZIP_CODE, zipET.getText().toString())
                .addParameters(Keys.server.key.COUNTRY, countryTXT.getText().toString())
                .addParameters(Keys.server.key.COUNTRY_CODE, countryData.code1)
                .addParameters(Keys.server.key.PHONE_NUMBER, contactCodeTXT.getText().toString() + contactET.getText().toString())
                .addParameters(Keys.server.key.PHONE_COUNTRY_DIAL_CODE, phoneData.code3)
                .addParameters(Keys.server.key.PHONE_COUNTRY_CODE, phoneData.code1)
                .execute();
    }

    private void deleteCommentDialog(){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Address Book")
                .setNote("Are you sure you want to remove this address book?")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Remove")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        attemptDelete();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        confirmationDialog.dismiss();
                    }
                })
                .build(getChildFragmentManager());
    }

    private void attemptDelete(){
        DeleteAddressRequest editAddressRequest = new DeleteAddressRequest(getContext());
        editAddressRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Deleting address...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.ADDRESS_ID, id)
                .execute();
    }

    private void displayData(){
        if(addressRaw.length() > 0){
            AddressBookItem addressBookItem = new Gson().fromJson(addressRaw, AddressBookItem.class);
            id = addressBookItem.id;

            countryData = CountryData.getCountryDataByCode1(addressBookItem.country_code);

            phoneData = CountryData.getCountryDataByCode1(addressBookItem.phone_country_code);
            phoneCode(phoneData);

            addressLabelET.setText(addressBookItem.address_label);
            nameET.setText(addressBookItem.receiver_name);
            streetAddressET.setText(addressBookItem.street_address);
            cityET.setText(addressBookItem.city);
            stateET.setText(addressBookItem.state);
            zipET.setText(addressBookItem.zip_code);
            countryTXT.setText(addressBookItem.country);
            contactET.setText(addressBookItem.phone_number);
        }
    }

    private void phoneCode(CountryData.Country country){
        contactCodeTXT.setText(country.code3);
        Glide.with(getContext())
                .load(country.getFlag())
                .into(countryFlagIV);
    }

    @Override
    public void onSelectCountry(CountryData.Country country, int requestCode) {
        switch (requestCode){
            case PHONE:
                phoneData = country;
                phoneCode(country);
                break;
            case COUNTRY:
                countryData = country;
                countryTXT.setText(country.country);
                contactET.requestFocus();
                contactET.setSelection(contactET.getText().length());
                phoneData = country;
                phoneCode(country);
                break;
        }
    }

    private void showCountryPicker(int requestCode){
        CountryDialog.newInstance(this, requestCode).show(getChildFragmentManager(), CategoryDialog.TAG);
    }

    @Subscribe
    public void onResponse(EditAddressRequest.ServerResponse responseData) {
        SingleTransformer<AddressBookItem, AddressBookItem.Errors> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            wishListActivity.onBackPressed();
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
            if(singleTransformer.hasRequirements()){
                ErrorResponseManger.first(addressLabelET, singleTransformer.errors.address_label);
                ErrorResponseManger.first(nameET, singleTransformer.errors.receiver_name);
                ErrorResponseManger.first(contactET, singleTransformer.errors.phone_number);
                ErrorResponseManger.first(zipET, singleTransformer.errors.zip_code);
                ErrorResponseManger.first(stateET, singleTransformer.errors.state);
                ErrorResponseManger.first(cityET, singleTransformer.errors.city);
                ErrorResponseManger.first(streetAddressET, singleTransformer.errors.street_address);
            }
        }
    }

    @Subscribe
    public void onResponse(DeleteAddressRequest.ServerResponse responseData) {
        SingleTransformer<AddressBookItem, AddressBookItem.Errors> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            wishListActivity.onBackPressed();
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }
}
