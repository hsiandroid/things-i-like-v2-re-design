package com.thingsilikeapp.android.fragment.profilebak;

import android.app.ProgressDialog;
import android.view.View;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.thingsilikeapp.R;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListTransactionItem;
import com.thingsilikeapp.server.request.wishlist.UserSentWishListRequest;
import com.thingsilikeapp.server.request.wishlist.WishListSendPermissionRequest;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListTransactionTransformer;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.vendor.android.java.CustomLinearLayoutManager;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.android.adapter.GiftRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.SendGiftDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class IGaveFragmentBak extends BaseFragment implements
        GiftRecycleViewAdapter.ClickListener,
        EndlessRecyclerViewScrollListener.Callback,
        SendGiftDialog.Callback{

    public static final String TAG = IGaveFragmentBak.class.getName();

    private GiftRecycleViewAdapter giftRecycleViewAdapter;
    private UserSentWishListRequest userSentWishListRequest;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private CustomLinearLayoutManager linearLayoutManager;

    @BindView(R.id.observableRecyclerView)      ObservableRecyclerView observableRecyclerView;
    @BindView(R.id.thingsPB)                    View thingsPB;
    @BindView(R.id.placeHolderCON)              View placeHolderCON;

    @State int userID;

    public static IGaveFragmentBak newInstance(int userID) {
        IGaveFragmentBak iGaveFragment = new IGaveFragmentBak();
        iGaveFragment.userID = userID;
        return iGaveFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_i_gave;
    }

    @Override
    public void onViewReady() {
        setupListView();
        initFeedAPI();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void setupListView(){
        giftRecycleViewAdapter = new GiftRecycleViewAdapter(getContext(), GiftRecycleViewAdapter.DisplayType.GAVE);
        linearLayoutManager = new CustomLinearLayoutManager(getContext());
        linearLayoutManager.setScrollEnabled(false);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        observableRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        observableRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(20));
        observableRecyclerView.setLayoutManager(linearLayoutManager);
        observableRecyclerView.setHasFixedSize(false);
        observableRecyclerView.setAdapter(giftRecycleViewAdapter);
        giftRecycleViewAdapter.setOnItemClickListener(this);
    }

    public void setScrollable(boolean scrollable){
        if(linearLayoutManager != null){
            linearLayoutManager.setScrollEnabled(scrollable);
        }
    }

    @Override
    public void onItemClick(int position, View v) {
        WishListTransactionItem wishListTransactionItem = (WishListTransactionItem) giftRecycleViewAdapter.getItemData(position);
        ((RouteActivity) getContext()).startItemActivity(wishListTransactionItem.id, wishListTransactionItem.owner.data.id, "i_gave");
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        ((RouteActivity)getActivity()).startProfileActivity(userItem.id);
    }

    @Override
    public void onStatusClick(String status, int wishlistID, int userID) {
        SendGiftDialog.newInstance("Dedication Message", wishlistID, this).show(getChildFragmentManager(), SendGiftDialog.TAG);
    }

    private void initFeedAPI(){
        userSentWishListRequest = new UserSentWishListRequest(getContext());
        userSentWishListRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.info,owner.social,owner.statistics,sender.info,sender.social,sender.statistics,delivery_info")
                .addParameters(Keys.server.key.USER_ID, userID)
                .addParameters(Keys.server.key.VIEW_ALL, "yes")
                .setPerPage(10);
    }

    public void refreshList(){
        System.gc();
        if(thingsPB != null){
            thingsPB.setVisibility(View.VISIBLE);
        }
        if (userSentWishListRequest != null) {
            userSentWishListRequest.first();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(UserSentWishListRequest.ServerResponse responseData) {
        WishListTransactionTransformer wishListTransactionTransformer = responseData.getData(WishListTransactionTransformer.class);
        if(wishListTransactionTransformer.status){
            if(responseData.isNext()){
                giftRecycleViewAdapter.addNewData(wishListTransactionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                giftRecycleViewAdapter.setNewData(wishListTransactionTransformer.data);
            }
        }
        if (giftRecycleViewAdapter.getItemCount() == 0){
            placeHolderCON.setVisibility(View.VISIBLE);
        }else {
            placeHolderCON.setVisibility(View.GONE);
        }
        thingsPB.setVisibility(View.GONE);
    }

    public void scrollUp(){
        if(observableRecyclerView != null){
            observableRecyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        thingsPB.setVisibility(userSentWishListRequest.hasMorePage() ? View.VISIBLE : View.GONE);
        userSentWishListRequest.nextPage();
    }

    private void attemptSendRequest(int wishListID, String dedication) {
        new WishListSendPermissionRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Sending Gift...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .addParameters(Keys.server.key.DEDICATION_MESSAGE, dedication)
                .execute();
    }

    @Subscribe
    public void onResponse(WishListSendPermissionRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            giftRecycleViewAdapter.updateStatus(wishListInfoTransformer.wishListItem.id, "pending");
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onAccept(int wishlistID, String dedication) {
        attemptSendRequest(wishlistID, dedication);
    }
}