package com.thingsilikeapp.android.fragment.account;

import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.AccountActivity;
import com.thingsilikeapp.android.adapter.FragmentViewPagerAdapter;
import com.thingsilikeapp.android.fragment.account.users.AccountContactsFragment;
import com.thingsilikeapp.android.fragment.account.users.AccountSuggestionFragment;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class AccountSearchUserFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = AccountSearchUserFragment.class.getName();

    private AccountActivity accountActivity;

    private FragmentViewPagerAdapter fragmentViewPagerAdapter;

    @BindView(R.id.suggestedUserBTN)                TextView suggestedUserBTN;
    @BindView(R.id.fbFriendsBTN)                    TextView fbFriendsBTN;
    @BindView(R.id.contactsBTN)                     TextView contactsBTN;
    @BindView(R.id.searchVP)                        ViewPager searchVP;

    public static AccountSearchUserFragment newInstance() {
        AccountSearchUserFragment suggestionFragment = new AccountSearchUserFragment();
        return suggestionFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_account_search_user;
    }

    @Override
    public void onViewReady() {
        accountActivity = (AccountActivity) getActivity();
        accountActivity.setTitle("PEOPLE");
        accountActivity.showSearchContainer(false);

        suggestedUserBTN.setOnClickListener(this);
        fbFriendsBTN.setOnClickListener(this);
        contactsBTN.setOnClickListener(this);

        setupViewPager();
        Analytics.trackEvent("account_AccountSearchUserFragment_onViewReady");
    }

    private void setupViewPager(){
        fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getChildFragmentManager());
        fragmentViewPagerAdapter.addFragment(AccountSuggestionFragment.newInstance());
//        fragmentViewPagerAdapter.addFragment(FacebookFriendsFragment.newInstance());
        fragmentViewPagerAdapter.addFragment(AccountContactsFragment.newInstance());
        searchVP.setPageMargin(20);
        searchVP.setPageMarginDrawable(null);
        searchVP.setAdapter(fragmentViewPagerAdapter);
        searchVP.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        suggestedUserBTN.setSelected(true);
                        fbFriendsBTN.setSelected(false);
                        contactsBTN.setSelected(false);
                        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
                        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        break;
//                    case 1:
//                        suggestedUserBTN.setSelected(false);
//                        fbFriendsBTN.setSelected(true);
//                        contactsBTN.setSelected(false);
//                        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
//                        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
//                        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
//                        break;
                    case 1:
                        suggestedUserBTN.setSelected(false);
                        fbFriendsBTN.setSelected(false);
                        contactsBTN.setSelected(true);
                        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
                        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        searchVP.setOffscreenPageLimit(2);
        suggestedUserActive();

    }

    public void suggestedUserActive(){
        suggestedUserBTN.setSelected(true);
        fbFriendsBTN.setSelected(false);
        contactsBTN.setSelected(false);
        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        searchVP.post(new Runnable() {
            @Override
            public void run() {
                if(searchVP != null){
                    searchVP.setCurrentItem(0);
                }
            }
        });
    }

//    public void fbFriendsActive(){
//        suggestedUserBTN.setSelected(false);
//        fbFriendsBTN.setSelected(true);
//        contactsBTN.setSelected(false);
//        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
//        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
//        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
//        searchVP.post(new Runnable() {
//            @Override
//            public void run() {
//                if(searchVP != null){
//                    searchVP.setCurrentItem(1);
//                }
//            }
//        });
//    }

    public void contactsActive(){
        suggestedUserBTN.setSelected(false);
        fbFriendsBTN.setSelected(false);
        contactsBTN.setSelected(true);
        suggestedUserBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        fbFriendsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        contactsBTN.setTextColor(ActivityCompat.getColor(getContext(), R.color.white));
        searchVP.post(new Runnable() {
            @Override
            public void run() {
                if(searchVP != null){
                    searchVP.setCurrentItem(1);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.suggestedUserBTN:
                suggestedUserActive();
                Analytics.trackEvent("account_AccountSearchUserFragment_suggestedUserBTN");
                break;
            case R.id.fbFriendsBTN:
//                fbFriendsActive();
                Analytics.trackEvent("account_AccountSearchUserFragment_fbFriendsBTN");
                break;
            case R.id.contactsBTN:
                contactsActive();
                Analytics.trackEvent("account_AccountSearchUserFragment_contactsBTN");
                break;
        }
    }
}
