package com.thingsilikeapp.android.fragment.search;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.SearchActivity;
import com.thingsilikeapp.android.adapter.SuggestionRecycleViewAdapter;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.social.RePostRequest;
import com.thingsilikeapp.server.transformer.social.SuggestionTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.thingsilikeapp.vendor.android.java.decoration.DividerItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class RepostFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        SearchActivity.SearchListener,
        EndlessRecyclerViewScrollListener.Callback,
        SuggestionRecycleViewAdapter.ClickListener{

    public static final String TAG = RepostFragment.class.getName();
    private SearchActivity searchActivity;
    private RePostRequest rePostRequest;

    private SuggestionRecycleViewAdapter rePostRecycleViewAdapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;

    @State int wishListID;

    @BindView(R.id.rePostRV)                RecyclerView rePostRV;
    @BindView(R.id.rePostSRL)               SwipeRefreshLayout rePostSRL;
    @BindView(R.id.placeHolderCON)          View placeHolderCON;
    @BindView(R.id.thingsPB)                View thingsPB;

    public static RepostFragment newInstance(int wishListID) {
        RepostFragment repostFragment = new RepostFragment();
        repostFragment.wishListID = wishListID;
        return repostFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_repost;
    }

    @Override
    public void onViewReady() {
        searchActivity = (SearchActivity) getActivity();
        searchActivity.showSearchContainer(true);
        Log.e("Wishlist ID",">>>" + wishListID);
        setupRePostListView();
        initSuggestionAPI();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void setupRePostListView() {

        rePostRecycleViewAdapter = new SuggestionRecycleViewAdapter(getContext());
        rePostRecycleViewAdapter.setOnItemClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);

        rePostRV.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST).setDividerHeight(10));
        rePostRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        rePostRV.setLayoutManager(linearLayoutManager);
        rePostRV.setAdapter(rePostRecycleViewAdapter);
    }

    private void initSuggestionAPI(){

        rePostSRL.setColorSchemeResources(R.color.colorPrimary);
        rePostSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        rePostSRL.setOnRefreshListener(this);
        rePostRecycleViewAdapter.reset();

        rePostRequest = new RePostRequest(getContext());
        rePostRequest
                .setSwipeRefreshLayout(rePostSRL)
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListID)
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(20);
    }

    private void refreshList(){
        thingsPB.setVisibility(View.GONE);
        endlessRecyclerViewScrollListener.reset();
        rePostRecycleViewAdapter.reset();
        rePostRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onCommandClick(UserItem userItem) {
        if(userItem.social.data.is_following.equals("yes")){
            rePostRecycleViewAdapter.unFollowUser(userItem);
        }else{
            rePostRecycleViewAdapter.followUser(userItem);
        }
    }

    @Override
    public void onUserClick(UserItem userItem) {

    }

    @Override
    public void onFollowSuccessClick() {

    }

    @Override
    public void onResume() {
        super.onResume();
        searchActivity.setSearchListener(this);
        refreshList();
    }

    @Override
    public void onPause() {
        searchActivity.setSearchListener(null);
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(RePostRequest.ServerResponse responseData) {
        SuggestionTransformer suggestionTransformer = responseData.getData(SuggestionTransformer.class);
        if(suggestionTransformer.status){

            if(responseData.isNext()){
                rePostRecycleViewAdapter.addNewData(suggestionTransformer.data);
            }else{
                endlessRecyclerViewScrollListener.reset();
                rePostRecycleViewAdapter.setNewData(suggestionTransformer.data);
            }

            thingsPB.setVisibility(View.GONE);
            if(rePostRecycleViewAdapter.getItemCount() == 0){
                placeHolderCON.setVisibility(View.VISIBLE);
                rePostRV.setVisibility(View.GONE);
            }else {
                placeHolderCON.setVisibility(View.GONE);
                rePostRV.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void searchTextChange(final String keyword) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                refreshList();
            }
        });
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        thingsPB.setVisibility(rePostRequest.hasMorePage() ? View.VISIBLE : View.GONE);
        rePostRequest.nextPage();
    }
}
