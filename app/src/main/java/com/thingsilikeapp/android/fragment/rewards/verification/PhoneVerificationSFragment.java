package com.thingsilikeapp.android.fragment.rewards.verification;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.VerificationActivity;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.android.dialog.CountryDialog;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.preference.CountryData;
import com.thingsilikeapp.server.KYC;
import com.thingsilikeapp.server.request.GetCountryRequest;
import com.thingsilikeapp.server.transformer.GetCountryTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class PhoneVerificationSFragment extends BaseFragment  implements View.OnClickListener, CountryDialog.CountryPickerListener {

    public static final String TAG = PhoneVerificationSFragment.class.getName();

    @BindView(R.id.countryFlagIV)   ImageView countryFlagIV;
    @BindView(R.id.contactCodeTXT)  TextView contactCodeTXT;
    @BindView(R.id.contactET)       EditText contactET;
    @BindView(R.id.contactCON)      View contactCON;
    @BindView(R.id.submitBTN)       View submitBTN;

    @State String country;
    @State String contry_code;

    private CountryData.Country countryData;


    private VerificationActivity verificationActivity;

    public static PhoneVerificationSFragment newInstance() {
        PhoneVerificationSFragment fragment = new PhoneVerificationSFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_verification_phone_s;
    }

    @Override
    public void onViewReady() {
        verificationActivity = (VerificationActivity) getContext();
        verificationActivity.setTitle(" Verification Phone number");
        contactCON.setOnClickListener(this);
        getCountry();
        submitBTN.setOnClickListener(this);
        verificationActivity.setBackBTN(true);
        Analytics.trackEvent("rewards_verification_PhoneVerificationFragment_onViewReady");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(GetCountryRequest.ServerResponse responseData) {
        GetCountryTransformer getCountryTransformer = responseData.getData(GetCountryTransformer.class);
        if(getCountryTransformer.status){
            countryData = CountryData.getCountryDataByCode1(getCountryTransformer.data.country);
            contactCodeTXT.setText(countryData.code3);
            countryCode(countryData);
        }else{
            ToastMessage.show(getContext(), getCountryTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(KYC.PhoneResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            verificationActivity.openOTPSFragment(contry_code + contactET.getText().toString(), contry_code, country);
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    private void getCountry(){
        GetCountryRequest createCommentRequest = new GetCountryRequest(getContext());
        createCommentRequest
                .showNoInternetConnection(false)
                .execute();
    }

    private void countryCode(CountryData.Country country){
        contactCodeTXT.setText(country.code3);
        this.country = countryData.code1;
        this.contry_code = countryData.code3;
        Glide.with(getContext())
                .load(country.getFlag())
                .into(countryFlagIV);
    }

    private void showCountryPicker(){
        CountryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.contactCON:
                showCountryPicker();
                Analytics.trackEvent("rewards_verification_PhoneVerificationFragment_contactCON");
                break;
            case R.id.submitBTN:
                KYC.getDefault().phone(getContext(), contry_code + contactET.getText().toString(), contry_code, country);
                Analytics.trackEvent("rewards_verification_PhoneVerificationFragment_submitBTN");
                break;
        }
    }

@Override
public void onSelectCountry(CountryData.Country country, int requestCode) {
        countryData = country;
        countryCode(countryData);
        }
        }
