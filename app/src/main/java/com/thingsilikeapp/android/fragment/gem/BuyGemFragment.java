package com.thingsilikeapp.android.fragment.gem;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.GemActivity;
import com.thingsilikeapp.android.adapter.GemTransactionHistoryRecycleViewAdapter;
import com.thingsilikeapp.data.model.GemTransactionHistoryItem;
import com.thingsilikeapp.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class BuyGemFragment extends BaseFragment {

    public static final String TAG = BuyGemFragment.class.getName();

    private GemActivity gemActivity;

    private GemTransactionHistoryRecycleViewAdapter gemTransactionHistoryRecycleViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.transactionHistoryRV)            RecyclerView transactionHistoryRV;


    public static BuyGemFragment newInstance() {
        BuyGemFragment fragment = new BuyGemFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_buy_gem;
    }

    @Override
    public void onViewReady() {
        gemActivity = (GemActivity) getContext();
        gemActivity.setTitle("Buy Lyka Gems");

        setUpTransactionHistoryListView();
        Analytics.trackEvent("gem_BuyGemFragment_onViewReady");
    }


    private void setUpTransactionHistoryListView(){
        linearLayoutManager = new LinearLayoutManager(getContext());
        gemTransactionHistoryRecycleViewAdapter = new GemTransactionHistoryRecycleViewAdapter(getContext());
        transactionHistoryRV.setLayoutManager(linearLayoutManager);
        transactionHistoryRV.setAdapter(gemTransactionHistoryRecycleViewAdapter);

        gemTransactionHistoryRecycleViewAdapter.setNewData(getGemTransactionHistoryData());
    }

    private List<GemTransactionHistoryItem> getGemTransactionHistoryData(){
        List<GemTransactionHistoryItem> gemTransactionHistoryItems = new ArrayList<>();

        GemTransactionHistoryItem gemTransactionHistoryItem = new GemTransactionHistoryItem();
        gemTransactionHistoryItem.id = 1;
        gemTransactionHistoryItem.title = "Account Top-Up";
        gemTransactionHistoryItem.date = "5/29/2018";
        gemTransactionHistoryItem.amount = "Php 1000";
        gemTransactionHistoryItem.reference_number = "00123456789";
        gemTransactionHistoryItems.add(gemTransactionHistoryItem);

         gemTransactionHistoryItem = new GemTransactionHistoryItem();
        gemTransactionHistoryItem.id = 2;
        gemTransactionHistoryItem.title = "Change Gem Currency";
        gemTransactionHistoryItem.date = "5/29/2018";
        gemTransactionHistoryItem.currency_from = "USD";
        gemTransactionHistoryItem.currency_to = "PHP";
        gemTransactionHistoryItem.reference_number = "00123456789";
        gemTransactionHistoryItems.add(gemTransactionHistoryItem);


        return gemTransactionHistoryItems;

    }
}
