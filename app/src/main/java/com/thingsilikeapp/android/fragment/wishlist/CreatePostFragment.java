package com.thingsilikeapp.android.fragment.wishlist;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.WishListActivity;
import com.thingsilikeapp.android.adapter.RelatedTempRecycleViewAdapter;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.android.dialog.GroupDialog;
import com.thingsilikeapp.android.dialog.ImagePickerV2Dialog;
import com.thingsilikeapp.android.dialog.UnfollowDialog;
import com.thingsilikeapp.android.dialog.defaultdialog.ConfirmationDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.model.CategoryItem;
import com.thingsilikeapp.data.model.EventItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.data.model.WishListItem;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.helper.LinkPreviewRequest;
import com.thingsilikeapp.server.request.like.LikeRequest;
import com.thingsilikeapp.server.request.report.ReportRequest;
import com.thingsilikeapp.server.request.social.FollowRequest;
import com.thingsilikeapp.server.request.social.UnFollowRequest;
import com.thingsilikeapp.server.request.wishlist.BlockRequest;
import com.thingsilikeapp.server.request.wishlist.CreateWishListRequest;
import com.thingsilikeapp.server.request.wishlist.WishListRecommendedRequest;
import com.thingsilikeapp.server.transformer.helper.LinkTransformer;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListInfoTransformer;
import com.thingsilikeapp.server.transformer.wishlist.WishListRelatedTransformer;
import com.thingsilikeapp.vendor.android.base.BaseActivity;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.base.RouteActivity;
import com.thingsilikeapp.vendor.android.java.ImageQualityManager;
import com.thingsilikeapp.vendor.android.java.Keyboard;
import com.thingsilikeapp.vendor.android.java.PermissionChecker;
import com.thingsilikeapp.vendor.android.java.SnackBar;
import com.thingsilikeapp.vendor.android.java.StringFormatter;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.WalkThrough;
import com.thingsilikeapp.vendor.android.java.decoration.SpacesItemDecoration;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.base.BaseRequest;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import icepick.State;
import uk.co.deanwild.materialshowcaseview.IShowcaseListener;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class CreatePostFragment extends BaseFragment implements View.OnClickListener,
        CategoryDialog.Callback,
        ImagePickerV2Dialog.ImageCallback,
        WishListActivity.onBackPressedListener,
        RelatedTempRecycleViewAdapter.ClickListener,
        GroupDialog.Callback{

    public static final String TAG = CreatePostFragment.class.getName();
    private WishListActivity wishListActivity;
    private static final int PERMISSION_GALLERY = 102;
    private static final int WRITE_EXTERNAL_STORAGE = 102;

    private Timer timer;

    private Handler handler;
    private MaterialShowcaseView.Builder materialShowcaseView;

    private RelatedTempRecycleViewAdapter socialFeedRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private WishListRecommendedRequest wishListRelatedRequest;

    @BindView(R.id.postBTN)                 View postBTN;
    @BindView(R.id.descriptionET)           EditText descriptionET;
    @BindView(R.id.othersET)                EditText othersET;
    @BindView(R.id.othersTIL)               View othersTIL;
    @BindView(R.id.categoryTXT)             TextView categoryTXT;

    @BindView(R.id.metaImageCON)            View metaImageCON;
    @BindView(R.id.metaImageImgIV)          ImageView metaImageImgIV;
    @BindView(R.id.metaImageCloseBTN)       View metaImageCloseBTN;

    @BindView(R.id.metaCON)                 View metaCON;
    @BindView(R.id.metaPB)                  View metaPB;
    @BindView(R.id.metaCloseBTN)            View metaCloseBTN;
    @BindView(R.id.metaDataCON)             View metaDataCON;

    @BindView(R.id.metaTitleTXT)            TextView metaTitleTXT;
    @BindView(R.id.metaDescriptionTXT)      TextView metaDescriptionTXT;
    @BindView(R.id.counterTXT)              TextView counterTXT;
    @BindView(R.id.metaImageIV)             ImageView metaImageIV;
    @BindView(R.id.categoryIV)              ImageView categoryIV;
    @BindView(R.id.cameraBTN)               View cameraBTN;
    @BindView(R.id.counterCON)              View counterCON;
    @BindView(R.id.prevBTN)                 View prevBTN;
    @BindView(R.id.nextBTN)                 View nextBTN;
    @BindView(R.id.cameraLBL)               View cameraLBL;
    @BindView(R.id.imageCloseBTN)           View imageCloseBTN;
    @BindView(R.id.cameraCon)               View cameraCon;
    @BindView(R.id.imageLBL)                View imageLBL;
    @BindView(R.id.imageIV)                 ImageView imageIV;
    @BindView(R.id.loadingIV)               View loadingIV;
    @BindView(R.id.recommmendedRV)          RecyclerView recommmendedRV;
    @BindView(R.id.recommendedCON)          View recommendedCON;
    @BindView(R.id.relatedCON)              View relatedCON;
    @BindView(R.id.recommendedPB)           View recommendedPB;
    @BindView(R.id.categoryBTN)             View categoryBTN;
    @BindView(R.id.createPostSV)            ScrollView createPostSV;

    @State String currentSelectedURL;
    @State boolean isSearching;
    @State File file;

    @State String content = "";
    @State String url = "";
    @State String urlTitle = "";
    @State String urlDescription = "-";
    @State String urlImage = "/";
    @State int thumbnailIndex = 0;
    private long doneButtonClickTime = 0;

    private List<String> thumbnailImages;

    public static CreatePostFragment newInstance() {
        CreatePostFragment createPostFragment = new CreatePostFragment();
        return createPostFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_create_post;
    }

    @Override
    public void onViewReady() {
        wishListActivity = (WishListActivity) getContext();
        wishListActivity.setTitle(getString(R.string.title_main_add));
        wishListActivity.setOnBackPressedListener(this);
        postBTN.setOnClickListener(this);
        categoryTXT.setOnClickListener(this);
        categoryIV.setOnClickListener(this);
        metaCloseBTN.setOnClickListener(this);
        prevBTN.setOnClickListener(this);
        nextBTN.setOnClickListener(this);
        metaImageCloseBTN.setOnClickListener(this);
        cameraBTN.setOnClickListener(this);
        cameraLBL.setOnClickListener(this);
        imageCloseBTN.setOnClickListener(this);
        categoryBTN.setOnClickListener(this);

        thumbnailImages = new ArrayList<>();

        categoryTXT.setText("I Just Like It");

        setUpUrlSearch();
        strictPolicy();
        openSavedInfo();
        openSharePost();
        openShareImage();

        setupRecommendedItemView();
        initFeedAPI();

//        MaterialShowcaseView.resetSingleUse(getContext(), WalkThrough.CAMERA);
//        MaterialShowcaseView.resetSingleUse(getContext(), WalkThrough.DESCRIPTION);
//        MaterialShowcaseView.resetSingleUse(getContext(), WalkThrough.CATEGORY);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            createPostSV.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                    Keyboard.hideKeyboard(getActivity());
                }
            });
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void onDescriptionTextChange(){
        if(!UserData.getBoolean(UserData.WALKTHRU_DONE)){
//            descriptionET.setOnKeyListener(new View.OnKeyListener() {
//                public boolean onKey(View v, int keyCode, KeyEvent event) {
//                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
//                        switch (keyCode) {
//                            case KeyEvent.KEYCODE_DPAD_CENTER:
//                            case KeyEvent.KEYCODE_ENTER:
//                                Keyboard.hideKeyboard(getContext());
//                                showAddPhotoWalkThru();
//                                return true;
//                            default:
//                                break;
//                        }
//                    }
//                    return false;
//                }
//            });
            descriptionET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    Handler handler  = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Keyboard.hideKeyboard(getContext());
//                            showAddPhotoWalkThru();
                        }
                    },1500);
                }
            });
        }
    }

    private void showDescriptionWalkThru(){
        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)){

            descriptionET.setImeOptions(EditorInfo.IME_ACTION_DONE);
            descriptionET.setInputType(InputType.TYPE_CLASS_TEXT);
            new MaterialShowcaseView.Builder(getActivity())
                    .setTarget(descriptionET)
                    .setMaskColour(ActivityCompat.getColor(getContext(), R.color.walkthrough_bg))
                    .setTargetTouchable(true)
                    .setShapePadding(-10)
                    .withRectangleShape()
                    .singleUse(WalkThrough.DESCRIPTION)
                    .setContentTextColor(ActivityCompat.getColor(getContext(), R.color.white_dim))
                    .setContentText("Enter a description or URL of things you like.")
                    .show();
        }
    }

    private void showAddPhotoWalkThru(){

        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)) {
            new MaterialShowcaseView.Builder(getActivity())
                    .setTarget(cameraBTN)
                    .setMaskColour(ActivityCompat.getColor(getContext(), R.color.walkthrough_bg))
                    .setTargetTouchable(true)
                    .setDismissOnTargetTouch(true)
                    .setShapePadding(5)
                    .singleUse(WalkThrough.CAMERA)
                    .setListener(new IShowcaseListener() {
                        @Override
                        public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {

                        }

                        @Override
                        public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
                            materialShowcaseView.removeAllViews();

                        }
                    })
                    .setContentTextColor(ActivityCompat.getColor(getContext(), R.color.white_dim))
                    .setContentText("Click to upload a photo. \n\n *Note: If the link you posted has a photo, the image will be readily shown.")
                    .show();
        }
    }

    private void showCategoryWalkThru(){

        if (!UserData.getBoolean(UserData.WALKTHRU_DONE)) {
            materialShowcaseView = new MaterialShowcaseView.Builder(getActivity());
            materialShowcaseView
                    .setTarget(categoryIV)
                    .setMaskColour(ActivityCompat.getColor(getContext(), R.color.walkthrough_bg))
                    .setTargetTouchable(true)
                    .setDismissOnTargetTouch(true)
                    .setShapePadding(5)
                    .singleUse(WalkThrough.CATEGORY)
                    .setListener(new IShowcaseListener() {
                        @Override
                        public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {

                        }

                        @Override
                        public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
                            materialShowcaseView.removeAllViews();

                        }
                    })
                    .setContentTextColor(ActivityCompat.getColor(getContext(), R.color.white_dim))
                    .setContentText("Tap the icon to select a \"Category\" from the list.")
                    .show();
        }
    }

    private void showCreatePostWalkThru(){

            if (!UserData.getBoolean(UserData.WALKTHRU_DONE)) {
                materialShowcaseView = new MaterialShowcaseView.Builder(getActivity());
                materialShowcaseView
                        .setTarget(postBTN)
                        .setMaskColour(ActivityCompat.getColor(getContext(), R.color.walkthrough_bg))
                        .setTargetTouchable(true)
                        .setDismissOnTargetTouch(true)
                        .withRectangleShape()
                        .setShapePadding(5)
                        .setListener(new IShowcaseListener() {
                            @Override
                            public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {

                            }

                            @Override
                            public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
                                materialShowcaseView.removeAllViews();

                            }
                        })
                        .setContentTextColor(ActivityCompat.getColor(getContext(), R.color.white_dim))
                        .setContentText("Tap to publish a post.")
                        .show();
            }
        }


    private void refreshList(){
        loadingIV.setVisibility(View.VISIBLE);
        recommendedPB.setVisibility(View.VISIBLE);

        wishListRelatedRequest
                .execute();
    }

    private void setupRecommendedItemView() {
        socialFeedRecyclerViewAdapter = new RelatedTempRecycleViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recommmendedRV.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.social_feed_spacing)));
        recommmendedRV.setLayoutManager(linearLayoutManager);

        recommmendedRV.setAdapter(socialFeedRecyclerViewAdapter);

        if(UserData.getBoolean(UserData.WALKTHRU_DONE)){
            socialFeedRecyclerViewAdapter.setOnItemClickListener(this);
        }else {
            socialFeedRecyclerViewAdapter.setOnItemClickListener(null);
        }
//        ViewCompat.setNestedScrollingEnabled(relatedRV, true);
    }

    private void initFeedAPI() {
        wishListRelatedRequest = new WishListRecommendedRequest(getContext());
        wishListRelatedRequest
                .clearParameters()
                .setDeviceRegID(((RouteActivity) getContext()).getDeviceRegID())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                .setPerPage(15)
                .execute();
    }

    private void openShareImage(){

        if(wishListActivity.getActivityBundle() != null){
            Bundle bundle = wishListActivity.getActivityBundle();
            if(bundle.containsKey("image_uri")){
                ImagePickerV2Dialog.newInstance(getString(R.string.post_hint_add_photo), false,  (Uri) bundle.getParcelable("image_uri"), this).show(getChildFragmentManager(), ImagePickerV2Dialog.TAG);
            }
            if(bundle.containsKey("post_content")){
                descriptionET.setText(bundle.getString("post_content"));
            }
        }
    }

    private void strictPolicy(){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    private void openSharePost(){
        if(!StringFormatter.isEmpty(url)){
            currentSelectedURL = url;
            if (!urlImage.equals("")){
                descriptionET.setText(url);
            }else {
                descriptionET.setText(content);
            }
//            descriptionET.setText("repost:\n\n- " + url);
//            descriptionET.setSelection("repost:".length());
            metaPB.setVisibility(View.GONE);
            postBTN.setEnabled(true);
            metaCON.setVisibility(View.VISIBLE);
            metaDataCON.setVisibility(View.VISIBLE);
            thumbnailIndex = 0;
            thumbnailImages = new ArrayList<>();
            thumbnailImages.add(urlImage);
            if(thumbnailImages.size() > 0){
                displayCounter();
            }
            metaTitleTXT.setText(urlTitle);
            metaDescriptionTXT.setText(urlDescription);
        }else{
            url= "";
//            downloadImageWithPermision(urlImage);
        }
    }

    private void openSavedInfo(){
        String content = UserData.getString(UserData.POST_MESSAGE);
        String category = UserData.getString(UserData.CATEGORY);

        if(content.length() > 0){
            descriptionET.setText(content);
            descriptionET.setSelection(descriptionET.getText().length());
        }

        if(category.length() > 0){
            categoryTXT.setText(category);
        }

    }

    private void downloadImage(String image){

//        Log.e("URLImage", ">>>" + image);
//        final ProgressDialog progressDialog = ProgressDialog.show(getContext(), "", "Please wait", true, false);
//
//        Glide.with(getContext())
//                .asBitmap()
//                .load(image)
//                .into(new SimpleTarget<Bitmap>() {
//                    @Override
//                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//
//                    }
//
//                    @Override
//                    public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {
//                        ImageQualityManager.getFileFromBitmap(getContext(), bitmap, new ImageQualityManager.Callback() {
//                            @Override
//                            public void success(File file) {
//                                if(file != null){
////                                    CreatePostFragment.this.file = file;
//                                    cameraCon.setVisibility(View.VISIBLE);
//                                    postBTN.setEnabled(true);
//                                    Glide.with(getContext())
//                                            .load(file)
//                                            .into(imageIV);
//
////                                    Glide.with(getContext())
////                                            .load(CreatePostFragment.this.file)
////                                            .into(imageIV);
//
////                                    descriptionET.setText("repost:\n\n");
////                                    descriptionET.setSelection("repost:".length());
//                                    if (!urlImage.equals("")){
//                                        descriptionET.setText(url);
//                                    }else {
//                                        descriptionET.setText(content);
//                                    }
//                                    progressDialog.cancel();
//                                }
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                        progressDialog.cancel();
//                        super.onLoadFailed(e, errorDrawable);
//                    }
//                });
//    }
//
//    private void downloadImageWithPermision(String image){
//        if(PermissionChecker.checkPermissions(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_GALLERY)){
//            downloadImage(image);
//
//            MaterialShowcaseView.resetAll(getContext());
////            showDescriptionWalkThru();
////            onDescriptionTextChange();
//        }else {
//            wishListActivity.onBackPressed();
//        }
    }

    public void setUpUrlSearch(){
        descriptionET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("Url Description", ">>>" + descriptionET.getText().toString());
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if(getActivity() != null){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(descriptionET != null){
                                        if(currentSelectedURL !=null && !descriptionET.getText().toString().contains(currentSelectedURL)){
                                            closeMeta();
                                            closeImageMeta();
                                            postBTN.setVisibility(View.VISIBLE);
                                        }
                                        searchURL(descriptionET.getText().toString());
                                    }
                                }
                            });
                        }
                    }
                }, 600);
            }
        });
    }

    public void searchURL(String s){
        String [] parts = s.split("\\s+");
        for( String item : parts ){
            if(Patterns.WEB_URL.matcher(item).matches()){

                if(!item.equals(currentSelectedURL)){
                    currentSelectedURL = item;
                    attemptGetMeta(item);
                    postBTN.setEnabled(false);
                }
                break;
            }
        }
    }

    public void closeMeta(){
        metaCON.setVisibility(View.GONE);
        metaPB.setVisibility(View.GONE);
        postBTN.setEnabled(true);
        currentSelectedURL = "";
        url = "";
        urlTitle = "";
        urlDescription = "-";
        urlImage = "/";
        thumbnailIndex = 0;
    }

    public void closeImageMeta(){
        metaImageCON.setVisibility(View.GONE);
        postBTN.setEnabled(true);
        urlImage = "/";
    }

    @Override
    public void onClick(View v) {

        if (SystemClock.elapsedRealtime() - doneButtonClickTime < 300){
            return;
        }
        doneButtonClickTime = SystemClock.elapsedRealtime();

        switch (v.getId()){
            case R.id.postBTN:
                attemptCreate();
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.CREATE_POST);
                break;
            case R.id.categoryTXT:
            case R.id.categoryIV:
            case R.id.categoryBTN:
                categoryTXT.setError(null);
                CategoryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.CATEGORY);
                break;
            case R.id.metaCloseBTN:
                closeMeta();
                break;
            case R.id.metaImageCloseBTN:
                closeImageMeta();
                break;
            case R.id.cameraBTN:
            case R.id.cameraLBL:
                imageLBL.setVisibility(View.GONE);
                ImagePickerV2Dialog.newInstance(getString(R.string.post_hint_add_photo), false, this).show(getChildFragmentManager(), ImagePickerV2Dialog.TAG);
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.PICK_IMAGE);
                break;
            case R.id.imageCloseBTN:
                closeImage();
                ((RouteActivity) getContext()).facebookLogger(FacebookCustomLoggerMessage.WishlistActivity.EVENT,FacebookCustomLoggerMessage.WishlistActivity.CLOSE_IMAGE);
                break;
            case R.id.prevBTN:
                prevThumbnail();
                break;
            case R.id.nextBTN:
                nextThumbnail();
                break;
        }
    }

    private void nextThumbnail(){
        if(thumbnailIndex < thumbnailImages.size() -1 ){
            thumbnailIndex++;
            displayCounter();
        }
    }

    private void prevThumbnail(){
        if(thumbnailIndex > 0){
            thumbnailIndex--;
            displayCounter();
        }
    }

    @Override
    public void callback(String categoryItem) {
        categoryTXT.setText(categoryItem);

//        showCreatePostWalkThru();

        if(categoryItem.equalsIgnoreCase("other") || categoryItem.equalsIgnoreCase("others")){
            othersTIL.setVisibility(View.VISIBLE);
        }else{
            othersTIL.setVisibility(View.GONE);
        }
    }

    private void attemptCreate(){

        imageLBL.setVisibility(View.GONE);
        othersET.setError(null);

        CreateWishListRequest createWishListRequest = new CreateWishListRequest(getContext());

        if(file != null){
            createWishListRequest.addMultipartBody(Keys.server.key.FILE, file);
        }else {
            createWishListRequest.addMultipartBody(Keys.server.key.URL, StringFormatter.replaceNull(url))
                    .addMultipartBody(Keys.server.key.URL_TITLE, StringFormatter.replaceNull(urlTitle))
                    .addMultipartBody(Keys.server.key.URL_DESCRIPTION, urlDescription)
                    .addMultipartBody(Keys.server.key.URL_IMAGE, urlImage);
        }

        String category = categoryTXT.getText().toString();
        if(category.equalsIgnoreCase("other") || category.equalsIgnoreCase("others")){
           if(TextUtils.isEmpty(othersET.getText().toString())){
               othersET.setError("Field is required.");
               return;
           }else{
               category = othersET.getText().toString();
           }
        }

        createWishListRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Creating post...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addMultipartBody(Keys.server.key.TITLE, StringFormatter.replaceNull(urlTitle, category))
                .addMultipartBody(Keys.server.key.CATEGORY, category)
                .addMultipartBody(Keys.server.key.CONTENT, descriptionET.getText().toString())
                .execute();

    }

    private void attemptGetMeta(String meta_url){
        metaCON.setVisibility(View.VISIBLE);
        metaPB.setVisibility(View.VISIBLE);
        postBTN.setEnabled(false);
        metaDataCON.setVisibility(View.GONE);
        imageLBL.setVisibility(View.GONE);

        url = "";
        urlTitle = "";
        urlDescription = "-";
        urlImage = "/";
        thumbnailIndex = 0;

        LinkPreviewRequest linkPreviewRequest = new LinkPreviewRequest(getContext());
        linkPreviewRequest.addParameters(Keys.server.key.URL, meta_url)
                .execute();

        Log.e("Meta", ">>>" + meta_url);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    public void hideSoftKeyboard() {
        if(getActivity().getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Subscribe
    public void onResponse(CreateWishListRequest.ServerResponse responseData) {
        Log.e("ResponseCode", ">>>" + responseData.getCode());
        if(responseData.getCode() == 500){
            ToastMessage.show(getActivity(), "Something went wrong. Please try again!", ToastMessage.Status.FAILED);
        }
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if(wishListInfoTransformer.status){
            int count = UserData.getInt(UserData.TOTAL_POST) + 1;
            UserData.insert(UserData.TOTAL_POST, count);
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
            hideSoftKeyboard();

            if (!UserData.getBoolean(UserData.WALKTHRU_DONE)){
                UserData.insert(UserData.CREATE_POST_SHOW, 2);
                UserData.insert(UserData.WALKTHRU_CURRENT, 2);
                UserData.insert(UserData.WALKTHRU_CREATE_DONE, true);
                UserData.insert(UserData.PROFILE_CURRENT, true);
                Handler handler;
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                    wishListActivity.finish();
                    }
                }, 300);
            }else {
//                sampleSnackbar(postBTN);
                wishListActivity.finish();
            }

        }else{
            ToastMessage.show(getActivity(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
            if(wishListInfoTransformer.hasRequirements()){
                ErrorResponseManger.first(descriptionET, wishListInfoTransformer.requires.content);
                ErrorResponseManger.first(categoryTXT, wishListInfoTransformer.requires.category);

                if(!ErrorResponseManger.first(wishListInfoTransformer.requires.file).equals("")){
                    imageLBL.setVisibility(View.VISIBLE);
                }
            }
        }
        doneButtonClickTime = 0;
        clearPost();
    }

    private void sampleSnackbar(View view){
        SnackBar.make(view, "Success", SnackBar.SHORT);
        SnackBar.setDuration(500);
        SnackBar.setActionTextColor(ActivityCompat.getColor(getContext(),R.color.white));
//        SnackBar.setAction("Next", new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                contactsActive();
//            }
//        });
        SnackBar.addCallback(new Snackbar.Callback(){
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                wishListActivity.finish();
            }

            @Override
            public void onShown(Snackbar sb) {
                super.onShown(sb);
            }
        });
        SnackBar.show();
    }

    @Subscribe
    public void onResponse(LinkPreviewRequest.ServerResponse responseData) {
        if(responseData.getCode() == 500){
            closeMeta();
            postBTN.setEnabled(true);
        }
        LinkTransformer linkTransformer = responseData.getData(LinkTransformer.class);
        if(linkTransformer.status){
            metaPB.setVisibility(View.GONE);
            postBTN.setEnabled(true);
            metaDataCON.setVisibility(View.VISIBLE);

            urlTitle = linkTransformer.data.title;
            url = linkTransformer.data.url;
            urlDescription = linkTransformer.data.description;
            urlImage = "/";
            thumbnailIndex = 0;
            thumbnailImages = linkTransformer.data.images;
            if(thumbnailImages.size() > 0){
                displayCounter();
            }
            metaTitleTXT.setText(urlTitle);
            metaDescriptionTXT.setText(urlDescription);
        }else{
            metaCON.setVisibility(View.GONE);
            metaPB.setVisibility(View.GONE);
            postBTN.setEnabled(true);
            metaDataCON.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onResponse(BaseRequest.OnFailedResponse onFailedResponse){
        metaCON.setVisibility(View.GONE);
        metaPB.setVisibility(View.GONE);
        postBTN.setEnabled(true);
        metaDataCON.setVisibility(View.GONE);
    }

    private void displayCounter(){
        urlImage = thumbnailImages.get(thumbnailIndex);
        urlImage = urlImage.equals("") ? "/" : urlImage;
        Glide.with(getContext())
                .load(urlImage)
                .apply(new RequestOptions()
                .placeholder(R.drawable.icon_image_placeholder)
                .error(R.drawable.icon_image_corrupted))
                .into(metaImageIV);
        counterTXT.setText(String.valueOf(thumbnailIndex + 1) + " of " + thumbnailImages.size());
        counterCON.setVisibility(thumbnailImages.size() < 2 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void result(File file) {
        this.file = file;
        cameraCon.setVisibility(View.VISIBLE);
//        createPostSV.smoothScrollTo(0, 0);
//        createPostSV.scrollTo(10,100);

        postBTN.setEnabled(true);
        Glide.with(getContext())
                .load(file)
                .into(imageIV);


//        handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                createPostSV.scrollTo(0, createPostSV.getBottom());
//            showCategoryWalkThru();
//            }
//        }, 1000);
    }

    public void closeImage(){
        file = null;
        cameraCon.setVisibility(View.GONE);
        postBTN.setEnabled(true);
//        showAddPhotoWalkThru();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_GALLERY) {
            if(wishListActivity.isAllPermissionResultGranted(grantResults)){
                downloadImage(urlImage);
            }else {
                wishListActivity.onBackPressed();
            }
        }
//        UserData.insert(UserData.WALKTHRU_CREATE_POST_SHOW,true);
    }

    @Override
    public void onBackPressed() {
        exitConfirmation();
    }

    public void exitConfirmation(){
        if(descriptionET.getText().toString().length() > 0){
            final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
            confirmationDialog
                    .setDescription("Save Post as Draft?")
                    .setNote("Click save so you can still edit this post later.")
                    .setIcon(R.drawable.icon_information)
                    .setPositiveButtonText("Save")
                    .setPositiveButtonClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            savePost();
                            wishListActivity.finish();
                            confirmationDialog.dismiss();
                        }
                    })
                    .setNegativeButtonClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            clearPost();
                            wishListActivity.finish();
                            confirmationDialog.dismiss();
                        }
                    })
                    .setNegativeButtonText("Discard")
                    .build(wishListActivity.getSupportFragmentManager());
        }else{
            wishListActivity.finish();
            clearPost();
        }
    }

    private void clearPost(){
        UserData.insert(UserData.POST_MESSAGE, "");
        UserData.insert(UserData.POST_IMAGE, "");
        UserData.insert(UserData.STREET_ADDRESS, "");
        UserData.insert(UserData.CITY, "");
        UserData.insert(UserData.STATE, "");
        UserData.insert(UserData.ZIP, "");
        UserData.insert(UserData.CONTACT_NUMBER, "");
        UserData.insert(UserData.CATEGORY, "");
        UserData.insert(UserData.COUNTRY, "");
    }

    private void savePost(){
        UserData.insert(UserData.POST_MESSAGE, descriptionET.getText().toString());
        UserData.insert(UserData.POST_IMAGE, "");
        UserData.insert(UserData.CATEGORY, categoryTXT.getText().toString());
    }

    @Override
    public void onItemClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startItemActivity(wishListItem.id, wishListItem.owner.data.id, "i_like");
    }

    @Override
    public void onCommentClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startCommentActivity(wishListItem.id, wishListItem.title);
    }

    @Override
    public void onAvatarClick(UserItem userItem) {
        ((RouteActivity) getContext()).startProfileActivity(userItem.id);
    }

    @Override
    public void onGrabClick(WishListItem wishListItem) {

        String data = new Gson().toJson(wishListItem);
        ((RouteActivity) getContext()).startWishListActivity(data, "create_grab_other");
    }

    @Override
    public void onHideClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to hide?")
                .setNote("You will no longer see this post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Hide")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BlockRequest blockRequest = new BlockRequest(getContext());
                        blockRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());

    }

    @Override
    public void onUnfollowClick(final WishListItem wishListItem) {
        if(wishListItem.owner.data.social.data.is_following.equals("yes")){
            UnfollowDialog.newInstance(wishListItem.owner.data.id, wishListItem.owner.data.name, new UnfollowDialog.Callback() {
                @Override
                public void onAccept(int userID) {
                    UnFollowRequest unFollowRequest = new UnFollowRequest(getContext());
                    unFollowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                            .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                            .addParameters(Keys.server.key.USER_ID, wishListItem.owner.data.id)
                            .execute();
                }

                @Override
                public void onCancel(int userID) {

                }
            }).show(((BaseActivity) getContext()).getSupportFragmentManager(), UnfollowDialog.TAG);
        }else{

            GroupDialog.newInstance(wishListItem.owner.data, this).show(getFragmentManager(),GroupDialog.TAG);
        }
    }

    @Override
    public void onReportClick(final WishListItem wishListItem) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to report?")
                .setNote("You are about to report a post.")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Report")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportRequest reportRequest = new ReportRequest(getContext());
                        reportRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                                .addParameters(Keys.server.key.TYPE, "wishlist")
                                .addParameters(Keys.server.key.REFERENCE_ID, wishListItem.id)
                                .execute();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .build(getChildFragmentManager());
    }

    @Override
    public void onHeartClick(WishListItem wishListItem) {

        LikeRequest likeRequest = new LikeRequest(getContext());
        likeRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,image,owner.social")
                .addParameters(Keys.server.key.WISHLIST_ID, wishListItem.id)
                .execute();
    }

    @Override
    public void onRepostClick(WishListItem wishListItem) {

        String data = new Gson().toJson(wishListItem);
        ((RouteActivity) getContext()).startWishListActivity(data, "create_grab_other");
    }

    @Override
    public void onRepostUsersClick(WishListItem wishListItem) {
        ((RouteActivity) getContext()).startSearchActivity(wishListItem.id, wishListItem.owner.data.common_name, "repost");
    }

    @Override
    public void onHeaderClick(EventItem eventItem) {
        ((RouteActivity) getContext()).startBirthdayActivity(eventItem, "event");
    }

    @Override
    public void onHeaderShowClick(EventItem eventItem) {
        ((RouteActivity) getContext()).startBirthdayActivity(eventItem, "greeting");
    }

    @Override
    public void onGroupSelect(String groupName, int id) {
        Log.e("Group Name",">>>" + groupName);
        FollowRequest followRequest = new FollowRequest(getContext());
        followRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Please wait...", false, false))
                .addParameters(Keys.server.key.USER_ID, id)
                .addParameters(Keys.server.key.GROUP,groupName)
                .execute();
    }

    @Override
    public void onCancel(String groupName, int id) {
//        loadingPB.setVisibility(View.GONE);
//        commandBTN.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onResponse(BlockRequest.ServerResponse responseData) {
        WishListInfoTransformer wishListInfoTransformer = responseData.getData(WishListInfoTransformer.class);
        if (wishListInfoTransformer.status) {
            socialFeedRecyclerViewAdapter.removeItem(wishListInfoTransformer.wishListItem);
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(getContext(), wishListInfoTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(UnFollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
//            ownerInfo(userTransformer.userItem);
        }
//        loadingPB.setVisibility(View.GONE);
//        commandTXT.setVisibility(View.VISIBLE);
//        commandBTN.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onResponse(FollowRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
//            ownerInfo(userTransformer.userItem);
        }
//        loadingPB.setVisibility(View.GONE);
//        commandBTN.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onResponse(WishListRecommendedRequest.ServerResponse responseData) {
        WishListRelatedTransformer wishListRelatedTransformer = responseData.getData(WishListRelatedTransformer.class);
        if (wishListRelatedTransformer.status) {
            if (responseData.isNext()) {
                socialFeedRecyclerViewAdapter.addNewData(wishListRelatedTransformer.data);
            } else {
                socialFeedRecyclerViewAdapter.setNewData(wishListRelatedTransformer.data);
            }
        }

        loadingIV.setVisibility(View.GONE);
        recommendedPB.setVisibility(View.GONE);
        recommmendedRV.setVisibility(View.VISIBLE);
        if (socialFeedRecyclerViewAdapter.getItemCount() == 0) {
            relatedCON.setVisibility(View.GONE);
            recommendedCON.setVisibility(View.GONE);
        } else {
            relatedCON.setVisibility(View.VISIBLE);
            recommendedCON.setVisibility(View.VISIBLE);
        }
    }
}
