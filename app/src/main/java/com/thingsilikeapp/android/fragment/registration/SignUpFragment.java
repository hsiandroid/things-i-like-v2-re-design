package com.thingsilikeapp.android.fragment.registration;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.analytics.Analytics;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.RegistrationActivity;
import com.thingsilikeapp.android.dialog.CategoryDialog;
import com.thingsilikeapp.android.dialog.CountryDialog;
import com.thingsilikeapp.config.Keys;
import com.thingsilikeapp.data.preference.CountryData;
import com.thingsilikeapp.data.preference.UserData;
import com.thingsilikeapp.server.request.auth.SignUpRequest;
import com.thingsilikeapp.server.transformer.user.UserTransformer;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.PasswordEditTextManager;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.java.facebook.FacebookCustomLoggerMessage;
import com.thingsilikeapp.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class SignUpFragment extends BaseFragment implements
        View.OnClickListener,
        CountryDialog.CountryPickerListener {

    public static final String TAG = SignUpFragment.class.getName();

    public RegistrationActivity registrationActivity;
    private CountryData.Country countryData;

    @BindView(R.id.nameET)                  EditText nameET;
    @BindView(R.id.displayNameET)           EditText displayNameET;
    @BindView(R.id.contactET)               EditText contactET;
    @BindView(R.id.contactCodeTXT)          TextView contactCodeTXT;
    @BindView(R.id.emailET)                 EditText emailET;
    @BindView(R.id.passwordET)              EditText passwordET;
    @BindView(R.id.showPasswordBTN)         ImageView showPasswordBTN;
    @BindView(R.id.confirmPasswordET)       EditText confirmPasswordET;
    @BindView(R.id.showConfirmPasswordBTN)  ImageView showConfirmPasswordBTN;
    @BindView(R.id.signUpTV)                TextView signUpTV;
    @BindView(R.id.termsBTN)                TextView termsBTN;
    @BindView(R.id.showPasswordCHBX)        CheckBox showPasswordCHBX;
    @BindView(R.id.showPassword1CHBX)       CheckBox showPassword1CHBX;

    public static SignUpFragment newInstance() {
        SignUpFragment signUpFragment = new SignUpFragment();
        return signUpFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_signup;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();
        registrationActivity.setTitle(getString(R.string.title_sign_up));
        contactCodeTXT.setOnClickListener(this);
        signUpTV.setOnClickListener(this);
        termsBTN.setOnClickListener(this);
        if (showPasswordCHBX.isChecked()) {
            showPassword1CHBX.setChecked(true);
        } else {
            showPassword1CHBX.setChecked(false);
        }
        PasswordEditTextManager.addShowPassword(getContext(), passwordET, showPassword1CHBX);
        PasswordEditTextManager.addShowPassword(getContext(), confirmPasswordET, showPasswordCHBX);
        PasswordEditTextManager.usernameFormat(displayNameET);


        countryData = CountryData.getCountryDataByCode1("AF");
        contactCodeTXT.setText(countryData.code3);
        Analytics.trackEvent("registration_SignUpFragment_onViewReady");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signUpTV:
                attemptSignUp();
                Analytics.trackEvent("registration_SignUpFragment_signUpTV");
                break;
            case R.id.termsBTN:
                openUrl("http://privacypolicy.technology/kpp.php");
                Analytics.trackEvent("registration_SignUpFragment_termsBTN");
                break;
            case R.id.contactCodeTXT:
                CountryDialog.newInstance(this).show(getChildFragmentManager(), CategoryDialog.TAG);
                Analytics.trackEvent("registration_SignUpFragment_contactCodeTXT");
                break;
        }
    }

    public void openUrl(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    private void attemptSignUp() {
        SignUpRequest signUpRequest = new SignUpRequest(getContext());
        signUpRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Signing up...", false, false))
                .setDeviceRegID(registrationActivity.getDeviceRegID())
                .addParameters(Keys.server.key.INCLUDE, "info,social,statistics")
                .addParameters(Keys.server.key.NAME, nameET.getText().toString())
                .addParameters(Keys.server.key.USERNAME, displayNameET.getText().toString())
//                .addParameters(Keys.server.key.CONTACT_NUMBER, contactCodeTXT.getText().toString() + contactET.getText().toString())
                .addParameters(Keys.server.key.EMAIL, emailET.getText().toString())
                .addParameters(Keys.server.key.PASSWORD, passwordET.getText().toString())
                .addParameters(Keys.server.key.PASSWORD_CONFIRMATION, confirmPasswordET.getText().toString())
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(SignUpRequest.ServerResponse responseData) {
        UserTransformer userTransformer = responseData.getData(UserTransformer.class);
        if (userTransformer.status) {
            UserData.insert(userTransformer.userItem);
            UserData.insert(UserData.AUTHORIZATION, userTransformer.token);
            UserData.insert(UserData.FIRST_LOGIN, true);
            UserData.insert(UserData.SHOW_SHARE, true);
            UserData.insert(UserData.TOKEN_EXPIRED, false);

            if (!UserData.getBoolean(UserData.WALKTHRU_DONE, false)) {
                UserData.insert(UserData.WALKTHRU_DONE, false);
                UserData.insert(UserData.WALKTHRU_CURRENT, 0);
            } else {
                UserData.insert(UserData.WALKTHRU_DONE, true);
                UserData.insert(UserData.WALKTHRU_CURRENT, 10);
            }
            UserData.insert(UserData.TOTAL_POST, userTransformer.userItem.statistics.data.total_wishlist);
            registrationActivity.startMainActivity("home");
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.SUCCESS);
            registrationActivity.facebookLogger(FacebookCustomLoggerMessage.MainActivity.EVENT, FacebookCustomLoggerMessage.SIGN_UP);
        } else {
            ToastMessage.show(getActivity(), userTransformer.msg, ToastMessage.Status.FAILED);
            if (userTransformer.hasRequirements()) {
                ErrorResponseManger.first(nameET, userTransformer.requires.name);
                ErrorResponseManger.first(displayNameET, userTransformer.requires.username);
                ErrorResponseManger.first(emailET, userTransformer.requires.email);
                ErrorResponseManger.first(passwordET, userTransformer.requires.password);
                ErrorResponseManger.first(confirmPasswordET, userTransformer.requires.password);
            }
        }
    }

    @Override
    public void onSelectCountry(CountryData.Country country, int requestCode) {
        contactCodeTXT.setText(country.code3);
    }
}
