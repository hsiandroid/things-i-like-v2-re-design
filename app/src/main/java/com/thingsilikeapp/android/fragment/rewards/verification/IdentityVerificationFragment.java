package com.thingsilikeapp.android.fragment.rewards.verification;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.microsoft.appcenter.analytics.Analytics;
import com.squareup.picasso.Picasso;
import com.thingsilikeapp.R;
import com.thingsilikeapp.android.activity.VerificationActivity;
import com.thingsilikeapp.android.dialog.ImagePickerV2Dialog;
import com.thingsilikeapp.data.model.KYCItem;
import com.thingsilikeapp.data.model.UserItem;
import com.thingsilikeapp.server.KYC;
import com.thingsilikeapp.vendor.android.base.BaseFragment;
import com.thingsilikeapp.vendor.android.java.ToastMessage;
import com.thingsilikeapp.vendor.android.widget.image.ResizableImageView;
import com.thingsilikeapp.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class IdentityVerificationFragment extends BaseFragment implements ImagePickerV2Dialog.ImageCallback, View.OnClickListener {

    public static final String TAG = IdentityVerificationFragment.class.getName();

    @BindView(R.id.attachBTN) TextView attachBTN;
    @BindView(R.id.submitBTN) TextView submitBTN;
    @BindView(R.id.typeSPR)   Spinner typeSPR;
    @BindView(R.id.imageIV)   ResizableImageView imageView;

    @State String types;
    @State File file;

    private VerificationActivity verificationActivity;

    public static IdentityVerificationFragment newInstance() {
        IdentityVerificationFragment fragment = new IdentityVerificationFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_verification_identityn;
    }

    @Override
    public void onViewReady() {
        verificationActivity = (VerificationActivity) getContext();
        verificationActivity.setTitle("Identity Verification");
        attachBTN.setOnClickListener(this);
        setupTypeSPR();
        submitBTN.setOnClickListener(this);
        verificationActivity.setBackBTN(true);
        Analytics.trackEvent("rewards_verification_IdentityVerificationFragment_onViewReady");
    }

    private void setupTypeSPR(){
        final List<String> type = new ArrayList<String>();
        type.add("Passport");
        type.add("Driver's License");
        type.add("Multi-purpose ID");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, type);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSPR.setAdapter(dataAdapter);

        typeSPR.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (parent.getItemAtPosition(position).toString()){
                    case "Passport":
                        types = "Passport";
                        break;
                    case "Driver's License":
                        types = "Driver's License";
                        break;
                    case "Multi-purpose ID":
                        types = "Multi-purpose ID";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void result(File file) {
        this.file = file;
        Picasso.with(getContext()).load(file).into(imageView);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(KYC.IdResponse responseData) {
        SingleTransformer<UserItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            verificationActivity.openVerificationFragment();
        }else{
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.attachBTN:
                ImagePickerV2Dialog.newInstance("Upload Photo", false, this).show(getFragmentManager(), TAG);
                Analytics.trackEvent("rewards_verification_IdentityVerificationFragment_attachBTN");
                break;
            case R.id.submitBTN:
                KYC.getDefault().identity(getContext(), types, file);
                Analytics.trackEvent("rewards_verification_IdentityVerificationFragment_submitBTN");
                break;
        }
    }
}
