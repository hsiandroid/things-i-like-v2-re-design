# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

########## For okio #############
-dontwarn okio.**

########## For EventBus #############
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}

########## For Picasso #############
-dontwarn com.squareup.okhttp.**


-dontwarn org.slf4j.**

########## For Crashlytics #############
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception
-printmapping mapping.txt
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**


########## For AndEngine #############
-keep class org.andengine.** { *; }
-dontwarn org.andengine.**

########## For TwoWayView #############
-keep class org.lucasr.twowayview.** { *; }

-keepnames class com.mypackage.MyGlideModule

-dontwarn com.googlecode.mp4parser.**
# or more generally:
#-keep public class * implements com.bumptech.glide.module.GlideModule

# for DexGuard only
#-keepresourcexmlelements manifest/application/meta-data@value=GlideModule

#ijkplayer
-keep class tv.danmaku.ijk.media.player.** {*; }
-keep class tv.danmaku.ijk.media.player.IjkMediaPlayer{
*;
}
-keep class tv.danmaku.ijk.media.player.ffmpeg.FFmpegApi{
*;
}

